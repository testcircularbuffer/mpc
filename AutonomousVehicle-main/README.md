# Autonomous Electric Vehicles

## Description

Thesis Autonomous Electric Vehicles

[another]

[Hardware](https://github.com/quanghuy26/AutonomousVehicle/tree/main/HardwareDocs/07_AEV_CabiNet)

[GUI Application](https://github.com/quanghuy26/AutonomousVehicle/tree/main/GUI)


## Usage

### Build ROS nodes

##### 1. Node A

    ...

##### 2. Node B

    ...

### How to run

##### 1. Run file `.sh` in folder aev_ws

    sh ~/aev/aev_ws/run.sh
    
##### 2. Run `test all` to print Data System - option

    ...
    
##### 3. Run `CAN-python`

    ...CAN_node_py ????
    
    
    
## Notes

### Switch Mode MANUAL to AUTO

1. Chuyển switch FW/BW trên xe về vị trí   **BW (xe lùi)**
2. Nhấn công tắc **AUTO** trên tủ điện để chuyển Mode
3. Khi xe đang ở chế độ AUTO, **không** tác động đến chân Ga. Nếu không tín hiệu DAC sẽ không được điều khiển.


### Remote to Embedded

##### 1. Password AEV

    MTN Jetson Nx:    thepassword
    Terminal sudo:    jetson12
    dlink Router:     12345678
    
##### 2. Static IP
    
    Router: 192.168.0.1
    Jetson: 192.168.1.2
    Mr.Huy: 192.168.1.3
    Mr.Tin: 192.168.1.4
    Mr.


##### 3. How to connect ROS Master across multiple machines
###### Chú ý: chỉ thao tác trên cùng 1 terminal. Hoặc có thể thêm `export ROS_MASTER_URI=http://192.168.0.2:11311` vào trong file `.bashrc` để tự connect Master
    1. Kết nối chung vào mạng LAN **dlink** wifi
    2. Mở terminal:

        export ROS_MASTER_URI=http://192.168.0.2:11311

    3. Run any node
    4. Check the connection:

        rostopic list



### AEV chạy thực tế

##### 1. Led Status STMF4

    Led Orange:   ON = stop   OFF = start
    Led Blue:     ON = BW     OFF = FW
    Led Green:    CAN
    Led Red:      Brake
    

## Acknowledgments and References

another ...

https://github.com/giangtin1920/ttcRadar_pkg
