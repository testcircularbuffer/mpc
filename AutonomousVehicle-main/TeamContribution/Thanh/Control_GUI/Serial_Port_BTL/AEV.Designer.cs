﻿
namespace Serial_Port_BTL
{
    partial class AEV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Setting = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_communicate = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tBoxOutData = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbLengthSendData = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSendData = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tBoxDataReceive = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbLengthReceiveData = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chBoxAddToOldData = new System.Windows.Forms.CheckBox();
            this.chBoxAlwayUpDate = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.cBoxPatityBits = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lbStaticComPort = new System.Windows.Forms.Label();
            this.cBoxStopBits = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.cBoxDataBits = new System.Windows.Forms.ComboBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.cBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.cBoxComPort = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Communication = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.aGauge2 = new System.Windows.Forms.AGauge();
            this.break_pbar = new System.Windows.Forms.ProgressBar();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbox_break = new System.Windows.Forms.TextBox();
            this.btn_returnsetting = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tbox_Setspeed = new System.Windows.Forms.TextBox();
            this.btn_backward = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.track_break = new System.Windows.Forms.TrackBar();
            this.btn_forward = new System.Windows.Forms.Button();
            this.btn_turnright = new System.Windows.Forms.Button();
            this.btn_ahead = new System.Windows.Forms.Button();
            this.btn_turnleft = new System.Windows.Forms.Button();
            this.aGauge1 = new System.Windows.Forms.AGauge();
            this.btn_horn = new System.Windows.Forms.Button();
            this.btn_frontlight = new System.Windows.Forms.Button();
            this.track_speed = new System.Windows.Forms.TrackBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbox_latitude = new System.Windows.Forms.TextBox();
            this.tbox_longtitude = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Setting.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.Communication.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.track_break)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_speed)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(943, 460);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Setting);
            this.tabControl1.Controls.Add(this.Communication);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(931, 431);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseClick);
            // 
            // Setting
            // 
            this.Setting.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Setting.Controls.Add(this.textBox1);
            this.Setting.Controls.Add(this.btn_communicate);
            this.Setting.Controls.Add(this.groupBox11);
            this.Setting.Controls.Add(this.groupBox4);
            this.Setting.Controls.Add(this.groupBox1);
            this.Setting.Location = new System.Drawing.Point(4, 22);
            this.Setting.Name = "Setting";
            this.Setting.Padding = new System.Windows.Forms.Padding(3);
            this.Setting.Size = new System.Drawing.Size(923, 405);
            this.Setting.TabIndex = 0;
            this.Setting.Text = "Setting";
            this.Setting.Click += new System.EventHandler(this.Setting_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(233, 372);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(350, 20);
            this.textBox1.TabIndex = 26;
            // 
            // btn_communicate
            // 
            this.btn_communicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_communicate.Location = new System.Drawing.Point(6, 361);
            this.btn_communicate.Name = "btn_communicate";
            this.btn_communicate.Size = new System.Drawing.Size(179, 38);
            this.btn_communicate.TabIndex = 25;
            this.btn_communicate.Text = "COMMUNICATION";
            this.btn_communicate.UseVisualStyleBackColor = true;
            this.btn_communicate.Click += new System.EventHandler(this.btn_communicate_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.groupBox3);
            this.groupBox11.Controls.Add(this.groupBox8);
            this.groupBox11.Location = new System.Drawing.Point(6, 32);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(301, 327);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Data Send";
            this.groupBox11.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tBoxOutData);
            this.groupBox3.Location = new System.Drawing.Point(13, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(275, 157);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Write Data Send Here";
            // 
            // tBoxOutData
            // 
            this.tBoxOutData.Location = new System.Drawing.Point(6, 19);
            this.tBoxOutData.Multiline = true;
            this.tBoxOutData.Name = "tBoxOutData";
            this.tBoxOutData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBoxOutData.Size = new System.Drawing.Size(261, 129);
            this.tBoxOutData.TabIndex = 15;
            this.tBoxOutData.TextChanged += new System.EventHandler(this.tBoxOutData_TextChanged_1);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Controls.Add(this.button2);
            this.groupBox8.Controls.Add(this.btnSendData);
            this.groupBox8.Location = new System.Drawing.Point(13, 182);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(275, 141);
            this.groupBox8.TabIndex = 20;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Control";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lbLengthSendData);
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(16, 89);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(248, 41);
            this.groupBox9.TabIndex = 18;
            this.groupBox9.TabStop = false;
            // 
            // lbLengthSendData
            // 
            this.lbLengthSendData.AutoSize = true;
            this.lbLengthSendData.Location = new System.Drawing.Point(127, 16);
            this.lbLengthSendData.Name = "lbLengthSendData";
            this.lbLengthSendData.Size = new System.Drawing.Size(23, 15);
            this.lbLengthSendData.TabIndex = 21;
            this.lbLengthSendData.Text = "00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "Data Send Length: ";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(156, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 56);
            this.button2.TabIndex = 18;
            this.button2.Text = "Clear Data Send";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSendData
            // 
            this.btnSendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnSendData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSendData.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnSendData.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendData.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSendData.Location = new System.Drawing.Point(25, 32);
            this.btnSendData.Name = "btnSendData";
            this.btnSendData.Size = new System.Drawing.Size(92, 54);
            this.btnSendData.TabIndex = 2;
            this.btnSendData.Text = "Send CMD";
            this.btnSendData.UseVisualStyleBackColor = false;
            this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox12);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Location = new System.Drawing.Point(313, 32);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(315, 327);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Receive";
            this.groupBox4.Visible = false;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tBoxDataReceive);
            this.groupBox12.Location = new System.Drawing.Point(6, 19);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(295, 157);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            // 
            // tBoxDataReceive
            // 
            this.tBoxDataReceive.BackColor = System.Drawing.Color.White;
            this.tBoxDataReceive.Location = new System.Drawing.Point(6, 19);
            this.tBoxDataReceive.Multiline = true;
            this.tBoxDataReceive.Name = "tBoxDataReceive";
            this.tBoxDataReceive.ReadOnly = true;
            this.tBoxDataReceive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBoxDataReceive.Size = new System.Drawing.Size(278, 129);
            this.tBoxDataReceive.TabIndex = 15;
            this.tBoxDataReceive.TextChanged += new System.EventHandler(this.tBoxDataReceive_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Location = new System.Drawing.Point(6, 182);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(295, 139);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Control";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbLengthReceiveData);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(16, 91);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(268, 41);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            // 
            // lbLengthReceiveData
            // 
            this.lbLengthReceiveData.AutoSize = true;
            this.lbLengthReceiveData.Location = new System.Drawing.Point(160, 17);
            this.lbLengthReceiveData.Name = "lbLengthReceiveData";
            this.lbLengthReceiveData.Size = new System.Drawing.Size(23, 15);
            this.lbLengthReceiveData.TabIndex = 21;
            this.lbLengthReceiveData.Text = "00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "Data Receive Length: ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(16, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 57);
            this.button1.TabIndex = 18;
            this.button1.Text = "Clear Data Receive";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chBoxAddToOldData);
            this.groupBox5.Controls.Add(this.chBoxAlwayUpDate);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(137, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(147, 75);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            // 
            // chBoxAddToOldData
            // 
            this.chBoxAddToOldData.AutoSize = true;
            this.chBoxAddToOldData.Location = new System.Drawing.Point(11, 47);
            this.chBoxAddToOldData.Name = "chBoxAddToOldData";
            this.chBoxAddToOldData.Size = new System.Drawing.Size(113, 19);
            this.chBoxAddToOldData.TabIndex = 17;
            this.chBoxAddToOldData.Text = "Show all data";
            this.chBoxAddToOldData.UseVisualStyleBackColor = true;
            this.chBoxAddToOldData.CheckedChanged += new System.EventHandler(this.chBoxAddToOldData_CheckedChanged_1);
            // 
            // chBoxAlwayUpDate
            // 
            this.chBoxAlwayUpDate.AutoSize = true;
            this.chBoxAlwayUpDate.Location = new System.Drawing.Point(11, 22);
            this.chBoxAlwayUpDate.Name = "chBoxAlwayUpDate";
            this.chBoxAlwayUpDate.Size = new System.Drawing.Size(116, 19);
            this.chBoxAlwayUpDate.TabIndex = 16;
            this.chBoxAlwayUpDate.Text = "Only new data";
            this.chBoxAlwayUpDate.UseVisualStyleBackColor = true;
            this.chBoxAlwayUpDate.CheckedChanged += new System.EventHandler(this.chBoxAlwayUpDate_CheckedChanged_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.cBoxPatityBits);
            this.groupBox1.Controls.Add(this.groupBox10);
            this.groupBox1.Controls.Add(this.cBoxStopBits);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.cBoxDataBits);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Controls.Add(this.cBoxBaudRate);
            this.groupBox1.Controls.Add(this.cBoxComPort);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox1.Location = new System.Drawing.Point(675, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 327);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Com Port Control";
            // 
            // progressBar1
            // 
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.progressBar1.Location = new System.Drawing.Point(42, 257);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(156, 23);
            this.progressBar1.TabIndex = 2;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Location = new System.Drawing.Point(75, 288);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(96, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cBoxPatityBits
            // 
            this.cBoxPatityBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxPatityBits.FormattingEnabled = true;
            this.cBoxPatityBits.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.cBoxPatityBits.Location = new System.Drawing.Point(110, 146);
            this.cBoxPatityBits.Name = "cBoxPatityBits";
            this.cBoxPatityBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxPatityBits.TabIndex = 17;
            this.cBoxPatityBits.Text = "None";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lbStaticComPort);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(110, 182);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(109, 69);
            this.groupBox10.TabIndex = 19;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "State Com Port";
            // 
            // lbStaticComPort
            // 
            this.lbStaticComPort.AutoSize = true;
            this.lbStaticComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStaticComPort.Location = new System.Drawing.Point(16, 25);
            this.lbStaticComPort.Name = "lbStaticComPort";
            this.lbStaticComPort.Size = new System.Drawing.Size(72, 31);
            this.lbStaticComPort.TabIndex = 22;
            this.lbStaticComPort.Text = "OFF";
            // 
            // cBoxStopBits
            // 
            this.cBoxStopBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxStopBits.FormattingEnabled = true;
            this.cBoxStopBits.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.cBoxStopBits.Location = new System.Drawing.Point(110, 116);
            this.cBoxStopBits.Name = "cBoxStopBits";
            this.cBoxStopBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxStopBits.TabIndex = 16;
            this.cBoxStopBits.Text = "One";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(23, 224);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cBoxDataBits
            // 
            this.cBoxDataBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxDataBits.FormattingEnabled = true;
            this.cBoxDataBits.Items.AddRange(new object[] {
            "6",
            "7",
            "8"});
            this.cBoxDataBits.Location = new System.Drawing.Point(110, 87);
            this.cBoxDataBits.Name = "cBoxDataBits";
            this.cBoxDataBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxDataBits.TabIndex = 15;
            this.cBoxDataBits.Text = "8";
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(23, 189);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "OPEN";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click_1);
            // 
            // cBoxBaudRate
            // 
            this.cBoxBaudRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxBaudRate.FormattingEnabled = true;
            this.cBoxBaudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200"});
            this.cBoxBaudRate.Location = new System.Drawing.Point(110, 56);
            this.cBoxBaudRate.Name = "cBoxBaudRate";
            this.cBoxBaudRate.Size = new System.Drawing.Size(109, 21);
            this.cBoxBaudRate.TabIndex = 14;
            this.cBoxBaudRate.Text = "9600";
            // 
            // cBoxComPort
            // 
            this.cBoxComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxComPort.FormattingEnabled = true;
            this.cBoxComPort.Location = new System.Drawing.Point(110, 26);
            this.cBoxComPort.Name = "cBoxComPort";
            this.cBoxComPort.Size = new System.Drawing.Size(109, 21);
            this.cBoxComPort.TabIndex = 13;
            this.cBoxComPort.Text = "COM4";
            this.cBoxComPort.SelectedIndexChanged += new System.EventHandler(this.cBoxComPort_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Parity bit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Stop bit";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Data bits";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Baudrate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "COM Port";
            // 
            // Communication
            // 
            this.Communication.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Communication.Controls.Add(this.groupBox13);
            this.Communication.Controls.Add(this.groupBox2);
            this.Communication.Location = new System.Drawing.Point(4, 22);
            this.Communication.Name = "Communication";
            this.Communication.Padding = new System.Windows.Forms.Padding(3);
            this.Communication.Size = new System.Drawing.Size(923, 405);
            this.Communication.TabIndex = 1;
            this.Communication.Text = "Communication";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.aGauge2);
            this.groupBox13.Controls.Add(this.break_pbar);
            this.groupBox13.Controls.Add(this.label18);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.label15);
            this.groupBox13.Controls.Add(this.label10);
            this.groupBox13.Controls.Add(this.label8);
            this.groupBox13.Controls.Add(this.tbox_break);
            this.groupBox13.Controls.Add(this.btn_returnsetting);
            this.groupBox13.Controls.Add(this.label14);
            this.groupBox13.Controls.Add(this.tbox_Setspeed);
            this.groupBox13.Controls.Add(this.btn_backward);
            this.groupBox13.Controls.Add(this.label7);
            this.groupBox13.Controls.Add(this.track_break);
            this.groupBox13.Controls.Add(this.btn_forward);
            this.groupBox13.Controls.Add(this.btn_turnright);
            this.groupBox13.Controls.Add(this.btn_ahead);
            this.groupBox13.Controls.Add(this.btn_turnleft);
            this.groupBox13.Controls.Add(this.aGauge1);
            this.groupBox13.Controls.Add(this.btn_horn);
            this.groupBox13.Controls.Add(this.btn_frontlight);
            this.groupBox13.Controls.Add(this.track_speed);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(3, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(694, 401);
            this.groupBox13.TabIndex = 18;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "VEHICLE CONTROL";
            this.groupBox13.Enter += new System.EventHandler(this.groupBox13_Enter);
            // 
            // aGauge2
            // 
            this.aGauge2.BaseArcColor = System.Drawing.Color.Gray;
            this.aGauge2.BaseArcRadius = 80;
            this.aGauge2.BaseArcStart = 202;
            this.aGauge2.BaseArcSweep = 135;
            this.aGauge2.BaseArcWidth = 2;
            this.aGauge2.GaugeAutoSize = false;
            this.aGauge2.Location = new System.Drawing.Point(491, 18);
            this.aGauge2.MaxValue = 45F;
            this.aGauge2.MinValue = -45F;
            this.aGauge2.Name = "aGauge2";
            this.aGauge2.NeedleColor1 = System.Windows.Forms.AGaugeNeedleColor.Gray;
            this.aGauge2.NeedleColor2 = System.Drawing.Color.DimGray;
            this.aGauge2.NeedleRadius = 60;
            this.aGauge2.NeedleType = System.Windows.Forms.NeedleType.Advance;
            this.aGauge2.NeedleWidth = 4;
            this.aGauge2.ScaleLinesInterColor = System.Drawing.Color.Black;
            this.aGauge2.ScaleLinesInterInnerRadius = 73;
            this.aGauge2.ScaleLinesInterOuterRadius = 80;
            this.aGauge2.ScaleLinesInterWidth = 1;
            this.aGauge2.ScaleLinesMajorColor = System.Drawing.Color.Black;
            this.aGauge2.ScaleLinesMajorInnerRadius = 70;
            this.aGauge2.ScaleLinesMajorOuterRadius = 80;
            this.aGauge2.ScaleLinesMajorStepValue = 45F;
            this.aGauge2.ScaleLinesMajorWidth = 2;
            this.aGauge2.ScaleLinesMinorColor = System.Drawing.Color.Gray;
            this.aGauge2.ScaleLinesMinorInnerRadius = 75;
            this.aGauge2.ScaleLinesMinorOuterRadius = 80;
            this.aGauge2.ScaleLinesMinorTicks = 9;
            this.aGauge2.ScaleLinesMinorWidth = 1;
            this.aGauge2.ScaleNumbersColor = System.Drawing.Color.Black;
            this.aGauge2.ScaleNumbersFormat = null;
            this.aGauge2.ScaleNumbersRadius = 90;
            this.aGauge2.ScaleNumbersRotation = 0;
            this.aGauge2.ScaleNumbersStartScaleLine = 0;
            this.aGauge2.ScaleNumbersStepScaleLines = 1;
            this.aGauge2.Size = new System.Drawing.Size(197, 113);
            this.aGauge2.TabIndex = 44;
            this.aGauge2.Text = "aGauge2";
            this.aGauge2.Value = 0F;
            this.aGauge2.ValueInRangeChanged += new System.EventHandler<System.Windows.Forms.ValueInRangeChangedEventArgs>(this.aGauge2_ValueInRangeChanged);
            // 
            // break_pbar
            // 
            this.break_pbar.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.break_pbar.Location = new System.Drawing.Point(535, 236);
            this.break_pbar.Name = "break_pbar";
            this.break_pbar.Size = new System.Drawing.Size(32, 139);
            this.break_pbar.Step = 1;
            this.break_pbar.TabIndex = 43;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(420, 283);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 16);
            this.label18.TabIndex = 41;
            this.label18.Text = "30 Km/h";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(194, 283);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 16);
            this.label16.TabIndex = 40;
            this.label16.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(599, 361);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 16);
            this.label15.TabIndex = 39;
            this.label15.Text = "0%";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(599, 236);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 16);
            this.label10.TabIndex = 38;
            this.label10.Text = "100%";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(656, 298);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = "%";
            // 
            // tbox_break
            // 
            this.tbox_break.Location = new System.Drawing.Point(602, 292);
            this.tbox_break.Name = "tbox_break";
            this.tbox_break.Size = new System.Drawing.Size(48, 22);
            this.tbox_break.TabIndex = 36;
            this.tbox_break.Text = "0";
            this.tbox_break.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbox_break.TextChanged += new System.EventHandler(this.tbox_break_TextChanged);
            // 
            // btn_returnsetting
            // 
            this.btn_returnsetting.Location = new System.Drawing.Point(0, 361);
            this.btn_returnsetting.Name = "btn_returnsetting";
            this.btn_returnsetting.Size = new System.Drawing.Size(169, 40);
            this.btn_returnsetting.TabIndex = 35;
            this.btn_returnsetting.Text = "RETURN SETTING";
            this.btn_returnsetting.UseVisualStyleBackColor = true;
            this.btn_returnsetting.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(545, 115);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 16);
            this.label14.TabIndex = 32;
            this.label14.Text = "DIRECTION";
            // 
            // tbox_Setspeed
            // 
            this.tbox_Setspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbox_Setspeed.Location = new System.Drawing.Point(278, 320);
            this.tbox_Setspeed.Multiline = true;
            this.tbox_Setspeed.Name = "tbox_Setspeed";
            this.tbox_Setspeed.Size = new System.Drawing.Size(105, 24);
            this.tbox_Setspeed.TabIndex = 30;
            this.tbox_Setspeed.Text = "0";
            this.tbox_Setspeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbox_Setspeed.TextChanged += new System.EventHandler(this.tbox_Setspeed_TextChanged);
            // 
            // btn_backward
            // 
            this.btn_backward.Location = new System.Drawing.Point(586, 151);
            this.btn_backward.Name = "btn_backward";
            this.btn_backward.Size = new System.Drawing.Size(102, 36);
            this.btn_backward.TabIndex = 29;
            this.btn_backward.Text = "BACKWARD";
            this.btn_backward.UseVisualStyleBackColor = true;
            this.btn_backward.Click += new System.EventHandler(this.btn_backward_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(510, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "BREAK PERCENTAGE";
            // 
            // track_break
            // 
            this.track_break.Location = new System.Drawing.Point(573, 234);
            this.track_break.Maximum = 100;
            this.track_break.Name = "track_break";
            this.track_break.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.track_break.RightToLeftLayout = true;
            this.track_break.Size = new System.Drawing.Size(45, 143);
            this.track_break.TabIndex = 18;
            this.track_break.Scroll += new System.EventHandler(this.track_break_Scroll);
            // 
            // btn_forward
            // 
            this.btn_forward.Location = new System.Drawing.Point(453, 151);
            this.btn_forward.Name = "btn_forward";
            this.btn_forward.Size = new System.Drawing.Size(102, 35);
            this.btn_forward.TabIndex = 28;
            this.btn_forward.Text = "FORWARD";
            this.btn_forward.UseVisualStyleBackColor = true;
            this.btn_forward.Click += new System.EventHandler(this.button8_Click);
            // 
            // btn_turnright
            // 
            this.btn_turnright.Location = new System.Drawing.Point(356, 16);
            this.btn_turnright.Name = "btn_turnright";
            this.btn_turnright.Size = new System.Drawing.Size(112, 32);
            this.btn_turnright.TabIndex = 27;
            this.btn_turnright.Text = "TURN RIGHT";
            this.btn_turnright.UseVisualStyleBackColor = true;
            this.btn_turnright.Click += new System.EventHandler(this.btn_turnright_Click);
            // 
            // btn_ahead
            // 
            this.btn_ahead.Location = new System.Drawing.Point(278, 15);
            this.btn_ahead.Name = "btn_ahead";
            this.btn_ahead.Size = new System.Drawing.Size(81, 33);
            this.btn_ahead.TabIndex = 26;
            this.btn_ahead.Text = "AHEAD";
            this.btn_ahead.UseVisualStyleBackColor = true;
            this.btn_ahead.Click += new System.EventHandler(this.btn_ahead_Click);
            // 
            // btn_turnleft
            // 
            this.btn_turnleft.Location = new System.Drawing.Point(168, 15);
            this.btn_turnleft.Name = "btn_turnleft";
            this.btn_turnleft.Size = new System.Drawing.Size(115, 33);
            this.btn_turnleft.TabIndex = 25;
            this.btn_turnleft.Text = "TURN LEFT";
            this.btn_turnleft.UseVisualStyleBackColor = true;
            this.btn_turnleft.Click += new System.EventHandler(this.btn_turnleft_Click);
            // 
            // aGauge1
            // 
            this.aGauge1.BaseArcColor = System.Drawing.Color.Gray;
            this.aGauge1.BaseArcRadius = 80;
            this.aGauge1.BaseArcStart = 135;
            this.aGauge1.BaseArcSweep = 270;
            this.aGauge1.BaseArcWidth = 2;
            this.aGauge1.GaugeAutoSize = false;
            this.aGauge1.Location = new System.Drawing.Point(219, 101);
            this.aGauge1.MaxValue = 30F;
            this.aGauge1.MinValue = 0F;
            this.aGauge1.Name = "aGauge1";
            this.aGauge1.NeedleColor1 = System.Windows.Forms.AGaugeNeedleColor.Red;
            this.aGauge1.NeedleColor2 = System.Drawing.Color.Red;
            this.aGauge1.NeedleRadius = 80;
            this.aGauge1.NeedleType = System.Windows.Forms.NeedleType.Advance;
            this.aGauge1.NeedleWidth = 2;
            this.aGauge1.ScaleLinesInterColor = System.Drawing.Color.Black;
            this.aGauge1.ScaleLinesInterInnerRadius = 73;
            this.aGauge1.ScaleLinesInterOuterRadius = 80;
            this.aGauge1.ScaleLinesInterWidth = 1;
            this.aGauge1.ScaleLinesMajorColor = System.Drawing.Color.Black;
            this.aGauge1.ScaleLinesMajorInnerRadius = 70;
            this.aGauge1.ScaleLinesMajorOuterRadius = 80;
            this.aGauge1.ScaleLinesMajorStepValue = 5F;
            this.aGauge1.ScaleLinesMajorWidth = 2;
            this.aGauge1.ScaleLinesMinorColor = System.Drawing.Color.DarkOrange;
            this.aGauge1.ScaleLinesMinorInnerRadius = 75;
            this.aGauge1.ScaleLinesMinorOuterRadius = 80;
            this.aGauge1.ScaleLinesMinorTicks = 9;
            this.aGauge1.ScaleLinesMinorWidth = 1;
            this.aGauge1.ScaleNumbersColor = System.Drawing.Color.Red;
            this.aGauge1.ScaleNumbersFormat = null;
            this.aGauge1.ScaleNumbersRadius = 95;
            this.aGauge1.ScaleNumbersRotation = 0;
            this.aGauge1.ScaleNumbersStartScaleLine = 0;
            this.aGauge1.ScaleNumbersStepScaleLines = 1;
            this.aGauge1.Size = new System.Drawing.Size(213, 176);
            this.aGauge1.TabIndex = 22;
            this.aGauge1.Text = "aGauge1";
            this.aGauge1.Value = 0F;
            this.aGauge1.ValueInRangeChanged += new System.EventHandler<System.Windows.Forms.ValueInRangeChangedEventArgs>(this.aGauge1_ValueInRangeChanged);
            // 
            // btn_horn
            // 
            this.btn_horn.Location = new System.Drawing.Point(13, 203);
            this.btn_horn.Name = "btn_horn";
            this.btn_horn.Size = new System.Drawing.Size(141, 36);
            this.btn_horn.TabIndex = 21;
            this.btn_horn.Text = "HORN";
            this.btn_horn.UseVisualStyleBackColor = true;
            this.btn_horn.Click += new System.EventHandler(this.btn_horn_Click);
            // 
            // btn_frontlight
            // 
            this.btn_frontlight.Location = new System.Drawing.Point(13, 147);
            this.btn_frontlight.Name = "btn_frontlight";
            this.btn_frontlight.Size = new System.Drawing.Size(141, 35);
            this.btn_frontlight.TabIndex = 20;
            this.btn_frontlight.Text = "FRONT LIGHT";
            this.btn_frontlight.UseVisualStyleBackColor = true;
            this.btn_frontlight.Click += new System.EventHandler(this.btn_frontlight_Click);
            // 
            // track_speed
            // 
            this.track_speed.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.track_speed.Location = new System.Drawing.Point(207, 283);
            this.track_speed.Maximum = 30;
            this.track_speed.Name = "track_speed";
            this.track_speed.Size = new System.Drawing.Size(211, 45);
            this.track_speed.TabIndex = 17;
            this.track_speed.TabStop = false;
            this.track_speed.Scroll += new System.EventHandler(this.track_speed_Scroll);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tbox_latitude);
            this.groupBox2.Controls.Add(this.tbox_longtitude);
            this.groupBox2.Controls.Add(this.shapeContainer1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(697, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 401);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "VEHICLE STATUS";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 16);
            this.label17.TabIndex = 4;
            this.label17.Text = "Map";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(22, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 16);
            this.label13.TabIndex = 3;
            this.label13.Text = "Latitude";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(22, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 16);
            this.label12.TabIndex = 2;
            this.label12.Text = "Longtitude";
            // 
            // tbox_latitude
            // 
            this.tbox_latitude.Location = new System.Drawing.Point(25, 140);
            this.tbox_latitude.Multiline = true;
            this.tbox_latitude.Name = "tbox_latitude";
            this.tbox_latitude.Size = new System.Drawing.Size(191, 29);
            this.tbox_latitude.TabIndex = 1;
            // 
            // tbox_longtitude
            // 
            this.tbox_longtitude.Location = new System.Drawing.Point(25, 67);
            this.tbox_longtitude.Multiline = true;
            this.tbox_longtitude.Name = "tbox_longtitude";
            this.tbox_longtitude.Size = new System.Drawing.Size(192, 30);
            this.tbox_longtitude.TabIndex = 0;
            this.tbox_longtitude.TextChanged += new System.EventHandler(this.tbox_longtitude_TextChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 18);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(219, 380);
            this.shapeContainer1.TabIndex = 5;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(22, 199);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(189, 161);
            // 
            // timer2
            // 
            this.timer2.Interval = 150;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // AEV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 450);
            this.Controls.Add(this.panel1);
            this.Name = "AEV";
            this.Text = "AEV";
            this.Load += new System.EventHandler(this.AEV_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.Setting.ResumeLayout(false);
            this.Setting.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.Communication.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.track_break)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.track_speed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Setting;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tBoxOutData;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lbLengthSendData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSendData;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox tBoxDataReceive;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbLengthReceiveData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chBoxAddToOldData;
        private System.Windows.Forms.CheckBox chBoxAlwayUpDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cBoxPatityBits;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lbStaticComPort;
        private System.Windows.Forms.ComboBox cBoxStopBits;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cBoxDataBits;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ComboBox cBoxBaudRate;
        private System.Windows.Forms.ComboBox cBoxComPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Communication;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button btn_returnsetting;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbox_Setspeed;
        private System.Windows.Forms.Button btn_backward;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TrackBar track_break;
        private System.Windows.Forms.Button btn_forward;
        private System.Windows.Forms.Button btn_turnright;
        private System.Windows.Forms.Button btn_ahead;
        private System.Windows.Forms.Button btn_turnleft;
        private System.Windows.Forms.AGauge aGauge1;
        private System.Windows.Forms.Button btn_horn;
        private System.Windows.Forms.Button btn_frontlight;
        private System.Windows.Forms.TrackBar track_speed;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbox_latitude;
        private System.Windows.Forms.TextBox tbox_longtitude;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbox_break;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_communicate;
        private System.Windows.Forms.ProgressBar break_pbar;
        private System.Windows.Forms.AGauge aGauge2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Timer timer2;
    }
}