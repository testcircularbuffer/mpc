﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace Serial_Port_BTL
{

    public partial class AEV : Form
    {
        //Define global variable

        string dataOut, dataIn;
        string direct_light = "AHEAD";
        string mode = "FORWARD", set_mode = "FORWARD", pre_mode = "FORWARD";
        Int16 break_p = 0, speed = 0, set_speed = 0, angle = 0;
        bool horn = false, front_light = false;
        Int16 direct_rx, turn_rx, horn_rx, fr_light_rx;
        int intlen = 0;

        public AEV()
        {
            InitializeComponent();
        }

        private void AEV_Load(object sender, EventArgs e)
        {
            groupBox11.Visible = false;
            groupBox4.Visible = false;
            chBoxAlwayUpDate.Checked = false;
            chBoxAddToOldData.Checked = true;
        }

        private void btnOpen_Click_1(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cBoxComPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(cBoxBaudRate.Text);
                serialPort1.DataBits = Convert.ToInt32(cBoxDataBits.Text);
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cBoxStopBits.Text);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), cBoxPatityBits.Text);
                serialPort1.Open();
                progressBar1.Value = 100;
                lbStaticComPort.Text = "ON";
                groupBox11.Visible = true;
                groupBox4.Visible = true;
                
                //Khởi tạo trạng thái ban đầu của Guide
                ori_status();
                timer2.Enabled = true;                
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //_______________Timer1  100ms tick____________________//
        private void timer1_Tick(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            if (intlen != ports.Length)
            {
                intlen = ports.Length;
                cBoxComPort.Items.Clear();
                for (int j = 0; j < intlen; j++)
                {
                    cBoxComPort.Items.Add(ports[j]);
                }
            }
            mode_status(set_mode, pre_mode);        //Set FORWARD/BACKWARD mode
            set_direct_light(direct_light);         //Turn signal       
        }
        //_____________END Timer1______________________//

        private void ori_status()
        {
            track_break.Value = 0;
            tbox_break.Text = track_break.Value.ToString();

            track_speed.Value = 0;
            tbox_Setspeed.Text = track_speed.Value.ToString();

            direct_light = "AHEAD";
            btn_ahead.BackColor = Color.Lime;
            btn_turnleft.BackColor = Color.LightSteelBlue;
            btn_turnright.BackColor = Color.LightSteelBlue;
            tbox_longtitude.Text = "0".ToString();
            aGauge2.Value = angle;

            set_mode = "FORWARD";

            horn = false;
            btn_horn.BackColor = Color.LightSteelBlue;

            front_light = false;
            btn_frontlight.BackColor = Color.LightSteelBlue;
        }
        //_________Timer2 150ms tick____________//
        private void timer2_Tick_1(object sender, EventArgs e)
        {
            set_SendFrame(set_speed, break_p, direct_light, mode, horn, front_light);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                //Reset cac gia tri toc do va phanh ve 0//
                ori_status();
                timer2.Enabled = false;
                //Send frame
                set_SendFrame(set_speed, break_p, direct_light, mode, horn, front_light);
                
                serialPort1.Close();
                delay_100ms(1);
                              
                progressBar1.Value = 0;
                lbStaticComPort.Text = "OFF";
                groupBox11.Visible = false;
                groupBox4.Visible = false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ori_status();
            timer2.Enabled = false;
            set_SendFrame(set_speed, break_p, direct_light, mode, horn, front_light);
            serialPort1.Close();
            delay_100ms(1);
            
            this.Close();
        }


        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            dataIn = (string)serialPort1.ReadLine();
            this.Invoke(new EventHandler(SetGuideStatus));
            
        }


        //Xử lý frame nhận từ VĐK để set Guide
        Int16[] Rx_Data_Int16 = new Int16[10];
        Int16 frame3_Rx = 0;
        private void SetGuideStatus(object sender, EventArgs e)
        {
            if (dataIn.Length == 9)
            {
                for (int i = 0; i < 9; i++)
                {
                    Rx_Data_Int16[i] = Convert.ToInt16(Convert.ToChar(dataIn.Substring(i, 1)));
                }
                speed = Rx_Data_Int16[0];
                aGauge1.Value = speed;         
                break_pbar.Value = Rx_Data_Int16[1];
                frame3_Rx = Rx_Data_Int16[3];
                direct_rx = (short)((frame3_Rx) & 0x01);
                turn_rx = (short)(((frame3_Rx) >> 2) & 0x03);
                horn_rx = (short)(((frame3_Rx) >> 3) & 0x01);
                fr_light_rx = (short)(((frame3_Rx) >> 4) & 0x01);
                ///_______Trả về các giá trị ngôn ngữ____________________///
                //fr3_RX(); ;// không gọi hàm này nếu quan sát trạng thái trên guide gửi xuống

            }
        }

        private void showdata(object sender, EventArgs e)
        {
            if (chBoxAlwayUpDate.Checked)
            {
                tBoxDataReceive.Text = dataIn;
                chBoxAddToOldData.Checked = false;
            }
            else if (chBoxAddToOldData.Checked)
            {
                chBoxAlwayUpDate.Checked = false;
                tBoxDataReceive.Text += dataIn;
            }
        }
        //_____________________Đọc frame nhận về_________________
        

        private void tBoxDataReceive_TextChanged(object sender, EventArgs e)
        {

        }

        private void fr3_RX()//Hàm này để quan sát trạng thái từ VĐK gửi lên
        {
            if (direct_rx==0)
                set_mode = "FORWARD";
            else
                set_mode = "BACKWARD";
            switch (turn_rx)
            {
                case 1:
                    {
                        direct_light = "right";
                        tbox_longtitude.Text = "45".ToString();//tbox tạm thời đang lưu angle
                        break;
                    }
                case 2:
                    {
                        direct_light = "left";
                        tbox_longtitude.Text = "-45".ToString();
                        break;
                    }
                default:
                    {
                        direct_light = "AHEAD";
                        tbox_longtitude.Text = "0".ToString();
                        break;
                    }
            }
            if (fr_light_rx == 1)
            {
                btn_frontlight.BackColor = Color.Lime;
                front_light = true;
            }
            else
            {
                btn_frontlight.BackColor = Color.LightSteelBlue;
                front_light = false;
            }
            if (horn_rx==1)
            {
                btn_horn.BackColor = Color.Lime;
                horn = true;
            }
            else
            {
                btn_horn.BackColor = Color.LightSteelBlue;
                horn = false;
            }

        }

        private void tBoxOutData_TextChanged_1(object sender, EventArgs e)
        {
            int dataOUTLength = tBoxOutData.TextLength;
            lbLengthSendData.Text = string.Format("{0:00}", dataOUTLength);
        }

        //_________SendData==OutData_____________//
        private void btnSendData_Click(object sender, EventArgs e)
        {
            dataOut = tBoxOutData.Text + "\r";
            if (serialPort1.IsOpen)
                serialPort1.WriteLine(dataOut);
        }

        //btn_clearReceive CLEAR RECEIVE DATA
        private void button1_Click(object sender, EventArgs e)
        {
            if (tBoxDataReceive.Text != "")
            {
                tBoxDataReceive.Text = "";
            }
        }
        
        //btn_clearSend CLEAR SEND DATA BOX
        private void button2_Click(object sender, EventArgs e)
        {
            if (tBoxOutData.Text != "")
            {
                tBoxOutData.Text = "";
            }
        }
      
        //btn_forward SET FORWARD MODE
        private void button8_Click(object sender, EventArgs e) 
        {
            pre_mode = mode; ;
            set_mode = "FORWARD";
        }
        //btn_backward SET BACKWARD MODE
        private void btn_backward_Click(object sender, EventArgs e)
        {
            pre_mode = mode;
            set_mode = "BACKWARD";
        }

        //Procedure Set mode FORWARD/BACKWARD________ by mode_status_________ function
        private void mode_status(string set, string pre_set)
        {
            if (pre_set != set)
            {
                track_speed.Value = 0;
                tbox_Setspeed.Text = track_speed.Value.ToString();
                if (aGauge1.Value==0)
                {
                    mode = set;
                    pre_mode = mode;
                }
            }
            if (mode == "FORWARD")
            {
                btn_forward.BackColor = Color.Lime;
                btn_backward.BackColor = Color.LightSteelBlue;
            }
            else
            {
                btn_forward.BackColor = Color.LightSteelBlue;
                btn_backward.BackColor = Color.Lime;
            }

        }

        //Procedure Set direct Light _____TURN SIGNAL__________
        private void set_direct_light(string direct)
        {
            switch (direct)
            {
                case "right":
                    {
                        //Send angle
                        btn_ahead.BackColor = Color.LightSteelBlue;
                        btn_turnleft.BackColor = Color.LightSteelBlue;
                        if (btn_turnright.BackColor == Color.Lime)
                        {
                            btn_turnright.BackColor = Color.LightSteelBlue;
                        }
                        else
                        {
                            btn_turnright.BackColor = Color.Lime;
                        }
                        break;
                    }
                case "left":
                    {
                        //Send angle
                        btn_ahead.BackColor = Color.LightSteelBlue;
                        btn_turnright.BackColor = Color.LightSteelBlue;
                        if (btn_turnleft.BackColor == Color.Lime)
                        {
                            btn_turnleft.BackColor = Color.LightSteelBlue;
                        }
                        else
                        {
                            btn_turnleft.BackColor = Color.Lime;
                        }
                        break;
                    }
                default:
                    {
                        btn_ahead.BackColor = Color.Lime;
                        btn_turnleft.BackColor = Color.LightSteelBlue;
                        btn_turnright.BackColor = Color.LightSteelBlue;
                        break;
                    }
            }
        }

        //SET BRAKE PERCENTAGE BY SCROLL TRACK BAR
        private void track_break_Scroll(object sender, EventArgs e)
        {
            tbox_break.Text = track_break.Value.ToString();
            break_p = Int16.Parse(tbox_break.Text);
        }
        private void tbox_break_TextChanged(object sender, EventArgs e)
        {
            break_p = Int16.Parse(tbox_break.Text);
        }
        //SET SPEED BY SCROLL TRACK BAR
        private void track_speed_Scroll(object sender, EventArgs e)
        {
            tbox_Setspeed.Text = track_speed.Value.ToString();
            set_speed = Int16.Parse(track_speed.Value.ToString());           
        }

        private void tbox_Setspeed_TextChanged(object sender, EventArgs e)
        {
            set_speed = Int16.Parse(track_speed.Value.ToString());
        }

       
        //TURN ON/OFF FRONT LIGHT
        private void btn_frontlight_Click(object sender, EventArgs e)
        {            
            if (btn_frontlight.BackColor == Color.LightSteelBlue)
            {
                btn_frontlight.BackColor = Color.Lime;
                front_light = true;
            }
            else
            {
                btn_frontlight.BackColor = Color.LightSteelBlue;
                front_light = false;
            }
        }       

        //Check mode Show  Receive Data  ______________ AlwayUpDate
        private void chBoxAlwayUpDate_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chBoxAlwayUpDate.Checked)
            {
                chBoxAddToOldData.Checked = false;
            }
            else
            {
                chBoxAddToOldData.Checked = true;
            }
        }
                
        
        //_____________Set Frame to SEND_______________
        private void set_SendFrame(Int16 speed_d, Int16 brake_d, string turn_d, string mode_d, bool horn_d,bool front_light_d)
        {
            byte[] data_byte = new byte[10];
            data_byte[0] = (byte)speed_d;
            data_byte[1] = (byte)brake_d;
            int frame_3 = 0x1F;
            if (turn_d == "right")
            {
                data_byte[2] = 90;
                frame_3 = frame_3 & 0xFB;
            }
            else if (turn_d == "left")
            {
                data_byte[2] = 0;
                frame_3 = frame_3 & 0xFD;
            }
            else
            {
                data_byte[2] = 45;
                frame_3 = frame_3 & 0xF9;
            }

            if (horn_d == true) frame_3 = frame_3 & 0xFF;
            else frame_3 = frame_3 & 0xF7;

            if (mode_d == "BACKWARD") frame_3 = frame_3 & 0xFF;
            else frame_3 = frame_3 & 0xFE;

            if (front_light_d == true) frame_3 = frame_3 & 0xFF;
            else frame_3 = frame_3 & 0xEF;

            data_byte[3] =(byte)frame_3;
            data_byte[4] = 49;
            data_byte[5] = 49;
            data_byte[6] = 49;
            data_byte[7] = 49;
            data_byte[8] = 0x0D;
            data_byte[9] = 0x0A;

            if (serialPort1.IsOpen)
            {
                serialPort1.Write(data_byte, 0, 10);
            }             
            aGauge2.Value = angle;
        }


        //Check mode Show  Receive Data  ______________ AddToOldData
        private void chBoxAddToOldData_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chBoxAddToOldData.Checked)
            {
                chBoxAlwayUpDate.Checked = false;
            }
            else
            {
                chBoxAlwayUpDate.Checked = true;
            }
        }

        private void tbox_longtitude_TextChanged(object sender, EventArgs e)
        {
             angle = Int16.Parse(tbox_longtitude.Text);
        }

        //Go to COMMUNICATE TAB once having set serial port
        private void btn_communicate_Click(object sender, EventArgs e)
        {
            if (lbStaticComPort.Text == "ON")        
                tabControl1.SelectedIndex = 1;
            else        
                MessageBox.Show("Error: NOT HAVING SET SERIAL PORT");
        }


        //btn_turn TURN SIGNAL
        private void btn_turnleft_Click(object sender, EventArgs e)
        {
            direct_light = "left";
            tbox_longtitude.Text = "-45".ToString();
        }

        private void btn_ahead_Click(object sender, EventArgs e)
        {
            direct_light = "ahead";
            tbox_longtitude.Text = "0".ToString();
        }

        private void btn_turnright_Click(object sender, EventArgs e)
        {
            direct_light = "right";
            tbox_longtitude.Text = "45".ToString();
        }

        //PUSH HORN
        private void btn_horn_Click(object sender, EventArgs e)
        {

            if (btn_horn.BackColor == Color.LightSteelBlue)
            {
                btn_horn.BackColor = Color.Lime;
                horn = true;
            }
            else
            { 
                btn_horn.BackColor = Color.LightSteelBlue;
                horn = false;                
            }
        }

        //btn_settingtab RETURN TO SETTING TAB
        private void button10_Click_1(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
            //Return to Setting tab
        }
        
        
        UInt16 t_counter=0;
        private void delay_100ms(UInt16 delay)
        {
            while (t_counter < delay) t_counter++;
            t_counter = 0;
        }
        // Object function without code//
        private void button5_Click(object sender, EventArgs e)
        {

        }
        private void label13_Click(object sender, EventArgs e)
        {

        }
        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }
        private void Setting_Click_1(object sender, EventArgs e)
        {

        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
        private void aGauge1_ValueInRangeChanged(object sender, ValueInRangeChangedEventArgs e)
        {

        }
        private void groupBox13_Enter(object sender, EventArgs e)
        {

        }
        private void cBoxComPort_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void aGauge2_ValueInRangeChanged(object sender, ValueInRangeChangedEventArgs e)
        {

        }
        private void groupBox13_EnabledChanged(object sender, EventArgs e)
        {

        }
        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {

        }
    }
}
