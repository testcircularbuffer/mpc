﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace Serial_Port_BTL
{
    public partial class Form1 : Form
    {
        string dataOut, dataIN; 
        public Form1()
        {
            InitializeComponent();
        }
        // thiets lập serial Port
       
        //hàm tự động cập nhập tất cả các Port 
        int intlen = 0; // intlen bằng tổng số Port trong Com
        int t;
        private void timer1_Tick(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            if(intlen != ports.Length)
            {
                intlen = ports.Length;
                cBoxComPort.Items.Clear();
                for (int j = 0; j < intlen; j++)
                {
                    cBoxComPort.Items.Add(ports[j]);                  // hàm chọn cổng COM
                }
            }
        }

        // hàm bật kết nối serial Port
        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cBoxComPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(cBoxBaudRate.Text);
                serialPort1.DataBits = Convert.ToInt32(cBoxDataBits.Text);
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cBoxStopBits.Text);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), cBoxPatityBits.Text);
                serialPort1.Open();
                progressBar1.Value = 100;
                lbStaticComPort.Text = "ON";
                groupBox11.Visible = true;
                groupBox4.Visible = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        //hàm ngắt kết nối serial Port
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                progressBar1.Value = 0;
                lbStaticComPort.Text = "OFF";
                groupBox11.Visible = false;
                groupBox4.Visible = false;
            }
        }

      
        // hàm đóng cửa sổ app
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();


        }

        // thiết lập nhận data
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {  
         dataIN =(string)serialPort1.ReadExisting();
            this.Invoke(new EventHandler (showdata));

        }

        // hàm con của biến nhận data
        private void showdata(object sender, EventArgs e)
        {
            
            if (chBoxAlwayUpDate.Checked)
            {
                //t = dataIN;
                tBoxDataReceive.Text = dataIN;
            }
            else if (chBoxAddToOldData.Checked)
            {
                tBoxDataReceive.Text += dataIN;
            }

        }

        //lựa chọn cách nhận về là liên tục hoặc cộng dồn data cũ

        private void chBoxAlwayUpDate_Checked
            (object sender, EventArgs e)
        {
            if (chBoxAlwayUpDate.Checked)
            {
                chBoxAlwayUpDate.Checked = true;
                chBoxAddToOldData.Checked = false;
            }
            else { chBoxAddToOldData.Checked = true; }
        }

        private void chBoxAddToOldData_CheckedChanged(object sender, EventArgs e)
        {
            if (chBoxAddToOldData.Checked)
            {
                chBoxAddToOldData.Checked = true;
                chBoxAlwayUpDate.Checked = false;
            }
            else { chBoxAlwayUpDate.Checked = true; }
        }

    // xóa ô dữ liệu nhận về
        private void button1_Click(object sender, EventArgs e)
        {
            if (tBoxDataReceive.Text != "")
            {
                tBoxDataReceive.Text = "";
            }
        }


    // xóa ô data gửi đi
        private void button2_Click(object sender, EventArgs e)
        {
            if (tBoxOutData.Text != "")
            {
                tBoxOutData.Text = "";
            }
        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            dataOut = tBoxOutData.Text+"\r";
            if (serialPort1.IsOpen)
                serialPort1.WriteLine(dataOut);

        }

        
        // độ dài chuỗi data gửi đi
        private void tBoxOutData_TextChanged(object sender, EventArgs e)
        {
            int dataOUTLength = tBoxOutData.TextLength;
            lbLengthSendData.Text = string.Format("{0:00}", dataOUTLength);
        }

        // độ dài chuỗi data nhận về
        private void tBoxDataReceive_TextChanged(object sender, EventArgs e)
        {
            int RxLength = tBoxDataReceive.TextLength;
            lbLengthReceiveData.Text = string.Format("{0:00}", RxLength);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox11.Visible = false;
            groupBox4.Visible = false;
            chBoxAlwayUpDate.Checked = true;
        }

        private void btnExit_MouseMove(object sender, MouseEventArgs e)
        {
            btnExit.ForeColor = Color.White;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.ForeColor = Color.Black;
        }

        // hiển thị form hướng dẫn


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void fmb_FormClosed1(object sender, FormClosedEventArgs e)
        {
        }
     }
 }

