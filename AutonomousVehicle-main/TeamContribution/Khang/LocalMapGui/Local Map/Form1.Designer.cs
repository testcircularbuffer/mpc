﻿namespace Local_Map
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pic_Drawing = new System.Windows.Forms.PictureBox();
            this.textBox_N = new System.Windows.Forms.TextBox();
            this.button_Line = new System.Windows.Forms.Button();
            this.button_Rectangle = new System.Windows.Forms.Button();
            this.button_Elipse = new System.Windows.Forms.Button();
            this.button_Eraser = new System.Windows.Forms.Button();
            this.button_Pencil = new System.Windows.Forms.Button();
            this.button_Fill = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Drawing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.pic_Drawing);
            this.panel1.Controls.Add(this.textBox_N);
            this.panel1.Controls.Add(this.button_Line);
            this.panel1.Controls.Add(this.button_Rectangle);
            this.panel1.Controls.Add(this.button_Elipse);
            this.panel1.Controls.Add(this.button_Eraser);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button_Pencil);
            this.panel1.Controls.Add(this.button_Fill);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1134, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 687);
            this.panel1.TabIndex = 0;
            // 
            // pic_Drawing
            // 
            this.pic_Drawing.Location = new System.Drawing.Point(0, 0);
            this.pic_Drawing.Name = "pic_Drawing";
            this.pic_Drawing.Size = new System.Drawing.Size(150, 120);
            this.pic_Drawing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_Drawing.TabIndex = 5;
            this.pic_Drawing.TabStop = false;
            // 
            // textBox_N
            // 
            this.textBox_N.Location = new System.Drawing.Point(25, 347);
            this.textBox_N.Multiline = true;
            this.textBox_N.Name = "textBox_N";
            this.textBox_N.Size = new System.Drawing.Size(38, 30);
            this.textBox_N.TabIndex = 4;
            // 
            // button_Line
            // 
            this.button_Line.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Line.Location = new System.Drawing.Point(25, 645);
            this.button_Line.Name = "button_Line";
            this.button_Line.Size = new System.Drawing.Size(100, 30);
            this.button_Line.TabIndex = 2;
            this.button_Line.Text = "Line";
            this.button_Line.UseVisualStyleBackColor = true;
            this.button_Line.Click += new System.EventHandler(this.button_Line_Click);
            // 
            // button_Rectangle
            // 
            this.button_Rectangle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Rectangle.Location = new System.Drawing.Point(25, 594);
            this.button_Rectangle.Name = "button_Rectangle";
            this.button_Rectangle.Size = new System.Drawing.Size(100, 30);
            this.button_Rectangle.TabIndex = 2;
            this.button_Rectangle.Text = "Rectangle";
            this.button_Rectangle.UseVisualStyleBackColor = true;
            this.button_Rectangle.Click += new System.EventHandler(this.button_Rectangle_Click);
            // 
            // button_Elipse
            // 
            this.button_Elipse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Elipse.Location = new System.Drawing.Point(25, 543);
            this.button_Elipse.Name = "button_Elipse";
            this.button_Elipse.Size = new System.Drawing.Size(100, 30);
            this.button_Elipse.TabIndex = 2;
            this.button_Elipse.Text = "Elipse";
            this.button_Elipse.UseVisualStyleBackColor = true;
            this.button_Elipse.Click += new System.EventHandler(this.button_Elpise_Click);
            // 
            // button_Eraser
            // 
            this.button_Eraser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Eraser.Location = new System.Drawing.Point(25, 492);
            this.button_Eraser.Name = "button_Eraser";
            this.button_Eraser.Size = new System.Drawing.Size(100, 30);
            this.button_Eraser.TabIndex = 2;
            this.button_Eraser.Text = "Eraser";
            this.button_Eraser.UseVisualStyleBackColor = true;
            this.button_Eraser.Click += new System.EventHandler(this.button_Eraser_Click);
            // 
            // button_Pencil
            // 
            this.button_Pencil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Pencil.Location = new System.Drawing.Point(25, 441);
            this.button_Pencil.Name = "button_Pencil";
            this.button_Pencil.Size = new System.Drawing.Size(100, 30);
            this.button_Pencil.TabIndex = 2;
            this.button_Pencil.Text = "Pencil";
            this.button_Pencil.UseVisualStyleBackColor = true;
            this.button_Pencil.Click += new System.EventHandler(this.button_Pencil_Click);
            // 
            // button_Fill
            // 
            this.button_Fill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Fill.Location = new System.Drawing.Point(87, 347);
            this.button_Fill.Name = "button_Fill";
            this.button_Fill.Size = new System.Drawing.Size(38, 30);
            this.button_Fill.TabIndex = 2;
            this.button_Fill.Text = "Fill";
            this.button_Fill.UseVisualStyleBackColor = true;
            this.button_Fill.Click += new System.EventHandler(this.button_Fill_Click);
            // 
            // pic
            // 
            this.pic.BackColor = System.Drawing.Color.White;
            this.pic.Location = new System.Drawing.Point(90, 76);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(989, 598);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic.TabIndex = 1;
            this.pic.TabStop = false;
            this.pic.Paint += new System.Windows.Forms.PaintEventHandler(this.pic_Paint);
            this.pic.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pic_MouseClick);
            this.pic.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic_MouseDown);
            this.pic.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pic_MouseMove);
            this.pic.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1284, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(25, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "Show";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Show_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 711);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Local Map";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Drawing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Button button_Line;
        private System.Windows.Forms.Button button_Rectangle;
        private System.Windows.Forms.Button button_Elipse;
        private System.Windows.Forms.Button button_Eraser;
        private System.Windows.Forms.Button button_Pencil;
        private System.Windows.Forms.Button button_Fill;
        private System.Windows.Forms.TextBox textBox_N;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.PictureBox pic_Drawing;
        private System.Windows.Forms.Button button1;
    }
}

