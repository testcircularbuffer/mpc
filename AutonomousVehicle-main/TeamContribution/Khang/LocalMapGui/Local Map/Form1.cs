﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Local_Map
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Width = 1300;
            this.Height = 750;
            bm = new Bitmap(pic.Width, pic.Height);
            g = Graphics.FromImage(bm);
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.Clear(Color.FromArgb(255, 255, 255));
            pic.Image = bm;

            bm1 = new Bitmap(pic.Width, pic.Height);
            g1 = Graphics.FromImage(bm1);
            //g1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g1.Clear(Color.FromArgb(255, 255, 255));
            pic.Image = bm1;

            p.StartCap = p.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            eraser.StartCap = eraser.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pic_Drawing.Image = bm;
        }

        
        Bitmap bm, bm1;
        Graphics g, g1;
        bool paint = false;
        Point px, py;
        Pen p = new Pen(Color.FromArgb(0,0,0), 1);
        
        Pen eraser = new Pen(Color.FromArgb(255, 255, 255), 10);
        
        int index;
        int x, y, sX, sY, cX, cY, uX, uY;

        ColorDialog cd = new ColorDialog();
        Color new_color;

        Label lblDynamic = new Label();

        private void pic_MouseDown(object sender, MouseEventArgs e)
        {
            paint = true;
            py = e.Location;

            //circle
            cX = e.X;
            cY = e.Y;
            //

        }

        private void pic_MouseMove(object sender, MouseEventArgs e)
        {
            if (paint)
            {
                if (index == 1)
                {
                    px = e.Location;
                    g.DrawLine(p, px, py);
                    g1.DrawLine(p, px, py);
                    py = px;
                }

                if (index == 2)
                {
                    px = e.Location;
                    g.DrawLine(eraser, px, py);
                    g1.DrawLine(eraser, px, py);
                    py = px;
                }
            }
            pic.Refresh();

            //circle
            x = e.X;
            y = e.Y;
            sX = e.X - cX;
            sY = e.Y - cY;//

            pic_Drawing.Image = bm;

        }

        private void pic_MouseUp(object sender, MouseEventArgs e)
        {
            paint = false;

            uX = x;
            uY = y;
            //circle
            sX = x - cX;
            sY = y - cY;
            if (index == 3)
            {
                g.DrawEllipse(p, cX, cY, sX, sY);
                g1.DrawEllipse(p, cX, cY, sX, sY);
            }//
            

            if (index == 4)
            {
                g.DrawRectangle(p, cX, cY, sX, sY);
                g1.DrawRectangle(p, cX, cY, sX, sY);
            }

            if (index == 5)
            {
                g.DrawLine(p, cX, cY, x, y);
                g1.DrawLine(p, cX, cY, x, y);
            }

            if (index == 7)
            {
                crt_lbl();
            }
            pic_Drawing.Image = bm;

        }


        private void button_Pencil_Click(object sender, EventArgs e)
        {
            index = 1;
        }

        private void button_Eraser_Click(object sender, EventArgs e)
        {
            index = 2;
        }


        private void button_Elpise_Click(object sender, EventArgs e)
        {
            index = 3;
        }

        private void button_Rectangle_Click(object sender, EventArgs e)
        {
            index = 4;
        }

        private void button_Line_Click(object sender, EventArgs e)
        {
            index = 5;
        }

        private void button_Fill_Click(object sender, EventArgs e)
        {
            index = 6;
        }

        private void button_Show_Click(object sender, EventArgs e)
        {
            index = 7;
        }

        private void pic_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics, g1 = e.Graphics;

            if (paint)
            {
                if (index == 3)
                {
                    g.DrawEllipse(p, cX, cY, sX, sY);
                    g1.DrawEllipse(p, cX, cY, sX, sY);
                }

                if (index == 4)
                {
                    g.DrawRectangle(p, cX, cY, sX, sY);
                    g1.DrawRectangle(p, cX, cY, sX, sY);
                }

                if (index == 5)
                {
                    g.DrawLine(p, cX, cY, x, y);
                    g1.DrawLine(p, cX, cY, x, y);
                }
            }

        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(Color.FromArgb(255, 255, 255));
            g1.Clear(Color.FromArgb(255, 255, 255));
            pic.Image = bm;
            pic.Image = bm1;
            index = 0;
            pic_Drawing.Image = bm;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "Image(*.jpg)|*.jpg|(*.*|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Bitmap btm = bm.Clone(new Rectangle(0, 0, bm1.Width, bm1.Height), bm.PixelFormat);
                btm.Save(sfd.FileName, ImageFormat.Jpeg);
                MessageBox.Show("Image Saved Sucessfully");
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {         
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "jpg files(*.jpg)|*.jpg| PNG files(*.png)|*.png| All files(*.*)|*.*";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {                    
                    bm1 = new Bitmap(dialog.FileName);
                    g1 = Graphics.FromImage(bm1);
                    bm = new Bitmap(bm1.Width, bm1.Height);
                    g = Graphics.FromImage(bm);
                    g.Clear(Color.FromArgb(255, 255, 255));


                    pic.Image = bm;
                    pic_Drawing.Image = bm;
                    pic.Image = bm1;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("An Error Occured", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        static Point set_point(PictureBox pb, Point pt)
        {
            float pX = 1f * pb.Width / pb.Width;
            float pY = 1f * pb.Height / pb.Height;
            return new Point((int)(pt.X*pX), (int)(pt.Y*pY));
        }// Method to set anh return color palette image point


        private void validate(Bitmap bm, Stack<Point> sp, int x, int y, Color old_color, Color new_color)
        {
            Color cx = bm.GetPixel(x, y);
            if (cx == old_color)
            {
                sp.Push(new Point(x, y));
                bm.SetPixel(x, y, new_color);
            }
        }

        public void Fill(Bitmap bm, int x, int y, Color new_clr) //Floodfill function using validate method
        {
            Color old_color = bm.GetPixel(x, y);
            Stack<Point> pixel = new Stack<Point>();
            pixel.Push(new Point(x, y));
            bm.SetPixel(x, y, new_clr);
            if (old_color == new_clr) return;

            //lay mau cua pixel cu va dien vao mau moi bang cach click va fill khi stack count > 0 neu mau cu va moi giong nhau thi khong lam gi

            while (pixel.Count > 0)
            {
                Point pt = (Point)pixel.Pop();
                if ((pt.X > 0) && (pt.Y > 0) && (pt.X < bm.Width - 1) && (pt.Y < bm.Height - 1))
                {
                    validate(bm, pixel, pt.X - 1, pt.Y, old_color, new_clr);
                    validate(bm, pixel, pt.X, pt.Y - 1, old_color, new_clr);
                    validate(bm, pixel, pt.X + 1, pt.Y, old_color, new_clr);
                    validate(bm, pixel, pt.X, pt.Y + 1, old_color, new_clr);
                }
            }
        }

        void crt_lbl()
        {
            pic.Controls.Remove(lblDynamic);
            pic.Controls.Add(lblDynamic);
            lblDynamic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblDynamic.AutoSize = true;
            lblDynamic.BackColor = System.Drawing.Color.SkyBlue;
            lblDynamic.Location = new System.Drawing.Point(cX, cY);
            lblDynamic.Text = "Name: Road\r\n" + "Long: " + cX.ToString() + "\r\nLat: " + cY.ToString() + "\r\nV: 15Km";
        }

        private void pic_MouseClick(object sender, MouseEventArgs e)
        {
            if (index == 6)
            {
                try
                {
                    Int16 N;
                    N = Int16.Parse(textBox_N.Text);
                    Point point = set_point(pic, e.Location);
                    new_color = Color.FromArgb(N, N, N);
                    Fill(bm, point.X, point.Y, new_color);
                    Fill(bm1, point.X, point.Y, new_color);
                    MessageBox.Show("Filled Sucessfully");
                    pic_Drawing.Image = bm;
                }
                catch
                {
                    MessageBox.Show("An Error Occured", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }   
    }
}
