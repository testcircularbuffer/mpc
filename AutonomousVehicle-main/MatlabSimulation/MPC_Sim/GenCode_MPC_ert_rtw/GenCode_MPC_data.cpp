//
// File: GenCode_MPC_data.cpp
//
// Code generated for Simulink model 'GenCode_MPC'.
//
// Model version                  : 1.29
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Wed May 18 00:44:46 2022
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "GenCode_MPC.h"
#include "GenCode_MPC_private.h"

// Invariant block signals (default storage)
const MPCModelClass::ConstB_GenCode_MPC_T GenCode_MPC_ConstB = {
  10.0
  ,                                    // '<S7>/Data Type Conversion10'
  10.0
  ,                                    // '<S7>/Data Type Conversion22'
  2.0
  ,                                    // '<S7>/Data Type Conversion23'
  0.0
  ,                                    // '<S7>/Data Type Conversion8'

  {
    10.0,
    10.0
  }
  ,                                    // '<S7>/Data Type Conversion9'

  {
    10.0,
    10.0
  }
  ,                                    // '<S7>/Math Function'
  10.0
  ,                                    // '<S7>/Math Function1'
  0.0
  ,                                    // '<S7>/Math Function2'
  14.285714285714286
  ,                                    // '<S38>/Divide'
  28.571428571428573
  ,                                    // '<S38>/Gain'
  10.666666666666666
  ,                                    // '<S38>/Divide1'
  21.333333333333332
  ,                                    // '<S38>/Gain1'
  16000.000000000004
  ,                                    // '<S38>/Divide6'
  6400.0000000000009
  ,                                    // '<S38>/Divide7'
  35000.0
  ,                                    // '<S38>/Sum'
  -70000.0
  ,                                    // '<S38>/Gain2'
  20000.0
  ,                                    // '<S38>/Product'
  8000.0
  ,                                    // '<S38>/Product1'
  -12000.0
  ,                                    // '<S38>/Sum1'
  24000.0
  ,                                    // '<S38>/Gain3'
  22400.000000000004
  ,                                    // '<S38>/Sum3'
  -44800.000000000007
  ,                                    // '<S38>/Gain4'
  0U
  // '<S2>/Switch'
};

// Constant parameters (default storage)
const MPCModelClass::ConstP_GenCode_MPC_T GenCode_MPC_ConstP = {
  // Expression: lastPcov
  //  Referenced by: '<S7>/LastPcov'

  { 1.6044444444444446, 0.46877681159420281, 0.0018590413571097318,
    0.0011611108630952788, 3.9724297147124864E-17, 0.46877681159420281,
    0.13696435538752355, 0.00054316338781639113, 0.00033924630434782756,
    1.3256187157533929E-16, 0.0018590413571097318, 0.00054316338781639113,
    0.0013260917447855741, 0.0011140406040081527, -0.011053095177209617,
    0.0011611108630952788, 0.00033924630434782756, 0.0011140406040081527,
    0.0021519660748227291, -0.021247488176721466, 3.9724297147124864E-17,
    1.3256187157533929E-16, -0.011053095177209617, -0.021247488176721466,
    0.43225704935933462 }
};

//
// File trailer for generated code.
//
// [EOF]
//
