/*
 * MPC.h
 *
 * Code generation for model "MPC".
 *
 * Model version              : 1.15
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Mon Nov  8 23:38:03 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_MPC_h_
#define RTW_HEADER_MPC_h_
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef MPC_COMMON_INCLUDES_
#define MPC_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* MPC_COMMON_INCLUDES_ */

#include "MPC_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rt_assert.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetContStateDisabled
#define rtmGetContStateDisabled(rtm)   ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
#define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
#define rtmGetContStates(rtm)          ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
#define rtmSetContStates(rtm, val)     ((rtm)->contStates = (val))
#endif

#ifndef rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm) ((rtm)->CTOutputIncnstWithState)
#endif

#ifndef rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm, val) ((rtm)->CTOutputIncnstWithState = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
#define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
#define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetIntgData
#define rtmGetIntgData(rtm)            ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
#define rtmSetIntgData(rtm, val)       ((rtm)->intgData = (val))
#endif

#ifndef rtmGetOdeF
#define rtmGetOdeF(rtm)                ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
#define rtmSetOdeF(rtm, val)           ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
#define rtmGetOdeY(rtm)                ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
#define rtmSetOdeY(rtm, val)           ((rtm)->odeY = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
#define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
#define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
#define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
#define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetZCCacheNeedsReset
#define rtmGetZCCacheNeedsReset(rtm)   ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
#define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
#define rtmGetdX(rtm)                  ((rtm)->derivs)
#endif

#ifndef rtmSetdX
#define rtmSetdX(rtm, val)             ((rtm)->derivs = (val))
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                ((rtm)->Timing.t)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T unusedU0_data[32767];
  int16_T iC_data[32767];
  real_T Mv_aux_data[1760];
  real_T Mu_data[800];
  real_T e2;                           /* '<S5>/Integrator2' */
  real_T Product;                      /* '<S1>/Product' */
  real_T e2dot;                        /* '<S5>/Subtract' */
  real_T A[16];                        /* '<S4>/Adaptive Model' */
  real_T B[8];                         /* '<S4>/Adaptive Model' */
  real_T C[8];                         /* '<S4>/Adaptive Model' */
  real_T D[4];                         /* '<S4>/Adaptive Model' */
  real_T xk1[5];                       /* '<S35>/VariableHorizonOptimizer' */
  real_T u;                            /* '<S35>/VariableHorizonOptimizer' */
  real_T Pk1[25];                      /* '<S35>/VariableHorizonOptimizer' */
  boolean_T min_relop;                 /* '<S3>/min_relop' */
  boolean_T iAout[4];                  /* '<S35>/VariableHorizonOptimizer' */
} B_MPC_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T last_mv_DSTATE;               /* '<S7>/last_mv' */
  real_T last_x_PreviousInput[5];      /* '<S7>/last_x' */
  real_T LastPcov_PreviousInput[25];   /* '<S7>/LastPcov' */
  boolean_T Memory_PreviousInput[4];   /* '<S7>/Memory' */
} DW_MPC_T;

/* Continuous states (default storage) */
typedef struct {
  real_T Integrator2_CSTATE;           /* '<S5>/Integrator2' */
} X_MPC_T;

/* State derivatives (default storage) */
typedef struct {
  real_T Integrator2_CSTATE;           /* '<S5>/Integrator2' */
} XDot_MPC_T;

/* State disabled  */
typedef struct {
  boolean_T Integrator2_CSTATE;        /* '<S5>/Integrator2' */
} XDis_MPC_T;

/* Invariant block signals (default storage) */
typedef struct {
  const real_T DataTypeConversion22;   /* '<S7>/Data Type Conversion22' */
  const real_T DataTypeConversion23;   /* '<S7>/Data Type Conversion23' */
  const real_T DataTypeConversion8;    /* '<S7>/Data Type Conversion8' */
  const real_T MathFunction[2];        /* '<S7>/Math Function' */
  const real_T MathFunction1;          /* '<S7>/Math Function1' */
  const real_T MathFunction2;          /* '<S7>/Math Function2' */
  const real_T Divide;                 /* '<S38>/Divide' */
  const real_T b1;                     /* '<S38>/Gain' */
  const real_T Divide1;                /* '<S38>/Divide1' */
  const real_T b2;                     /* '<S38>/Gain1' */
  const real_T Divide6;                /* '<S38>/Divide6' */
  const real_T Divide7;                /* '<S38>/Divide7' */
  const real_T Sum;                    /* '<S38>/Sum' */
  const real_T Gain2;                  /* '<S38>/Gain2' */
  const real_T Product;                /* '<S38>/Product' */
  const real_T Product1;               /* '<S38>/Product1' */
  const real_T Sum1;                   /* '<S38>/Sum1' */
  const real_T Gain3;                  /* '<S38>/Gain3' */
  const real_T Sum3;                   /* '<S38>/Sum3' */
  const real_T Gain4;                  /* '<S38>/Gain4' */
  const uint8_T Switch;                /* '<S2>/Switch' */
} ConstB_MPC_T;

#ifndef ODE4_INTG
#define ODE4_INTG

/* ODE4 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[4];                        /* derivatives */
} ODE4_IntgData;

#endif

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: lastPcov
   * Referenced by: '<S7>/LastPcov'
   */
  real_T LastPcov_InitialCondition[25];
} ConstP_MPC_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T Longitudinalvelocity;         /* '<Root>/Longitudinal velocity' */
  real_T Curvature;                    /* '<Root>/Curvature' */
  real_T Lateraldeviation;             /* '<Root>/Lateral deviation' */
  real_T Relativeyawangle;             /* '<Root>/Relative yaw angle' */
} ExtU_MPC_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Steeringangle;                /* '<Root>/Steering angle' */
} ExtY_MPC_T;

/* Real-time Model Data Structure */
struct tag_RTM_MPC_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;
  RTWSolverInfo solverInfo;
  X_MPC_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[1];
  real_T odeF[4][1];
  ODE4_IntgData intgData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T tFinal;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block signals (default storage) */
extern B_MPC_T MPC_B;

/* Continuous states (default storage) */
extern X_MPC_T MPC_X;

/* Block states (default storage) */
extern DW_MPC_T MPC_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_MPC_T MPC_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_MPC_T MPC_Y;
extern const ConstB_MPC_T MPC_ConstB;  /* constant block i/o */

/* Constant parameters (default storage) */
extern const ConstP_MPC_T MPC_ConstP;

/* Model entry point functions */
extern void MPC_initialize(void);
extern void MPC_step(void);
extern void MPC_terminate(void);

/* Real-time Model object */
extern RT_MODEL_MPC_T *const MPC_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S8>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S9>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S10>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S11>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S12>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S13>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S14>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S15>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S16>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S17>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S18>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S19>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S20>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S21>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S22>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S23>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S24>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S25>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S26>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S27>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S28>/Vector Dimension Check' : Unused code path elimination
 * Block '<S29>/Vector Dimension Check' : Unused code path elimination
 * Block '<S30>/Vector Dimension Check' : Unused code path elimination
 * Block '<S31>/Vector Dimension Check' : Unused code path elimination
 * Block '<S32>/Vector Dimension Check' : Unused code path elimination
 * Block '<S33>/Vector Dimension Check' : Unused code path elimination
 * Block '<S34>/Vector Dimension Check' : Unused code path elimination
 * Block '<S7>/useq_scale' : Unused code path elimination
 * Block '<S7>/useq_scale1' : Unused code path elimination
 * Block '<S7>/ym_zero' : Unused code path elimination
 * Block '<S1>/External control signal constant' : Unused code path elimination
 * Block '<S7>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion10' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion11' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion12' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion13' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion14' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion15' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion16' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion17' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion18' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion19' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion20' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion21' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion3' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion4' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion5' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion6' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion7' : Eliminate redundant data type conversion
 * Block '<S7>/Data Type Conversion9' : Eliminate redundant data type conversion
 * Block '<S7>/E Conversion' : Eliminate redundant data type conversion
 * Block '<S7>/F Conversion' : Eliminate redundant data type conversion
 * Block '<S7>/G Conversion' : Eliminate redundant data type conversion
 * Block '<S7>/Reshape' : Reshape block reduction
 * Block '<S7>/Reshape1' : Reshape block reduction
 * Block '<S7>/Reshape2' : Reshape block reduction
 * Block '<S7>/Reshape3' : Reshape block reduction
 * Block '<S7>/Reshape4' : Reshape block reduction
 * Block '<S7>/Reshape5' : Reshape block reduction
 * Block '<S7>/S Conversion' : Eliminate redundant data type conversion
 * Block '<S7>/mo or x Conversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'MPC'
 * '<S1>'   : 'MPC/LKA subsystem'
 * '<S2>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA'
 * '<S3>'   : 'MPC/LKA subsystem/Longitudinal velocity must be positive'
 * '<S4>'   : 'MPC/LKA subsystem/Model for Adaptive MPC'
 * '<S5>'   : 'MPC/LKA subsystem/Sensor Dynamics'
 * '<S6>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller'
 * '<S7>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC'
 * '<S8>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check'
 * '<S9>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check A'
 * '<S10>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check B'
 * '<S11>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check C'
 * '<S12>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check D'
 * '<S13>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check DX'
 * '<S14>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check U'
 * '<S15>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check X'
 * '<S16>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check Y'
 * '<S17>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check1'
 * '<S18>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check2'
 * '<S19>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check'
 * '<S20>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check1'
 * '<S21>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check2'
 * '<S22>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check3'
 * '<S23>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check4'
 * '<S24>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check5'
 * '<S25>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check6'
 * '<S26>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check7'
 * '<S27>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check8'
 * '<S28>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check'
 * '<S29>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check1'
 * '<S30>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check2'
 * '<S31>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check'
 * '<S32>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check1'
 * '<S33>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check11'
 * '<S34>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check6'
 * '<S35>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer'
 * '<S36>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer/VariableHorizonOptimizer'
 * '<S37>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Adaptive Model'
 * '<S38>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Vehicle Dynamics from Parameters'
 */
#endif                                 /* RTW_HEADER_MPC_h_ */
