/*
 * MPC_data.c
 *
 * Code generation for model "MPC".
 *
 * Model version              : 1.12
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Sun Sep  5 12:06:20 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "MPC.h"
#include "MPC_private.h"

/* Invariant block signals (default storage) */
const ConstB_MPC_T MPC_ConstB = {
  3.0,                                 /* '<S6>/Data Type Conversion22' */
  3.0,                                 /* '<S6>/Data Type Conversion23' */
  0.0,                                 /* '<S6>/Data Type Conversion8' */

  { 0.0, 0.0 },                        /* '<S6>/Math Function' */
  0.0,                                 /* '<S6>/Math Function1' */
  0.0,                                 /* '<S6>/Math Function2' */
  12.063492063492063,                  /* '<S37>/Divide' */
  24.126984126984127,                  /* '<S37>/Gain' */
  7.9304347826086961,                  /* '<S37>/Divide1' */
  15.860869565217392,                  /* '<S37>/Gain1' */
  84480.000000000015,                  /* '<S37>/Divide6' */
  27360.0,                             /* '<S37>/Divide7' */
  52000.0,                             /* '<S37>/Sum' */
  -104000.0,                           /* '<S37>/Gain2' */
  52800.0,                             /* '<S37>/Product' */
  22800.0,                             /* '<S37>/Product1' */
  -30000.0,                            /* '<S37>/Sum1' */
  60000.0,                             /* '<S37>/Gain3' */
  111840.00000000001,                  /* '<S37>/Sum3' */
  -223680.00000000003,                 /* '<S37>/Gain4' */
  0U                                   /* '<S2>/Switch' */
};

/* Constant parameters (default storage) */
const ConstP_MPC_T MPC_ConstP = {
  /* Expression: lastPcov
   * Referenced by: '<S6>/LastPcov'
   */
  { 0.54363269209673259, 0.38838008933431462, -0.071531568145133326,
    -0.028280807001852516, 0.026986068896119327, 0.38838008933431462,
    1.31080570982802, 0.32742793223742245, 0.17800810203494544,
    -0.0033958017073179129, -0.071531568145133326, 0.32742793223742245,
    0.38448836559015964, 0.1028820196854511, -0.12252027899228292,
    -0.028280807001852516, 0.17800810203494544, 0.1028820196854511,
    0.043957908611054183, -0.019942338017053168, 0.026986068896119327,
    -0.0033958017073179129, -0.12252027899228292, -0.019942338017053168,
    0.27950228221271567 }
};
