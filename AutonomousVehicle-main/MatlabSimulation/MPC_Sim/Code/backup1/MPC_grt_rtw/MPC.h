/*
 * MPC.h
 *
 * Code generation for model "MPC".
 *
 * Model version              : 1.12
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Sun Sep  5 12:06:20 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_MPC_h_
#define RTW_HEADER_MPC_h_
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef MPC_COMMON_INCLUDES_
#define MPC_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* MPC_COMMON_INCLUDES_ */

#include "MPC_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rt_assert.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                (&(rtm)->Timing.taskTime0)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T unusedU0_data[32767];
  int16_T iC_data[32767];
  real_T Mv_aux_data[1760];
  real_T Mu_data[800];
} B_MPC_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T last_mv_DSTATE;               /* '<S6>/last_mv' */
  real_T last_x_PreviousInput[5];      /* '<S6>/last_x' */
  real_T LastPcov_PreviousInput[25];   /* '<S6>/LastPcov' */
  boolean_T Memory_PreviousInput[4];   /* '<S6>/Memory' */
} DW_MPC_T;

/* Invariant block signals (default storage) */
typedef struct {
  const real_T DataTypeConversion22;   /* '<S6>/Data Type Conversion22' */
  const real_T DataTypeConversion23;   /* '<S6>/Data Type Conversion23' */
  const real_T DataTypeConversion8;    /* '<S6>/Data Type Conversion8' */
  const real_T MathFunction[2];        /* '<S6>/Math Function' */
  const real_T MathFunction1;          /* '<S6>/Math Function1' */
  const real_T MathFunction2;          /* '<S6>/Math Function2' */
  const real_T Divide;                 /* '<S37>/Divide' */
  const real_T b1;                     /* '<S37>/Gain' */
  const real_T Divide1;                /* '<S37>/Divide1' */
  const real_T b2;                     /* '<S37>/Gain1' */
  const real_T Divide6;                /* '<S37>/Divide6' */
  const real_T Divide7;                /* '<S37>/Divide7' */
  const real_T Sum;                    /* '<S37>/Sum' */
  const real_T Gain2;                  /* '<S37>/Gain2' */
  const real_T Product;                /* '<S37>/Product' */
  const real_T Product1;               /* '<S37>/Product1' */
  const real_T Sum1;                   /* '<S37>/Sum1' */
  const real_T Gain3;                  /* '<S37>/Gain3' */
  const real_T Sum3;                   /* '<S37>/Sum3' */
  const real_T Gain4;                  /* '<S37>/Gain4' */
  const uint8_T Switch;                /* '<S2>/Switch' */
} ConstB_MPC_T;

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: lastPcov
   * Referenced by: '<S6>/LastPcov'
   */
  real_T LastPcov_InitialCondition[25];
} ConstP_MPC_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T Longitudinalvelocity;         /* '<Root>/Longitudinal velocity' */
  real_T Curvature;                    /* '<Root>/Curvature' */
  real_T Lateraldeviation;             /* '<Root>/Lateral deviation' */
  real_T Relativeyawangle;             /* '<Root>/Relative yaw angle' */
} ExtU_MPC_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Steeringangle;                /* '<Root>/Steering angle' */
} ExtY_MPC_T;

/* Real-time Model Data Structure */
struct tag_RTM_MPC_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block signals (default storage) */
extern B_MPC_T MPC_B;

/* Block states (default storage) */
extern DW_MPC_T MPC_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_MPC_T MPC_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_MPC_T MPC_Y;
extern const ConstB_MPC_T MPC_ConstB;  /* constant block i/o */

/* Constant parameters (default storage) */
extern const ConstP_MPC_T MPC_ConstP;

/* Model entry point functions */
extern void MPC_initialize(void);
extern void MPC_step(void);
extern void MPC_terminate(void);

/* Real-time Model object */
extern RT_MODEL_MPC_T *const MPC_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S7>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S8>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S9>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S10>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S11>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S12>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S13>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S14>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S15>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S16>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S17>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S18>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S19>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S20>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S21>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S22>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S23>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S24>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S25>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S26>/Matrix Dimension Check' : Unused code path elimination
 * Block '<S27>/Vector Dimension Check' : Unused code path elimination
 * Block '<S28>/Vector Dimension Check' : Unused code path elimination
 * Block '<S29>/Vector Dimension Check' : Unused code path elimination
 * Block '<S30>/Vector Dimension Check' : Unused code path elimination
 * Block '<S31>/Vector Dimension Check' : Unused code path elimination
 * Block '<S32>/Vector Dimension Check' : Unused code path elimination
 * Block '<S33>/Vector Dimension Check' : Unused code path elimination
 * Block '<S6>/useq_scale' : Unused code path elimination
 * Block '<S6>/useq_scale1' : Unused code path elimination
 * Block '<S6>/ym_zero' : Unused code path elimination
 * Block '<S1>/External control signal constant' : Unused code path elimination
 * Block '<S6>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion10' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion11' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion12' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion13' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion14' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion15' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion16' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion17' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion18' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion19' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion20' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion21' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion3' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion4' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion5' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion6' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion7' : Eliminate redundant data type conversion
 * Block '<S6>/Data Type Conversion9' : Eliminate redundant data type conversion
 * Block '<S6>/E Conversion' : Eliminate redundant data type conversion
 * Block '<S6>/F Conversion' : Eliminate redundant data type conversion
 * Block '<S6>/G Conversion' : Eliminate redundant data type conversion
 * Block '<S6>/Reshape' : Reshape block reduction
 * Block '<S6>/Reshape1' : Reshape block reduction
 * Block '<S6>/Reshape2' : Reshape block reduction
 * Block '<S6>/Reshape3' : Reshape block reduction
 * Block '<S6>/Reshape4' : Reshape block reduction
 * Block '<S6>/Reshape5' : Reshape block reduction
 * Block '<S6>/S Conversion' : Eliminate redundant data type conversion
 * Block '<S6>/mo or x Conversion' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'MPC'
 * '<S1>'   : 'MPC/LKA subsystem'
 * '<S2>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA'
 * '<S3>'   : 'MPC/LKA subsystem/Longitudinal velocity must be positive'
 * '<S4>'   : 'MPC/LKA subsystem/Model for Adaptive MPC'
 * '<S5>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller'
 * '<S6>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC'
 * '<S7>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check'
 * '<S8>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check A'
 * '<S9>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check B'
 * '<S10>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check C'
 * '<S11>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check D'
 * '<S12>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check DX'
 * '<S13>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check U'
 * '<S14>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check X'
 * '<S15>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check Y'
 * '<S16>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check1'
 * '<S17>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check2'
 * '<S18>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check'
 * '<S19>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check1'
 * '<S20>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check2'
 * '<S21>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check3'
 * '<S22>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check4'
 * '<S23>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check5'
 * '<S24>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check6'
 * '<S25>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check7'
 * '<S26>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check8'
 * '<S27>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check'
 * '<S28>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check1'
 * '<S29>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check2'
 * '<S30>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check'
 * '<S31>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check1'
 * '<S32>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check11'
 * '<S33>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check6'
 * '<S34>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer'
 * '<S35>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer/VariableHorizonOptimizer'
 * '<S36>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Adaptive Model'
 * '<S37>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Vehicle Dynamics from Parameters'
 */
#endif                                 /* RTW_HEADER_MPC_h_ */
