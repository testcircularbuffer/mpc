/*
 * MPC_data.c
 *
 * Code generation for model "MPC".
 *
 * Model version              : 1.9
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Sat Sep  4 22:58:34 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "MPC.h"
#include "MPC_private.h"

/* Block parameters (default storage) */
P_MPC_T MPC_P = {
  /* Variable: Cf
   * Referenced by: '<S37>/Vehicle front tire cornering stiffness constant'
   */
  19000.0,

  /* Variable: Cr
   * Referenced by: '<S37>/Vehicle rear tire cornering stiffness constant'
   */
  33000.0,

  /* Variable: Iz
   * Referenced by: '<S37>/Vehicle yaw inertia constant'
   */
  2875.0,

  /* Variable: TransportLag
   * Referenced by: '<S4>/Transport lag constant'
   */
  0.0,

  /* Variable: Ts
   * Referenced by: '<S4>/Sample time constant'
   */
  0.2,

  /* Variable: lf
   * Referenced by: '<S37>/Vehicle length to front constant'
   */
  1.2,

  /* Variable: lr
   * Referenced by: '<S37>/Vehicle length to rear constant'
   */
  1.6,

  /* Variable: m
   * Referenced by: '<S37>/Vehicle mass constant'
   */
  1575.0,

  /* Variable: u_max
   * Referenced by: '<S1>/Maximum steering angle constant'
   */
  0.5,

  /* Variable: u_min
   * Referenced by: '<S1>/Minimum steering angle constant'
   */
  -0.5,

  /* Mask Parameter: Longitudinalvelocitymustbeposit
   * Referenced by: '<S3>/min_val'
   */
  0.0,

  /* Expression: lastx+xoff
   * Referenced by: '<S6>/last_x'
   */
  { 0.0, 0.0, 0.0, 0.0, 0.0 },

  /* Expression: lastu+uoff
   * Referenced by: '<S6>/last_mv'
   */
  0.0,

  /* Expression: [0,0]
   * Referenced by: '<S2>/Reference'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(2,1)
   * Referenced by: '<S5>/ymin_zero'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(2,1)
   * Referenced by: '<S5>/ymax_zero'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/E_zero'
   */
  0.0,

  /* Expression: MVscale(:,ones(1,max(nCC,1)))'
   * Referenced by: '<S6>/umin_scale4'
   */
  0.52,

  /* Expression: zeros(1,2)
   * Referenced by: '<S5>/F_zero'
   */
  { 0.0, 0.0 },

  /* Expression: Yscale(:,ones(1,max(nCC,1)))'
   * Referenced by: '<S6>/ymin_scale1'
   */
  { 0.5, 0.1 },

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/G_zero'
   */
  0.0,

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/S_zero'
   */
  0.0,

  /* Expression: MDscale(:,ones(1,max(nCC,1)))'
   * Referenced by: '<S6>/ymin_scale2'
   */
  0.01,

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/ext.mv_zero'
   */
  0.0,

  /* Expression: RMVscale
   * Referenced by: '<S6>/ext.mv_scale'
   */
  1.9230769230769229,

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/mv.target_zero'
   */
  0.0,

  /* Expression: RMVscale
   * Referenced by: '<S6>/uref_scale'
   */
  1.9230769230769229,

  /* Expression: zeros(2,1)
   * Referenced by: '<S5>/y.wt_zero'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/u.wt_zero'
   */
  0.0,

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/du.wt_zero'
   */
  0.0,

  /* Expression: zeros(1,1)
   * Referenced by: '<S5>/ecr.wt_zero'
   */
  0.0,

  /* Expression: -2
   * Referenced by: '<S37>/Gain2'
   */
  -2.0,

  /* Expression: -2
   * Referenced by: '<S37>/Gain3'
   */
  -2.0,

  /* Expression: -2
   * Referenced by: '<S37>/Gain4'
   */
  -2.0,

  /* Expression: 2
   * Referenced by: '<S37>/Gain'
   */
  2.0,

  /* Expression: 2
   * Referenced by: '<S37>/Gain1'
   */
  2.0,

  /* Expression: eye(2)
   * Referenced by: '<S37>/Cm Constant'
   */
  { 1.0, 0.0, 0.0, 1.0 },

  /* Expression: zeros(2,1)
   * Referenced by: '<S4>/U Constant'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(2,1)
   * Referenced by: '<S4>/Y Constant'
   */
  { 0.0, 0.0 },

  /* Expression: zeros(NumEgoStates+2*(1+hasLag),1)
   * Referenced by: '<S4>/X Constant'
   */
  { 0.0, 0.0, 0.0, 0.0 },

  /* Expression: zeros(NumEgoStates+2*(1+hasLag),1)
   * Referenced by: '<S4>/DX Constant'
   */
  { 0.0, 0.0, 0.0, 0.0 },

  /* Expression: lastPcov
   * Referenced by: '<S6>/LastPcov'
   */
  { 0.54363269209673259, 0.38838008933431462, -0.071531568145133326,
    -0.028280807001852516, 0.026986068896119327, 0.38838008933431462,
    1.31080570982802, 0.32742793223742245, 0.17800810203494544,
    -0.0033958017073179129, -0.071531568145133326, 0.32742793223742245,
    0.38448836559015964, 0.1028820196854511, -0.12252027899228292,
    -0.028280807001852516, 0.17800810203494544, 0.1028820196854511,
    0.043957908611054183, -0.019942338017053168, 0.026986068896119327,
    -0.0033958017073179129, -0.12252027899228292, -0.019942338017053168,
    0.27950228221271567 },

  /* Expression: MVscale
   * Referenced by: '<S6>/u_scale'
   */
  0.52,

  /* Expression: iA
   * Referenced by: '<S6>/Memory'
   */
  { 0, 0, 0, 0 },

  /* Computed Parameter: UseLKAoutputconstant_Value
   * Referenced by: '<S2>/Use LKA output constant'
   */
  0U,

  /* Computed Parameter: NotuseLKAoutputconstant_Value
   * Referenced by: '<S2>/Not use LKA output constant'
   */
  1U,

  /* Computed Parameter: Enableoptimizationconstant_Valu
   * Referenced by: '<S1>/Enable optimization constant'
   */
  1U,

  /* Computed Parameter: PredictionHorizon_Value
   * Referenced by: '<S1>/Prediction Horizon'
   */
  10U,

  /* Computed Parameter: ControlHorizon_Value
   * Referenced by: '<S1>/Control Horizon'
   */
  5U
};
