/*
 * MPC.h
 *
 * Code generation for model "MPC".
 *
 * Model version              : 1.9
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Sat Sep  4 22:58:34 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_MPC_h_
#define RTW_HEADER_MPC_h_
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef MPC_COMMON_INCLUDES_
#define MPC_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* MPC_COMMON_INCLUDES_ */

#include "MPC_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rt_assert.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                (&(rtm)->Timing.taskTime0)
#endif

/* Block signals (default storage) */
typedef struct {
  int16_T iC_data[32767];
  real_T Mv_aux_data[1760];
  real_T Mu_data[800];
} B_MPC_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T last_mv_DSTATE;               /* '<S6>/last_mv' */
  real_T last_x_PreviousInput[5];      /* '<S6>/last_x' */
  real_T LastPcov_PreviousInput[25];   /* '<S6>/LastPcov' */
  boolean_T Memory_PreviousInput[4];   /* '<S6>/Memory' */
} DW_MPC_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T Longitudinalvelocity;         /* '<Root>/Longitudinal velocity' */
  real_T Curvature;                    /* '<Root>/Curvature' */
  real_T Lateraldeviation;             /* '<Root>/Lateral deviation' */
  real_T Relativeyawangle;             /* '<Root>/Relative yaw angle' */
} ExtU_MPC_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Steeringangle;                /* '<Root>/Steering angle' */
} ExtY_MPC_T;

/* Parameters (default storage) */
struct P_MPC_T_ {
  real_T Cf;                           /* Variable: Cf
                                        * Referenced by: '<S37>/Vehicle front tire cornering stiffness constant'
                                        */
  real_T Cr;                           /* Variable: Cr
                                        * Referenced by: '<S37>/Vehicle rear tire cornering stiffness constant'
                                        */
  real_T Iz;                           /* Variable: Iz
                                        * Referenced by: '<S37>/Vehicle yaw inertia constant'
                                        */
  real_T TransportLag;                 /* Variable: TransportLag
                                        * Referenced by: '<S4>/Transport lag constant'
                                        */
  real_T Ts;                           /* Variable: Ts
                                        * Referenced by: '<S4>/Sample time constant'
                                        */
  real_T lf;                           /* Variable: lf
                                        * Referenced by: '<S37>/Vehicle length to front constant'
                                        */
  real_T lr;                           /* Variable: lr
                                        * Referenced by: '<S37>/Vehicle length to rear constant'
                                        */
  real_T m;                            /* Variable: m
                                        * Referenced by: '<S37>/Vehicle mass constant'
                                        */
  real_T u_max;                        /* Variable: u_max
                                        * Referenced by: '<S1>/Maximum steering angle constant'
                                        */
  real_T u_min;                        /* Variable: u_min
                                        * Referenced by: '<S1>/Minimum steering angle constant'
                                        */
  real_T Longitudinalvelocitymustbeposit;
                              /* Mask Parameter: Longitudinalvelocitymustbeposit
                               * Referenced by: '<S3>/min_val'
                               */
  real_T last_x_InitialCondition[5];   /* Expression: lastx+xoff
                                        * Referenced by: '<S6>/last_x'
                                        */
  real_T last_mv_InitialCondition;     /* Expression: lastu+uoff
                                        * Referenced by: '<S6>/last_mv'
                                        */
  real_T Reference_Value[2];           /* Expression: [0,0]
                                        * Referenced by: '<S2>/Reference'
                                        */
  real_T ymin_zero_Value[2];           /* Expression: zeros(2,1)
                                        * Referenced by: '<S5>/ymin_zero'
                                        */
  real_T ymax_zero_Value[2];           /* Expression: zeros(2,1)
                                        * Referenced by: '<S5>/ymax_zero'
                                        */
  real_T E_zero_Value;                 /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/E_zero'
                                        */
  real_T umin_scale4_Gain;         /* Expression: MVscale(:,ones(1,max(nCC,1)))'
                                    * Referenced by: '<S6>/umin_scale4'
                                    */
  real_T F_zero_Value[2];              /* Expression: zeros(1,2)
                                        * Referenced by: '<S5>/F_zero'
                                        */
  real_T ymin_scale1_Gain[2];       /* Expression: Yscale(:,ones(1,max(nCC,1)))'
                                     * Referenced by: '<S6>/ymin_scale1'
                                     */
  real_T G_zero_Value;                 /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/G_zero'
                                        */
  real_T S_zero_Value;                 /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/S_zero'
                                        */
  real_T ymin_scale2_Gain;         /* Expression: MDscale(:,ones(1,max(nCC,1)))'
                                    * Referenced by: '<S6>/ymin_scale2'
                                    */
  real_T extmv_zero_Value;             /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/ext.mv_zero'
                                        */
  real_T extmv_scale_Gain;             /* Expression: RMVscale
                                        * Referenced by: '<S6>/ext.mv_scale'
                                        */
  real_T mvtarget_zero_Value;          /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/mv.target_zero'
                                        */
  real_T uref_scale_Gain;              /* Expression: RMVscale
                                        * Referenced by: '<S6>/uref_scale'
                                        */
  real_T ywt_zero_Value[2];            /* Expression: zeros(2,1)
                                        * Referenced by: '<S5>/y.wt_zero'
                                        */
  real_T uwt_zero_Value;               /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/u.wt_zero'
                                        */
  real_T duwt_zero_Value;              /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/du.wt_zero'
                                        */
  real_T ecrwt_zero_Value;             /* Expression: zeros(1,1)
                                        * Referenced by: '<S5>/ecr.wt_zero'
                                        */
  real_T Gain2_Gain;                   /* Expression: -2
                                        * Referenced by: '<S37>/Gain2'
                                        */
  real_T Gain3_Gain;                   /* Expression: -2
                                        * Referenced by: '<S37>/Gain3'
                                        */
  real_T Gain4_Gain;                   /* Expression: -2
                                        * Referenced by: '<S37>/Gain4'
                                        */
  real_T Gain_Gain;                    /* Expression: 2
                                        * Referenced by: '<S37>/Gain'
                                        */
  real_T Gain1_Gain;                   /* Expression: 2
                                        * Referenced by: '<S37>/Gain1'
                                        */
  real_T CmConstant_Value[4];          /* Expression: eye(2)
                                        * Referenced by: '<S37>/Cm Constant'
                                        */
  real_T UConstant_Value[2];           /* Expression: zeros(2,1)
                                        * Referenced by: '<S4>/U Constant'
                                        */
  real_T YConstant_Value[2];           /* Expression: zeros(2,1)
                                        * Referenced by: '<S4>/Y Constant'
                                        */
  real_T XConstant_Value[4];   /* Expression: zeros(NumEgoStates+2*(1+hasLag),1)
                                * Referenced by: '<S4>/X Constant'
                                */
  real_T DXConstant_Value[4];  /* Expression: zeros(NumEgoStates+2*(1+hasLag),1)
                                * Referenced by: '<S4>/DX Constant'
                                */
  real_T LastPcov_InitialCondition[25];/* Expression: lastPcov
                                        * Referenced by: '<S6>/LastPcov'
                                        */
  real_T u_scale_Gain;                 /* Expression: MVscale
                                        * Referenced by: '<S6>/u_scale'
                                        */
  boolean_T Memory_InitialCondition[4];/* Expression: iA
                                        * Referenced by: '<S6>/Memory'
                                        */
  uint8_T UseLKAoutputconstant_Value;
                               /* Computed Parameter: UseLKAoutputconstant_Value
                                * Referenced by: '<S2>/Use LKA output constant'
                                */
  uint8_T NotuseLKAoutputconstant_Value;
                            /* Computed Parameter: NotuseLKAoutputconstant_Value
                             * Referenced by: '<S2>/Not use LKA output constant'
                             */
  uint8_T Enableoptimizationconstant_Valu;
                          /* Computed Parameter: Enableoptimizationconstant_Valu
                           * Referenced by: '<S1>/Enable optimization constant'
                           */
  uint8_T PredictionHorizon_Value;/* Computed Parameter: PredictionHorizon_Value
                                   * Referenced by: '<S1>/Prediction Horizon'
                                   */
  uint8_T ControlHorizon_Value;      /* Computed Parameter: ControlHorizon_Value
                                      * Referenced by: '<S1>/Control Horizon'
                                      */
};

/* Real-time Model Data Structure */
struct tag_RTM_MPC_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_MPC_T MPC_P;

/* Block signals (default storage) */
extern B_MPC_T MPC_B;

/* Block states (default storage) */
extern DW_MPC_T MPC_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_MPC_T MPC_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_MPC_T MPC_Y;

/* Model entry point functions */
extern void MPC_initialize(void);
extern void MPC_step(void);
extern void MPC_terminate(void);

/* Real-time Model object */
extern RT_MODEL_MPC_T *const MPC_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'MPC'
 * '<S1>'   : 'MPC/LKA subsystem'
 * '<S2>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA'
 * '<S3>'   : 'MPC/LKA subsystem/Longitudinal velocity must be positive'
 * '<S4>'   : 'MPC/LKA subsystem/Model for Adaptive MPC'
 * '<S5>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller'
 * '<S6>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC'
 * '<S7>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check'
 * '<S8>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check A'
 * '<S9>'   : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check B'
 * '<S10>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check C'
 * '<S11>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check D'
 * '<S12>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check DX'
 * '<S13>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check U'
 * '<S14>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check X'
 * '<S15>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check Y'
 * '<S16>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check1'
 * '<S17>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Matrix Signal Check2'
 * '<S18>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check'
 * '<S19>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check1'
 * '<S20>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check2'
 * '<S21>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check3'
 * '<S22>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check4'
 * '<S23>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check5'
 * '<S24>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check6'
 * '<S25>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check7'
 * '<S26>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Preview Signal Check8'
 * '<S27>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check'
 * '<S28>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check1'
 * '<S29>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Scalar Signal Check2'
 * '<S30>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check'
 * '<S31>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check1'
 * '<S32>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check11'
 * '<S33>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/MPC Vector Signal Check6'
 * '<S34>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer'
 * '<S35>'  : 'MPC/LKA subsystem/Adaptive MPC for LKA/Adaptive MPC Controller/MPC/optimizer/VariableHorizonOptimizer'
 * '<S36>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Adaptive Model'
 * '<S37>'  : 'MPC/LKA subsystem/Model for Adaptive MPC/Vehicle Dynamics from Parameters'
 */
#endif                                 /* RTW_HEADER_MPC_h_ */
