function md = getCurvature(time)
% Get previewed curvature from desired X and Y positions for LKA
%
% Inputs:
%   Vx: longitudinal velocity
%   time: time vector
%
% Outputs:
%   md: previewed curvature

%%
% % Desired X position
% Xref = Vx*time;
% % Desired Y position
% z1 = (2.4/50)*(Xref-27.19)-1.2;
% z2 = (2.4/43.9)*(Xref-56.46)-1.2;
% Yref = 8.1/2*(1+tanh(z1)) - 11.4/2*(1+tanh(z2));
% % Desired curvature
% DX = gradient(Xref,0.1);
% DY = gradient(Yref,0.1);
% D2Y = gradient(DY,0.1);
% curvature = DX.*D2Y./(DX.^2+DY.^2).^(3/2);

%%
Y_Target_1 = 1/10;
Y_Target_2 = -1/7;

T_Change_1 = 25;
T_Change_2 = 35;
T_Change_3 = 40;
T_Change_4 = 50;

T_Total_1 = 2;
T_Total_2 = 2;
T_Total_3 = 1.5;
T_Total_4 = 1.5;

% Desired X position
Xref = time;

% Desired Y position
z1 = 1/T_Total_1 * (Xref - T_Change_1);
z2 = 1/T_Total_2 * (-(Xref - T_Change_2));

z3 = 1/T_Total_3 * (Xref - T_Change_3);
z4 = 1/T_Total_4 * (-(Xref - T_Change_4));

curvature = Y_Target_1*(1+ 0.5*tanh(z1)) - Y_Target_1*(1- 0.5*tanh(z2)) ...
          + Y_Target_2*(1+ 0.5*tanh(z3)) - Y_Target_2*(1- 0.5*tanh(z4));

%%
% Stored curvature (as input for LKA)
md.time = time;
md.signals.values = curvature';
