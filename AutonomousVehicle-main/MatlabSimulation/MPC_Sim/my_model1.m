%%
% To describe the lateral vehicle dynamics, this example uses a _bicycle
% model_ with the following parameters:
%
% * |m| is the total vehicle mass (kg).
% * |Iz| is the yaw moment of inertia of the vehicle (mNs^2).
% * |lf| is the longitudinal distance from the center of gravity to the
% front tires (m).
% * |lr| is the longitudinal distance from center of gravity to the rear
% tires (m).
% * |Cf| is the cornering stiffness of the front tires (N/rad).
% * |Cr| is the cornering stiffness of the rear tires (N/rad).
%
m = 1575;
Iz = 2875;
lf = 1.2;
lr = 1.6;
Cf = 19000;
Cr = 33000;

%%
% You can represent the lateral vehicle dynamics using a linear
% time-invariant (LTI) system with the following state, input, and output
% variables. The initial conditions for the state variables are assumed to
% be zero.
%
% * State variables: Lateral velocity $V_y$ and yaw angle rate $r$
% * Input variable: Front steering angle $\delta$
% * Output variables: Same as state variables

%%
% In this example, the longitudinal vehicle dynamics are separated from the
% lateral vehicle dynamics. Therefore, the longitudinal velocity is assumed
% to be constant. In practice, the longitudinal velocity can vary. The Lane
% Keeping Assist System block uses adaptive MPC to adjust the model of the
% lateral dynamics accordingly.

% Specify the longitudinal velocity in m/s.
Vx = 15;


%%
% Specify a state-space model, |G(s)|, of the lateral vehicle dynamics.
A = [-(2*Cf+2*Cr)/m/Vx, -Vx-(2*Cf*lf-2*Cr*lr)/m/Vx;...
     -(2*Cf*lf-2*Cr*lr)/Iz/Vx, -(2*Cf*lf^2+2*Cr*lr^2)/Iz/Vx];
B = [2*Cf/m, 2*Cf*lf/Iz]';
C = eye(2);
model = ss(A,B,C,0)