Vx = 10;
time = 0:0.1:50;

% Get previewed curvature from desired X and Y positions for LKA
%
% Inputs:
%   Vx: longitudinal velocity
%   time: time vector
%
% Outputs:
%   md: previewed curvature

Y_Target_1 = 5;
Y_Target_2 = -10;

T_Change_1 = 50;
T_Change_2 = 250;

T_Total_1 = 50;
T_Total_2 = 50;

% Desired X position
Xref = Vx*time;

% Desired Y position
z1 = 4/T_Total_1 * (Xref-T_Change_1);
z2 = 4/T_Total_2 * (Xref-T_Change_2);
Yref = Y_Target_1*(1+tanh(z1)) + Y_Target_2*(1+tanh(z2));

figure(1);
subplot(3,1,1);
plot(tanh(z1))
hold on;
plot(tanh(z2))
hold on;
subplot(3,1,2);
plot(Yref)
hold on;

% Desired curvature
DX = gradient(Xref,0.1);
DY = gradient(Yref,0.1);
D2Y = gradient(DY,0.1);
curvature = DX.*D2Y./(DX.^2+DY.^2).^(3/2);

subplot(3,1,3);
plot(curvature)
hold on;
