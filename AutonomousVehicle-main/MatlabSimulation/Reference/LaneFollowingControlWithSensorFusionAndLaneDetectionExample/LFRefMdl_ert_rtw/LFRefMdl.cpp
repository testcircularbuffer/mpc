//
// File: LFRefMdl.cpp
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "LFRefMdl.h"
#include "LFRefMdl_private.h"

const BusMultiObjectTracker1 LFRefMdl_rtZBusMultiObjectTracker1 = {
  0.0,                                 // NumTracks

  {
    {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    }, {
      0U,                              // TrackID
      0U,                              // BranchID
      0U,                              // SourceIndex
      0.0,                             // UpdateTime
      0U,                              // Age

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // State

      {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
      ,                                // StateCovariance
      0.0,                             // ObjectClassID
      History,                         // TrackLogic

      {
        false, false, false, false, false }
      ,                                // TrackLogicState
      false,                           // IsConfirmed
      false,                           // IsCoasted
      false,                           // IsSelfReported

      {
        {
          0.0,                         // TargetIndex
          0.0                          // SNR
        }, {
          0.0,                         // TargetIndex
          0.0                          // SNR
        } }
      // ObjectAttributes
    } }
  // Tracks
} ;                                    // BusMultiObjectTracker1 ground

// Model step function
void PathFollowingControllerRefMdlModelClass::step()
{
  real_T rtb_Sum[11];
  real_T rtb_u_scale[2];
  real_T rtb_vx;
  real_T rtb_x;
  real32_T rtb_Saturation;
  real32_T rtb_Saturation1;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.Curvature = LFRefMdl_U.LaneSensor_e.Left.Curvature;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.CurvatureDerivative =
    LFRefMdl_U.LaneSensor_e.Left.CurvatureDerivative;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.HeadingAngle = LFRefMdl_U.LaneSensor_e.Left.HeadingAngle;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.LateralOffset = LFRefMdl_U.LaneSensor_e.Left.LateralOffset;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.Curvature_p = LFRefMdl_U.LaneSensor_e.Right.Curvature;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.CurvatureDerivative_b =
    LFRefMdl_U.LaneSensor_e.Right.CurvatureDerivative;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.HeadingAngle_g = LFRefMdl_U.LaneSensor_e.Right.HeadingAngle;

  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'

  LFRefMdl_B.LateralOffset_k = LFRefMdl_U.LaneSensor_e.Right.LateralOffset;

  // Outputs for Atomic SubSystem: '<Root>/Estimate Lane Center'
  // SignalConversion generated from: '<Root>/Lane Sensor' incorporates:
  //   Inport: '<Root>/Lane Sensor'
  //   Inport: '<Root>/Longitudinal Velocity'

  EstimateLaneCenter(LFRefMdl_B.Curvature, LFRefMdl_B.CurvatureDerivative,
                     LFRefMdl_B.HeadingAngle, LFRefMdl_B.LateralOffset,
                     LFRefMdl_U.LaneSensor_e.Left.Strength,
                     LFRefMdl_B.Curvature_p, LFRefMdl_B.CurvatureDerivative_b,
                     LFRefMdl_B.HeadingAngle_g, LFRefMdl_B.LateralOffset_k,
                     LFRefMdl_U.LaneSensor_e.Right.Strength,
                     LFRefMdl_U.LongitudinalVelocity, rtb_Sum, &rtb_Saturation,
                     &rtb_Saturation1, &LFRefMdl_DW.EstimateLaneCenter_a);

  // End of Outputs for SubSystem: '<Root>/Estimate Lane Center'

  // Outputs for Atomic SubSystem: '<Root>/Tracking and Sensor Fusion'
  // Inport: '<Root>/Radar' incorporates:
  //   Inport: '<Root>/Lane Sensor'
  //   Inport: '<Root>/System Clock'
  //   Inport: '<Root>/Vision'
  //   Outport: '<Root>/MIO Track Index'
  //   SignalConversion generated from: '<Root>/Lane Sensor'

  TrackingandSensorFusion(&LFRefMdl_U.Radar, &LFRefMdl_U.Vision,
    LFRefMdl_B.Curvature, LFRefMdl_B.CurvatureDerivative,
    LFRefMdl_B.HeadingAngle, LFRefMdl_B.LateralOffset,
    LFRefMdl_U.LaneSensor_e.Left.Strength, LFRefMdl_B.Curvature_p,
    LFRefMdl_B.CurvatureDerivative_b, LFRefMdl_B.HeadingAngle_g,
    LFRefMdl_B.LateralOffset_k, LFRefMdl_U.LaneSensor_e.Right.Strength,
    LFRefMdl_U.SystemClock, &rtb_x, &rtb_vx, &LFRefMdl_Y.MIOTrackIndex,
    &LFRefMdl_B.TrackingandSensorFusion_l,
    &LFRefMdl_DW.TrackingandSensorFusion_l);

  // End of Outputs for SubSystem: '<Root>/Tracking and Sensor Fusion'

  // Switch: '<S2>/Switch1' incorporates:
  //   Constant: '<S2>/Constant1'

  if (rtb_x > 1000.0) {
    rtb_x = 200.0;

    // Switch: '<S2>/Switch2' incorporates:
    //   Constant: '<S2>/Constant1'
    //   Constant: '<S2>/Constant2'

    rtb_vx = 0.0;
  }

  // End of Switch: '<S2>/Switch1'

  // Outputs for Atomic SubSystem: '<S2>/Path Following Control System'
  // Inport: '<Root>/Driver Set Velocity' incorporates:
  //   Constant: '<S2>/Constant'
  //   Inport: '<Root>/Longitudinal Velocity'

  PathFollowingControlSystem(LFRefMdl_U.DriverSetVelocity, 1.5, rtb_x, rtb_vx,
    LFRefMdl_U.LongitudinalVelocity, rtb_Sum, rtb_Saturation, rtb_Saturation1,
    rtb_u_scale, &LFRefMdl_B.PathFollowingControlSystem_n,
    &LFRefMdl_ConstB.PathFollowingControlSystem_n,
    &LFRefMdl_DW.PathFollowingControlSystem_n, 10.0, -3.0, -0.26, 2.0, 0.26);

  // End of Outputs for SubSystem: '<S2>/Path Following Control System'

  // Outport: '<Root>/Acceleration'
  LFRefMdl_Y.Acceleration = rtb_u_scale[0];

  // Outport: '<Root>/Steering Angle'
  LFRefMdl_Y.SteeringAngle = rtb_u_scale[1];

  // Outport: '<Root>/Tracks' incorporates:
  //   MATLABSystem: '<S3>/Multi-Object Tracker'

  LFRefMdl_Y.Tracks = LFRefMdl_B.TrackingandSensorFusion_l.MultiObjectTracker;
}

// Model initialize function
void PathFollowingControllerRefMdlModelClass::initialize()
{
  // Registration code

  // initialize non-finites
  rt_InitInfAndNaN(sizeof(real_T));

  // block I/O
  {
    LFRefMdl_B.TrackingandSensorFusion_l.MultiObjectTracker =
      LFRefMdl_rtZBusMultiObjectTracker1;
  }

  // external outputs
  LFRefMdl_Y.Tracks = LFRefMdl_rtZBusMultiObjectTracker1;

  // SystemInitialize for Atomic SubSystem: '<Root>/Tracking and Sensor Fusion'

  // SystemInitialize for Inport: '<Root>/Radar' incorporates:
  //   Inport: '<Root>/System Clock'
  //   Inport: '<Root>/Vision'

  TrackingandSensorFusion_Init(&LFRefMdl_U.Vision,
    &LFRefMdl_B.TrackingandSensorFusion_l,
    &LFRefMdl_DW.TrackingandSensorFusion_l);

  // End of SystemInitialize for SubSystem: '<Root>/Tracking and Sensor Fusion'

  // SystemInitialize for Atomic SubSystem: '<S2>/Path Following Control System' 
  PathFollowingControlSystem_Init(&LFRefMdl_DW.PathFollowingControlSystem_n);

  // End of SystemInitialize for SubSystem: '<S2>/Path Following Control System' 
}

// Model terminate function
void PathFollowingControllerRefMdlModelClass::terminate()
{
  // Terminate for Atomic SubSystem: '<Root>/Tracking and Sensor Fusion'
  TrackingandSensorFusion_Term(&LFRefMdl_DW.TrackingandSensorFusion_l);

  // End of Terminate for SubSystem: '<Root>/Tracking and Sensor Fusion'
}

// Constructor
PathFollowingControllerRefMdlModelClass::PathFollowingControllerRefMdlModelClass
  () :
  LFRefMdl_U(),
  LFRefMdl_Y(),
  LFRefMdl_B(),
  LFRefMdl_DW(),
  LFRefMdl_M()
{
  // Currently there is no constructor body generated.
}

// Destructor
PathFollowingControllerRefMdlModelClass::
  ~PathFollowingControllerRefMdlModelClass()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
RT_MODEL_LFRefMdl_T * PathFollowingControllerRefMdlModelClass::getRTM()
{
  return (&LFRefMdl_M);
}

//
// File trailer for generated code.
//
// [EOF]
//
