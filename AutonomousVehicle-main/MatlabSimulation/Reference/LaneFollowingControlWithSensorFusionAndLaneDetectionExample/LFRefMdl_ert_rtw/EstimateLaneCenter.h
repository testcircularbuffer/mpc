//
// File: EstimateLaneCenter.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_EstimateLaneCenter_h_
#define RTW_HEADER_EstimateLaneCenter_h_
#include "rtwtypes.h"
#include "LFRefMdl_types.h"

// Block states (default storage) for system '<Root>/Estimate Lane Center'
typedef struct {
  real32_T Delay_4_DSTATE;             // '<S1>/Delay'
  real32_T Delay_3_DSTATE;             // '<S1>/Delay'
  real32_T Delay_2_DSTATE;             // '<S1>/Delay'
  real32_T Delay_1_DSTATE;             // '<S1>/Delay'
} DW_EstimateLaneCenter_LFRefMdl_T;

#endif                                 // RTW_HEADER_EstimateLaneCenter_h_

//
// File trailer for generated code.
//
// [EOF]
//
