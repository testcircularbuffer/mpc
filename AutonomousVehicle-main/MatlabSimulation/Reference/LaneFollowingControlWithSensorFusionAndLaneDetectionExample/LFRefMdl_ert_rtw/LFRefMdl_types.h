//
// File: LFRefMdl_types.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_LFRefMdl_types_h_
#define RTW_HEADER_LFRefMdl_types_h_
#include "rtwtypes.h"

// Model Code Variants
#ifndef DEFINED_TYPEDEF_FOR_BusVisionDetectionsObjectAttributes_
#define DEFINED_TYPEDEF_FOR_BusVisionDetectionsObjectAttributes_

typedef struct {
  real_T TargetIndex;
} BusVisionDetectionsObjectAttributes;

#endif

#ifndef DEFINED_TYPEDEF_FOR_drivingCoordinateFrameType_
#define DEFINED_TYPEDEF_FOR_drivingCoordinateFrameType_

typedef uint8_T drivingCoordinateFrameType;

// enum drivingCoordinateFrameType
const drivingCoordinateFrameType Invalid = 0U;// Default value
const drivingCoordinateFrameType Rectangular = 1U;
const drivingCoordinateFrameType Spherical = 2U;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusVisionDetectionsMeasurementParameters_
#define DEFINED_TYPEDEF_FOR_BusVisionDetectionsMeasurementParameters_

typedef struct {
  drivingCoordinateFrameType Frame;
  real_T OriginPosition[3];
  real_T Orientation[9];
  boolean_T HasVelocity;
} BusVisionDetectionsMeasurementParameters;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusVisionDetections_
#define DEFINED_TYPEDEF_FOR_BusVisionDetections_

typedef struct {
  real_T Time;
  real_T Measurement[6];
  real_T MeasurementNoise[36];
  real_T SensorIndex;
  real_T ObjectClassID;
  BusVisionDetectionsObjectAttributes ObjectAttributes;
  BusVisionDetectionsMeasurementParameters MeasurementParameters;
} BusVisionDetections;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusVision_
#define DEFINED_TYPEDEF_FOR_BusVision_

typedef struct {
  real_T NumDetections;
  boolean_T IsValidTime;
  BusVisionDetections Detections[20];
} BusVision;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusRadarDetectionsObjectAttributes_
#define DEFINED_TYPEDEF_FOR_BusRadarDetectionsObjectAttributes_

typedef struct {
  real_T TargetIndex;
  real_T SNR;
} BusRadarDetectionsObjectAttributes;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusRadarDetectionsMeasurementParameters_
#define DEFINED_TYPEDEF_FOR_BusRadarDetectionsMeasurementParameters_

typedef struct {
  drivingCoordinateFrameType Frame;
  real_T OriginPosition[3];
  real_T Orientation[9];
  boolean_T HasElevation;
  boolean_T HasVelocity;
} BusRadarDetectionsMeasurementParameters;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusRadarDetections_
#define DEFINED_TYPEDEF_FOR_BusRadarDetections_

typedef struct {
  real_T Time;
  real_T Measurement[6];
  real_T MeasurementNoise[36];
  real_T SensorIndex;
  real_T ObjectClassID;
  BusRadarDetectionsObjectAttributes ObjectAttributes;
  BusRadarDetectionsMeasurementParameters MeasurementParameters;
} BusRadarDetections;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusRadar_
#define DEFINED_TYPEDEF_FOR_BusRadar_

typedef struct {
  real_T NumDetections;
  boolean_T IsValidTime;
  BusRadarDetections Detections[50];
} BusRadar;

#endif

#ifndef DEFINED_TYPEDEF_FOR_LaneSensorBoundaries_
#define DEFINED_TYPEDEF_FOR_LaneSensorBoundaries_

// Describes sensor structure interface for lane boundaries from lane sensor
typedef struct {
  real32_T Curvature;
  real32_T CurvatureDerivative;
  real32_T HeadingAngle;
  real32_T LateralOffset;
  real32_T Strength;
} LaneSensorBoundaries;

#endif

#ifndef DEFINED_TYPEDEF_FOR_LaneSensor_
#define DEFINED_TYPEDEF_FOR_LaneSensor_

// Describes sensor structure interface for lane sensor
typedef struct {
  LaneSensorBoundaries Left;
  LaneSensorBoundaries Right;
} LaneSensor;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1DetectionsMeasurementParameters_
#define DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1DetectionsMeasurementParameters_

typedef struct {
  drivingCoordinateFrameType Frame;
  real_T OriginPosition[3];
  real_T Orientation[9];
  boolean_T HasVelocity;
  boolean_T HasElevation;
} BusDetectionConcatenation1DetectionsMeasurementParameters;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1Detections_
#define DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1Detections_

typedef struct {
  real_T Time;
  real_T Measurement[6];
  real_T MeasurementNoise[36];
  real_T SensorIndex;
  real_T ObjectClassID;
  BusDetectionConcatenation1DetectionsMeasurementParameters
    MeasurementParameters;
  BusRadarDetectionsObjectAttributes ObjectAttributes;
} BusDetectionConcatenation1Detections;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1_
#define DEFINED_TYPEDEF_FOR_BusDetectionConcatenation1_

typedef struct {
  real_T NumDetections;
  boolean_T IsValidTime;
  BusDetectionConcatenation1Detections Detections[70];
} BusDetectionConcatenation1;

#endif

#ifndef DEFINED_TYPEDEF_FOR_trackLogicType_
#define DEFINED_TYPEDEF_FOR_trackLogicType_

typedef int32_T trackLogicType;

// enum trackLogicType
const trackLogicType History = 1;      // Default value
const trackLogicType Score = 2;
const trackLogicType Integrated = 3;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusMultiObjectTracker1Tracks_
#define DEFINED_TYPEDEF_FOR_BusMultiObjectTracker1Tracks_

typedef struct {
  uint32_T TrackID;
  uint32_T BranchID;
  uint32_T SourceIndex;
  real_T UpdateTime;
  uint32_T Age;
  real_T State[6];
  real_T StateCovariance[36];
  real_T ObjectClassID;
  trackLogicType TrackLogic;
  boolean_T TrackLogicState[5];
  boolean_T IsConfirmed;
  boolean_T IsCoasted;
  boolean_T IsSelfReported;
  BusRadarDetectionsObjectAttributes ObjectAttributes[2];
} BusMultiObjectTracker1Tracks;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusMultiObjectTracker1_
#define DEFINED_TYPEDEF_FOR_BusMultiObjectTracker1_

typedef struct {
  real_T NumTracks;
  BusMultiObjectTracker1Tracks Tracks[100];
} BusMultiObjectTracker1;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_qYRJtcce7MM7XuQ3AAWdMD_
#define DEFINED_TYPEDEF_FOR_struct_qYRJtcce7MM7XuQ3AAWdMD_

typedef struct {
  real_T MaxIterations;
  real_T ConstraintTolerance;
  boolean_T UseWarmStart;
} struct_qYRJtcce7MM7XuQ3AAWdMD;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_SmvKLCDySlKdToNTroAGyF_
#define DEFINED_TYPEDEF_FOR_struct_SmvKLCDySlKdToNTroAGyF_

typedef struct {
  real_T MaxIterations;
  real_T ConstraintTolerance;
  real_T OptimalityTolerance;
  real_T ComplementarityTolerance;
  real_T StepTolerance;
} struct_SmvKLCDySlKdToNTroAGyF;

#endif

#ifndef struct_tag_Ctn44e8rPB9dn82XarkeK
#define struct_tag_Ctn44e8rPB9dn82XarkeK

struct tag_Ctn44e8rPB9dn82XarkeK
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                                 //struct_tag_Ctn44e8rPB9dn82XarkeK

#ifndef typedef_helperClusterDetections_LFRefMdl_T
#define typedef_helperClusterDetections_LFRefMdl_T

typedef tag_Ctn44e8rPB9dn82XarkeK helperClusterDetections_LFRefMdl_T;

#endif                              //typedef_helperClusterDetections_LFRefMdl_T

#ifndef struct_tag_Bmvm57AUSIUMC1RrbqRmaD
#define struct_tag_Bmvm57AUSIUMC1RrbqRmaD

struct tag_Bmvm57AUSIUMC1RrbqRmaD
{
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                                 //struct_tag_Bmvm57AUSIUMC1RrbqRmaD

#ifndef typedef_c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T
#define typedef_c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T

typedef tag_Bmvm57AUSIUMC1RrbqRmaD
  c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T;

#endif  //typedef_c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T

#ifndef struct_emxArray_char_T_1x11
#define struct_emxArray_char_T_1x11

struct emxArray_char_T_1x11
{
  char_T data[11];
  int32_T size[2];
};

#endif                                 //struct_emxArray_char_T_1x11

#ifndef typedef_emxArray_char_T_1x11_LFRefMdl_T
#define typedef_emxArray_char_T_1x11_LFRefMdl_T

typedef emxArray_char_T_1x11 emxArray_char_T_1x11_LFRefMdl_T;

#endif                                 //typedef_emxArray_char_T_1x11_LFRefMdl_T

#ifndef struct_tag_z9vNaXoQgneDJl3q3kNYIF
#define struct_tag_z9vNaXoQgneDJl3q3kNYIF

struct tag_z9vNaXoQgneDJl3q3kNYIF
{
  emxArray_char_T_1x11_LFRefMdl_T f1;
};

#endif                                 //struct_tag_z9vNaXoQgneDJl3q3kNYIF

#ifndef typedef_h_cell_wrap_LFRefMdl_T
#define typedef_h_cell_wrap_LFRefMdl_T

typedef tag_z9vNaXoQgneDJl3q3kNYIF h_cell_wrap_LFRefMdl_T;

#endif                                 //typedef_h_cell_wrap_LFRefMdl_T

#ifndef struct_emxArray_uint32_T_1x100
#define struct_emxArray_uint32_T_1x100

struct emxArray_uint32_T_1x100
{
  uint32_T data[100];
  int32_T size[2];
};

#endif                                 //struct_emxArray_uint32_T_1x100

#ifndef typedef_emxArray_uint32_T_1x100_LFRefMdl_T
#define typedef_emxArray_uint32_T_1x100_LFRefMdl_T

typedef emxArray_uint32_T_1x100 emxArray_uint32_T_1x100_LFRefMdl_T;

#endif                              //typedef_emxArray_uint32_T_1x100_LFRefMdl_T

#ifndef struct_tag_PMfBDzoakfdM9QAdfx2o6D
#define struct_tag_PMfBDzoakfdM9QAdfx2o6D

struct tag_PMfBDzoakfdM9QAdfx2o6D
{
  uint32_T f1[8];
};

#endif                                 //struct_tag_PMfBDzoakfdM9QAdfx2o6D

#ifndef typedef_cell_wrap_LFRefMdl_T
#define typedef_cell_wrap_LFRefMdl_T

typedef tag_PMfBDzoakfdM9QAdfx2o6D cell_wrap_LFRefMdl_T;

#endif                                 //typedef_cell_wrap_LFRefMdl_T

#ifndef struct_tag_xzQhUlYCW8bvVgWsCHrOYG
#define struct_tag_xzQhUlYCW8bvVgWsCHrOYG

struct tag_xzQhUlYCW8bvVgWsCHrOYG
{
  boolean_T pRecentHistory[50];
  boolean_T pIsFirstUpdate;
};

#endif                                 //struct_tag_xzQhUlYCW8bvVgWsCHrOYG

#ifndef typedef_c_trackHistoryLogic_LFRefMdl_T
#define typedef_c_trackHistoryLogic_LFRefMdl_T

typedef tag_xzQhUlYCW8bvVgWsCHrOYG c_trackHistoryLogic_LFRefMdl_T;

#endif                                 //typedef_c_trackHistoryLogic_LFRefMdl_T

#ifndef struct_tag_YaWk42pjcytpfoi9mOxWpC
#define struct_tag_YaWk42pjcytpfoi9mOxWpC

struct tag_YaWk42pjcytpfoi9mOxWpC
{
  real_T pState[6];
  real_T pSqrtStateCovariance[36];
  real_T pSqrtStateCovarianceScalar;
  boolean_T pIsSetStateCovariance;
  real_T pSqrtProcessNoise[9];
  real_T pSqrtProcessNoiseScalar;
  boolean_T pIsSetProcessNoise;
  real_T pSqrtMeasurementNoise[36];
  real_T pSqrtMeasurementNoiseScalar;
  boolean_T pIsSetMeasurementNoise;
  boolean_T pHasPrediction;
  boolean_T pIsValidStateTransitionFcn;
  boolean_T pIsValidMeasurementFcn;
  boolean_T pIsFirstCallPredict;
  boolean_T pIsFirstCallCorrect;
};

#endif                                 //struct_tag_YaWk42pjcytpfoi9mOxWpC

#ifndef typedef_c_trackingEKF_LFRefMdl_T
#define typedef_c_trackingEKF_LFRefMdl_T

typedef tag_YaWk42pjcytpfoi9mOxWpC c_trackingEKF_LFRefMdl_T;

#endif                                 //typedef_c_trackingEKF_LFRefMdl_T

#ifndef struct_tag_RAcioICp5st0b2zr6s4LgD
#define struct_tag_RAcioICp5st0b2zr6s4LgD

struct tag_RAcioICp5st0b2zr6s4LgD
{
  uint32_T BranchID;
  uint32_T TrackID;
  real_T Time;
  c_trackHistoryLogic_LFRefMdl_T *TrackLogic;
  boolean_T IsConfirmed;
  real_T ObjectClassID;
  real_T UpdateTime;
  c_trackingEKF_LFRefMdl_T *Filter;
  uint8_T pMotionModel;
  uint32_T pAge;
  boolean_T pIsCoasted;
  c_trackingEKF_LFRefMdl_T *pDistanceFilter;
  BusRadarDetectionsObjectAttributes pObjectAttributes[2];
  boolean_T pUsedObjectAttributes[2];
  c_trackingEKF_LFRefMdl_T _pobj0;
};

#endif                                 //struct_tag_RAcioICp5st0b2zr6s4LgD

#ifndef typedef_b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T
#define typedef_b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T

typedef tag_RAcioICp5st0b2zr6s4LgD
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T;

#endif  //typedef_b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T

#ifndef struct_tag_IatdutsZftNaHre6PrIB1B
#define struct_tag_IatdutsZftNaHre6PrIB1B

struct tag_IatdutsZftNaHre6PrIB1B
{
  int32_T isInitialized;
  cell_wrap_LFRefMdl_T inputVarSize[4];
};

#endif                                 //struct_tag_IatdutsZftNaHre6PrIB1B

#ifndef typedef_f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T
#define typedef_f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T

typedef tag_IatdutsZftNaHre6PrIB1B
  f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T;

#endif //typedef_f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T

#ifndef struct_tag_5xAxq2KMUZABn2WnkkMdGB
#define struct_tag_5xAxq2KMUZABn2WnkkMdGB

struct tag_5xAxq2KMUZABn2WnkkMdGB
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  cell_wrap_LFRefMdl_T inputVarSize[2];
  BusDetectionConcatenation1 pOutTemp;
};

#endif                                 //struct_tag_5xAxq2KMUZABn2WnkkMdGB

#ifndef typedef_driving_internal_DetectionConcatenation_LFRefMdl_T
#define typedef_driving_internal_DetectionConcatenation_LFRefMdl_T

typedef tag_5xAxq2KMUZABn2WnkkMdGB
  driving_internal_DetectionConcatenation_LFRefMdl_T;

#endif              //typedef_driving_internal_DetectionConcatenation_LFRefMdl_T

#ifndef struct_tag_kMkOaLoRiAqVp1drdSQTS
#define struct_tag_kMkOaLoRiAqVp1drdSQTS

struct tag_kMkOaLoRiAqVp1drdSQTS
{
  int32_T isInitialized;
  real_T pNumDetections;
  BusDetectionConcatenation1Detections pDetections[70];
  real_T pOriginatingSensor[70];
};

#endif                                 //struct_tag_kMkOaLoRiAqVp1drdSQTS

#ifndef typedef_e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T
#define typedef_e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T

typedef tag_kMkOaLoRiAqVp1drdSQTS
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T;

#endif //typedef_e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T

#ifndef struct_tag_KMmzImYdBE5WSpB1jhstWE
#define struct_tag_KMmzImYdBE5WSpB1jhstWE

struct tag_KMmzImYdBE5WSpB1jhstWE
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  boolean_T TunablePropsChanged;
  cell_wrap_LFRefMdl_T inputVarSize[2];
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *pTracksList
    [100];
  real_T pNumLiveTracks;
  uint32_T pTrackIDs[100];
  boolean_T pConfirmedTracks[100];
  c_trackHistoryLogic_LFRefMdl_T *pLogic;
  BusDetectionConcatenation1Detections pSampleDetection;
  c_trackingEKF_LFRefMdl_T *pDistFilter;
  boolean_T pWasDetectable[100];
  real_T pNumDetections;
  uint32_T pLastTrackID;
  real_T pLastTimeStamp;
  real_T pCostOfNonAssignment;
  f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T
    cCostCalculator;
  c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T *cAssigner;
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T
    *cDetectionManager;
  c_trackingEKF_LFRefMdl_T _pobj0[101];
  c_trackHistoryLogic_LFRefMdl_T _pobj1[100];
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T _pobj2[100];
  c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T _pobj3;
  c_trackHistoryLogic_LFRefMdl_T _pobj4;
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T _pobj5;
};

#endif                                 //struct_tag_KMmzImYdBE5WSpB1jhstWE

#ifndef typedef_multiObjectTracker_LFRefMdl_T
#define typedef_multiObjectTracker_LFRefMdl_T

typedef tag_KMmzImYdBE5WSpB1jhstWE multiObjectTracker_LFRefMdl_T;

#endif                                 //typedef_multiObjectTracker_LFRefMdl_T

#ifndef struct_tag_sW4W9Ljj33pbPPYs8uukpCB
#define struct_tag_sW4W9Ljj33pbPPYs8uukpCB

struct tag_sW4W9Ljj33pbPPYs8uukpCB
{
  uint32_T TrackID;
  uint32_T BranchID;
  uint32_T SourceIndex;
  real_T UpdateTime;
  uint32_T Age;
  real_T State[6];
  real_T StateCovariance[36];
  real_T ObjectClassID;
  trackLogicType TrackLogic;
  boolean_T TrackLogicState[5];
  boolean_T IsConfirmed;
  boolean_T IsCoasted;
  boolean_T IsSelfReported;
  BusRadarDetectionsObjectAttributes ObjectAttributes[2];
};

#endif                                 //struct_tag_sW4W9Ljj33pbPPYs8uukpCB

#ifndef typedef_sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T
#define typedef_sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T

typedef tag_sW4W9Ljj33pbPPYs8uukpCB sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T;

#endif                              //typedef_sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T

#ifndef struct_emxArray_uint32_T_200
#define struct_emxArray_uint32_T_200

struct emxArray_uint32_T_200
{
  uint32_T data[200];
  int32_T size;
};

#endif                                 //struct_emxArray_uint32_T_200

#ifndef typedef_emxArray_uint32_T_200_LFRefMdl_T
#define typedef_emxArray_uint32_T_200_LFRefMdl_T

typedef emxArray_uint32_T_200 emxArray_uint32_T_200_LFRefMdl_T;

#endif                                //typedef_emxArray_uint32_T_200_LFRefMdl_T

// Forward declaration for rtModel
typedef struct tag_RTM_LFRefMdl_T RT_MODEL_LFRefMdl_T;

#endif                                 // RTW_HEADER_LFRefMdl_types_h_

//
// File trailer for generated code.
//
// [EOF]
//
