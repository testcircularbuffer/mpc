//
// File: TrackingandSensorFusion.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_TrackingandSensorFusion_h_
#define RTW_HEADER_TrackingandSensorFusion_h_
#include <cfloat>
#include <cmath>
#include <cstring>
#include <math.h>
#include "rtwtypes.h"
#include "LFRefMdl_types.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"

// Block signals for system '<Root>/Tracking and Sensor Fusion'
typedef struct {
  real_T paddedCost_data[28900];
  real_T paddedCost_data_m[28900];
  BusDetectionConcatenation1Detections trackDetections_data[200];
  real_T costMatrix_data[7000];
  real_T costMatrix[7000];
  real_T costMatrix_data_c[7000];
  real_T costMatrix_data_k[7000];
  real_T costMatrix_c[7000];
  BusMultiObjectTracker1 MultiObjectTracker;// '<S3>/Multi-Object Tracker'
  BusMultiObjectTracker1 vargs;
  BusMultiObjectTracker1Tracks tracks[100];
  BusDetectionConcatenation1 DetectionConcatenation1;// '<S3>/Detection Concatenation1' 
  BusDetectionConcatenation1 r;
  BusDetectionConcatenation1Detections b_Detections[70];
  BusDetectionConcatenation1Detections b[70];
  BusDetectionConcatenation1Detections dets[70];
  BusDetectionConcatenation1Detections dets_b[70];
  BusDetectionConcatenation1Detections dets_p[70];
  int32_T g_data[7000];
  BusRadar MATLABSystem;               // '<S3>/MATLAB System'
  BusRadar r1;
  BusRadarDetections detsClust_data[50];
  BusRadarDetections detBusIn_Detections_data[50];
  real_T distances_data[2500];
  boolean_T toCalculate_data[7000];
} B_TrackingandSensorFusion_LFRefMdl_T;

// Block states (default storage) for system '<Root>/Tracking and Sensor Fusion' 
typedef struct {
  multiObjectTracker_LFRefMdl_T obj;   // '<S3>/Multi-Object Tracker'
  driving_internal_DetectionConcatenation_LFRefMdl_T obj_m;// '<S3>/Detection Concatenation1' 
  helperClusterDetections_LFRefMdl_T obj_d;// '<S3>/MATLAB System'
  boolean_T objisempty;                // '<S3>/Multi-Object Tracker'
  boolean_T objisempty_p;              // '<S3>/MATLAB System'
  boolean_T objisempty_i;              // '<S3>/Detection Concatenation1'
} DW_TrackingandSensorFusion_LFRefMdl_T;

#endif                                 // RTW_HEADER_TrackingandSensorFusion_h_

//
// File trailer for generated code.
//
// [EOF]
//
