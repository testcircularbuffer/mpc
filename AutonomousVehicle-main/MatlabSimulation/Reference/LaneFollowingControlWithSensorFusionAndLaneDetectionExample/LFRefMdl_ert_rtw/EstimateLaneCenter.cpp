//
// File: EstimateLaneCenter.cpp
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "EstimateLaneCenter.h"

// Include model header file for global data
#include "LFRefMdl.h"
#include "LFRefMdl_private.h"

// Output and update for atomic system: '<Root>/Estimate Lane Center'
void PathFollowingControllerRefMdlModelClass::EstimateLaneCenter(real32_T
  rtu_LaneDetections, real32_T rtu_LaneDetections_m, real32_T
  rtu_LaneDetections_f, real32_T rtu_LaneDetections_a, real32_T
  rtu_LaneDetections_b, real32_T rtu_LaneDetections_j, real32_T
  rtu_LaneDetections_l, real32_T rtu_LaneDetections_jl, real32_T
  rtu_LaneDetections_d, real32_T rtu_LaneDetections_d0, real_T
  rtu_LongitudinalVelocity, real_T rty_PreviewedCurvature[11], real32_T
  *rty_LateralDeviation, real32_T *rty_RelativeYawAngle,
  DW_EstimateLaneCenter_LFRefMdl_T *localDW)
{
  real_T rtb_Product;
  int32_T i;
  real32_T rtb_Subtract;

  // MATLAB Function: '<S1>/MATLAB Function' incorporates:
  //   BusCreator generated from: '<S1>/MATLAB Function'

  if ((rtu_LaneDetections_b > 0.0F) && (rtu_LaneDetections_d0 > 0.0F)) {
    // Outputs for Function Call SubSystem: '<S1>/Center from Left and Right'
    // Merge generated from: '<S1>/Merge' incorporates:
    //   Gain: '<S5>/Gain'
    //   Sum: '<S5>/Add'

    localDW->Delay_1_DSTATE = (rtu_LaneDetections + rtu_LaneDetections_j) * 0.5F;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Gain: '<S5>/Gain1'
    //   Sum: '<S5>/Add1'

    localDW->Delay_2_DSTATE = (rtu_LaneDetections_m + rtu_LaneDetections_l) *
      0.5F;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Gain: '<S5>/Gain2'
    //   Sum: '<S5>/Add2'

    localDW->Delay_3_DSTATE = (rtu_LaneDetections_f + rtu_LaneDetections_jl) *
      0.5F;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Gain: '<S5>/Gain3'
    //   Sum: '<S5>/Add3'

    localDW->Delay_4_DSTATE = (rtu_LaneDetections_a + rtu_LaneDetections_d) *
      0.5F;

    // End of Outputs for SubSystem: '<S1>/Center from Left and Right'
  } else if (rtu_LaneDetections_b > 0.0F) {
    // Outputs for Function Call SubSystem: '<S1>/Center from Left'
    // Sum: '<S4>/Subtract' incorporates:
    //   Constant: '<S4>/Constant'
    //   Constant: '<S4>/Half Lane Width Estimate'
    //   Product: '<S4>/Product'

    rtb_Subtract = 1.0F - rtu_LaneDetections * 1.8F;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Product: '<S4>/Divide'

    localDW->Delay_1_DSTATE = rtu_LaneDetections / rtb_Subtract;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Product: '<S4>/Divide1'
    //   Product: '<S4>/Product1'

    localDW->Delay_2_DSTATE = rtu_LaneDetections_m / (rtb_Subtract *
      rtb_Subtract);

    // Merge generated from: '<S1>/Merge' incorporates:
    //   Constant: '<S4>/Half Lane Width Estimate1'
    //   Sum: '<S4>/Subtract1'

    localDW->Delay_4_DSTATE = rtu_LaneDetections_a - 1.8F;

    // Merge generated from: '<S1>/Merge' incorporates:
    //   SignalConversion generated from: '<S4>/Center'

    localDW->Delay_3_DSTATE = rtu_LaneDetections_f;

    // End of Outputs for SubSystem: '<S1>/Center from Left'
  } else {
    if (rtu_LaneDetections_d0 > 0.0F) {
      // Outputs for Function Call SubSystem: '<S1>/Center from Right'
      // Sum: '<S7>/Subtract' incorporates:
      //   Constant: '<S7>/Constant'
      //   Constant: '<S7>/Half Lane Width Estimate'
      //   Product: '<S7>/Product'

      rtb_Subtract = rtu_LaneDetections_j * 1.8F + 1.0F;

      // Merge generated from: '<S1>/Merge' incorporates:
      //   Product: '<S7>/Divide'

      localDW->Delay_1_DSTATE = rtu_LaneDetections_j / rtb_Subtract;

      // Merge generated from: '<S1>/Merge' incorporates:
      //   Product: '<S7>/Divide1'
      //   Product: '<S7>/Product1'

      localDW->Delay_2_DSTATE = rtu_LaneDetections_l / (rtb_Subtract *
        rtb_Subtract);

      // Merge generated from: '<S1>/Merge' incorporates:
      //   Constant: '<S7>/Half Lane Width Estimate1'
      //   Sum: '<S7>/Subtract1'

      localDW->Delay_4_DSTATE = rtu_LaneDetections_d + 1.8F;

      // Merge generated from: '<S1>/Merge' incorporates:
      //   SignalConversion generated from: '<S7>/Center'

      localDW->Delay_3_DSTATE = rtu_LaneDetections_jl;

      // End of Outputs for SubSystem: '<S1>/Center from Right'
    }
  }

  // End of MATLAB Function: '<S1>/MATLAB Function'

  // UnaryMinus: '<S8>/Unary Minus1'
  *rty_RelativeYawAngle = -localDW->Delay_2_DSTATE;

  // Saturate: '<S1>/Saturation3'
  if (*rty_RelativeYawAngle > 0.06F) {
    *rty_RelativeYawAngle = 0.06F;
  } else {
    if (*rty_RelativeYawAngle < -0.06F) {
      *rty_RelativeYawAngle = -0.06F;
    }
  }

  // End of Saturate: '<S1>/Saturation3'

  // Product: '<S10>/Product'
  rtb_Product = *rty_RelativeYawAngle * rtu_LongitudinalVelocity;

  // UnaryMinus: '<S8>/Unary Minus'
  *rty_RelativeYawAngle = -localDW->Delay_1_DSTATE;

  // Saturate: '<S1>/Saturation2'
  if (*rty_RelativeYawAngle > 0.15F) {
    *rty_RelativeYawAngle = 0.15F;
  } else {
    if (*rty_RelativeYawAngle < -0.15F) {
      *rty_RelativeYawAngle = -0.15F;
    }
  }

  // End of Saturate: '<S1>/Saturation2'

  // Sum: '<S10>/Sum' incorporates:
  //   Gain: '<S10>/Gain'

  for (i = 0; i < 11; i++) {
    rty_PreviewedCurvature[i] = LFRefMdl_ConstP.Gain_Gain[i] * rtb_Product +
      *rty_RelativeYawAngle;
  }

  // End of Sum: '<S10>/Sum'

  // UnaryMinus: '<S1>/Unary Minus' incorporates:
  //   UnaryMinus: '<S8>/Unary Minus2'

  *rty_RelativeYawAngle = localDW->Delay_3_DSTATE;

  // Saturate: '<S1>/Saturation1'
  if (*rty_RelativeYawAngle > 0.6F) {
    *rty_RelativeYawAngle = 0.6F;
  } else {
    if (*rty_RelativeYawAngle < -0.6F) {
      *rty_RelativeYawAngle = -0.6F;
    }
  }

  // End of Saturate: '<S1>/Saturation1'

  // UnaryMinus: '<S1>/Unary Minus1' incorporates:
  //   UnaryMinus: '<S8>/Unary Minus3'

  *rty_LateralDeviation = localDW->Delay_4_DSTATE;

  // Saturate: '<S1>/Saturation'
  if (*rty_LateralDeviation > 0.5F) {
    *rty_LateralDeviation = 0.5F;
  } else {
    if (*rty_LateralDeviation < -0.5F) {
      *rty_LateralDeviation = -0.5F;
    }
  }

  // End of Saturate: '<S1>/Saturation'
}

//
// File trailer for generated code.
//
// [EOF]
//
