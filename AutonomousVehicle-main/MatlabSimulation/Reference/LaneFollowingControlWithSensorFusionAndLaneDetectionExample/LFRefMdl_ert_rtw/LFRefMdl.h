//
// File: LFRefMdl.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_LFRefMdl_h_
#define RTW_HEADER_LFRefMdl_h_
#include "rtwtypes.h"
#include "LFRefMdl_types.h"

// Child system includes
#include "EstimateLaneCenter.h"
#include "PathFollowingControlSystem.h"
#include "TrackingandSensorFusion.h"
#include "rt_nonfinite.h"

// Macros for accessing real-time model data structure
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

// Block signals (default storage)
typedef struct {
  real32_T Curvature;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T CurvatureDerivative;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T HeadingAngle;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T LateralOffset;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T Curvature_p;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T CurvatureDerivative_b;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T HeadingAngle_g;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  real32_T LateralOffset_k;
                 // '<Root>/BusConversion_InsertedFor_Lane Sensor_at_outport_0'
  B_TrackingandSensorFusion_LFRefMdl_T TrackingandSensorFusion_l;// '<Root>/Tracking and Sensor Fusion' 
  B_PathFollowingControlSystem_LFRefMdl_T PathFollowingControlSystem_n;// '<S2>/Path Following Control System' 
} B_LFRefMdl_T;

// Block states (default storage) for system '<Root>'
typedef struct {
  DW_TrackingandSensorFusion_LFRefMdl_T TrackingandSensorFusion_l;// '<Root>/Tracking and Sensor Fusion' 
  DW_PathFollowingControlSystem_LFRefMdl_T PathFollowingControlSystem_n;// '<S2>/Path Following Control System' 
  DW_EstimateLaneCenter_LFRefMdl_T EstimateLaneCenter_a;// '<Root>/Estimate Lane Center' 
} DW_LFRefMdl_T;

// Invariant block signals (default storage)
typedef const struct tag_ConstB_LFRefMdl_T {
  ConstB_PathFollowingControlSystem_LFRefMdl_T PathFollowingControlSystem_n;// '<S2>/Path Following Control System' 
} ConstB_LFRefMdl_T;

// Constant parameters (default storage)
typedef struct {
  // Expression: [0:Ts:PredictionHorizon*Ts]'
  //  Referenced by: '<S10>/Gain'

  real_T Gain_Gain[11];

  // Expression: [0;0;InitialLongVel;0]
  //  Referenced by: '<S50>/U Constant'

  real_T UConstant_Value[4];

  // Expression: [1.4*InitialLongVel+DefaultSpacing;InitialLongVel;0;0]
  //  Referenced by: '<S50>/Y Constant'

  real_T YConstant_Value[4];

  // Pooled Parameter (Expression: zeros(NumEgoStates+3+4*hasLag,1))
  //  Referenced by:
  //    '<S50>/DX Constant'
  //    '<S50>/X Constant'

  real_T pooled12[11];

  // Expression: lastPcov
  //  Referenced by: '<S20>/LastPcov'

  real_T LastPcov_InitialCondition[169];

  // Expression: posSelector
  //  Referenced by: '<S3>/Position Selector'

  real_T PositionSelector_Value[12];
} ConstP_LFRefMdl_T;

// External inputs (root inport signals with default storage)
typedef struct {
  BusVision Vision;                    // '<Root>/Vision'
  BusRadar Radar;                      // '<Root>/Radar'
  real_T DriverSetVelocity;            // '<Root>/Driver Set Velocity'
  real_T LongitudinalVelocity;         // '<Root>/Longitudinal Velocity'
  LaneSensor LaneSensor_e;             // '<Root>/Lane Sensor'
  real_T SystemClock;                  // '<Root>/System Clock'
} ExtU_LFRefMdl_T;

// External outputs (root outports fed by signals with default storage)
typedef struct {
  real_T Acceleration;                 // '<Root>/Acceleration'
  real_T SteeringAngle;                // '<Root>/Steering Angle'
  BusMultiObjectTracker1 Tracks;       // '<Root>/Tracks'
  real_T MIOTrackIndex;                // '<Root>/MIO Track Index'
} ExtY_LFRefMdl_T;

// Real-time Model Data Structure
struct tag_RTM_LFRefMdl_T {
  const char_T * volatile errorStatus;
};

// External data declarations for dependent source files
extern const BusMultiObjectTracker1 LFRefMdl_rtZBusMultiObjectTracker1;// BusMultiObjectTracker1 ground 
extern const ConstB_LFRefMdl_T LFRefMdl_ConstB;// constant block i/o

// Constant parameters (default storage)
extern const ConstP_LFRefMdl_T LFRefMdl_ConstP;

// Class declaration for model LFRefMdl
class PathFollowingControllerRefMdlModelClass {
  // public data and function members
 public:
  // External inputs
  ExtU_LFRefMdl_T LFRefMdl_U;

  // External outputs
  ExtY_LFRefMdl_T LFRefMdl_Y;

  // model initialize function
  void initialize();

  // model step function
  void step();

  // model terminate function
  void terminate();

  // Constructor
  PathFollowingControllerRefMdlModelClass();

  // Destructor
  ~PathFollowingControllerRefMdlModelClass();

  // Real-Time Model get method
  RT_MODEL_LFRefMdl_T * getRTM();

  // private data and function members
 private:
  // Block signals
  B_LFRefMdl_T LFRefMdl_B;

  // Block states
  DW_LFRefMdl_T LFRefMdl_DW;

  // Real-Time Model
  RT_MODEL_LFRefMdl_T LFRefMdl_M;

  // private member function(s) for subsystem '<Root>/Estimate Lane Center'
  static void EstimateLaneCenter(real32_T rtu_LaneDetections, real32_T
    rtu_LaneDetections_m, real32_T rtu_LaneDetections_f, real32_T
    rtu_LaneDetections_a, real32_T rtu_LaneDetections_b, real32_T
    rtu_LaneDetections_j, real32_T rtu_LaneDetections_l, real32_T
    rtu_LaneDetections_jl, real32_T rtu_LaneDetections_d, real32_T
    rtu_LaneDetections_d0, real_T rtu_LongitudinalVelocity, real_T
    rty_PreviewedCurvature[11], real32_T *rty_LateralDeviation, real32_T
    *rty_RelativeYawAngle, DW_EstimateLaneCenter_LFRefMdl_T *localDW);

  // private member function(s) for subsystem '<S2>/Path Following Control System'
  static void PathFollowingControlSystem_Init
    (DW_PathFollowingControlSystem_LFRefMdl_T *localDW);
  void PathFollowingControlSystem(real_T rtu_Setvelocity, real_T rtu_Timegap,
    real_T rtu_Relativedistance, real_T rtu_Relativevelocity, real_T
    rtu_Longitudinalvelocity, const real_T rtu_Curvature[11], real32_T
    rtu_Lateraldeviation, real32_T rtu_Relativeyawangle, real_T
    rtyy_LongitudinalaccelerationSteeringangle[2],
    B_PathFollowingControlSystem_LFRefMdl_T *localB, const
    ConstB_PathFollowingControlSystem_LFRefMdl_T *localC,
    DW_PathFollowingControlSystem_LFRefMdl_T *localDW, real_T rtp_DefaultSpacing,
    real_T rtp_MinAcceleration, real_T rtp_MinSteering, real_T
    rtp_MaxAcceleration, real_T rtp_MaxSteering);
  void pfcblock_utilAugmentVehicleMdlHasSpacingCtrl(const real_T Ag[16], const
    real_T Bg[8], const real_T Cg[12], real_T Vx, real_T As[49], real_T Bs[28],
    real_T Cs[28]);
  int32_T xgetrfs(real_T A[121], real_T B[121]);
  void lusolve(const real_T A[121], const real_T B[121], real_T X[121]);
  void PadeApproximantOfDegree(const real_T A[121], uint8_T m, real_T F[121]);
  void expm(real_T A[121], real_T F[121]);
  void adasblocks_utilDicretizeModel(const real_T a[121], const real_T b[44],
    real_T Ts, real_T A[121], real_T B[44]);
  void mpc_plantupdate(const real_T a[121], real_T b[44], const real_T c[44],
                       real_T d[16], real_T b_A[169], real_T b_B[143], real_T
                       b_C[52], real_T b_D[44], const real_T b_mvindex[2], const
                       real_T b_mdindex[2], const real_T b_myindex[4], const
                       real_T b_Uscale[4], const real_T b_Yscale[4], real_T Bu
                       [26], real_T Bv[39], real_T Cm[52], real_T Dv[12], real_T
                       Dvm[12], real_T QQ[169], real_T RR[16], real_T NN[52]);
  real_T mod(real_T x);
  real_T mod_o(real_T x);
  void mpc_updateFromNominal(real_T b_Mlim[18], const real_T b_Mrows[18], const
    real_T U0[4], const real_T b_Uscale[4], const real_T old_mvoff[2], const
    real_T b_mvindex[2], const real_T b_mdindex[2], real_T b_utarget[20], const
    real_T Y0[4], const real_T b_Yscale[4], const real_T old_yoff[4], const
    real_T b_myindex[4], const real_T X0[11], real_T b_xoff[13], const real_T
    DX0[11], real_T Bv[429], real_T new_mvoff[2], real_T new_mdoff[2], real_T
    new_yoff[4], real_T new_myoff[4]);
  void mrdiv(const real_T b_A[16], const real_T b_B[16], real_T Y[16]);
  void mpc_constraintcoef(const real_T b_A[169], const real_T Bu[26], const
    real_T Bv[39], const real_T b_C[52], const real_T Dv[12], const real_T b_Jm
    [80], real_T b_SuJm[160], real_T b_Sx[520], real_T b_Su1[80], real_T b_Hv
    [1320], B_PathFollowingControlSystem_LFRefMdl_T *localB);
  void WtMult(const real_T W[2], const real_T M[80], real_T nwt, real_T WM[80]);
  void mpc_calculatehessian(const real_T b_Wy[4], const real_T b_Wu[2], const
    real_T b_Wdu[2], const real_T b_SuJm[160], const real_T I2Jm[80], const
    real_T b_Jm[80], const real_T b_I1[40], const real_T b_Su1[80], const real_T
    b_Sx[520], const real_T b_Hv[1320], real_T nmv, real_T b_ny, real_T b_H[16],
    real_T b_Ku1[8], real_T b_Kut[80], real_T b_Kx[52], real_T b_Kv[132], real_T
    b_Kr[160]);
  int32_T xpotrf(real_T b_A[25]);
  real_T minimum(const real_T x[5]);
  void mpc_checkhessian(real_T b_H[25], real_T L[25], real_T *BadH);
  void trisolve(const real_T b_A[25], real_T b_B[25]);
  void Unconstrained(const real_T b_Hinv[25], const real_T f[5], real_T x[5],
                     int16_T n);
  real_T norm(const real_T x[5]);
  real_T xnrm2(int32_T n, const real_T x[25], int32_T ix0);
  void xgemv(int32_T b_m, int32_T n, const real_T b_A[25], int32_T ia0, const
             real_T x[25], int32_T ix0, real_T y[5]);
  void xgerc(int32_T b_m, int32_T n, real_T alpha1, int32_T ix0, const real_T y
             [5], real_T b_A[25], int32_T ia0);
  void qr(const real_T b_A[25], real_T Q[25], real_T R[25]);
  real_T KWIKfactor(const real_T b_Ac[90], const int16_T iC[18], int16_T nA,
                    const real_T b_Linv[25], real_T RLinv[25], real_T b_D[25],
                    real_T b_H[25], int16_T n);
  void abs_a(const real_T x[5], real_T y[5]);
  real_T maximum(const real_T x[5]);
  void abs_ai(const real_T x[18], real_T y[18]);
  void maximum2(const real_T x[18], real_T y, real_T ex[18]);
  real_T mtimes(const real_T b_A[5], const real_T b_B[5]);
  void DropConstraint(int16_T kDrop, int16_T iA[18], int16_T *nA, int16_T iC[18]);
  void qpkwik(const real_T b_Linv[25], const real_T b_Hinv[25], const real_T f[5],
              const real_T b_Ac[90], const real_T b[18], int16_T iA[18], int16_T
              maxiter, real_T FeasTol, real_T x[5], real_T lambda[18], real_T
              *status);
  void mpc_solveQP(const real_T xQP[13], const real_T b_Kx[52], const real_T
                   b_Kr[160], const real_T rseq[40], const real_T b_Ku1[8],
                   const real_T old_u[2], const real_T b_Kv[132], const real_T
                   vseq[33], const real_T b_Kut[80], const real_T b_utarget[20],
                   const real_T b_Linv[25], const real_T b_Hinv[25], const
                   real_T b_Ac[90], const real_T Bc[18], boolean_T iA[18],
                   real_T zopt[5], real_T f[5], real_T *status);
  void mpcblock_optimizer(const real_T rseq[40], const real_T vseq[33], const
    real_T umin[2], const real_T umax[2], const real_T ymin[4], real_T switch_in,
    const real_T x[13], const real_T old_u[2], const boolean_T iA[18], const
    real_T b_Mlim[18], real_T b_Mx[234], real_T b_Mu1[36], real_T b_Mv[594],
    const real_T b_utarget[20], const real_T b_uoff[2], const real_T b_yoff[4],
    real_T b_enable_value, real_T b_H[25], real_T b_Ac[90], const real_T b_Wy[4],
    const real_T b_Wdu[2], const real_T b_Jm[80], const real_T b_Wu[2], const
    real_T b_I1[40], const real_T b_A[169], const real_T Bu[286], const real_T
    Bv[429], const real_T b_C[52], const real_T Dv[132], const real_T b_Mrows[18],
    const real_T b_RYscale[4], const real_T b_RMVscale[2], real_T u[2], real_T
    useq[22], real_T *status, boolean_T iAout[18],
    B_PathFollowingControlSystem_LFRefMdl_T *localB);

  // private member function(s) for subsystem '<Root>/Tracking and Sensor Fusion'
  void TrackingandSensorFusion_Init(const BusVision *rtu_Vision,
    B_TrackingandSensorFusion_LFRefMdl_T *localB,
    DW_TrackingandSensorFusion_LFRefMdl_T *localDW);
  void TrackingandSensorFusion(const BusRadar *rtu_Radar, const BusVision
    *rtu_Vision, real32_T rtu_Lane, real32_T rtu_Lane_p, real32_T rtu_Lane_px,
    real32_T rtu_Lane_m, real32_T rtu_Lane_k, real32_T rtu_Lane_c, real32_T
    rtu_Lane_mn, real32_T rtu_Lane_h, real32_T rtu_Lane_o, real32_T rtu_Lane_j,
    real_T rtu_SystemClock, real_T *rty_RelativeDistance, real_T
    *rty_RelativeVelocity, real_T *rty_MIOTrackIndex,
    B_TrackingandSensorFusion_LFRefMdl_T *localB,
    DW_TrackingandSensorFusion_LFRefMdl_T *localDW);
  static void TrackingandSensorFusion_Term(DW_TrackingandSensorFusion_LFRefMdl_T
    *localDW);
  void DetectionConcatenation_createObjDetStructTemplate(real_T
    in1_NumDetections, boolean_T in1_IsValidTime, const BusVisionDetections
    in1_Detections[20], const BusRadarDetections in2_Detections[50], real_T
    *out_NumDetections, boolean_T *out_IsValidTime,
    BusDetectionConcatenation1Detections out_Detections[70]);
  void nullify_k(const BusDetectionConcatenation1Detections in[70],
                 BusDetectionConcatenation1Detections out[70]);
  void nullify(const BusDetectionConcatenation1Detections in_Detections[70],
               real_T *out_NumDetections, boolean_T *out_IsValidTime,
               BusDetectionConcatenation1Detections out_Detections[70]);
  void DetectionConcatenation_setupImpl
    (driving_internal_DetectionConcatenation_LFRefMdl_T *obj, real_T
     varargin_1_NumDetections, boolean_T varargin_1_IsValidTime, const
     BusVisionDetections varargin_1_Detections[20], const BusRadarDetections
     varargin_2_Detections[50], B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void char_k(drivingCoordinateFrameType varargin_1, char_T y_data[], int32_T
              y_size[2]);
  boolean_T strcmp_m(const char_T a_data[], const int32_T a_size[2]);
  real_T norm_m(const real_T x[3]);
  real_T cosd(real_T x);
  void parseDetectionForInitFcn(const real_T detection_Measurement[6], const
    real_T detection_MeasurementNoise[36], drivingCoordinateFrameType
    detection_MeasurementParameters_Frame, const real_T
    detection_MeasurementParameters_OriginPosition[3], const real_T
    detection_MeasurementParameters_Orientation[9], boolean_T
    detection_MeasurementParameters_HasVelocity, boolean_T
    detection_MeasurementParameters_HasElevation, real_T posMeas[3], real_T
    velMeas[3], real_T posCov[9], real_T velCov[9], boolean_T *invalidDet);
  real_T maximum_p(const real_T x[6]);
  void cholesky(const real_T A_data[], const int32_T A_size[2], real_T b_A_data[],
                int32_T b_A_size[2], int32_T *info);
  real_T xnrm2_n(int32_T n, const real_T x[36], int32_T ix0);
  real_T xnrm2_n1aj(int32_T n, const real_T x[6], int32_T ix0);
  real_T xdotc(int32_T n, const real_T x[36], int32_T ix0, const real_T y[36],
               int32_T iy0);
  void xaxpy(int32_T n, real_T a, int32_T ix0, const real_T y[36], int32_T iy0,
             real_T b_y[36]);
  void xaxpy_f0cuq0(int32_T n, real_T a, const real_T x[36], int32_T ix0, real_T
                    y[6], int32_T iy0);
  void xrotg(real_T a, real_T b, real_T *b_a, real_T *b_b, real_T *c, real_T *s);
  void xaxpy_f0(int32_T n, real_T a, const real_T x[6], int32_T ix0, const
                real_T y[36], int32_T iy0, real_T b_y[36]);
  void xrot(const real_T x[36], int32_T ix0, int32_T iy0, real_T c, real_T s,
            real_T b_x[36]);
  void xswap(const real_T x[36], int32_T ix0, int32_T iy0, real_T b_x[36]);
  void svd(const real_T A[36], real_T U[36], real_T s[6], real_T V[36]);
  void cholPSD(const real_T A[36], real_T value[36]);
  void ExtendedKalmanFilter_set_StateCovariance(c_trackingEKF_LFRefMdl_T *obj,
    const real_T value[36]);
  real_T xnrm2_n1aj1(int32_T n, const real_T x[9], int32_T ix0);
  real_T xnrm2_n1aj1q(const real_T x[3], int32_T ix0);
  real_T xdotc_p(int32_T n, const real_T x[9], int32_T ix0, const real_T y[9],
                 int32_T iy0);
  void xaxpy_f0c(int32_T n, real_T a, int32_T ix0, const real_T y[9], int32_T
                 iy0, real_T b_y[9]);
  void xaxpy_f0cuq0u(int32_T n, real_T a, const real_T x[9], int32_T ix0, real_T
                     y[3], int32_T iy0);
  void xaxpy_f0cuq(int32_T n, real_T a, const real_T x[3], int32_T ix0, const
                   real_T y[9], int32_T iy0, real_T b_y[9]);
  void xrot_g(const real_T x[9], int32_T ix0, int32_T iy0, real_T c, real_T s,
              real_T b_x[9]);
  void xswap_n(const real_T x[9], int32_T ix0, int32_T iy0, real_T b_x[9]);
  void svd_i(const real_T A[9], real_T U[9], real_T s[3], real_T V[9]);
  void svdPSD(const real_T A[9], real_T R[9]);
  void ExtendedKalmanFilter_set_MeasurementNoise(c_trackingEKF_LFRefMdl_T *obj,
    const real_T value[36]);
  c_trackingEKF_LFRefMdl_T *initcvekf(const real_T Detection_Measurement[6],
    const real_T Detection_MeasurementNoise[36], drivingCoordinateFrameType
    Detection_MeasurementParameters_Frame, const real_T
    Detection_MeasurementParameters_OriginPosition[3], const real_T
    Detection_MeasurementParameters_Orientation[9], boolean_T
    Detection_MeasurementParameters_HasVelocity, boolean_T
    Detection_MeasurementParameters_HasElevation, c_trackingEKF_LFRefMdl_T
    *iobj_0);
  c_trackingEKF_LFRefMdl_T *ExtendedKalmanFilter_clone(const
    c_trackingEKF_LFRefMdl_T *EKF, c_trackingEKF_LFRefMdl_T *iobj_0);
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *allocateTrack
    (c_trackHistoryLogic_LFRefMdl_T *trackLogic, const real_T
     sampleDetection_Measurement[6], const real_T
     sampleDetection_MeasurementNoise[36], real_T sampleDetection_ObjectClassID,
     drivingCoordinateFrameType sampleDetection_MeasurementParameters_Frame,
     const real_T sampleDetection_MeasurementParameters_OriginPosition[3], const
     real_T sampleDetection_MeasurementParameters_Orientation[9], boolean_T
     sampleDetection_MeasurementParameters_HasVelocity, boolean_T
     sampleDetection_MeasurementParameters_HasElevation, real_T
     sampleDetection_ObjectAttributes_TargetIndex, real_T
     sampleDetection_ObjectAttributes_SNR,
     b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *iobj_0,
     c_trackingEKF_LFRefMdl_T *iobj_1);
  void ObjectTrack_nullify
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track);
  void trackingEKF_sync(c_trackingEKF_LFRefMdl_T *EKF, c_trackingEKF_LFRefMdl_T *
                        EKF2);
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T
    *ObjectTrack_copy
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *iobj_0,
     c_trackHistoryLogic_LFRefMdl_T *iobj_1, c_trackingEKF_LFRefMdl_T *iobj_2);
  void ObjectTrack_trackToStruct
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T *trackStruct);
  void TrackManager_setupImpl(multiObjectTracker_LFRefMdl_T *obj);
  void SystemCore_setup(multiObjectTracker_LFRefMdl_T *obj, real_T
                        varargin_1_NumDetections, const
                        BusDetectionConcatenation1Detections
                        varargin_1_Detections[70],
                        B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_resetImpl(multiObjectTracker_LFRefMdl_T *obj);
  void nullAssignment_mk(BusRadarDetections x_data[], int32_T *x_size, const
    int32_T idx_data[], const int32_T idx_size[2]);
  void localClusterDetections(const BusRadarDetections detections_data[], const
    int32_T *detections_size, BusRadarDetections detectionClusters_data[],
    int32_T *detectionClusters_size, B_TrackingandSensorFusion_LFRefMdl_T
    *localB);
  void helperClusterDetections_stepImpl(real_T detBusIn_NumDetections, boolean_T
    detBusIn_IsValidTime, const BusRadarDetections detBusIn_Detections[50],
    real_T *detBusOut_NumDetections, boolean_T *detBusOut_IsValidTime,
    BusRadarDetections detBusOut_Detections[50],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void DetectionConcatenation_checkUniqueSensorIndex(real_T
    varargin_1_NumDetections, const BusVisionDetections varargin_1_Detections[20],
    real_T varargin_2_NumDetections, const BusRadarDetections
    varargin_2_Detections[50]);
  real_T tagIndicies(void);
  boolean_T getLogicalIndices(void);
  void getLogicalIndices_l(boolean_T indOut[6]);
  void getLogicalIndices_l2(boolean_T indOut[36]);
  void getLogicalIndices_l2h5w(boolean_T indOut[3]);
  real_T SimulinkDetectionManager_stepImpl
    (e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj,
     real_T dets_NumDetections, const BusDetectionConcatenation1Detections
     dets_Detections[70]);
  real_T SystemCore_step
    (e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj,
     real_T varargin_1_NumDetections, const BusDetectionConcatenation1Detections
     varargin_1_Detections[70]);
  void unique_vector(const real_T a[70], real_T b_data[], int32_T b_size[2]);
  void AssignmentCostCalculator_findPairsToCost(real_T numLiveTracks, const
    int32_T detIndices_size[2], boolean_T toCalculate_data[], int32_T
    toCalculate_size[2]);
  boolean_T sortLE(const real_T v_data[], const int32_T v_size[2], int32_T idx1,
                   int32_T idx2);
  void sortrows(const real_T y_data[], const int32_T y_size[2], real_T b_y_data[],
                int32_T b_y_size[2]);
  real_T xnrm2_n1aj1qb(int32_T n, const real_T x[54], int32_T ix0);
  void xgeqrf(const real_T A[54], real_T b_A[54], real_T tau[6]);
  void qr_b(const real_T A[54], real_T Q[54], real_T R[36]);
  void predictTrackFilter(c_trackingEKF_LFRefMdl_T *filter, real_T dt);
  void get_match(const char_T str_data[], const int32_T str_size[2], char_T
                 match_data[], int32_T match_size[2], real_T *nmatched);
  void cvmeasjac(const real_T state[6], drivingCoordinateFrameType
                 varargin_1_Frame, const real_T varargin_1_OriginPosition[3],
                 const real_T varargin_1_Orientation[9], boolean_T
                 varargin_1_HasVelocity, boolean_T varargin_1_HasElevation,
                 real_T jacobian_data[], int32_T jacobian_size[2]);
  void cvmeas(const real_T state[6], drivingCoordinateFrameType varargin_1_Frame,
              const real_T varargin_1_OriginPosition[3], const real_T
              varargin_1_Orientation[9], boolean_T varargin_1_HasVelocity,
              boolean_T varargin_1_HasElevation, real_T measurement_data[],
              int32_T *measurement_size);
  void xzgetrf(const real_T A[36], real_T b_A[36], int32_T ipiv[6], int32_T
               *info);
  real_T normalizedDistance(const real_T z_data[], const int32_T *z_size, const
    real_T mu_data[], const real_T sigma[36]);
  void ObjectTrack_calcCostOneDetection
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     const real_T detection_Measurement[6], drivingCoordinateFrameType
     detection_MeasurementParameters_Frame, const real_T
     detection_MeasurementParameters_OriginPosition[3], const real_T
     detection_MeasurementParameters_Orientation[9], boolean_T
     detection_MeasurementParameters_HasVelocity, boolean_T
     detection_MeasurementParameters_HasElevation, real_T costValue_data[],
     int32_T costValue_size[2]);
  void ObjectTrack_distance
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     const BusDetectionConcatenation1Detections detections[70], const real_T
     sensorDetections_data[], const int32_T sensorDetections_size[2], real_T
     cost_data[], int32_T cost_size[2]);
  void AssignmentCostCalculator_stepImpl
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *tracks[100],
     const BusDetectionConcatenation1Detections detections[70], real_T
     numLiveTracks, const real_T detIndices_data[], const int32_T
     detIndices_size[2], real_T outCostMatrix_data[], int32_T
     outCostMatrix_size[2], B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void SystemCore_step_b
    (f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T *obj,
     b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *varargin_1
     [100], const BusDetectionConcatenation1Detections varargin_2[70], real_T
     varargin_3, const real_T varargin_4_data[], const int32_T varargin_4_size[2],
     real_T varargout_1_data[], int32_T varargout_1_size[2],
     B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_calcCostMatrix(multiObjectTracker_LFRefMdl_T *obj, const
    real_T SensorDetections_data[], const int32_T SensorDetections_size[2],
    real_T outCostMatrix[7000], B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void minimum_l(const real_T x_data[], const int32_T x_size[2], real_T ex_data[],
                 int32_T *ex_size, int32_T idx_data[], int32_T *idx_size);
  int32_T percUp_p(int32_T heap_heapList_data[], int32_T heap_indexToHeap_data[],
                   int32_T heap_currentSize, const real_T dist_data[], int32_T i);
  boolean_T augmentedShortestPath_k(const real_T matrixRep_data[], const int32_T
    matrixRep_size[2], real_T colStart, real_T rowWeight_data[], real_T
    colWeight_data[], int32_T matchCtoR_data[], int32_T matchRtoC_data[], real_T
    pairWeightR_data[], real_T newPairWeightR_data[], const int32_T
    *newPairWeightR_size, int32_T predecessorsC_data[], const int32_T
    *predecessorsC_size, real_T distancesR_data[], const int32_T
    *distancesR_size, uint8_T colorsR_data[], const int32_T *colorsR_size,
    int32_T queue_heapList_data[], const int32_T *queue_heapList_size, int32_T
    queue_indexToHeap_data[]);
  void matlabPerfectMatching(const real_T matrixRep_data[], const int32_T
    matrixRep_size[2], int32_T matchCtoR_data[], int32_T *matchCtoR_size,
    int32_T matchRtoC_data[], int32_T *matchRtoC_size, real_T rowWeight_data[],
    int32_T *rowWeight_size, real_T colWeight_data[], int32_T *colWeight_size,
    boolean_T *success);
  void perfectMatching(const real_T A_data[], const int32_T A_size[2], int32_T
                       m1_data[], int32_T *m1_size, int32_T m2_data[], int32_T
                       *m2_size);
  void eml_find(const boolean_T x_data[], const int32_T *x_size, int32_T i_data[],
                int32_T *i_size);
  void eml_matchpairs(const real_T Cost_data[], const int32_T Cost_size[2],
                      int32_T matchings_data[], int32_T matchings_size[2],
                      int32_T unassignedRows_data[], int32_T
                      *unassignedRows_size, int32_T unassignedCols_data[],
                      int32_T *unassignedCols_size,
                      B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void SystemCore_step_bk
    (c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T *obj, const
     real_T varargin_1_data[], const int32_T varargin_1_size[2], uint32_T
     varargout_1_data[], int32_T varargout_1_size[2],
     B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_associateDetectionsToTracks(multiObjectTracker_LFRefMdl_T *obj,
    uint32_T OverallAssignments_data[], int32_T OverallAssignments_size[2],
    uint32_T outOverallUnassignedTracks_data[], int32_T
    *outOverallUnassignedTracks_size, uint32_T
    outOverallUnassignedDetections_data[], int32_T
    *outOverallUnassignedDetections_size, real_T overallCostMatrix_data[],
    int32_T overallCostMatrix_size[2], B_TrackingandSensorFusion_LFRefMdl_T
    *localB);
  void TrackManager_trackIDs(const multiObjectTracker_LFRefMdl_T *obj, const
    uint32_T indices_data[], const int32_T *indices_size, uint32_T
    trackIDList_data[], int32_T *trackIDList_size);
  void GNNTracker_associate(multiObjectTracker_LFRefMdl_T *obj, uint32_T
    assigned_data[], int32_T assigned_size[2], uint32_T unassignedTrs_data[],
    int32_T *unassignedTrs_size, uint32_T unassignedDets_data[], int32_T
    *unassignedDets_size, real_T costMatrix_data[], int32_T costMatrix_size[2],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_trackDetectability(multiObjectTracker_LFRefMdl_T *obj);
  boolean_T TrackManager_initiateTrack(multiObjectTracker_LFRefMdl_T *obj,
    uint32_T newTrackID, real_T Det_Time, const real_T Det_Measurement[6], const
    real_T Det_MeasurementNoise[36], real_T Det_SensorIndex, real_T
    Det_ObjectClassID, drivingCoordinateFrameType
    Det_MeasurementParameters_Frame, const real_T
    Det_MeasurementParameters_OriginPosition[3], const real_T
    Det_MeasurementParameters_Orientation[9], boolean_T
    Det_MeasurementParameters_HasVelocity, boolean_T
    Det_MeasurementParameters_HasElevation, const
    BusRadarDetectionsObjectAttributes Det_ObjectAttributes);
  void ObjectTrack_distance_k
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     const BusDetectionConcatenation1Detections detections[70], const uint32_T
     sensorDetections_data[], const int32_T *sensorDetections_size, real_T
     cost_data[], int32_T cost_size[2]);
  real_T xnrm2_n1aj1qbx(int32_T n, const real_T x_data[], int32_T ix0);
  void xgeqrf_i(const real_T A_data[], real_T b_A_data[], int32_T b_A_size[2],
                real_T tau_data[], int32_T *tau_size);
  void qr_bd(const real_T A_data[], real_T Q_data[], int32_T Q_size[2], real_T
             R_data[], int32_T R_size[2]);
  void EKFCorrectorAdditive_getMeasurementJacobianAndCovariance(const real_T Rs
    [36], const real_T x[6], const real_T S[36], drivingCoordinateFrameType
    varargin_1_Frame, const real_T varargin_1_OriginPosition[3], const real_T
    varargin_1_Orientation[9], boolean_T varargin_1_HasVelocity, boolean_T
    varargin_1_HasElevation, real_T zEstimated_data[], int32_T *zEstimated_size,
    real_T Pxy_data[], int32_T Pxy_size[2], real_T Sy_data[], int32_T Sy_size[2],
    real_T dHdx_data[], int32_T dHdx_size[2], real_T Rsqrt[36]);
  real_T trackingEKF_likelihood(c_trackingEKF_LFRefMdl_T *EKF, const real_T z[6],
    const BusDetectionConcatenation1DetectionsMeasurementParameters *varargin_1);
  void trisolve_n(const real_T A_data[], const real_T B_data[], real_T b_B_data[],
                  int32_T b_B_size[2]);
  void trisolve_np(const real_T A_data[], const real_T B[36], real_T b_B[36]);
  real_T xnrm2_n1aj1qbxe(int32_T n, const real_T x[72], int32_T ix0);
  void xgeqrf_ii(const real_T A[72], real_T b_A[72], real_T tau[6]);
  void qr_bd0(const real_T A[72], real_T Q[72], real_T R[36]);
  void EKFCorrector_correctStateAndSqrtCovariance(const real_T x[6], const
    real_T S[36], const real_T residue[6], const real_T Pxy_data[], const
    int32_T Pxy_size[2], const real_T Sy_data[], const real_T H_data[], const
    int32_T H_size[2], const real_T Rsqrt[36], real_T b_x[6], real_T b_S[36]);
  void ExtendedKalmanFilter_correct(c_trackingEKF_LFRefMdl_T *obj, const real_T
    z[6], drivingCoordinateFrameType varargin_1_Frame, const real_T
    varargin_1_OriginPosition[3], const real_T varargin_1_Orientation[9],
    boolean_T varargin_1_HasVelocity, boolean_T varargin_1_HasElevation);
  void ObjectTrack_correct
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     const BusDetectionConcatenation1Detections *detections);
  void nullAssignment_mkj(uint32_T x_data[], int32_T *x_size, const int32_T
    idx_data[], const int32_T idx_size[2]);
  void GNNTracker_initiateTracks(multiObjectTracker_LFRefMdl_T *obj, uint32_T
    OverallUnassigned_data[], int32_T *OverallUnassigned_size, uint32_T
    initTrIDs_data[], int32_T initTrIDs_size[2],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void unique_vector_d(const uint32_T a_data[], const int32_T *a_size, uint32_T
                       b_data[], int32_T *b_size);
  void repmat(const BusDetectionConcatenation1Detections *a, const real_T
              varargin_1[2], BusDetectionConcatenation1Detections b_data[],
              int32_T b_size[2]);
  void GNNTracker_extractDetectionsForTrack(multiObjectTracker_LFRefMdl_T *obj,
    const uint32_T detectionsForCorrect_data[], const int32_T
    *detectionsForCorrect_size, BusDetectionConcatenation1Detections
    trackDetections_data[], int32_T trackDetections_size[2],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void ObjectTrack_correct_c
    (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
     const BusDetectionConcatenation1Detections detections_data[], const int32_T
     detections_size[2]);
  void GNNTracker_updateAssignedTracks(multiObjectTracker_LFRefMdl_T *obj, const
    uint32_T OverallAssignments_data[], const int32_T OverallAssignments_size[2],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_deleteOldTracks(multiObjectTracker_LFRefMdl_T *obj, const
    uint32_T UnassignedTracks_data[], const int32_T *UnassignedTracks_size,
    uint32_T delTrIDs_data[], int32_T delTrIDs_size[2]);
  void TrackManager_sendToBus(const BusMultiObjectTracker1Tracks st[100], const
    BusMultiObjectTracker1Tracks *varargin_2, real_T *tracksOutput_NumTracks,
    BusMultiObjectTracker1Tracks tracksOutput_Tracks[100]);
  void GNNTracker_getTracks(multiObjectTracker_LFRefMdl_T *obj, const boolean_T
    list[100], real_T *tracksOutput_NumTracks, BusMultiObjectTracker1Tracks
    tracksOutput_Tracks[100], B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void multiObjectTracker_formatSimulinkOutputs(multiObjectTracker_LFRefMdl_T
    *obj, real_T *varargout_1_NumTracks, BusMultiObjectTracker1Tracks
    varargout_1_Tracks[100], B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void GNNTracker_stepImpl(multiObjectTracker_LFRefMdl_T *obj, real_T
    detections_NumDetections, const BusDetectionConcatenation1Detections
    detections_Detections[70], real_T varargin_1, real_T *varargout_1_NumTracks,
    BusMultiObjectTracker1Tracks varargout_1_Tracks[100],
    B_TrackingandSensorFusion_LFRefMdl_T *localB);
  void getLogicalIndices_l2h5wg(boolean_T indOut[9]);
  void DetectionConcatenation_setScalarStructVals(const
    BusDetectionConcatenation1Detections *out, real_T in_Time, const real_T
    in_Measurement[6], const real_T in_MeasurementNoise[36], real_T
    in_SensorIndex, real_T in_ObjectClassID, real_T
    in_ObjectAttributes_TargetIndex, real_T in_ObjectAttributes_SNR, const
    BusRadarDetectionsMeasurementParameters *in_MeasurementParameters,
    BusDetectionConcatenation1Detections *b_out);
};

//-
//  These blocks were eliminated from the model due to optimizations:
//
//  Block '<S12>/DataTypeConversion_extmv' : Unused code path elimination
//  Block '<S11>/External control signal constant' : Unused code path elimination
//  Block '<S20>/Data Type Conversion22' : Unused code path elimination
//  Block '<S20>/Data Type Conversion23' : Unused code path elimination
//  Block '<S21>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S22>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S23>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S24>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S25>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S26>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S27>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S28>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S29>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S30>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S31>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S32>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S33>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S34>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S35>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S36>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S37>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S38>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S39>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S40>/Matrix Dimension Check' : Unused code path elimination
//  Block '<S41>/Vector Dimension Check' : Unused code path elimination
//  Block '<S42>/Vector Dimension Check' : Unused code path elimination
//  Block '<S43>/Vector Dimension Check' : Unused code path elimination
//  Block '<S44>/Vector Dimension Check' : Unused code path elimination
//  Block '<S45>/Vector Dimension Check' : Unused code path elimination
//  Block '<S46>/Vector Dimension Check' : Unused code path elimination
//  Block '<S47>/Vector Dimension Check' : Unused code path elimination
//  Block '<S20>/useq_scale' : Unused code path elimination
//  Block '<S20>/useq_scale1' : Unused code path elimination
//  Block '<S20>/ym_zero' : Unused code path elimination
//  Block '<S18>/m_zero' : Unused code path elimination
//  Block '<S18>/p_zero' : Unused code path elimination
//  Block '<S12>/DataTypeConversion_Vx' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_amax' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_amin' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_curvature' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_optsgn' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_umax' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_umin' : Eliminate redundant data type conversion
//  Block '<S12>/DataTypeConversion_vset' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion10' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion11' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion12' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion13' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion14' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion15' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion16' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion17' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion18' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion19' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion2' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion20' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion21' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion3' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion4' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion5' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion7' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion8' : Eliminate redundant data type conversion
//  Block '<S20>/Data Type Conversion9' : Eliminate redundant data type conversion
//  Block '<S20>/E Conversion' : Eliminate redundant data type conversion
//  Block '<S20>/F Conversion' : Eliminate redundant data type conversion
//  Block '<S20>/G Conversion' : Eliminate redundant data type conversion
//  Block '<S20>/Reshape' : Reshape block reduction
//  Block '<S20>/Reshape1' : Reshape block reduction
//  Block '<S20>/Reshape2' : Reshape block reduction
//  Block '<S20>/Reshape3' : Reshape block reduction
//  Block '<S20>/Reshape4' : Reshape block reduction
//  Block '<S20>/Reshape5' : Reshape block reduction
//  Block '<S20>/S Conversion' : Eliminate redundant data type conversion
//  Block '<S20>/mo or x Conversion' : Eliminate redundant data type conversion
//  Block '<S17>/DataTypeConversion_dmin' : Eliminate redundant data type conversion
//  Block '<S17>/DataTypeConversion_reldist' : Eliminate redundant data type conversion
//  Block '<S17>/DataTypeConversion_vrel' : Eliminate redundant data type conversion


//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'LFRefMdl'
//  '<S1>'   : 'LFRefMdl/Estimate Lane Center'
//  '<S2>'   : 'LFRefMdl/MPC Controller '
//  '<S3>'   : 'LFRefMdl/Tracking and Sensor Fusion'
//  '<S4>'   : 'LFRefMdl/Estimate Lane Center/Center from Left'
//  '<S5>'   : 'LFRefMdl/Estimate Lane Center/Center from Left and Right'
//  '<S6>'   : 'LFRefMdl/Estimate Lane Center/Center from None'
//  '<S7>'   : 'LFRefMdl/Estimate Lane Center/Center from Right'
//  '<S8>'   : 'LFRefMdl/Estimate Lane Center/ISO 8855 to SAE J670E'
//  '<S9>'   : 'LFRefMdl/Estimate Lane Center/MATLAB Function'
//  '<S10>'  : 'LFRefMdl/Estimate Lane Center/Preview curvature'
//  '<S11>'  : 'LFRefMdl/MPC Controller /Path Following Control System'
//  '<S12>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Data Processing for MPC Controller'
//  '<S13>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Longitudinal velocity must be positive'
//  '<S14>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers'
//  '<S15>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Models'
//  '<S16>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Vehicle Models'
//  '<S17>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control'
//  '<S18>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller'
//  '<S19>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Compare To Zero'
//  '<S20>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC'
//  '<S21>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check'
//  '<S22>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check A'
//  '<S23>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check B'
//  '<S24>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check C'
//  '<S25>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check D'
//  '<S26>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check DX'
//  '<S27>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check U'
//  '<S28>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check X'
//  '<S29>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check Y'
//  '<S30>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check1'
//  '<S31>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Matrix Signal Check2'
//  '<S32>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check'
//  '<S33>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check1'
//  '<S34>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check2'
//  '<S35>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check3'
//  '<S36>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check4'
//  '<S37>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check5'
//  '<S38>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check6'
//  '<S39>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check7'
//  '<S40>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Preview Signal Check8'
//  '<S41>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Scalar Signal Check'
//  '<S42>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Scalar Signal Check1'
//  '<S43>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Scalar Signal Check2'
//  '<S44>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Vector Signal Check'
//  '<S45>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Vector Signal Check1'
//  '<S46>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Vector Signal Check11'
//  '<S47>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/MPC Vector Signal Check6'
//  '<S48>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/optimizer'
//  '<S49>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Controllers/Path Following Controller with Spacing Control/Adaptive MPC Controller/MPC/optimizer/FixedHorizonOptimizer'
//  '<S50>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Models/MPC Model with Spacing Control'
//  '<S51>'  : 'LFRefMdl/MPC Controller /Path Following Control System/MPC Models/MPC Model with Spacing Control/Adaptive Model with Spacing Control'
//  '<S52>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Vehicle Models/Vehicle parametric model'
//  '<S53>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Vehicle Models/Vehicle parametric model/Vehicle combined dynamics'
//  '<S54>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Vehicle Models/Vehicle parametric model/Vehicle lateral dynamics'
//  '<S55>'  : 'LFRefMdl/MPC Controller /Path Following Control System/Vehicle Models/Vehicle parametric model/Vehicle longitudinal dynamics'
//  '<S56>'  : 'LFRefMdl/Tracking and Sensor Fusion/Find Lead Car'

#endif                                 // RTW_HEADER_LFRefMdl_h_

//
// File trailer for generated code.
//
// [EOF]
//
