//
// File: LFRefMdl_private.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_LFRefMdl_private_h_
#define RTW_HEADER_LFRefMdl_private_h_
#include "rtwtypes.h"

extern real_T rt_powd_snf(real_T u0, real_T u1);
extern real_T rt_roundd_snf(real_T u);
extern real_T rt_hypotd_snf(real_T u0, real_T u1);
extern real_T rt_remd_snf(real_T u0, real_T u1);
extern real_T rt_atan2d_snf(real_T u0, real_T u1);

#endif                                 // RTW_HEADER_LFRefMdl_private_h_

//
// File trailer for generated code.
//
// [EOF]
//
