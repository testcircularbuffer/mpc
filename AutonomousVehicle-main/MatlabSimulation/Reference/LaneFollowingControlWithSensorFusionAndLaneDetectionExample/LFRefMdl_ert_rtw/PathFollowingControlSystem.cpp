//
// File: PathFollowingControlSystem.cpp
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "PathFollowingControlSystem.h"

// Include model header file for global data
#include "LFRefMdl.h"
#include "LFRefMdl_private.h"

// Named constants for MATLAB Function: '<S48>/FixedHorizonOptimizer'
const real_T LFRefMdl_enable_value = 0.0;
const real_T LFRefMdl_nu = 2.0;
const real_T LFRefMdl_ny = 4.0;

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
void PathFollowingControllerRefMdlModelClass::
  pfcblock_utilAugmentVehicleMdlHasSpacingCtrl(const real_T Ag[16], const real_T
  Bg[8], const real_T Cg[12], real_T Vx, real_T As[49], real_T Bs[28], real_T
  Cs[28])
{
  static const int8_T b_a_0[20] = { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0 };

  static const int8_T a_0[15] = { -1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, -1
  };

  static const int8_T b[12] = { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1 };

  static const int8_T c[12] = { 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

  real_T Cm[20];
  real_T b_a[16];
  real_T a[12];
  real_T Ap[9];
  int32_T As_tmp;
  int32_T Bs_tmp;
  int32_T a_tmp;
  int32_T i;
  for (i = 0; i < 4; i++) {
    Cm[5 * i] = Cg[3 * i];
    Cm[5 * i + 1] = Cg[3 * i + 1];
    Cm[5 * i + 2] = Cg[3 * i + 2];
    Cm[5 * i + 3] = 0.0;
    Cm[5 * i + 4] = 0.0;
  }

  std::memset(&Ap[0], 0, 9U * sizeof(real_T));
  Ap[7] = Vx;
  for (i = 0; i < 4; i++) {
    for (As_tmp = 0; As_tmp < 3; As_tmp++) {
      a_tmp = As_tmp + 3 * i;
      a[a_tmp] = 0.0;
      for (Bs_tmp = 0; Bs_tmp < 5; Bs_tmp++) {
        a[a_tmp] += static_cast<real_T>(a_0[3 * Bs_tmp + As_tmp]) * Cm[5 * i +
          Bs_tmp];
      }
    }

    As_tmp = i << 2;
    As[7 * i] = Ag[As_tmp];
    As[7 * i + 1] = Ag[As_tmp + 1];
    As[7 * i + 2] = Ag[As_tmp + 2];
    As[7 * i + 3] = Ag[As_tmp + 3];
  }

  for (i = 0; i < 3; i++) {
    As_tmp = 7 * (i + 4);
    As[As_tmp] = 0.0;
    As[As_tmp + 1] = 0.0;
    As[As_tmp + 2] = 0.0;
    As[As_tmp + 3] = 0.0;
  }

  for (i = 0; i < 4; i++) {
    As[7 * i + 4] = a[3 * i];
    As[7 * i + 5] = a[3 * i + 1];
    As[7 * i + 6] = a[3 * i + 2];
  }

  for (i = 0; i < 3; i++) {
    As_tmp = 7 * (i + 4);
    As[As_tmp + 4] = 0.0;
    As[As_tmp + 5] = Ap[3 * i + 1];
    As[As_tmp + 6] = Ap[3 * i + 2];
  }

  for (i = 0; i < 2; i++) {
    As_tmp = i << 2;
    Bs[7 * i] = Bg[As_tmp];
    Bs_tmp = 7 * (i + 2);
    Bs[Bs_tmp] = 0.0;
    Bs[7 * i + 1] = Bg[As_tmp + 1];
    Bs[Bs_tmp + 1] = 0.0;
    Bs[7 * i + 2] = Bg[As_tmp + 2];
    Bs[Bs_tmp + 2] = 0.0;
    Bs[7 * i + 3] = Bg[As_tmp + 3];
    Bs[Bs_tmp + 3] = 0.0;
  }

  for (i = 0; i < 4; i++) {
    Bs[7 * i + 4] = b[3 * i];
    Bs[7 * i + 5] = b[3 * i + 1];
    Bs[7 * i + 6] = b[3 * i + 2];
    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      a_tmp = i + (As_tmp << 2);
      b_a[a_tmp] = 0.0;
      for (Bs_tmp = 0; Bs_tmp < 5; Bs_tmp++) {
        b_a[a_tmp] += static_cast<real_T>(b_a_0[(Bs_tmp << 2) + i]) * Cm[5 *
          As_tmp + Bs_tmp];
      }
    }
  }

  std::memcpy(&Cs[0], &b_a[0], sizeof(real_T) << 4U);
  for (i = 0; i < 12; i++) {
    Cs[i + 16] = c[i];
  }
}

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
int32_T PathFollowingControllerRefMdlModelClass::xgetrfs(real_T A[121], real_T
  B[121])
{
  real_T smax;
  real_T y;
  int32_T b_j;
  int32_T c;
  int32_T c_ix;
  int32_T d;
  int32_T ijA;
  int32_T info;
  int32_T ix;
  int32_T iy;
  int32_T jA;
  int8_T ipiv[11];
  int8_T ipiv_0;
  for (b_j = 0; b_j < 11; b_j++) {
    ipiv[b_j] = static_cast<int8_T>(b_j + 1);
  }

  info = 0;
  for (b_j = 0; b_j < 10; b_j++) {
    c = b_j * 12;
    jA = 0;
    ix = c;
    smax = std::abs(A[c]);
    for (iy = 2; iy <= 11 - b_j; iy++) {
      ix++;
      y = std::abs(A[ix]);
      if (y > smax) {
        jA = iy - 1;
        smax = y;
      }
    }

    if (A[c + jA] != 0.0) {
      if (jA != 0) {
        iy = b_j + jA;
        ipiv[b_j] = static_cast<int8_T>(iy + 1);
        ix = b_j;
        for (jA = 0; jA < 11; jA++) {
          smax = A[ix];
          A[ix] = A[iy];
          A[iy] = smax;
          ix += 11;
          iy += 11;
        }
      }

      jA = (c - b_j) + 11;
      for (ix = c + 1; ix < jA; ix++) {
        A[ix] /= A[c];
      }
    } else {
      info = b_j + 1;
    }

    jA = c;
    ix = c + 11;
    for (iy = 0; iy <= 9 - b_j; iy++) {
      if (A[ix] != 0.0) {
        smax = -A[ix];
        c_ix = c + 1;
        d = (jA - b_j) + 22;
        for (ijA = jA + 12; ijA < d; ijA++) {
          A[ijA] += A[c_ix] * smax;
          c_ix++;
        }
      }

      ix += 11;
      jA += 11;
    }
  }

  if ((info == 0) && (!(A[120] != 0.0))) {
    info = 11;
  }

  for (c = 0; c < 10; c++) {
    ipiv_0 = ipiv[c];
    if (c + 1 != ipiv_0) {
      for (jA = 0; jA < 11; jA++) {
        ix = 11 * jA + c;
        smax = B[ix];
        b_j = (ipiv_0 + 11 * jA) - 1;
        B[ix] = B[b_j];
        B[b_j] = smax;
      }
    }
  }

  for (jA = 0; jA < 11; jA++) {
    iy = 11 * jA;
    for (ix = 0; ix < 11; ix++) {
      c_ix = 11 * ix;
      b_j = ix + iy;
      if (B[b_j] != 0.0) {
        for (d = ix + 2; d < 12; d++) {
          c = (d + iy) - 1;
          B[c] -= A[(d + c_ix) - 1] * B[b_j];
        }
      }
    }
  }

  for (jA = 0; jA < 11; jA++) {
    iy = 11 * jA;
    for (ix = 10; ix >= 0; ix--) {
      c_ix = 11 * ix;
      b_j = ix + iy;
      smax = B[b_j];
      if (smax != 0.0) {
        B[b_j] = smax / A[ix + c_ix];
        for (d = 0; d < ix; d++) {
          c = d + iy;
          B[c] -= B[b_j] * A[d + c_ix];
        }
      }
    }
  }

  return info;
}

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
void PathFollowingControllerRefMdlModelClass::lusolve(const real_T A[121], const
  real_T B[121], real_T X[121])
{
  real_T b_A[121];
  std::memcpy(&X[0], &B[0], 121U * sizeof(real_T));
  std::memcpy(&b_A[0], &A[0], 121U * sizeof(real_T));
  xgetrfs(b_A, X);
}

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
void PathFollowingControllerRefMdlModelClass::PadeApproximantOfDegree(const
  real_T A[121], uint8_T m, real_T F[121])
{
  real_T A2[121];
  real_T A3[121];
  real_T A4[121];
  real_T A4_0[121];
  real_T U[121];
  real_T V[121];
  real_T V_0;
  real_T d;
  int32_T A2_tmp;
  int32_T F_tmp;
  int32_T e_k;
  int32_T i;
  for (e_k = 0; e_k < 11; e_k++) {
    for (F_tmp = 0; F_tmp < 11; F_tmp++) {
      A2_tmp = F_tmp + 11 * e_k;
      A2[A2_tmp] = 0.0;
      for (i = 0; i < 11; i++) {
        A2[A2_tmp] += A[11 * i + F_tmp] * A[11 * e_k + i];
      }
    }
  }

  if (m == 3) {
    std::memcpy(&U[0], &A2[0], 121U * sizeof(real_T));
    for (e_k = 0; e_k < 11; e_k++) {
      A2_tmp = 11 * e_k + e_k;
      U[A2_tmp] += 60.0;
    }

    for (e_k = 0; e_k < 11; e_k++) {
      for (F_tmp = 0; F_tmp < 11; F_tmp++) {
        A2_tmp = F_tmp + 11 * e_k;
        A4_0[A2_tmp] = 0.0;
        for (i = 0; i < 11; i++) {
          A4_0[A2_tmp] += A[11 * i + F_tmp] * U[11 * e_k + i];
        }
      }
    }

    for (e_k = 0; e_k < 121; e_k++) {
      U[e_k] = A4_0[e_k];
      V[e_k] = 12.0 * A2[e_k];
    }

    d = 120.0;
  } else {
    for (e_k = 0; e_k < 11; e_k++) {
      for (F_tmp = 0; F_tmp < 11; F_tmp++) {
        A2_tmp = F_tmp + 11 * e_k;
        A3[A2_tmp] = 0.0;
        for (i = 0; i < 11; i++) {
          A3[A2_tmp] += A2[11 * i + F_tmp] * A2[11 * e_k + i];
        }
      }
    }

    if (m == 5) {
      for (e_k = 0; e_k < 121; e_k++) {
        U[e_k] = 420.0 * A2[e_k] + A3[e_k];
      }

      for (e_k = 0; e_k < 11; e_k++) {
        A2_tmp = 11 * e_k + e_k;
        U[A2_tmp] += 15120.0;
      }

      for (e_k = 0; e_k < 11; e_k++) {
        for (F_tmp = 0; F_tmp < 11; F_tmp++) {
          A2_tmp = F_tmp + 11 * e_k;
          A4_0[A2_tmp] = 0.0;
          for (i = 0; i < 11; i++) {
            A4_0[A2_tmp] += A[11 * i + F_tmp] * U[11 * e_k + i];
          }
        }
      }

      for (e_k = 0; e_k < 121; e_k++) {
        U[e_k] = A4_0[e_k];
        V[e_k] = 30.0 * A3[e_k] + 3360.0 * A2[e_k];
      }

      d = 30240.0;
    } else {
      for (e_k = 0; e_k < 11; e_k++) {
        for (F_tmp = 0; F_tmp < 11; F_tmp++) {
          A2_tmp = F_tmp + 11 * e_k;
          A4[A2_tmp] = 0.0;
          for (i = 0; i < 11; i++) {
            A4[A2_tmp] += A3[11 * i + F_tmp] * A2[11 * e_k + i];
          }
        }
      }

      switch (m) {
       case 7:
        for (e_k = 0; e_k < 121; e_k++) {
          U[e_k] = (1512.0 * A3[e_k] + A4[e_k]) + 277200.0 * A2[e_k];
        }

        for (e_k = 0; e_k < 11; e_k++) {
          A2_tmp = 11 * e_k + e_k;
          U[A2_tmp] += 8.64864E+6;
        }

        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            A2_tmp = F_tmp + 11 * e_k;
            A4_0[A2_tmp] = 0.0;
            for (i = 0; i < 11; i++) {
              A4_0[A2_tmp] += A[11 * i + F_tmp] * U[11 * e_k + i];
            }
          }
        }

        for (e_k = 0; e_k < 121; e_k++) {
          U[e_k] = A4_0[e_k];
          V[e_k] = (56.0 * A4[e_k] + 25200.0 * A3[e_k]) + 1.99584E+6 * A2[e_k];
        }

        d = 1.729728E+7;
        break;

       case 9:
        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            A2_tmp = F_tmp + 11 * e_k;
            V[A2_tmp] = 0.0;
            for (i = 0; i < 11; i++) {
              V[A2_tmp] += A4[11 * i + F_tmp] * A2[11 * e_k + i];
            }
          }
        }

        for (e_k = 0; e_k < 121; e_k++) {
          U[e_k] = ((3960.0 * A4[e_k] + V[e_k]) + 2.16216E+6 * A3[e_k]) +
            3.027024E+8 * A2[e_k];
        }

        for (e_k = 0; e_k < 11; e_k++) {
          A2_tmp = 11 * e_k + e_k;
          U[A2_tmp] += 8.8216128E+9;
        }

        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            A2_tmp = F_tmp + 11 * e_k;
            A4_0[A2_tmp] = 0.0;
            for (i = 0; i < 11; i++) {
              A4_0[A2_tmp] += A[11 * i + F_tmp] * U[11 * e_k + i];
            }
          }
        }

        for (e_k = 0; e_k < 121; e_k++) {
          U[e_k] = A4_0[e_k];
          V[e_k] = ((90.0 * V[e_k] + 110880.0 * A4[e_k]) + 3.027024E+7 * A3[e_k])
            + 2.0756736E+9 * A2[e_k];
        }

        d = 1.76432256E+10;
        break;

       default:
        for (e_k = 0; e_k < 121; e_k++) {
          U[e_k] = (3.352212864E+10 * A4[e_k] + 1.05594705216E+13 * A3[e_k]) +
            1.1873537964288E+15 * A2[e_k];
        }

        for (e_k = 0; e_k < 11; e_k++) {
          A2_tmp = 11 * e_k + e_k;
          U[A2_tmp] += 3.238237626624E+16;
        }

        for (e_k = 0; e_k < 121; e_k++) {
          V[e_k] = (16380.0 * A3[e_k] + A4[e_k]) + 4.08408E+7 * A2[e_k];
        }

        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            d = 0.0;
            for (i = 0; i < 11; i++) {
              d += A4[11 * i + e_k] * V[11 * F_tmp + i];
            }

            A2_tmp = 11 * F_tmp + e_k;
            A4_0[A2_tmp] = U[A2_tmp] + d;
          }
        }

        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            A2_tmp = F_tmp + 11 * e_k;
            U[A2_tmp] = 0.0;
            for (i = 0; i < 11; i++) {
              U[A2_tmp] += A[11 * i + F_tmp] * A4_0[11 * e_k + i];
            }
          }
        }

        for (e_k = 0; e_k < 121; e_k++) {
          A4_0[e_k] = (182.0 * A4[e_k] + 960960.0 * A3[e_k]) + 1.32324192E+9 *
            A2[e_k];
        }

        for (e_k = 0; e_k < 11; e_k++) {
          for (F_tmp = 0; F_tmp < 11; F_tmp++) {
            d = 0.0;
            for (i = 0; i < 11; i++) {
              d += A4[11 * i + e_k] * A4_0[11 * F_tmp + i];
            }

            A2_tmp = 11 * F_tmp + e_k;
            V[A2_tmp] = ((A4[A2_tmp] * 6.704425728E+11 + d) + A3[A2_tmp] *
                         1.29060195264E+14) + A2[A2_tmp] * 7.7717703038976E+15;
          }
        }

        d = 6.476475253248E+16;
        break;
      }
    }
  }

  for (e_k = 0; e_k < 11; e_k++) {
    A2_tmp = 11 * e_k + e_k;
    V[A2_tmp] += d;
  }

  for (e_k = 0; e_k < 121; e_k++) {
    d = U[e_k];
    V_0 = V[e_k] - d;
    d *= 2.0;
    V[e_k] = V_0;
    U[e_k] = d;
  }

  lusolve(V, U, F);
  for (e_k = 0; e_k < 11; e_k++) {
    F_tmp = 11 * e_k + e_k;
    F[F_tmp]++;
  }
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T tmp;
  real_T tmp_0;
  real_T y;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = std::abs(u0);
    tmp_0 = std::abs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = 1.0;
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = std::sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > std::floor(u1))) {
      y = (rtNaN);
    } else {
      y = std::pow(u0, u1);
    }
  }

  return y;
}

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
void PathFollowingControllerRefMdlModelClass::expm(real_T A[121], real_T F[121])
{
  static const real_T theta[5] = { 0.01495585217958292, 0.253939833006323,
    0.95041789961629319, 2.097847961257068, 5.3719203511481517 };

  static const uint8_T b[5] = { 3U, 5U, 7U, 9U, 13U };

  real_T F_0[121];
  real_T b_s;
  real_T normA;
  int32_T F_tmp;
  int32_T b_i;
  int32_T b_j;
  int32_T e;
  int32_T i;
  boolean_T exitg1;
  normA = 0.0;
  b_j = 0;
  exitg1 = false;
  while ((!exitg1) && (b_j < 11)) {
    b_s = 0.0;
    for (b_i = 0; b_i < 11; b_i++) {
      b_s += std::abs(A[11 * b_j + b_i]);
    }

    if (rtIsNaN(b_s)) {
      normA = (rtNaN);
      exitg1 = true;
    } else {
      if (b_s > normA) {
        normA = b_s;
      }

      b_j++;
    }
  }

  if (normA <= 5.3719203511481517) {
    b_j = 0;
    exitg1 = false;
    while ((!exitg1) && (b_j < 5)) {
      if (normA <= theta[b_j]) {
        PadeApproximantOfDegree(A, b[b_j], F);
        exitg1 = true;
      } else {
        b_j++;
      }
    }
  } else {
    b_s = normA / 5.3719203511481517;
    if ((!rtIsInf(b_s)) && (!rtIsNaN(b_s))) {
      b_s = frexp(b_s, &e);
    } else {
      e = 0;
    }

    normA = e;
    if (b_s == 0.5) {
      normA = static_cast<real_T>(e) - 1.0;
    }

    b_s = rt_powd_snf(2.0, normA);
    for (b_j = 0; b_j < 121; b_j++) {
      A[b_j] /= b_s;
    }

    PadeApproximantOfDegree(A, 13, F);
    for (b_i = 0; b_i < static_cast<int32_T>(normA); b_i++) {
      for (b_j = 0; b_j < 11; b_j++) {
        for (e = 0; e < 11; e++) {
          F_tmp = b_j + 11 * e;
          F_0[F_tmp] = 0.0;
          for (i = 0; i < 11; i++) {
            F_0[F_tmp] += F[11 * i + b_j] * F[11 * e + i];
          }
        }
      }

      std::memcpy(&F[0], &F_0[0], 121U * sizeof(real_T));
    }
  }
}

// Function for MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
void PathFollowingControllerRefMdlModelClass::adasblocks_utilDicretizeModel(
  const real_T a[121], const real_T b[44], real_T Ts, real_T A[121], real_T B[44])
{
  real_T Ai[121];
  real_T a_0[121];
  real_T b_b[121];
  real_T h;
  int32_T Coef;
  int32_T i;
  int32_T i_0;
  int32_T k;
  if (Ts > 0.0) {
    for (k = 0; k < 121; k++) {
      a_0[k] = a[k] * Ts;
    }

    expm(a_0, A);
    h = Ts / 4.0;
    std::memset(&Ai[0], 0, 121U * sizeof(real_T));
    for (k = 0; k < 11; k++) {
      Ai[k + 11 * k] = 1.0;
    }

    for (k = 0; k < 121; k++) {
      Ai[k] += A[k];
    }

    Coef = 2;
    for (i = 0; i < 3; i++) {
      if (Coef == 2) {
        Coef = 4;
      } else {
        Coef = 2;
      }

      for (k = 0; k < 121; k++) {
        a_0[k] = static_cast<real_T>(i + 1) * a[k] * h;
      }

      expm(a_0, b_b);
      for (k = 0; k < 121; k++) {
        Ai[k] += static_cast<real_T>(Coef) * b_b[k];
      }
    }

    h /= 3.0;
    for (k = 0; k < 11; k++) {
      for (Coef = 0; Coef < 4; Coef++) {
        i = k + 11 * Coef;
        B[i] = 0.0;
        for (i_0 = 0; i_0 < 11; i_0++) {
          B[i] += Ai[11 * i_0 + k] * h * b[11 * Coef + i_0];
        }
      }
    }
  } else {
    std::memcpy(&A[0], &a[0], 121U * sizeof(real_T));
    std::memcpy(&B[0], &b[0], 44U * sizeof(real_T));
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_plantupdate(const real_T a[121],
  real_T b[44], const real_T c[44], real_T d[16], real_T b_A[169], real_T b_B
  [143], real_T b_C[52], real_T b_D[44], const real_T b_mvindex[2], const real_T
  b_mdindex[2], const real_T b_myindex[4], const real_T b_Uscale[4], const
  real_T b_Yscale[4], real_T Bu[26], real_T Bv[39], real_T Cm[52], real_T Dv[12],
  real_T Dvm[12], real_T QQ[169], real_T RR[16], real_T NN[52])
{
  real_T CovMat[289];
  real_T b_B_0[170];
  real_T b_B_1[170];
  real_T b_Uscale_0;
  int32_T CovMat_tmp;
  int32_T b_tmp;
  int32_T d_tmp;
  int32_T i;
  int8_T UnknownIn[10];
  for (i = 0; i < 4; i++) {
    for (d_tmp = 0; d_tmp < 11; d_tmp++) {
      b_tmp = 11 * i + d_tmp;
      b[b_tmp] *= b_Uscale[i];
    }
  }

  for (i = 0; i < 11; i++) {
    d_tmp = i << 2;
    b_C[d_tmp] = c[d_tmp] / b_Yscale[0];
    b_C[d_tmp + 1] = c[d_tmp + 1] / b_Yscale[1];
    b_C[d_tmp + 2] = c[d_tmp + 2] / b_Yscale[2];
    b_C[d_tmp + 3] = c[d_tmp + 3] / b_Yscale[3];
  }

  for (i = 0; i < 4; i++) {
    b_Uscale_0 = b_Uscale[i];
    d_tmp = i << 2;
    d[d_tmp] = d[d_tmp] / b_Yscale[0] * b_Uscale_0;
    d[d_tmp + 1] = d[d_tmp + 1] / b_Yscale[1] * b_Uscale_0;
    d[d_tmp + 2] = d[d_tmp + 2] / b_Yscale[2] * b_Uscale_0;
    d[d_tmp + 3] = d[d_tmp + 3] / b_Yscale[3] * b_Uscale_0;
  }

  for (i = 0; i < 11; i++) {
    std::memcpy(&b_A[i * 13], &a[i * 11], 11U * sizeof(real_T));
  }

  for (i = 0; i < 2; i++) {
    for (d_tmp = 0; d_tmp < 11; d_tmp++) {
      b_B[d_tmp + 13 * i] = b[(static_cast<int32_T>(b_mvindex[i]) - 1) * 11 +
        d_tmp];
      b_B[d_tmp + 13 * (i + 2)] = b[(static_cast<int32_T>(b_mdindex[i]) - 1) *
        11 + d_tmp];
    }

    d_tmp = (static_cast<int32_T>(b_mdindex[i]) - 1) << 2;
    b_tmp = (i + 2) << 2;
    b_D[b_tmp] = d[d_tmp];
    b_D[b_tmp + 1] = d[d_tmp + 1];
    b_D[b_tmp + 2] = d[d_tmp + 2];
    b_D[b_tmp + 3] = d[d_tmp + 3];
    std::memcpy(&Bu[i * 13], &b_B[i * 13], 13U * sizeof(real_T));
  }

  for (i = 0; i < 3; i++) {
    std::memcpy(&Bv[i * 13], &b_B[i * 13 + 26], 13U * sizeof(real_T));
  }

  for (i = 0; i < 13; i++) {
    d_tmp = i << 2;
    Cm[d_tmp] = b_C[(d_tmp + static_cast<int32_T>(b_myindex[0])) - 1];
    Cm[d_tmp + 1] = b_C[(d_tmp + static_cast<int32_T>(b_myindex[1])) - 1];
    Cm[d_tmp + 2] = b_C[(d_tmp + static_cast<int32_T>(b_myindex[2])) - 1];
    Cm[d_tmp + 3] = b_C[(d_tmp + static_cast<int32_T>(b_myindex[3])) - 1];
  }

  for (i = 0; i < 3; i++) {
    d_tmp = (i + 2) << 2;
    b_tmp = i << 2;
    Dv[b_tmp] = b_D[d_tmp];
    Dvm[b_tmp] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[0])) - 1];
    Dv[b_tmp + 1] = b_D[d_tmp + 1];
    Dvm[b_tmp + 1] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[1])) - 1];
    Dv[b_tmp + 2] = b_D[d_tmp + 2];
    Dvm[b_tmp + 2] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[2])) - 1];
    Dv[b_tmp + 3] = b_D[d_tmp + 3];
    Dvm[b_tmp + 3] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[3])) - 1];
  }

  for (i = 0; i < 10; i++) {
    UnknownIn[i] = 0;
  }

  UnknownIn[0] = 1;
  UnknownIn[1] = 2;
  UnknownIn[2] = 3;
  UnknownIn[3] = 4;
  for (i = 0; i < 6; i++) {
    UnknownIn[i + 4] = static_cast<int8_T>(i + 6);
  }

  for (i = 0; i < 10; i++) {
    for (d_tmp = 0; d_tmp < 13; d_tmp++) {
      b_B_0[d_tmp + 17 * i] = b_B[(UnknownIn[i] - 1) * 13 + d_tmp];
    }

    d_tmp = (UnknownIn[i] - 1) << 2;
    b_B_0[17 * i + 13] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[0])) - 1];
    b_B_0[17 * i + 14] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[1])) - 1];
    b_B_0[17 * i + 15] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[2])) - 1];
    b_B_0[17 * i + 16] = b_D[(d_tmp + static_cast<int32_T>(b_myindex[3])) - 1];
  }

  for (i = 0; i < 13; i++) {
    for (d_tmp = 0; d_tmp < 10; d_tmp++) {
      b_B_1[d_tmp + 10 * i] = b_B[(UnknownIn[d_tmp] - 1) * 13 + i];
    }
  }

  for (i = 0; i < 4; i++) {
    for (d_tmp = 0; d_tmp < 10; d_tmp++) {
      b_B_1[d_tmp + 10 * (i + 13)] = b_D[(((UnknownIn[d_tmp] - 1) << 2) +
        static_cast<int32_T>(b_myindex[i])) - 1];
    }
  }

  for (i = 0; i < 17; i++) {
    for (d_tmp = 0; d_tmp < 17; d_tmp++) {
      CovMat_tmp = d_tmp + 17 * i;
      CovMat[CovMat_tmp] = 0.0;
      for (b_tmp = 0; b_tmp < 10; b_tmp++) {
        CovMat[CovMat_tmp] += b_B_0[17 * b_tmp + d_tmp] * b_B_1[10 * i + b_tmp];
      }
    }
  }

  for (i = 0; i < 13; i++) {
    std::memcpy(&QQ[i * 13], &CovMat[i * 17], 13U * sizeof(real_T));
  }

  for (i = 0; i < 4; i++) {
    d_tmp = (i + 13) * 17;
    b_tmp = i << 2;
    RR[b_tmp] = CovMat[d_tmp + 13];
    RR[b_tmp + 1] = CovMat[d_tmp + 14];
    RR[b_tmp + 2] = CovMat[d_tmp + 15];
    RR[b_tmp + 3] = CovMat[d_tmp + 16];
    std::memcpy(&NN[i * 13], &CovMat[d_tmp], 13U * sizeof(real_T));
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::mod(real_T x)
{
  real_T r;
  if (rtIsNaN(x)) {
    r = (rtNaN);
  } else if (rtIsInf(x)) {
    r = (rtNaN);
  } else if (x == 0.0) {
    r = 0.0;
  } else {
    r = std::fmod(x, LFRefMdl_ny);
    if (r == 0.0) {
      r = 0.0;
    } else {
      if (x < 0.0) {
        r += LFRefMdl_ny;
      }
    }
  }

  return r;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::mod_o(real_T x)
{
  real_T r;
  if (rtIsNaN(x)) {
    r = (rtNaN);
  } else if (rtIsInf(x)) {
    r = (rtNaN);
  } else if (x == 0.0) {
    r = 0.0;
  } else {
    r = std::fmod(x, LFRefMdl_nu);
    if (r == 0.0) {
      r = 0.0;
    } else {
      if (x < 0.0) {
        r += LFRefMdl_nu;
      }
    }
  }

  return r;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_updateFromNominal(real_T
  b_Mlim[18], const real_T b_Mrows[18], const real_T U0[4], const real_T
  b_Uscale[4], const real_T old_mvoff[2], const real_T b_mvindex[2], const
  real_T b_mdindex[2], real_T b_utarget[20], const real_T Y0[4], const real_T
  b_Yscale[4], const real_T old_yoff[4], const real_T b_myindex[4], const real_T
  X0[11], real_T b_xoff[13], const real_T DX0[11], real_T Bv[429], real_T
  new_mvoff[2], real_T new_mdoff[2], real_T new_yoff[4], real_T new_myoff[4])
{
  real_T U[4];
  real_T U_0;
  real_T k;
  int32_T b_ct;
  int32_T b_utarget_tmp;
  int32_T i;
  U[0] = U0[0] / b_Uscale[0];
  new_yoff[0] = Y0[0] / b_Yscale[0];
  U[1] = U0[1] / b_Uscale[1];
  new_yoff[1] = Y0[1] / b_Yscale[1];
  U[2] = U0[2] / b_Uscale[2];
  new_yoff[2] = Y0[2] / b_Yscale[2];
  U[3] = U0[3] / b_Uscale[3];
  new_yoff[3] = Y0[3] / b_Yscale[3];
  new_mvoff[0] = U[static_cast<int32_T>(b_mvindex[0]) - 1];
  new_mdoff[0] = U[static_cast<int32_T>(b_mdindex[0]) - 1];
  new_mvoff[1] = U[static_cast<int32_T>(b_mvindex[1]) - 1];
  new_mdoff[1] = U[static_cast<int32_T>(b_mdindex[1]) - 1];
  new_myoff[0] = new_yoff[static_cast<int32_T>(b_myindex[0]) - 1];
  new_myoff[1] = new_yoff[static_cast<int32_T>(b_myindex[1]) - 1];
  new_myoff[2] = new_yoff[static_cast<int32_T>(b_myindex[2]) - 1];
  new_myoff[3] = new_yoff[static_cast<int32_T>(b_myindex[3]) - 1];
  for (b_ct = 0; b_ct < 18; b_ct++) {
    k = b_Mrows[b_ct];
    if (k <= 40.0) {
      k = mod(k - 1.0) + 1.0;
      b_Mlim[b_ct] += old_yoff[static_cast<int32_T>(k) - 1] - new_yoff[
        static_cast<int32_T>(k) - 1];
    } else if (k <= 80.0) {
      k = mod((k - 40.0) - 1.0) + 1.0;
      b_Mlim[b_ct] -= old_yoff[static_cast<int32_T>(k) - 1] - new_yoff[
        static_cast<int32_T>(k) - 1];
    } else if (k <= 100.0) {
      k = mod_o((k - 80.0) - 1.0) + 1.0;
      b_Mlim[b_ct] += old_mvoff[static_cast<int32_T>(k) - 1] - U
        [static_cast<int32_T>(b_mvindex[static_cast<int32_T>(k) - 1]) - 1];
    } else {
      if (k <= 120.0) {
        k = mod_o(((k - 80.0) - 20.0) - 1.0) + 1.0;
        b_Mlim[b_ct] -= old_mvoff[static_cast<int32_T>(k) - 1] - U
          [static_cast<int32_T>(b_mvindex[static_cast<int32_T>(k) - 1]) - 1];
      }
    }
  }

  for (b_ct = 0; b_ct < 2; b_ct++) {
    k = old_mvoff[b_ct];
    U_0 = U[static_cast<int32_T>(b_mvindex[b_ct]) - 1];
    for (i = 0; i < 10; i++) {
      b_utarget_tmp = (i << 1) + b_ct;
      b_utarget[b_utarget_tmp] = (b_utarget[b_utarget_tmp] + k) - U_0;
    }
  }

  std::memcpy(&b_xoff[0], &X0[0], 11U * sizeof(real_T));
  std::memcpy(&Bv[26], &DX0[0], 11U * sizeof(real_T));
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mrdiv(const real_T b_A[16], const
  real_T b_B[16], real_T Y[16])
{
  real_T c_A[16];
  real_T smax;
  real_T y;
  int32_T c;
  int32_T c_ix;
  int32_T c_k;
  int32_T d;
  int32_T d_j;
  int32_T ijA;
  int32_T ix;
  int32_T jA;
  int8_T ipiv[4];
  int8_T ipiv_0;
  std::memcpy(&c_A[0], &b_B[0], sizeof(real_T) << 4U);
  ipiv[0] = 1;
  ipiv[1] = 2;
  ipiv[2] = 3;
  ipiv[3] = 4;
  for (d_j = 0; d_j < 3; d_j++) {
    c = d_j * 5;
    jA = 0;
    ix = c;
    smax = std::abs(c_A[c]);
    for (c_k = 2; c_k <= 4 - d_j; c_k++) {
      ix++;
      y = std::abs(c_A[ix]);
      if (y > smax) {
        jA = c_k - 1;
        smax = y;
      }
    }

    if (c_A[c + jA] != 0.0) {
      if (jA != 0) {
        jA += d_j;
        ipiv[d_j] = static_cast<int8_T>(jA + 1);
        smax = c_A[d_j];
        c_A[d_j] = c_A[jA];
        c_A[jA] = smax;
        smax = c_A[d_j + 4];
        c_A[d_j + 4] = c_A[jA + 4];
        c_A[jA + 4] = smax;
        smax = c_A[d_j + 8];
        c_A[d_j + 8] = c_A[jA + 8];
        c_A[jA + 8] = smax;
        smax = c_A[d_j + 12];
        c_A[d_j + 12] = c_A[jA + 12];
        c_A[jA + 12] = smax;
      }

      jA = (c - d_j) + 4;
      for (ix = c + 1; ix < jA; ix++) {
        c_A[ix] /= c_A[c];
      }
    }

    jA = c;
    ix = c + 4;
    for (c_k = 0; c_k <= 2 - d_j; c_k++) {
      if (c_A[ix] != 0.0) {
        smax = -c_A[ix];
        c_ix = c + 1;
        d = (jA - d_j) + 8;
        for (ijA = jA + 5; ijA < d; ijA++) {
          c_A[ijA] += c_A[c_ix] * smax;
          c_ix++;
        }
      }

      ix += 4;
      jA += 4;
    }
  }

  std::memcpy(&Y[0], &b_A[0], sizeof(real_T) << 4U);
  for (d_j = 0; d_j < 4; d_j++) {
    c = d_j << 2;
    for (jA = 0; jA < d_j; jA++) {
      ix = jA << 2;
      smax = c_A[jA + c];
      if (smax != 0.0) {
        Y[c] -= smax * Y[ix];
        Y[c + 1] -= smax * Y[ix + 1];
        Y[c + 2] -= smax * Y[ix + 2];
        Y[c + 3] -= smax * Y[ix + 3];
      }
    }

    smax = 1.0 / c_A[d_j + c];
    Y[c] *= smax;
    Y[c + 1] *= smax;
    Y[c + 2] *= smax;
    Y[c + 3] *= smax;
  }

  for (d_j = 3; d_j >= 0; d_j--) {
    c = d_j << 2;
    for (jA = d_j + 2; jA < 5; jA++) {
      ix = (jA - 1) << 2;
      smax = c_A[(jA + c) - 1];
      if (smax != 0.0) {
        Y[c] -= smax * Y[ix];
        Y[c + 1] -= smax * Y[ix + 1];
        Y[c + 2] -= smax * Y[ix + 2];
        Y[c + 3] -= smax * Y[ix + 3];
      }
    }
  }

  for (d_j = 2; d_j >= 0; d_j--) {
    ipiv_0 = ipiv[d_j];
    if (d_j + 1 != ipiv_0) {
      c = d_j << 2;
      smax = Y[c];
      jA = (ipiv_0 - 1) << 2;
      Y[c] = Y[jA];
      Y[jA] = smax;
      smax = Y[c + 1];
      Y[c + 1] = Y[jA + 1];
      Y[jA + 1] = smax;
      smax = Y[c + 2];
      Y[c + 2] = Y[jA + 2];
      Y[jA + 2] = smax;
      smax = Y[c + 3];
      Y[c + 3] = Y[jA + 3];
      Y[jA + 3] = smax;
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_constraintcoef(const real_T
  b_A[169], const real_T Bu[26], const real_T Bv[39], const real_T b_C[52],
  const real_T Dv[12], const real_T b_Jm[80], real_T b_SuJm[160], real_T b_Sx
  [520], real_T b_Su1[80], real_T b_Hv[1320],
  B_PathFollowingControlSystem_LFRefMdl_T *localB)
{
  real_T CA_0[132];
  real_T Sum_0[80];
  real_T CA[52];
  real_T CA_1[52];
  real_T b_C_0[12];
  real_T Sum[8];
  real_T b_Su1_tmp;
  real_T b_Su1_tmp_0;
  real_T b_Su1_tmp_1;
  real_T b_Su1_tmp_3;
  int32_T CA_tmp;
  int32_T b_C_tmp;
  int32_T b_Hv_tmp;
  int32_T b_Su1_tmp_2;
  int32_T i_0;
  int8_T rows[4];
  int8_T i;
  for (i_0 = 0; i_0 < 4; i_0++) {
    for (b_Hv_tmp = 0; b_Hv_tmp < 13; b_Hv_tmp++) {
      CA_tmp = i_0 + (b_Hv_tmp << 2);
      CA[CA_tmp] = 0.0;
      for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
        CA[CA_tmp] += b_C[(b_Su1_tmp_2 << 2) + i_0] * b_A[13 * b_Hv_tmp +
          b_Su1_tmp_2];
      }
    }

    for (b_Hv_tmp = 0; b_Hv_tmp < 2; b_Hv_tmp++) {
      CA_tmp = i_0 + (b_Hv_tmp << 2);
      Sum[CA_tmp] = 0.0;
      for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
        Sum[CA_tmp] += b_C[(b_Su1_tmp_2 << 2) + i_0] * Bu[13 * b_Hv_tmp +
          b_Su1_tmp_2];
      }
    }

    for (b_Hv_tmp = 0; b_Hv_tmp < 3; b_Hv_tmp++) {
      b_C_tmp = i_0 + (b_Hv_tmp << 2);
      b_C_0[b_C_tmp] = 0.0;
      for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
        b_C_0[b_C_tmp] += b_C[(b_Su1_tmp_2 << 2) + i_0] * Bv[13 * b_Hv_tmp +
          b_Su1_tmp_2];
      }
    }
  }

  for (i_0 = 0; i_0 < 3; i_0++) {
    b_Hv_tmp = i_0 << 2;
    b_Hv[40 * i_0] = b_C_0[b_Hv_tmp];
    b_C_tmp = 40 * (i_0 + 3);
    b_Hv[b_C_tmp] = Dv[b_Hv_tmp];
    b_Hv[40 * i_0 + 1] = b_C_0[b_Hv_tmp + 1];
    b_Hv[b_C_tmp + 1] = Dv[b_Hv_tmp + 1];
    b_Hv[40 * i_0 + 2] = b_C_0[b_Hv_tmp + 2];
    b_Hv[b_C_tmp + 2] = Dv[b_Hv_tmp + 2];
    b_Hv[40 * i_0 + 3] = b_C_0[b_Hv_tmp + 3];
    b_Hv[b_C_tmp + 3] = Dv[b_Hv_tmp + 3];
  }

  for (i_0 = 0; i_0 < 27; i_0++) {
    b_Hv_tmp = 40 * (i_0 + 6);
    b_Hv[b_Hv_tmp] = 0.0;
    b_Hv[b_Hv_tmp + 1] = 0.0;
    b_Hv[b_Hv_tmp + 2] = 0.0;
    b_Hv[b_Hv_tmp + 3] = 0.0;
  }

  for (i_0 = 0; i_0 < 33; i_0++) {
    std::memset(&b_Hv[i_0 * 40 + 4], 0, 36U * sizeof(real_T));
  }

  for (i_0 = 0; i_0 < 13; i_0++) {
    b_Hv_tmp = i_0 << 2;
    b_Sx[40 * i_0] = CA[b_Hv_tmp];
    b_Sx[40 * i_0 + 1] = CA[b_Hv_tmp + 1];
    b_Sx[40 * i_0 + 2] = CA[b_Hv_tmp + 2];
    b_Sx[40 * i_0 + 3] = CA[b_Hv_tmp + 3];
    std::memset(&b_Sx[i_0 * 40 + 4], 0, 36U * sizeof(real_T));
  }

  for (i_0 = 0; i_0 < 2; i_0++) {
    b_C_tmp = i_0 << 2;
    b_Su1_tmp = Sum[b_C_tmp];
    b_Su1[40 * i_0] = b_Su1_tmp;
    b_Su1_tmp_0 = Sum[b_C_tmp + 1];
    b_Hv_tmp = 40 * i_0 + 1;
    b_Su1[b_Hv_tmp] = b_Su1_tmp_0;
    b_Su1_tmp_1 = Sum[b_C_tmp + 2];
    b_Su1_tmp_2 = 40 * i_0 + 2;
    b_Su1[b_Su1_tmp_2] = b_Su1_tmp_1;
    b_Su1_tmp_3 = Sum[b_C_tmp + 3];
    b_C_tmp = 40 * i_0 + 3;
    b_Su1[b_C_tmp] = b_Su1_tmp_3;
    std::memset(&b_Su1[i_0 * 40 + 4], 0, 36U * sizeof(real_T));
    localB->Su[40 * i_0] = b_Su1_tmp;
    localB->Su[b_Hv_tmp] = b_Su1_tmp_0;
    localB->Su[b_Su1_tmp_2] = b_Su1_tmp_1;
    localB->Su[b_C_tmp] = b_Su1_tmp_3;
  }

  for (i_0 = 0; i_0 < 18; i_0++) {
    b_Hv_tmp = 40 * (i_0 + 2);
    localB->Su[b_Hv_tmp] = 0.0;
    localB->Su[b_Hv_tmp + 1] = 0.0;
    localB->Su[b_Hv_tmp + 2] = 0.0;
    localB->Su[b_Hv_tmp + 3] = 0.0;
  }

  for (i_0 = 0; i_0 < 20; i_0++) {
    std::memset(&localB->Su[i_0 * 40 + 4], 0, 36U * sizeof(real_T));
  }

  for (b_C_tmp = 0; b_C_tmp < 9; b_C_tmp++) {
    i = static_cast<int8_T>(((b_C_tmp + 1) << 2) + 1);
    for (i_0 = 0; i_0 < 4; i_0++) {
      rows[i_0] = static_cast<int8_T>(i_0 + i);
      for (b_Hv_tmp = 0; b_Hv_tmp < 2; b_Hv_tmp++) {
        b_Su1_tmp = 0.0;
        for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
          b_Su1_tmp += CA[(b_Su1_tmp_2 << 2) + i_0] * Bu[13 * b_Hv_tmp +
            b_Su1_tmp_2];
        }

        CA_tmp = (b_Hv_tmp << 2) + i_0;
        Sum[CA_tmp] += b_Su1_tmp;
      }
    }

    for (i_0 = 0; i_0 < 2; i_0++) {
      CA_tmp = i_0 << 2;
      b_Su1_tmp = Sum[CA_tmp];
      b_Su1[(rows[0] + 40 * i_0) - 1] = b_Su1_tmp;
      Sum_0[CA_tmp] = b_Su1_tmp;
      b_Su1_tmp = Sum[CA_tmp + 1];
      b_Su1[(rows[1] + 40 * i_0) - 1] = b_Su1_tmp;
      Sum_0[CA_tmp + 1] = b_Su1_tmp;
      b_Su1_tmp = Sum[CA_tmp + 2];
      b_Su1[(rows[2] + 40 * i_0) - 1] = b_Su1_tmp;
      Sum_0[CA_tmp + 2] = b_Su1_tmp;
      b_Su1_tmp = Sum[CA_tmp + 3];
      b_Su1[(rows[3] + 40 * i_0) - 1] = b_Su1_tmp;
      Sum_0[CA_tmp + 3] = b_Su1_tmp;
    }

    for (i_0 = 0; i_0 < 18; i_0++) {
      CA_tmp = (i_0 + 2) << 2;
      Sum_0[CA_tmp] = localB->Su[(40 * i_0 + rows[0]) - 5];
      Sum_0[CA_tmp + 1] = localB->Su[(40 * i_0 + rows[1]) - 5];
      Sum_0[CA_tmp + 2] = localB->Su[(40 * i_0 + rows[2]) - 5];
      Sum_0[CA_tmp + 3] = localB->Su[(40 * i_0 + rows[3]) - 5];
    }

    for (i_0 = 0; i_0 < 20; i_0++) {
      b_Hv_tmp = i_0 << 2;
      localB->Su[(rows[0] + 40 * i_0) - 1] = Sum_0[b_Hv_tmp];
      localB->Su[(rows[1] + 40 * i_0) - 1] = Sum_0[b_Hv_tmp + 1];
      localB->Su[(rows[2] + 40 * i_0) - 1] = Sum_0[b_Hv_tmp + 2];
      localB->Su[(rows[3] + 40 * i_0) - 1] = Sum_0[b_Hv_tmp + 3];
    }

    for (i_0 = 0; i_0 < 4; i_0++) {
      for (b_Hv_tmp = 0; b_Hv_tmp < 3; b_Hv_tmp++) {
        CA_tmp = i_0 + (b_Hv_tmp << 2);
        b_C_0[CA_tmp] = 0.0;
        for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
          b_C_0[CA_tmp] += CA[(b_Su1_tmp_2 << 2) + i_0] * Bv[13 * b_Hv_tmp +
            b_Su1_tmp_2];
        }
      }
    }

    for (i_0 = 0; i_0 < 3; i_0++) {
      CA_tmp = i_0 << 2;
      CA_0[CA_tmp] = b_C_0[CA_tmp];
      CA_0[CA_tmp + 1] = b_C_0[CA_tmp + 1];
      CA_0[CA_tmp + 2] = b_C_0[CA_tmp + 2];
      CA_0[CA_tmp + 3] = b_C_0[CA_tmp + 3];
    }

    for (i_0 = 0; i_0 < 30; i_0++) {
      CA_tmp = (i_0 + 3) << 2;
      CA_0[CA_tmp] = b_Hv[(40 * i_0 + rows[0]) - 5];
      CA_0[CA_tmp + 1] = b_Hv[(40 * i_0 + rows[1]) - 5];
      CA_0[CA_tmp + 2] = b_Hv[(40 * i_0 + rows[2]) - 5];
      CA_0[CA_tmp + 3] = b_Hv[(40 * i_0 + rows[3]) - 5];
    }

    for (i_0 = 0; i_0 < 33; i_0++) {
      b_Hv_tmp = i_0 << 2;
      b_Hv[(rows[0] + 40 * i_0) - 1] = CA_0[b_Hv_tmp];
      b_Hv[(rows[1] + 40 * i_0) - 1] = CA_0[b_Hv_tmp + 1];
      b_Hv[(rows[2] + 40 * i_0) - 1] = CA_0[b_Hv_tmp + 2];
      b_Hv[(rows[3] + 40 * i_0) - 1] = CA_0[b_Hv_tmp + 3];
    }

    for (i_0 = 0; i_0 < 4; i_0++) {
      for (b_Hv_tmp = 0; b_Hv_tmp < 13; b_Hv_tmp++) {
        CA_tmp = i_0 + (b_Hv_tmp << 2);
        CA_1[CA_tmp] = 0.0;
        for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 13; b_Su1_tmp_2++) {
          CA_1[CA_tmp] += CA[(b_Su1_tmp_2 << 2) + i_0] * b_A[13 * b_Hv_tmp +
            b_Su1_tmp_2];
        }
      }
    }

    std::memcpy(&CA[0], &CA_1[0], 52U * sizeof(real_T));
    for (i_0 = 0; i_0 < 13; i_0++) {
      b_Hv_tmp = i_0 << 2;
      b_Sx[(rows[0] + 40 * i_0) - 1] = CA[b_Hv_tmp];
      b_Sx[(rows[1] + 40 * i_0) - 1] = CA[b_Hv_tmp + 1];
      b_Sx[(rows[2] + 40 * i_0) - 1] = CA[b_Hv_tmp + 2];
      b_Sx[(rows[3] + 40 * i_0) - 1] = CA[b_Hv_tmp + 3];
    }
  }

  for (i_0 = 0; i_0 < 4; i_0++) {
    for (b_Hv_tmp = 0; b_Hv_tmp < 40; b_Hv_tmp++) {
      b_C_tmp = b_Hv_tmp + 40 * i_0;
      b_SuJm[b_C_tmp] = 0.0;
      for (b_Su1_tmp_2 = 0; b_Su1_tmp_2 < 20; b_Su1_tmp_2++) {
        b_SuJm[b_C_tmp] += localB->Su[40 * b_Su1_tmp_2 + b_Hv_tmp] * b_Jm[20 *
          i_0 + b_Su1_tmp_2];
      }
    }
  }
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (std::abs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = std::floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = std::ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::WtMult(const real_T W[2], const
  real_T M[80], real_T nwt, real_T WM[80])
{
  real_T W_0;
  int32_T i;
  int32_T tmp;
  int16_T ixw;
  ixw = 1;
  for (i = 0; i < 20; i++) {
    W_0 = W[ixw - 1];
    WM[i] = W_0 * M[i];
    WM[i + 20] = M[i + 20] * W_0;
    WM[i + 40] = M[i + 40] * W_0;
    WM[i + 60] = M[i + 60] * W_0;
    tmp = ixw + 1;
    if (ixw + 1 > 32767) {
      tmp = 32767;
    }

    ixw = static_cast<int16_T>(tmp);
    if (static_cast<int16_T>(tmp) > 2) {
      W_0 = rt_roundd_snf(2.0 - nwt);
      if (W_0 < 32768.0) {
        if (W_0 >= -32768.0) {
          ixw = static_cast<int16_T>(W_0);
        } else {
          ixw = MIN_int16_T;
        }
      } else {
        ixw = MAX_int16_T;
      }

      tmp = ixw + 1;
      if (W_0 < 32768.0) {
        if (W_0 >= -32768.0) {
          ixw = static_cast<int16_T>(W_0);
        } else {
          ixw = MIN_int16_T;
        }
      } else {
        ixw = MAX_int16_T;
      }

      if (ixw + 1 > 32767) {
        tmp = 32767;
      }

      ixw = static_cast<int16_T>(tmp);
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_calculatehessian(const real_T
  b_Wy[4], const real_T b_Wu[2], const real_T b_Wdu[2], const real_T b_SuJm[160],
  const real_T I2Jm[80], const real_T b_Jm[80], const real_T b_I1[40], const
  real_T b_Su1[80], const real_T b_Sx[520], const real_T b_Hv[1320], real_T nmv,
  real_T b_ny, real_T b_H[16], real_T b_Ku1[8], real_T b_Kut[80], real_T b_Kx[52],
  real_T b_Kv[132], real_T b_Kr[160])
{
  real_T tmp[80];
  real_T b_Jm_0[16];
  real_T b_SuJm_0[16];
  real_T b_I1_0[8];
  real_T b_Su1_0[8];
  real_T b_Wy_0;
  int32_T b_SuJm_tmp;
  int32_T i;
  int32_T i_0;
  int32_T i_1;
  int32_T tmp_0;
  int32_T tmp_1;
  int16_T ixw;
  ixw = 1;
  for (i = 0; i < 40; i++) {
    b_Wy_0 = b_Wy[ixw - 1];
    b_Kr[i] = b_Wy_0 * b_SuJm[i];
    b_Kr[i + 40] = b_SuJm[i + 40] * b_Wy_0;
    b_Kr[i + 80] = b_SuJm[i + 80] * b_Wy_0;
    b_Kr[i + 120] = b_SuJm[i + 120] * b_Wy_0;
    i_1 = ixw + 1;
    if (ixw + 1 > 32767) {
      i_1 = 32767;
    }

    ixw = static_cast<int16_T>(i_1);
    if (static_cast<int16_T>(i_1) > 4) {
      b_Wy_0 = rt_roundd_snf(4.0 - b_ny);
      if (b_Wy_0 < 32768.0) {
        if (b_Wy_0 >= -32768.0) {
          ixw = static_cast<int16_T>(b_Wy_0);
        } else {
          ixw = MIN_int16_T;
        }
      } else {
        ixw = MAX_int16_T;
      }

      i_1 = ixw + 1;
      if (b_Wy_0 < 32768.0) {
        if (b_Wy_0 >= -32768.0) {
          ixw = static_cast<int16_T>(b_Wy_0);
        } else {
          ixw = MIN_int16_T;
        }
      } else {
        ixw = MAX_int16_T;
      }

      if (ixw + 1 > 32767) {
        i_1 = 32767;
      }

      ixw = static_cast<int16_T>(i_1);
    }
  }

  WtMult(b_Wu, I2Jm, nmv, b_Kut);
  WtMult(b_Wdu, b_Jm, nmv, tmp);
  for (i_1 = 0; i_1 < 4; i_1++) {
    for (i = 0; i < 4; i++) {
      b_SuJm_tmp = i_1 + (i << 2);
      b_SuJm_0[b_SuJm_tmp] = 0.0;
      for (i_0 = 0; i_0 < 40; i_0++) {
        b_SuJm_0[b_SuJm_tmp] += b_SuJm[40 * i_1 + i_0] * b_Kr[40 * i + i_0];
      }

      b_Jm_0[b_SuJm_tmp] = 0.0;
      b_Wy_0 = 0.0;
      for (i_0 = 0; i_0 < 20; i_0++) {
        tmp_0 = 20 * i_1 + i_0;
        tmp_1 = 20 * i + i_0;
        b_Wy_0 += I2Jm[tmp_0] * b_Kut[tmp_1];
        b_Jm_0[b_SuJm_tmp] += b_Jm[tmp_0] * tmp[tmp_1];
      }

      b_H[b_SuJm_tmp] = (b_SuJm_0[b_SuJm_tmp] + b_Jm_0[b_SuJm_tmp]) + b_Wy_0;
    }
  }

  for (i_1 = 0; i_1 < 2; i_1++) {
    for (i = 0; i < 4; i++) {
      b_SuJm_tmp = i_1 + (i << 1);
      b_Su1_0[b_SuJm_tmp] = 0.0;
      for (i_0 = 0; i_0 < 40; i_0++) {
        b_Su1_0[b_SuJm_tmp] += b_Su1[40 * i_1 + i_0] * b_Kr[40 * i + i_0];
      }

      b_I1_0[b_SuJm_tmp] = 0.0;
      for (i_0 = 0; i_0 < 20; i_0++) {
        b_I1_0[b_SuJm_tmp] += b_I1[20 * i_1 + i_0] * b_Kut[20 * i + i_0];
      }
    }
  }

  for (i_1 = 0; i_1 < 8; i_1++) {
    b_Ku1[i_1] = b_Su1_0[i_1] + b_I1_0[i_1];
  }

  for (i_1 = 0; i_1 < 80; i_1++) {
    b_Kut[i_1] = -b_Kut[i_1];
  }

  for (i_1 = 0; i_1 < 13; i_1++) {
    for (i = 0; i < 4; i++) {
      b_SuJm_tmp = i_1 + 13 * i;
      b_Kx[b_SuJm_tmp] = 0.0;
      for (i_0 = 0; i_0 < 40; i_0++) {
        b_Kx[b_SuJm_tmp] += b_Sx[40 * i_1 + i_0] * b_Kr[40 * i + i_0];
      }
    }
  }

  for (i_1 = 0; i_1 < 33; i_1++) {
    for (i = 0; i < 4; i++) {
      b_SuJm_tmp = i_1 + 33 * i;
      b_Kv[b_SuJm_tmp] = 0.0;
      for (i_0 = 0; i_0 < 40; i_0++) {
        b_Kv[b_SuJm_tmp] += b_Hv[40 * i_1 + i_0] * b_Kr[40 * i + i_0];
      }
    }
  }

  for (i_1 = 0; i_1 < 160; i_1++) {
    b_Kr[i_1] = -b_Kr[i_1];
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
int32_T PathFollowingControllerRefMdlModelClass::xpotrf(real_T b_A[25])
{
  real_T c;
  real_T ssq;
  int32_T b_ix;
  int32_T b_iy;
  int32_T b_k;
  int32_T d;
  int32_T ia;
  int32_T idxAjj;
  int32_T info;
  int32_T iy;
  int32_T j;
  boolean_T exitg1;
  info = 0;
  j = 0;
  exitg1 = false;
  while ((!exitg1) && (j < 5)) {
    idxAjj = j * 5 + j;
    ssq = 0.0;
    if (j >= 1) {
      b_ix = j;
      b_iy = j;
      for (b_k = 0; b_k < j; b_k++) {
        ssq += b_A[b_ix] * b_A[b_iy];
        b_ix += 5;
        b_iy += 5;
      }
    }

    ssq = b_A[idxAjj] - ssq;
    if (ssq > 0.0) {
      ssq = std::sqrt(ssq);
      b_A[idxAjj] = ssq;
      if (j + 1 < 5) {
        if (j != 0) {
          b_ix = j;
          b_iy = ((j - 1) * 5 + j) + 2;
          for (b_k = j + 2; b_k <= b_iy; b_k += 5) {
            c = -b_A[b_ix];
            iy = idxAjj + 1;
            d = (b_k - j) + 3;
            for (ia = b_k; ia <= d; ia++) {
              b_A[iy] += b_A[ia - 1] * c;
              iy++;
            }

            b_ix += 5;
          }
        }

        ssq = 1.0 / ssq;
        b_ix = (idxAjj - j) + 5;
        for (idxAjj++; idxAjj < b_ix; idxAjj++) {
          b_A[idxAjj] *= ssq;
        }
      }

      j++;
    } else {
      b_A[idxAjj] = ssq;
      info = j + 1;
      exitg1 = true;
    }
  }

  return info;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::minimum(const real_T x[5])
{
  real_T ex;
  int32_T idx;
  int32_T k;
  boolean_T exitg1;
  if (!rtIsNaN(x[0])) {
    idx = 1;
  } else {
    idx = 0;
    k = 2;
    exitg1 = false;
    while ((!exitg1) && (k < 6)) {
      if (!rtIsNaN(x[k - 1])) {
        idx = k;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  if (idx == 0) {
    ex = x[0];
  } else {
    ex = x[idx - 1];
    while (idx + 1 <= 5) {
      if (ex > x[idx]) {
        ex = x[idx];
      }

      idx++;
    }
  }

  return ex;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_checkhessian(real_T b_H[25],
  real_T L[25], real_T *BadH)
{
  real_T varargin_1[5];
  real_T normH;
  real_T s;
  int32_T Tries;
  int32_T j;
  int8_T b[25];
  boolean_T exitg1;
  boolean_T exitg2;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  *BadH = 0.0;
  std::memcpy(&L[0], &b_H[0], 25U * sizeof(real_T));
  Tries = xpotrf(L);
  guard1 = false;
  if (Tries == 0) {
    for (Tries = 0; Tries < 5; Tries++) {
      varargin_1[Tries] = L[5 * Tries + Tries];
    }

    if (minimum(varargin_1) > 1.4901161193847656E-7) {
    } else {
      guard1 = true;
    }
  } else {
    guard1 = true;
  }

  if (guard1) {
    normH = 0.0;
    Tries = 0;
    exitg2 = false;
    while ((!exitg2) && (Tries < 5)) {
      s = 0.0;
      for (j = 0; j < 5; j++) {
        s += std::abs(b_H[5 * j + Tries]);
      }

      if (rtIsNaN(s)) {
        normH = (rtNaN);
        exitg2 = true;
      } else {
        if (s > normH) {
          normH = s;
        }

        Tries++;
      }
    }

    if (normH >= 1.0E+10) {
      *BadH = 2.0;
    } else {
      Tries = 0;
      exitg1 = false;
      while ((!exitg1) && (Tries <= 4)) {
        normH = rt_powd_snf(10.0, static_cast<real_T>(Tries)) *
          1.4901161193847656E-7;
        for (j = 0; j < 25; j++) {
          b[j] = 0;
        }

        for (j = 0; j < 5; j++) {
          b[j + 5 * j] = 1;
        }

        for (j = 0; j < 25; j++) {
          s = normH * static_cast<real_T>(b[j]) + b_H[j];
          L[j] = s;
          b_H[j] = s;
        }

        j = xpotrf(L);
        guard2 = false;
        if (j == 0) {
          for (j = 0; j < 5; j++) {
            varargin_1[j] = L[5 * j + j];
          }

          if (minimum(varargin_1) > 1.4901161193847656E-7) {
            *BadH = 1.0;
            exitg1 = true;
          } else {
            guard2 = true;
          }
        } else {
          guard2 = true;
        }

        if (guard2) {
          *BadH = 3.0;
          Tries++;
        }
      }
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::trisolve(const real_T b_A[25],
  real_T b_B[25])
{
  real_T tmp_0;
  int32_T i;
  int32_T j;
  int32_T jBcol;
  int32_T k;
  int32_T kAcol;
  int32_T tmp;
  int32_T tmp_1;
  for (j = 0; j < 5; j++) {
    jBcol = 5 * j;
    for (k = 0; k < 5; k++) {
      kAcol = 5 * k;
      tmp = k + jBcol;
      tmp_0 = b_B[tmp];
      if (tmp_0 != 0.0) {
        b_B[tmp] = tmp_0 / b_A[k + kAcol];
        for (i = k + 2; i < 6; i++) {
          tmp_1 = (i + jBcol) - 1;
          b_B[tmp_1] -= b_A[(i + kAcol) - 1] * b_B[tmp];
        }
      }
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::Unconstrained(const real_T b_Hinv
  [25], const real_T f[5], real_T x[5], int16_T n)
{
  real_T b_Hinv_0;
  int32_T i;
  int32_T i_0;
  for (i = 1; i - 1 < n; i++) {
    b_Hinv_0 = 0.0;
    for (i_0 = 0; i_0 < 5; i_0++) {
      b_Hinv_0 += -b_Hinv[(5 * i_0 + static_cast<int16_T>(i)) - 1] * f[i_0];
    }

    x[static_cast<int16_T>(i) - 1] = b_Hinv_0;
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::norm(const real_T x[5])
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = 0; k < 5; k++) {
    absxk = std::abs(x[k]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::xnrm2(int32_T n, const real_T x
  [25], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  if (n >= 1) {
    if (n == 1) {
      y = std::abs(x[ix0 - 1]);
    } else {
      scale = 3.3121686421112381E-170;
      kend = (ix0 + n) - 1;
      for (k = ix0; k <= kend; k++) {
        absxk = std::abs(x[k - 1]);
        if (absxk > scale) {
          t = scale / absxk;
          y = y * t * t + 1.0;
          scale = absxk;
        } else {
          t = absxk / scale;
          y += t * t;
        }
      }

      y = scale * std::sqrt(y);
    }
  }

  return y;
}

real_T rt_hypotd_snf(real_T u0, real_T u1)
{
  real_T a;
  real_T y;
  a = std::abs(u0);
  y = std::abs(u1);
  if (a < y) {
    a /= y;
    y *= std::sqrt(a * a + 1.0);
  } else if (a > y) {
    y /= a;
    y = std::sqrt(y * y + 1.0) * a;
  } else {
    if (!rtIsNaN(y)) {
      y = a * 1.4142135623730951;
    }
  }

  return y;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::xgemv(int32_T b_m, int32_T n,
  const real_T b_A[25], int32_T ia0, const real_T x[25], int32_T ix0, real_T y[5])
{
  real_T c;
  int32_T b;
  int32_T b_iy;
  int32_T d;
  int32_T ia;
  int32_T iac;
  int32_T ix;
  if ((b_m != 0) && (n != 0)) {
    for (b_iy = 0; b_iy < n; b_iy++) {
      y[b_iy] = 0.0;
    }

    b_iy = 0;
    b = (n - 1) * 5 + ia0;
    for (iac = ia0; iac <= b; iac += 5) {
      ix = ix0;
      c = 0.0;
      d = (iac + b_m) - 1;
      for (ia = iac; ia <= d; ia++) {
        c += b_A[ia - 1] * x[ix - 1];
        ix++;
      }

      y[b_iy] += c;
      b_iy++;
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::xgerc(int32_T b_m, int32_T n,
  real_T alpha1, int32_T ix0, const real_T y[5], real_T b_A[25], int32_T ia0)
{
  real_T temp;
  int32_T b;
  int32_T ijA;
  int32_T ix;
  int32_T j;
  int32_T jA;
  int32_T jy;
  if (!(alpha1 == 0.0)) {
    jA = ia0 - 1;
    jy = 0;
    for (j = 0; j < n; j++) {
      if (y[jy] != 0.0) {
        temp = y[jy] * alpha1;
        ix = ix0;
        b = b_m + jA;
        for (ijA = jA; ijA < b; ijA++) {
          b_A[ijA] += b_A[ix - 1] * temp;
          ix++;
        }
      }

      jy++;
      jA += 5;
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::qr(const real_T b_A[25], real_T Q
  [25], real_T R[25])
{
  real_T c_A[25];
  real_T tau[5];
  real_T work[5];
  real_T b_atmp;
  real_T beta1;
  int32_T b_coltop;
  int32_T c_lastc;
  int32_T coltop;
  int32_T exitg1;
  int32_T i;
  int32_T ia;
  int32_T ii;
  int32_T itau;
  boolean_T exitg2;
  for (i = 0; i < 5; i++) {
    tau[i] = 0.0;
  }

  std::memcpy(&c_A[0], &b_A[0], 25U * sizeof(real_T));
  for (i = 0; i < 5; i++) {
    work[i] = 0.0;
  }

  for (itau = 0; itau < 5; itau++) {
    ii = itau * 5 + itau;
    if (itau + 1 < 5) {
      b_atmp = c_A[ii];
      tau[itau] = 0.0;
      beta1 = xnrm2(4 - itau, c_A, ii + 2);
      if (beta1 != 0.0) {
        beta1 = rt_hypotd_snf(c_A[ii], beta1);
        if (c_A[ii] >= 0.0) {
          beta1 = -beta1;
        }

        if (std::abs(beta1) < 1.0020841800044864E-292) {
          i = -1;
          c_lastc = ii - itau;
          do {
            i++;
            for (b_coltop = ii + 1; b_coltop < c_lastc + 5; b_coltop++) {
              c_A[b_coltop] *= 9.9792015476736E+291;
            }

            beta1 *= 9.9792015476736E+291;
            b_atmp *= 9.9792015476736E+291;
          } while (!(std::abs(beta1) >= 1.0020841800044864E-292));

          beta1 = rt_hypotd_snf(b_atmp, xnrm2(4 - itau, c_A, ii + 2));
          if (b_atmp >= 0.0) {
            beta1 = -beta1;
          }

          tau[itau] = (beta1 - b_atmp) / beta1;
          b_atmp = 1.0 / (b_atmp - beta1);
          for (b_coltop = ii + 1; b_coltop < c_lastc + 5; b_coltop++) {
            c_A[b_coltop] *= b_atmp;
          }

          for (c_lastc = 0; c_lastc <= i; c_lastc++) {
            beta1 *= 1.0020841800044864E-292;
          }

          b_atmp = beta1;
        } else {
          tau[itau] = (beta1 - c_A[ii]) / beta1;
          b_atmp = 1.0 / (c_A[ii] - beta1);
          i = ii - itau;
          for (c_lastc = ii + 1; c_lastc < i + 5; c_lastc++) {
            c_A[c_lastc] *= b_atmp;
          }

          b_atmp = beta1;
        }
      }

      c_A[ii] = b_atmp;
      b_atmp = c_A[ii];
      c_A[ii] = 1.0;
      if (tau[itau] != 0.0) {
        i = 5 - itau;
        c_lastc = ii - itau;
        while ((i > 0) && (c_A[c_lastc + 4] == 0.0)) {
          i--;
          c_lastc--;
        }

        c_lastc = 4 - itau;
        exitg2 = false;
        while ((!exitg2) && (c_lastc > 0)) {
          b_coltop = ((c_lastc - 1) * 5 + ii) + 5;
          coltop = b_coltop;
          do {
            exitg1 = 0;
            if (coltop + 1 <= b_coltop + i) {
              if (c_A[coltop] != 0.0) {
                exitg1 = 1;
              } else {
                coltop++;
              }
            } else {
              c_lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        i = 0;
        c_lastc = 0;
      }

      if (i > 0) {
        xgemv(i, c_lastc, c_A, ii + 6, c_A, ii + 1, work);
        xgerc(i, c_lastc, -tau[itau], ii + 1, work, c_A, ii + 6);
      }

      c_A[ii] = b_atmp;
    } else {
      tau[4] = 0.0;
    }
  }

  itau = 4;
  for (ii = 0; ii < 5; ii++) {
    for (i = 0; i <= ii; i++) {
      R[i + 5 * ii] = c_A[5 * ii + i];
    }

    for (i = ii + 1; i + 1 < 6; i++) {
      R[i + 5 * ii] = 0.0;
    }

    work[ii] = 0.0;
  }

  for (ii = 4; ii >= 0; ii--) {
    i = (ii * 5 + ii) + 6;
    if (ii + 1 < 5) {
      c_A[i - 6] = 1.0;
      if (tau[itau] != 0.0) {
        c_lastc = 5 - ii;
        b_coltop = i - ii;
        while ((c_lastc > 0) && (c_A[b_coltop - 2] == 0.0)) {
          c_lastc--;
          b_coltop--;
        }

        b_coltop = 4 - ii;
        exitg2 = false;
        while ((!exitg2) && (b_coltop > 0)) {
          coltop = (b_coltop - 1) * 5 + i;
          ia = coltop;
          do {
            exitg1 = 0;
            if (ia <= (coltop + c_lastc) - 1) {
              if (c_A[ia - 1] != 0.0) {
                exitg1 = 1;
              } else {
                ia++;
              }
            } else {
              b_coltop--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }
      } else {
        c_lastc = 0;
        b_coltop = 0;
      }

      if (c_lastc > 0) {
        xgemv(c_lastc, b_coltop, c_A, i, c_A, i - 5, work);
        xgerc(c_lastc, b_coltop, -tau[itau], i - 5, work, c_A, i);
      }

      c_lastc = (i - ii) - 1;
      for (b_coltop = i - 5; b_coltop < c_lastc; b_coltop++) {
        c_A[b_coltop] *= -tau[itau];
      }
    }

    c_A[i - 6] = 1.0 - tau[itau];
    for (c_lastc = 0; c_lastc < ii; c_lastc++) {
      c_A[(i - c_lastc) - 7] = 0.0;
    }

    itau--;
  }

  for (itau = 0; itau < 5; itau++) {
    for (ii = 0; ii < 5; ii++) {
      Q[ii + 5 * itau] = c_A[5 * itau + ii];
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::KWIKfactor(const real_T b_Ac[90],
  const int16_T iC[18], int16_T nA, const real_T b_Linv[25], real_T RLinv[25],
  real_T b_D[25], real_T b_H[25], int16_T n)
{
  real_T QQ[25];
  real_T RR[25];
  real_T TL[25];
  real_T Status;
  real_T b_Linv_0;
  int32_T b_i;
  int32_T c_i;
  int32_T e_i;
  int32_T exitg1;
  int32_T i;
  int32_T iC_0;
  int16_T b_j;
  int16_T c_k;
  Status = 1.0;
  std::memset(&RLinv[0], 0, 25U * sizeof(real_T));
  for (i = 1; i - 1 < nA; i++) {
    iC_0 = iC[static_cast<int16_T>(i) - 1];
    for (b_i = 0; b_i < 5; b_i++) {
      c_i = b_i + 5 * (static_cast<int16_T>(i) - 1);
      RLinv[c_i] = 0.0;
      for (e_i = 0; e_i < 5; e_i++) {
        RLinv[c_i] += b_Ac[(18 * e_i + iC_0) - 1] * b_Linv[5 * e_i + b_i];
      }
    }
  }

  qr(RLinv, QQ, RR);
  b_i = 1;
  do {
    exitg1 = 0;
    if (b_i - 1 <= nA - 1) {
      if (std::abs(RR[((static_cast<int16_T>(b_i) - 1) * 5 + static_cast<int16_T>
                       (b_i)) - 1]) < 1.0E-12) {
        Status = -2.0;
        exitg1 = 1;
      } else {
        b_i++;
      }
    } else {
      for (c_i = 1; c_i - 1 < n; c_i++) {
        for (e_i = 1; e_i - 1 < n; e_i++) {
          b_Linv_0 = 0.0;
          for (b_i = 0; b_i < 5; b_i++) {
            b_Linv_0 += b_Linv[(static_cast<int16_T>(c_i) - 1) * 5 + b_i] * QQ[(
              static_cast<int16_T>(e_i) - 1) * 5 + b_i];
          }

          TL[(static_cast<int16_T>(c_i) + 5 * (static_cast<int16_T>(e_i) - 1)) -
            1] = b_Linv_0;
        }
      }

      std::memset(&RLinv[0], 0, 25U * sizeof(real_T));
      for (b_j = nA; b_j > 0; b_j = static_cast<int16_T>(b_j - 1)) {
        b_i = 5 * (b_j - 1);
        c_i = (b_j + b_i) - 1;
        RLinv[c_i] = 1.0;
        for (c_k = b_j; c_k <= nA; c_k = static_cast<int16_T>(c_k + 1)) {
          e_i = ((c_k - 1) * 5 + b_j) - 1;
          RLinv[e_i] /= RR[c_i];
        }

        if (b_j > 1) {
          for (i = 1; i - 1 <= b_j - 2; i++) {
            for (c_k = b_j; c_k <= nA; c_k = static_cast<int16_T>(c_k + 1)) {
              c_i = (c_k - 1) * 5;
              e_i = (c_i + static_cast<int16_T>(i)) - 1;
              RLinv[e_i] -= RR[(b_i + static_cast<int16_T>(i)) - 1] * RLinv[(c_i
                + b_j) - 1];
            }
          }
        }
      }

      for (e_i = 1; e_i - 1 < n; e_i++) {
        for (b_j = static_cast<int16_T>(e_i); b_j <= n; b_j =
             static_cast<int16_T>(b_j + 1)) {
          b_i = (static_cast<int16_T>(e_i) + 5 * (b_j - 1)) - 1;
          b_H[b_i] = 0.0;
          c_i = nA + 1;
          if (nA + 1 > 32767) {
            c_i = 32767;
          }

          for (c_k = static_cast<int16_T>(c_i); c_k <= n; c_k =
               static_cast<int16_T>(c_k + 1)) {
            c_i = (c_k - 1) * 5;
            b_H[b_i] -= TL[(c_i + static_cast<int16_T>(e_i)) - 1] * TL[(c_i +
              b_j) - 1];
          }

          b_H[(b_j + 5 * (static_cast<int16_T>(e_i) - 1)) - 1] = b_H[b_i];
        }
      }

      for (e_i = 1; e_i - 1 < nA; e_i++) {
        for (i = 1; i - 1 < n; i++) {
          b_i = (static_cast<int16_T>(i) + 5 * (static_cast<int16_T>(e_i) - 1))
            - 1;
          b_D[b_i] = 0.0;
          for (b_j = static_cast<int16_T>(e_i); b_j <= nA; b_j =
               static_cast<int16_T>(b_j + 1)) {
            c_i = (b_j - 1) * 5;
            b_D[b_i] += TL[(c_i + static_cast<int16_T>(i)) - 1] * RLinv[(c_i +
              static_cast<int16_T>(e_i)) - 1];
          }
        }
      }

      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return Status;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::abs_a(const real_T x[5], real_T y
  [5])
{
  int32_T k;
  for (k = 0; k < 5; k++) {
    y[k] = std::abs(x[k]);
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::maximum(const real_T x[5])
{
  real_T ex;
  int32_T idx;
  int32_T k;
  boolean_T exitg1;
  if (!rtIsNaN(x[0])) {
    idx = 1;
  } else {
    idx = 0;
    k = 2;
    exitg1 = false;
    while ((!exitg1) && (k < 6)) {
      if (!rtIsNaN(x[k - 1])) {
        idx = k;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  if (idx == 0) {
    ex = x[0];
  } else {
    ex = x[idx - 1];
    while (idx + 1 <= 5) {
      if (ex < x[idx]) {
        ex = x[idx];
      }

      idx++;
    }
  }

  return ex;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::abs_ai(const real_T x[18], real_T
  y[18])
{
  int32_T k;
  for (k = 0; k < 18; k++) {
    y[k] = std::abs(x[k]);
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::maximum2(const real_T x[18],
  real_T y, real_T ex[18])
{
  real_T u0;
  int32_T k;
  for (k = 0; k < 18; k++) {
    u0 = x[k];
    if ((u0 > y) || rtIsNaN(y)) {
      ex[k] = u0;
    } else {
      ex[k] = y;
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
real_T PathFollowingControllerRefMdlModelClass::mtimes(const real_T b_A[5],
  const real_T b_B[5])
{
  real_T b_C;
  int32_T k;
  b_C = 0.0;
  for (k = 0; k < 5; k++) {
    b_C += b_A[k] * b_B[k];
  }

  return b_C;
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::DropConstraint(int16_T kDrop,
  int16_T iA[18], int16_T *nA, int16_T iC[18])
{
  int32_T tmp;
  int16_T i;
  iA[iC[kDrop - 1] - 1] = 0;
  if (kDrop < *nA) {
    tmp = *nA - 1;
    if (*nA - 1 < -32768) {
      tmp = -32768;
    }

    for (i = kDrop; i <= static_cast<int16_T>(tmp); i = static_cast<int16_T>(i +
          1)) {
      iC[i - 1] = iC[i];
    }
  }

  iC[*nA - 1] = 0;
  tmp = *nA - 1;
  if (*nA - 1 < -32768) {
    tmp = -32768;
  }

  *nA = static_cast<int16_T>(tmp);
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::qpkwik(const real_T b_Linv[25],
  const real_T b_Hinv[25], const real_T f[5], const real_T b_Ac[90], const
  real_T b[18], int16_T iA[18], int16_T maxiter, real_T FeasTol, real_T x[5],
  real_T lambda[18], real_T *status)
{
  real_T RLinv[25];
  real_T U[25];
  real_T b_D[25];
  real_T b_H[25];
  real_T cTol[18];
  real_T tmp_2[18];
  real_T Opt[10];
  real_T Rhs[10];
  real_T b_Ac_0[5];
  real_T r[5];
  real_T z[5];
  real_T Xnorm0;
  real_T cMin;
  real_T cVal;
  real_T rMin;
  real_T zTa;
  int32_T b_k;
  int32_T c_k;
  int32_T ct;
  int32_T exitg1;
  int32_T exitg3;
  int32_T i;
  int32_T tmp;
  int16_T iC[18];
  int16_T kDrop;
  int16_T kNext;
  int16_T nA;
  int16_T tmp_0;
  int16_T tmp_1;
  uint16_T b_x;
  uint16_T q;
  boolean_T ColdReset;
  boolean_T DualFeasible;
  boolean_T cTolComputed;
  boolean_T exitg2;
  boolean_T exitg4;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  *status = 1.0;
  std::memset(&lambda[0], 0, 18U * sizeof(real_T));
  for (i = 0; i < 5; i++) {
    x[i] = 0.0;
    r[i] = 0.0;
  }

  rMin = 0.0;
  cTolComputed = false;
  for (i = 0; i < 18; i++) {
    cTol[i] = 1.0;
    iC[i] = 0;
  }

  nA = 0;
  for (i = 0; i < 18; i++) {
    if (iA[i] == 1) {
      ct = nA + 1;
      if (nA + 1 > 32767) {
        ct = 32767;
      }

      nA = static_cast<int16_T>(ct);
      iC[static_cast<int16_T>(ct) - 1] = static_cast<int16_T>(i + 1);
    }
  }

  guard1 = false;
  if (nA > 0) {
    std::memset(&Opt[0], 0, 10U * sizeof(real_T));
    for (i = 0; i < 5; i++) {
      Rhs[i] = f[i];
      Rhs[i + 5] = 0.0;
    }

    DualFeasible = false;
    ct = 3 * nA;
    if (ct > 32767) {
      ct = 32767;
    }

    if (static_cast<int16_T>(ct) > 50) {
      kNext = static_cast<int16_T>(ct);
    } else {
      kNext = 50;
    }

    q = static_cast<uint16_T>(kNext / 10U);
    b_x = static_cast<uint16_T>(static_cast<uint32_T>(kNext) - q * 10);
    if ((b_x > 0) && (b_x >= 5)) {
      q = static_cast<uint16_T>(q + 1);
    }

    ColdReset = false;
    do {
      exitg3 = 0;
      if ((!DualFeasible) && (nA > 0) && (static_cast<int32_T>(*status) <=
           maxiter)) {
        Xnorm0 = KWIKfactor(b_Ac, iC, nA, b_Linv, RLinv, b_D, b_H, 5);
        if (Xnorm0 < 0.0) {
          if (ColdReset) {
            *status = -2.0;
            exitg3 = 2;
          } else {
            nA = 0;
            for (i = 0; i < 18; i++) {
              iA[i] = 0;
              iC[i] = 0;
            }

            ColdReset = true;
          }
        } else {
          for (i = 1; i - 1 < nA; i++) {
            ct = static_cast<int16_T>(i) + 5;
            if (static_cast<int16_T>(i) + 5 > 32767) {
              ct = 32767;
            }

            Rhs[ct - 1] = b[iC[static_cast<int16_T>(i) - 1] - 1];
            for (kNext = static_cast<int16_T>(i); kNext <= nA; kNext =
                 static_cast<int16_T>(kNext + 1)) {
              ct = (kNext + 5 * (static_cast<int16_T>(i) - 1)) - 1;
              U[ct] = 0.0;
              for (b_k = 1; b_k - 1 < nA; b_k++) {
                c_k = (static_cast<int16_T>(b_k) - 1) * 5;
                U[ct] += RLinv[(c_k + kNext) - 1] * RLinv[(c_k +
                  static_cast<int16_T>(i)) - 1];
              }

              U[(static_cast<int16_T>(i) + 5 * (kNext - 1)) - 1] = U[ct];
            }
          }

          for (i = 0; i < 5; i++) {
            Xnorm0 = 0.0;
            for (ct = 0; ct < 5; ct++) {
              Xnorm0 += b_H[i + 5 * ct] * Rhs[ct];
            }

            Opt[i] = Xnorm0;
            for (b_k = 1; b_k - 1 < nA; b_k++) {
              ct = static_cast<int16_T>(b_k) + 5;
              if (static_cast<int16_T>(b_k) + 5 > 32767) {
                ct = 32767;
              }

              Opt[i] += b_D[(static_cast<int16_T>(b_k) - 1) * 5 + i] * Rhs[ct -
                1];
            }
          }

          for (b_k = 1; b_k - 1 < nA; b_k++) {
            Xnorm0 = 0.0;
            for (ct = 0; ct < 5; ct++) {
              Xnorm0 += b_D[(static_cast<int16_T>(b_k) - 1) * 5 + ct] * Rhs[ct];
            }

            ct = static_cast<int16_T>(b_k) + 5;
            if (static_cast<int16_T>(b_k) + 5 > 32767) {
              ct = 32767;
            }

            Opt[ct - 1] = Xnorm0;
            for (c_k = 1; c_k - 1 < nA; c_k++) {
              ct = static_cast<int16_T>(b_k) + 5;
              if (static_cast<int16_T>(b_k) + 5 > 32767) {
                ct = 32767;
              }

              i = static_cast<int16_T>(b_k) + 5;
              if (static_cast<int16_T>(b_k) + 5 > 32767) {
                i = 32767;
              }

              tmp = static_cast<int16_T>(c_k) + 5;
              if (static_cast<int16_T>(c_k) + 5 > 32767) {
                tmp = 32767;
              }

              Opt[ct - 1] = U[((static_cast<int16_T>(c_k) - 1) * 5 +
                               static_cast<int16_T>(b_k)) - 1] * Rhs[tmp - 1] +
                Opt[i - 1];
            }
          }

          Xnorm0 = -1.0E-12;
          kDrop = 0;
          for (i = 1; i - 1 < nA; i++) {
            ct = static_cast<int16_T>(i) + 5;
            if (static_cast<int16_T>(i) + 5 > 32767) {
              ct = 32767;
            }

            lambda[iC[static_cast<int16_T>(i) - 1] - 1] = Opt[ct - 1];
            ct = static_cast<int16_T>(i) + 5;
            if (static_cast<int16_T>(i) + 5 > 32767) {
              ct = 32767;
            }

            if ((Opt[ct - 1] < Xnorm0) && (static_cast<int16_T>(i) <= nA)) {
              kDrop = static_cast<int16_T>(i);
              ct = static_cast<int16_T>(i) + 5;
              if (static_cast<int16_T>(i) + 5 > 32767) {
                ct = 32767;
              }

              Xnorm0 = Opt[ct - 1];
            }
          }

          if (kDrop <= 0) {
            DualFeasible = true;
            for (i = 0; i < 5; i++) {
              x[i] = Opt[i];
            }
          } else {
            (*status)++;
            if (static_cast<int32_T>(*status) > q) {
              nA = 0;
              for (i = 0; i < 18; i++) {
                iA[i] = 0;
                iC[i] = 0;
              }

              ColdReset = true;
            } else {
              lambda[iC[kDrop - 1] - 1] = 0.0;
              DropConstraint(kDrop, iA, &nA, iC);
            }
          }
        }
      } else {
        if (nA <= 0) {
          std::memset(&lambda[0], 0, 18U * sizeof(real_T));
          Unconstrained(b_Hinv, f, x, 5);
        }

        exitg3 = 1;
      }
    } while (exitg3 == 0);

    if (exitg3 == 1) {
      guard1 = true;
    }
  } else {
    Unconstrained(b_Hinv, f, x, 5);
    guard1 = true;
  }

  if (guard1) {
    Xnorm0 = norm(x);
    exitg2 = false;
    while ((!exitg2) && (static_cast<int32_T>(*status) <= maxiter)) {
      cMin = -FeasTol;
      kNext = 0;
      for (i = 0; i < 18; i++) {
        zTa = cTol[i];
        if (!cTolComputed) {
          for (ct = 0; ct < 5; ct++) {
            b_Ac_0[ct] = b_Ac[i + 18 * ct] * x[ct];
          }

          abs_a(b_Ac_0, z);
          cVal = maximum(z);
          if ((!(zTa > cVal)) && (!rtIsNaN(cVal))) {
            zTa = cVal;
          }
        }

        if (iA[i] == 0) {
          cVal = 0.0;
          for (ct = 0; ct < 5; ct++) {
            cVal += b_Ac[i + 18 * ct] * x[ct];
          }

          cVal = (cVal - b[i]) / zTa;
          if (cVal < cMin) {
            cMin = cVal;
            kNext = static_cast<int16_T>(i + 1);
          }
        }

        cTol[i] = zTa;
      }

      cTolComputed = true;
      if (kNext <= 0) {
        exitg2 = true;
      } else if (static_cast<int32_T>(*status) == maxiter) {
        *status = 0.0;
        exitg2 = true;
      } else {
        do {
          exitg1 = 0;
          if ((kNext > 0) && (static_cast<int32_T>(*status) <= maxiter)) {
            guard2 = false;
            if (nA == 0) {
              for (ct = 0; ct < 5; ct++) {
                z[ct] = 0.0;
                for (i = 0; i < 5; i++) {
                  z[ct] += b_Ac[(18 * i + kNext) - 1] * b_Hinv[5 * i + ct];
                }
              }

              guard2 = true;
            } else {
              cMin = KWIKfactor(b_Ac, iC, nA, b_Linv, RLinv, b_D, b_H, 5);
              if (cMin <= 0.0) {
                *status = -2.0;
                exitg1 = 1;
              } else {
                for (ct = 0; ct < 25; ct++) {
                  U[ct] = -b_H[ct];
                }

                for (ct = 0; ct < 5; ct++) {
                  z[ct] = 0.0;
                  for (i = 0; i < 5; i++) {
                    z[ct] += b_Ac[(18 * i + kNext) - 1] * U[5 * i + ct];
                  }
                }

                for (i = 1; i - 1 < nA; i++) {
                  cVal = 0.0;
                  for (ct = 0; ct < 5; ct++) {
                    cVal += b_Ac[(18 * ct + kNext) - 1] * b_D[(static_cast<
                      int16_T>(i) - 1) * 5 + ct];
                  }

                  r[static_cast<int16_T>(i) - 1] = cVal;
                }

                guard2 = true;
              }
            }

            if (guard2) {
              kDrop = 0;
              cMin = 0.0;
              DualFeasible = true;
              ColdReset = true;
              if (nA > 0) {
                ct = 0;
                exitg4 = false;
                while ((!exitg4) && (ct <= nA - 1)) {
                  if (r[ct] >= 1.0E-12) {
                    ColdReset = false;
                    exitg4 = true;
                  } else {
                    ct++;
                  }
                }
              }

              if ((nA != 0) && (!ColdReset)) {
                for (ct = 1; ct - 1 < nA; ct++) {
                  zTa = r[static_cast<int16_T>(ct) - 1];
                  if (zTa > 1.0E-12) {
                    zTa = lambda[iC[static_cast<int16_T>(ct) - 1] - 1] / zTa;
                    if ((kDrop == 0) || (zTa < rMin)) {
                      rMin = zTa;
                      kDrop = static_cast<int16_T>(ct);
                    }
                  }
                }

                if (kDrop > 0) {
                  cMin = rMin;
                  DualFeasible = false;
                }
              }

              for (ct = 0; ct < 5; ct++) {
                b_Ac_0[ct] = b_Ac[(18 * ct + kNext) - 1];
              }

              zTa = mtimes(z, b_Ac_0);
              if (zTa <= 0.0) {
                zTa = 0.0;
                ColdReset = true;
              } else {
                cVal = 0.0;
                for (ct = 0; ct < 5; ct++) {
                  cVal += b_Ac[(18 * ct + kNext) - 1] * x[ct];
                }

                zTa = (b[kNext - 1] - cVal) / zTa;
                ColdReset = false;
              }

              if (DualFeasible && ColdReset) {
                *status = -1.0;
                exitg1 = 1;
              } else {
                if (ColdReset) {
                  cVal = cMin;
                } else if (DualFeasible) {
                  cVal = zTa;
                } else if ((cMin < zTa) || rtIsNaN(zTa)) {
                  cVal = cMin;
                } else {
                  cVal = zTa;
                }

                for (ct = 1; ct - 1 < nA; ct++) {
                  i = iC[static_cast<int16_T>(ct) - 1];
                  lambda[i - 1] -= r[static_cast<int16_T>(ct) - 1] * cVal;
                  if ((i <= 18) && (lambda[i - 1] < 0.0)) {
                    lambda[i - 1] = 0.0;
                  }
                }

                lambda[kNext - 1] += cVal;
                if (cVal == cMin) {
                  DropConstraint(kDrop, iA, &nA, iC);
                }

                if (!ColdReset) {
                  for (ct = 0; ct < 5; ct++) {
                    x[ct] += cVal * z[ct];
                  }

                  if (cVal == zTa) {
                    if (nA == 5) {
                      *status = -1.0;
                      exitg1 = 1;
                    } else {
                      ct = nA + 1;
                      if (nA + 1 > 32767) {
                        ct = 32767;
                      }

                      nA = static_cast<int16_T>(ct);
                      iC[static_cast<int16_T>(ct) - 1] = kNext;
                      kDrop = static_cast<int16_T>(ct);
                      exitg4 = false;
                      while ((!exitg4) && (kDrop > 1)) {
                        tmp_0 = iC[kDrop - 1];
                        tmp_1 = iC[kDrop - 2];
                        if (tmp_0 > tmp_1) {
                          exitg4 = true;
                        } else {
                          iC[kDrop - 1] = tmp_1;
                          iC[kDrop - 2] = tmp_0;
                          kDrop = static_cast<int16_T>(kDrop - 1);
                        }
                      }

                      iA[kNext - 1] = 1;
                      kNext = 0;
                      (*status)++;
                    }
                  } else {
                    (*status)++;
                  }
                } else {
                  (*status)++;
                }
              }
            }
          } else {
            cMin = norm(x);
            if (std::abs(cMin - Xnorm0) > 0.001) {
              Xnorm0 = cMin;
              abs_ai(b, tmp_2);
              maximum2(tmp_2, 1.0, cTol);
              cTolComputed = false;
            }

            exitg1 = 2;
          }
        } while (exitg1 == 0);

        if (exitg1 == 1) {
          exitg2 = true;
        }
      }
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpc_solveQP(const real_T xQP[13],
  const real_T b_Kx[52], const real_T b_Kr[160], const real_T rseq[40], const
  real_T b_Ku1[8], const real_T old_u[2], const real_T b_Kv[132], const real_T
  vseq[33], const real_T b_Kut[80], const real_T b_utarget[20], const real_T
  b_Linv[25], const real_T b_Hinv[25], const real_T b_Ac[90], const real_T Bc[18],
  boolean_T iA[18], real_T zopt[5], real_T f[5], real_T *status)
{
  real_T unusedU0[18];
  real_T b_Kr_0;
  real_T b_Kut_0;
  real_T b_Kv_0;
  real_T b_Kx_0;
  int32_T f_tmp;
  int32_T i;
  int16_T iAnew[18];
  for (i = 0; i < 5; i++) {
    f[i] = 0.0;
  }

  for (i = 0; i < 4; i++) {
    b_Kx_0 = 0.0;
    for (f_tmp = 0; f_tmp < 13; f_tmp++) {
      b_Kx_0 += b_Kx[13 * i + f_tmp] * xQP[f_tmp];
    }

    b_Kr_0 = 0.0;
    for (f_tmp = 0; f_tmp < 40; f_tmp++) {
      b_Kr_0 += b_Kr[40 * i + f_tmp] * rseq[f_tmp];
    }

    b_Kv_0 = 0.0;
    for (f_tmp = 0; f_tmp < 33; f_tmp++) {
      b_Kv_0 += b_Kv[33 * i + f_tmp] * vseq[f_tmp];
    }

    b_Kut_0 = 0.0;
    for (f_tmp = 0; f_tmp < 20; f_tmp++) {
      b_Kut_0 += b_Kut[20 * i + f_tmp] * b_utarget[f_tmp];
    }

    f_tmp = i << 1;
    f[i] = (((b_Ku1[f_tmp + 1] * old_u[1] + b_Ku1[f_tmp] * old_u[0]) + (b_Kx_0 +
              b_Kr_0)) + b_Kv_0) + b_Kut_0;
  }

  for (i = 0; i < 18; i++) {
    iAnew[i] = iA[i];
  }

  qpkwik(b_Linv, b_Hinv, f, b_Ac, Bc, iAnew, 120, 1.0E-6, zopt, unusedU0, status);
  for (i = 0; i < 18; i++) {
    iA[i] = (iAnew[i] != 0);
  }

  if ((*status < 0.0) || (*status == 0.0)) {
    for (i = 0; i < 5; i++) {
      zopt[i] = 0.0;
    }
  }
}

// Function for MATLAB Function: '<S48>/FixedHorizonOptimizer'
void PathFollowingControllerRefMdlModelClass::mpcblock_optimizer(const real_T
  rseq[40], const real_T vseq[33], const real_T umin[2], const real_T umax[2],
  const real_T ymin[4], real_T switch_in, const real_T x[13], const real_T
  old_u[2], const boolean_T iA[18], const real_T b_Mlim[18], real_T b_Mx[234],
  real_T b_Mu1[36], real_T b_Mv[594], const real_T b_utarget[20], const real_T
  b_uoff[2], const real_T b_yoff[4], real_T b_enable_value, real_T b_H[25],
  real_T b_Ac[90], const real_T b_Wy[4], const real_T b_Wdu[2], const real_T
  b_Jm[80], const real_T b_Wu[2], const real_T b_I1[40], const real_T b_A[169],
  const real_T Bu[286], const real_T Bv[429], const real_T b_C[52], const real_T
  Dv[132], const real_T b_Mrows[18], const real_T b_RYscale[4], const real_T
  b_RMVscale[2], real_T u[2], real_T useq[22], real_T *status, boolean_T iAout
  [18], B_PathFollowingControlSystem_LFRefMdl_T *localB)
{
  static const int8_T c_A[100] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1,
    1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 1 };

  real_T c_Kr[160];
  real_T c_SuJm[160];
  real_T c_Kv[132];
  real_T a_0[80];
  real_T c_Kut[80];
  real_T c_Su1[80];
  real_T c_Kx[52];
  real_T c_Linv[25];
  real_T d_Linv[25];
  real_T Bc[18];
  real_T b[16];
  real_T c_Ku1[8];
  real_T f[5];
  real_T zopt[5];
  real_T ymin_incr[4];
  real_T umax_incr[2];
  real_T umin_incr[2];
  real_T c_DelBound;
  real_T ii;
  int32_T a_tmp;
  int32_T b_j1;
  int32_T i;
  int32_T i1;
  int32_T j2;
  int32_T kidx;
  int8_T a[400];
  int8_T b_I[25];
  boolean_T ymin_incr_flag[4];
  boolean_T umax_incr_flag[2];
  boolean_T umin_incr_flag[2];
  boolean_T c_Del_Save_Flag0_tmp;
  boolean_T exitg1;
  std::memset(&useq[0], 0, 22U * sizeof(real_T));
  *status = 1.0;
  for (i = 0; i < 18; i++) {
    iAout[i] = false;
  }

  if (switch_in != b_enable_value) {
    for (i = 0; i < 2; i++) {
      u[i] = old_u[i] + b_uoff[i];
      for (kidx = 0; kidx < 11; kidx++) {
        useq[kidx + 11 * i] = u[i];
      }
    }
  } else {
    mpc_constraintcoef(b_A, &Bu[0], &Bv[0], b_C, &Dv[0], b_Jm, c_SuJm,
                       localB->c_Sx, c_Su1, localB->c_Hv, localB);
    if (b_Mrows[0] > 0.0) {
      i = 0;
      exitg1 = false;
      while ((!exitg1) && (i < 18)) {
        if (b_Mrows[i] <= 40.0) {
          b_j1 = static_cast<int32_T>(b_Mrows[i]);
          b_Ac[i] = -c_SuJm[b_j1 - 1];
          b_Ac[i + 18] = -c_SuJm[b_j1 + 39];
          b_Ac[i + 36] = -c_SuJm[b_j1 + 79];
          b_Ac[i + 54] = -c_SuJm[b_j1 + 119];
          for (kidx = 0; kidx < 13; kidx++) {
            b_Mx[i + 18 * kidx] = -localB->c_Sx[(40 * kidx + b_j1) - 1];
          }

          b_Mu1[i] = -c_Su1[b_j1 - 1];
          b_Mu1[i + 18] = -c_Su1[b_j1 + 39];
          for (kidx = 0; kidx < 33; kidx++) {
            b_Mv[i + 18 * kidx] = -localB->c_Hv[(40 * kidx + b_j1) - 1];
          }

          i++;
        } else if (b_Mrows[i] <= 80.0) {
          b_j1 = static_cast<int32_T>(b_Mrows[i] - 40.0);
          b_Ac[i] = c_SuJm[b_j1 - 1];
          b_Ac[i + 18] = c_SuJm[b_j1 + 39];
          b_Ac[i + 36] = c_SuJm[b_j1 + 79];
          b_Ac[i + 54] = c_SuJm[b_j1 + 119];
          for (kidx = 0; kidx < 13; kidx++) {
            b_Mx[i + 18 * kidx] = localB->c_Sx[(40 * kidx + b_j1) - 1];
          }

          b_Mu1[i] = c_Su1[b_j1 - 1];
          b_Mu1[i + 18] = c_Su1[b_j1 + 39];
          for (kidx = 0; kidx < 33; kidx++) {
            b_Mv[i + 18 * kidx] = localB->c_Hv[(40 * kidx + b_j1) - 1];
          }

          i++;
        } else {
          exitg1 = true;
        }
      }
    }

    ymin_incr[1] = 0.0;
    ymin_incr[2] = 0.0;
    ymin_incr[0] = 1.0;
    ymin_incr[3] = 1.0;
    kidx = -1;
    for (b_j1 = 0; b_j1 < 10; b_j1++) {
      for (j2 = 0; j2 < 2; j2++) {
        for (i1 = 0; i1 < 10; i1++) {
          kidx++;
          i = j2 << 1;
          a_tmp = c_A[10 * b_j1 + i1];
          a[kidx] = static_cast<int8_T>(a_tmp * static_cast<int32_T>(ymin_incr[i]));
          kidx++;
          a[kidx] = static_cast<int8_T>(a_tmp * static_cast<int32_T>(ymin_incr[i
            + 1]));
        }
      }
    }

    for (kidx = 0; kidx < 4; kidx++) {
      for (b_j1 = 0; b_j1 < 20; b_j1++) {
        i = b_j1 + 20 * kidx;
        a_0[i] = 0.0;
        for (j2 = 0; j2 < 20; j2++) {
          a_0[i] += static_cast<real_T>(a[20 * j2 + b_j1]) * b_Jm[20 * kidx + j2];
        }
      }
    }

    mpc_calculatehessian(b_Wy, b_Wu, b_Wdu, c_SuJm, a_0, b_Jm, b_I1, c_Su1,
                         localB->c_Sx, localB->c_Hv, LFRefMdl_nu, LFRefMdl_ny, b,
                         c_Ku1, c_Kut, c_Kx, c_Kv, c_Kr);
    for (kidx = 0; kidx < 4; kidx++) {
      i = kidx << 2;
      b_H[5 * kidx] = b[i];
      b_H[5 * kidx + 1] = b[i + 1];
      b_H[5 * kidx + 2] = b[i + 2];
      b_H[5 * kidx + 3] = b[i + 3];
    }

    std::memcpy(&c_Linv[0], &b_H[0], 25U * sizeof(real_T));
    mpc_checkhessian(c_Linv, d_Linv, &ii);
    if (ii > 1.0) {
      for (i = 0; i < 2; i++) {
        u[i] = old_u[i] + b_uoff[i];
        for (kidx = 0; kidx < 11; kidx++) {
          useq[kidx + 11 * i] = u[i];
        }
      }

      *status = -2.0;
    } else {
      for (kidx = 0; kidx < 25; kidx++) {
        b_I[kidx] = 0;
      }

      for (kidx = 0; kidx < 5; kidx++) {
        b_I[kidx + 5 * kidx] = 1;
      }

      for (kidx = 0; kidx < 5; kidx++) {
        for (b_j1 = 0; b_j1 < 5; b_j1++) {
          i = 5 * kidx + b_j1;
          c_Linv[i] = b_I[i];
        }
      }

      trisolve(d_Linv, c_Linv);
      for (kidx = 0; kidx < 18; kidx++) {
        ii = 0.0;
        for (b_j1 = 0; b_j1 < 13; b_j1++) {
          ii += b_Mx[18 * b_j1 + kidx] * x[b_j1];
        }

        c_DelBound = 0.0;
        for (b_j1 = 0; b_j1 < 33; b_j1++) {
          c_DelBound += b_Mv[18 * b_j1 + kidx] * vseq[b_j1];
        }

        Bc[kidx] = -(((b_Mlim[kidx] + ii) + (b_Mu1[kidx + 18] * old_u[1] +
          b_Mu1[kidx] * old_u[0])) + c_DelBound);
      }

      ymin_incr_flag[0] = false;
      ymin_incr[0] = 0.0;
      ymin_incr_flag[1] = false;
      ymin_incr[1] = 0.0;
      ymin_incr_flag[2] = false;
      ymin_incr[2] = 0.0;
      ymin_incr_flag[3] = false;
      ymin_incr[3] = 0.0;
      umax_incr_flag[0] = false;
      umax_incr[0] = 0.0;
      umin_incr_flag[0] = false;
      umin_incr[0] = 0.0;
      umax_incr_flag[1] = false;
      umax_incr[1] = 0.0;
      umin_incr_flag[1] = false;
      umin_incr[1] = 0.0;
      if (b_Mrows[0] > 0.0) {
        kidx = 0;
        exitg1 = false;
        while ((!exitg1) && (kidx < 18)) {
          if (b_Mrows[kidx] <= 40.0) {
            kidx++;
          } else if (b_Mrows[kidx] <= 80.0) {
            ii = mod((b_Mrows[kidx] - 40.0) - 1.0) + 1.0;
            c_Del_Save_Flag0_tmp = ymin_incr_flag[static_cast<int32_T>(ii) - 1];
            if (!ymin_incr_flag[static_cast<int32_T>(ii) - 1]) {
              c_DelBound = (b_RYscale[static_cast<int32_T>(ii) - 1] * ymin[
                            static_cast<int32_T>(ii) - 1] - b_yoff
                            [static_cast<int32_T>(ii) - 1]) - (-b_Mlim[kidx]);
              c_Del_Save_Flag0_tmp = true;
            } else {
              c_DelBound = ymin_incr[static_cast<int32_T>(ii) - 1];
            }

            ymin_incr[static_cast<int32_T>(ii) - 1] = c_DelBound;
            ymin_incr_flag[static_cast<int32_T>(ii) - 1] = c_Del_Save_Flag0_tmp;
            Bc[kidx] += c_DelBound;
            kidx++;
          } else if (b_Mrows[kidx] <= 100.0) {
            ii = mod_o((b_Mrows[kidx] - 80.0) - 1.0) + 1.0;
            c_Del_Save_Flag0_tmp = umax_incr_flag[static_cast<int32_T>(ii) - 1];
            if (!c_Del_Save_Flag0_tmp) {
              c_DelBound = -(b_RMVscale[static_cast<int32_T>(ii) - 1] * umax[
                             static_cast<int32_T>(ii) - 1] - b_uoff
                             [static_cast<int32_T>(ii) - 1]) - (-b_Mlim[kidx]);
              c_Del_Save_Flag0_tmp = true;
            } else {
              c_DelBound = umax_incr[static_cast<int32_T>(ii) - 1];
            }

            umax_incr[static_cast<int32_T>(ii) - 1] = c_DelBound;
            umax_incr_flag[static_cast<int32_T>(ii) - 1] = c_Del_Save_Flag0_tmp;
            Bc[kidx] += c_DelBound;
            kidx++;
          } else if (b_Mrows[kidx] <= 120.0) {
            ii = mod_o(((b_Mrows[kidx] - 80.0) - 20.0) - 1.0) + 1.0;
            c_Del_Save_Flag0_tmp = umin_incr_flag[static_cast<int32_T>(ii) - 1];
            if (!c_Del_Save_Flag0_tmp) {
              c_DelBound = (b_RMVscale[static_cast<int32_T>(ii) - 1] * umin[
                            static_cast<int32_T>(ii) - 1] - b_uoff
                            [static_cast<int32_T>(ii) - 1]) - (-b_Mlim[kidx]);
              c_Del_Save_Flag0_tmp = true;
            } else {
              c_DelBound = umin_incr[static_cast<int32_T>(ii) - 1];
            }

            umin_incr[static_cast<int32_T>(ii) - 1] = c_DelBound;
            umin_incr_flag[static_cast<int32_T>(ii) - 1] = c_Del_Save_Flag0_tmp;
            Bc[kidx] += c_DelBound;
            kidx++;
          } else {
            exitg1 = true;
          }
        }
      }

      for (i = 0; i < 18; i++) {
        iAout[i] = iA[i];
      }

      for (kidx = 0; kidx < 5; kidx++) {
        for (b_j1 = 0; b_j1 < 5; b_j1++) {
          i = kidx + 5 * b_j1;
          d_Linv[i] = 0.0;
          for (j2 = 0; j2 < 5; j2++) {
            d_Linv[i] += c_Linv[5 * kidx + j2] * c_Linv[5 * b_j1 + j2];
          }
        }
      }

      mpc_solveQP(x, c_Kx, c_Kr, rseq, c_Ku1, old_u, c_Kv, vseq, c_Kut,
                  b_utarget, c_Linv, d_Linv, b_Ac, Bc, iAout, zopt, f, status);
      u[0] = (old_u[0] + zopt[0]) + b_uoff[0];
      u[1] = (old_u[1] + zopt[1]) + b_uoff[1];
    }
  }
}

// System initialize for atomic system: '<S2>/Path Following Control System'
void PathFollowingControllerRefMdlModelClass::PathFollowingControlSystem_Init
  (DW_PathFollowingControlSystem_LFRefMdl_T *localDW)
{
  // InitializeConditions for Memory: '<S20>/LastPcov'
  std::memcpy(&localDW->LastPcov_PreviousInput[0],
              &LFRefMdl_ConstP.LastPcov_InitialCondition[0], 169U * sizeof
              (real_T));
}

// Output and update for atomic system: '<S2>/Path Following Control System'
void PathFollowingControllerRefMdlModelClass::PathFollowingControlSystem(real_T
  rtu_Setvelocity, real_T rtu_Timegap, real_T rtu_Relativedistance, real_T
  rtu_Relativevelocity, real_T rtu_Longitudinalvelocity, const real_T
  rtu_Curvature[11], real32_T rtu_Lateraldeviation, real32_T
  rtu_Relativeyawangle, real_T rtyy_LongitudinalaccelerationSteeringangle[2],
  B_PathFollowingControlSystem_LFRefMdl_T *localB, const
  ConstB_PathFollowingControlSystem_LFRefMdl_T *localC,
  DW_PathFollowingControlSystem_LFRefMdl_T *localDW, real_T rtp_DefaultSpacing,
  real_T rtp_MinAcceleration, real_T rtp_MinSteering, real_T rtp_MaxAcceleration,
  real_T rtp_MaxSteering)
{
  static const real_T db[594] = { 0.022071949480768065, 0.046047049970262724,
    0.05486699654036719, 0.058111673555739193, 0.059305323522936076,
    0.059744442805822771, 0.059905985762218734, 0.059965414094742868,
    0.059987276556501594, 0.059995319306716026, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022071949480768065,
    0.046047049970262724, 0.05486699654036719, 0.058111673555739193,
    0.059305323522936076, 0.059744442805822771, 0.059905985762218734,
    0.059965414094742868, 0.059987276556501594, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022071949480768065,
    0.046047049970262724, 0.05486699654036719, 0.058111673555739193,
    0.059305323522936076, 0.059744442805822771, 0.059905985762218734,
    0.059965414094742868, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022071949480768065, 0.046047049970262724,
    0.05486699654036719, 0.058111673555739193, 0.059305323522936076,
    0.059744442805822771, 0.059905985762218734, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.022071949480768065, 0.046047049970262724, 0.05486699654036719,
    0.058111673555739193, 0.059305323522936076, 0.059744442805822771, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.022071949480768065, 0.046047049970262724, 0.05486699654036719,
    0.058111673555739193, 0.059305323522936076, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.022071949480768065, 0.046047049970262724, 0.05486699654036719,
    0.058111673555739193, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.022071949480768065,
    0.046047049970262724, 0.05486699654036719, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.022071949480768065, 0.046047049970262724, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.022071949480768065, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

  static const real_T bb[234] = { -2.5097346444526045E-5,
    -0.00015566646691359248, -0.00041762595849569929, -0.00080372711601045153,
    -0.0012975620205717435, -0.0018818441364070967, -0.0025410030541436522,
    -0.0032617693718096216, -0.0040330871989338948, -0.0048458341702463893, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00073575888234288472,
    -0.0022706705664732258, -0.0040995741367357286, -0.0060366312777774683,
    -0.0080134758939981714, -0.010004957504353333, -0.012001823763931109,
    -0.014000670925255806, -0.016000246819608176, -0.018000090799859526, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.012642411176571153, 0.017293294335267745, 0.019004258632642719,
    0.019633687222225315, 0.019865241060018288, 0.019950424956466671,
    0.019981762360688909, 0.019993290747441949, 0.019997531803918264,
    0.019999092001404747, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0073575888234288468, 0.0027067056647322543,
    0.000995741367357279, 0.00036631277777468367, 0.00013475893998170937,
    4.9575043533327183E-5, 1.8237639311090329E-5, 6.7092525580502387E-6,
    2.468196081733592E-6, 9.0799859524969739E-7, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0 };

  static const real_T f[169] = { 0.81873075307798182, 0.090634623461009076, 0.0,
    0.0, -0.0046826882694954658, 0.0, 0.0, -0.0012548673222263023,
    0.03427820947269164, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, -0.1, 0.0, 0.0,
    -0.036787944117144235, 0.63212055882855767, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.59029522013726721, 0.083693736924718945, 0.0, 0.081595384470467269,
    0.0050186685703353349, 0.0, 0.0, 0.031727734227050726, 0.0014265270184191715,
    0.0, 0.0, 0.0, 0.0, -0.74954881953164709, 0.54309372380774523, 0.0,
    0.017801733348590944, 0.0760340469863906, 0.0, 0.0, 0.0044743070473577421,
    0.030332079081723702, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
    0.63212055882855767, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,
    0.0, 0.0, 0.0, 0.63212055882855767, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    1.5, 1.0, 0.0, 0.0, 0.5518191617571635, 0.63212055882855767, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.36787944117144233, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.36787944117144233, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.36787944117144233, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.36787944117144233, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 1.0 };

  static const real_T g[143] = { 0.90634626607108237, 0.046826866964458827, 0.0,
    0.0, -0.0015865665177705894, 0.0, 0.0, -0.00033153095570658269,
    0.012550355620640066, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.61873334759502352,
    0.6900642918472496, 0.0, 0.059282882687925628, 0.036786097444293293, 0.0,
    0.0, 0.015790431816442217, 0.009980720706687575, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 3.0, 0.0, 0.0, 1.1035974740384031, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, -0.0075, -0.010000000000000002, 0.0, 0.0,
    -0.0019820126298079844, -0.0036786582467946772, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

  static const real_T gb[90] = { -6.6306191141316537E-6, -8.6328083384773266E-5,
    -0.00036207851145087232, -0.00096304409178842639, -0.0020054472868750216,
    -0.0035882959519204487, -0.0057940682842349857, -0.0086908131606272913,
    -0.012334436116386939, -0.016770779301781606, -1.0, -0.0, -1.0, -0.0, 1.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, -1.0,
    -0.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0, -6.6306191141316537E-6,
    -8.6328083384773266E-5, -0.00036207851145087232, -0.00096304409178842639,
    -0.0020054472868750216, -0.0035882959519204487, -0.0057940682842349857,
    -0.0086908131606272913, -0.012334436116386939, -0.0, -0.0, -1.0, -0.0, 0.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, -0.0,
    -0.0, -1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
    1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

  static const real_T ib[80] = { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

  static const real_T h[52] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.02, 0.0, 0.0, 0.0, 0.0, 0.033333333333333333, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 5.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };

  static const real_T l[44] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0 };

  static const real_T nb[40] = { 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0,
    0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0,
    0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
    1.0 };

  static const real_T cb[36] = { -6.6306191141316537E-6, -8.6328083384773266E-5,
    -0.00036207851145087232, -0.00096304409178842639, -0.0020054472868750216,
    -0.0035882959519204487, -0.0057940682842349857, -0.0086908131606272913,
    -0.012334436116386939, -0.016770779301781606, -1.0, -0.0, -1.0, -0.0, 1.0,
    0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.0, -1.0,
    -0.0, -1.0, 0.0, 1.0, 0.0, 1.0 };

  static const real_T eb[25] = { 0.010174761645622152, 0.0,
    0.00013793903871334287, 0.0, 0.0, 0.0, 87.80876150093448, 0.0,
    64.89715064245047, 0.0, 0.00013793903871334287, 0.0, 0.010109533425685675,
    0.0, 0.0, 0.0, 64.89715064245047, 0.0, 48.17656182080011, 0.0, 0.0, 0.0, 0.0,
    0.0, 100000.0 };

  static const real_T q[18] = { -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0, -0.0,
    -0.0, -0.0, 0.4, 0.5, 0.4, 0.5, 0.6, 0.5, 0.6, 0.5 };

  static const real_T r[18] = { 41.0, 45.0, 49.0, 53.0, 57.0, 61.0, 65.0, 69.0,
    73.0, 77.0, 81.0, 82.0, 83.0, 84.0, 101.0, 102.0, 103.0, 104.0 };

  static const real_T b_RYscale[4] = { 0.02, 0.033333333333333333, 1.0, 5.0 };

  static const real_T hb[4] = { 0.0, 0.010000000000000002, 1.0, 0.0 };

  static const real_T n[4] = { 5.0, 0.52, 30.0, 0.1 };

  static const real_T o[4] = { 50.0, 30.0, 1.0, 0.2 };

  static const real_T s[4] = { 0.62, 0.5, 0.0, 0.0 };

  static const int8_T b[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

  real_T Bv[429];
  real_T Bu[286];
  real_T bb_0[234];
  real_T L_tmp[169];
  real_T L_tmp_0[169];
  real_T L_tmp_1[169];
  real_T Qk[169];
  real_T b_A[169];
  real_T g_0[143];
  real_T Dv[132];
  real_T Dvm[132];
  real_T As_0[121];
  real_T rtb_A[121];
  real_T gb_0[90];
  real_T tmp_4[80];
  real_T Cm[52];
  real_T L[52];
  real_T Nk[52];
  real_T b_C[52];
  real_T tmp_3[52];
  real_T As[49];
  real_T Bs_0[44];
  real_T l_0[44];
  real_T rtb_B[44];
  real_T rseq[40];
  real_T cb_0[36];
  real_T vseq[33];
  real_T Bs[28];
  real_T Cs[28];
  real_T rtb_MatrixConcatenate_0[28];
  real_T eb_0[25];
  real_T rtb_VectorConcatenate[22];
  real_T b_utarget[20];
  real_T b_Mlim[18];
  real_T Cm_0[16];
  real_T c_A[16];
  real_T rtb_MatrixConcatenate[16];
  real_T b_xoff[13];
  real_T xk[13];
  real_T xk_0[13];
  real_T tmp[8];
  real_T Cm_1[4];
  real_T rtb_Am[4];
  real_T rtu_Relativedistance_0[4];
  real_T y_innov[4];
  real_T b_uoff[2];
  real_T b_voff[2];
  real_T tmp_0[2];
  real_T tmp_1[2];
  real_T tmp_2[2];
  real_T tmp_5[2];
  real_T tmp_6[2];
  real_T L_0;
  real_T rtb_Product;
  real_T rtb_leadvelocity;
  real_T status;
  real_T tmp_7;
  real_T v_idx_2;
  int32_T As_tmp;
  int32_T i;
  int32_T rtb_MatrixConcatenate_tmp;
  int32_T rtb_MatrixConcatenate_tmp_tmp;
  boolean_T tmp_8[18];

  // Sum: '<S17>/Sum'
  rtb_leadvelocity = rtu_Longitudinalvelocity + rtu_Relativevelocity;
  for (i = 0; i < 11; i++) {
    // Product: '<S12>/Product'
    rtb_Product = rtu_Curvature[i] * rtu_Longitudinalvelocity;

    // Product: '<S17>/Product' incorporates:
    //   Abs: '<S17>/Abs'
    //   Constant: '<S19>/Constant'
    //   DataTypeConversion: '<S17>/Data Type  Conversion'
    //   RelationalOperator: '<S19>/Compare'

    rtb_VectorConcatenate[i] = static_cast<real_T>(std::abs(rtb_Product) >= 0.0)
      * rtb_leadvelocity;

    // SignalConversion generated from: '<S17>/Vector Concatenate'
    rtb_VectorConcatenate[i + 11] = rtb_Product;
  }

  // Concatenate: '<S53>/Matrix Concatenate4' incorporates:
  //   Constant: '<S53>/Matrix constant'
  //   Constant: '<S54>/Vehicle mass constant'
  //   Product: '<S54>/Divide2'

  tmp[0] = 0.0;
  tmp[2] = localC->Gain2 / 1575.0 / rtu_Longitudinalvelocity;
  tmp[1] = 0.0;

  // Product: '<S54>/Divide4' incorporates:
  //   Product: '<S54>/Divide3'

  status = localC->Gain3 / rtu_Longitudinalvelocity;

  // Concatenate: '<S53>/Matrix Concatenate4' incorporates:
  //   Constant: '<S53>/Matrix constant'
  //   Constant: '<S54>/Vehicle mass constant'
  //   Constant: '<S54>/Vehicle yaw inertia constant'
  //   Product: '<S54>/Divide3'
  //   Product: '<S54>/Divide4'
  //   Product: '<S54>/Divide5'
  //   Sum: '<S54>/Sum2'

  tmp[3] = status / 2875.0;
  tmp[4] = 0.0;
  tmp[6] = status / 1575.0 - rtu_Longitudinalvelocity;
  tmp[5] = 0.0;
  tmp[7] = localC->Gain4 / rtu_Longitudinalvelocity / 2875.0;
  for (i = 0; i < 8; i++) {
    // SignalConversion generated from: '<S53>/Matrix Concatenate' incorporates:
    //   Concatenate: '<S53>/Matrix Concatenate3'

    rtb_MatrixConcatenate[i] = localC->firstcolumn_k[i];

    // Concatenate: '<S53>/Matrix Concatenate4'
    rtb_MatrixConcatenate[i + 8] = tmp[i];
  }

  // MATLAB Function: '<S50>/Adaptive Model with Spacing Control' incorporates:
  //   Concatenate: '<S53>/Matrix Concatenate'
  //   Concatenate: '<S53>/Matrix Concatenate1'
  //   Concatenate: '<S53>/Matrix Concatenate2'
  //   Constant: '<S11>/Sample time constant'

  pfcblock_utilAugmentVehicleMdlHasSpacingCtrl(rtb_MatrixConcatenate,
    localC->MatrixConcatenate1, localC->MatrixConcatenate2,
    rtu_Longitudinalvelocity, As, Bs, Cs);
  for (i = 0; i < 16; i++) {
    rtb_MatrixConcatenate[i] = 10.0 * static_cast<real_T>(b[i]);
  }

  for (i = 0; i < 7; i++) {
    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      rtb_MatrixConcatenate_tmp_tmp = i << 2;
      rtb_MatrixConcatenate_tmp = As_tmp + rtb_MatrixConcatenate_tmp_tmp;
      rtb_MatrixConcatenate_0[rtb_MatrixConcatenate_tmp] = 0.0;
      rtb_MatrixConcatenate_0[rtb_MatrixConcatenate_tmp] +=
        Cs[rtb_MatrixConcatenate_tmp_tmp] * rtb_MatrixConcatenate[As_tmp];
      rtb_MatrixConcatenate_0[rtb_MatrixConcatenate_tmp] +=
        Cs[rtb_MatrixConcatenate_tmp_tmp + 1] * rtb_MatrixConcatenate[As_tmp + 4];
      rtb_MatrixConcatenate_0[rtb_MatrixConcatenate_tmp] +=
        Cs[rtb_MatrixConcatenate_tmp_tmp + 2] * rtb_MatrixConcatenate[As_tmp + 8];
      rtb_MatrixConcatenate_0[rtb_MatrixConcatenate_tmp] +=
        Cs[rtb_MatrixConcatenate_tmp_tmp + 3] * rtb_MatrixConcatenate[As_tmp +
        12];
    }
  }

  for (i = 0; i < 7; i++) {
    for (As_tmp = 0; As_tmp < 7; As_tmp++) {
      As_0[As_tmp + 11 * i] = As[7 * i + As_tmp];
    }
  }

  for (i = 0; i < 4; i++) {
    for (As_tmp = 0; As_tmp < 7; As_tmp++) {
      As_0[As_tmp + 11 * (i + 7)] = 0.0;
    }
  }

  for (i = 0; i < 7; i++) {
    As_tmp = i << 2;
    As_0[11 * i + 7] = rtb_MatrixConcatenate_0[As_tmp];
    As_0[11 * i + 8] = rtb_MatrixConcatenate_0[As_tmp + 1];
    As_0[11 * i + 9] = rtb_MatrixConcatenate_0[As_tmp + 2];
    As_0[11 * i + 10] = rtb_MatrixConcatenate_0[As_tmp + 3];
  }

  for (i = 0; i < 4; i++) {
    As_tmp = i << 2;
    rtb_MatrixConcatenate_tmp_tmp = 11 * (i + 7);
    As_0[rtb_MatrixConcatenate_tmp_tmp + 7] = static_cast<real_T>(b[As_tmp]) *
      -10.0;
    As_0[rtb_MatrixConcatenate_tmp_tmp + 8] = static_cast<real_T>(b[As_tmp + 1])
      * -10.0;
    As_0[rtb_MatrixConcatenate_tmp_tmp + 9] = static_cast<real_T>(b[As_tmp + 2])
      * -10.0;
    As_0[rtb_MatrixConcatenate_tmp_tmp + 10] = static_cast<real_T>(b[As_tmp + 3])
      * -10.0;
    for (As_tmp = 0; As_tmp < 7; As_tmp++) {
      Bs_0[As_tmp + 11 * i] = Bs[7 * i + As_tmp];
    }

    Bs_0[11 * i + 7] = 0.0;
    Bs_0[11 * i + 8] = 0.0;
    Bs_0[11 * i + 9] = 0.0;
    Bs_0[11 * i + 10] = 0.0;
  }

  adasblocks_utilDicretizeModel(As_0, Bs_0, 0.1, rtb_A, rtb_B);

  // MATLAB Function: '<S48>/FixedHorizonOptimizer'
  std::memset(&Bu[0], 0, 286U * sizeof(real_T));
  std::memset(&Bv[0], 0, 429U * sizeof(real_T));
  std::memset(&Dv[0], 0, 132U * sizeof(real_T));
  std::memset(&Dvm[0], 0, 132U * sizeof(real_T));
  std::memset(&localB->Cm[0], 0, 572U * sizeof(real_T));
  std::memcpy(&b_A[0], &f[0], 169U * sizeof(real_T));
  std::memcpy(&b_C[0], &h[0], 52U * sizeof(real_T));

  // MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
  for (i = 0; i < 4; i++) {
    for (As_tmp = 0; As_tmp < 7; As_tmp++) {
      rtb_MatrixConcatenate_tmp_tmp = As_tmp << 2;
      rtb_MatrixConcatenate_tmp = i + rtb_MatrixConcatenate_tmp_tmp;
      Bs[rtb_MatrixConcatenate_tmp] = 0.0;
      Bs[rtb_MatrixConcatenate_tmp] += Cs[rtb_MatrixConcatenate_tmp_tmp] * 0.0;
      Bs[rtb_MatrixConcatenate_tmp] += Cs[rtb_MatrixConcatenate_tmp_tmp + 1] *
        0.0;
      Bs[rtb_MatrixConcatenate_tmp] += Cs[rtb_MatrixConcatenate_tmp_tmp + 2] *
        0.0;
      Bs[rtb_MatrixConcatenate_tmp] += Cs[rtb_MatrixConcatenate_tmp_tmp + 3] *
        0.0;
    }
  }

  std::memcpy(&Bs_0[0], &Bs[0], 28U * sizeof(real_T));
  for (i = 0; i < 16; i++) {
    Bs_0[i + 28] = b[i];
    c_A[i] = 0.0;
  }

  // MATLAB Function: '<S48>/FixedHorizonOptimizer' incorporates:
  //   Concatenate: '<S17>/Vector Concatenate'
  //   Constant: '<S11>/Default spacing constant'
  //   Constant: '<S50>/DX Constant'
  //   Constant: '<S50>/U Constant'
  //   Constant: '<S50>/X Constant'
  //   Constant: '<S50>/Y Constant'
  //   DataTypeConversion: '<S20>/Data Type Conversion1'
  //   MATLAB Function: '<S50>/Adaptive Model with Spacing Control'
  //   Memory: '<S20>/LastPcov'
  //   Memory: '<S20>/last_x'

  tmp_0[0] = 1.0;
  tmp_1[0] = 3.0;
  tmp_0[1] = 2.0;
  tmp_1[1] = 4.0;
  Cm_1[0] = 1.0;
  Cm_1[1] = 2.0;
  Cm_1[2] = 3.0;
  Cm_1[3] = 4.0;
  std::memcpy(&g_0[0], &g[0], 143U * sizeof(real_T));
  std::memcpy(&l_0[0], &l[0], 44U * sizeof(real_T));
  mpc_plantupdate(rtb_A, rtb_B, Bs_0, c_A, b_A, g_0, b_C, l_0, tmp_0, tmp_1,
                  Cm_1, n, o, &Bu[0], &Bv[0], L, &Dv[0], &Dvm[0], Qk,
                  rtb_MatrixConcatenate, Nk);
  for (i = 0; i < 13; i++) {
    rtb_MatrixConcatenate_tmp_tmp = i << 2;
    localB->Cm[rtb_MatrixConcatenate_tmp_tmp] = L[rtb_MatrixConcatenate_tmp_tmp];
    localB->Cm[rtb_MatrixConcatenate_tmp_tmp + 1] =
      L[rtb_MatrixConcatenate_tmp_tmp + 1];
    localB->Cm[rtb_MatrixConcatenate_tmp_tmp + 2] =
      L[rtb_MatrixConcatenate_tmp_tmp + 2];
    localB->Cm[rtb_MatrixConcatenate_tmp_tmp + 3] =
      L[rtb_MatrixConcatenate_tmp_tmp + 3];
  }

  std::memcpy(&b_Mlim[0], &q[0], 18U * sizeof(real_T));
  std::memset(&b_utarget[0], 0, 20U * sizeof(real_T));
  std::memset(&b_xoff[0], 0, 13U * sizeof(real_T));
  tmp_0[0] = 0.0;
  tmp_1[0] = 1.0;
  tmp_2[0] = 3.0;
  tmp_0[1] = 0.0;
  tmp_1[1] = 2.0;
  tmp_2[1] = 4.0;
  Cm_1[0] = 1.0;
  Cm_1[1] = 2.0;
  Cm_1[2] = 3.0;
  Cm_1[3] = 4.0;
  mpc_updateFromNominal(b_Mlim, r, LFRefMdl_ConstP.UConstant_Value, n, tmp_0,
                        tmp_1, tmp_2, b_utarget, LFRefMdl_ConstP.YConstant_Value,
                        o, s, Cm_1, LFRefMdl_ConstP.pooled12, b_xoff,
                        LFRefMdl_ConstP.pooled12, Bv, b_uoff, b_voff, rtb_Am,
                        y_innov);
  std::memset(&vseq[0], 0, 33U * sizeof(real_T));
  for (i = 0; i < 11; i++) {
    vseq[i * 3 + 2] = 1.0;
  }

  for (i = 0; i < 10; i++) {
    As_tmp = i << 2;
    rseq[As_tmp] = rtp_DefaultSpacing * 0.02 - rtb_Am[0];
    rseq[As_tmp + 1] = rtu_Setvelocity * 0.033333333333333333 - rtb_Am[1];
    rseq[As_tmp + 2] = 0.0 - rtb_Am[2];
    rseq[As_tmp + 3] = 0.0 - rtb_Am[3];
  }

  for (i = 0; i < 11; i++) {
    vseq[i * 3] = 0.033333333333333333 * rtb_VectorConcatenate[i] - b_voff[0];
    vseq[i * 3 + 1] = rtb_VectorConcatenate[i + 11] * 10.0 - b_voff[1];
  }

  rtb_leadvelocity = vseq[0];
  rtb_Product = vseq[1];
  v_idx_2 = vseq[2];
  std::memset(&c_A[0], 0, sizeof(real_T) << 4U);
  for (rtb_MatrixConcatenate_tmp_tmp = 0; rtb_MatrixConcatenate_tmp_tmp < 4;
       rtb_MatrixConcatenate_tmp_tmp++) {
    c_A[rtb_MatrixConcatenate_tmp_tmp + (rtb_MatrixConcatenate_tmp_tmp << 2)] =
      1.0;
    for (i = 0; i < 13; i++) {
      rtb_MatrixConcatenate_tmp = (i << 2) + rtb_MatrixConcatenate_tmp_tmp;
      L[i + 13 * rtb_MatrixConcatenate_tmp_tmp] = localB->
        Cm[rtb_MatrixConcatenate_tmp];
      Cm[rtb_MatrixConcatenate_tmp] = 0.0;
      for (As_tmp = 0; As_tmp < 13; As_tmp++) {
        Cm[rtb_MatrixConcatenate_tmp] += localB->Cm[(As_tmp << 2) +
          rtb_MatrixConcatenate_tmp_tmp] * localDW->LastPcov_PreviousInput[13 *
          i + As_tmp];
      }
    }
  }

  for (i = 0; i < 4; i++) {
    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      status = 0.0;
      for (rtb_MatrixConcatenate_tmp_tmp = 0; rtb_MatrixConcatenate_tmp_tmp < 13;
           rtb_MatrixConcatenate_tmp_tmp++) {
        status += Cm[(rtb_MatrixConcatenate_tmp_tmp << 2) + i] * L[13 * As_tmp +
          rtb_MatrixConcatenate_tmp_tmp];
      }

      rtb_MatrixConcatenate_tmp_tmp = (As_tmp << 2) + i;
      Cm_0[rtb_MatrixConcatenate_tmp_tmp] =
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp_tmp] + status;
    }
  }

  mrdiv(c_A, Cm_0, rtb_MatrixConcatenate);
  for (i = 0; i < 13; i++) {
    for (As_tmp = 0; As_tmp < 13; As_tmp++) {
      rtb_MatrixConcatenate_tmp = i + 13 * As_tmp;
      L_tmp[rtb_MatrixConcatenate_tmp] = 0.0;
      for (rtb_MatrixConcatenate_tmp_tmp = 0; rtb_MatrixConcatenate_tmp_tmp < 13;
           rtb_MatrixConcatenate_tmp_tmp++) {
        L_tmp[rtb_MatrixConcatenate_tmp] += b_A[13 *
          rtb_MatrixConcatenate_tmp_tmp + i] * localDW->LastPcov_PreviousInput
          [13 * As_tmp + rtb_MatrixConcatenate_tmp_tmp];
      }
    }

    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      status = 0.0;
      for (rtb_MatrixConcatenate_tmp_tmp = 0; rtb_MatrixConcatenate_tmp_tmp < 13;
           rtb_MatrixConcatenate_tmp_tmp++) {
        status += L_tmp[13 * rtb_MatrixConcatenate_tmp_tmp + i] * L[13 * As_tmp
          + rtb_MatrixConcatenate_tmp_tmp];
      }

      rtb_MatrixConcatenate_tmp = 13 * As_tmp + i;
      Cm[rtb_MatrixConcatenate_tmp] = Nk[rtb_MatrixConcatenate_tmp] + status;
    }
  }

  for (i = 0; i < 13; i++) {
    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      rtb_MatrixConcatenate_tmp = i + 13 * As_tmp;
      L[rtb_MatrixConcatenate_tmp] = 0.0;
      rtb_MatrixConcatenate_tmp_tmp = As_tmp << 2;
      L[rtb_MatrixConcatenate_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp_tmp] * Cm[i];
      L[rtb_MatrixConcatenate_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp_tmp + 1] * Cm[i + 13];
      L[rtb_MatrixConcatenate_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp_tmp + 2] * Cm[i + 26];
      L[rtb_MatrixConcatenate_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp_tmp + 3] * Cm[i + 39];
    }

    xk[i] = (Bu[i + 13] * 0.0 + Bu[i] * 0.0) + (localDW->last_x_PreviousInput[i]
      - b_xoff[i]);
  }

  // SignalConversion generated from: '<S49>/ SFunction ' incorporates:
  //   DataTypeConversion: '<S12>/DataTypeConversion_e1'
  //   DataTypeConversion: '<S12>/DataTypeConversion_e2'
  //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

  rtu_Relativedistance_0[0] = rtu_Relativedistance * 0.02 - y_innov[0];
  rtu_Relativedistance_0[1] = rtu_Longitudinalvelocity * 0.033333333333333333 -
    y_innov[1];
  rtu_Relativedistance_0[2] = rtu_Lateraldeviation - y_innov[2];
  rtu_Relativedistance_0[3] = rtu_Relativeyawangle * 5.0 - y_innov[3];

  // MATLAB Function: '<S48>/FixedHorizonOptimizer' incorporates:
  //   Memory: '<S20>/LastPcov'

  for (i = 0; i < 4; i++) {
    Cm_1[i] = 0.0;
    for (As_tmp = 0; As_tmp < 13; As_tmp++) {
      Cm_1[i] += localB->Cm[(As_tmp << 2) + i] * xk[As_tmp];
    }

    y_innov[i] = rtu_Relativedistance_0[i] - (Cm_1[i] + (Dvm[i + 8] * v_idx_2 +
      (Dvm[i + 4] * rtb_Product + Dvm[i] * rtb_leadvelocity)));
  }

  for (i = 0; i < 13; i++) {
    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      rtb_MatrixConcatenate_tmp_tmp = i + 13 * As_tmp;
      Nk[rtb_MatrixConcatenate_tmp_tmp] = 0.0;
      for (rtb_MatrixConcatenate_tmp = 0; rtb_MatrixConcatenate_tmp < 13;
           rtb_MatrixConcatenate_tmp++) {
        Nk[rtb_MatrixConcatenate_tmp_tmp] += localDW->LastPcov_PreviousInput[13 *
          rtb_MatrixConcatenate_tmp + i] * localB->Cm[(rtb_MatrixConcatenate_tmp
          << 2) + As_tmp];
      }
    }

    for (As_tmp = 0; As_tmp < 4; As_tmp++) {
      rtb_MatrixConcatenate_tmp_tmp = i + 13 * As_tmp;
      tmp_3[rtb_MatrixConcatenate_tmp_tmp] = 0.0;
      rtb_MatrixConcatenate_tmp = As_tmp << 2;
      tmp_3[rtb_MatrixConcatenate_tmp_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp] * Nk[i];
      tmp_3[rtb_MatrixConcatenate_tmp_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp + 1] * Nk[i + 13];
      tmp_3[rtb_MatrixConcatenate_tmp_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp + 2] * Nk[i + 26];
      tmp_3[rtb_MatrixConcatenate_tmp_tmp] +=
        rtb_MatrixConcatenate[rtb_MatrixConcatenate_tmp + 3] * Nk[i + 39];
    }
  }

  // SignalConversion generated from: '<S49>/ SFunction ' incorporates:
  //   Constant: '<S11>/Minimum longitudinal acceleration constant'
  //   Constant: '<S11>/Minimum steering angle constant'
  //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

  tmp_0[0] = rtp_MinAcceleration;
  tmp_0[1] = rtp_MinSteering;

  // SignalConversion generated from: '<S49>/ SFunction ' incorporates:
  //   Constant: '<S11>/Maximum longitudinal acceleration constant'
  //   Constant: '<S11>/Maximum steering angle constant'
  //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

  tmp_1[0] = rtp_MaxAcceleration;
  tmp_1[1] = rtp_MaxSteering;

  // DataTypeConversion: '<S20>/Data Type Conversion6' incorporates:
  //   Constant: '<S11>/Default spacing constant'
  //   Constant: '<S17>/Ymin(2:4) unconstrained constant'
  //   Product: '<S17>/Product2'
  //   Sum: '<S17>/Sum1'

  Cm_1[0] = rtu_Longitudinalvelocity * rtu_Timegap + rtp_DefaultSpacing;
  Cm_1[1] = 0.0;
  Cm_1[2] = 0.0;
  Cm_1[3] = 0.0;

  // MATLAB Function: '<S48>/FixedHorizonOptimizer' incorporates:
  //   UnitDelay: '<S20>/last_mv'

  for (i = 0; i < 13; i++) {
    xk_0[i] = (((tmp_3[i + 13] * y_innov[1] + tmp_3[i] * y_innov[0]) + tmp_3[i +
                26] * y_innov[2]) + tmp_3[i + 39] * y_innov[3]) + xk[i];
  }

  tmp_2[0] = localDW->last_mv_DSTATE[0] - b_uoff[0];
  tmp_2[1] = localDW->last_mv_DSTATE[1] - b_uoff[1];
  std::memset(&tmp_4[0], 0, 80U * sizeof(real_T));
  b_voff[0] = 0.010000000000000002;
  tmp_5[0] = 0.0;
  tmp_6[0] = 0.2;
  b_voff[1] = 0.010000000000000002;
  tmp_5[1] = 0.0;
  tmp_6[1] = 1.9230769230769229;

  // Memory: '<S20>/Memory'
  for (i = 0; i < 18; i++) {
    tmp_8[i] = localDW->Memory_PreviousInput[i];
  }

  // End of Memory: '<S20>/Memory'

  // MATLAB Function: '<S48>/FixedHorizonOptimizer'
  std::memcpy(&bb_0[0], &bb[0], 234U * sizeof(real_T));
  std::memcpy(&cb_0[0], &cb[0], 36U * sizeof(real_T));
  std::memcpy(&localB->db[0], &db[0], 594U * sizeof(real_T));
  std::memcpy(&eb_0[0], &eb[0], 25U * sizeof(real_T));
  std::memcpy(&gb_0[0], &gb[0], 90U * sizeof(real_T));

  // Update for Memory: '<S20>/Memory' incorporates:
  //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

  mpcblock_optimizer(rseq, vseq, tmp_0, tmp_1, Cm_1, localC->Switch2, xk_0,
                     tmp_2, tmp_8, b_Mlim, bb_0, cb_0, localB->db, b_utarget,
                     b_uoff, rtb_Am, LFRefMdl_enable_value, eb_0, gb_0, hb,
                     b_voff, ib, tmp_5, nb, b_A, Bu, Bv, b_C, Dv, r, b_RYscale,
                     tmp_6, localDW->last_mv_DSTATE, rtb_VectorConcatenate,
                     &status, localDW->Memory_PreviousInput, localB);

  // MATLAB Function: '<S48>/FixedHorizonOptimizer'
  for (i = 0; i < 13; i++) {
    for (As_tmp = 0; As_tmp < 13; As_tmp++) {
      rtb_MatrixConcatenate_tmp = i + 13 * As_tmp;
      L_tmp_0[rtb_MatrixConcatenate_tmp] = 0.0;
      for (rtb_MatrixConcatenate_tmp_tmp = 0; rtb_MatrixConcatenate_tmp_tmp < 13;
           rtb_MatrixConcatenate_tmp_tmp++) {
        L_tmp_0[rtb_MatrixConcatenate_tmp] += L_tmp[13 *
          rtb_MatrixConcatenate_tmp_tmp + i] * b_A[13 *
          rtb_MatrixConcatenate_tmp_tmp + As_tmp];
      }

      L_tmp_1[rtb_MatrixConcatenate_tmp] = 0.0;
      L_tmp_1[rtb_MatrixConcatenate_tmp] += Cm[i] * L[As_tmp];
      L_tmp_1[rtb_MatrixConcatenate_tmp] += Cm[i + 13] * L[As_tmp + 13];
      L_tmp_1[rtb_MatrixConcatenate_tmp] += Cm[i + 26] * L[As_tmp + 26];
      L_tmp_1[rtb_MatrixConcatenate_tmp] += Cm[i + 39] * L[As_tmp + 39];
    }
  }

  for (i = 0; i < 169; i++) {
    L_tmp[i] = (L_tmp_0[i] - L_tmp_1[i]) + Qk[i];
  }

  // Gain: '<S20>/u_scale'
  rtyy_LongitudinalaccelerationSteeringangle[0] = 5.0 * localDW->last_mv_DSTATE
    [0];
  rtyy_LongitudinalaccelerationSteeringangle[1] = 0.52 * localDW->
    last_mv_DSTATE[1];

  // Assertion: '<S13>/Assertion' incorporates:
  //   Constant: '<S13>/min_val'
  //   RelationalOperator: '<S13>/min_relop'

  utAssert(0.0 < rtu_Longitudinalvelocity);

  // MATLAB Function: '<S48>/FixedHorizonOptimizer'
  status = localDW->last_mv_DSTATE[0] - b_uoff[0];
  tmp_7 = localDW->last_mv_DSTATE[1] - b_uoff[1];
  for (i = 0; i < 13; i++) {
    // MATLAB Function: '<S48>/FixedHorizonOptimizer'
    xk_0[i] = 0.0;
    for (As_tmp = 0; As_tmp < 13; As_tmp++) {
      // MATLAB Function: '<S48>/FixedHorizonOptimizer'
      rtb_MatrixConcatenate_tmp_tmp = 13 * i + As_tmp;
      rtb_MatrixConcatenate_tmp = 13 * As_tmp + i;

      // Update for Memory: '<S20>/LastPcov' incorporates:
      //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

      localDW->LastPcov_PreviousInput[rtb_MatrixConcatenate_tmp_tmp] =
        (L_tmp[rtb_MatrixConcatenate_tmp_tmp] + L_tmp[rtb_MatrixConcatenate_tmp])
        * 0.5;

      // MATLAB Function: '<S48>/FixedHorizonOptimizer'
      xk_0[i] += b_A[rtb_MatrixConcatenate_tmp] * xk[As_tmp];
    }

    // MATLAB Function: '<S48>/FixedHorizonOptimizer'
    L_0 = L[i + 39] * y_innov[3] + (L[i + 26] * y_innov[2] + (L[i + 13] *
      y_innov[1] + L[i] * y_innov[0]));

    // Update for Memory: '<S20>/last_x' incorporates:
    //   MATLAB Function: '<S48>/FixedHorizonOptimizer'

    localDW->last_x_PreviousInput[i] = ((((Bv[i + 13] * rtb_Product + Bv[i] *
      rtb_leadvelocity) + Bv[i + 26] * v_idx_2) + (xk_0[i] + (Bu[i + 13] * tmp_7
      + Bu[i] * status))) + L_0) + b_xoff[i];
  }
}

//
// File trailer for generated code.
//
// [EOF]
//
