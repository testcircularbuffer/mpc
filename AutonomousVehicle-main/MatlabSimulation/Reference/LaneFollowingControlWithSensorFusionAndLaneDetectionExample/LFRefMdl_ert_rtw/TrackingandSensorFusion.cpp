//
// File: TrackingandSensorFusion.cpp
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "TrackingandSensorFusion.h"

// Include model header file for global data
#include "LFRefMdl.h"
#include "LFRefMdl_private.h"

void PathFollowingControllerRefMdlModelClass::
  DetectionConcatenation_createObjDetStructTemplate(real_T in1_NumDetections,
  boolean_T in1_IsValidTime, const BusVisionDetections in1_Detections[20], const
  BusRadarDetections in2_Detections[50], real_T *out_NumDetections, boolean_T
  *out_IsValidTime, BusDetectionConcatenation1Detections out_Detections[70])
{
  BusDetectionConcatenation1Detections det0;
  int32_T i;
  *out_NumDetections = in1_NumDetections;
  *out_IsValidTime = in1_IsValidTime;
  det0.Time = in1_Detections[0].Time;
  for (i = 0; i < 6; i++) {
    det0.Measurement[i] = 0.0;
  }

  std::memset(&det0.MeasurementNoise[0], 0, 36U * sizeof(real_T));
  det0.SensorIndex = in1_Detections[0].SensorIndex;
  det0.ObjectClassID = in1_Detections[0].ObjectClassID;
  det0.MeasurementParameters.Frame = in1_Detections[0].
    MeasurementParameters.Frame;
  det0.MeasurementParameters.OriginPosition[0] = in1_Detections[0].
    MeasurementParameters.OriginPosition[0];
  det0.MeasurementParameters.OriginPosition[1] = in1_Detections[0].
    MeasurementParameters.OriginPosition[0];
  det0.MeasurementParameters.OriginPosition[2] = in1_Detections[0].
    MeasurementParameters.OriginPosition[0];
  for (i = 0; i < 9; i++) {
    det0.MeasurementParameters.Orientation[i] = in1_Detections[0].
      MeasurementParameters.Orientation[0];
  }

  det0.MeasurementParameters.HasVelocity = in1_Detections[0].
    MeasurementParameters.HasVelocity;
  det0.MeasurementParameters.HasElevation = in2_Detections[0].
    MeasurementParameters.HasElevation;
  det0.ObjectAttributes.TargetIndex = in1_Detections[0].
    ObjectAttributes.TargetIndex;
  det0.ObjectAttributes.SNR = in2_Detections[0].ObjectAttributes.SNR;
  for (i = 0; i < 70; i++) {
    out_Detections[i] = det0;
  }
}

void PathFollowingControllerRefMdlModelClass::nullify_k(const
  BusDetectionConcatenation1Detections in[70],
  BusDetectionConcatenation1Detections out[70])
{
  int32_T b_n;
  int32_T i;
  for (b_n = 0; b_n < 70; b_n++) {
    out[b_n].Time = 0.0;
    for (i = 0; i < 6; i++) {
      out[b_n].Measurement[i] = 0.0;
    }

    std::memset(&out[b_n].MeasurementNoise[0], 0, 36U * sizeof(real_T));
    out[b_n].SensorIndex = 0.0;
    out[b_n].ObjectClassID = 0.0;
    out[b_n].MeasurementParameters.Frame = in[b_n].MeasurementParameters.Frame;
    out[b_n].MeasurementParameters.OriginPosition[0] = 0.0;
    out[b_n].MeasurementParameters.OriginPosition[1] = 0.0;
    out[b_n].MeasurementParameters.OriginPosition[2] = 0.0;
    std::memset(&out[b_n].MeasurementParameters.Orientation[0], 0, 9U * sizeof
                (real_T));
    out[b_n].MeasurementParameters.HasVelocity = false;
    out[b_n].MeasurementParameters.HasElevation = false;
    out[b_n].ObjectAttributes.TargetIndex = 0.0;
    out[b_n].ObjectAttributes.SNR = 0.0;
  }
}

void PathFollowingControllerRefMdlModelClass::nullify(const
  BusDetectionConcatenation1Detections in_Detections[70], real_T
  *out_NumDetections, boolean_T *out_IsValidTime,
  BusDetectionConcatenation1Detections out_Detections[70])
{
  *out_NumDetections = 0.0;
  *out_IsValidTime = false;
  nullify_k(in_Detections, out_Detections);
}

void PathFollowingControllerRefMdlModelClass::DetectionConcatenation_setupImpl
  (driving_internal_DetectionConcatenation_LFRefMdl_T *obj, real_T
   varargin_1_NumDetections, boolean_T varargin_1_IsValidTime, const
   BusVisionDetections varargin_1_Detections[20], const BusRadarDetections
   varargin_2_Detections[50], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  real_T expl_temp;
  boolean_T expl_temp_0;
  DetectionConcatenation_createObjDetStructTemplate(varargin_1_NumDetections,
    varargin_1_IsValidTime, varargin_1_Detections, varargin_2_Detections,
    &expl_temp, &expl_temp_0, localB->b_Detections);
  nullify(localB->b_Detections, &obj->pOutTemp.NumDetections,
          &obj->pOutTemp.IsValidTime, obj->pOutTemp.Detections);
}

void PathFollowingControllerRefMdlModelClass::char_k(drivingCoordinateFrameType
  varargin_1, char_T y_data[], int32_T y_size[2])
{
  static const char_T tmp_0[11] = { 'R', 'e', 'c', 't', 'a', 'n', 'g', 'u', 'l',
    'a', 'r' };

  static const char_T tmp_1[9] = { 'S', 'p', 'h', 'e', 'r', 'i', 'c', 'a', 'l' };

  static const char_T tmp[7] = { 'I', 'n', 'v', 'a', 'l', 'i', 'd' };

  h_cell_wrap_LFRefMdl_T b_0[3];
  h_cell_wrap_LFRefMdl_T b;
  h_cell_wrap_LFRefMdl_T c;
  h_cell_wrap_LFRefMdl_T d;
  int32_T b_k;
  int32_T enumIdx;
  int32_T loop_ub;
  drivingCoordinateFrameType enumVals[3];
  boolean_T exitg1;
  enumVals[0] = Invalid;
  enumVals[1] = Rectangular;
  enumVals[2] = Spherical;
  b.f1.size[0] = 1;
  b.f1.size[1] = 7;
  for (b_k = 0; b_k < 7; b_k++) {
    b.f1.data[b_k] = tmp[b_k];
  }

  c.f1.size[0] = 1;
  c.f1.size[1] = 11;
  for (b_k = 0; b_k < 11; b_k++) {
    c.f1.data[b_k] = tmp_0[b_k];
  }

  d.f1.size[0] = 1;
  d.f1.size[1] = 9;
  for (b_k = 0; b_k < 9; b_k++) {
    d.f1.data[b_k] = tmp_1[b_k];
  }

  enumIdx = -1;
  b_k = 1;
  exitg1 = false;
  while ((!exitg1) && (b_k - 1 < 3)) {
    if (enumVals[b_k - 1] == varargin_1) {
      enumIdx = b_k - 1;
      exitg1 = true;
    } else {
      b_k++;
    }
  }

  y_size[0] = 1;
  b_0[0] = b;
  b_0[1] = c;
  b_0[2] = d;
  y_size[1] = b_0[enumIdx].f1.size[1];
  b_0[0] = b;
  b_0[1] = c;
  b_0[2] = d;
  loop_ub = b_0[enumIdx].f1.size[1] - 1;
  b_0[0] = b;
  b_0[1] = c;
  b_0[2] = d;
  for (b_k = 0; b_k <= loop_ub; b_k++) {
    y_data[b_k] = b_0[enumIdx].f1.data[b_k];
  }
}

boolean_T PathFollowingControllerRefMdlModelClass::strcmp_m(const char_T a_data[],
  const int32_T a_size[2])
{
  static const char_T tmp_0[128] = { '\x00', '\x01', '\x02', '\x03', '\x04',
    '\x05', '\x06', '\x07', '\x08', '	', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e',
    '\x0f', '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
    '\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', ' ', '!',
    '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']',
    '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{',
    '|', '}', '~', '\x7f' };

  static const char_T tmp[11] = { 'r', 'e', 'c', 't', 'a', 'n', 'g', 'u', 'l',
    'a', 'r' };

  int32_T b_kstr;
  int32_T exitg1;
  char_T b[11];
  boolean_T b_bool;
  for (b_kstr = 0; b_kstr < 11; b_kstr++) {
    b[b_kstr] = tmp[b_kstr];
  }

  b_bool = false;
  if (a_size[1] == 11) {
    b_kstr = 1;
    do {
      exitg1 = 0;
      if (b_kstr - 1 < 11) {
        if (tmp_0[static_cast<uint8_T>(a_data[b_kstr - 1])] != tmp_0[
            static_cast<int32_T>(b[b_kstr - 1])]) {
          exitg1 = 1;
        } else {
          b_kstr++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  return b_bool;
}

real_T PathFollowingControllerRefMdlModelClass::norm_m(const real_T x[3])
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  scale = 3.3121686421112381E-170;
  absxk = std::abs(x[0]);
  if (absxk > 3.3121686421112381E-170) {
    y = 1.0;
    scale = absxk;
  } else {
    t = absxk / 3.3121686421112381E-170;
    y = t * t;
  }

  absxk = std::abs(x[1]);
  if (absxk > scale) {
    t = scale / absxk;
    y = y * t * t + 1.0;
    scale = absxk;
  } else {
    t = absxk / scale;
    y += t * t;
  }

  absxk = std::abs(x[2]);
  if (absxk > scale) {
    t = scale / absxk;
    y = y * t * t + 1.0;
    scale = absxk;
  } else {
    t = absxk / scale;
    y += t * t;
  }

  return scale * std::sqrt(y);
}

real_T rt_remd_snf(real_T u0, real_T u1)
{
  real_T u1_0;
  real_T y;
  if (rtIsNaN(u0) || rtIsNaN(u1) || rtIsInf(u0)) {
    y = (rtNaN);
  } else if (rtIsInf(u1)) {
    y = u0;
  } else {
    if (u1 < 0.0) {
      u1_0 = std::ceil(u1);
    } else {
      u1_0 = std::floor(u1);
    }

    if ((u1 != 0.0) && (u1 != u1_0)) {
      u1_0 = std::abs(u0 / u1);
      if (!(std::abs(u1_0 - std::floor(u1_0 + 0.5)) > DBL_EPSILON * u1_0)) {
        y = 0.0 * u0;
      } else {
        y = std::fmod(u0, u1);
      }
    } else {
      y = std::fmod(u0, u1);
    }
  }

  return y;
}

real_T PathFollowingControllerRefMdlModelClass::cosd(real_T x)
{
  real_T absx;
  real_T b_x;
  int8_T n;
  if (rtIsInf(x) || rtIsNaN(x)) {
    b_x = (rtNaN);
  } else {
    b_x = rt_remd_snf(x, 360.0);
    absx = std::abs(b_x);
    if (absx > 180.0) {
      if (b_x > 0.0) {
        b_x -= 360.0;
      } else {
        b_x += 360.0;
      }

      absx = std::abs(b_x);
    }

    if (absx <= 45.0) {
      b_x *= 0.017453292519943295;
      b_x = std::cos(b_x);
    } else {
      if (absx <= 135.0) {
        if (b_x > 0.0) {
          b_x = (b_x - 90.0) * 0.017453292519943295;
          n = 1;
        } else {
          b_x = (b_x + 90.0) * 0.017453292519943295;
          n = -1;
        }
      } else if (b_x > 0.0) {
        b_x = (b_x - 180.0) * 0.017453292519943295;
        n = 2;
      } else {
        b_x = (b_x + 180.0) * 0.017453292519943295;
        n = -2;
      }

      if (n == 1) {
        b_x = -std::sin(b_x);
      } else if (n == -1) {
        b_x = std::sin(b_x);
      } else {
        b_x = -std::cos(b_x);
      }
    }
  }

  return b_x;
}

void PathFollowingControllerRefMdlModelClass::parseDetectionForInitFcn(const
  real_T detection_Measurement[6], const real_T detection_MeasurementNoise[36],
  drivingCoordinateFrameType detection_MeasurementParameters_Frame, const real_T
  detection_MeasurementParameters_OriginPosition[3], const real_T
  detection_MeasurementParameters_Orientation[9], boolean_T
  detection_MeasurementParameters_HasVelocity, boolean_T
  detection_MeasurementParameters_HasElevation, real_T posMeas[3], real_T
  velMeas[3], real_T posCov[9], real_T velCov[9], boolean_T *invalidDet)
{
  static const char_T tmp_0[128] = { '\x00', '\x01', '\x02', '\x03', '\x04',
    '\x05', '\x06', '\x07', '\x08', '	', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e',
    '\x0f', '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
    '\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', ' ', '!',
    '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']',
    '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{',
    '|', '}', '~', '\x7f' };

  static const char_T tmp[7] = { 'I', 'n', 'v', 'a', 'l', 'i', 'd' };

  static const int8_T tmp_1[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  real_T lclCov_data[49];
  real_T Rpos[9];
  real_T b_argsinCell_f4_tmp[9];
  real_T posLclCov[9];
  real_T rot[9];
  real_T rot_0[9];
  real_T velLclCov[9];
  real_T originPosition[3];
  real_T lclCoord_idx_0_tmp;
  real_T lclMeas_idx_0;
  real_T lclMeas_idx_1;
  real_T lclMeas_idx_2;
  real_T lclRect_idx_0;
  real_T lclRect_idx_2;
  real_T rcoselev;
  int32_T a_tmp_size[2];
  int32_T b_kstr;
  int32_T exitg1;
  int32_T lclCov_size_idx_0;
  int32_T posCov_tmp;
  int32_T rot_tmp;
  char_T a_tmp_data[11];
  char_T b[7];
  int8_T posLclCov_tmp[3];
  char_k(detection_MeasurementParameters_Frame, a_tmp_data, a_tmp_size);
  for (b_kstr = 0; b_kstr < 7; b_kstr++) {
    b[b_kstr] = tmp[b_kstr];
  }

  *invalidDet = false;
  if (a_tmp_size[1] == 7) {
    b_kstr = 1;
    do {
      exitg1 = 0;
      if (b_kstr - 1 < 7) {
        if (tmp_0[static_cast<uint8_T>(a_tmp_data[b_kstr - 1])] != tmp_0[
            static_cast<int32_T>(b[b_kstr - 1])]) {
          exitg1 = 1;
        } else {
          b_kstr++;
        }
      } else {
        *invalidDet = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  if (*invalidDet) {
    posMeas[0] = 0.0;
    velMeas[0] = 0.0;
    posMeas[1] = 0.0;
    velMeas[1] = 0.0;
    posMeas[2] = 0.0;
    velMeas[2] = 0.0;
    std::memset(&posCov[0], 0, 9U * sizeof(real_T));
    posCov[0] = 1.0;
    posCov[4] = 1.0;
    posCov[8] = 1.0;
    std::memset(&velCov[0], 0, 9U * sizeof(real_T));
    velCov[0] = 1.0;
    velCov[4] = 1.0;
    velCov[8] = 1.0;
  } else {
    for (b_kstr = 0; b_kstr < 3; b_kstr++) {
      lclRect_idx_0 = 0.0;
      for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++) {
        rot_tmp = 3 * lclCov_size_idx_0 + b_kstr;
        lclRect_idx_0 += static_cast<real_T>(tmp_1[rot_tmp]) *
          detection_MeasurementParameters_OriginPosition[lclCov_size_idx_0];
        b_argsinCell_f4_tmp[rot_tmp] = 0.0;
        b_argsinCell_f4_tmp[rot_tmp] +=
          detection_MeasurementParameters_Orientation[3 * lclCov_size_idx_0] *
          static_cast<real_T>(tmp_1[b_kstr]);
        b_argsinCell_f4_tmp[rot_tmp] +=
          detection_MeasurementParameters_Orientation[3 * lclCov_size_idx_0 + 1]
          * static_cast<real_T>(tmp_1[b_kstr + 3]);
        b_argsinCell_f4_tmp[rot_tmp] +=
          detection_MeasurementParameters_Orientation[3 * lclCov_size_idx_0 + 2]
          * static_cast<real_T>(tmp_1[b_kstr + 6]);
      }

      originPosition[b_kstr] = lclRect_idx_0;
    }

    if (strcmp_m(a_tmp_data, a_tmp_size)) {
      for (b_kstr = 0; b_kstr < 3; b_kstr++) {
        lclMeas_idx_0 = b_argsinCell_f4_tmp[b_kstr];
        lclRect_idx_0 = lclMeas_idx_0 * detection_Measurement[0];
        lclMeas_idx_1 = lclMeas_idx_0 * detection_Measurement[3];
        lclMeas_idx_0 = b_argsinCell_f4_tmp[b_kstr + 3];
        lclRect_idx_0 += lclMeas_idx_0 * detection_Measurement[1];
        lclMeas_idx_1 += lclMeas_idx_0 * detection_Measurement[4];
        lclMeas_idx_0 = b_argsinCell_f4_tmp[b_kstr + 6];
        lclRect_idx_0 += lclMeas_idx_0 * detection_Measurement[2];
        velMeas[b_kstr] = lclMeas_idx_0 * detection_Measurement[5] +
          lclMeas_idx_1;
        posMeas[b_kstr] = originPosition[b_kstr] + lclRect_idx_0;
        posLclCov_tmp[b_kstr] = static_cast<int8_T>(b_kstr + 1);
      }

      for (b_kstr = 0; b_kstr < 3; b_kstr++) {
        rot_tmp = (posLclCov_tmp[b_kstr] - 1) * 6;
        posLclCov[3 * b_kstr] = detection_MeasurementNoise[(rot_tmp +
          posLclCov_tmp[0]) - 1];
        posLclCov[3 * b_kstr + 1] = detection_MeasurementNoise[(rot_tmp +
          posLclCov_tmp[1]) - 1];
        posLclCov[3 * b_kstr + 2] = detection_MeasurementNoise[(rot_tmp +
          posLclCov_tmp[2]) - 1];
      }

      posLclCov_tmp[0] = 4;
      posLclCov_tmp[1] = 5;
      posLclCov_tmp[2] = 6;
      for (b_kstr = 0; b_kstr < 3; b_kstr++) {
        lclCov_size_idx_0 = (posLclCov_tmp[b_kstr] - 1) * 6;
        velLclCov[3 * b_kstr] = detection_MeasurementNoise[lclCov_size_idx_0 + 3];
        velLclCov[3 * b_kstr + 1] = detection_MeasurementNoise[lclCov_size_idx_0
          + 4];
        velLclCov[3 * b_kstr + 2] = detection_MeasurementNoise[lclCov_size_idx_0
          + 5];
      }
    } else {
      if (detection_MeasurementParameters_HasElevation) {
        lclMeas_idx_0 = detection_Measurement[0];
        lclMeas_idx_1 = detection_Measurement[1];
        lclMeas_idx_2 = detection_Measurement[2];
        lclCov_size_idx_0 = 6;
        std::memcpy(&lclCov_data[0], &detection_MeasurementNoise[0], 36U *
                    sizeof(real_T));
      } else {
        lclMeas_idx_0 = detection_Measurement[0];
        lclMeas_idx_1 = 0.0;
        lclMeas_idx_2 = detection_Measurement[1];
        std::memset(&lclCov_data[0], 0, 49U * sizeof(real_T));
        lclCov_data[0] = detection_MeasurementNoise[0];
        lclCov_data[8] = 2704.0;
        for (b_kstr = 0; b_kstr < 5; b_kstr++) {
          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 5; lclCov_size_idx_0++)
          {
            lclCov_data[(lclCov_size_idx_0 + 7 * (b_kstr + 2)) + 2] =
              detection_MeasurementNoise[((b_kstr + 1) * 6 + lclCov_size_idx_0)
              + 1];
          }
        }

        lclCov_size_idx_0 = 7;
      }

      lclCoord_idx_0_tmp = 0.017453292519943295 * lclMeas_idx_0;
      lclMeas_idx_0 = 0.017453292519943295 * lclMeas_idx_1;
      if ((lclMeas_idx_2 < 0.0) || rtIsNaN(lclMeas_idx_2)) {
        lclRect_idx_2 = 0.0;
      } else {
        lclRect_idx_2 = lclMeas_idx_2;
      }

      rcoselev = lclRect_idx_2 * std::cos(lclMeas_idx_0);
      lclRect_idx_0 = rcoselev * std::cos(lclCoord_idx_0_tmp);
      rcoselev *= std::sin(lclCoord_idx_0_tmp);
      lclRect_idx_2 *= std::sin(lclMeas_idx_0);
      for (b_kstr = 0; b_kstr < 3; b_kstr++) {
        posMeas[b_kstr] = ((b_argsinCell_f4_tmp[b_kstr + 3] * rcoselev +
                            b_argsinCell_f4_tmp[b_kstr] * lclRect_idx_0) +
                           b_argsinCell_f4_tmp[b_kstr + 6] * lclRect_idx_2) +
          originPosition[b_kstr];
      }

      if (detection_MeasurementParameters_HasVelocity) {
        originPosition[0] = posMeas[0] - originPosition[0];
        originPosition[1] = posMeas[1] - originPosition[1];
        lclRect_idx_2 = posMeas[2] - originPosition[2];
        originPosition[2] = lclRect_idx_2;
        lclRect_idx_0 = norm_m(originPosition);
        velMeas[0] = detection_Measurement[5] * originPosition[0] /
          lclRect_idx_0;
        velMeas[1] = detection_Measurement[5] * originPosition[1] /
          lclRect_idx_0;
        velMeas[2] = detection_Measurement[5] * lclRect_idx_2 / lclRect_idx_0;
        lclRect_idx_0 = std::sqrt(lclCov_data[(lclCov_size_idx_0 << 1) + 2]);
        if ((lclMeas_idx_2 < lclRect_idx_0) || (rtIsNaN(lclMeas_idx_2) &&
             (!rtIsNaN(lclRect_idx_0)))) {
          lclMeas_idx_2 = lclRect_idx_0;
        }

        originPosition[1] = lclMeas_idx_2 * cosd(lclMeas_idx_1) *
          (0.017453292519943295 * std::sqrt(lclCov_data[0]));
        originPosition[2] = std::sqrt(lclCov_data[lclCov_size_idx_0 + 1]) *
          0.017453292519943295 * lclMeas_idx_2;
        std::memset(&Rpos[0], 0, 9U * sizeof(real_T));
        lclMeas_idx_1 = std::sin(lclCoord_idx_0_tmp);
        lclMeas_idx_2 = std::cos(lclCoord_idx_0_tmp);
        lclCoord_idx_0_tmp = std::sin(lclMeas_idx_0);
        lclMeas_idx_0 = std::cos(lclMeas_idx_0);
        velLclCov[0] = lclMeas_idx_2;
        velLclCov[3] = -lclMeas_idx_1;
        velLclCov[6] = 0.0;
        velLclCov[1] = lclMeas_idx_1;
        velLclCov[4] = lclMeas_idx_2;
        velLclCov[7] = 0.0;
        posLclCov[0] = lclMeas_idx_0;
        posLclCov[1] = 0.0;
        posLclCov[2] = lclCoord_idx_0_tmp;
        Rpos[0] = lclRect_idx_0 * lclRect_idx_0;
        velLclCov[2] = 0.0;
        posLclCov[3] = 0.0;
        Rpos[4] = originPosition[1] * originPosition[1];
        velLclCov[5] = 0.0;
        posLclCov[4] = 1.0;
        Rpos[8] = originPosition[2] * originPosition[2];
        velLclCov[8] = 1.0;
        posLclCov[5] = 0.0;
        posLclCov[6] = -lclCoord_idx_0_tmp;
        posLclCov[7] = 0.0;
        posLclCov[8] = lclMeas_idx_0;
        for (b_kstr = 0; b_kstr < 3; b_kstr++) {
          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = lclCov_size_idx_0 + 3 * b_kstr;
            rot[rot_tmp] = 0.0;
            rot[rot_tmp] += posLclCov[3 * b_kstr] * velLclCov[lclCov_size_idx_0];
            rot[rot_tmp] += posLclCov[3 * b_kstr + 1] *
              velLclCov[lclCov_size_idx_0 + 3];
            rot[rot_tmp] += posLclCov[3 * b_kstr + 2] *
              velLclCov[lclCov_size_idx_0 + 6];
          }
        }

        for (b_kstr = 0; b_kstr < 9; b_kstr++) {
          velLclCov[b_kstr] = 100.0 * static_cast<real_T>(tmp_1[b_kstr]);
        }

        for (b_kstr = 0; b_kstr < 3; b_kstr++) {
          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
            rot_0[rot_tmp] = 0.0;
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0] * rot[b_kstr];
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 1] * rot[b_kstr + 3];
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 2] * rot[b_kstr + 6];
          }

          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
            posLclCov[rot_tmp] = 0.0;
            posLclCov[rot_tmp] += rot_0[b_kstr] * rot[lclCov_size_idx_0];
            posLclCov[rot_tmp] += rot_0[b_kstr + 3] * rot[lclCov_size_idx_0 + 3];
            posLclCov[rot_tmp] += rot_0[b_kstr + 6] * rot[lclCov_size_idx_0 + 6];
          }
        }
      } else {
        velMeas[0] = 0.0;
        velMeas[1] = 0.0;
        velMeas[2] = 0.0;
        lclRect_idx_0 = std::sqrt(lclCov_data[(lclCov_size_idx_0 << 1) + 2]);
        if ((lclMeas_idx_2 < lclRect_idx_0) || (rtIsNaN(lclMeas_idx_2) &&
             (!rtIsNaN(lclRect_idx_0)))) {
          lclMeas_idx_2 = lclRect_idx_0;
        }

        originPosition[1] = lclMeas_idx_2 * cosd(lclMeas_idx_1) *
          (0.017453292519943295 * std::sqrt(lclCov_data[0]));
        originPosition[2] = std::sqrt(lclCov_data[lclCov_size_idx_0 + 1]) *
          0.017453292519943295 * lclMeas_idx_2;
        std::memset(&Rpos[0], 0, 9U * sizeof(real_T));
        lclMeas_idx_1 = std::sin(lclCoord_idx_0_tmp);
        lclMeas_idx_2 = std::cos(lclCoord_idx_0_tmp);
        lclCoord_idx_0_tmp = std::sin(lclMeas_idx_0);
        lclMeas_idx_0 = std::cos(lclMeas_idx_0);
        velLclCov[0] = lclMeas_idx_2;
        velLclCov[3] = -lclMeas_idx_1;
        velLclCov[6] = 0.0;
        velLclCov[1] = lclMeas_idx_1;
        velLclCov[4] = lclMeas_idx_2;
        velLclCov[7] = 0.0;
        posLclCov[0] = lclMeas_idx_0;
        posLclCov[1] = 0.0;
        posLclCov[2] = lclCoord_idx_0_tmp;
        Rpos[0] = lclRect_idx_0 * lclRect_idx_0;
        velLclCov[2] = 0.0;
        posLclCov[3] = 0.0;
        Rpos[4] = originPosition[1] * originPosition[1];
        velLclCov[5] = 0.0;
        posLclCov[4] = 1.0;
        Rpos[8] = originPosition[2] * originPosition[2];
        velLclCov[8] = 1.0;
        posLclCov[5] = 0.0;
        posLclCov[6] = -lclCoord_idx_0_tmp;
        posLclCov[7] = 0.0;
        posLclCov[8] = lclMeas_idx_0;
        for (b_kstr = 0; b_kstr < 3; b_kstr++) {
          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
            rot[rot_tmp] = 0.0;
            rot[rot_tmp] += posLclCov[3 * lclCov_size_idx_0] * velLclCov[b_kstr];
            rot[rot_tmp] += posLclCov[3 * lclCov_size_idx_0 + 1] *
              velLclCov[b_kstr + 3];
            rot[rot_tmp] += posLclCov[3 * lclCov_size_idx_0 + 2] *
              velLclCov[b_kstr + 6];
          }

          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
            rot_0[rot_tmp] = 0.0;
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0] * rot[b_kstr];
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 1] * rot[b_kstr + 3];
            rot_0[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 2] * rot[b_kstr + 6];
          }
        }

        for (b_kstr = 0; b_kstr < 3; b_kstr++) {
          for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++)
          {
            rot_tmp = lclCov_size_idx_0 + 3 * b_kstr;
            posLclCov[rot_tmp] = 0.0;
            posLclCov[rot_tmp] += rot_0[lclCov_size_idx_0] * rot[b_kstr];
            posLclCov[rot_tmp] += rot_0[lclCov_size_idx_0 + 3] * rot[b_kstr + 3];
            posLclCov[rot_tmp] += rot_0[lclCov_size_idx_0 + 6] * rot[b_kstr + 6];
          }
        }

        for (b_kstr = 0; b_kstr < 9; b_kstr++) {
          velLclCov[b_kstr] = 100.0 * static_cast<real_T>(tmp_1[b_kstr]);
        }
      }
    }

    for (b_kstr = 0; b_kstr < 3; b_kstr++) {
      for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++) {
        rot_tmp = 3 * b_kstr + lclCov_size_idx_0;
        Rpos[b_kstr + 3 * lclCov_size_idx_0] = b_argsinCell_f4_tmp[rot_tmp];
        rot[rot_tmp] = 0.0;
        rot[rot_tmp] += posLclCov[3 * b_kstr] *
          b_argsinCell_f4_tmp[lclCov_size_idx_0];
        rot[rot_tmp] += posLclCov[3 * b_kstr + 1] *
          b_argsinCell_f4_tmp[lclCov_size_idx_0 + 3];
        rot[rot_tmp] += posLclCov[3 * b_kstr + 2] *
          b_argsinCell_f4_tmp[lclCov_size_idx_0 + 6];
      }
    }

    for (b_kstr = 0; b_kstr < 3; b_kstr++) {
      for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++) {
        rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
        posCov[rot_tmp] = 0.0;
        rot_0[rot_tmp] = 0.0;
        posCov[rot_tmp] += Rpos[3 * lclCov_size_idx_0] * rot[b_kstr];
        rot_0[rot_tmp] += velLclCov[3 * lclCov_size_idx_0] *
          b_argsinCell_f4_tmp[b_kstr];
        posCov_tmp = 3 * lclCov_size_idx_0 + 1;
        posCov[rot_tmp] += Rpos[posCov_tmp] * rot[b_kstr + 3];
        rot_0[rot_tmp] += velLclCov[posCov_tmp] * b_argsinCell_f4_tmp[b_kstr + 3];
        posCov_tmp = 3 * lclCov_size_idx_0 + 2;
        posCov[rot_tmp] += Rpos[posCov_tmp] * rot[b_kstr + 6];
        rot_0[rot_tmp] += velLclCov[posCov_tmp] * b_argsinCell_f4_tmp[b_kstr + 6];
      }

      for (lclCov_size_idx_0 = 0; lclCov_size_idx_0 < 3; lclCov_size_idx_0++) {
        rot_tmp = b_kstr + 3 * lclCov_size_idx_0;
        velCov[rot_tmp] = 0.0;
        velCov[rot_tmp] += Rpos[3 * lclCov_size_idx_0] * rot_0[b_kstr];
        velCov[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 1] * rot_0[b_kstr + 3];
        velCov[rot_tmp] += Rpos[3 * lclCov_size_idx_0 + 2] * rot_0[b_kstr + 6];
      }
    }
  }
}

real_T PathFollowingControllerRefMdlModelClass::maximum_p(const real_T x[6])
{
  real_T ex;
  real_T tmp;
  int32_T b_idx;
  int32_T k;
  boolean_T exitg1;
  if (!rtIsNaN(x[0])) {
    b_idx = 1;
  } else {
    b_idx = 0;
    k = 2;
    exitg1 = false;
    while ((!exitg1) && (k < 7)) {
      if (!rtIsNaN(x[k - 1])) {
        b_idx = k;
        exitg1 = true;
      } else {
        k++;
      }
    }
  }

  if (b_idx == 0) {
    ex = x[0];
  } else {
    ex = x[b_idx - 1];
    for (k = b_idx + 1; k < 7; k++) {
      tmp = x[k - 1];
      if (ex < tmp) {
        ex = tmp;
      }
    }
  }

  return ex;
}

void PathFollowingControllerRefMdlModelClass::cholesky(const real_T A_data[],
  const int32_T A_size[2], real_T b_A_data[], int32_T b_A_size[2], int32_T *info)
{
  real_T c_A_data[36];
  real_T c;
  real_T ssq;
  int32_T b;
  int32_T b_j;
  int32_T c_A_size_idx_0;
  int32_T d;
  int32_T ia;
  int32_T iac;
  int32_T idxA1j;
  int32_T idxAjj;
  int32_T ix;
  int32_T iy;
  int32_T jm1;
  int32_T loop_ub_tmp;
  int32_T n;
  int32_T nmj;
  boolean_T exitg1;
  n = A_size[1];
  c_A_size_idx_0 = A_size[0];
  loop_ub_tmp = A_size[0] * A_size[1] - 1;
  if (0 <= loop_ub_tmp) {
    std::memcpy(&c_A_data[0], &A_data[0], (loop_ub_tmp + 1) * sizeof(real_T));
  }

  *info = 0;
  b_j = 1;
  exitg1 = false;
  while ((!exitg1) && (b_j - 1 <= n - 1)) {
    jm1 = b_j - 2;
    idxA1j = (b_j - 1) * n + 1;
    idxAjj = (b_j + idxA1j) - 2;
    ssq = 0.0;
    if (b_j - 1 >= 1) {
      ix = idxA1j - 1;
      iy = idxA1j - 1;
      for (nmj = 0; nmj <= jm1; nmj++) {
        ssq += c_A_data[ix] * c_A_data[iy];
        ix++;
        iy++;
      }
    }

    ssq = c_A_data[idxAjj] - ssq;
    if (ssq > 0.0) {
      ssq = std::sqrt(ssq);
      c_A_data[idxAjj] = ssq;
      if (b_j < n) {
        nmj = (n - b_j) - 1;
        jm1 = idxA1j + n;
        idxAjj = (idxAjj + n) + 1;
        if ((b_j - 1 == 0) || (nmj + 1 == 0)) {
        } else {
          iy = idxAjj - 1;
          b = n * nmj + jm1;
          for (iac = jm1; n < 0 ? iac >= b : iac <= b; iac += n) {
            ix = idxA1j - 1;
            c = 0.0;
            d = (b_j + iac) - 1;
            for (ia = iac; ia < d; ia++) {
              c += c_A_data[ia - 1] * c_A_data[ix];
              ix++;
            }

            c_A_data[iy] += -c;
            iy += n;
          }
        }

        ssq = 1.0 / ssq;
        b = n * nmj + idxAjj;
        for (nmj = idxAjj; n < 0 ? nmj >= b : nmj <= b; nmj += n) {
          c_A_data[nmj - 1] *= ssq;
        }
      }

      b_j++;
    } else {
      c_A_data[idxAjj] = ssq;
      *info = b_j;
      exitg1 = true;
    }
  }

  if (0 <= loop_ub_tmp) {
    std::memcpy(&b_A_data[0], &c_A_data[0], (loop_ub_tmp + 1) * sizeof(real_T));
  }

  if (*info == 0) {
    n = A_size[1];
  } else {
    n = *info - 1;
  }

  for (b_j = 0; b_j < n; b_j++) {
    for (loop_ub_tmp = b_j + 2; loop_ub_tmp <= n; loop_ub_tmp++) {
      b_A_data[(loop_ub_tmp + c_A_size_idx_0 * b_j) - 1] = 0.0;
    }
  }

  if (1 > n) {
    n = 0;
    idxA1j = 0;
  } else {
    idxA1j = n;
  }

  for (b_j = 0; b_j < idxA1j; b_j++) {
    for (loop_ub_tmp = 0; loop_ub_tmp < n; loop_ub_tmp++) {
      c_A_data[loop_ub_tmp + n * b_j] = b_A_data[c_A_size_idx_0 * b_j +
        loop_ub_tmp];
    }
  }

  b_A_size[0] = n;
  b_A_size[1] = idxA1j;
  idxA1j *= n;
  if (0 <= idxA1j - 1) {
    std::memcpy(&b_A_data[0], &c_A_data[0], idxA1j * sizeof(real_T));
  }
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n(int32_T n, const real_T
  x[36], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  if (n >= 1) {
    if (n == 1) {
      y = std::abs(x[ix0 - 1]);
    } else {
      scale = 3.3121686421112381E-170;
      kend = ix0 + n;
      for (k = ix0; k < kend; k++) {
        absxk = std::abs(x[k - 1]);
        if (absxk > scale) {
          t = scale / absxk;
          y = y * t * t + 1.0;
          scale = absxk;
        } else {
          t = absxk / scale;
          y += t * t;
        }
      }

      y = scale * std::sqrt(y);
    }
  }

  return y;
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj(int32_T n, const
  real_T x[6], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  kend = ix0 + n;
  for (k = ix0; k < kend; k++) {
    absxk = std::abs(x[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

real_T PathFollowingControllerRefMdlModelClass::xdotc(int32_T n, const real_T x
  [36], int32_T ix0, const real_T y[36], int32_T iy0)
{
  real_T d;
  int32_T ix;
  int32_T iy;
  int32_T k;
  d = 0.0;
  if (n >= 1) {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      d += x[ix] * y[iy];
      ix++;
      iy++;
    }
  }

  return d;
}

void PathFollowingControllerRefMdlModelClass::xaxpy(int32_T n, real_T a, int32_T
  ix0, const real_T y[36], int32_T iy0, real_T b_y[36])
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_y[0], &y[0], 36U * sizeof(real_T));
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      b_y[iy] += b_y[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xaxpy_f0cuq0(int32_T n, real_T a,
  const real_T x[36], int32_T ix0, real_T y[6], int32_T iy0)
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += x[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xrotg(real_T a, real_T b, real_T
  *b_a, real_T *b_b, real_T *c, real_T *s)
{
  real_T absa;
  real_T absb;
  real_T ads;
  real_T bds;
  real_T roe;
  real_T scale;
  roe = b;
  absa = std::abs(a);
  absb = std::abs(b);
  if (absa > absb) {
    roe = a;
  }

  scale = absa + absb;
  if (scale == 0.0) {
    *s = 0.0;
    *c = 1.0;
    *b_a = 0.0;
    *b_b = 0.0;
  } else {
    ads = absa / scale;
    bds = absb / scale;
    *b_a = std::sqrt(ads * ads + bds * bds) * scale;
    if (roe < 0.0) {
      *b_a = -*b_a;
    }

    *c = a / *b_a;
    *s = b / *b_a;
    if (absa > absb) {
      *b_b = *s;
    } else if (*c != 0.0) {
      *b_b = 1.0 / *c;
    } else {
      *b_b = 1.0;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xaxpy_f0(int32_T n, real_T a,
  const real_T x[6], int32_T ix0, const real_T y[36], int32_T iy0, real_T b_y[36])
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_y[0], &y[0], 36U * sizeof(real_T));
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      b_y[iy] += x[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xrot(const real_T x[36], int32_T
  ix0, int32_T iy0, real_T c, real_T s, real_T b_x[36])
{
  real_T temp;
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_x[0], &x[0], 36U * sizeof(real_T));
  ix = ix0 - 1;
  iy = iy0 - 1;
  for (k = 0; k < 6; k++) {
    temp = c * b_x[ix] + s * b_x[iy];
    b_x[iy] = c * b_x[iy] - s * b_x[ix];
    b_x[ix] = temp;
    iy++;
    ix++;
  }
}

void PathFollowingControllerRefMdlModelClass::xswap(const real_T x[36], int32_T
  ix0, int32_T iy0, real_T b_x[36])
{
  real_T temp;
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_x[0], &x[0], 36U * sizeof(real_T));
  ix = ix0 - 1;
  iy = iy0 - 1;
  for (k = 0; k < 6; k++) {
    temp = b_x[ix];
    b_x[ix] = b_x[iy];
    b_x[iy] = temp;
    ix++;
    iy++;
  }
}

void PathFollowingControllerRefMdlModelClass::svd(const real_T A[36], real_T U
  [36], real_T s[6], real_T V[36])
{
  real_T A_0[36];
  real_T A_1[36];
  real_T e[6];
  real_T s_0[6];
  real_T work[6];
  real_T b;
  real_T d_sn;
  real_T emm1;
  real_T nrm;
  real_T r;
  real_T rt;
  real_T smm1;
  real_T unusedU2;
  real_T ztest;
  int32_T b_0;
  int32_T i;
  int32_T qjj;
  int32_T qp1;
  int32_T qp1jj;
  int32_T qq;
  boolean_T apply_transform;
  boolean_T exitg1;
  std::memcpy(&A_0[0], &A[0], 36U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    s_0[i] = 0.0;
    e[i] = 0.0;
    work[i] = 0.0;
  }

  std::memset(&U[0], 0, 36U * sizeof(real_T));
  std::memset(&V[0], 0, 36U * sizeof(real_T));
  for (i = 0; i < 5; i++) {
    qp1 = i + 2;
    qp1jj = 6 * i + i;
    qq = qp1jj + 1;
    apply_transform = false;
    nrm = xnrm2_n(6 - i, A_0, qp1jj + 1);
    if (nrm > 0.0) {
      apply_transform = true;
      if (A_0[qp1jj] < 0.0) {
        s_0[i] = -nrm;
      } else {
        s_0[i] = nrm;
      }

      if (std::abs(s_0[i]) >= 1.0020841800044864E-292) {
        nrm = 1.0 / s_0[i];
        b_0 = (qp1jj - i) + 1;
        for (qjj = qq; qjj <= b_0 + 5; qjj++) {
          A_0[qjj - 1] *= nrm;
        }
      } else {
        b_0 = (qp1jj - i) + 1;
        for (qjj = qq; qjj <= b_0 + 5; qjj++) {
          A_0[qjj - 1] /= s_0[i];
        }
      }

      A_0[qp1jj]++;
      s_0[i] = -s_0[i];
    } else {
      s_0[i] = 0.0;
    }

    for (qq = qp1; qq < 7; qq++) {
      qjj = ((qq - 1) * 6 + i) + 1;
      if (apply_transform) {
        std::memcpy(&A_1[0], &A_0[0], 36U * sizeof(real_T));
        xaxpy(6 - i, -(xdotc(6 - i, A_0, qp1jj + 1, A_0, qjj) / A_0[qp1jj]),
              qp1jj + 1, A_1, qjj, A_0);
      }

      e[qq - 1] = A_0[qjj - 1];
    }

    for (qq = i + 1; qq < 7; qq++) {
      qjj = (6 * i + qq) - 1;
      U[qjj] = A_0[qjj];
    }

    if (i + 1 <= 4) {
      nrm = xnrm2_n1aj(5 - i, e, i + 2);
      if (nrm == 0.0) {
        e[i] = 0.0;
      } else {
        if (e[i + 1] < 0.0) {
          e[i] = -nrm;
        } else {
          e[i] = nrm;
        }

        nrm = e[i];
        if (std::abs(e[i]) >= 1.0020841800044864E-292) {
          nrm = 1.0 / e[i];
          for (qjj = qp1; qjj < 7; qjj++) {
            e[qjj - 1] *= nrm;
          }
        } else {
          for (qjj = qp1; qjj < 7; qjj++) {
            e[qjj - 1] /= nrm;
          }
        }

        e[i + 1]++;
        e[i] = -e[i];
        for (qq = qp1; qq < 7; qq++) {
          work[qq - 1] = 0.0;
        }

        for (qq = qp1; qq < 7; qq++) {
          xaxpy_f0cuq0(5 - i, e[qq - 1], A_0, (i + 6 * (qq - 1)) + 2, work, i +
                       2);
        }

        for (qq = qp1; qq < 7; qq++) {
          std::memcpy(&A_1[0], &A_0[0], 36U * sizeof(real_T));
          xaxpy_f0(5 - i, -e[qq - 1] / e[i + 1], work, i + 2, A_1, ((qq - 1) * 6
                    + i) + 2, A_0);
        }
      }

      for (qq = qp1; qq < 7; qq++) {
        V[(qq + 6 * i) - 1] = e[qq - 1];
      }
    }
  }

  i = 5;
  s_0[5] = A_0[35];
  e[4] = A_0[34];
  e[5] = 0.0;
  for (qp1 = 0; qp1 < 6; qp1++) {
    U[qp1 + 30] = 0.0;
  }

  U[35] = 1.0;
  for (qp1 = 4; qp1 >= 0; qp1--) {
    qq = 6 * qp1 + qp1;
    if (s_0[qp1] != 0.0) {
      for (qp1jj = qp1 + 2; qp1jj < 7; qp1jj++) {
        qjj = ((qp1jj - 1) * 6 + qp1) + 1;
        std::memcpy(&A_0[0], &U[0], 36U * sizeof(real_T));
        xaxpy(6 - qp1, -(xdotc(6 - qp1, U, qq + 1, U, qjj) / U[qq]), qq + 1, A_0,
              qjj, U);
      }

      for (qp1jj = qp1 + 1; qp1jj < 7; qp1jj++) {
        qjj = (6 * qp1 + qp1jj) - 1;
        U[qjj] = -U[qjj];
      }

      U[qq]++;
      if (0 <= qp1 - 1) {
        std::memset(&U[qp1 * 6], 0, ((qp1 - 1) + 1) * sizeof(real_T));
      }
    } else {
      for (qjj = 0; qjj < 6; qjj++) {
        U[qjj + 6 * qp1] = 0.0;
      }

      U[qq] = 1.0;
    }
  }

  for (qp1 = 5; qp1 >= 0; qp1--) {
    if ((qp1 + 1 <= 4) && (e[qp1] != 0.0)) {
      qq = (6 * qp1 + qp1) + 2;
      for (qjj = qp1 + 2; qjj < 7; qjj++) {
        qp1jj = ((qjj - 1) * 6 + qp1) + 2;
        std::memcpy(&A_0[0], &V[0], 36U * sizeof(real_T));
        xaxpy(5 - qp1, -(xdotc(5 - qp1, V, qq, V, qp1jj) / V[qq - 1]), qq, A_0,
              qp1jj, V);
      }
    }

    for (qq = 0; qq < 6; qq++) {
      V[qq + 6 * qp1] = 0.0;
    }

    V[qp1 + 6 * qp1] = 1.0;
  }

  qp1 = 0;
  nrm = 0.0;
  for (qq = 0; qq < 6; qq++) {
    ztest = e[qq];
    if (s_0[qq] != 0.0) {
      rt = std::abs(s_0[qq]);
      r = s_0[qq] / rt;
      s_0[qq] = rt;
      if (qq + 1 < 6) {
        ztest /= r;
      }

      qp1jj = 6 * qq;
      for (qjj = qp1jj + 1; qjj <= qp1jj + 6; qjj++) {
        U[qjj - 1] *= r;
      }
    }

    if ((qq + 1 < 6) && (ztest != 0.0)) {
      rt = std::abs(ztest);
      r = rt / ztest;
      ztest = rt;
      s_0[qq + 1] *= r;
      qp1jj = (qq + 1) * 6;
      for (qjj = qp1jj + 1; qjj <= qp1jj + 6; qjj++) {
        V[qjj - 1] *= r;
      }
    }

    b = std::abs(s_0[qq]);
    rt = std::abs(ztest);
    if ((b > rt) || rtIsNaN(rt)) {
      rt = b;
    }

    if ((!(nrm > rt)) && (!rtIsNaN(rt))) {
      nrm = rt;
    }

    e[qq] = ztest;
  }

  while ((i + 1 > 0) && (!(qp1 >= 75))) {
    qq = i;
    qjj = i;
    exitg1 = false;
    while ((!exitg1) && (qjj > -1)) {
      qq = qjj;
      if (qjj == 0) {
        exitg1 = true;
      } else {
        rt = std::abs(e[qjj - 1]);
        if ((rt <= (std::abs(s_0[qjj - 1]) + std::abs(s_0[qjj])) *
             2.2204460492503131E-16) || (rt <= 1.0020841800044864E-292) || ((qp1
              > 20) && (rt <= 2.2204460492503131E-16 * nrm))) {
          e[qjj - 1] = 0.0;
          exitg1 = true;
        } else {
          qjj--;
        }
      }
    }

    if (qq == i) {
      qp1jj = 4;
    } else {
      qjj = i + 1;
      qp1jj = i + 1;
      exitg1 = false;
      while ((!exitg1) && (qp1jj >= qq)) {
        qjj = qp1jj;
        if (qp1jj == qq) {
          exitg1 = true;
        } else {
          rt = 0.0;
          if (qp1jj < i + 1) {
            rt = std::abs(e[qp1jj - 1]);
          }

          if (qp1jj > qq + 1) {
            rt += std::abs(e[qp1jj - 2]);
          }

          ztest = std::abs(s_0[qp1jj - 1]);
          if ((ztest <= 2.2204460492503131E-16 * rt) || (ztest <=
               1.0020841800044864E-292)) {
            s_0[qp1jj - 1] = 0.0;
            exitg1 = true;
          } else {
            qp1jj--;
          }
        }
      }

      if (qjj == qq) {
        qp1jj = 3;
      } else if (i + 1 == qjj) {
        qp1jj = 1;
      } else {
        qp1jj = 2;
        qq = qjj;
      }
    }

    switch (qp1jj) {
     case 1:
      rt = e[i - 1];
      e[i - 1] = 0.0;
      for (qjj = i; qjj >= qq + 1; qjj--) {
        xrotg(s_0[qjj - 1], rt, &s_0[qjj - 1], &rt, &r, &b);
        if (qjj > qq + 1) {
          ztest = e[qjj - 2];
          rt = ztest * -b;
          e[qjj - 2] = ztest * r;
        }

        std::memcpy(&A_0[0], &V[0], 36U * sizeof(real_T));
        xrot(A_0, (qjj - 1) * 6 + 1, 6 * i + 1, r, b, V);
      }
      break;

     case 2:
      rt = e[qq - 1];
      e[qq - 1] = 0.0;
      for (qjj = qq + 1; qjj <= i + 1; qjj++) {
        xrotg(s_0[qjj - 1], rt, &s_0[qjj - 1], &ztest, &r, &b);
        ztest = e[qjj - 1];
        rt = ztest * -b;
        e[qjj - 1] = ztest * r;
        std::memcpy(&A_0[0], &U[0], 36U * sizeof(real_T));
        xrot(A_0, (qjj - 1) * 6 + 1, (qq - 1) * 6 + 1, r, b, U);
      }
      break;

     case 3:
      b = std::abs(s_0[i]);
      r = s_0[i - 1];
      rt = std::abs(r);
      if ((b > rt) || rtIsNaN(rt)) {
        rt = b;
      }

      b = e[i - 1];
      ztest = std::abs(b);
      if ((rt > ztest) || rtIsNaN(ztest)) {
        ztest = rt;
      }

      rt = std::abs(s_0[qq]);
      if ((ztest > rt) || rtIsNaN(rt)) {
        rt = ztest;
      }

      ztest = std::abs(e[qq]);
      if ((rt > ztest) || rtIsNaN(ztest)) {
        ztest = rt;
      }

      rt = s_0[i] / ztest;
      smm1 = r / ztest;
      emm1 = b / ztest;
      r = s_0[qq] / ztest;
      b = ((smm1 + rt) * (smm1 - rt) + emm1 * emm1) / 2.0;
      smm1 = rt * emm1;
      smm1 *= smm1;
      if ((b != 0.0) || (smm1 != 0.0)) {
        emm1 = std::sqrt(b * b + smm1);
        if (b < 0.0) {
          emm1 = -emm1;
        }

        emm1 = smm1 / (b + emm1);
      } else {
        emm1 = 0.0;
      }

      rt = (r + rt) * (r - rt) + emm1;
      r *= e[qq] / ztest;
      for (b_0 = qq + 1; b_0 <= i; b_0++) {
        xrotg(rt, r, &ztest, &emm1, &b, &smm1);
        if (b_0 > qq + 1) {
          e[b_0 - 2] = ztest;
        }

        ztest = e[b_0 - 1];
        rt = s_0[b_0 - 1];
        e[b_0 - 1] = ztest * b - rt * smm1;
        r = smm1 * s_0[b_0];
        s_0[b_0] *= b;
        qjj = (b_0 - 1) * 6 + 1;
        qp1jj = 6 * b_0 + 1;
        std::memcpy(&A_0[0], &V[0], 36U * sizeof(real_T));
        xrot(A_0, qjj, qp1jj, b, smm1, V);
        xrotg(rt * b + ztest * smm1, r, &s_0[b_0 - 1], &unusedU2, &emm1, &d_sn);
        rt = e[b_0 - 1] * emm1 + d_sn * s_0[b_0];
        s_0[b_0] = e[b_0 - 1] * -d_sn + emm1 * s_0[b_0];
        r = d_sn * e[b_0];
        e[b_0] *= emm1;
        std::memcpy(&A_0[0], &U[0], 36U * sizeof(real_T));
        xrot(A_0, qjj, qp1jj, emm1, d_sn, U);
      }

      e[i - 1] = rt;
      qp1++;
      break;

     default:
      if (s_0[qq] < 0.0) {
        s_0[qq] = -s_0[qq];
        qp1jj = 6 * qq;
        for (qjj = qp1jj + 1; qjj <= qp1jj + 6; qjj++) {
          V[qjj - 1] = -V[qjj - 1];
        }
      }

      qp1 = qq + 1;
      while ((qq + 1 < 6) && (s_0[qq] < s_0[qp1])) {
        rt = s_0[qq];
        s_0[qq] = s_0[qp1];
        s_0[qp1] = rt;
        qjj = 6 * qq + 1;
        qp1jj = (qq + 1) * 6 + 1;
        std::memcpy(&A_0[0], &V[0], 36U * sizeof(real_T));
        xswap(A_0, qjj, qp1jj, V);
        std::memcpy(&A_0[0], &U[0], 36U * sizeof(real_T));
        xswap(A_0, qjj, qp1jj, U);
        qq = qp1;
        qp1++;
      }

      qp1 = 0;
      i--;
      break;
    }
  }

  for (i = 0; i < 6; i++) {
    s[i] = s_0[i];
  }
}

void PathFollowingControllerRefMdlModelClass::cholPSD(const real_T A[36], real_T
  value[36])
{
  static const int32_T tmp[2] = { 6, 6 };

  real_T Ss[36];
  real_T b_B_data[36];
  real_T s[6];
  real_T c;
  real_T ssq;
  int32_T b_B_size[2];
  int32_T d;
  int32_T i;
  int32_T ia;
  int32_T idxA1j;
  int32_T idxAjj;
  int32_T info;
  int32_T ix;
  int32_T iy;
  int32_T jm1;
  int32_T k;
  boolean_T exitg1;
  boolean_T p;
  cholesky(A, tmp, b_B_data, b_B_size, &info);
  if (info == 0) {
    std::memcpy(&b_B_data[0], &A[0], 36U * sizeof(real_T));
    i = 0;
    info = 1;
    exitg1 = false;
    while ((!exitg1) && (info - 1 < 6)) {
      jm1 = info - 2;
      idxA1j = (info - 1) * 6 + 1;
      idxAjj = (info + idxA1j) - 2;
      ssq = 0.0;
      if (info - 1 >= 1) {
        ix = idxA1j - 1;
        iy = idxA1j - 1;
        for (k = 0; k <= jm1; k++) {
          ssq += b_B_data[ix] * b_B_data[iy];
          ix++;
          iy++;
        }
      }

      ssq = b_B_data[idxAjj] - ssq;
      if (ssq > 0.0) {
        ssq = std::sqrt(ssq);
        b_B_data[idxAjj] = ssq;
        if (info < 6) {
          if (info - 1 != 0) {
            iy = idxAjj + 6;
            jm1 = (5 - info) * 6 + idxA1j;
            for (k = idxA1j + 6; k <= jm1 + 6; k += 6) {
              ix = idxA1j - 1;
              c = 0.0;
              d = (info + k) - 1;
              for (ia = k; ia < d; ia++) {
                c += b_B_data[ia - 1] * b_B_data[ix];
                ix++;
              }

              b_B_data[iy] += -c;
              iy += 6;
            }
          }

          ssq = 1.0 / ssq;
          jm1 = (5 - info) * 6 + idxAjj;
          for (k = idxAjj + 7; k <= jm1 + 7; k += 6) {
            b_B_data[k - 1] *= ssq;
          }
        }

        info++;
      } else {
        b_B_data[idxAjj] = ssq;
        i = info;
        exitg1 = true;
      }
    }

    if (i == 0) {
      idxA1j = 6;
    } else {
      idxA1j = i - 1;
    }

    for (info = 0; info < idxA1j; info++) {
      for (i = info + 2; i <= idxA1j; i++) {
        b_B_data[(i + 6 * info) - 1] = 0.0;
      }
    }

    for (info = 0; info < 6; info++) {
      for (i = 0; i < 6; i++) {
        value[i + 6 * info] = b_B_data[6 * i + info];
      }
    }
  } else {
    p = true;
    for (info = 0; info < 36; info++) {
      if (p) {
        ssq = A[info];
        if ((!rtIsInf(ssq)) && (!rtIsNaN(ssq))) {
        } else {
          p = false;
        }
      } else {
        p = false;
      }
    }

    if (p) {
      svd(A, Ss, s, b_B_data);
    } else {
      for (i = 0; i < 6; i++) {
        s[i] = (rtNaN);
      }

      for (info = 0; info < 36; info++) {
        b_B_data[info] = (rtNaN);
      }
    }

    std::memset(&Ss[0], 0, 36U * sizeof(real_T));
    for (k = 0; k < 6; k++) {
      Ss[k + 6 * k] = s[k];
    }

    for (info = 0; info < 36; info++) {
      Ss[info] = std::sqrt(Ss[info]);
    }

    for (info = 0; info < 6; info++) {
      for (i = 0; i < 6; i++) {
        idxA1j = i + 6 * info;
        value[idxA1j] = 0.0;
        for (idxAjj = 0; idxAjj < 6; idxAjj++) {
          value[idxA1j] += b_B_data[6 * idxAjj + i] * Ss[6 * info + idxAjj];
        }
      }
    }
  }
}

void PathFollowingControllerRefMdlModelClass::
  ExtendedKalmanFilter_set_StateCovariance(c_trackingEKF_LFRefMdl_T *obj, const
  real_T value[36])
{
  real_T varargin_1[6];
  real_T absx;
  int32_T b_exponent;
  int32_T k;
  for (k = 0; k < 6; k++) {
    varargin_1[k] = std::abs(value[6 * k + k]);
  }

  absx = std::abs(maximum_p(varargin_1));
  if ((!rtIsInf(absx)) && (!rtIsNaN(absx)) && (!(absx <= 2.2250738585072014E-308)))
  {
    frexp(absx, &b_exponent);
  }

  cholPSD(value, obj->pSqrtStateCovariance);
  obj->pIsSetStateCovariance = true;
  obj->pSqrtStateCovarianceScalar = -1.0;
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj1(int32_T n, const
  real_T x[9], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  kend = ix0 + n;
  for (k = ix0; k < kend; k++) {
    absxk = std::abs(x[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj1q(const real_T x[3],
  int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  for (k = ix0; k <= ix0 + 1; k++) {
    absxk = std::abs(x[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

real_T PathFollowingControllerRefMdlModelClass::xdotc_p(int32_T n, const real_T
  x[9], int32_T ix0, const real_T y[9], int32_T iy0)
{
  real_T d;
  int32_T ix;
  int32_T iy;
  int32_T k;
  d = 0.0;
  if (n >= 1) {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      d += x[ix] * y[iy];
      ix++;
      iy++;
    }
  }

  return d;
}

void PathFollowingControllerRefMdlModelClass::xaxpy_f0c(int32_T n, real_T a,
  int32_T ix0, const real_T y[9], int32_T iy0, real_T b_y[9])
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_y[0], &y[0], 9U * sizeof(real_T));
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      b_y[iy] += b_y[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xaxpy_f0cuq0u(int32_T n, real_T a,
  const real_T x[9], int32_T ix0, real_T y[3], int32_T iy0)
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += x[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xaxpy_f0cuq(int32_T n, real_T a,
  const real_T x[3], int32_T ix0, const real_T y[9], int32_T iy0, real_T b_y[9])
{
  int32_T ix;
  int32_T iy;
  int32_T k;
  std::memcpy(&b_y[0], &y[0], 9U * sizeof(real_T));
  if (!(a == 0.0)) {
    ix = ix0;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      b_y[iy] += x[ix - 1] * a;
      ix++;
      iy++;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xrot_g(const real_T x[9], int32_T
  ix0, int32_T iy0, real_T c, real_T s, real_T b_x[9])
{
  real_T temp;
  real_T temp_tmp;
  std::memcpy(&b_x[0], &x[0], 9U * sizeof(real_T));
  temp = b_x[iy0 - 1];
  temp_tmp = b_x[ix0 - 1];
  b_x[iy0 - 1] = temp * c - temp_tmp * s;
  b_x[ix0 - 1] = temp_tmp * c + temp * s;
  temp = b_x[ix0] * c + b_x[iy0] * s;
  b_x[iy0] = b_x[iy0] * c - b_x[ix0] * s;
  b_x[ix0] = temp;
  temp = b_x[iy0 + 1];
  temp_tmp = b_x[ix0 + 1];
  b_x[iy0 + 1] = temp * c - temp_tmp * s;
  b_x[ix0 + 1] = temp_tmp * c + temp * s;
}

void PathFollowingControllerRefMdlModelClass::xswap_n(const real_T x[9], int32_T
  ix0, int32_T iy0, real_T b_x[9])
{
  real_T temp;
  std::memcpy(&b_x[0], &x[0], 9U * sizeof(real_T));
  temp = b_x[ix0 - 1];
  b_x[ix0 - 1] = b_x[iy0 - 1];
  b_x[iy0 - 1] = temp;
  temp = b_x[ix0];
  b_x[ix0] = b_x[iy0];
  b_x[iy0] = temp;
  temp = b_x[ix0 + 1];
  b_x[ix0 + 1] = b_x[iy0 + 1];
  b_x[iy0 + 1] = temp;
}

void PathFollowingControllerRefMdlModelClass::svd_i(const real_T A[9], real_T U
  [9], real_T s[3], real_T V[9])
{
  real_T A_0[9];
  real_T A_1[9];
  real_T e[3];
  real_T s_0[3];
  real_T work[3];
  real_T b;
  real_T d_sn;
  real_T emm1;
  real_T nrm;
  real_T rt;
  real_T smm1;
  real_T sqds;
  real_T unusedU2;
  real_T ztest;
  int32_T c_q;
  int32_T d_k;
  int32_T kase;
  int32_T m;
  int32_T qjj;
  int32_T qq;
  boolean_T apply_transform;
  boolean_T exitg1;
  e[0] = 0.0;
  work[0] = 0.0;
  e[1] = 0.0;
  work[1] = 0.0;
  e[2] = 0.0;
  work[2] = 0.0;
  for (m = 0; m < 9; m++) {
    A_0[m] = A[m];
    U[m] = 0.0;
    V[m] = 0.0;
  }

  apply_transform = false;
  nrm = xnrm2_n1aj1(3, A_0, 1);
  if (nrm > 0.0) {
    apply_transform = true;
    if (A_0[0] < 0.0) {
      s_0[0] = -nrm;
    } else {
      s_0[0] = nrm;
    }

    if (std::abs(s_0[0]) >= 1.0020841800044864E-292) {
      nrm = 1.0 / s_0[0];
      for (qq = 1; qq < 4; qq++) {
        A_0[qq - 1] *= nrm;
      }
    } else {
      for (qq = 1; qq < 4; qq++) {
        A_0[qq - 1] /= s_0[0];
      }
    }

    A_0[0]++;
    s_0[0] = -s_0[0];
  } else {
    s_0[0] = 0.0;
  }

  for (m = 2; m < 4; m++) {
    qjj = (m - 1) * 3 + 1;
    if (apply_transform) {
      std::memcpy(&A_1[0], &A_0[0], 9U * sizeof(real_T));
      xaxpy_f0c(3, -(xdotc_p(3, A_0, 1, A_0, qjj) / A_0[0]), 1, A_1, qjj, A_0);
    }

    e[m - 1] = A_0[qjj - 1];
  }

  for (m = 1; m < 4; m++) {
    U[m - 1] = A_0[m - 1];
  }

  nrm = xnrm2_n1aj1q(e, 2);
  if (nrm == 0.0) {
    e[0] = 0.0;
  } else {
    if (e[1] < 0.0) {
      rt = -nrm;
      e[0] = -nrm;
    } else {
      rt = nrm;
      e[0] = nrm;
    }

    if (std::abs(rt) >= 1.0020841800044864E-292) {
      nrm = 1.0 / rt;
      for (qq = 2; qq < 4; qq++) {
        e[qq - 1] *= nrm;
      }
    } else {
      for (qq = 2; qq < 4; qq++) {
        e[qq - 1] /= rt;
      }
    }

    e[1]++;
    e[0] = -e[0];
    for (m = 2; m < 4; m++) {
      work[m - 1] = 0.0;
    }

    for (m = 2; m < 4; m++) {
      xaxpy_f0cuq0u(2, e[m - 1], A_0, 3 * (m - 1) + 2, work, 2);
    }

    for (m = 2; m < 4; m++) {
      std::memcpy(&A_1[0], &A_0[0], 9U * sizeof(real_T));
      xaxpy_f0cuq(2, -e[m - 1] / e[1], work, 2, A_1, (m - 1) * 3 + 2, A_0);
    }
  }

  for (m = 2; m < 4; m++) {
    V[m - 1] = e[m - 1];
  }

  apply_transform = false;
  nrm = xnrm2_n1aj1(2, A_0, 5);
  if (nrm > 0.0) {
    apply_transform = true;
    if (A_0[4] < 0.0) {
      s_0[1] = -nrm;
    } else {
      s_0[1] = nrm;
    }

    if (std::abs(s_0[1]) >= 1.0020841800044864E-292) {
      nrm = 1.0 / s_0[1];
      for (qq = 5; qq < 7; qq++) {
        A_0[qq - 1] *= nrm;
      }
    } else {
      for (qq = 5; qq < 7; qq++) {
        A_0[qq - 1] /= s_0[1];
      }
    }

    A_0[4]++;
    s_0[1] = -s_0[1];
  } else {
    s_0[1] = 0.0;
  }

  if (apply_transform) {
    for (m = 3; m < 4; m++) {
      std::memcpy(&A_1[0], &A_0[0], 9U * sizeof(real_T));
      xaxpy_f0c(2, -(xdotc_p(2, A_0, 5, A_0, 8) / A_0[4]), 5, A_1, 8, A_0);
    }
  }

  for (m = 2; m < 4; m++) {
    U[m + 2] = A_0[m + 2];
  }

  m = 2;
  s_0[2] = A_0[8];
  e[1] = A_0[7];
  e[2] = 0.0;
  U[6] = 0.0;
  U[7] = 0.0;
  U[8] = 1.0;
  for (c_q = 1; c_q >= 0; c_q--) {
    qq = 3 * c_q + c_q;
    if (s_0[c_q] != 0.0) {
      for (kase = c_q + 2; kase < 4; kase++) {
        qjj = ((kase - 1) * 3 + c_q) + 1;
        std::memcpy(&A_0[0], &U[0], 9U * sizeof(real_T));
        xaxpy_f0c(3 - c_q, -(xdotc_p(3 - c_q, U, qq + 1, U, qjj) / U[qq]), qq +
                  1, A_0, qjj, U);
      }

      for (qjj = c_q + 1; qjj < 4; qjj++) {
        kase = (3 * c_q + qjj) - 1;
        U[kase] = -U[kase];
      }

      U[qq]++;
      if (0 <= c_q - 1) {
        U[3 * c_q] = 0.0;
      }
    } else {
      U[3 * c_q] = 0.0;
      U[3 * c_q + 1] = 0.0;
      U[3 * c_q + 2] = 0.0;
      U[qq] = 1.0;
    }
  }

  for (c_q = 2; c_q >= 0; c_q--) {
    if ((c_q + 1 <= 1) && (e[0] != 0.0)) {
      std::memcpy(&A_0[0], &V[0], 9U * sizeof(real_T));
      xaxpy_f0c(2, -(xdotc_p(2, V, 2, V, 5) / V[1]), 2, A_0, 5, V);
      std::memcpy(&A_0[0], &V[0], 9U * sizeof(real_T));
      xaxpy_f0c(2, -(xdotc_p(2, V, 2, V, 8) / V[1]), 2, A_0, 8, V);
    }

    V[3 * c_q] = 0.0;
    V[3 * c_q + 1] = 0.0;
    V[3 * c_q + 2] = 0.0;
    V[c_q + 3 * c_q] = 1.0;
  }

  for (c_q = 0; c_q < 3; c_q++) {
    ztest = e[c_q];
    if (s_0[c_q] != 0.0) {
      rt = std::abs(s_0[c_q]);
      nrm = s_0[c_q] / rt;
      s_0[c_q] = rt;
      if (c_q + 1 < 3) {
        ztest /= nrm;
      }

      qjj = 3 * c_q;
      for (qq = qjj + 1; qq <= qjj + 3; qq++) {
        U[qq - 1] *= nrm;
      }
    }

    if ((c_q + 1 < 3) && (ztest != 0.0)) {
      rt = std::abs(ztest);
      nrm = rt / ztest;
      ztest = rt;
      s_0[c_q + 1] *= nrm;
      qjj = (c_q + 1) * 3;
      for (qq = qjj + 1; qq <= qjj + 3; qq++) {
        V[qq - 1] *= nrm;
      }
    }

    e[c_q] = ztest;
  }

  qq = 0;
  nrm = 0.0;
  ztest = std::abs(s_0[0]);
  rt = std::abs(e[0]);
  if ((ztest > rt) || rtIsNaN(rt)) {
    rt = ztest;
  }

  if (!rtIsNaN(rt)) {
    nrm = rt;
  }

  ztest = std::abs(s_0[1]);
  rt = std::abs(e[1]);
  if ((ztest > rt) || rtIsNaN(rt)) {
    rt = ztest;
  }

  if ((!(nrm > rt)) && (!rtIsNaN(rt))) {
    nrm = rt;
  }

  ztest = std::abs(s_0[2]);
  rt = std::abs(e[2]);
  if ((ztest > rt) || rtIsNaN(rt)) {
    rt = ztest;
  }

  if ((!(nrm > rt)) && (!rtIsNaN(rt))) {
    nrm = rt;
  }

  while ((m + 1 > 0) && (!(qq >= 75))) {
    c_q = m;
    qjj = m;
    exitg1 = false;
    while ((!exitg1) && (qjj > -1)) {
      c_q = qjj;
      if (qjj == 0) {
        exitg1 = true;
      } else {
        rt = std::abs(e[qjj - 1]);
        if ((rt <= (std::abs(s_0[qjj - 1]) + std::abs(s_0[qjj])) *
             2.2204460492503131E-16) || (rt <= 1.0020841800044864E-292) || ((qq >
              20) && (rt <= 2.2204460492503131E-16 * nrm))) {
          e[qjj - 1] = 0.0;
          exitg1 = true;
        } else {
          qjj--;
        }
      }
    }

    if (c_q == m) {
      kase = 4;
    } else {
      qjj = m + 1;
      kase = m + 1;
      exitg1 = false;
      while ((!exitg1) && (kase >= c_q)) {
        qjj = kase;
        if (kase == c_q) {
          exitg1 = true;
        } else {
          rt = 0.0;
          if (kase < m + 1) {
            rt = std::abs(e[kase - 1]);
          }

          if (kase > c_q + 1) {
            rt += std::abs(e[kase - 2]);
          }

          ztest = std::abs(s_0[kase - 1]);
          if ((ztest <= 2.2204460492503131E-16 * rt) || (ztest <=
               1.0020841800044864E-292)) {
            s_0[kase - 1] = 0.0;
            exitg1 = true;
          } else {
            kase--;
          }
        }
      }

      if (qjj == c_q) {
        kase = 3;
      } else if (m + 1 == qjj) {
        kase = 1;
      } else {
        kase = 2;
        c_q = qjj;
      }
    }

    switch (kase) {
     case 1:
      rt = e[m - 1];
      e[m - 1] = 0.0;
      for (qjj = m; qjj >= c_q + 1; qjj--) {
        ztest = e[0];
        xrotg(s_0[qjj - 1], rt, &s_0[qjj - 1], &rt, &sqds, &b);
        if (qjj > c_q + 1) {
          rt = -b * e[0];
          ztest = e[0] * sqds;
        }

        std::memcpy(&A_0[0], &V[0], 9U * sizeof(real_T));
        xrot_g(A_0, (qjj - 1) * 3 + 1, 3 * m + 1, sqds, b, V);
        e[0] = ztest;
      }
      break;

     case 2:
      rt = e[c_q - 1];
      e[c_q - 1] = 0.0;
      for (qjj = c_q + 1; qjj <= m + 1; qjj++) {
        xrotg(s_0[qjj - 1], rt, &s_0[qjj - 1], &ztest, &sqds, &b);
        ztest = e[qjj - 1];
        rt = ztest * -b;
        e[qjj - 1] = ztest * sqds;
        std::memcpy(&A_0[0], &U[0], 9U * sizeof(real_T));
        xrot_g(A_0, (qjj - 1) * 3 + 1, (c_q - 1) * 3 + 1, sqds, b, U);
      }
      break;

     case 3:
      ztest = std::abs(s_0[m]);
      sqds = s_0[m - 1];
      rt = std::abs(sqds);
      if ((ztest > rt) || rtIsNaN(rt)) {
        rt = ztest;
      }

      b = e[m - 1];
      ztest = std::abs(b);
      if ((rt > ztest) || rtIsNaN(ztest)) {
        ztest = rt;
      }

      rt = std::abs(s_0[c_q]);
      if ((ztest > rt) || rtIsNaN(rt)) {
        rt = ztest;
      }

      ztest = std::abs(e[c_q]);
      if ((rt > ztest) || rtIsNaN(ztest)) {
        ztest = rt;
      }

      rt = s_0[m] / ztest;
      smm1 = sqds / ztest;
      emm1 = b / ztest;
      sqds = s_0[c_q] / ztest;
      b = ((smm1 + rt) * (smm1 - rt) + emm1 * emm1) / 2.0;
      smm1 = rt * emm1;
      smm1 *= smm1;
      if ((b != 0.0) || (smm1 != 0.0)) {
        emm1 = std::sqrt(b * b + smm1);
        if (b < 0.0) {
          emm1 = -emm1;
        }

        emm1 = smm1 / (b + emm1);
      } else {
        emm1 = 0.0;
      }

      rt = (sqds + rt) * (sqds - rt) + emm1;
      sqds *= e[c_q] / ztest;
      for (d_k = c_q + 1; d_k <= m; d_k++) {
        xrotg(rt, sqds, &ztest, &emm1, &b, &smm1);
        if (d_k > c_q + 1) {
          e[0] = ztest;
        }

        ztest = e[d_k - 1];
        rt = s_0[d_k - 1];
        e[d_k - 1] = ztest * b - rt * smm1;
        sqds = smm1 * s_0[d_k];
        s_0[d_k] *= b;
        qjj = (d_k - 1) * 3 + 1;
        kase = 3 * d_k + 1;
        std::memcpy(&A_0[0], &V[0], 9U * sizeof(real_T));
        xrot_g(A_0, qjj, kase, b, smm1, V);
        xrotg(rt * b + ztest * smm1, sqds, &s_0[d_k - 1], &unusedU2, &emm1,
              &d_sn);
        rt = e[d_k - 1] * emm1 + d_sn * s_0[d_k];
        s_0[d_k] = e[d_k - 1] * -d_sn + emm1 * s_0[d_k];
        sqds = d_sn * e[d_k];
        e[d_k] *= emm1;
        std::memcpy(&A_0[0], &U[0], 9U * sizeof(real_T));
        xrot_g(A_0, qjj, kase, emm1, d_sn, U);
      }

      e[m - 1] = rt;
      qq++;
      break;

     default:
      if (s_0[c_q] < 0.0) {
        s_0[c_q] = -s_0[c_q];
        qjj = 3 * c_q;
        for (qq = qjj + 1; qq <= qjj + 3; qq++) {
          V[qq - 1] = -V[qq - 1];
        }
      }

      qq = c_q + 1;
      while ((c_q + 1 < 3) && (s_0[c_q] < s_0[qq])) {
        rt = s_0[c_q];
        s_0[c_q] = s_0[qq];
        s_0[qq] = rt;
        qjj = 3 * c_q + 1;
        kase = (c_q + 1) * 3 + 1;
        std::memcpy(&A_0[0], &V[0], 9U * sizeof(real_T));
        xswap_n(A_0, qjj, kase, V);
        std::memcpy(&A_0[0], &U[0], 9U * sizeof(real_T));
        xswap_n(A_0, qjj, kase, U);
        c_q = qq;
        qq++;
      }

      qq = 0;
      m--;
      break;
    }
  }

  s[0] = s_0[0];
  s[1] = s_0[1];
  s[2] = s_0[2];
}

void PathFollowingControllerRefMdlModelClass::svdPSD(const real_T A[9], real_T
  R[9])
{
  real_T Ss[9];
  real_T V[9];
  real_T s[3];
  real_T x;
  int32_T R_tmp;
  int32_T b_k;
  int32_T i;
  boolean_T p;
  p = true;
  for (b_k = 0; b_k < 9; b_k++) {
    if (p) {
      x = A[b_k];
      if ((!rtIsInf(x)) && (!rtIsNaN(x))) {
      } else {
        p = false;
      }
    } else {
      p = false;
    }
  }

  if (p) {
    svd_i(A, Ss, s, V);
  } else {
    s[0] = (rtNaN);
    s[1] = (rtNaN);
    s[2] = (rtNaN);
    for (b_k = 0; b_k < 9; b_k++) {
      V[b_k] = (rtNaN);
    }
  }

  std::memset(&Ss[0], 0, 9U * sizeof(real_T));
  Ss[0] = s[0];
  Ss[4] = s[1];
  Ss[8] = s[2];
  for (b_k = 0; b_k < 9; b_k++) {
    Ss[b_k] = std::sqrt(Ss[b_k]);
  }

  for (b_k = 0; b_k < 3; b_k++) {
    for (i = 0; i < 3; i++) {
      R_tmp = i + 3 * b_k;
      R[R_tmp] = 0.0;
      R[R_tmp] += Ss[3 * b_k] * V[i];
      R[R_tmp] += Ss[3 * b_k + 1] * V[i + 3];
      R[R_tmp] += Ss[3 * b_k + 2] * V[i + 6];
    }
  }
}

void PathFollowingControllerRefMdlModelClass::
  ExtendedKalmanFilter_set_MeasurementNoise(c_trackingEKF_LFRefMdl_T *obj, const
  real_T value[36])
{
  real_T varargin_1[6];
  real_T absx;
  int32_T b_exponent;
  int32_T k;
  for (k = 0; k < 6; k++) {
    varargin_1[k] = std::abs(value[6 * k + k]);
  }

  absx = std::abs(maximum_p(varargin_1));
  if ((!rtIsInf(absx)) && (!rtIsNaN(absx)) && (!(absx <= 2.2250738585072014E-308)))
  {
    frexp(absx, &b_exponent);
  }

  cholPSD(value, obj->pSqrtMeasurementNoise);
  obj->pIsSetMeasurementNoise = true;
  obj->pSqrtMeasurementNoiseScalar = -1.0;
}

c_trackingEKF_LFRefMdl_T *PathFollowingControllerRefMdlModelClass::initcvekf(
  const real_T Detection_Measurement[6], const real_T
  Detection_MeasurementNoise[36], drivingCoordinateFrameType
  Detection_MeasurementParameters_Frame, const real_T
  Detection_MeasurementParameters_OriginPosition[3], const real_T
  Detection_MeasurementParameters_Orientation[9], boolean_T
  Detection_MeasurementParameters_HasVelocity, boolean_T
  Detection_MeasurementParameters_HasElevation, c_trackingEKF_LFRefMdl_T *iobj_0)
{
  static const real_T tmp[9] = { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };

  static const real_T tmp_c[9] = { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };

  static const int32_T tmp_b[2] = { 3, 3 };

  static const int8_T tmp_9[18] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1,
    0, 0, 0 };

  static const int8_T tmp_a[18] = { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
    0, 0, 1 };

  c_trackingEKF_LFRefMdl_T *EKF;
  real_T b_B_data[36];
  real_T tmp_4[36];
  real_T tmp_5[36];
  real_T tmp_2[18];
  real_T tmp_3[18];
  real_T posCov[9];
  real_T velCov[9];
  real_T tmp_0[6];
  real_T tmp_1[6];
  real_T posMeas[3];
  real_T velMeas[3];
  int32_T b_B_size[2];
  int32_T i;
  int32_T info;
  int32_T tmp_6;
  int32_T tmp_7;
  int32_T tmp_8;
  boolean_T invalidDet;
  parseDetectionForInitFcn(Detection_Measurement, Detection_MeasurementNoise,
    Detection_MeasurementParameters_Frame,
    Detection_MeasurementParameters_OriginPosition,
    Detection_MeasurementParameters_Orientation,
    Detection_MeasurementParameters_HasVelocity,
    Detection_MeasurementParameters_HasElevation, posMeas, velMeas, posCov,
    velCov, &invalidDet);
  if (invalidDet) {
    iobj_0[1].pIsFirstCallPredict = true;
    iobj_0[1].pIsFirstCallCorrect = true;
    EKF = &iobj_0[1];
    for (info = 0; info < 6; info++) {
      i = 3 * info + 1;
      tmp_6 = 3 * info + 2;
      tmp_0[info] = static_cast<real_T>(tmp_9[tmp_6]) * posMeas[2] + (
        static_cast<real_T>(tmp_9[i]) * posMeas[1] + static_cast<real_T>(tmp_9[3
        * info]) * posMeas[0]);
      tmp_1[info] = static_cast<real_T>(tmp_a[tmp_6]) * velMeas[2] + (
        static_cast<real_T>(tmp_a[i]) * velMeas[1] + static_cast<real_T>(tmp_a[3
        * info]) * velMeas[0]);
    }

    for (info = 0; info < 6; info++) {
      iobj_0[1].pState[info] = tmp_0[info] + tmp_1[info];
    }

    iobj_0[1].pSqrtStateCovarianceScalar = 1.0;
    for (info = 0; info < 6; info++) {
      for (i = 0; i < 3; i++) {
        tmp_6 = info + 6 * i;
        tmp_2[tmp_6] = 0.0;
        tmp_3[tmp_6] = 0.0;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[3 * info]) * posCov[3 * i];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[3 * info]) * velCov[3 * i];
        tmp_7 = 3 * info + 1;
        tmp_8 = 3 * i + 1;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * posCov[tmp_8];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * velCov[tmp_8];
        tmp_7 = 3 * info + 2;
        tmp_8 = 3 * i + 2;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * posCov[tmp_8];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * velCov[tmp_8];
      }

      for (i = 0; i < 6; i++) {
        tmp_6 = info + 6 * i;
        b_B_data[tmp_6] = 0.0;
        tmp_4[tmp_6] = 0.0;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[3 * i]) * tmp_2[info];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[3 * i]) * tmp_3[info];
        tmp_7 = 3 * i + 1;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * tmp_2[info + 6];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * tmp_3[info + 6];
        tmp_7 = 3 * i + 2;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * tmp_2[info + 12];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * tmp_3[info + 12];
      }
    }

    for (info = 0; info < 36; info++) {
      tmp_5[info] = b_B_data[info] + tmp_4[info];
    }

    ExtendedKalmanFilter_set_StateCovariance(&iobj_0[1], tmp_5);
    iobj_0[1].pIsValidStateTransitionFcn = false;
    iobj_0[1].pIsValidMeasurementFcn = false;
    iobj_0[1].pIsValidMeasurementFcn = false;
    iobj_0[1].pIsValidStateTransitionFcn = false;
    iobj_0[1].pSqrtProcessNoiseScalar = 1.0;
    cholesky(tmp, tmp_b, b_B_data, b_B_size, &info);
    svdPSD(tmp_c, posCov);
    if (info == 0) {
      for (info = 0; info < 3; info++) {
        iobj_0[1].pSqrtProcessNoise[3 * info] = tmp_c[info];
        iobj_0[1].pSqrtProcessNoise[3 * info + 1] = tmp_c[info + 3];
        iobj_0[1].pSqrtProcessNoise[3 * info + 2] = tmp_c[info + 6];
      }
    } else {
      for (info = 0; info < 3; info++) {
        iobj_0[1].pSqrtProcessNoise[3 * info] = posCov[3 * info];
        i = 3 * info + 1;
        iobj_0[1].pSqrtProcessNoise[i] = posCov[i];
        i = 3 * info + 2;
        iobj_0[1].pSqrtProcessNoise[i] = posCov[i];
      }
    }

    iobj_0[1].pIsSetProcessNoise = true;
    iobj_0[1].pSqrtProcessNoiseScalar = -1.0;
    iobj_0[1].pSqrtMeasurementNoiseScalar = 1.0;
    iobj_0[1].pHasPrediction = false;
  } else {
    iobj_0[0].pIsFirstCallPredict = true;
    iobj_0[0].pIsFirstCallCorrect = true;
    EKF = &iobj_0[0];
    for (info = 0; info < 6; info++) {
      i = 3 * info + 1;
      tmp_6 = 3 * info + 2;
      tmp_0[info] = static_cast<real_T>(tmp_9[tmp_6]) * posMeas[2] + (
        static_cast<real_T>(tmp_9[i]) * posMeas[1] + static_cast<real_T>(tmp_9[3
        * info]) * posMeas[0]);
      tmp_1[info] = static_cast<real_T>(tmp_a[tmp_6]) * velMeas[2] + (
        static_cast<real_T>(tmp_a[i]) * velMeas[1] + static_cast<real_T>(tmp_a[3
        * info]) * velMeas[0]);
    }

    for (info = 0; info < 6; info++) {
      iobj_0[0].pState[info] = tmp_0[info] + tmp_1[info];
    }

    iobj_0[0].pSqrtStateCovarianceScalar = 1.0;
    for (info = 0; info < 6; info++) {
      for (i = 0; i < 3; i++) {
        tmp_6 = info + 6 * i;
        tmp_2[tmp_6] = 0.0;
        tmp_3[tmp_6] = 0.0;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[3 * info]) * posCov[3 * i];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[3 * info]) * velCov[3 * i];
        tmp_7 = 3 * info + 1;
        tmp_8 = 3 * i + 1;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * posCov[tmp_8];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * velCov[tmp_8];
        tmp_7 = 3 * info + 2;
        tmp_8 = 3 * i + 2;
        tmp_2[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * posCov[tmp_8];
        tmp_3[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * velCov[tmp_8];
      }

      for (i = 0; i < 6; i++) {
        tmp_6 = info + 6 * i;
        b_B_data[tmp_6] = 0.0;
        tmp_4[tmp_6] = 0.0;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[3 * i]) * tmp_2[info];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[3 * i]) * tmp_3[info];
        tmp_7 = 3 * i + 1;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * tmp_2[info + 6];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * tmp_3[info + 6];
        tmp_7 = 3 * i + 2;
        b_B_data[tmp_6] += static_cast<real_T>(tmp_9[tmp_7]) * tmp_2[info + 12];
        tmp_4[tmp_6] += static_cast<real_T>(tmp_a[tmp_7]) * tmp_3[info + 12];
      }
    }

    for (info = 0; info < 36; info++) {
      tmp_5[info] = b_B_data[info] + tmp_4[info];
    }

    ExtendedKalmanFilter_set_StateCovariance(&iobj_0[0], tmp_5);
    iobj_0[0].pIsValidStateTransitionFcn = false;
    iobj_0[0].pIsValidMeasurementFcn = false;
    iobj_0[0].pIsValidMeasurementFcn = false;
    iobj_0[0].pIsValidStateTransitionFcn = false;
    iobj_0[0].pSqrtProcessNoiseScalar = 1.0;
    cholesky(tmp, tmp_b, b_B_data, b_B_size, &info);
    svdPSD(tmp_c, posCov);
    if (info == 0) {
      for (info = 0; info < 3; info++) {
        iobj_0[0].pSqrtProcessNoise[3 * info] = tmp_c[info];
        iobj_0[0].pSqrtProcessNoise[3 * info + 1] = tmp_c[info + 3];
        iobj_0[0].pSqrtProcessNoise[3 * info + 2] = tmp_c[info + 6];
      }
    } else {
      for (info = 0; info < 3; info++) {
        iobj_0[0].pSqrtProcessNoise[3 * info] = posCov[3 * info];
        i = 3 * info + 1;
        iobj_0[0].pSqrtProcessNoise[i] = posCov[i];
        i = 3 * info + 2;
        iobj_0[0].pSqrtProcessNoise[i] = posCov[i];
      }
    }

    iobj_0[0].pIsSetProcessNoise = true;
    iobj_0[0].pSqrtProcessNoiseScalar = -1.0;
    iobj_0[0].pSqrtMeasurementNoiseScalar = 1.0;
    ExtendedKalmanFilter_set_MeasurementNoise(&iobj_0[0],
      Detection_MeasurementNoise);
    iobj_0[0].pHasPrediction = false;
  }

  return EKF;
}

c_trackingEKF_LFRefMdl_T *PathFollowingControllerRefMdlModelClass::
  ExtendedKalmanFilter_clone(const c_trackingEKF_LFRefMdl_T *EKF,
  c_trackingEKF_LFRefMdl_T *iobj_0)
{
  c_trackingEKF_LFRefMdl_T *newEKF;
  real_T value[36];
  real_T value_0[9];
  real_T EKF_0[6];
  int32_T i;
  iobj_0->pIsFirstCallPredict = true;
  iobj_0->pIsFirstCallCorrect = true;
  newEKF = iobj_0;
  iobj_0->pSqrtStateCovarianceScalar = 1.0;
  iobj_0->pIsValidMeasurementFcn = false;
  iobj_0->pIsValidStateTransitionFcn = false;
  iobj_0->pSqrtProcessNoiseScalar = 1.0;
  iobj_0->pSqrtMeasurementNoiseScalar = 1.0;
  iobj_0->pHasPrediction = false;
  iobj_0->pIsSetStateCovariance = EKF->pIsSetStateCovariance;
  iobj_0->pIsSetProcessNoise = EKF->pIsSetProcessNoise;
  iobj_0->pIsSetMeasurementNoise = EKF->pIsSetMeasurementNoise;
  for (i = 0; i < 6; i++) {
    EKF_0[i] = EKF->pState[i];
  }

  for (i = 0; i < 6; i++) {
    iobj_0->pState[i] = EKF_0[i];
  }

  iobj_0->pIsValidStateTransitionFcn = false;
  iobj_0->pIsValidMeasurementFcn = false;
  for (i = 0; i < 36; i++) {
    value[i] = EKF->pSqrtStateCovariance[i];
  }

  for (i = 0; i < 36; i++) {
    iobj_0->pSqrtStateCovariance[i] = value[i];
  }

  iobj_0->pIsSetStateCovariance = true;
  iobj_0->pSqrtStateCovarianceScalar = -1.0;
  iobj_0->pSqrtStateCovarianceScalar = EKF->pSqrtStateCovarianceScalar;
  for (i = 0; i < 9; i++) {
    value_0[i] = EKF->pSqrtProcessNoise[i];
  }

  for (i = 0; i < 9; i++) {
    iobj_0->pSqrtProcessNoise[i] = value_0[i];
  }

  iobj_0->pIsSetProcessNoise = true;
  iobj_0->pSqrtProcessNoiseScalar = -1.0;
  iobj_0->pSqrtProcessNoiseScalar = EKF->pSqrtProcessNoiseScalar;
  for (i = 0; i < 36; i++) {
    value[i] = EKF->pSqrtMeasurementNoise[i];
  }

  for (i = 0; i < 36; i++) {
    iobj_0->pSqrtMeasurementNoise[i] = value[i];
  }

  iobj_0->pIsSetMeasurementNoise = true;
  iobj_0->pSqrtMeasurementNoiseScalar = -1.0;
  iobj_0->pSqrtMeasurementNoiseScalar = EKF->pSqrtMeasurementNoiseScalar;
  iobj_0->pHasPrediction = EKF->pHasPrediction;
  iobj_0->pIsValidStateTransitionFcn = EKF->pIsValidStateTransitionFcn;
  iobj_0->pIsValidMeasurementFcn = EKF->pIsValidMeasurementFcn;
  return newEKF;
}

b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T
  *PathFollowingControllerRefMdlModelClass::allocateTrack
  (c_trackHistoryLogic_LFRefMdl_T *trackLogic, const real_T
   sampleDetection_Measurement[6], const real_T
   sampleDetection_MeasurementNoise[36], real_T sampleDetection_ObjectClassID,
   drivingCoordinateFrameType sampleDetection_MeasurementParameters_Frame, const
   real_T sampleDetection_MeasurementParameters_OriginPosition[3], const real_T
   sampleDetection_MeasurementParameters_Orientation[9], boolean_T
   sampleDetection_MeasurementParameters_HasVelocity, boolean_T
   sampleDetection_MeasurementParameters_HasElevation, real_T
   sampleDetection_ObjectAttributes_TargetIndex, real_T
   sampleDetection_ObjectAttributes_SNR,
   b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *iobj_0,
   c_trackingEKF_LFRefMdl_T *iobj_1)
{
  BusRadarDetectionsObjectAttributes b_idx_0;
  BusRadarDetectionsObjectAttributes b_idx_1;
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track;
  c_trackingEKF_LFRefMdl_T *filter;
  c_trackingEKF_LFRefMdl_T *iobj_0_0;
  filter = initcvekf(sampleDetection_Measurement,
                     sampleDetection_MeasurementNoise,
                     sampleDetection_MeasurementParameters_Frame,
                     sampleDetection_MeasurementParameters_OriginPosition,
                     sampleDetection_MeasurementParameters_Orientation,
                     sampleDetection_MeasurementParameters_HasVelocity,
                     sampleDetection_MeasurementParameters_HasElevation,
                     &iobj_1[0]);
  ExtendedKalmanFilter_set_MeasurementNoise(filter,
    sampleDetection_MeasurementNoise);
  track = iobj_0;
  iobj_0_0 = &iobj_0->_pobj0;
  iobj_0->BranchID = 1U;
  iobj_0->TrackID = 0U;
  iobj_0->Filter = filter;
  iobj_0->pMotionModel = 0U;
  iobj_0->pDistanceFilter = ExtendedKalmanFilter_clone(filter, iobj_0_0);
  iobj_0->UpdateTime = 0.0;
  iobj_0->Time = 0.0;
  iobj_0->ObjectClassID = sampleDetection_ObjectClassID;
  iobj_0->TrackLogic = trackLogic;
  iobj_0->pUsedObjectAttributes[0] = false;
  iobj_0->pUsedObjectAttributes[1] = false;
  b_idx_0.TargetIndex = sampleDetection_ObjectAttributes_TargetIndex;
  b_idx_0.SNR = sampleDetection_ObjectAttributes_SNR;
  b_idx_1.TargetIndex = sampleDetection_ObjectAttributes_TargetIndex;
  b_idx_1.SNR = sampleDetection_ObjectAttributes_SNR;
  iobj_0->pObjectAttributes[0] = b_idx_0;
  iobj_0->pObjectAttributes[1] = b_idx_1;
  iobj_0->pUsedObjectAttributes[0] = true;
  return track;
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_nullify
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track)
{
  c_trackHistoryLogic_LFRefMdl_T *obj;
  c_trackingEKF_LFRefMdl_T *EKF;
  real_T b_I[36];
  int32_T i;
  track->BranchID = 0U;
  track->TrackID = 0U;
  track->IsConfirmed = false;
  track->ObjectClassID = 0.0;
  track->pObjectAttributes[0].TargetIndex = 0.0;
  track->pObjectAttributes[0].SNR = 0.0;
  track->pObjectAttributes[1].TargetIndex = 0.0;
  track->pObjectAttributes[1].SNR = 0.0;
  track->pUsedObjectAttributes[0] = false;
  track->pUsedObjectAttributes[1] = false;
  track->UpdateTime = 0.0;
  track->Time = 0.0;
  track->pAge = 0U;
  track->pIsCoasted = true;
  obj = track->TrackLogic;
  for (i = 0; i < 50; i++) {
    obj->pRecentHistory[i] = false;
  }

  obj->pIsFirstUpdate = true;
  EKF = track->Filter;
  for (i = 0; i < 6; i++) {
    EKF->pState[i] = 0.0;
  }

  std::memset(&b_I[0], 0, 36U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    b_I[i + 6 * i] = 1.0;
  }

  cholPSD(b_I, EKF->pSqrtStateCovariance);
  EKF->pIsSetStateCovariance = true;
  EKF->pSqrtStateCovarianceScalar = -1.0;
  EKF = track->pDistanceFilter;
  for (i = 0; i < 6; i++) {
    EKF->pState[i] = 0.0;
  }

  std::memset(&b_I[0], 0, 36U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    b_I[i + 6 * i] = 1.0;
  }

  cholPSD(b_I, EKF->pSqrtStateCovariance);
  EKF->pIsSetStateCovariance = true;
  EKF->pSqrtStateCovarianceScalar = -1.0;
}

void PathFollowingControllerRefMdlModelClass::trackingEKF_sync
  (c_trackingEKF_LFRefMdl_T *EKF, c_trackingEKF_LFRefMdl_T *EKF2)
{
  static const int32_T tmp_1[2] = { 3, 3 };

  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  static const int8_T tmp_0[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  real_T a[36];
  real_T a_1[36];
  real_T b[36];
  real_T a_0[9];
  real_T b_0[9];
  real_T value_0[9];
  real_T value[6];
  real_T c;
  real_T ssq;
  int32_T b_B_size[2];
  int32_T b_1;
  int32_T d;
  int32_T i;
  int32_T idxA1j;
  int32_T idxAjj;
  int32_T info;
  int32_T ix;
  int32_T iy;
  int32_T jm1;
  int32_T k;
  boolean_T exitg1;
  for (i = 0; i < 6; i++) {
    value[i] = EKF2->pState[i];
  }

  for (i = 0; i < 6; i++) {
    EKF->pState[i] = value[i];
  }

  if ((!EKF2->pIsSetStateCovariance) || (EKF2->pSqrtStateCovarianceScalar !=
       -1.0)) {
    ssq = EKF2->pSqrtStateCovarianceScalar;
    for (info = 0; info < 36; info++) {
      EKF2->pSqrtStateCovariance[info] = ssq * static_cast<real_T>(tmp[info]);
    }

    EKF2->pIsSetStateCovariance = true;
    EKF2->pSqrtStateCovarianceScalar = -1.0;
  }

  for (info = 0; info < 36; info++) {
    b[info] = EKF2->pSqrtStateCovariance[info];
  }

  for (info = 0; info < 36; info++) {
    a[info] = EKF2->pSqrtStateCovariance[info];
  }

  for (info = 0; info < 6; info++) {
    for (i = 0; i < 6; i++) {
      idxAjj = info + 6 * i;
      a_1[idxAjj] = 0.0;
      for (idxA1j = 0; idxA1j < 6; idxA1j++) {
        a_1[idxAjj] += a[6 * idxA1j + info] * b[6 * idxA1j + i];
      }
    }
  }

  cholPSD(a_1, EKF->pSqrtStateCovariance);
  EKF->pIsSetStateCovariance = true;
  EKF->pSqrtStateCovarianceScalar = -1.0;
  if ((!EKF2->pIsSetMeasurementNoise) || (EKF2->pSqrtMeasurementNoiseScalar !=
       -1.0)) {
    ssq = EKF2->pSqrtMeasurementNoiseScalar;
    for (info = 0; info < 36; info++) {
      EKF2->pSqrtMeasurementNoise[info] = ssq * static_cast<real_T>(tmp[info]);
    }

    EKF2->pIsSetMeasurementNoise = true;
    EKF2->pSqrtMeasurementNoiseScalar = -1.0;
  }

  for (info = 0; info < 36; info++) {
    b[info] = EKF2->pSqrtMeasurementNoise[info];
  }

  for (info = 0; info < 36; info++) {
    a[info] = EKF2->pSqrtMeasurementNoise[info];
  }

  for (info = 0; info < 6; info++) {
    for (i = 0; i < 6; i++) {
      idxAjj = info + 6 * i;
      a_1[idxAjj] = 0.0;
      for (idxA1j = 0; idxA1j < 6; idxA1j++) {
        a_1[idxAjj] += a[6 * idxA1j + info] * b[6 * idxA1j + i];
      }
    }
  }

  cholPSD(a_1, EKF->pSqrtMeasurementNoise);
  EKF->pIsSetMeasurementNoise = true;
  EKF->pSqrtMeasurementNoiseScalar = -1.0;
  if ((!EKF2->pIsSetProcessNoise) || (EKF2->pSqrtProcessNoiseScalar != -1.0)) {
    ssq = EKF2->pSqrtProcessNoiseScalar;
    for (info = 0; info < 9; info++) {
      EKF2->pSqrtProcessNoise[info] = ssq * static_cast<real_T>(tmp_0[info]);
    }

    EKF2->pIsSetProcessNoise = true;
    EKF2->pSqrtProcessNoiseScalar = -1.0;
  }

  for (info = 0; info < 9; info++) {
    b_0[info] = EKF2->pSqrtProcessNoise[info];
  }

  for (info = 0; info < 9; info++) {
    a_0[info] = EKF2->pSqrtProcessNoise[info];
  }

  for (info = 0; info < 3; info++) {
    for (i = 0; i < 3; i++) {
      idxA1j = i + 3 * info;
      value_0[idxA1j] = 0.0;
      value_0[idxA1j] += a_0[i] * b_0[info];
      value_0[idxA1j] += a_0[i + 3] * b_0[info + 3];
      value_0[idxA1j] += a_0[i + 6] * b_0[info + 6];
    }
  }

  cholesky(value_0, tmp_1, b, b_B_size, &info);
  if (info == 0) {
    i = 0;
    info = 1;
    exitg1 = false;
    while ((!exitg1) && (info - 1 < 3)) {
      jm1 = info - 2;
      idxA1j = (info - 1) * 3 + 1;
      idxAjj = (info + idxA1j) - 2;
      ssq = 0.0;
      if (info - 1 >= 1) {
        ix = idxA1j - 1;
        iy = idxA1j - 1;
        for (k = 0; k <= jm1; k++) {
          ssq += value_0[ix] * value_0[iy];
          ix++;
          iy++;
        }
      }

      ssq = value_0[idxAjj] - ssq;
      if (ssq > 0.0) {
        ssq = std::sqrt(ssq);
        value_0[idxAjj] = ssq;
        if (info < 3) {
          if (info - 1 != 0) {
            iy = idxAjj + 3;
            b_1 = (2 - info) * 3 + idxA1j;
            for (k = idxA1j + 3; k <= b_1 + 3; k += 3) {
              ix = idxA1j - 1;
              c = 0.0;
              d = (info + k) - 1;
              for (jm1 = k; jm1 < d; jm1++) {
                c += value_0[jm1 - 1] * value_0[ix];
                ix++;
              }

              value_0[iy] += -c;
              iy += 3;
            }
          }

          ssq = 1.0 / ssq;
          b_1 = (2 - info) * 3 + idxAjj;
          for (k = idxAjj + 4; k <= b_1 + 4; k += 3) {
            value_0[k - 1] *= ssq;
          }
        }

        info++;
      } else {
        value_0[idxAjj] = ssq;
        i = info;
        exitg1 = true;
      }
    }

    if (i == 0) {
      idxA1j = 3;
    } else {
      idxA1j = i - 1;
    }

    for (info = 0; info < idxA1j; info++) {
      for (i = info + 2; i <= idxA1j; i++) {
        value_0[(i + 3 * info) - 1] = 0.0;
      }
    }

    for (info = 0; info < 3; info++) {
      b_0[3 * info] = value_0[info];
      b_0[3 * info + 1] = value_0[info + 3];
      b_0[3 * info + 2] = value_0[info + 6];
    }
  } else {
    svdPSD(value_0, b_0);
  }

  for (info = 0; info < 9; info++) {
    EKF->pSqrtProcessNoise[info] = b_0[info];
  }

  EKF->pIsSetProcessNoise = true;
  EKF->pSqrtProcessNoiseScalar = -1.0;
}

b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T
  *PathFollowingControllerRefMdlModelClass::ObjectTrack_copy
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
   b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *iobj_0,
   c_trackHistoryLogic_LFRefMdl_T *iobj_1, c_trackingEKF_LFRefMdl_T *iobj_2)
{
  BusRadarDetectionsObjectAttributes c;
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *newTrack;
  c_trackHistoryLogic_LFRefMdl_T *obj;
  c_trackingEKF_LFRefMdl_T *filter;
  c_trackingEKF_LFRefMdl_T *iobj_0_0;
  real_T varargin_10;
  real_T varargin_6;
  int32_T i;
  uint32_T varargin_2;
  uint32_T varargin_4;
  boolean_T track_idx_0;
  boolean_T track_idx_1;
  boolean_T x_idx_2;
  filter = ExtendedKalmanFilter_clone(track->Filter, iobj_2);
  obj = track->TrackLogic;
  for (i = 0; i < 50; i++) {
    iobj_1->pRecentHistory[i] = false;
  }

  *iobj_1 = *obj;
  c = track->pObjectAttributes[0];
  varargin_2 = track->BranchID;
  varargin_4 = track->TrackID;
  varargin_6 = track->Time;
  varargin_10 = track->ObjectClassID;
  newTrack = iobj_0;
  iobj_0_0 = &iobj_0->_pobj0;
  iobj_0->BranchID = varargin_2;
  iobj_0->TrackID = varargin_4;
  iobj_0->Filter = filter;
  iobj_0->pMotionModel = 0U;
  iobj_0->pDistanceFilter = ExtendedKalmanFilter_clone(filter, iobj_0_0);
  iobj_0->UpdateTime = varargin_6;
  iobj_0->Time = varargin_6;
  iobj_0->ObjectClassID = varargin_10;
  iobj_0->TrackLogic = iobj_1;
  iobj_0->pUsedObjectAttributes[0] = false;
  iobj_0->pUsedObjectAttributes[1] = false;
  iobj_0->pObjectAttributes[0] = c;
  iobj_0->pObjectAttributes[1] = c;
  iobj_0->pUsedObjectAttributes[0] = true;
  iobj_0->pObjectAttributes[0] = track->pObjectAttributes[0];
  iobj_0->pObjectAttributes[1] = track->pObjectAttributes[1];
  iobj_0->UpdateTime = track->UpdateTime;
  iobj_0->pIsCoasted = track->pIsCoasted;
  iobj_0->pMotionModel = track->pMotionModel;
  track_idx_0 = track->pUsedObjectAttributes[0];
  track_idx_1 = track->pUsedObjectAttributes[1];
  iobj_0->pUsedObjectAttributes[0] = track_idx_0;
  iobj_0->pUsedObjectAttributes[1] = track_idx_1;
  iobj_0->pAge = track->pAge;
  if (iobj_0->ObjectClassID != 0.0) {
    track_idx_0 = true;
  } else {
    obj = iobj_0->TrackLogic;
    if (obj->pIsFirstUpdate) {
      track_idx_0 = false;
    } else {
      track_idx_0 = obj->pRecentHistory[0];
      track_idx_1 = obj->pRecentHistory[1];
      x_idx_2 = obj->pRecentHistory[2];
      track_idx_0 = ((track_idx_0 + track_idx_1) + x_idx_2 >= 2);
    }
  }

  iobj_0->IsConfirmed = track_idx_0;
  trackingEKF_sync(iobj_0->Filter, track->Filter);
  trackingEKF_sync(iobj_0->pDistanceFilter, track->pDistanceFilter);
  return newTrack;
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_trackToStruct
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track,
   sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T *trackStruct)
{
  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  c_trackHistoryLogic_LFRefMdl_T *obj;
  c_trackingEKF_LFRefMdl_T *obj_0;
  real_T a[36];
  real_T a_0[36];
  real_T b[36];
  real_T state[6];
  real_T value;
  int32_T a_tmp;
  int32_T i;
  int32_T i_0;
  int32_T i_1;
  int8_T trackStruct_tmp[6];
  trackStruct->TrackID = track->TrackID;
  trackStruct->BranchID = track->BranchID;
  trackStruct->UpdateTime = track->Time;
  trackStruct->Age = track->pAge;
  obj_0 = track->Filter;
  for (i = 0; i < 6; i++) {
    state[i] = obj_0->pState[i];
  }

  obj_0 = track->Filter;
  if ((!obj_0->pIsSetStateCovariance) || (obj_0->pSqrtStateCovarianceScalar !=
       -1.0)) {
    value = obj_0->pSqrtStateCovarianceScalar;
    for (i = 0; i < 36; i++) {
      obj_0->pSqrtStateCovariance[i] = value * static_cast<real_T>(tmp[i]);
    }

    obj_0->pIsSetStateCovariance = true;
    obj_0->pSqrtStateCovarianceScalar = -1.0;
  }

  for (i = 0; i < 36; i++) {
    b[i] = obj_0->pSqrtStateCovariance[i];
  }

  for (i = 0; i < 36; i++) {
    a[i] = obj_0->pSqrtStateCovariance[i];
  }

  trackStruct->ObjectClassID = track->ObjectClassID;
  obj = track->TrackLogic;
  for (i = 0; i < 5; i++) {
    trackStruct->TrackLogicState[i] = obj->pRecentHistory[i];
  }

  trackStruct->IsConfirmed = track->IsConfirmed;
  trackStruct->IsCoasted = track->pIsCoasted;
  trackStruct->SourceIndex = 0U;
  for (i = 0; i < 6; i++) {
    trackStruct->State[i] = state[i];
    trackStruct_tmp[i] = static_cast<int8_T>(i + 1);
    for (i_0 = 0; i_0 < 6; i_0++) {
      a_tmp = i + 6 * i_0;
      a_0[a_tmp] = 0.0;
      for (i_1 = 0; i_1 < 6; i_1++) {
        a_0[a_tmp] += a[6 * i_1 + i] * b[6 * i_1 + i_0];
      }
    }
  }

  for (i = 0; i < 6; i++) {
    for (i_0 = 0; i_0 < 6; i_0++) {
      trackStruct->StateCovariance[i_0 + 6 * i] = a_0[((trackStruct_tmp[i] - 1) *
        6 + trackStruct_tmp[i_0]) - 1];
    }
  }

  trackStruct->TrackLogic = History;
  trackStruct->IsSelfReported = true;
  trackStruct->ObjectAttributes[0] = track->pObjectAttributes[0];
  trackStruct->ObjectAttributes[1] = track->pObjectAttributes[1];
}

void PathFollowingControllerRefMdlModelClass::TrackManager_setupImpl
  (multiObjectTracker_LFRefMdl_T *obj)
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *tracks[100];
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T track;
  c_trackHistoryLogic_LFRefMdl_T *trackLogic;
  c_trackingEKF_LFRefMdl_T lobj_1[2];
  sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T unusedExpr;
  real_T sampleDetection_MeasurementNoise[36];
  real_T sampleDetection_MeasurementParameters_Orientation[9];
  real_T sampleDetection_Measurement[6];
  real_T sampleDetection_MeasurementParameters_OriginPosition[3];
  real_T sampleDetection_ObjectAttributes_SNR;
  real_T sampleDetection_ObjectAttributes_TargetIndex;
  real_T sampleDetection_ObjectClassID;
  int32_T i;
  drivingCoordinateFrameType sampleDetection_MeasurementParameters_Frame;
  boolean_T sampleDetection_MeasurementParameters_HasElevation;
  boolean_T sampleDetection_MeasurementParameters_HasVelocity;
  for (i = 0; i < 6; i++) {
    sampleDetection_Measurement[i] = obj->pSampleDetection.Measurement[i];
  }

  for (i = 0; i < 36; i++) {
    sampleDetection_MeasurementNoise[i] = obj->
      pSampleDetection.MeasurementNoise[i];
  }

  sampleDetection_ObjectClassID = obj->pSampleDetection.ObjectClassID;
  sampleDetection_MeasurementParameters_Frame =
    obj->pSampleDetection.MeasurementParameters.Frame;
  sampleDetection_MeasurementParameters_OriginPosition[0] =
    obj->pSampleDetection.MeasurementParameters.OriginPosition[0];
  sampleDetection_MeasurementParameters_OriginPosition[1] =
    obj->pSampleDetection.MeasurementParameters.OriginPosition[1];
  sampleDetection_MeasurementParameters_OriginPosition[2] =
    obj->pSampleDetection.MeasurementParameters.OriginPosition[2];
  for (i = 0; i < 9; i++) {
    sampleDetection_MeasurementParameters_Orientation[i] =
      obj->pSampleDetection.MeasurementParameters.Orientation[i];
  }

  sampleDetection_MeasurementParameters_HasVelocity =
    obj->pSampleDetection.MeasurementParameters.HasVelocity;
  sampleDetection_MeasurementParameters_HasElevation =
    obj->pSampleDetection.MeasurementParameters.HasElevation;
  sampleDetection_ObjectAttributes_TargetIndex =
    obj->pSampleDetection.ObjectAttributes.TargetIndex;
  sampleDetection_ObjectAttributes_SNR =
    obj->pSampleDetection.ObjectAttributes.SNR;
  trackLogic = obj->pLogic;
  allocateTrack(trackLogic, sampleDetection_Measurement,
                sampleDetection_MeasurementNoise, sampleDetection_ObjectClassID,
                sampleDetection_MeasurementParameters_Frame,
                sampleDetection_MeasurementParameters_OriginPosition,
                sampleDetection_MeasurementParameters_Orientation,
                sampleDetection_MeasurementParameters_HasVelocity,
                sampleDetection_MeasurementParameters_HasElevation,
                sampleDetection_ObjectAttributes_TargetIndex,
                sampleDetection_ObjectAttributes_SNR, &track, &lobj_1[0]);
  ObjectTrack_nullify(&track);
  tracks[0] = ObjectTrack_copy(&track, &obj->_pobj2[0], &obj->_pobj1[0],
    &obj->_pobj0[0]);
  tracks[1] = ObjectTrack_copy(&track, &obj->_pobj2[1], &obj->_pobj1[1],
    &obj->_pobj0[1]);
  tracks[2] = ObjectTrack_copy(&track, &obj->_pobj2[2], &obj->_pobj1[2],
    &obj->_pobj0[2]);
  tracks[3] = ObjectTrack_copy(&track, &obj->_pobj2[3], &obj->_pobj1[3],
    &obj->_pobj0[3]);
  tracks[4] = ObjectTrack_copy(&track, &obj->_pobj2[4], &obj->_pobj1[4],
    &obj->_pobj0[4]);
  tracks[5] = ObjectTrack_copy(&track, &obj->_pobj2[5], &obj->_pobj1[5],
    &obj->_pobj0[5]);
  tracks[6] = ObjectTrack_copy(&track, &obj->_pobj2[6], &obj->_pobj1[6],
    &obj->_pobj0[6]);
  tracks[7] = ObjectTrack_copy(&track, &obj->_pobj2[7], &obj->_pobj1[7],
    &obj->_pobj0[7]);
  tracks[8] = ObjectTrack_copy(&track, &obj->_pobj2[8], &obj->_pobj1[8],
    &obj->_pobj0[8]);
  tracks[9] = ObjectTrack_copy(&track, &obj->_pobj2[9], &obj->_pobj1[9],
    &obj->_pobj0[9]);
  tracks[10] = ObjectTrack_copy(&track, &obj->_pobj2[10], &obj->_pobj1[10],
    &obj->_pobj0[10]);
  tracks[11] = ObjectTrack_copy(&track, &obj->_pobj2[11], &obj->_pobj1[11],
    &obj->_pobj0[11]);
  tracks[12] = ObjectTrack_copy(&track, &obj->_pobj2[12], &obj->_pobj1[12],
    &obj->_pobj0[12]);
  tracks[13] = ObjectTrack_copy(&track, &obj->_pobj2[13], &obj->_pobj1[13],
    &obj->_pobj0[13]);
  tracks[14] = ObjectTrack_copy(&track, &obj->_pobj2[14], &obj->_pobj1[14],
    &obj->_pobj0[14]);
  tracks[15] = ObjectTrack_copy(&track, &obj->_pobj2[15], &obj->_pobj1[15],
    &obj->_pobj0[15]);
  tracks[16] = ObjectTrack_copy(&track, &obj->_pobj2[16], &obj->_pobj1[16],
    &obj->_pobj0[16]);
  tracks[17] = ObjectTrack_copy(&track, &obj->_pobj2[17], &obj->_pobj1[17],
    &obj->_pobj0[17]);
  tracks[18] = ObjectTrack_copy(&track, &obj->_pobj2[18], &obj->_pobj1[18],
    &obj->_pobj0[18]);
  tracks[19] = ObjectTrack_copy(&track, &obj->_pobj2[19], &obj->_pobj1[19],
    &obj->_pobj0[19]);
  tracks[20] = ObjectTrack_copy(&track, &obj->_pobj2[20], &obj->_pobj1[20],
    &obj->_pobj0[20]);
  tracks[21] = ObjectTrack_copy(&track, &obj->_pobj2[21], &obj->_pobj1[21],
    &obj->_pobj0[21]);
  tracks[22] = ObjectTrack_copy(&track, &obj->_pobj2[22], &obj->_pobj1[22],
    &obj->_pobj0[22]);
  tracks[23] = ObjectTrack_copy(&track, &obj->_pobj2[23], &obj->_pobj1[23],
    &obj->_pobj0[23]);
  tracks[24] = ObjectTrack_copy(&track, &obj->_pobj2[24], &obj->_pobj1[24],
    &obj->_pobj0[24]);
  tracks[25] = ObjectTrack_copy(&track, &obj->_pobj2[25], &obj->_pobj1[25],
    &obj->_pobj0[25]);
  tracks[26] = ObjectTrack_copy(&track, &obj->_pobj2[26], &obj->_pobj1[26],
    &obj->_pobj0[26]);
  tracks[27] = ObjectTrack_copy(&track, &obj->_pobj2[27], &obj->_pobj1[27],
    &obj->_pobj0[27]);
  tracks[28] = ObjectTrack_copy(&track, &obj->_pobj2[28], &obj->_pobj1[28],
    &obj->_pobj0[28]);
  tracks[29] = ObjectTrack_copy(&track, &obj->_pobj2[29], &obj->_pobj1[29],
    &obj->_pobj0[29]);
  tracks[30] = ObjectTrack_copy(&track, &obj->_pobj2[30], &obj->_pobj1[30],
    &obj->_pobj0[30]);
  tracks[31] = ObjectTrack_copy(&track, &obj->_pobj2[31], &obj->_pobj1[31],
    &obj->_pobj0[31]);
  tracks[32] = ObjectTrack_copy(&track, &obj->_pobj2[32], &obj->_pobj1[32],
    &obj->_pobj0[32]);
  tracks[33] = ObjectTrack_copy(&track, &obj->_pobj2[33], &obj->_pobj1[33],
    &obj->_pobj0[33]);
  tracks[34] = ObjectTrack_copy(&track, &obj->_pobj2[34], &obj->_pobj1[34],
    &obj->_pobj0[34]);
  tracks[35] = ObjectTrack_copy(&track, &obj->_pobj2[35], &obj->_pobj1[35],
    &obj->_pobj0[35]);
  tracks[36] = ObjectTrack_copy(&track, &obj->_pobj2[36], &obj->_pobj1[36],
    &obj->_pobj0[36]);
  tracks[37] = ObjectTrack_copy(&track, &obj->_pobj2[37], &obj->_pobj1[37],
    &obj->_pobj0[37]);
  tracks[38] = ObjectTrack_copy(&track, &obj->_pobj2[38], &obj->_pobj1[38],
    &obj->_pobj0[38]);
  tracks[39] = ObjectTrack_copy(&track, &obj->_pobj2[39], &obj->_pobj1[39],
    &obj->_pobj0[39]);
  tracks[40] = ObjectTrack_copy(&track, &obj->_pobj2[40], &obj->_pobj1[40],
    &obj->_pobj0[40]);
  tracks[41] = ObjectTrack_copy(&track, &obj->_pobj2[41], &obj->_pobj1[41],
    &obj->_pobj0[41]);
  tracks[42] = ObjectTrack_copy(&track, &obj->_pobj2[42], &obj->_pobj1[42],
    &obj->_pobj0[42]);
  tracks[43] = ObjectTrack_copy(&track, &obj->_pobj2[43], &obj->_pobj1[43],
    &obj->_pobj0[43]);
  tracks[44] = ObjectTrack_copy(&track, &obj->_pobj2[44], &obj->_pobj1[44],
    &obj->_pobj0[44]);
  tracks[45] = ObjectTrack_copy(&track, &obj->_pobj2[45], &obj->_pobj1[45],
    &obj->_pobj0[45]);
  tracks[46] = ObjectTrack_copy(&track, &obj->_pobj2[46], &obj->_pobj1[46],
    &obj->_pobj0[46]);
  tracks[47] = ObjectTrack_copy(&track, &obj->_pobj2[47], &obj->_pobj1[47],
    &obj->_pobj0[47]);
  tracks[48] = ObjectTrack_copy(&track, &obj->_pobj2[48], &obj->_pobj1[48],
    &obj->_pobj0[48]);
  tracks[49] = ObjectTrack_copy(&track, &obj->_pobj2[49], &obj->_pobj1[49],
    &obj->_pobj0[49]);
  tracks[50] = ObjectTrack_copy(&track, &obj->_pobj2[50], &obj->_pobj1[50],
    &obj->_pobj0[50]);
  tracks[51] = ObjectTrack_copy(&track, &obj->_pobj2[51], &obj->_pobj1[51],
    &obj->_pobj0[51]);
  tracks[52] = ObjectTrack_copy(&track, &obj->_pobj2[52], &obj->_pobj1[52],
    &obj->_pobj0[52]);
  tracks[53] = ObjectTrack_copy(&track, &obj->_pobj2[53], &obj->_pobj1[53],
    &obj->_pobj0[53]);
  tracks[54] = ObjectTrack_copy(&track, &obj->_pobj2[54], &obj->_pobj1[54],
    &obj->_pobj0[54]);
  tracks[55] = ObjectTrack_copy(&track, &obj->_pobj2[55], &obj->_pobj1[55],
    &obj->_pobj0[55]);
  tracks[56] = ObjectTrack_copy(&track, &obj->_pobj2[56], &obj->_pobj1[56],
    &obj->_pobj0[56]);
  tracks[57] = ObjectTrack_copy(&track, &obj->_pobj2[57], &obj->_pobj1[57],
    &obj->_pobj0[57]);
  tracks[58] = ObjectTrack_copy(&track, &obj->_pobj2[58], &obj->_pobj1[58],
    &obj->_pobj0[58]);
  tracks[59] = ObjectTrack_copy(&track, &obj->_pobj2[59], &obj->_pobj1[59],
    &obj->_pobj0[59]);
  tracks[60] = ObjectTrack_copy(&track, &obj->_pobj2[60], &obj->_pobj1[60],
    &obj->_pobj0[60]);
  tracks[61] = ObjectTrack_copy(&track, &obj->_pobj2[61], &obj->_pobj1[61],
    &obj->_pobj0[61]);
  tracks[62] = ObjectTrack_copy(&track, &obj->_pobj2[62], &obj->_pobj1[62],
    &obj->_pobj0[62]);
  tracks[63] = ObjectTrack_copy(&track, &obj->_pobj2[63], &obj->_pobj1[63],
    &obj->_pobj0[63]);
  tracks[64] = ObjectTrack_copy(&track, &obj->_pobj2[64], &obj->_pobj1[64],
    &obj->_pobj0[64]);
  tracks[65] = ObjectTrack_copy(&track, &obj->_pobj2[65], &obj->_pobj1[65],
    &obj->_pobj0[65]);
  tracks[66] = ObjectTrack_copy(&track, &obj->_pobj2[66], &obj->_pobj1[66],
    &obj->_pobj0[66]);
  tracks[67] = ObjectTrack_copy(&track, &obj->_pobj2[67], &obj->_pobj1[67],
    &obj->_pobj0[67]);
  tracks[68] = ObjectTrack_copy(&track, &obj->_pobj2[68], &obj->_pobj1[68],
    &obj->_pobj0[68]);
  tracks[69] = ObjectTrack_copy(&track, &obj->_pobj2[69], &obj->_pobj1[69],
    &obj->_pobj0[69]);
  tracks[70] = ObjectTrack_copy(&track, &obj->_pobj2[70], &obj->_pobj1[70],
    &obj->_pobj0[70]);
  tracks[71] = ObjectTrack_copy(&track, &obj->_pobj2[71], &obj->_pobj1[71],
    &obj->_pobj0[71]);
  tracks[72] = ObjectTrack_copy(&track, &obj->_pobj2[72], &obj->_pobj1[72],
    &obj->_pobj0[72]);
  tracks[73] = ObjectTrack_copy(&track, &obj->_pobj2[73], &obj->_pobj1[73],
    &obj->_pobj0[73]);
  tracks[74] = ObjectTrack_copy(&track, &obj->_pobj2[74], &obj->_pobj1[74],
    &obj->_pobj0[74]);
  tracks[75] = ObjectTrack_copy(&track, &obj->_pobj2[75], &obj->_pobj1[75],
    &obj->_pobj0[75]);
  tracks[76] = ObjectTrack_copy(&track, &obj->_pobj2[76], &obj->_pobj1[76],
    &obj->_pobj0[76]);
  tracks[77] = ObjectTrack_copy(&track, &obj->_pobj2[77], &obj->_pobj1[77],
    &obj->_pobj0[77]);
  tracks[78] = ObjectTrack_copy(&track, &obj->_pobj2[78], &obj->_pobj1[78],
    &obj->_pobj0[78]);
  tracks[79] = ObjectTrack_copy(&track, &obj->_pobj2[79], &obj->_pobj1[79],
    &obj->_pobj0[79]);
  tracks[80] = ObjectTrack_copy(&track, &obj->_pobj2[80], &obj->_pobj1[80],
    &obj->_pobj0[80]);
  tracks[81] = ObjectTrack_copy(&track, &obj->_pobj2[81], &obj->_pobj1[81],
    &obj->_pobj0[81]);
  tracks[82] = ObjectTrack_copy(&track, &obj->_pobj2[82], &obj->_pobj1[82],
    &obj->_pobj0[82]);
  tracks[83] = ObjectTrack_copy(&track, &obj->_pobj2[83], &obj->_pobj1[83],
    &obj->_pobj0[83]);
  tracks[84] = ObjectTrack_copy(&track, &obj->_pobj2[84], &obj->_pobj1[84],
    &obj->_pobj0[84]);
  tracks[85] = ObjectTrack_copy(&track, &obj->_pobj2[85], &obj->_pobj1[85],
    &obj->_pobj0[85]);
  tracks[86] = ObjectTrack_copy(&track, &obj->_pobj2[86], &obj->_pobj1[86],
    &obj->_pobj0[86]);
  tracks[87] = ObjectTrack_copy(&track, &obj->_pobj2[87], &obj->_pobj1[87],
    &obj->_pobj0[87]);
  tracks[88] = ObjectTrack_copy(&track, &obj->_pobj2[88], &obj->_pobj1[88],
    &obj->_pobj0[88]);
  tracks[89] = ObjectTrack_copy(&track, &obj->_pobj2[89], &obj->_pobj1[89],
    &obj->_pobj0[89]);
  tracks[90] = ObjectTrack_copy(&track, &obj->_pobj2[90], &obj->_pobj1[90],
    &obj->_pobj0[90]);
  tracks[91] = ObjectTrack_copy(&track, &obj->_pobj2[91], &obj->_pobj1[91],
    &obj->_pobj0[91]);
  tracks[92] = ObjectTrack_copy(&track, &obj->_pobj2[92], &obj->_pobj1[92],
    &obj->_pobj0[92]);
  tracks[93] = ObjectTrack_copy(&track, &obj->_pobj2[93], &obj->_pobj1[93],
    &obj->_pobj0[93]);
  tracks[94] = ObjectTrack_copy(&track, &obj->_pobj2[94], &obj->_pobj1[94],
    &obj->_pobj0[94]);
  tracks[95] = ObjectTrack_copy(&track, &obj->_pobj2[95], &obj->_pobj1[95],
    &obj->_pobj0[95]);
  tracks[96] = ObjectTrack_copy(&track, &obj->_pobj2[96], &obj->_pobj1[96],
    &obj->_pobj0[96]);
  tracks[97] = ObjectTrack_copy(&track, &obj->_pobj2[97], &obj->_pobj1[97],
    &obj->_pobj0[97]);
  tracks[98] = ObjectTrack_copy(&track, &obj->_pobj2[98], &obj->_pobj1[98],
    &obj->_pobj0[98]);
  tracks[99] = ObjectTrack_copy(&track, &obj->_pobj2[99], &obj->_pobj1[99],
    &obj->_pobj0[99]);
  for (i = 0; i < 100; i++) {
    obj->pTracksList[i] = tracks[i];
  }

  ObjectTrack_trackToStruct(&track, &unusedExpr);
  obj->pDistFilter = ExtendedKalmanFilter_clone(obj->pTracksList[0]->Filter,
    &obj->_pobj0[100]);
}

void PathFollowingControllerRefMdlModelClass::SystemCore_setup
  (multiObjectTracker_LFRefMdl_T *obj, real_T varargin_1_NumDetections, const
   BusDetectionConcatenation1Detections varargin_1_Detections[70],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T *obj_0;
  c_trackingEKF_LFRefMdl_T lobj_0[2];
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_1;
  real_T obj_3[36];
  real_T obj_5[9];
  real_T obj_2[6];
  real_T obj_4[3];
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  obj->_pobj5.isInitialized = 0;
  obj->cDetectionManager = &obj->_pobj5;
  obj_1 = obj->cDetectionManager;
  obj_1->isInitialized = 1;
  for (i = 0; i < 70; i++) {
    localB->b[i] = varargin_1_Detections[0];
  }

  for (i = 0; i < 70; i++) {
    obj_1->pDetections[i] = localB->b[i];
  }

  for (i = 0; i < 70; i++) {
    obj_1->pOriginatingSensor[i] = 0.0;
  }

  obj_1->pNumDetections = varargin_1_NumDetections;
  obj->pSampleDetection = varargin_1_Detections[0];
  for (i = 0; i < 6; i++) {
    obj_2[i] = obj->pSampleDetection.Measurement[i];
  }

  std::memcpy(&obj_3[0], &obj->pSampleDetection.MeasurementNoise[0], 36U *
              sizeof(real_T));
  for (i = 0; i < 3; i++) {
    obj_4[i] = obj->pSampleDetection.MeasurementParameters.OriginPosition[i];
  }

  std::memcpy(&obj_5[0],
              &obj->pSampleDetection.MeasurementParameters.Orientation[0], 9U *
              sizeof(real_T));
  initcvekf(obj_2, obj_3, obj->pSampleDetection.MeasurementParameters.Frame,
            obj_4, obj_5,
            obj->pSampleDetection.MeasurementParameters.HasVelocity,
            obj->pSampleDetection.MeasurementParameters.HasElevation, &lobj_0[0]);
  for (i = 0; i < 50; i++) {
    obj->_pobj4.pRecentHistory[i] = false;
  }

  obj->pLogic = &obj->_pobj4;
  TrackManager_setupImpl(obj);
  obj->cCostCalculator.isInitialized = 0;
  obj->_pobj3.isInitialized = 0;
  obj->cAssigner = &obj->_pobj3;
  obj_0 = obj->cAssigner;
  obj_0->isSetupComplete = false;
  obj_0->isInitialized = 1;
  obj_0->isSetupComplete = true;
  obj->isSetupComplete = true;
  obj->TunablePropsChanged = false;
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_resetImpl
  (multiObjectTracker_LFRefMdl_T *obj)
{
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_0;
  int32_T i;
  for (i = 0; i < 100; i++) {
    ObjectTrack_nullify(obj->pTracksList[i]);
  }

  obj->pNumLiveTracks = 0.0;
  for (i = 0; i < 100; i++) {
    obj->pTrackIDs[i] = 0U;
  }

  for (i = 0; i < 100; i++) {
    obj->pConfirmedTracks[i] = false;
  }

  obj->pLastTimeStamp = -0.1;
  obj->pLastTrackID = 0U;
  for (i = 0; i < 100; i++) {
    obj->pWasDetectable[i] = true;
  }

  obj->pCostOfNonAssignment = 10.0;
  obj_0 = obj->cDetectionManager;
  if (obj_0->isInitialized == 1) {
    obj_0->pNumDetections = 0.0;
    for (i = 0; i < 70; i++) {
      obj_0->pOriginatingSensor[i] = 0.0;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::nullAssignment_mk
  (BusRadarDetections x_data[], int32_T *x_size, const int32_T idx_data[], const
   int32_T idx_size[2])
{
  int32_T k0;
  int32_T loop_ub;
  int32_T nxin;
  int32_T nxout;
  boolean_T b_data[50];
  nxin = *x_size - 1;
  if (0 <= *x_size - 1) {
    std::memset(&b_data[0], 0, ((*x_size - 1) + 1) * sizeof(boolean_T));
  }

  nxout = idx_size[1] - 1;
  for (loop_ub = 0; loop_ub <= nxout; loop_ub++) {
    b_data[idx_data[loop_ub] - 1] = true;
  }

  k0 = 0;
  nxout = *x_size - 1;
  for (loop_ub = 0; loop_ub <= nxout; loop_ub++) {
    k0 += b_data[loop_ub];
  }

  nxout = *x_size - k0;
  k0 = -1;
  for (loop_ub = 0; loop_ub <= nxin; loop_ub++) {
    if ((loop_ub + 1 > *x_size) || (!b_data[loop_ub])) {
      k0++;
      x_data[k0] = x_data[loop_ub];
    }
  }

  if (1 > nxout) {
    nxout = 0;
  }

  *x_size = nxout;
}

void PathFollowingControllerRefMdlModelClass::localClusterDetections(const
  BusRadarDetections detections_data[], const int32_T *detections_size,
  BusRadarDetections detectionClusters_data[], int32_T *detectionClusters_size,
  B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  real_T measNoise[36];
  real_T clusterMeas[6];
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T tmp_data[50];
  int32_T tmp_size[2];
  int32_T b_i;
  int32_T k0;
  int32_T loop_ub;
  int32_T nxin;
  int32_T nxout;
  uint32_T detInds_data[50];
  uint32_T leftToCheck_data[50];
  uint32_T leftToCheck_data_0[50];
  uint32_T j;
  boolean_T clusterInds_data[50];
  if (*detections_size < 1) {
    *detectionClusters_size = *detections_size;
    if (0 <= *detections_size - 1) {
      std::memcpy(&detectionClusters_data[0], &detections_data[0],
                  *detections_size * sizeof(BusRadarDetections));
    }
  } else {
    loop_ub = *detections_size * *detections_size - 1;
    if (0 <= loop_ub) {
      std::memset(&localB->distances_data[0], 0, (loop_ub + 1) * sizeof(real_T));
    }

    nxout = *detections_size - 1;
    for (b_i = 0; b_i <= nxout; b_i++) {
      nxin = (static_cast<int32_T>(1.0 - ((static_cast<real_T>(b_i) + 1.0) + 1.0))
              + *detections_size) - 1;
      for (loop_ub = 0; loop_ub <= nxin; loop_ub++) {
        j = (static_cast<uint32_T>(b_i) + loop_ub) + 2U;
        if (detections_data[static_cast<int32_T>(j) - 1].SensorIndex ==
            detections_data[b_i].SensorIndex) {
          scale = 3.3121686421112381E-170;
          absxk = std::abs(detections_data[b_i].Measurement[0] -
                           detections_data[static_cast<int32_T>(j) - 1].
                           Measurement[0]);
          if (absxk > 3.3121686421112381E-170) {
            y = 1.0;
            scale = absxk;
          } else {
            t = absxk / 3.3121686421112381E-170;
            y = t * t;
          }

          absxk = std::abs(detections_data[b_i].Measurement[1] -
                           detections_data[static_cast<int32_T>(j) - 1].
                           Measurement[1]);
          if (absxk > scale) {
            t = scale / absxk;
            y = y * t * t + 1.0;
            scale = absxk;
          } else {
            t = absxk / scale;
            y += t * t;
          }

          localB->distances_data[b_i + *detections_size * (static_cast<int32_T>
            (j) - 1)] = scale * std::sqrt(y);
        } else {
          localB->distances_data[b_i + *detections_size * (static_cast<int32_T>
            (j) - 1)] = (rtInf);
        }
      }
    }

    b_i = *detections_size;
    loop_ub = *detections_size - 1;
    for (nxout = 0; nxout <= loop_ub; nxout++) {
      leftToCheck_data[nxout] = static_cast<uint32_T>(nxout + 1);
    }

    scale = 0.0;
    *detectionClusters_size = *detections_size;
    if (0 <= *detections_size - 1) {
      std::memcpy(&detectionClusters_data[0], &detections_data[0],
                  *detections_size * sizeof(BusRadarDetections));
    }

    while (b_i != 0) {
      loop_ub = static_cast<int32_T>(leftToCheck_data[0]);
      for (nxout = 0; nxout < b_i; nxout++) {
        clusterInds_data[nxout] = (localB->distances_data[((static_cast<int32_T>
          (leftToCheck_data[nxout]) - 1) * *detections_size + loop_ub) - 1] <
          4.0);
      }

      nxout = b_i - 1;
      loop_ub = 0;
      for (nxin = 0; nxin <= nxout; nxin++) {
        if (clusterInds_data[nxin]) {
          loop_ub++;
        }
      }

      k0 = 0;
      for (nxin = 0; nxin <= nxout; nxin++) {
        if (clusterInds_data[nxin]) {
          detInds_data[k0] = leftToCheck_data[nxin];
          k0++;
        }
      }

      for (nxout = 0; nxout < 6; nxout++) {
        clusterMeas[nxout] = detections_data[static_cast<int32_T>(detInds_data[0])
          - 1].Measurement[nxout];
      }

      k0 = loop_ub - 2;
      for (nxin = 0; nxin <= k0; nxin++) {
        for (nxout = 0; nxout < 6; nxout++) {
          clusterMeas[nxout] += detections_data[static_cast<int32_T>
            (detInds_data[nxin + 1]) - 1].Measurement[nxout];
        }
      }

      scale++;
      detectionClusters_data[static_cast<int32_T>(scale) - 1] = detections_data[
        static_cast<int32_T>(detInds_data[0]) - 1];
      for (nxout = 0; nxout < 6; nxout++) {
        detectionClusters_data[static_cast<int32_T>(scale) - 1]
          .Measurement[nxout] = clusterMeas[nxout] / static_cast<real_T>(loop_ub);
      }

      nxin = b_i - 1;
      k0 = 0;
      nxout = b_i - 1;
      for (loop_ub = 0; loop_ub <= nxout; loop_ub++) {
        k0 += clusterInds_data[loop_ub];
      }

      nxout = b_i - k0;
      k0 = -1;
      for (loop_ub = 0; loop_ub <= nxin; loop_ub++) {
        if ((loop_ub + 1 > b_i) || (!clusterInds_data[loop_ub])) {
          k0++;
          leftToCheck_data[k0] = leftToCheck_data[loop_ub];
        }
      }

      if (1 > nxout) {
        nxout = 0;
      }

      if (0 <= nxout - 1) {
        std::memcpy(&leftToCheck_data_0[0], &leftToCheck_data[0], nxout * sizeof
                    (uint32_T));
      }

      b_i = nxout;
      if (0 <= nxout - 1) {
        std::memcpy(&leftToCheck_data[0], &leftToCheck_data_0[0], nxout * sizeof
                    (uint32_T));
      }
    }

    tmp_size[0] = 1;
    loop_ub = *detections_size - static_cast<int32_T>(scale + 1.0);
    tmp_size[1] = loop_ub + 1;
    for (nxout = 0; nxout <= loop_ub; nxout++) {
      tmp_data[nxout] = static_cast<int32_T>(scale + 1.0) + nxout;
    }

    nullAssignment_mk(detectionClusters_data, detectionClusters_size, tmp_data,
                      tmp_size);
    b_i = *detectionClusters_size - 1;
    for (loop_ub = 0; loop_ub <= b_i; loop_ub++) {
      std::memcpy(&measNoise[0], &detectionClusters_data[loop_ub].
                  MeasurementNoise[0], 36U * sizeof(real_T));
      measNoise[0] = 16.0 * detectionClusters_data[loop_ub].MeasurementNoise[0];
      measNoise[21] *= 1600.0;
      measNoise[1] = 16.0 * detectionClusters_data[loop_ub].MeasurementNoise[1];
      measNoise[22] *= 1600.0;
      measNoise[6] = 16.0 * detectionClusters_data[loop_ub].MeasurementNoise[6];
      measNoise[27] *= 1600.0;
      measNoise[7] = 16.0 * detectionClusters_data[loop_ub].MeasurementNoise[7];
      measNoise[28] *= 1600.0;
      std::memcpy(&detectionClusters_data[loop_ub].MeasurementNoise[0],
                  &measNoise[0], 36U * sizeof(real_T));
    }
  }
}

void PathFollowingControllerRefMdlModelClass::helperClusterDetections_stepImpl
  (real_T detBusIn_NumDetections, boolean_T detBusIn_IsValidTime, const
   BusRadarDetections detBusIn_Detections[50], real_T *detBusOut_NumDetections,
   boolean_T *detBusOut_IsValidTime, BusRadarDetections detBusOut_Detections[50],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  int32_T detBusIn_Detections_size;
  int32_T detsClust_size;
  if (1.0 > detBusIn_NumDetections) {
    detsClust_size = -1;
  } else {
    detsClust_size = static_cast<int32_T>(detBusIn_NumDetections) - 1;
  }

  detBusIn_Detections_size = detsClust_size + 1;
  if (0 <= detsClust_size) {
    std::memcpy(&localB->detBusIn_Detections_data[0], &detBusIn_Detections[0],
                (detsClust_size + 1) * sizeof(BusRadarDetections));
  }

  localClusterDetections(localB->detBusIn_Detections_data,
    &detBusIn_Detections_size, localB->detsClust_data, &detsClust_size, localB);
  *detBusOut_IsValidTime = detBusIn_IsValidTime;
  std::memcpy(&detBusOut_Detections[0], &detBusIn_Detections[0], 50U * sizeof
              (BusRadarDetections));
  *detBusOut_NumDetections = detsClust_size;
  if (1 > detsClust_size) {
    detsClust_size = 0;
  }

  if (0 <= detsClust_size - 1) {
    std::memcpy(&detBusOut_Detections[0], &localB->detsClust_data[0],
                detsClust_size * sizeof(BusRadarDetections));
  }
}

void PathFollowingControllerRefMdlModelClass::
  DetectionConcatenation_checkUniqueSensorIndex(real_T varargin_1_NumDetections,
  const BusVisionDetections varargin_1_Detections[20], real_T
  varargin_2_NumDetections, const BusRadarDetections varargin_2_Detections[50])
{
  real_T sensorIndices[2];
  real_T validIndices_data[2];
  real_T absx;
  real_T x;
  int32_T idx_data[2];
  int32_T b_exponent;
  int32_T exitg2;
  int32_T i;
  int32_T k;
  int32_T nb;
  boolean_T f[2];
  boolean_T exitg1;
  sensorIndices[0] = (rtNaN);
  sensorIndices[1] = (rtNaN);
  if (varargin_1_NumDetections > 0.0) {
    sensorIndices[0] = varargin_1_Detections[0].SensorIndex;
  }

  if (varargin_2_NumDetections > 0.0) {
    sensorIndices[1] = varargin_2_Detections[0].SensorIndex;
  }

  f[0] = !rtIsNaN(sensorIndices[0]);
  f[1] = !rtIsNaN(sensorIndices[1]);
  nb = 0;
  k = 0;
  for (i = 0; i < 2; i++) {
    if (f[i]) {
      nb++;
      validIndices_data[k] = sensorIndices[i];
      k++;
    }
  }

  if (0 <= nb - 1) {
    std::memset(&idx_data[0], 0, nb * sizeof(int32_T));
  }

  if (nb != 0) {
    if (1 <= nb - 1) {
      if ((validIndices_data[0] <= validIndices_data[1]) || rtIsNaN
          (validIndices_data[1])) {
        idx_data[0] = 1;
        idx_data[1] = 2;
      } else {
        idx_data[0] = 2;
        idx_data[1] = 1;
      }
    }

    if ((nb & 1U) != 0U) {
      idx_data[nb - 1] = nb;
    }
  }

  for (k = 0; k < nb; k++) {
    sensorIndices[k] = validIndices_data[idx_data[k] - 1];
  }

  k = 0;
  while ((k + 1 <= nb) && rtIsInf(sensorIndices[k]) && (sensorIndices[k] < 0.0))
  {
    k++;
  }

  i = k;
  k = nb;
  while ((k >= 1) && rtIsNaN(sensorIndices[k - 1])) {
    k--;
  }

  exitg1 = false;
  while ((!exitg1) && (k >= 1)) {
    x = sensorIndices[k - 1];
    if (rtIsInf(x) && (x > 0.0)) {
      k--;
    } else {
      exitg1 = true;
    }
  }

  nb = -1;
  if (i > 0) {
    nb = 0;
  }

  while (i + 1 <= k) {
    x = sensorIndices[i];
    do {
      exitg2 = 0;
      i++;
      if (i + 1 > k) {
        exitg2 = 1;
      } else {
        absx = std::abs(x / 2.0);
        if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &b_exponent);
            absx = std::ldexp(1.0, b_exponent - 53);
          }
        } else {
          absx = (rtNaN);
        }

        if ((std::abs(x - sensorIndices[1]) < absx) || (rtIsInf(sensorIndices[1])
             && rtIsInf(x) && ((sensorIndices[1] > 0.0) == (x > 0.0)))) {
        } else {
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    nb++;
    sensorIndices[nb] = x;
  }
}

real_T PathFollowingControllerRefMdlModelClass::tagIndicies(void)
{
  return 2.0;
}

boolean_T PathFollowingControllerRefMdlModelClass::getLogicalIndices(void)
{
  boolean_T indOut;
  indOut = (tagIndicies() == 2.0);
  return indOut;
}

void PathFollowingControllerRefMdlModelClass::getLogicalIndices_l(boolean_T
  indOut[6])
{
  real_T y[6];
  int32_T b_k;
  int32_T indOut_tmp_0;
  int8_T indOut_tmp[6];
  for (b_k = 0; b_k < 6; b_k++) {
    indOut[b_k] = false;
    indOut_tmp[b_k] = static_cast<int8_T>(b_k + 1);
    y[b_k] = 2.0;
  }

  for (b_k = 0; b_k < 6; b_k++) {
    indOut_tmp_0 = indOut_tmp[b_k];
    indOut[indOut_tmp_0 - 1] = (y[indOut_tmp_0 - 1] == 2.0);
  }
}

void PathFollowingControllerRefMdlModelClass::getLogicalIndices_l2(boolean_T
  indOut[36])
{
  int32_T b_k;
  int32_T b_k_0;
  int32_T y_tmp;
  int8_T y[36];
  int8_T y_0[36];
  for (b_k_0 = 0; b_k_0 < 6; b_k_0++) {
    for (b_k = 0; b_k < 6; b_k++) {
      y_tmp = b_k + 6 * b_k_0;
      y_0[y_tmp] = 1;
      y[b_k_0 + 6 * b_k] = y_0[y_tmp];
    }
  }

  for (b_k_0 = 0; b_k_0 < 6; b_k_0++) {
    for (b_k = 0; b_k < 6; b_k++) {
      y_tmp = 6 * b_k_0 + b_k;
      y_0[y_tmp] = static_cast<int8_T>(y[y_tmp] + 1);
      y[y_tmp] = y_0[y_tmp];
    }
  }

  for (b_k_0 = 0; b_k_0 < 36; b_k_0++) {
    indOut[b_k_0] = false;
    indOut[b_k_0] = (y[b_k_0] == 2);
  }
}

void PathFollowingControllerRefMdlModelClass::getLogicalIndices_l2h5w(boolean_T
  indOut[3])
{
  int32_T i;
  int8_T y[3];
  for (i = 0; i < 3; i++) {
    y[i] = 2;
  }

  indOut[0] = (y[0] == 2);
  indOut[1] = (y[1] == 2);
  indOut[2] = (y[2] == 2);
}

real_T PathFollowingControllerRefMdlModelClass::
  SimulinkDetectionManager_stepImpl
  (e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj, real_T
   dets_NumDetections, const BusDetectionConcatenation1Detections
   dets_Detections[70])
{
  real_T numDets;
  int32_T b_i;
  int32_T c;
  numDets = dets_NumDetections;
  obj->pNumDetections = dets_NumDetections;
  if (dets_NumDetections > 0.0) {
    if (0 <= static_cast<int32_T>(obj->pNumDetections) - 1) {
      std::memcpy(&obj->pDetections[0], &dets_Detections[0],
                  ((static_cast<int32_T>(obj->pNumDetections) - 1) + 1) * sizeof
                  (BusDetectionConcatenation1Detections));
    }

    std::memset(&obj->pOriginatingSensor[0], 0, 70U * sizeof(real_T));
    c = static_cast<int32_T>(obj->pNumDetections) - 1;
    for (b_i = 0; b_i <= c; b_i++) {
      obj->pOriginatingSensor[b_i] = obj->pDetections[b_i].SensorIndex;
    }
  }

  return numDets;
}

real_T PathFollowingControllerRefMdlModelClass::SystemCore_step
  (e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj, real_T
   varargin_1_NumDetections, const BusDetectionConcatenation1Detections
   varargin_1_Detections[70])
{
  real_T varargout_1;
  int32_T b_itilerow;
  if (obj->isInitialized != 1) {
    obj->isInitialized = 1;
    obj->pNumDetections = varargin_1_NumDetections;
    obj->pNumDetections = 0.0;
    for (b_itilerow = 0; b_itilerow < 70; b_itilerow++) {
      obj->pDetections[b_itilerow] = varargin_1_Detections[0];
      obj->pOriginatingSensor[b_itilerow] = 0.0;
      obj->pOriginatingSensor[b_itilerow] = 0.0;
    }
  }

  varargout_1 = SimulinkDetectionManager_stepImpl(obj, varargin_1_NumDetections,
    varargin_1_Detections);
  return varargout_1;
}

void PathFollowingControllerRefMdlModelClass::unique_vector(const real_T a[70],
  real_T b_data[], int32_T b_size[2])
{
  real_T absx;
  real_T x;
  int32_T idx[70];
  int32_T iwork[70];
  int32_T b_j;
  int32_T exitg2;
  int32_T i;
  int32_T i2;
  int32_T k;
  int32_T kEnd;
  int32_T nb;
  int32_T p;
  int32_T pEnd;
  int32_T q;
  int32_T qEnd;
  boolean_T exitg1;
  for (k = 0; k <= 68; k += 2) {
    x = a[k + 1];
    if ((a[k] <= x) || rtIsNaN(x)) {
      idx[k] = k + 1;
      idx[k + 1] = k + 2;
    } else {
      idx[k] = k + 2;
      idx[k + 1] = k + 1;
    }
  }

  i = 2;
  while (i < 70) {
    i2 = i << 1;
    nb = 1;
    for (pEnd = i + 1; pEnd < 71; pEnd = qEnd + i) {
      p = nb - 1;
      q = pEnd - 1;
      qEnd = nb + i2;
      if (qEnd > 71) {
        qEnd = 71;
      }

      k = 0;
      kEnd = qEnd - nb;
      while (k + 1 <= kEnd) {
        x = a[idx[q] - 1];
        if ((a[idx[p] - 1] <= x) || rtIsNaN(x)) {
          iwork[k] = idx[p];
          p++;
          if (p + 1 == pEnd) {
            while (q + 1 < qEnd) {
              k++;
              iwork[k] = idx[q];
              q++;
            }
          }
        } else {
          iwork[k] = idx[q];
          q++;
          if (q + 1 == qEnd) {
            while (p + 1 < pEnd) {
              k++;
              iwork[k] = idx[p];
              p++;
            }
          }
        }

        k++;
      }

      for (k = 0; k < kEnd; k++) {
        idx[(nb + k) - 1] = iwork[k];
      }

      nb = qEnd;
    }

    i = i2;
  }

  for (k = 0; k < 70; k++) {
    b_data[k] = a[idx[k] - 1];
  }

  k = 0;
  while ((k + 1 <= 70) && rtIsInf(b_data[k]) && (b_data[k] < 0.0)) {
    k++;
  }

  i = k;
  k = 70;
  while ((k >= 1) && rtIsNaN(b_data[k - 1])) {
    k--;
  }

  i2 = 70 - k;
  exitg1 = false;
  while ((!exitg1) && (k >= 1)) {
    x = b_data[k - 1];
    if (rtIsInf(x) && (x > 0.0)) {
      k--;
    } else {
      exitg1 = true;
    }
  }

  qEnd = 70 - (k + i2);
  nb = -1;
  if (i > 0) {
    nb = 0;
  }

  while (i + 1 <= k) {
    x = b_data[i];
    do {
      exitg2 = 0;
      i++;
      if (i + 1 > k) {
        exitg2 = 1;
      } else {
        absx = std::abs(x / 2.0);
        if ((!rtIsInf(absx)) && (!rtIsNaN(absx))) {
          if (absx <= 2.2250738585072014E-308) {
            absx = 4.94065645841247E-324;
          } else {
            frexp(absx, &b_j);
            absx = std::ldexp(1.0, b_j - 53);
          }
        } else {
          absx = (rtNaN);
        }

        if ((std::abs(x - b_data[i]) < absx) || (rtIsInf(b_data[i]) && rtIsInf(x)
             && ((b_data[i] > 0.0) == (x > 0.0)))) {
        } else {
          exitg2 = 1;
        }
      }
    } while (exitg2 == 0);

    nb++;
    b_data[nb] = x;
  }

  if (qEnd > 0) {
    nb++;
    b_data[nb] = b_data[k];
  }

  i = k + qEnd;
  for (b_j = 0; b_j < i2; b_j++) {
    nb++;
    b_data[nb] = b_data[i + b_j];
  }

  if (1 > nb + 1) {
    b_j = 0;
  } else {
    b_j = nb + 1;
  }

  b_size[0] = 1;
  b_size[1] = b_j;
}

void PathFollowingControllerRefMdlModelClass::
  AssignmentCostCalculator_findPairsToCost(real_T numLiveTracks, const int32_T
  detIndices_size[2], boolean_T toCalculate_data[], int32_T toCalculate_size[2])
{
  int32_T b;
  int32_T c;
  int32_T i;
  int32_T i_0;
  if (1.0 > numLiveTracks) {
    b = -1;
  } else {
    b = static_cast<int32_T>(numLiveTracks) - 1;
  }

  if (1 > detIndices_size[1]) {
    c = -1;
  } else {
    c = detIndices_size[1] - 1;
  }

  toCalculate_size[0] = b + 1;
  toCalculate_size[1] = c + 1;
  for (i_0 = 0; i_0 <= c; i_0++) {
    for (i = 0; i <= b; i++) {
      toCalculate_data[i + (b + 1) * i_0] = true;
    }
  }
}

boolean_T PathFollowingControllerRefMdlModelClass::sortLE(const real_T v_data[],
  const int32_T v_size[2], int32_T idx1, int32_T idx2)
{
  real_T v1;
  real_T v2;
  boolean_T p;
  p = true;
  v1 = v_data[(idx1 + v_size[0]) - 1];
  v2 = v_data[(idx2 + v_size[0]) - 1];
  if ((v1 == v2) || (rtIsNaN(v1) && rtIsNaN(v2)) || (v1 <= v2) || rtIsNaN(v2)) {
  } else {
    p = false;
  }

  return p;
}

void PathFollowingControllerRefMdlModelClass::sortrows(const real_T y_data[],
  const int32_T y_size[2], real_T b_y_data[], int32_T b_y_size[2])
{
  real_T ycol_data[200];
  int32_T idx_data[200];
  int32_T iwork_data[200];
  int32_T i;
  int32_T i2;
  int32_T j;
  int32_T k;
  int32_T kEnd;
  int32_T n;
  int32_T p;
  int32_T pEnd;
  int32_T q;
  int32_T qEnd;
  n = y_size[0] + 1;
  i = y_size[0];
  if (0 <= i - 1) {
    std::memset(&idx_data[0], 0, i * sizeof(int32_T));
  }

  if (y_size[0] == 0) {
    for (i = 0; i <= n - 2; i++) {
      idx_data[i] = i + 1;
    }
  } else {
    i2 = y_size[0];
    for (i = 1; i <= i2 - 1; i += 2) {
      if (sortLE(y_data, y_size, i, i + 1)) {
        idx_data[i - 1] = i;
        idx_data[i] = i + 1;
      } else {
        idx_data[i - 1] = i + 1;
        idx_data[i] = i;
      }
    }

    if ((y_size[0] & 1U) != 0U) {
      idx_data[y_size[0] - 1] = y_size[0];
    }

    i = 2;
    while (i < n - 1) {
      i2 = i << 1;
      j = 1;
      for (pEnd = i + 1; pEnd < n; pEnd = qEnd + i) {
        p = j - 1;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          if (sortLE(y_data, y_size, idx_data[p], idx_data[q])) {
            iwork_data[k] = idx_data[p];
            p++;
            if (p + 1 == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                iwork_data[k] = idx_data[q];
                q++;
              }
            }
          } else {
            iwork_data[k] = idx_data[q];
            q++;
            if (q + 1 == qEnd) {
              while (p + 1 < pEnd) {
                k++;
                iwork_data[k] = idx_data[p];
                p++;
              }
            }
          }

          k++;
        }

        for (pEnd = 0; pEnd < kEnd; pEnd++) {
          idx_data[(j + pEnd) - 1] = iwork_data[pEnd];
        }

        j = qEnd;
      }

      i = i2;
    }
  }

  b_y_size[0] = y_size[0];
  b_y_size[1] = 2;
  i = y_size[0] * y_size[1] - 1;
  if (0 <= i) {
    std::memcpy(&b_y_data[0], &y_data[0], (i + 1) * sizeof(real_T));
  }

  n = y_size[0] - 1;
  for (i = 0; i <= n; i++) {
    ycol_data[i] = b_y_data[idx_data[i] - 1];
  }

  if (0 <= n) {
    std::memcpy(&b_y_data[0], &ycol_data[0], (n + 1) * sizeof(real_T));
  }

  for (i = 0; i <= n; i++) {
    ycol_data[i] = b_y_data[(idx_data[i] + b_y_size[0]) - 1];
  }

  for (i = 0; i <= n; i++) {
    b_y_data[i + b_y_size[0]] = ycol_data[i];
  }
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj1qb(int32_T n, const
  real_T x[54], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  kend = ix0 + n;
  for (k = ix0; k < kend; k++) {
    absxk = std::abs(x[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

void PathFollowingControllerRefMdlModelClass::xgeqrf(const real_T A[54], real_T
  b_A[54], real_T tau[6])
{
  real_T work[6];
  real_T b_atmp;
  real_T xnorm;
  int32_T b_i;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T iac;
  int32_T ii;
  int32_T iy;
  int32_T jA;
  int32_T jy;
  int32_T knt;
  int32_T lastv;
  boolean_T exitg2;
  for (i = 0; i < 6; i++) {
    tau[i] = 0.0;
  }

  std::memcpy(&b_A[0], &A[0], 54U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    work[i] = 0.0;
  }

  for (b_i = 0; b_i < 6; b_i++) {
    ii = b_i * 9 + b_i;
    lastv = ii + 2;
    b_atmp = b_A[ii];
    tau[b_i] = 0.0;
    xnorm = xnrm2_n1aj1qb(8 - b_i, b_A, ii + 2);
    if (xnorm != 0.0) {
      xnorm = rt_hypotd_snf(b_A[ii], xnorm);
      if (b_A[ii] >= 0.0) {
        xnorm = -xnorm;
      }

      if (std::abs(xnorm) < 1.0020841800044864E-292) {
        knt = -1;
        jA = ii - b_i;
        do {
          knt++;
          for (i = lastv; i <= jA + 9; i++) {
            b_A[i - 1] *= 9.9792015476736E+291;
          }

          xnorm *= 9.9792015476736E+291;
          b_atmp *= 9.9792015476736E+291;
        } while (!(std::abs(xnorm) >= 1.0020841800044864E-292));

        xnorm = rt_hypotd_snf(b_atmp, xnrm2_n1aj1qb(8 - b_i, b_A, ii + 2));
        if (b_atmp >= 0.0) {
          xnorm = -xnorm;
        }

        tau[b_i] = (xnorm - b_atmp) / xnorm;
        b_atmp = 1.0 / (b_atmp - xnorm);
        for (i = lastv; i <= jA + 9; i++) {
          b_A[i - 1] *= b_atmp;
        }

        for (i = 0; i <= knt; i++) {
          xnorm *= 1.0020841800044864E-292;
        }

        b_atmp = xnorm;
      } else {
        tau[b_i] = (xnorm - b_A[ii]) / xnorm;
        b_atmp = 1.0 / (b_A[ii] - xnorm);
        jA = ii - b_i;
        for (i = lastv; i <= jA + 9; i++) {
          b_A[i - 1] *= b_atmp;
        }

        b_atmp = xnorm;
      }
    }

    b_A[ii] = b_atmp;
    if (b_i + 1 < 6) {
      b_atmp = b_A[ii];
      b_A[ii] = 1.0;
      if (tau[b_i] != 0.0) {
        lastv = 9 - b_i;
        i = ii - b_i;
        while ((lastv > 0) && (b_A[i + 8] == 0.0)) {
          lastv--;
          i--;
        }

        i = 5 - b_i;
        exitg2 = false;
        while ((!exitg2) && (i > 0)) {
          knt = (i - 1) * 9 + ii;
          jy = knt + 10;
          do {
            exitg1 = 0;
            if (jy <= (knt + lastv) + 9) {
              if (b_A[jy - 1] != 0.0) {
                exitg1 = 1;
              } else {
                jy++;
              }
            } else {
              i--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        i--;
      } else {
        lastv = 0;
        i = -1;
      }

      if (lastv > 0) {
        if (i + 1 != 0) {
          if (0 <= i) {
            std::memset(&work[0], 0, (i + 1) * sizeof(real_T));
          }

          iy = 0;
          jA = 9 * i + ii;
          for (iac = ii + 10; iac <= jA + 10; iac += 9) {
            knt = ii;
            xnorm = 0.0;
            d = iac + lastv;
            for (jy = iac; jy < d; jy++) {
              xnorm += b_A[jy - 1] * b_A[knt];
              knt++;
            }

            work[iy] += xnorm;
            iy++;
          }
        }

        if (!(-tau[b_i] == 0.0)) {
          jA = ii;
          jy = 0;
          for (iy = 0; iy <= i; iy++) {
            if (work[jy] != 0.0) {
              xnorm = work[jy] * -tau[b_i];
              knt = ii;
              iac = (lastv + jA) + 9;
              for (d = jA + 10; d <= iac; d++) {
                b_A[d - 1] += b_A[knt] * xnorm;
                knt++;
              }
            }

            jy++;
            jA += 9;
          }
        }
      }

      b_A[ii] = b_atmp;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::qr_b(const real_T A[54], real_T Q
  [54], real_T R[36])
{
  real_T b_A[54];
  real_T tau[6];
  real_T work[6];
  real_T c;
  int32_T coltop;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T iac;
  int32_T iaii;
  int32_T itau;
  int32_T iy;
  int32_T jA;
  int32_T jy;
  int32_T lastc;
  int32_T lastv;
  boolean_T exitg2;
  xgeqrf(A, b_A, tau);
  itau = 5;
  for (lastv = 0; lastv < 6; lastv++) {
    for (i = 0; i <= lastv; i++) {
      R[i + 6 * lastv] = b_A[9 * lastv + i];
    }

    for (i = lastv + 2; i < 7; i++) {
      R[(i + 6 * lastv) - 1] = 0.0;
    }

    work[lastv] = 0.0;
  }

  for (i = 5; i >= 0; i--) {
    iaii = (i * 9 + i) + 9;
    if (i + 1 < 6) {
      b_A[iaii - 9] = 1.0;
      if (tau[itau] != 0.0) {
        lastv = 9 - i;
        lastc = (iaii - i) - 1;
        while ((lastv > 0) && (b_A[lastc] == 0.0)) {
          lastv--;
          lastc--;
        }

        lastc = 5 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc > 0)) {
          coltop = (lastc - 1) * 9 + iaii;
          jA = coltop + 1;
          do {
            exitg1 = 0;
            if (jA <= coltop + lastv) {
              if (b_A[jA - 1] != 0.0) {
                exitg1 = 1;
              } else {
                jA++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        lastc--;
      } else {
        lastv = 0;
        lastc = -1;
      }

      if (lastv > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work[0], 0, (lastc + 1) * sizeof(real_T));
          }

          iy = 0;
          jy = 9 * lastc + iaii;
          for (iac = iaii + 1; iac <= jy + 1; iac += 9) {
            coltop = iaii - 9;
            c = 0.0;
            d = iac + lastv;
            for (jA = iac; jA < d; jA++) {
              c += b_A[jA - 1] * b_A[coltop];
              coltop++;
            }

            work[iy] += c;
            iy++;
          }
        }

        if (!(-tau[itau] == 0.0)) {
          jA = iaii;
          jy = 0;
          for (iy = 0; iy <= lastc; iy++) {
            if (work[jy] != 0.0) {
              c = work[jy] * -tau[itau];
              coltop = iaii - 9;
              iac = lastv + jA;
              for (d = jA + 1; d <= iac; d++) {
                b_A[d - 1] += b_A[coltop] * c;
                coltop++;
              }
            }

            jy++;
            jA += 9;
          }
        }
      }
    }

    jy = iaii - i;
    for (lastv = iaii - 7; lastv <= jy; lastv++) {
      b_A[lastv - 1] *= -tau[itau];
    }

    b_A[iaii - 9] = 1.0 - tau[itau];
    jy = i - 1;
    for (lastv = 0; lastv <= jy; lastv++) {
      b_A[(iaii - lastv) - 10] = 0.0;
    }

    itau--;
  }

  for (itau = 0; itau < 6; itau++) {
    std::memcpy(&Q[itau * 9], &b_A[itau * 9], 9U * sizeof(real_T));
  }
}

void PathFollowingControllerRefMdlModelClass::predictTrackFilter
  (c_trackingEKF_LFRefMdl_T *filter, real_T dt)
{
  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  static const int8_T tmp_0[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  real_T unusedU0[54];
  real_T y_0[54];
  real_T dFdx[36];
  real_T y[36];
  real_T dFdw[18];
  real_T dFdw_0[18];
  real_T stateCol[6];
  real_T B_idx_0;
  real_T B_idx_0_tmp;
  int32_T aoffset;
  int32_T b_i;
  int32_T b_j;
  int32_T b_k;
  int32_T coffset;
  if ((!filter->pIsSetStateCovariance) || (filter->pSqrtStateCovarianceScalar !=
       -1.0)) {
    for (b_j = 0; b_j < 36; b_j++) {
      filter->pSqrtStateCovariance[b_j] = filter->pSqrtStateCovarianceScalar *
        static_cast<real_T>(tmp[b_j]);
    }

    filter->pIsSetStateCovariance = true;
    filter->pSqrtStateCovarianceScalar = -1.0;
  }

  if ((!filter->pIsSetProcessNoise) || (filter->pSqrtProcessNoiseScalar != -1.0))
  {
    for (b_j = 0; b_j < 9; b_j++) {
      filter->pSqrtProcessNoise[b_j] = filter->pSqrtProcessNoiseScalar *
        static_cast<real_T>(tmp_0[b_j]);
    }

    filter->pIsSetProcessNoise = true;
    filter->pSqrtProcessNoiseScalar = -1.0;
  }

  if (filter->pIsFirstCallPredict) {
    if (!filter->pIsValidStateTransitionFcn) {
      filter->pIsValidStateTransitionFcn = true;
    }

    filter->pIsFirstCallPredict = false;
  }

  B_idx_0_tmp = dt * dt;
  B_idx_0 = B_idx_0_tmp / 2.0;
  std::memset(&dFdx[0], 0, 36U * sizeof(real_T));
  dFdx[0] = 1.0;
  dFdx[14] = 1.0;
  dFdx[28] = 1.0;
  dFdx[1] = 0.0;
  dFdx[15] = 0.0;
  dFdx[29] = 0.0;
  dFdx[6] = dt;
  dFdx[20] = dt;
  dFdx[34] = dt;
  dFdx[7] = 1.0;
  dFdx[21] = 1.0;
  dFdx[35] = 1.0;
  std::memset(&dFdw[0], 0, 18U * sizeof(real_T));
  dFdw[0] = B_idx_0;
  dFdw[8] = B_idx_0;
  dFdw[16] = B_idx_0;
  dFdw[1] = dt;
  dFdw[9] = dt;
  dFdw[17] = dt;
  for (b_j = 0; b_j < 6; b_j++) {
    stateCol[b_j] = filter->pState[b_j];
    coffset = b_j * 6 - 1;
    for (b_i = 0; b_i < 6; b_i++) {
      aoffset = b_i * 6 - 1;
      B_idx_0 = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        B_idx_0 += filter->pSqrtStateCovariance[(aoffset + b_k) + 1] * dFdx[b_k *
          6 + b_j];
      }

      y[(coffset + b_i) + 1] = B_idx_0;
    }
  }

  B_idx_0_tmp = B_idx_0_tmp * 0.5 * 0.0;
  stateCol[0] = B_idx_0_tmp + (stateCol[1] * dt + stateCol[0]);
  stateCol[1] += 0.0 * dt;
  stateCol[2] = B_idx_0_tmp + (stateCol[3] * dt + stateCol[2]);
  stateCol[3] += 0.0 * dt;
  stateCol[4] = B_idx_0_tmp + (stateCol[5] * dt + stateCol[4]);
  stateCol[5] += 0.0 * dt;
  for (b_j = 0; b_j < 3; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      b_k = b_j + 3 * b_i;
      dFdw_0[b_k] = 0.0;
      dFdw_0[b_k] += filter->pSqrtProcessNoise[3 * b_j] * dFdw[b_i];
      dFdw_0[b_k] += filter->pSqrtProcessNoise[3 * b_j + 1] * dFdw[b_i + 6];
      dFdw_0[b_k] += filter->pSqrtProcessNoise[3 * b_j + 2] * dFdw[b_i + 12];
    }
  }

  for (b_i = 0; b_i < 6; b_i++) {
    for (b_j = 0; b_j < 6; b_j++) {
      y_0[b_j + 9 * b_i] = y[6 * b_i + b_j];
    }

    y_0[9 * b_i + 6] = dFdw_0[3 * b_i];
    y_0[9 * b_i + 7] = dFdw_0[3 * b_i + 1];
    y_0[9 * b_i + 8] = dFdw_0[3 * b_i + 2];
    filter->pState[b_i] = stateCol[b_i];
  }

  qr_b(y_0, unusedU0, dFdx);
  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      filter->pSqrtStateCovariance[b_i + 6 * b_j] = dFdx[6 * b_i + b_j];
    }
  }

  filter->pIsSetStateCovariance = true;
  filter->pSqrtStateCovarianceScalar = -1.0;
  filter->pHasPrediction = true;
}

void PathFollowingControllerRefMdlModelClass::get_match(const char_T str_data[],
  const int32_T str_size[2], char_T match_data[], int32_T match_size[2], real_T *
  nmatched)
{
  static const char_T tmp_2[128] = { '\x00', '\x01', '\x02', '\x03', '\x04',
    '\x05', '\x06', '\x07', '\x08', '	', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e',
    '\x0f', '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
    '\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', ' ', '!',
    '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']',
    '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{',
    '|', '}', '~', '\x7f' };

  static const char_T tmp_3[11] = { 'r', 'e', 'c', 't', 'a', 'n', 'g', 'u', 'l',
    'a', 'r' };

  static const char_T tmp_1[9] = { 's', 'p', 'h', 'e', 'r', 'i', 'c', 'a', 'l' };

  static const char_T tmp_0[7] = { 'I', 'n', 'v', 'a', 'l', 'i', 'd' };

  int32_T exitg1;
  int32_T minnanb;
  int32_T ns;
  int32_T partial_match_size_idx_1;
  char_T b_1[11];
  char_T c_vstr[11];
  char_T partial_match_data[11];
  char_T b_0[9];
  char_T b_vstr[9];
  char_T b[7];
  char_T tmp;
  boolean_T b_bool;
  boolean_T guard1 = false;
  boolean_T guard11 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;
  boolean_T guard4 = false;
  boolean_T matched;
  partial_match_size_idx_1 = 0;
  guard1 = false;
  guard2 = false;
  guard3 = false;
  guard4 = false;
  if (str_size[1] <= 7) {
    for (minnanb = 0; minnanb < 7; minnanb++) {
      b[minnanb] = tmp_0[minnanb];
    }

    b_bool = false;
    minnanb = 1;
    do {
      exitg1 = 0;
      if (minnanb - 1 < 7) {
        if (tmp_2[static_cast<uint8_T>(str_data[minnanb - 1])] != tmp_2[
            static_cast<int32_T>(b[minnanb - 1])]) {
          exitg1 = 1;
        } else {
          minnanb++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);

    if (b_bool) {
      *nmatched = 1.0;
      match_size[0] = 1;
      match_size[1] = 7;
      for (minnanb = 0; minnanb < 7; minnanb++) {
        match_data[minnanb] = tmp_0[minnanb];
      }
    } else {
      guard4 = true;
    }
  } else {
    guard4 = true;
  }

  if (guard4) {
    for (minnanb = 0; minnanb < 9; minnanb++) {
      b_vstr[minnanb] = tmp_1[minnanb];
    }

    if (str_size[1] <= 9) {
      ns = str_size[1];
      for (minnanb = 0; minnanb < 9; minnanb++) {
        b_0[minnanb] = tmp_1[minnanb];
      }

      b_bool = false;
      minnanb = str_size[1];
      guard11 = false;
      if (ns <= minnanb) {
        if (minnanb < ns) {
          ns = minnanb;
        }

        ns--;
        guard11 = true;
      } else {
        if (str_size[1] == 9) {
          ns = 8;
          guard11 = true;
        }
      }

      if (guard11) {
        minnanb = 1;
        do {
          exitg1 = 0;
          if (minnanb - 1 <= ns) {
            if (tmp_2[static_cast<uint8_T>(str_data[minnanb - 1])] != tmp_2[
                static_cast<int32_T>(b_0[minnanb - 1])]) {
              exitg1 = 1;
            } else {
              minnanb++;
            }
          } else {
            b_bool = true;
            exitg1 = 1;
          }
        } while (exitg1 == 0);
      }

      if (b_bool) {
        if (str_size[1] == 9) {
          *nmatched = 1.0;
          match_size[0] = 1;
          match_size[1] = 9;
          for (minnanb = 0; minnanb < 9; minnanb++) {
            match_data[minnanb] = b_vstr[minnanb];
          }
        } else {
          partial_match_size_idx_1 = 9;
          for (minnanb = 0; minnanb < 9; minnanb++) {
            partial_match_data[minnanb] = b_vstr[minnanb];
          }

          matched = true;
          *nmatched = 1.0;
          guard2 = true;
        }
      } else {
        guard3 = true;
      }
    } else {
      guard3 = true;
    }
  }

  if (guard3) {
    matched = false;
    *nmatched = 0.0;
    guard2 = true;
  }

  if (guard2) {
    ns = str_size[1];
    for (minnanb = 0; minnanb < 11; minnanb++) {
      tmp = tmp_3[minnanb];
      c_vstr[minnanb] = tmp;
      b_1[minnanb] = tmp;
    }

    b_bool = false;
    if (str_size[1] < 11) {
      minnanb = str_size[1];
    } else {
      minnanb = 11;
    }

    guard11 = false;
    if (ns <= minnanb) {
      if (minnanb < ns) {
        ns = minnanb;
      }

      ns--;
      guard11 = true;
    } else {
      if (str_size[1] == 11) {
        ns = 10;
        guard11 = true;
      }
    }

    if (guard11) {
      minnanb = 1;
      do {
        exitg1 = 0;
        if (minnanb - 1 <= ns) {
          if (tmp_2[static_cast<uint8_T>(str_data[minnanb - 1])] != tmp_2[
              static_cast<int32_T>(b_1[minnanb - 1])]) {
            exitg1 = 1;
          } else {
            minnanb++;
          }
        } else {
          b_bool = true;
          exitg1 = 1;
        }
      } while (exitg1 == 0);
    }

    if (b_bool) {
      if (str_size[1] == 11) {
        *nmatched = 1.0;
        match_size[0] = 1;
        match_size[1] = 11;
        for (minnanb = 0; minnanb < 11; minnanb++) {
          match_data[minnanb] = c_vstr[minnanb];
        }
      } else {
        if (!matched) {
          partial_match_size_idx_1 = 11;
          for (minnanb = 0; minnanb < 11; minnanb++) {
            partial_match_data[minnanb] = c_vstr[minnanb];
          }
        }

        (*nmatched)++;
        guard1 = true;
      }
    } else {
      guard1 = true;
    }
  }

  if (guard1) {
    if (*nmatched == 0.0) {
      match_size[0] = 1;
      match_size[1] = 0;
    } else {
      match_size[0] = 1;
      match_size[1] = partial_match_size_idx_1;
      partial_match_size_idx_1--;
      if (0 <= partial_match_size_idx_1) {
        std::memcpy(&match_data[0], &partial_match_data[0],
                    (partial_match_size_idx_1 + 1) * sizeof(char_T));
      }
    }
  }
}

void PathFollowingControllerRefMdlModelClass::cvmeasjac(const real_T state[6],
  drivingCoordinateFrameType varargin_1_Frame, const real_T
  varargin_1_OriginPosition[3], const real_T varargin_1_Orientation[9],
  boolean_T varargin_1_HasVelocity, boolean_T varargin_1_HasElevation, real_T
  jacobian_data[], int32_T jacobian_size[2])
{
  static const char_T tmp_1[11] = { 'r', 'e', 'c', 't', 'a', 'n', 'g', 'u', 'l',
    'a', 'r' };

  static const int8_T tmp_2[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  static const int8_T tmp_3[9] = { 0, 0, 1, 0, 0, 1, 0, 0, 1 };

  real_T A[9];
  real_T b_jacobianRangeRateRow[9];
  real_T tmp[9];
  real_T relposlocal[3];
  real_T relvel[3];
  real_T tgtpos[3];
  real_T tgtpos_0[3];
  real_T A_tmp_0;
  real_T A_tmp_1;
  real_T b_x;
  real_T b_x_0;
  real_T relposlocal_idx_0;
  real_T relposlocal_idx_0_0;
  real_T relposlocal_idx_0_1;
  real_T relposlocal_idx_1;
  real_T relposlocal_idx_1_0;
  real_T relposlocal_idx_1_1;
  real_T relposlocal_idx_2;
  real_T x;
  real_T xyzsq;
  int32_T m_data[3];
  int32_T frame_size[2];
  int32_T partial_match_size[2];
  int32_T A_tmp;
  int32_T A_tmp_2;
  int32_T exitg1;
  int32_T k_i;
  int32_T loop_ub;
  int32_T measSize;
  char_T b[11];
  char_T frame_data[11];
  int8_T tmp_0[6];
  boolean_T measLogicalIndex[3];
  boolean_T b_bool;
  boolean_T hasRange;
  char_k(varargin_1_Frame, frame_data, frame_size);
  hasRange = !strcmp_m(frame_data, frame_size);
  get_match(frame_data, frame_size, b, partial_match_size, &x);
  if ((x == 0.0) || (partial_match_size[1] == 0)) {
    frame_size[1] = 0;
  } else {
    frame_size[1] = partial_match_size[1];
    loop_ub = partial_match_size[0] * partial_match_size[1] - 1;
    if (0 <= loop_ub) {
      std::memcpy(&frame_data[0], &b[0], (loop_ub + 1) * sizeof(char_T));
    }
  }

  for (loop_ub = 0; loop_ub < 11; loop_ub++) {
    b[loop_ub] = tmp_1[loop_ub];
  }

  b_bool = false;
  if (frame_size[1] == 11) {
    measSize = 1;
    do {
      exitg1 = 0;
      if (measSize - 1 < 11) {
        if (frame_data[measSize - 1] != b[measSize - 1]) {
          exitg1 = 1;
        } else {
          measSize++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  for (loop_ub = 0; loop_ub < 3; loop_ub++) {
    for (k_i = 0; k_i < 3; k_i++) {
      measSize = k_i + 3 * loop_ub;
      b_jacobianRangeRateRow[measSize] = 0.0;
      b_jacobianRangeRateRow[measSize] += varargin_1_Orientation[3 * loop_ub] *
        static_cast<real_T>(tmp_2[k_i]);
      b_jacobianRangeRateRow[measSize] += varargin_1_Orientation[3 * loop_ub + 1]
        * static_cast<real_T>(tmp_2[k_i + 3]);
      b_jacobianRangeRateRow[measSize] += varargin_1_Orientation[3 * loop_ub + 2]
        * static_cast<real_T>(tmp_2[k_i + 6]);
    }
  }

  for (loop_ub = 0; loop_ub < 3; loop_ub++) {
    x = norm_m(&b_jacobianRangeRateRow[3 * loop_ub]);
    b_jacobianRangeRateRow[3 * loop_ub] /= x;
    measSize = 3 * loop_ub + 1;
    b_jacobianRangeRateRow[measSize] /= x;
    measSize = 3 * loop_ub + 2;
    b_jacobianRangeRateRow[measSize] /= x;
  }

  if (b_bool) {
    if (!varargin_1_HasVelocity) {
      jacobian_size[0] = 3;
      jacobian_size[1] = 6;
      std::memset(&jacobian_data[0], 0, 18U * sizeof(real_T));
      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        measSize = 3 * (((loop_ub + 1) << 1) - 2);
        jacobian_data[measSize] = b_jacobianRangeRateRow[loop_ub];
        jacobian_data[measSize + 1] = b_jacobianRangeRateRow[loop_ub + 3];
        jacobian_data[measSize + 2] = b_jacobianRangeRateRow[loop_ub + 6];
      }
    } else {
      jacobian_size[0] = 6;
      jacobian_size[1] = 6;
      std::memset(&jacobian_data[0], 0, 36U * sizeof(real_T));
      for (k_i = 0; k_i < 3; k_i++) {
        loop_ub = (k_i + 1) << 1;
        x = b_jacobianRangeRateRow[k_i];
        measSize = 6 * (loop_ub - 2);
        jacobian_data[measSize] = x;
        A[3 * k_i] = x;
        x = b_jacobianRangeRateRow[k_i + 3];
        jacobian_data[measSize + 1] = x;
        A_tmp_2 = 3 * k_i + 1;
        A[A_tmp_2] = x;
        x = b_jacobianRangeRateRow[k_i + 6];
        jacobian_data[measSize + 2] = x;
        A_tmp = 3 * k_i + 2;
        A[A_tmp] = x;
        measSize = 6 * (loop_ub - 1);
        jacobian_data[measSize + 3] = A[3 * k_i];
        jacobian_data[measSize + 4] = A[A_tmp_2];
        jacobian_data[measSize + 5] = A[A_tmp];
      }
    }
  } else {
    if (varargin_1_HasVelocity && hasRange) {
      b_bool = true;
    } else {
      b_bool = false;
    }

    measSize = ((varargin_1_HasElevation + hasRange) + b_bool) + 1;
    jacobian_size[0] = measSize;
    jacobian_size[1] = 6;
    loop_ub = measSize * 6 - 1;
    if (0 <= loop_ub) {
      std::memset(&jacobian_data[0], 0, (loop_ub + 1) * sizeof(real_T));
    }

    for (loop_ub = 0; loop_ub < 3; loop_ub++) {
      x = static_cast<real_T>(tmp_2[loop_ub + 6]) * varargin_1_OriginPosition[2]
        + (static_cast<real_T>(tmp_2[loop_ub + 3]) * varargin_1_OriginPosition[1]
           + static_cast<real_T>(tmp_2[loop_ub]) * varargin_1_OriginPosition[0]);
      tgtpos_0[loop_ub] = state[loop_ub << 1] - x;
      relvel[loop_ub] = x;
    }

    for (loop_ub = 0; loop_ub < 3; loop_ub++) {
      relposlocal[loop_ub] = b_jacobianRangeRateRow[3 * loop_ub + 2] * tgtpos_0
        [2] + (b_jacobianRangeRateRow[3 * loop_ub + 1] * tgtpos_0[1] +
               b_jacobianRangeRateRow[3 * loop_ub] * tgtpos_0[0]);
    }

    x = relposlocal[0] * relposlocal[0] + relposlocal[1] * relposlocal[1];
    xyzsq = x + relposlocal[2] * relposlocal[2];
    std::memset(&A[0], 0, 9U * sizeof(real_T));
    if (xyzsq == 0.0) {
      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        for (k_i = 0; k_i < 3; k_i++) {
          A_tmp_2 = k_i + 3 * loop_ub;
          A[A_tmp_2] = 0.0;
          A[A_tmp_2] += static_cast<real_T>(tmp_3[3 * loop_ub]) *
            b_jacobianRangeRateRow[k_i];
          A[A_tmp_2] += static_cast<real_T>(tmp_3[3 * loop_ub + 1]) *
            b_jacobianRangeRateRow[k_i + 3];
          A[A_tmp_2] += static_cast<real_T>(tmp_3[3 * loop_ub + 2]) *
            b_jacobianRangeRateRow[k_i + 6];
        }
      }
    } else if (x == 0.0) {
      x = -1.0 / relposlocal[2] * 180.0 / 3.1415926535897931;
      tmp[1] = x;
      tmp[4] = x;
      tmp[7] = 0.0;
      tmp[0] = 0.0;
      tmp[2] = 0.0;
      tmp[3] = 0.0;
      tmp[5] = 0.0;
      tmp[6] = 0.0;
      tmp[8] = 1.0;
      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        for (k_i = 0; k_i < 3; k_i++) {
          A_tmp_2 = k_i + 3 * loop_ub;
          A[A_tmp_2] = 0.0;
          A[A_tmp_2] += tmp[3 * loop_ub] * b_jacobianRangeRateRow[k_i];
          A[A_tmp_2] += tmp[3 * loop_ub + 1] * b_jacobianRangeRateRow[k_i + 3];
          A[A_tmp_2] += tmp[3 * loop_ub + 2] * b_jacobianRangeRateRow[k_i + 6];
        }
      }
    } else {
      relposlocal_idx_0 = -relposlocal[1];
      relposlocal_idx_1 = relposlocal[0];
      b_x = std::sqrt(x);
      relposlocal_idx_0_0 = -relposlocal[0] * relposlocal[2];
      relposlocal_idx_1_0 = -relposlocal[1] * relposlocal[2];
      b_x_0 = std::sqrt(xyzsq);
      relposlocal_idx_0_1 = relposlocal[0];
      relposlocal_idx_1_1 = relposlocal[1];
      relposlocal_idx_2 = relposlocal[2];
      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        A_tmp_0 = b_jacobianRangeRateRow[loop_ub + 3];
        A_tmp_1 = b_jacobianRangeRateRow[loop_ub + 6];
        A[3 * loop_ub] = ((A_tmp_0 * relposlocal_idx_1 +
                           b_jacobianRangeRateRow[loop_ub] * relposlocal_idx_0)
                          + A_tmp_1 * 0.0) / x;
        A_tmp_2 = 3 * loop_ub + 1;
        A[A_tmp_2] = ((A_tmp_0 * relposlocal_idx_1_0 +
                       b_jacobianRangeRateRow[loop_ub] * relposlocal_idx_0_0) +
                      A_tmp_1 * x) / b_x / xyzsq;
        A[3 * loop_ub + 2] = ((A_tmp_0 * relposlocal_idx_1_1 +
          b_jacobianRangeRateRow[loop_ub] * relposlocal_idx_0_1) + A_tmp_1 *
                              relposlocal_idx_2) / b_x_0;
        A[3 * loop_ub] = A[3 * loop_ub] * 180.0 / 3.1415926535897931;
        A[A_tmp_2] = A[A_tmp_2] * 180.0 / 3.1415926535897931;
      }
    }

    measLogicalIndex[0] = true;
    measLogicalIndex[1] = varargin_1_HasElevation;
    measLogicalIndex[2] = hasRange;
    if (!b_bool) {
      A_tmp_2 = 0;
      loop_ub = 0;
      for (k_i = 0; k_i < 3; k_i++) {
        if (measLogicalIndex[k_i]) {
          A_tmp_2++;
          m_data[loop_ub] = k_i + 1;
          loop_ub++;
        }
      }

      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        for (k_i = 0; k_i < A_tmp_2; k_i++) {
          jacobian_data[k_i + measSize * (loop_ub << 1)] = A[(3 * loop_ub +
            m_data[k_i]) - 1];
        }
      }
    } else {
      A_tmp_2 = 0;
      loop_ub = 0;
      for (k_i = 0; k_i < 3; k_i++) {
        if (measLogicalIndex[k_i]) {
          A_tmp_2++;
          m_data[loop_ub] = k_i + 1;
          loop_ub++;
        }
      }

      for (loop_ub = 0; loop_ub < 3; loop_ub++) {
        for (k_i = 0; k_i < A_tmp_2; k_i++) {
          jacobian_data[k_i + measSize * (loop_ub << 1)] = A[(3 * loop_ub +
            m_data[k_i]) - 1];
        }
      }

      tgtpos[0] = state[0] - relvel[0];
      tgtpos[1] = state[2] - relvel[1];
      tgtpos[2] = state[4] - relvel[2];
      relvel[0] = state[1];
      relvel[1] = state[3];
      relvel[2] = state[5];
      x = norm_m(tgtpos);
      xyzsq = norm_m(relvel);
      if (x > 0.0) {
        xyzsq = (tgtpos[0] * state[1] + tgtpos[1] * state[3]) + tgtpos[2] *
          state[5];
        relposlocal_idx_0 = x * x;
        b_jacobianRangeRateRow[0] = (state[1] * x - xyzsq * tgtpos[0] / x) /
          relposlocal_idx_0;
        b_jacobianRangeRateRow[1] = tgtpos[0] / x;
        b_jacobianRangeRateRow[2] = 0.0;
        b_jacobianRangeRateRow[3] = (state[3] * x - xyzsq * tgtpos[1] / x) /
          relposlocal_idx_0;
        b_jacobianRangeRateRow[4] = tgtpos[1] / x;
        b_jacobianRangeRateRow[5] = 0.0;
        b_jacobianRangeRateRow[6] = (state[5] * x - xyzsq * tgtpos[2] / x) /
          relposlocal_idx_0;
        b_jacobianRangeRateRow[7] = tgtpos[2] / x;
        b_jacobianRangeRateRow[8] = 0.0;
      } else if (xyzsq != 0.0) {
        b_jacobianRangeRateRow[0] = 0.0;
        b_jacobianRangeRateRow[1] = state[1] / xyzsq;
        b_jacobianRangeRateRow[2] = 0.0;
        b_jacobianRangeRateRow[3] = 0.0;
        b_jacobianRangeRateRow[4] = state[3] / xyzsq;
        b_jacobianRangeRateRow[5] = 0.0;
        b_jacobianRangeRateRow[6] = 0.0;
        b_jacobianRangeRateRow[7] = state[5] / xyzsq;
        b_jacobianRangeRateRow[8] = 0.0;
      } else {
        b_jacobianRangeRateRow[0] = 0.0;
        b_jacobianRangeRateRow[1] = 1.0;
        b_jacobianRangeRateRow[2] = 0.0;
        b_jacobianRangeRateRow[3] = 0.0;
        b_jacobianRangeRateRow[4] = 1.0;
        b_jacobianRangeRateRow[5] = 0.0;
        b_jacobianRangeRateRow[6] = 0.0;
        b_jacobianRangeRateRow[7] = 1.0;
        b_jacobianRangeRateRow[8] = 0.0;
      }

      tmp_0[0] = 0;
      tmp_0[2] = 3;
      tmp_0[4] = 6;
      tmp_0[1] = 1;
      tmp_0[3] = 4;
      tmp_0[5] = 7;
      for (loop_ub = 0; loop_ub < 6; loop_ub++) {
        jacobian_data[(measSize + measSize * loop_ub) - 1] =
          b_jacobianRangeRateRow[tmp_0[loop_ub]];
      }
    }
  }
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = std::atan2(static_cast<real_T>(u0_0), static_cast<real_T>(u1_0));
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = std::atan2(u0, u1);
  }

  return y;
}

void PathFollowingControllerRefMdlModelClass::cvmeas(const real_T state[6],
  drivingCoordinateFrameType varargin_1_Frame, const real_T
  varargin_1_OriginPosition[3], const real_T varargin_1_Orientation[9],
  boolean_T varargin_1_HasVelocity, boolean_T varargin_1_HasElevation, real_T
  measurement_data[], int32_T *measurement_size)
{
  static const char_T tmp_0[11] = { 'r', 'e', 'c', 't', 'a', 'n', 'g', 'u', 'l',
    'a', 'r' };

  static const int8_T tmp[9] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };

  real_T localAxes[9];
  real_T measurement_tmp[9];
  real_T meas[3];
  real_T originPosition[3];
  real_T tgtdirec[3];
  real_T c_data;
  real_T hypotxy;
  real_T sn;
  real_T state_idx_2;
  int32_T i_data[3];
  int32_T frame_size[2];
  int32_T partial_match_size[2];
  int32_T b_m;
  int32_T exitg1;
  int32_T h_i;
  int32_T loop_ub;
  int32_T measSize;
  char_T b[11];
  char_T frame_data[11];
  boolean_T measLogicalIndex[3];
  boolean_T b_bool;
  boolean_T hasRange;
  char_k(varargin_1_Frame, frame_data, frame_size);
  hasRange = !strcmp_m(frame_data, frame_size);
  for (measSize = 0; measSize < 3; measSize++) {
    hypotxy = static_cast<real_T>(tmp[measSize + 6]) *
      varargin_1_OriginPosition[2] + (static_cast<real_T>(tmp[measSize + 3]) *
      varargin_1_OriginPosition[1] + static_cast<real_T>(tmp[measSize]) *
      varargin_1_OriginPosition[0]);
    originPosition[measSize] = hypotxy;
    tgtdirec[measSize] = hypotxy;
  }

  get_match(frame_data, frame_size, b, partial_match_size, &hypotxy);
  if ((hypotxy == 0.0) || (partial_match_size[1] == 0)) {
    frame_size[1] = 0;
  } else {
    frame_size[1] = partial_match_size[1];
    loop_ub = partial_match_size[0] * partial_match_size[1] - 1;
    if (0 <= loop_ub) {
      std::memcpy(&frame_data[0], &b[0], (loop_ub + 1) * sizeof(char_T));
    }
  }

  for (b_m = 0; b_m < 11; b_m++) {
    b[b_m] = tmp_0[b_m];
  }

  b_bool = false;
  if (frame_size[1] == 11) {
    measSize = 1;
    do {
      exitg1 = 0;
      if (measSize - 1 < 11) {
        if (frame_data[measSize - 1] != b[measSize - 1]) {
          exitg1 = 1;
        } else {
          measSize++;
        }
      } else {
        b_bool = true;
        exitg1 = 1;
      }
    } while (exitg1 == 0);
  }

  for (b_m = 0; b_m < 3; b_m++) {
    for (loop_ub = 0; loop_ub < 3; loop_ub++) {
      measSize = loop_ub + 3 * b_m;
      localAxes[measSize] = 0.0;
      localAxes[measSize] += varargin_1_Orientation[3 * b_m] *
        static_cast<real_T>(tmp[loop_ub]);
      localAxes[measSize] += varargin_1_Orientation[3 * b_m + 1] *
        static_cast<real_T>(tmp[loop_ub + 3]);
      localAxes[measSize] += varargin_1_Orientation[3 * b_m + 2] *
        static_cast<real_T>(tmp[loop_ub + 6]);
    }
  }

  for (b_m = 0; b_m < 3; b_m++) {
    hypotxy = norm_m(&localAxes[3 * b_m]);
    localAxes[3 * b_m] /= hypotxy;
    measSize = 3 * b_m + 1;
    localAxes[measSize] /= hypotxy;
    measSize = 3 * b_m + 2;
    localAxes[measSize] /= hypotxy;
  }

  if (b_bool) {
    if (!varargin_1_HasVelocity) {
      hypotxy = state[0] - originPosition[0];
      sn = state[2] - originPosition[1];
      state_idx_2 = state[4] - originPosition[2];
      *measurement_size = 3;
      for (b_m = 0; b_m < 3; b_m++) {
        measurement_data[b_m] = localAxes[3 * b_m + 2] * state_idx_2 +
          (localAxes[3 * b_m + 1] * sn + localAxes[3 * b_m] * hypotxy);
      }
    } else {
      for (b_m = 0; b_m < 3; b_m++) {
        measurement_tmp[3 * b_m] = localAxes[b_m];
        measurement_tmp[3 * b_m + 1] = localAxes[b_m + 3];
        measurement_tmp[3 * b_m + 2] = localAxes[b_m + 6];
        meas[b_m] = state[b_m << 1] - originPosition[b_m];
      }

      for (b_m = 0; b_m < 3; b_m++) {
        tgtdirec[b_m] = state[(b_m << 1) + 1];
        originPosition[b_m] = measurement_tmp[b_m + 6] * meas[2] +
          (measurement_tmp[b_m + 3] * meas[1] + measurement_tmp[b_m] * meas[0]);
      }

      *measurement_size = 6;
      for (b_m = 0; b_m < 3; b_m++) {
        measurement_data[b_m] = originPosition[b_m];
        measurement_data[b_m + 3] = measurement_tmp[b_m + 6] * tgtdirec[2] +
          (measurement_tmp[b_m + 3] * tgtdirec[1] + measurement_tmp[b_m] *
           tgtdirec[0]);
      }
    }
  } else {
    if (varargin_1_HasVelocity && hasRange) {
      b_bool = true;
    } else {
      b_bool = false;
    }

    measSize = ((varargin_1_HasElevation + hasRange) + b_bool) + 1;
    *measurement_size = measSize;
    if (0 <= measSize - 1) {
      std::memset(&measurement_data[0], 0, measSize * sizeof(real_T));
    }

    hypotxy = state[0] - tgtdirec[0];
    sn = state[2] - tgtdirec[1];
    state_idx_2 = state[4] - tgtdirec[2];
    for (b_m = 0; b_m < 3; b_m++) {
      tgtdirec[b_m] = localAxes[3 * b_m + 2] * state_idx_2 + (localAxes[3 * b_m
        + 1] * sn + localAxes[3 * b_m] * hypotxy);
    }

    hypotxy = rt_hypotd_snf(tgtdirec[0], tgtdirec[1]);
    meas[0] = rt_atan2d_snf(tgtdirec[1], tgtdirec[0]);
    meas[1] = rt_atan2d_snf(tgtdirec[2], hypotxy);
    meas[2] = rt_hypotd_snf(hypotxy, tgtdirec[2]);
    meas[0] *= 57.295779513082323;
    meas[1] *= 57.295779513082323;
    measLogicalIndex[0] = true;
    measLogicalIndex[1] = varargin_1_HasElevation;
    measLogicalIndex[2] = hasRange;
    if (!b_bool) {
      measSize = 0;
      loop_ub = 0;
      for (b_m = 0; b_m < 3; b_m++) {
        if (measLogicalIndex[b_m]) {
          measSize++;
          i_data[loop_ub] = b_m + 1;
          loop_ub++;
        }
      }

      *measurement_size = measSize;
      for (b_m = 0; b_m < measSize; b_m++) {
        measurement_data[b_m] = meas[i_data[b_m] - 1];
      }
    } else {
      loop_ub = 0;
      b_m = 0;
      for (h_i = 0; h_i < 3; h_i++) {
        if (measLogicalIndex[h_i]) {
          loop_ub++;
          i_data[b_m] = h_i + 1;
          b_m++;
        }
      }

      for (b_m = 0; b_m < loop_ub; b_m++) {
        measurement_data[b_m] = meas[i_data[b_m] - 1];
      }

      tgtdirec[0] = state[0] - originPosition[0];
      tgtdirec[1] = state[2] - originPosition[1];
      tgtdirec[2] = state[4] - originPosition[2];
      hypotxy = std::sqrt((tgtdirec[0] * tgtdirec[0] + tgtdirec[1] * tgtdirec[1])
                          + tgtdirec[2] * tgtdirec[2]);
      sn = std::sqrt((state[1] * state[1] + state[3] * state[3]) + state[5] *
                     state[5]);
      state_idx_2 = -(((state[1] * tgtdirec[0] + state[3] * tgtdirec[1]) +
                       state[5] * tgtdirec[2]) / hypotxy);
      if (sn == 0.0) {
        state_idx_2 = 0.0;
      }

      b_m = 0;
      if (hypotxy == 0.0) {
        for (loop_ub = 0; loop_ub < 1; loop_ub++) {
          b_m++;
        }
      }

      loop_ub = b_m - 1;
      for (b_m = 0; b_m <= loop_ub; b_m++) {
        c_data = -sn;
      }

      if (hypotxy == 0.0) {
        for (b_m = 0; b_m < 1; b_m++) {
          state_idx_2 = c_data;
        }
      }

      measurement_data[measSize - 1] = -state_idx_2;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::xzgetrf(const real_T A[36], real_T
  b_A[36], int32_T ipiv[6], int32_T *info)
{
  real_T s;
  real_T smax;
  int32_T a;
  int32_T b_j;
  int32_T c;
  int32_T c_0;
  int32_T ijA;
  int32_T ix;
  int32_T iy;
  int32_T jj;
  int32_T jy;
  std::memcpy(&b_A[0], &A[0], 36U * sizeof(real_T));
  for (b_j = 0; b_j < 6; b_j++) {
    ipiv[b_j] = b_j + 1;
  }

  *info = 0;
  for (b_j = 0; b_j < 5; b_j++) {
    c = b_j * 7 + 2;
    jj = b_j * 7;
    iy = 6 - b_j;
    a = 1;
    ix = c - 2;
    smax = std::abs(b_A[jj]);
    for (jy = 2; jy <= iy; jy++) {
      ix++;
      s = std::abs(b_A[ix]);
      if (s > smax) {
        a = jy;
        smax = s;
      }
    }

    if (b_A[(c + a) - 3] != 0.0) {
      if (a - 1 != 0) {
        iy = b_j + a;
        ipiv[b_j] = iy;
        ix = b_j;
        iy--;
        for (jy = 0; jy < 6; jy++) {
          smax = b_A[ix];
          b_A[ix] = b_A[iy];
          b_A[iy] = smax;
          ix += 6;
          iy += 6;
        }
      }

      ix = c - b_j;
      for (iy = c; iy <= ix + 4; iy++) {
        b_A[iy - 1] /= b_A[jj];
      }
    } else {
      *info = b_j + 1;
    }

    iy = 4 - b_j;
    jy = jj + 6;
    for (a = 0; a <= iy; a++) {
      smax = b_A[jy];
      if (b_A[jy] != 0.0) {
        ix = c - 1;
        c_0 = jj - b_j;
        for (ijA = jj + 8; ijA <= c_0 + 12; ijA++) {
          b_A[ijA - 1] += b_A[ix] * -smax;
          ix++;
        }
      }

      jy += 6;
      jj += 6;
    }
  }

  if ((*info == 0) && (!(b_A[35] != 0.0))) {
    *info = 6;
  }
}

real_T PathFollowingControllerRefMdlModelClass::normalizedDistance(const real_T
  z_data[], const int32_T *z_size, const real_T mu_data[], const real_T sigma[36])
{
  real_T c_A[36];
  real_T b_X_data[6];
  real_T zd_data[6];
  real_T b_X;
  real_T f;
  real_T temp;
  int32_T b_ipiv[6];
  int32_T b;
  int32_T b_info;
  int32_T jAcol;
  int32_T loop_ub;
  boolean_T isodd;
  loop_ub = *z_size;
  for (b_info = 0; b_info < loop_ub; b_info++) {
    zd_data[b_info] = z_data[b_info] - mu_data[b_info];
  }

  xzgetrf(sigma, c_A, b_ipiv, &b_info);
  if (0 <= *z_size - 1) {
    std::memcpy(&b_X_data[0], &zd_data[0], *z_size * sizeof(real_T));
  }

  for (b_info = 0; b_info < 6; b_info++) {
    jAcol = 6 * b_info - 1;
    b = b_info - 1;
    for (loop_ub = 0; loop_ub <= b; loop_ub++) {
      temp = c_A[(loop_ub + jAcol) + 1];
      if (temp != 0.0) {
        b_X_data[b_info] -= temp * b_X_data[loop_ub];
      }
    }

    b_X_data[b_info] *= 1.0 / c_A[(b_info + jAcol) + 1];
  }

  for (b_info = 5; b_info >= 0; b_info--) {
    jAcol = 6 * b_info - 1;
    for (loop_ub = b_info + 2; loop_ub < 7; loop_ub++) {
      temp = c_A[loop_ub + jAcol];
      if (temp != 0.0) {
        b_X_data[b_info] -= temp * b_X_data[loop_ub - 1];
      }
    }
  }

  for (b_info = 4; b_info >= 0; b_info--) {
    jAcol = b_ipiv[b_info];
    if (b_info + 1 != jAcol) {
      temp = b_X_data[b_info];
      b_X_data[b_info] = b_X_data[jAcol - 1];
      b_X_data[jAcol - 1] = temp;
    }
  }

  xzgetrf(sigma, c_A, b_ipiv, &b_info);
  temp = c_A[0];
  isodd = false;
  for (loop_ub = 0; loop_ub < 5; loop_ub++) {
    temp *= c_A[((loop_ub + 1) * 6 + loop_ub) + 1];
    if (b_ipiv[loop_ub] > loop_ub + 1) {
      isodd = !isodd;
    }
  }

  if (isodd) {
    temp = -temp;
  }

  b_X = 0.0;
  for (b_info = 0; b_info < 6; b_info++) {
    b_X += b_X_data[b_info] * zd_data[b_info];
  }

  f = b_X + std::log(temp);
  return f;
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_calcCostOneDetection
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track, const
   real_T detection_Measurement[6], drivingCoordinateFrameType
   detection_MeasurementParameters_Frame, const real_T
   detection_MeasurementParameters_OriginPosition[3], const real_T
   detection_MeasurementParameters_Orientation[9], boolean_T
   detection_MeasurementParameters_HasVelocity, boolean_T
   detection_MeasurementParameters_HasElevation, real_T costValue_data[],
   int32_T costValue_size[2])
{
  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  c_trackingEKF_LFRefMdl_T *EKF;
  real_T a[36];
  real_T b_data[36];
  real_T dHdx_data[36];
  real_T residualCovariance[36];
  real_T y_data[36];
  real_T zEstimated_data[6];
  real_T z_in_data[6];
  real_T s;
  int32_T dHdx_size[2];
  int32_T b_i;
  int32_T b_j;
  int32_T b_k;
  int32_T boffset;
  int32_T coffset;
  int32_T i;
  int32_T m;
  int32_T zEstimated_size;
  EKF = track->pDistanceFilter;
  if ((!EKF->pIsSetStateCovariance) || (EKF->pSqrtStateCovarianceScalar != -1.0))
  {
    s = EKF->pSqrtStateCovarianceScalar;
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtStateCovariance[b_j] = s * static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetStateCovariance = true;
    EKF->pSqrtStateCovarianceScalar = -1.0;
  }

  if (EKF->pIsFirstCallCorrect) {
    if (!EKF->pIsValidMeasurementFcn) {
      EKF->pIsValidMeasurementFcn = true;
    }

    EKF->pIsFirstCallCorrect = false;
  }

  if ((!EKF->pIsSetMeasurementNoise) || (EKF->pSqrtMeasurementNoiseScalar !=
       -1.0)) {
    s = EKF->pSqrtMeasurementNoiseScalar;
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtMeasurementNoise[b_j] = s * static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetMeasurementNoise = true;
    EKF->pSqrtMeasurementNoiseScalar = -1.0;
  }

  for (i = 0; i < 6; i++) {
    z_in_data[i] = EKF->pState[i];
  }

  cvmeasjac(z_in_data, detection_MeasurementParameters_Frame,
            detection_MeasurementParameters_OriginPosition,
            detection_MeasurementParameters_Orientation,
            detection_MeasurementParameters_HasVelocity,
            detection_MeasurementParameters_HasElevation, dHdx_data, dHdx_size);
  for (i = 0; i < 6; i++) {
    z_in_data[i] = EKF->pState[i];
  }

  cvmeas(z_in_data, detection_MeasurementParameters_Frame,
         detection_MeasurementParameters_OriginPosition,
         detection_MeasurementParameters_Orientation,
         detection_MeasurementParameters_HasVelocity,
         detection_MeasurementParameters_HasElevation, zEstimated_data,
         &zEstimated_size);
  if ((!EKF->pIsSetStateCovariance) || (EKF->pSqrtStateCovarianceScalar != -1.0))
  {
    s = EKF->pSqrtStateCovarianceScalar;
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtStateCovariance[b_j] = s * static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetStateCovariance = true;
    EKF->pSqrtStateCovarianceScalar = -1.0;
  }

  for (b_j = 0; b_j < 36; b_j++) {
    y_data[b_j] = EKF->pSqrtStateCovariance[b_j];
  }

  for (b_j = 0; b_j < 36; b_j++) {
    a[b_j] = EKF->pSqrtStateCovariance[b_j];
  }

  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      i = b_j + 6 * b_i;
      residualCovariance[i] = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        residualCovariance[i] += a[6 * b_k + b_j] * y_data[6 * b_k + b_i];
      }
    }
  }

  m = dHdx_size[0];
  i = dHdx_size[0];
  for (b_j = 0; b_j < 6; b_j++) {
    coffset = b_j * m - 1;
    boffset = b_j * 6 - 1;
    for (b_i = 0; b_i < m; b_i++) {
      s = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        s += dHdx_data[b_k * dHdx_size[0] + b_i] * residualCovariance[(boffset +
          b_k) + 1];
      }

      y_data[(coffset + b_i) + 1] = s;
    }
  }

  m = dHdx_size[0] - 1;
  for (b_j = 0; b_j <= m; b_j++) {
    coffset = b_j * i - 1;
    for (b_i = 0; b_i < i; b_i++) {
      s = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        s += y_data[b_k * i + b_i] * dHdx_data[b_k * dHdx_size[0] + b_j];
      }

      b_data[(coffset + b_i) + 1] = s;
    }
  }

  if ((!EKF->pIsSetMeasurementNoise) || (EKF->pSqrtMeasurementNoiseScalar !=
       -1.0)) {
    s = EKF->pSqrtMeasurementNoiseScalar;
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtMeasurementNoise[b_j] = s * static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetMeasurementNoise = true;
    EKF->pSqrtMeasurementNoiseScalar = -1.0;
  }

  for (b_j = 0; b_j < 36; b_j++) {
    y_data[b_j] = EKF->pSqrtMeasurementNoise[b_j];
  }

  for (b_j = 0; b_j < 36; b_j++) {
    a[b_j] = EKF->pSqrtMeasurementNoise[b_j];
  }

  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      s = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        s += a[6 * b_k + b_j] * y_data[6 * b_k + b_i];
      }

      residualCovariance[b_j + 6 * b_i] = b_data[i * b_i + b_j] + s;
    }
  }

  if (1 == zEstimated_size) {
    costValue_size[0] = 1;
    costValue_size[1] = 6;
    for (i = 0; i < 6; i++) {
      costValue_data[i] = 0.0;
      for (b_j = 0; b_j < zEstimated_size; b_j++) {
        z_in_data[b_j] = detection_Measurement[i];
      }

      costValue_data[i] = normalizedDistance(z_in_data, &zEstimated_size,
        zEstimated_data, residualCovariance);
    }
  } else {
    costValue_size[0] = 1;
    costValue_size[1] = 1;
    costValue_data[0] = normalizedDistance(detection_Measurement,
      &zEstimated_size, zEstimated_data, residualCovariance);
  }
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_distance
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track, const
   BusDetectionConcatenation1Detections detections[70], const real_T
   sensorDetections_data[], const int32_T sensorDetections_size[2], real_T
   cost_data[], int32_T cost_size[2])
{
  real_T tmp_data[400];
  real_T detTimes_data[140];
  real_T costValue_data[6];
  real_T detTimes_data_0;
  real_T trackTime;
  int32_T detTimes_size[2];
  int32_T tmp_size[2];
  int32_T b;
  int32_T b_i;
  int32_T loop_ub;
  boolean_T guard1 = false;
  if (sensorDetections_size[1] == 0) {
    cost_size[0] = 1;
    cost_size[1] = 0;
  } else {
    detTimes_size[0] = sensorDetections_size[1];
    detTimes_size[1] = 2;
    loop_ub = (sensorDetections_size[1] << 1) - 1;
    if (0 <= loop_ub) {
      std::memset(&detTimes_data[0], 0, (loop_ub + 1) * sizeof(real_T));
    }

    loop_ub = sensorDetections_size[1] - 1;
    for (b_i = 0; b_i <= loop_ub; b_i++) {
      detTimes_data[b_i] = static_cast<real_T>(b_i) + 1.0;
      detTimes_data[b_i + detTimes_size[0]] = detections[static_cast<int32_T>
        (sensorDetections_data[b_i]) - 1].Time;
    }

    sortrows(detTimes_data, detTimes_size, tmp_data, tmp_size);
    loop_ub = tmp_size[0] * tmp_size[1];
    if (0 <= loop_ub - 1) {
      std::memcpy(&detTimes_data[0], &tmp_data[0], loop_ub * sizeof(real_T));
    }

    cost_size[0] = 1;
    cost_size[1] = tmp_size[0];
    loop_ub = tmp_size[0] - 1;
    for (b_i = 0; b_i <= loop_ub; b_i++) {
      cost_data[b_i] = (rtInf);
    }

    trackTime = track->Time;
    trackingEKF_sync(track->pDistanceFilter, track->Filter);
    b = tmp_size[0] - 1;
    for (loop_ub = 0; loop_ub <= b; loop_ub++) {
      detTimes_data_0 = detTimes_data[loop_ub];
      ExtendedKalmanFilter_set_MeasurementNoise(track->pDistanceFilter,
        detections[static_cast<int32_T>(sensorDetections_data
        [static_cast<int32_T>(detTimes_data_0) - 1]) - 1].MeasurementNoise);
      guard1 = false;
      if (track->ObjectClassID == 0.0) {
        guard1 = true;
      } else {
        b_i = static_cast<int32_T>(sensorDetections_data[static_cast<int32_T>
          (detTimes_data_0) - 1]) - 1;
        if ((detections[b_i].ObjectClassID == 0.0) || (detections[b_i].
             ObjectClassID == track->ObjectClassID)) {
          guard1 = true;
        }
      }

      if (guard1) {
        b_i = static_cast<int32_T>(sensorDetections_data[static_cast<int32_T>
          (detTimes_data_0) - 1]) - 1;
        trackTime = detections[b_i].Time - trackTime;
        if (trackTime > 0.0) {
          predictTrackFilter(track->pDistanceFilter, trackTime);
        }

        trackTime = detections[b_i].Time;
        ObjectTrack_calcCostOneDetection(track, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .Measurement, detections[static_cast<int32_T>(sensorDetections_data[
          static_cast<int32_T>(detTimes_data_0) - 1]) - 1].
          MeasurementParameters.Frame, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.OriginPosition, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.Orientation, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.HasVelocity, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.HasElevation, costValue_data, detTimes_size);
        cost_data[loop_ub] = costValue_data[0];
      }
    }
  }
}

void PathFollowingControllerRefMdlModelClass::AssignmentCostCalculator_stepImpl
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *tracks[100],
   const BusDetectionConcatenation1Detections detections[70], real_T
   numLiveTracks, const real_T detIndices_data[], const int32_T detIndices_size
   [2], real_T outCostMatrix_data[], int32_T outCostMatrix_size[2],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  real_T detIndices_data_0[70];
  real_T f_data[70];
  int32_T q_data[70];
  int32_T s_data[70];
  int32_T detIndices_size_0[2];
  int32_T f_size[2];
  int32_T toCalculate_size[2];
  int32_T b_end;
  int32_T b_j;
  int32_T c_i;
  int32_T d;
  int32_T d_end;
  int32_T partialTrueCount;
  int32_T trueCount;
  for (c_i = 0; c_i < 7000; c_i++) {
    localB->costMatrix_c[c_i] = (rtInf);
  }

  if ((numLiveTracks == 0.0) || (detIndices_size[1] == 0)) {
    if (1.0 > numLiveTracks) {
      d = -1;
    } else {
      d = static_cast<int32_T>(numLiveTracks) - 1;
    }

    if (1 > detIndices_size[1]) {
      b_end = -1;
    } else {
      b_end = detIndices_size[1] - 1;
    }

    outCostMatrix_size[0] = d + 1;
    outCostMatrix_size[1] = b_end + 1;
    for (c_i = 0; c_i <= b_end; c_i++) {
      for (b_j = 0; b_j <= d; b_j++) {
        outCostMatrix_data[b_j + (d + 1) * c_i] = (rtInf);
      }
    }
  } else {
    AssignmentCostCalculator_findPairsToCost(numLiveTracks, detIndices_size,
      localB->toCalculate_data, toCalculate_size);
    d = static_cast<int32_T>(numLiveTracks) - 1;
    if (0 <= static_cast<int32_T>(numLiveTracks) - 1) {
      b_end = toCalculate_size[1] - 1;
      detIndices_size_0[0] = 1;
      d_end = toCalculate_size[1] - 1;
    }

    for (b_j = 0; b_j <= d; b_j++) {
      trueCount = 0;
      partialTrueCount = 0;
      for (c_i = 0; c_i <= b_end; c_i++) {
        if (localB->toCalculate_data[toCalculate_size[0] * c_i + b_j]) {
          trueCount++;
          q_data[partialTrueCount] = c_i + 1;
          partialTrueCount++;
        }
      }

      detIndices_size_0[1] = trueCount;
      for (c_i = 0; c_i < trueCount; c_i++) {
        detIndices_data_0[c_i] = detIndices_data[q_data[c_i] - 1];
      }

      ObjectTrack_distance(tracks[b_j], detections, detIndices_data_0,
                           detIndices_size_0, f_data, f_size);
      trueCount = 0;
      for (c_i = 0; c_i <= d_end; c_i++) {
        if (localB->toCalculate_data[toCalculate_size[0] * c_i + b_j]) {
          s_data[trueCount] = c_i + 1;
          trueCount++;
        }
      }

      trueCount = f_size[1];
      for (c_i = 0; c_i < trueCount; c_i++) {
        localB->costMatrix_c[b_j + 100 * (s_data[c_i] - 1)] = f_data[c_i];
      }
    }

    if (1.0 > numLiveTracks) {
      d = -1;
    } else {
      d = static_cast<int32_T>(numLiveTracks) - 1;
    }

    trueCount = detIndices_size[1];
    outCostMatrix_size[0] = d + 1;
    outCostMatrix_size[1] = detIndices_size[1];
    for (c_i = 0; c_i < trueCount; c_i++) {
      for (b_j = 0; b_j <= d; b_j++) {
        outCostMatrix_data[b_j + (d + 1) * c_i] = localB->costMatrix_c[100 * c_i
          + b_j];
      }
    }
  }
}

void PathFollowingControllerRefMdlModelClass::SystemCore_step_b
  (f_matlabshared_tracking_internal_fusion_AssignmentCostCalcula_T *obj,
   b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *varargin_1
   [100], const BusDetectionConcatenation1Detections varargin_2[70], real_T
   varargin_3, const real_T varargin_4_data[], const int32_T varargin_4_size[2],
   real_T varargout_1_data[], int32_T varargout_1_size[2],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *varargin_1_0
    [100];
  cell_wrap_LFRefMdl_T varSizes[4];
  int32_T b_k;
  uint32_T inSize[8];
  boolean_T exitg1;
  if (obj->isInitialized != 1) {
    obj->isInitialized = 1;
    varSizes[0].f1[0] = 1U;
    varSizes[0].f1[1] = 100U;
    varSizes[1].f1[0] = 70U;
    varSizes[1].f1[1] = 1U;
    varSizes[2].f1[0] = 1U;
    varSizes[2].f1[1] = 1U;
    for (b_k = 0; b_k < 6; b_k++) {
      varSizes[0].f1[b_k + 2] = 1U;
      varSizes[1].f1[b_k + 2] = 1U;
      varSizes[2].f1[b_k + 2] = 1U;
    }

    b_k = varargin_4_size[0];
    if (varargin_4_size[0] < 0) {
      b_k = 0;
    }

    varSizes[3].f1[0] = static_cast<uint32_T>(b_k);
    b_k = varargin_4_size[1];
    if (varargin_4_size[1] < 0) {
      b_k = 0;
    }

    varSizes[3].f1[1] = static_cast<uint32_T>(b_k);
    for (b_k = 0; b_k < 6; b_k++) {
      varSizes[3].f1[b_k + 2] = 1U;
    }

    obj->inputVarSize[0] = varSizes[0];
    obj->inputVarSize[1] = varSizes[1];
    obj->inputVarSize[2] = varSizes[2];
    obj->inputVarSize[3] = varSizes[3];
  }

  inSize[0] = 1U;
  inSize[1] = 100U;
  for (b_k = 0; b_k < 6; b_k++) {
    inSize[b_k + 2] = 1U;
  }

  b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (b_k < 8)) {
    if (obj->inputVarSize[0].f1[b_k] != inSize[b_k]) {
      for (b_k = 0; b_k < 8; b_k++) {
        obj->inputVarSize[0].f1[b_k] = inSize[b_k];
      }

      exitg1 = true;
    } else {
      b_k++;
    }
  }

  inSize[0] = 70U;
  inSize[1] = 1U;
  for (b_k = 0; b_k < 6; b_k++) {
    inSize[b_k + 2] = 1U;
  }

  b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (b_k < 8)) {
    if (obj->inputVarSize[1].f1[b_k] != inSize[b_k]) {
      for (b_k = 0; b_k < 8; b_k++) {
        obj->inputVarSize[1].f1[b_k] = inSize[b_k];
      }

      exitg1 = true;
    } else {
      b_k++;
    }
  }

  b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (b_k < 8)) {
    if (obj->inputVarSize[2].f1[b_k] != 1U) {
      for (b_k = 0; b_k < 8; b_k++) {
        obj->inputVarSize[2].f1[b_k] = 1U;
      }

      exitg1 = true;
    } else {
      b_k++;
    }
  }

  b_k = varargin_4_size[0];
  if (varargin_4_size[0] < 0) {
    b_k = 0;
  }

  inSize[0] = static_cast<uint32_T>(b_k);
  b_k = varargin_4_size[1];
  if (varargin_4_size[1] < 0) {
    b_k = 0;
  }

  inSize[1] = static_cast<uint32_T>(b_k);
  for (b_k = 0; b_k < 6; b_k++) {
    inSize[b_k + 2] = 1U;
  }

  b_k = 0;
  exitg1 = false;
  while ((!exitg1) && (b_k < 8)) {
    if (obj->inputVarSize[3].f1[b_k] != inSize[b_k]) {
      for (b_k = 0; b_k < 8; b_k++) {
        obj->inputVarSize[3].f1[b_k] = inSize[b_k];
      }

      exitg1 = true;
    } else {
      b_k++;
    }
  }

  for (b_k = 0; b_k < 100; b_k++) {
    varargin_1_0[b_k] = varargin_1[b_k];
  }

  AssignmentCostCalculator_stepImpl(varargin_1_0, varargin_2, varargin_3,
    varargin_4_data, varargin_4_size, varargout_1_data, varargout_1_size, localB);
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_calcCostMatrix
  (multiObjectTracker_LFRefMdl_T *obj, const real_T SensorDetections_data[],
   const int32_T SensorDetections_size[2], real_T outCostMatrix[7000],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *obj_1[100];
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_0;
  int32_T costMatrix_size[2];
  int32_T end;
  int32_T i;
  int32_T partialTrueCount;
  int32_T trueCount;
  obj_0 = obj->cDetectionManager;
  for (i = 0; i < 70; i++) {
    localB->dets[i] = obj_0->pDetections[i];
  }

  std::memset(&outCostMatrix[0], 0, 7000U * sizeof(real_T));
  for (i = 0; i < 100; i++) {
    obj_1[i] = obj->pTracksList[i];
  }

  SystemCore_step_b(&obj->cCostCalculator, obj_1, localB->dets,
                    obj->pNumLiveTracks, SensorDetections_data,
                    SensorDetections_size, localB->costMatrix_data_k,
                    costMatrix_size, localB);
  end = costMatrix_size[0] * costMatrix_size[1] - 1;
  trueCount = 0;
  partialTrueCount = 0;
  for (i = 0; i <= end; i++) {
    if (localB->costMatrix_data_k[i] > 20.0) {
      trueCount++;
      localB->g_data[partialTrueCount] = i + 1;
      partialTrueCount++;
    }
  }

  trueCount--;
  for (i = 0; i <= trueCount; i++) {
    localB->costMatrix_data_k[localB->g_data[i] - 1] = (rtInf);
  }

  trueCount = costMatrix_size[1];
  for (i = 0; i < trueCount; i++) {
    partialTrueCount = costMatrix_size[0];
    for (end = 0; end < partialTrueCount; end++) {
      outCostMatrix[end + 100 * (static_cast<int32_T>(SensorDetections_data[i])
        - 1)] = localB->costMatrix_data_k[costMatrix_size[0] * i + end];
    }
  }
}

void PathFollowingControllerRefMdlModelClass::minimum_l(const real_T x_data[],
  const int32_T x_size[2], real_T ex_data[], int32_T *ex_size, int32_T idx_data[],
  int32_T *idx_size)
{
  real_T b_tmp;
  real_T ex_data_0;
  int32_T j;
  int32_T loop_ub;
  int32_T m;
  int32_T n;
  boolean_T p;
  m = x_size[0] - 1;
  n = x_size[1];
  *ex_size = x_size[0];
  *idx_size = x_size[0];
  loop_ub = x_size[0];
  for (j = 0; j < loop_ub; j++) {
    idx_data[j] = 1;
  }

  if (x_size[0] >= 1) {
    if (0 <= m) {
      std::memcpy(&ex_data[0], &x_data[0], (m + 1) * sizeof(real_T));
    }

    for (j = 2; j <= n; j++) {
      for (loop_ub = 0; loop_ub <= m; loop_ub++) {
        ex_data_0 = ex_data[loop_ub];
        b_tmp = x_data[(j - 1) * x_size[0] + loop_ub];
        if (rtIsNaN(b_tmp)) {
          p = false;
        } else {
          p = (rtIsNaN(ex_data_0) || (ex_data_0 > b_tmp));
        }

        if (p) {
          ex_data_0 = b_tmp;
          idx_data[loop_ub] = j;
        }

        ex_data[loop_ub] = ex_data_0;
      }
    }
  }
}

int32_T PathFollowingControllerRefMdlModelClass::percUp_p(int32_T
  heap_heapList_data[], int32_T heap_indexToHeap_data[], int32_T
  heap_currentSize, const real_T dist_data[], int32_T i)
{
  real_T tmp;
  real_T tmp_0;
  int32_T b_heap_currentSize;
  int32_T heap_heapList_idx_1;
  int32_T heap_indexToHeap_idx_1;
  int32_T iparent;
  boolean_T exitg1;
  b_heap_currentSize = heap_currentSize;
  iparent = i / 2 - 1;
  exitg1 = false;
  while ((!exitg1) && (iparent + 1 > 0)) {
    heap_heapList_idx_1 = heap_heapList_data[i - 1];
    tmp = dist_data[heap_heapList_idx_1 - 1];
    tmp_0 = dist_data[heap_heapList_data[iparent] - 1];
    if ((tmp < tmp_0) || ((tmp == tmp_0) && (heap_heapList_idx_1 <=
          heap_heapList_data[iparent]))) {
      heap_heapList_data[i - 1] = heap_heapList_data[iparent];
      heap_heapList_data[iparent] = heap_heapList_idx_1;
      heap_heapList_idx_1 = heap_heapList_data[i - 1] - 1;
      heap_indexToHeap_idx_1 = heap_indexToHeap_data[heap_heapList_idx_1];
      heap_indexToHeap_data[heap_heapList_idx_1] =
        heap_indexToHeap_data[heap_heapList_data[iparent] - 1];
      heap_indexToHeap_data[heap_heapList_data[iparent] - 1] =
        heap_indexToHeap_idx_1;
      i = iparent + 1;
      iparent = (iparent + 1) / 2 - 1;
    } else {
      exitg1 = true;
    }
  }

  return b_heap_currentSize;
}

boolean_T PathFollowingControllerRefMdlModelClass::augmentedShortestPath_k(const
  real_T matrixRep_data[], const int32_T matrixRep_size[2], real_T colStart,
  real_T rowWeight_data[], real_T colWeight_data[], int32_T matchCtoR_data[],
  int32_T matchRtoC_data[], real_T pairWeightR_data[], real_T
  newPairWeightR_data[], const int32_T *newPairWeightR_size, int32_T
  predecessorsC_data[], const int32_T *predecessorsC_size, real_T
  distancesR_data[], const int32_T *distancesR_size, uint8_T colorsR_data[],
  const int32_T *colorsR_size, int32_T queue_heapList_data[], const int32_T
  *queue_heapList_size, int32_T queue_indexToHeap_data[])
{
  real_T last_weight_sap;
  real_T lc;
  real_T lsap;
  real_T lsp;
  real_T rc;
  real_T tmp;
  real_T tmp_0;
  int32_T b_queue_heapList_data[170];
  int32_T b_queue_heapList_idx_1;
  int32_T c;
  int32_T citer;
  int32_T clast;
  int32_T exitg1;
  int32_T exitg2;
  int32_T loop_ub;
  int32_T n;
  int32_T rlast;
  int32_T rnext;
  boolean_T exitg3;
  boolean_T foundRow;
  boolean_T guard1 = false;
  n = matrixRep_size[0] - 1;
  if (0 <= *newPairWeightR_size - 1) {
    std::memset(&newPairWeightR_data[0], 0, *newPairWeightR_size * sizeof(real_T));
  }

  if (0 <= *predecessorsC_size - 1) {
    std::memset(&predecessorsC_data[0], 0, *predecessorsC_size * sizeof(int32_T));
  }

  predecessorsC_data[static_cast<int32_T>(colStart) - 1] = 0;
  loop_ub = *distancesR_size;
  for (citer = 0; citer < loop_ub; citer++) {
    distancesR_data[citer] = (rtInf);
  }

  if (0 <= *colorsR_size - 1) {
    std::memset(&colorsR_data[0], 0, *colorsR_size * sizeof(uint8_T));
  }

  rnext = -1;
  lsp = 0.0;
  lsap = (rtInf);
  rlast = 0;
  clast = 0;
  last_weight_sap = (rtInf);
  c = static_cast<int32_T>(colStart);
  guard1 = false;
  do {
    exitg2 = 0;
    loop_ub = 1;
    do {
      exitg1 = 0;
      if (loop_ub - 1 <= n) {
        citer = colorsR_data[loop_ub - 1];
        if (citer == 2) {
          loop_ub++;
        } else {
          lc = matrixRep_data[((c - 1) * matrixRep_size[0] + loop_ub) - 1];
          rc = ((lc - rowWeight_data[loop_ub - 1]) - colWeight_data[c - 1]) +
            lsp;
          if (rc < lsap) {
            b_queue_heapList_idx_1 = matchRtoC_data[loop_ub - 1];
            if (b_queue_heapList_idx_1 == 0) {
              lsap = rc;
              rlast = loop_ub;
              clast = c;
              last_weight_sap = lc;
            } else {
              if (rc < distancesR_data[loop_ub - 1]) {
                distancesR_data[loop_ub - 1] = rc;
                predecessorsC_data[b_queue_heapList_idx_1 - 1] = c;
                newPairWeightR_data[loop_ub - 1] = lc;
                if (citer == 0) {
                  rnext += 2;
                  queue_heapList_data[rnext - 1] = loop_ub;
                  queue_indexToHeap_data[loop_ub - 1] = rnext;
                  rnext = percUp_p(queue_heapList_data, queue_indexToHeap_data,
                                   rnext, distancesR_data, rnext) - 1;
                  colorsR_data[loop_ub - 1] = 1U;
                } else {
                  rnext = percUp_p(queue_heapList_data, queue_indexToHeap_data,
                                   rnext + 1, distancesR_data,
                                   queue_indexToHeap_data[loop_ub - 1]) - 1;
                }
              }
            }

            loop_ub++;
          } else if (((-1.7976931348623157E+308 <= rc) && (rc <=
                       1.7976931348623157E+308)) || (!(-1.7976931348623157E+308 <=
            lc)) || (!(lc <= 1.7976931348623157E+308))) {
            loop_ub++;
          } else {
            foundRow = false;
            exitg1 = 1;
          }
        }
      } else {
        exitg1 = 2;
      }
    } while (exitg1 == 0);

    if (exitg1 == 1) {
      exitg2 = 1;
    } else if (rnext + 1 == 0) {
      guard1 = true;
      exitg2 = 1;
    } else {
      if (0 <= *queue_heapList_size - 1) {
        std::memcpy(&b_queue_heapList_data[0], &queue_heapList_data[0],
                    *queue_heapList_size * sizeof(int32_T));
      }

      loop_ub = queue_heapList_data[0] - 1;
      b_queue_heapList_data[0] = queue_heapList_data[rnext];
      queue_indexToHeap_data[queue_heapList_data[rnext] - 1] =
        queue_indexToHeap_data[queue_heapList_data[0] - 1];
      b_queue_heapList_data[rnext] = 0;
      queue_indexToHeap_data[queue_heapList_data[0] - 1] = 0;
      lsp = 1.0;
      exitg3 = false;
      while ((!exitg3) && (2.0 * lsp <= rnext)) {
        lc = 2.0 * lsp;
        rc = 2.0 * lsp + 1.0;
        if (rc > rnext) {
          rc = lc;
        } else {
          citer = b_queue_heapList_data[static_cast<int32_T>(lc) - 1];
          b_queue_heapList_idx_1 = b_queue_heapList_data[static_cast<int32_T>(rc)
            - 1];
          tmp = distancesR_data[citer - 1];
          tmp_0 = distancesR_data[b_queue_heapList_idx_1 - 1];
          if ((tmp < tmp_0) || ((tmp == tmp_0) && (citer <=
                b_queue_heapList_idx_1))) {
            rc = lc;
          }
        }

        b_queue_heapList_idx_1 = b_queue_heapList_data[static_cast<int32_T>(lsp)
          - 1];
        citer = b_queue_heapList_data[static_cast<int32_T>(rc) - 1];
        tmp = distancesR_data[b_queue_heapList_idx_1 - 1];
        tmp_0 = distancesR_data[citer - 1];
        if ((tmp < tmp_0) || ((tmp == tmp_0) && (b_queue_heapList_idx_1 <= citer)))
        {
          exitg3 = true;
        } else {
          b_queue_heapList_data[static_cast<int32_T>(lsp) - 1] = citer;
          b_queue_heapList_data[static_cast<int32_T>(rc) - 1] =
            b_queue_heapList_idx_1;
          citer = queue_indexToHeap_data[b_queue_heapList_data
            [static_cast<int32_T>(lsp) - 1] - 1];
          queue_indexToHeap_data[b_queue_heapList_data[static_cast<int32_T>(lsp)
            - 1] - 1] = queue_indexToHeap_data[b_queue_heapList_data[
            static_cast<int32_T>(rc) - 1] - 1];
          queue_indexToHeap_data[b_queue_heapList_data[static_cast<int32_T>(rc)
            - 1] - 1] = citer;
          lsp = rc;
        }
      }

      if (0 <= *queue_heapList_size - 1) {
        std::memcpy(&queue_heapList_data[0], &b_queue_heapList_data[0],
                    *queue_heapList_size * sizeof(int32_T));
      }

      rnext--;
      lsp = distancesR_data[loop_ub];
      if (lsap <= distancesR_data[loop_ub]) {
        guard1 = true;
        exitg2 = 1;
      } else {
        colorsR_data[loop_ub] = 2U;
        c = matchRtoC_data[loop_ub];
        guard1 = false;
      }
    }
  } while (exitg2 == 0);

  if (guard1) {
    foundRow = (lsap < (rtInf));
    if (foundRow) {
      citer = clast - 1;
      do {
        exitg1 = 0;
        rnext = matchCtoR_data[citer];
        matchRtoC_data[rlast - 1] = citer + 1;
        matchCtoR_data[citer] = rlast;
        pairWeightR_data[rlast - 1] = last_weight_sap;
        if (predecessorsC_data[citer] == 0) {
          exitg1 = 1;
        } else {
          rlast = rnext;
          citer = predecessorsC_data[citer] - 1;
          last_weight_sap = newPairWeightR_data[rnext - 1];
        }
      } while (exitg1 == 0);

      for (citer = 0; citer <= n; citer++) {
        if (colorsR_data[citer] == 2) {
          rowWeight_data[citer] = (rowWeight_data[citer] - lsap) +
            distancesR_data[citer];
        }
      }

      for (citer = 0; citer <= n; citer++) {
        if (matchCtoR_data[citer] != 0) {
          colWeight_data[citer] = pairWeightR_data[matchCtoR_data[citer] - 1] -
            rowWeight_data[matchCtoR_data[citer] - 1];
        }
      }
    }
  }

  return foundRow;
}

void PathFollowingControllerRefMdlModelClass::matlabPerfectMatching(const real_T
  matrixRep_data[], const int32_T matrixRep_size[2], int32_T matchCtoR_data[],
  int32_T *matchCtoR_size, int32_T matchRtoC_data[], int32_T *matchRtoC_size,
  real_T rowWeight_data[], int32_T *rowWeight_size, real_T colWeight_data[],
  int32_T *colWeight_size, boolean_T *success)
{
  real_T distancesR_data[170];
  real_T newPairWeightR_data[170];
  real_T pairWeightR_data[170];
  real_T edge_weight_shifted;
  real_T x;
  int32_T predecessorsC_data[170];
  int32_T queue_heapList_data[170];
  int32_T queue_indexToHeap_data[170];
  int32_T b_k;
  int32_T colorsR_size;
  int32_T distancesR_size;
  int32_T exitg1;
  int32_T n;
  int32_T newPairWeightR_data_0;
  int32_T newPairWeightR_size;
  int32_T nx;
  int32_T predecessorsC_size;
  uint8_T colorsR_data[170];
  boolean_T p;
  n = matrixRep_size[0];
  *matchCtoR_size = matrixRep_size[0];
  nx = matrixRep_size[0];
  if (0 <= nx - 1) {
    std::memset(&matchCtoR_data[0], 0, nx * sizeof(int32_T));
  }

  *matchRtoC_size = matrixRep_size[0];
  nx = matrixRep_size[0];
  if (0 <= nx - 1) {
    std::memset(&matchRtoC_data[0], 0, nx * sizeof(int32_T));
  }

  *colWeight_size = matrixRep_size[0];
  nx = matrixRep_size[0];
  for (b_k = 0; b_k < nx; b_k++) {
    colWeight_data[b_k] = (rtInf);
  }

  if (matrixRep_size[0] == 0) {
    *rowWeight_size = 0;
    *success = true;
  } else {
    minimum_l(matrixRep_data, matrixRep_size, rowWeight_data, rowWeight_size,
              predecessorsC_data, &predecessorsC_size);
    for (b_k = 0; b_k < predecessorsC_size; b_k++) {
      newPairWeightR_data[b_k] = predecessorsC_data[b_k];
    }

    nx = *rowWeight_size - 1;
    p = true;
    for (b_k = 0; b_k <= nx; b_k++) {
      if (p) {
        x = rowWeight_data[b_k];
        if ((!rtIsInf(x)) && (!rtIsNaN(x))) {
        } else {
          p = false;
        }
      } else {
        p = false;
      }
    }

    if (!p) {
      *success = false;
    } else {
      nx = matrixRep_size[0];
      if (0 <= nx - 1) {
        std::memset(&pairWeightR_data[0], 0, nx * sizeof(real_T));
      }

      nx = matrixRep_size[0] - 1;
      for (b_k = 0; b_k <= nx; b_k++) {
        newPairWeightR_data_0 = static_cast<int32_T>(newPairWeightR_data[b_k]);
        if (matchCtoR_data[newPairWeightR_data_0 - 1] == 0) {
          matchRtoC_data[b_k] = newPairWeightR_data_0;
          matchCtoR_data[newPairWeightR_data_0 - 1] = b_k + 1;
          pairWeightR_data[b_k] = rowWeight_data[b_k];
        }
      }

      newPairWeightR_data_0 = matrixRep_size[0] - 1;
      for (b_k = 0; b_k <= newPairWeightR_data_0; b_k++) {
        predecessorsC_size = n - 1;
        for (nx = 0; nx <= predecessorsC_size; nx++) {
          x = colWeight_data[b_k];
          edge_weight_shifted = matrixRep_data[matrixRep_size[0] * b_k + nx] -
            rowWeight_data[nx];
          if (edge_weight_shifted < x) {
            x = edge_weight_shifted;
          }

          colWeight_data[b_k] = x;
        }
      }

      newPairWeightR_size = matrixRep_size[0];
      nx = matrixRep_size[0];
      if (0 <= nx - 1) {
        std::memset(&newPairWeightR_data[0], 0, nx * sizeof(real_T));
      }

      predecessorsC_size = matrixRep_size[0];
      nx = matrixRep_size[0];
      if (0 <= nx - 1) {
        std::memset(&predecessorsC_data[0], 0, nx * sizeof(int32_T));
      }

      distancesR_size = matrixRep_size[0];
      nx = matrixRep_size[0];
      for (b_k = 0; b_k < nx; b_k++) {
        distancesR_data[b_k] = (rtInf);
      }

      colorsR_size = matrixRep_size[0];
      nx = matrixRep_size[0];
      if (0 <= nx - 1) {
        std::memset(&colorsR_data[0], 0, nx * sizeof(uint8_T));
      }

      b_k = matrixRep_size[0];
      nx = matrixRep_size[0];
      newPairWeightR_data_0 = 0;
      do {
        exitg1 = 0;
        if (newPairWeightR_data_0 <= n - 1) {
          if (matchCtoR_data[newPairWeightR_data_0] != 0) {
            newPairWeightR_data_0++;
          } else {
            if (0 <= b_k - 1) {
              std::memset(&queue_heapList_data[0], 0, b_k * sizeof(int32_T));
            }

            if (0 <= nx - 1) {
              std::memset(&queue_indexToHeap_data[0], 0, nx * sizeof(int32_T));
            }

            p = augmentedShortestPath_k(matrixRep_data, matrixRep_size,
              static_cast<real_T>(newPairWeightR_data_0) + 1.0, rowWeight_data,
              colWeight_data, matchCtoR_data, matchRtoC_data, pairWeightR_data,
              newPairWeightR_data, &newPairWeightR_size, predecessorsC_data,
              &predecessorsC_size, distancesR_data, &distancesR_size,
              colorsR_data, &colorsR_size, queue_heapList_data, &b_k,
              queue_indexToHeap_data);
            if (!p) {
              *success = false;
              exitg1 = 1;
            } else {
              newPairWeightR_data_0++;
            }
          }
        } else {
          *success = true;
          exitg1 = 1;
        }
      } while (exitg1 == 0);
    }
  }
}

void PathFollowingControllerRefMdlModelClass::perfectMatching(const real_T
  A_data[], const int32_T A_size[2], int32_T m1_data[], int32_T *m1_size,
  int32_T m2_data[], int32_T *m2_size)
{
  real_T unusedU0_data[170];
  real_T unusedU1_data[170];
  int32_T unusedU0_size;
  int32_T unusedU1_size;
  boolean_T success;
  matlabPerfectMatching(A_data, A_size, m1_data, m1_size, m2_data, m2_size,
                        unusedU0_data, &unusedU0_size, unusedU1_data,
                        &unusedU1_size, &success);
  if (!success) {
    *m1_size = 0;
    *m2_size = 0;
  }
}

void PathFollowingControllerRefMdlModelClass::eml_find(const boolean_T x_data[],
  const int32_T *x_size, int32_T i_data[], int32_T *i_size)
{
  int32_T b_ii;
  int32_T idx;
  boolean_T exitg1;
  idx = 0;
  *i_size = *x_size;
  b_ii = 1;
  exitg1 = false;
  while ((!exitg1) && (b_ii - 1 <= *x_size - 1)) {
    if (x_data[b_ii - 1]) {
      idx++;
      i_data[idx - 1] = b_ii;
      if (idx >= *x_size) {
        exitg1 = true;
      } else {
        b_ii++;
      }
    } else {
      b_ii++;
    }
  }

  if (*x_size == 1) {
    if (idx == 0) {
      *i_size = 0;
    }
  } else {
    if (1 > idx) {
      idx = 0;
    }

    *i_size = idx;
  }
}

void PathFollowingControllerRefMdlModelClass::eml_matchpairs(const real_T
  Cost_data[], const int32_T Cost_size[2], int32_T matchings_data[], int32_T
  matchings_size[2], int32_T unassignedRows_data[], int32_T *unassignedRows_size,
  int32_T unassignedCols_data[], int32_T *unassignedCols_size,
  B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  real_T absNewVal;
  real_T maxVal;
  int32_T colToRow_data[170];
  int32_T rowToCol_data[170];
  int32_T tmp_data[100];
  int32_T paddedCost_size[2];
  int32_T paddedCost_size_0[2];
  int32_T b_ii;
  int32_T b_jj;
  int32_T loop_ub_tmp;
  int32_T loop_ub_tmp_tmp;
  int32_T m;
  int32_T n;
  int32_T nr;
  int32_T r;
  boolean_T rowToCol_data_0[100];
  boolean_T colToRow_data_0[70];
  m = Cost_size[0] - 1;
  n = Cost_size[1] - 1;
  nr = Cost_size[0] + Cost_size[1];
  paddedCost_size[0] = nr;
  paddedCost_size[1] = nr;
  loop_ub_tmp_tmp = nr * nr;
  loop_ub_tmp = loop_ub_tmp_tmp - 1;
  for (b_jj = 0; b_jj <= loop_ub_tmp; b_jj++) {
    localB->paddedCost_data[b_jj] = (rtInf);
  }

  for (b_jj = 0; b_jj <= n; b_jj++) {
    for (b_ii = 0; b_ii <= m; b_ii++) {
      localB->paddedCost_data[b_ii + nr * b_jj] = Cost_data[Cost_size[0] * b_jj
        + b_ii];
    }
  }

  for (b_jj = 0; b_jj <= m; b_jj++) {
    for (b_ii = 0; b_ii <= n; b_ii++) {
      localB->paddedCost_data[((m + b_ii) + nr * ((n + b_jj) + 1)) + 1] =
        Cost_data[Cost_size[0] * b_ii + b_jj];
    }
  }

  for (b_jj = 0; b_jj <= m; b_jj++) {
    localB->paddedCost_data[b_jj + nr * ((n + b_jj) + 1)] = 20.0;
  }

  for (b_jj = 0; b_jj <= n; b_jj++) {
    localB->paddedCost_data[((m + b_jj) + nr * b_jj) + 1] = 20.0;
  }

  perfectMatching(localB->paddedCost_data, paddedCost_size, colToRow_data, &b_ii,
                  rowToCol_data, &b_jj);
  if ((b_ii == 0) && (nr > 0)) {
    maxVal = 0.0;
    for (b_jj = 0; b_jj <= loop_ub_tmp; b_jj++) {
      absNewVal = std::abs(localB->paddedCost_data[b_jj]);
      if ((absNewVal > maxVal) && (!rtIsInf(absNewVal))) {
        maxVal = absNewVal;
      }
    }

    if (!rtIsInf(maxVal)) {
      absNewVal = frexp(maxVal, &r);
      maxVal = r;
      if (absNewVal == 0.5) {
        maxVal = static_cast<real_T>(r) - 1.0;
      }
    }

    maxVal = rt_powd_snf(2.0, -maxVal);
    paddedCost_size_0[0] = nr;
    paddedCost_size_0[1] = nr;
    for (b_jj = 0; b_jj < loop_ub_tmp_tmp; b_jj++) {
      localB->paddedCost_data_m[b_jj] = localB->paddedCost_data[b_jj] * maxVal;
    }

    perfectMatching(localB->paddedCost_data_m, paddedCost_size_0, colToRow_data,
                    &b_ii, rowToCol_data, &b_jj);
  }

  nr = 0;
  for (loop_ub_tmp = 0; loop_ub_tmp <= n; loop_ub_tmp++) {
    loop_ub_tmp_tmp = colToRow_data[loop_ub_tmp];
    r = loop_ub_tmp_tmp - 1;
    if ((loop_ub_tmp_tmp <= m + 1) && (Cost_data[(Cost_size[0] * loop_ub_tmp +
          loop_ub_tmp_tmp) - 1] == 20.0)) {
      loop_ub_tmp_tmp = (m + n) + 3;
      rowToCol_data[r] = (m + n) + 3;
    }

    if (loop_ub_tmp_tmp <= m + 1) {
      nr++;
    }

    colToRow_data[loop_ub_tmp] = loop_ub_tmp_tmp;
  }

  matchings_size[0] = nr;
  matchings_size[1] = 2;
  r = -1;
  for (loop_ub_tmp = 0; loop_ub_tmp <= n; loop_ub_tmp++) {
    loop_ub_tmp_tmp = colToRow_data[loop_ub_tmp];
    if (loop_ub_tmp_tmp <= m + 1) {
      r++;
      matchings_data[r] = loop_ub_tmp_tmp;
      matchings_data[r + nr] = loop_ub_tmp + 1;
    }
  }

  m = Cost_size[1];
  if (1 > Cost_size[0]) {
    n = 0;
  } else {
    n = Cost_size[0];
  }

  b_jj = n;
  for (b_jj = 0; b_jj < n; b_jj++) {
    rowToCol_data_0[b_jj] = (rowToCol_data[b_jj] > m);
  }

  eml_find(rowToCol_data_0, &n, unassignedRows_data, unassignedRows_size);
  m = Cost_size[0];
  if (1 > Cost_size[1]) {
    n = 0;
  } else {
    n = Cost_size[1];
  }

  for (b_jj = 0; b_jj < n; b_jj++) {
    colToRow_data_0[b_jj] = (colToRow_data[b_jj] > m);
  }

  eml_find(colToRow_data_0, &n, tmp_data, &b_jj);
  *unassignedCols_size = b_jj;
  if (0 <= b_jj - 1) {
    std::memcpy(&unassignedCols_data[0], &tmp_data[0], b_jj * sizeof(int32_T));
  }
}

void PathFollowingControllerRefMdlModelClass::SystemCore_step_bk
  (c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T *obj, const
   real_T varargin_1_data[], const int32_T varargin_1_size[2], uint32_T
   varargout_1_data[], int32_T varargout_1_size[2],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  int32_T b_matchings_data[140];
  int32_T b_unassignedRows_data[100];
  int32_T b_unassignedCols_data[70];
  int32_T b_matchings_size[2];
  int32_T b_unassignedCols_size;
  int32_T b_unassignedRows_size;
  int32_T tmp;
  if (obj->isInitialized != 1) {
    obj->isSetupComplete = false;
    obj->isInitialized = 1;
    obj->isSetupComplete = true;
  }

  eml_matchpairs(varargin_1_data, varargin_1_size, b_matchings_data,
                 b_matchings_size, b_unassignedRows_data, &b_unassignedRows_size,
                 b_unassignedCols_data, &b_unassignedCols_size, localB);
  varargout_1_size[0] = b_matchings_size[0];
  varargout_1_size[1] = 2;
  b_unassignedCols_size = b_matchings_size[0] * b_matchings_size[1] - 1;
  for (b_unassignedRows_size = 0; b_unassignedRows_size <= b_unassignedCols_size;
       b_unassignedRows_size++) {
    tmp = b_matchings_data[b_unassignedRows_size];
    if (tmp < 0) {
      tmp = 0;
    }

    varargout_1_data[b_unassignedRows_size] = static_cast<uint32_T>(tmp);
  }
}

void PathFollowingControllerRefMdlModelClass::
  GNNTracker_associateDetectionsToTracks(multiObjectTracker_LFRefMdl_T *obj,
  uint32_T OverallAssignments_data[], int32_T OverallAssignments_size[2],
  uint32_T outOverallUnassignedTracks_data[], int32_T
  *outOverallUnassignedTracks_size, uint32_T
  outOverallUnassignedDetections_data[], int32_T
  *outOverallUnassignedDetections_size, real_T overallCostMatrix_data[], int32_T
  overallCostMatrix_size[2], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_0;
  real_T SensorDetections_data[70];
  real_T origSen[70];
  real_T sensorIDs_data[70];
  real_T lastAssigned;
  real_T sensorIDs;
  int32_T ii_data[70];
  int32_T ii_data_0[70];
  int32_T xb_data[70];
  int32_T SensorDetections_size[2];
  int32_T costMatrix_size[2];
  int32_T sensorIDs_size[2];
  int32_T b_end;
  int32_T b_partialTrueCount;
  int32_T c;
  int32_T d_i;
  int32_T i;
  int32_T idx;
  int32_T overallCostMatrix_data_tmp;
  int32_T p;
  uint32_T b_OverallAssignments[400];
  uint32_T assignments_data[140];
  int8_T OverallUnassignedTracks[100];
  int8_T OverallUnassignedDetections[70];
  uint8_T vb_data[200];
  uint8_T vb_data_0;
  boolean_T isTrackAssociated[100];
  boolean_T isDetectionAssociated[70];
  boolean_T x[70];
  boolean_T exitg1;
  std::memset(&b_OverallAssignments[0], 0, 400U * sizeof(uint32_T));
  for (i = 0; i < 70; i++) {
    OverallUnassignedDetections[i] = static_cast<int8_T>(i + 1);
  }

  for (i = 0; i < 100; i++) {
    OverallUnassignedTracks[i] = static_cast<int8_T>(i + 1);
  }

  lastAssigned = obj->pNumLiveTracks;
  if (1.0 > lastAssigned) {
    c = -1;
  } else {
    c = static_cast<int32_T>(lastAssigned) - 1;
  }

  overallCostMatrix_size[0] = c + 1;
  overallCostMatrix_size[1] = 70;
  for (i = 0; i < 70; i++) {
    for (b_partialTrueCount = 0; b_partialTrueCount <= c; b_partialTrueCount++)
    {
      overallCostMatrix_data[b_partialTrueCount + (c + 1) * i] = 0.0;
    }
  }

  for (i = 0; i < 100; i++) {
    isTrackAssociated[i] = true;
  }

  lastAssigned = obj->pNumLiveTracks;
  if (1.0 > lastAssigned) {
    i = 0;
  } else {
    i = static_cast<int32_T>(lastAssigned);
  }

  if (0 <= i - 1) {
    std::memset(&isTrackAssociated[0], 0, i * sizeof(boolean_T));
  }

  std::memset(&isDetectionAssociated[0], 0, 70U * sizeof(boolean_T));
  if (obj->pNumDetections == 0.0) {
    OverallAssignments_size[0] = 0;
    OverallAssignments_size[1] = 2;
    i = 0;
    d_i = 0;
    for (c = 0; c < 100; c++) {
      if (!isTrackAssociated[c]) {
        i++;
        outOverallUnassignedTracks_data[d_i] = static_cast<uint32_T>
          (OverallUnassignedTracks[c]);
        d_i++;
      }
    }

    *outOverallUnassignedTracks_size = i;
    lastAssigned = obj->pNumDetections;
    if (1.0 > lastAssigned) {
      i = -1;
    } else {
      i = static_cast<int32_T>(lastAssigned) - 1;
    }

    d_i = 0;
    b_end = 0;
    for (c = 0; c <= i; c++) {
      d_i++;
      xb_data[b_end] = c + 1;
      b_end++;
    }

    *outOverallUnassignedDetections_size = d_i;
    for (i = 0; i < d_i; i++) {
      outOverallUnassignedDetections_data[i] = static_cast<uint32_T>
        (OverallUnassignedDetections[xb_data[i] - 1]);
    }
  } else {
    obj_0 = obj->cDetectionManager;
    for (i = 0; i < 70; i++) {
      origSen[i] = obj_0->pOriginatingSensor[i];
    }

    unique_vector(origSen, sensorIDs_data, sensorIDs_size);
    b_end = sensorIDs_size[1] - 1;
    i = 0;
    for (d_i = 0; d_i <= b_end; d_i++) {
      if (sensorIDs_data[d_i] > 0.0) {
        i++;
      }
    }

    b_partialTrueCount = 0;
    for (d_i = 0; d_i <= b_end; d_i++) {
      if (sensorIDs_data[d_i] > 0.0) {
        sensorIDs_data[b_partialTrueCount] = sensorIDs_data[d_i];
        b_partialTrueCount++;
      }
    }

    lastAssigned = 0.0;
    b_end = i - 1;
    if (0 <= i - 1) {
      SensorDetections_size[0] = 1;
    }

    for (d_i = 0; d_i <= b_end; d_i++) {
      sensorIDs = sensorIDs_data[d_i];
      for (i = 0; i < 70; i++) {
        x[i] = (origSen[i] == sensorIDs);
      }

      idx = 0;
      i = 1;
      exitg1 = false;
      while ((!exitg1) && (i - 1 < 70)) {
        if (x[i - 1]) {
          idx++;
          ii_data[idx - 1] = i;
          if (idx >= 70) {
            exitg1 = true;
          } else {
            i++;
          }
        } else {
          i++;
        }
      }

      if (1 > idx) {
        idx = 0;
      }

      if (0 <= idx - 1) {
        std::memcpy(&ii_data_0[0], &ii_data[0], idx * sizeof(int32_T));
      }

      if (0 <= idx - 1) {
        std::memcpy(&ii_data[0], &ii_data_0[0], idx * sizeof(int32_T));
      }

      SensorDetections_size[1] = idx;
      b_partialTrueCount = idx - 1;
      for (i = 0; i <= b_partialTrueCount; i++) {
        SensorDetections_data[i] = ii_data[i];
      }

      if (idx != 0) {
        GNNTracker_calcCostMatrix(obj, SensorDetections_data,
          SensorDetections_size, localB->costMatrix, localB);
        sensorIDs = obj->pNumLiveTracks;
        if (1.0 > sensorIDs) {
          p = -1;
        } else {
          p = static_cast<int32_T>(sensorIDs) - 1;
        }

        for (i = 0; i < idx; i++) {
          for (b_partialTrueCount = 0; b_partialTrueCount <= p;
               b_partialTrueCount++) {
            overallCostMatrix_data_tmp = static_cast<int32_T>
              (SensorDetections_data[i]) - 1;
            overallCostMatrix_data[b_partialTrueCount + (c + 1) *
              overallCostMatrix_data_tmp] = localB->
              costMatrix[overallCostMatrix_data_tmp * 100 + b_partialTrueCount];
          }
        }

        sensorIDs = obj->pNumLiveTracks;
        if (1.0 > sensorIDs) {
          p = -1;
        } else {
          p = static_cast<int32_T>(sensorIDs) - 1;
        }

        costMatrix_size[0] = p + 1;
        costMatrix_size[1] = idx;
        for (i = 0; i < idx; i++) {
          for (b_partialTrueCount = 0; b_partialTrueCount <= p;
               b_partialTrueCount++) {
            localB->costMatrix_data_c[b_partialTrueCount + (p + 1) * i] =
              localB->costMatrix[(static_cast<int32_T>(SensorDetections_data[i])
                                  - 1) * 100 + b_partialTrueCount];
          }
        }

        SystemCore_step_bk(obj->cAssigner, localB->costMatrix_data_c,
                           costMatrix_size, assignments_data, sensorIDs_size,
                           localB);
        if (sensorIDs_size[0] != 0) {
          b_partialTrueCount = sensorIDs_size[0];
          for (i = 0; i < b_partialTrueCount; i++) {
            b_OverallAssignments[static_cast<int32_T>(lastAssigned +
              static_cast<real_T>(i + 1)) - 1] = assignments_data[i];
          }

          idx = static_cast<int32_T>(static_cast<real_T>(sensorIDs_size[0]) -
            1.0) + 1;
          b_partialTrueCount = static_cast<int32_T>(static_cast<real_T>
            (sensorIDs_size[0]) - 1.0);
          for (i = 0; i <= b_partialTrueCount; i++) {
            ii_data[i] = static_cast<int32_T>((static_cast<real_T>(i) + 1.0) +
              lastAssigned) - 1;
          }

          for (i = 0; i < idx; i++) {
            b_partialTrueCount = static_cast<int32_T>(SensorDetections_data[
              static_cast<int32_T>(assignments_data[i + sensorIDs_size[0]]) - 1]);
            if (b_partialTrueCount >= 0) {
              b_OverallAssignments[ii_data[i] + 200] = static_cast<uint32_T>
                (b_partialTrueCount);
            } else {
              b_OverallAssignments[ii_data[i] + 200] = 0U;
            }
          }

          lastAssigned += static_cast<real_T>(sensorIDs_size[0]);
          b_partialTrueCount = sensorIDs_size[0];
          for (i = 0; i < b_partialTrueCount; i++) {
            ii_data[i] = static_cast<int32_T>(assignments_data[i]);
          }

          for (i = 0; i < b_partialTrueCount; i++) {
            isTrackAssociated[ii_data[i] - 1] = true;
          }
        }

        b_partialTrueCount = sensorIDs_size[0];
        for (i = 0; i < b_partialTrueCount; i++) {
          ii_data[i] = static_cast<int32_T>(SensorDetections_data
            [static_cast<int32_T>(assignments_data[i + sensorIDs_size[0]]) - 1]);
        }

        for (i = 0; i < b_partialTrueCount; i++) {
          isDetectionAssociated[ii_data[i] - 1] = true;
        }
      }
    }

    d_i = 0;
    i = 0;
    for (c = 0; c < 200; c++) {
      if (b_OverallAssignments[c] > 0U) {
        d_i++;
        vb_data[i] = static_cast<uint8_T>(c + 1);
        i++;
      }
    }

    OverallAssignments_size[0] = d_i;
    OverallAssignments_size[1] = 2;
    for (i = 0; i < d_i; i++) {
      vb_data_0 = vb_data[i];
      OverallAssignments_data[i] = b_OverallAssignments[vb_data_0 - 1];
      OverallAssignments_data[i + d_i] = b_OverallAssignments[vb_data_0 + 199];
    }

    i = 0;
    d_i = 0;
    for (c = 0; c < 100; c++) {
      if (!isTrackAssociated[c]) {
        i++;
        outOverallUnassignedTracks_data[d_i] = static_cast<uint32_T>
          (OverallUnassignedTracks[c]);
        d_i++;
      }
    }

    *outOverallUnassignedTracks_size = i;
    lastAssigned = obj->pNumDetections;
    if (1.0 > lastAssigned) {
      d_i = -1;
    } else {
      d_i = static_cast<int32_T>(lastAssigned) - 1;
    }

    for (i = 0; i <= d_i; i++) {
      x[i] = !isDetectionAssociated[i];
    }

    b_end = 0;
    i = 0;
    for (c = 0; c <= d_i; c++) {
      if (x[c]) {
        b_end++;
        xb_data[i] = c + 1;
        i++;
      }
    }

    *outOverallUnassignedDetections_size = b_end;
    for (i = 0; i < b_end; i++) {
      outOverallUnassignedDetections_data[i] = static_cast<uint32_T>
        (OverallUnassignedDetections[xb_data[i] - 1]);
    }
  }
}

void PathFollowingControllerRefMdlModelClass::TrackManager_trackIDs(const
  multiObjectTracker_LFRefMdl_T *obj, const uint32_T indices_data[], const
  int32_T *indices_size, uint32_T trackIDList_data[], int32_T *trackIDList_size)
{
  int32_T i;
  int32_T loop_ub;
  *trackIDList_size = *indices_size;
  loop_ub = *indices_size;
  for (i = 0; i < loop_ub; i++) {
    trackIDList_data[i] = obj->pTrackIDs[static_cast<int32_T>(indices_data[i]) -
      1];
  }
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_associate
  (multiObjectTracker_LFRefMdl_T *obj, uint32_T assigned_data[], int32_T
   assigned_size[2], uint32_T unassignedTrs_data[], int32_T *unassignedTrs_size,
   uint32_T unassignedDets_data[], int32_T *unassignedDets_size, real_T
   costMatrix_data[], int32_T costMatrix_size[2],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  emxArray_uint32_T_200_LFRefMdl_T obj_0;
  int32_T b_assigned_size[2];
  int32_T b_assigned_size_0;
  int32_T loop_ub;
  uint32_T b_assigned_data[400];
  uint32_T b_assigned_data_0[200];
  GNNTracker_associateDetectionsToTracks(obj, b_assigned_data, b_assigned_size,
    unassignedTrs_data, unassignedTrs_size, unassignedDets_data,
    unassignedDets_size, costMatrix_data, costMatrix_size, localB);
  assigned_size[0] = b_assigned_size[0];
  assigned_size[1] = 2;
  loop_ub = b_assigned_size[0] * b_assigned_size[1] - 1;
  if (0 <= loop_ub) {
    std::memcpy(&assigned_data[0], &b_assigned_data[0], (loop_ub + 1) * sizeof
                (uint32_T));
  }

  loop_ub = b_assigned_size[0];
  b_assigned_size_0 = b_assigned_size[0];
  if (0 <= loop_ub - 1) {
    std::memcpy(&b_assigned_data_0[0], &b_assigned_data[0], loop_ub * sizeof
                (uint32_T));
  }

  TrackManager_trackIDs(obj, b_assigned_data_0, &b_assigned_size_0, obj_0.data,
                        &obj_0.size);
  TrackManager_trackIDs(obj, unassignedTrs_data, unassignedTrs_size, obj_0.data,
                        &obj_0.size);
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_trackDetectability
  (multiObjectTracker_LFRefMdl_T *obj)
{
  int32_T c;
  int32_T i;
  if (1.0 > obj->pNumLiveTracks) {
    c = 0;
  } else {
    c = static_cast<int32_T>(obj->pNumLiveTracks);
  }

  for (i = 0; i < c; i++) {
    obj->pWasDetectable[i] = true;
  }
}

boolean_T PathFollowingControllerRefMdlModelClass::TrackManager_initiateTrack
  (multiObjectTracker_LFRefMdl_T *obj, uint32_T newTrackID, real_T Det_Time,
   const real_T Det_Measurement[6], const real_T Det_MeasurementNoise[36],
   real_T Det_SensorIndex, real_T Det_ObjectClassID, drivingCoordinateFrameType
   Det_MeasurementParameters_Frame, const real_T
   Det_MeasurementParameters_OriginPosition[3], const real_T
   Det_MeasurementParameters_Orientation[9], boolean_T
   Det_MeasurementParameters_HasVelocity, boolean_T
   Det_MeasurementParameters_HasElevation, const
   BusRadarDetectionsObjectAttributes Det_ObjectAttributes)
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track;
  c_trackHistoryLogic_LFRefMdl_T *obj_0;
  c_trackingEKF_LFRefMdl_T lobj_0[2];
  c_trackingEKF_LFRefMdl_T *filter;
  real_T newTrackIndex;
  int32_T i;
  boolean_T tmp[50];
  boolean_T tf;
  boolean_T x_idx_0;
  boolean_T x_idx_1;
  boolean_T x_idx_2;
  newTrackIndex = obj->pNumLiveTracks + 1.0;
  if (newTrackIndex <= 100.0) {
    obj->pTrackIDs[static_cast<int32_T>(newTrackIndex) - 1] = newTrackID;
    obj->pNumLiveTracks++;
    track = obj->pTracksList[static_cast<int32_T>(newTrackIndex) - 1];
    track->TrackID = newTrackID;
    track->pObjectAttributes[static_cast<int32_T>(Det_SensorIndex) - 1] =
      Det_ObjectAttributes;
    track->pUsedObjectAttributes[static_cast<int32_T>(Det_SensorIndex) - 1] =
      true;
    track->Time = Det_Time;
    track->ObjectClassID = Det_ObjectClassID;
    track->pAge = 1U;
    obj_0 = track->TrackLogic;
    for (i = 0; i < 50; i++) {
      obj_0->pRecentHistory[i] = false;
    }

    obj_0->pIsFirstUpdate = true;
    obj_0 = track->TrackLogic;
    tmp[0] = true;
    for (i = 0; i < 49; i++) {
      tmp[i + 1] = obj_0->pRecentHistory[i];
    }

    for (i = 0; i < 50; i++) {
      obj_0->pRecentHistory[i] = tmp[i];
    }

    obj_0->pIsFirstUpdate = false;
    if (track->ObjectClassID != 0.0) {
      x_idx_0 = true;
    } else {
      obj_0 = track->TrackLogic;
      if (obj_0->pIsFirstUpdate) {
        x_idx_0 = false;
      } else {
        x_idx_0 = obj_0->pRecentHistory[0];
        x_idx_1 = obj_0->pRecentHistory[1];
        x_idx_2 = obj_0->pRecentHistory[2];
        x_idx_0 = ((x_idx_0 + x_idx_1) + x_idx_2 >= 2);
      }
    }

    track->IsConfirmed = x_idx_0;
    track->pIsCoasted = false;
    obj->pConfirmedTracks[static_cast<int32_T>(newTrackIndex) - 1] =
      track->IsConfirmed;
    filter = initcvekf(Det_Measurement, Det_MeasurementNoise,
                       Det_MeasurementParameters_Frame,
                       Det_MeasurementParameters_OriginPosition,
                       Det_MeasurementParameters_Orientation,
                       Det_MeasurementParameters_HasVelocity,
                       Det_MeasurementParameters_HasElevation, &lobj_0[0]);
    ExtendedKalmanFilter_set_MeasurementNoise(filter, Det_MeasurementNoise);
    track = obj->pTracksList[static_cast<int32_T>(newTrackIndex) - 1];
    trackingEKF_sync(track->Filter, filter);
    trackingEKF_sync(track->pDistanceFilter, filter);
    tf = true;
  } else {
    tf = false;
  }

  return tf;
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_distance_k
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track, const
   BusDetectionConcatenation1Detections detections[70], const uint32_T
   sensorDetections_data[], const int32_T *sensorDetections_size, real_T
   cost_data[], int32_T cost_size[2])
{
  real_T tmp_data[400];
  real_T detTimes_data[140];
  real_T costValue_data[6];
  real_T detTimes_data_0;
  real_T trackTime;
  int32_T detTimes_size[2];
  int32_T tmp_size[2];
  int32_T b;
  int32_T b_i;
  int32_T loop_ub;
  boolean_T guard1 = false;
  if (*sensorDetections_size == 0) {
    cost_size[0] = 1;
    cost_size[1] = 0;
  } else {
    detTimes_size[0] = *sensorDetections_size;
    detTimes_size[1] = 2;
    loop_ub = (*sensorDetections_size << 1) - 1;
    if (0 <= loop_ub) {
      std::memset(&detTimes_data[0], 0, (loop_ub + 1) * sizeof(real_T));
    }

    loop_ub = *sensorDetections_size - 1;
    for (b_i = 0; b_i <= loop_ub; b_i++) {
      detTimes_data[b_i] = static_cast<real_T>(b_i) + 1.0;
      detTimes_data[b_i + detTimes_size[0]] = detections[static_cast<int32_T>
        (sensorDetections_data[b_i]) - 1].Time;
    }

    sortrows(detTimes_data, detTimes_size, tmp_data, tmp_size);
    loop_ub = tmp_size[0] * tmp_size[1];
    if (0 <= loop_ub - 1) {
      std::memcpy(&detTimes_data[0], &tmp_data[0], loop_ub * sizeof(real_T));
    }

    cost_size[0] = 1;
    cost_size[1] = tmp_size[0];
    loop_ub = tmp_size[0] - 1;
    for (b_i = 0; b_i <= loop_ub; b_i++) {
      cost_data[b_i] = (rtInf);
    }

    trackTime = track->Time;
    trackingEKF_sync(track->pDistanceFilter, track->Filter);
    b = tmp_size[0] - 1;
    for (loop_ub = 0; loop_ub <= b; loop_ub++) {
      detTimes_data_0 = detTimes_data[loop_ub];
      ExtendedKalmanFilter_set_MeasurementNoise(track->pDistanceFilter,
        detections[static_cast<int32_T>(sensorDetections_data
        [static_cast<int32_T>(detTimes_data_0) - 1]) - 1].MeasurementNoise);
      guard1 = false;
      if (track->ObjectClassID == 0.0) {
        guard1 = true;
      } else {
        b_i = static_cast<int32_T>(sensorDetections_data[static_cast<int32_T>
          (detTimes_data_0) - 1]) - 1;
        if ((detections[b_i].ObjectClassID == 0.0) || (detections[b_i].
             ObjectClassID == track->ObjectClassID)) {
          guard1 = true;
        }
      }

      if (guard1) {
        b_i = static_cast<int32_T>(sensorDetections_data[static_cast<int32_T>
          (detTimes_data_0) - 1]) - 1;
        trackTime = detections[b_i].Time - trackTime;
        if (trackTime > 0.0) {
          predictTrackFilter(track->pDistanceFilter, trackTime);
        }

        trackTime = detections[b_i].Time;
        ObjectTrack_calcCostOneDetection(track, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .Measurement, detections[static_cast<int32_T>(sensorDetections_data[
          static_cast<int32_T>(detTimes_data_0) - 1]) - 1].
          MeasurementParameters.Frame, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.OriginPosition, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.Orientation, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.HasVelocity, detections[static_cast<int32_T>
          (sensorDetections_data[static_cast<int32_T>(detTimes_data_0) - 1]) - 1]
          .MeasurementParameters.HasElevation, costValue_data, detTimes_size);
        cost_data[loop_ub] = costValue_data[0];
      }
    }
  }
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj1qbx(int32_T n, const
  real_T x_data[], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  kend = ix0 + n;
  for (k = ix0; k < kend; k++) {
    absxk = std::abs(x_data[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

void PathFollowingControllerRefMdlModelClass::xgeqrf_i(const real_T A_data[],
  real_T b_A_data[], int32_T b_A_size[2], real_T tau_data[], int32_T *tau_size)
{
  real_T c_A_data[72];
  real_T work[6];
  real_T b_atmp;
  real_T xnorm;
  int32_T b;
  int32_T b_i;
  int32_T b_tmp;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T ii;
  int32_T jA;
  int32_T jy;
  int32_T knt;
  int32_T lastv;
  boolean_T exitg2;
  *tau_size = 6;
  for (b_i = 0; b_i < 6; b_i++) {
    tau_data[b_i] = 0.0;
  }

  std::memcpy(&c_A_data[0], &A_data[0], 72U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    work[i] = 0.0;
  }

  for (b_i = 0; b_i < 6; b_i++) {
    ii = b_i * 12 + b_i;
    i = ii + 2;
    b_atmp = c_A_data[ii];
    tau_data[b_i] = 0.0;
    xnorm = xnrm2_n1aj1qbx(11 - b_i, c_A_data, ii + 2);
    if (xnorm != 0.0) {
      xnorm = rt_hypotd_snf(c_A_data[ii], xnorm);
      if (c_A_data[ii] >= 0.0) {
        xnorm = -xnorm;
      }

      if (std::abs(xnorm) < 1.0020841800044864E-292) {
        knt = -1;
        b_tmp = ii - b_i;
        do {
          knt++;
          for (lastv = i; lastv <= b_tmp + 12; lastv++) {
            c_A_data[lastv - 1] *= 9.9792015476736E+291;
          }

          xnorm *= 9.9792015476736E+291;
          b_atmp *= 9.9792015476736E+291;
        } while (!(std::abs(xnorm) >= 1.0020841800044864E-292));

        xnorm = rt_hypotd_snf(b_atmp, xnrm2_n1aj1qbx(11 - b_i, c_A_data, ii + 2));
        if (b_atmp >= 0.0) {
          xnorm = -xnorm;
        }

        tau_data[b_i] = (xnorm - b_atmp) / xnorm;
        b_atmp = 1.0 / (b_atmp - xnorm);
        for (lastv = i; lastv <= b_tmp + 12; lastv++) {
          c_A_data[lastv - 1] *= b_atmp;
        }

        for (lastv = 0; lastv <= knt; lastv++) {
          xnorm *= 1.0020841800044864E-292;
        }

        b_atmp = xnorm;
      } else {
        tau_data[b_i] = (xnorm - c_A_data[ii]) / xnorm;
        b_atmp = 1.0 / (c_A_data[ii] - xnorm);
        b = ii - b_i;
        for (lastv = i; lastv <= b + 12; lastv++) {
          c_A_data[lastv - 1] *= b_atmp;
        }

        b_atmp = xnorm;
      }
    }

    c_A_data[ii] = b_atmp;
    if (b_i + 1 < 6) {
      b_atmp = c_A_data[ii];
      c_A_data[ii] = 1.0;
      if (tau_data[b_i] != 0.0) {
        lastv = 12 - b_i;
        i = ii - b_i;
        while ((lastv > 0) && (c_A_data[i + 11] == 0.0)) {
          lastv--;
          i--;
        }

        i = 5 - b_i;
        exitg2 = false;
        while ((!exitg2) && (i > 0)) {
          knt = (i - 1) * 12 + ii;
          b_tmp = knt + 13;
          do {
            exitg1 = 0;
            if (b_tmp <= (knt + lastv) + 12) {
              if (c_A_data[b_tmp - 1] != 0.0) {
                exitg1 = 1;
              } else {
                b_tmp++;
              }
            } else {
              i--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        i--;
      } else {
        lastv = 0;
        i = -1;
      }

      if (lastv > 0) {
        if (i + 1 != 0) {
          if (0 <= i) {
            std::memset(&work[0], 0, (i + 1) * sizeof(real_T));
          }

          jy = 0;
          b = 12 * i + ii;
          for (jA = ii + 13; jA <= b + 13; jA += 12) {
            knt = ii;
            xnorm = 0.0;
            d = jA + lastv;
            for (b_tmp = jA; b_tmp < d; b_tmp++) {
              xnorm += c_A_data[b_tmp - 1] * c_A_data[knt];
              knt++;
            }

            work[jy] += xnorm;
            jy++;
          }
        }

        if (!(-tau_data[b_i] == 0.0)) {
          jA = ii;
          jy = 0;
          for (b_tmp = 0; b_tmp <= i; b_tmp++) {
            if (work[jy] != 0.0) {
              xnorm = work[jy] * -tau_data[b_i];
              knt = ii;
              d = (lastv + jA) + 12;
              for (b = jA + 13; b <= d; b++) {
                c_A_data[b - 1] += c_A_data[knt] * xnorm;
                knt++;
              }
            }

            jy++;
            jA += 12;
          }
        }
      }

      c_A_data[ii] = b_atmp;
    }
  }

  b_A_size[0] = 12;
  b_A_size[1] = 6;
  std::memcpy(&b_A_data[0], &c_A_data[0], 72U * sizeof(real_T));
}

void PathFollowingControllerRefMdlModelClass::qr_bd(const real_T A_data[],
  real_T Q_data[], int32_T Q_size[2], real_T R_data[], int32_T R_size[2])
{
  real_T A_data_0[72];
  real_T b_A_data[72];
  real_T b_R_data[36];
  real_T tau_data[6];
  real_T work[6];
  real_T c;
  int32_T b_A_size[2];
  int32_T b;
  int32_T coltop;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T ia;
  int32_T iaii;
  int32_T itau;
  int32_T jA;
  int32_T jy;
  int32_T lastc;
  int32_T lastv;
  boolean_T exitg2;
  xgeqrf_i(A_data, b_A_data, b_A_size, tau_data, &lastv);
  for (lastv = 0; lastv < 6; lastv++) {
    for (i = 0; i <= lastv; i++) {
      b_R_data[i + 6 * lastv] = b_A_data[12 * lastv + i];
    }

    for (i = lastv + 2; i < 7; i++) {
      b_R_data[(i + 6 * lastv) - 1] = 0.0;
    }
  }

  std::memcpy(&A_data_0[0], &b_A_data[0], 72U * sizeof(real_T));
  itau = 5;
  for (i = 0; i < 6; i++) {
    work[i] = 0.0;
  }

  for (i = 5; i >= 0; i--) {
    iaii = (i * 12 + i) + 12;
    if (i + 1 < 6) {
      A_data_0[iaii - 12] = 1.0;
      std::memcpy(&b_A_data[0], &A_data_0[0], 72U * sizeof(real_T));
      if (tau_data[itau] != 0.0) {
        lastv = 12 - i;
        lastc = (iaii - i) - 1;
        while ((lastv > 0) && (A_data_0[lastc] == 0.0)) {
          lastv--;
          lastc--;
        }

        lastc = 5 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc > 0)) {
          coltop = (lastc - 1) * 12 + iaii;
          ia = coltop + 1;
          do {
            exitg1 = 0;
            if (ia <= coltop + lastv) {
              if (A_data_0[ia - 1] != 0.0) {
                exitg1 = 1;
              } else {
                ia++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        lastc--;
        std::memcpy(&b_A_data[0], &A_data_0[0], 72U * sizeof(real_T));
      } else {
        lastv = 0;
        lastc = -1;
      }

      if (lastv > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work[0], 0, (lastc + 1) * sizeof(real_T));
          }

          jy = 0;
          b = 12 * lastc + iaii;
          for (jA = iaii + 1; jA <= b + 1; jA += 12) {
            coltop = iaii - 12;
            c = 0.0;
            d = jA + lastv;
            for (ia = jA; ia < d; ia++) {
              c += b_A_data[ia - 1] * b_A_data[coltop];
              coltop++;
            }

            work[jy] += c;
            jy++;
          }
        }

        if (!(-tau_data[itau] == 0.0)) {
          jA = iaii;
          jy = 0;
          for (ia = 0; ia <= lastc; ia++) {
            if (work[jy] != 0.0) {
              c = work[jy] * -tau_data[itau];
              coltop = iaii - 12;
              d = lastv + jA;
              for (b = jA + 1; b <= d; b++) {
                b_A_data[b - 1] += b_A_data[coltop] * c;
                coltop++;
              }
            }

            jy++;
            jA += 12;
          }
        }
      }

      std::memcpy(&A_data_0[0], &b_A_data[0], 72U * sizeof(real_T));
    }

    b = iaii - i;
    for (lastv = iaii - 10; lastv <= b; lastv++) {
      A_data_0[lastv - 1] *= -tau_data[itau];
    }

    A_data_0[iaii - 12] = 1.0 - tau_data[itau];
    b = i - 1;
    for (lastv = 0; lastv <= b; lastv++) {
      A_data_0[(iaii - lastv) - 13] = 0.0;
    }

    itau--;
  }

  for (i = 0; i < 6; i++) {
    std::memcpy(&b_A_data[i * 12], &A_data_0[i * 12], 12U * sizeof(real_T));
  }

  Q_size[0] = 12;
  Q_size[1] = 6;
  std::memcpy(&Q_data[0], &b_A_data[0], 72U * sizeof(real_T));
  R_size[0] = 6;
  R_size[1] = 6;
  std::memcpy(&R_data[0], &b_R_data[0], 36U * sizeof(real_T));
}

void PathFollowingControllerRefMdlModelClass::
  EKFCorrectorAdditive_getMeasurementJacobianAndCovariance(const real_T Rs[36],
  const real_T x[6], const real_T S[36], drivingCoordinateFrameType
  varargin_1_Frame, const real_T varargin_1_OriginPosition[3], const real_T
  varargin_1_Orientation[9], boolean_T varargin_1_HasVelocity, boolean_T
  varargin_1_HasElevation, real_T zEstimated_data[], int32_T *zEstimated_size,
  real_T Pxy_data[], int32_T Pxy_size[2], real_T Sy_data[], int32_T Sy_size[2],
  real_T dHdx_data[], int32_T dHdx_size[2], real_T Rsqrt[36])
{
  real_T unusedU0_data[72];
  real_T y_data[72];
  real_T y[36];
  real_T s;
  int32_T R_size[2];
  int32_T unusedU0_size[2];
  int32_T aoffset;
  int32_T b_i;
  int32_T b_j;
  int32_T b_k;
  int32_T coffset;
  int32_T n;
  cvmeasjac(x, varargin_1_Frame, varargin_1_OriginPosition,
            varargin_1_Orientation, varargin_1_HasVelocity,
            varargin_1_HasElevation, dHdx_data, dHdx_size);
  cvmeas(x, varargin_1_Frame, varargin_1_OriginPosition, varargin_1_Orientation,
         varargin_1_HasVelocity, varargin_1_HasElevation, zEstimated_data,
         zEstimated_size);
  for (n = 0; n < 6; n++) {
    for (b_j = 0; b_j < 6; b_j++) {
      coffset = n + 6 * b_j;
      y[coffset] = 0.0;
      for (b_i = 0; b_i < 6; b_i++) {
        y[coffset] += S[6 * b_i + n] * S[6 * b_i + b_j];
      }
    }
  }

  n = dHdx_size[0] - 1;
  Pxy_size[0] = 6;
  Pxy_size[1] = dHdx_size[0];
  for (b_j = 0; b_j <= n; b_j++) {
    coffset = b_j * 6 - 1;
    for (b_i = 0; b_i < 6; b_i++) {
      s = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        s += y[b_k * 6 + b_i] * dHdx_data[b_k * dHdx_size[0] + b_j];
      }

      Pxy_data[(coffset + b_i) + 1] = s;
    }
  }

  std::memcpy(&Rsqrt[0], &Rs[0], 36U * sizeof(real_T));
  n = dHdx_size[0] - 1;
  for (b_j = 0; b_j <= n; b_j++) {
    coffset = b_j * 6 - 1;
    for (b_i = 0; b_i < 6; b_i++) {
      aoffset = b_i * 6 - 1;
      s = 0.0;
      for (b_k = 0; b_k < 6; b_k++) {
        s += S[(aoffset + b_k) + 1] * dHdx_data[b_k * dHdx_size[0] + b_j];
      }

      y[(coffset + b_i) + 1] = s;
    }
  }

  for (n = 0; n < 6; n++) {
    for (b_j = 0; b_j < 6; b_j++) {
      y_data[b_j + 12 * n] = y[6 * n + b_j];
    }
  }

  for (n = 0; n < 6; n++) {
    for (b_j = 0; b_j < 6; b_j++) {
      y_data[(b_j + 12 * n) + 6] = Rs[6 * b_j + n];
    }
  }

  qr_bd(y_data, unusedU0_data, unusedU0_size, y, R_size);
  Sy_size[0] = 6;
  Sy_size[1] = 6;
  for (n = 0; n < 6; n++) {
    for (b_j = 0; b_j < 6; b_j++) {
      Sy_data[b_j + 6 * n] = y[6 * b_j + n];
    }
  }
}

real_T PathFollowingControllerRefMdlModelClass::trackingEKF_likelihood
  (c_trackingEKF_LFRefMdl_T *EKF, const real_T z[6], const
   BusDetectionConcatenation1DetectionsMeasurementParameters *varargin_1)
{
  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  real_T Sy_data[36];
  real_T unusedU0_data[36];
  real_T unusedU1_data[36];
  real_T unusedU2[36];
  real_T b_res[6];
  real_T zEstimated_data[6];
  real_T b_X;
  real_T bkj;
  real_T l;
  int32_T b_ipiv[6];
  int32_T Sy_size[2];
  int32_T unusedU0_size[2];
  int32_T unusedU1_size[2];
  int32_T aoffset;
  int32_T b_i;
  int32_T b_j;
  int32_T c_i;
  int32_T coffset;
  int32_T unusedU2_tmp;
  boolean_T isodd;
  if ((!EKF->pIsSetStateCovariance) || (EKF->pSqrtStateCovarianceScalar != -1.0))
  {
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtStateCovariance[b_j] = EKF->pSqrtStateCovarianceScalar *
        static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetStateCovariance = true;
    EKF->pSqrtStateCovarianceScalar = -1.0;
  }

  if (EKF->pIsFirstCallCorrect) {
    if (!EKF->pIsValidMeasurementFcn) {
      EKF->pIsValidMeasurementFcn = true;
    }

    EKF->pIsFirstCallCorrect = false;
  }

  if ((!EKF->pIsSetMeasurementNoise) || (EKF->pSqrtMeasurementNoiseScalar !=
       -1.0)) {
    for (b_j = 0; b_j < 36; b_j++) {
      EKF->pSqrtMeasurementNoise[b_j] = EKF->pSqrtMeasurementNoiseScalar *
        static_cast<real_T>(tmp[b_j]);
    }

    EKF->pIsSetMeasurementNoise = true;
    EKF->pSqrtMeasurementNoiseScalar = -1.0;
  }

  EKFCorrectorAdditive_getMeasurementJacobianAndCovariance
    (EKF->pSqrtMeasurementNoise, EKF->pState, EKF->pSqrtStateCovariance,
     varargin_1->Frame, varargin_1->OriginPosition, varargin_1->Orientation,
     varargin_1->HasVelocity, varargin_1->HasElevation, zEstimated_data, &b_j,
     unusedU0_data, unusedU0_size, Sy_data, Sy_size, unusedU1_data,
     unusedU1_size, unusedU2);
  for (b_j = 0; b_j < 6; b_j++) {
    coffset = b_j * 6 - 1;
    for (b_i = 0; b_i < 6; b_i++) {
      unusedU2[(coffset + b_i) + 1] = 0.0;
    }

    for (b_i = 0; b_i < 6; b_i++) {
      aoffset = b_i * 6 - 1;
      bkj = Sy_data[b_i * 6 + b_j];
      for (c_i = 0; c_i < 6; c_i++) {
        unusedU2_tmp = (c_i + coffset) + 1;
        unusedU2[unusedU2_tmp] += Sy_data[(c_i + aoffset) + 1] * bkj;
      }
    }

    b_res[b_j] = z[b_j] - zEstimated_data[b_j];
  }

  xzgetrf(unusedU2, unusedU0_data, b_ipiv, &b_j);
  for (b_j = 0; b_j < 6; b_j++) {
    zEstimated_data[b_j] = b_res[b_j];
  }

  for (b_j = 0; b_j < 6; b_j++) {
    coffset = 6 * b_j - 1;
    c_i = b_j - 1;
    for (b_i = 0; b_i <= c_i; b_i++) {
      bkj = unusedU0_data[(b_i + coffset) + 1];
      if (bkj != 0.0) {
        zEstimated_data[b_j] -= bkj * zEstimated_data[b_i];
      }
    }

    zEstimated_data[b_j] *= 1.0 / unusedU0_data[(b_j + coffset) + 1];
  }

  for (b_j = 5; b_j >= 0; b_j--) {
    coffset = 6 * b_j - 1;
    for (b_i = b_j + 2; b_i < 7; b_i++) {
      bkj = unusedU0_data[b_i + coffset];
      if (bkj != 0.0) {
        zEstimated_data[b_j] -= bkj * zEstimated_data[b_i - 1];
      }
    }
  }

  for (b_j = 4; b_j >= 0; b_j--) {
    b_i = b_ipiv[b_j];
    if (b_j + 1 != b_i) {
      bkj = zEstimated_data[b_j];
      zEstimated_data[b_j] = zEstimated_data[b_i - 1];
      zEstimated_data[b_i - 1] = bkj;
    }
  }

  xzgetrf(unusedU2, unusedU0_data, b_ipiv, &b_j);
  bkj = unusedU0_data[0];
  isodd = false;
  for (b_i = 0; b_i < 5; b_i++) {
    bkj *= unusedU0_data[((b_i + 1) * 6 + b_i) + 1];
    if (b_ipiv[b_i] > b_i + 1) {
      isodd = !isodd;
    }
  }

  if (isodd) {
    bkj = -bkj;
  }

  b_X = 0.0;
  for (b_j = 0; b_j < 6; b_j++) {
    b_X += zEstimated_data[b_j] * b_res[b_j];
  }

  l = std::exp(-b_X / 2.0) / 248.05021344239853 / std::sqrt(bkj);
  if (!(l > 2.2250738585072014E-308)) {
    l = 2.2250738585072014E-308;
  }

  return l;
}

void PathFollowingControllerRefMdlModelClass::trisolve_n(const real_T A_data[],
  const real_T B_data[], real_T b_B_data[], int32_T b_B_size[2])
{
  real_T tmp_0;
  int32_T b_B_data_tmp;
  int32_T b_j;
  int32_T b_k;
  int32_T i;
  int32_T jBcol;
  int32_T k;
  int32_T kAcol;
  int32_T tmp;
  b_B_size[0] = 6;
  b_B_size[1] = 6;
  std::memcpy(&b_B_data[0], &B_data[0], 36U * sizeof(real_T));
  for (b_j = 0; b_j < 6; b_j++) {
    jBcol = 6 * b_j - 1;
    for (b_k = 0; b_k < 6; b_k++) {
      k = b_k + 1;
      kAcol = b_k * 6 - 1;
      tmp = (b_k + jBcol) + 1;
      tmp_0 = b_B_data[tmp];
      if (tmp_0 != 0.0) {
        b_B_data[tmp] = tmp_0 / A_data[(b_k + kAcol) + 1];
        for (i = k + 1; i < 7; i++) {
          b_B_data_tmp = i + jBcol;
          b_B_data[b_B_data_tmp] -= b_B_data[tmp] * A_data[i + kAcol];
        }
      }
    }
  }
}

void PathFollowingControllerRefMdlModelClass::trisolve_np(const real_T A_data[],
  const real_T B[36], real_T b_B[36])
{
  real_T tmp_0;
  int32_T b;
  int32_T b_B_tmp;
  int32_T b_i;
  int32_T b_j;
  int32_T jBcol;
  int32_T k;
  int32_T kAcol;
  int32_T tmp;
  std::memcpy(&b_B[0], &B[0], 36U * sizeof(real_T));
  for (b_j = 0; b_j < 6; b_j++) {
    jBcol = 6 * b_j;
    for (k = 5; k >= 0; k--) {
      kAcol = 6 * k;
      tmp = k + jBcol;
      tmp_0 = b_B[tmp];
      if (tmp_0 != 0.0) {
        b_B[tmp] = tmp_0 / A_data[k + kAcol];
        b = k - 1;
        for (b_i = 0; b_i <= b; b_i++) {
          b_B_tmp = b_i + jBcol;
          b_B[b_B_tmp] -= b_B[tmp] * A_data[b_i + kAcol];
        }
      }
    }
  }
}

real_T PathFollowingControllerRefMdlModelClass::xnrm2_n1aj1qbxe(int32_T n, const
  real_T x[72], int32_T ix0)
{
  real_T absxk;
  real_T scale;
  real_T t;
  real_T y;
  int32_T k;
  int32_T kend;
  y = 0.0;
  scale = 3.3121686421112381E-170;
  kend = ix0 + n;
  for (k = ix0; k < kend; k++) {
    absxk = std::abs(x[k - 1]);
    if (absxk > scale) {
      t = scale / absxk;
      y = y * t * t + 1.0;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }
  }

  return scale * std::sqrt(y);
}

void PathFollowingControllerRefMdlModelClass::xgeqrf_ii(const real_T A[72],
  real_T b_A[72], real_T tau[6])
{
  real_T work[6];
  real_T b_atmp;
  real_T xnorm;
  int32_T b_i;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T iac;
  int32_T ii;
  int32_T iy;
  int32_T jA;
  int32_T jy;
  int32_T knt;
  int32_T lastv;
  boolean_T exitg2;
  for (i = 0; i < 6; i++) {
    tau[i] = 0.0;
  }

  std::memcpy(&b_A[0], &A[0], 72U * sizeof(real_T));
  for (i = 0; i < 6; i++) {
    work[i] = 0.0;
  }

  for (b_i = 0; b_i < 6; b_i++) {
    ii = b_i * 12 + b_i;
    lastv = ii + 2;
    b_atmp = b_A[ii];
    tau[b_i] = 0.0;
    xnorm = xnrm2_n1aj1qbxe(11 - b_i, b_A, ii + 2);
    if (xnorm != 0.0) {
      xnorm = rt_hypotd_snf(b_A[ii], xnorm);
      if (b_A[ii] >= 0.0) {
        xnorm = -xnorm;
      }

      if (std::abs(xnorm) < 1.0020841800044864E-292) {
        knt = -1;
        jA = ii - b_i;
        do {
          knt++;
          for (i = lastv; i <= jA + 12; i++) {
            b_A[i - 1] *= 9.9792015476736E+291;
          }

          xnorm *= 9.9792015476736E+291;
          b_atmp *= 9.9792015476736E+291;
        } while (!(std::abs(xnorm) >= 1.0020841800044864E-292));

        xnorm = rt_hypotd_snf(b_atmp, xnrm2_n1aj1qbxe(11 - b_i, b_A, ii + 2));
        if (b_atmp >= 0.0) {
          xnorm = -xnorm;
        }

        tau[b_i] = (xnorm - b_atmp) / xnorm;
        b_atmp = 1.0 / (b_atmp - xnorm);
        for (i = lastv; i <= jA + 12; i++) {
          b_A[i - 1] *= b_atmp;
        }

        for (i = 0; i <= knt; i++) {
          xnorm *= 1.0020841800044864E-292;
        }

        b_atmp = xnorm;
      } else {
        tau[b_i] = (xnorm - b_A[ii]) / xnorm;
        b_atmp = 1.0 / (b_A[ii] - xnorm);
        jA = ii - b_i;
        for (i = lastv; i <= jA + 12; i++) {
          b_A[i - 1] *= b_atmp;
        }

        b_atmp = xnorm;
      }
    }

    b_A[ii] = b_atmp;
    if (b_i + 1 < 6) {
      b_atmp = b_A[ii];
      b_A[ii] = 1.0;
      if (tau[b_i] != 0.0) {
        lastv = 12 - b_i;
        i = ii - b_i;
        while ((lastv > 0) && (b_A[i + 11] == 0.0)) {
          lastv--;
          i--;
        }

        i = 5 - b_i;
        exitg2 = false;
        while ((!exitg2) && (i > 0)) {
          knt = (i - 1) * 12 + ii;
          jy = knt + 13;
          do {
            exitg1 = 0;
            if (jy <= (knt + lastv) + 12) {
              if (b_A[jy - 1] != 0.0) {
                exitg1 = 1;
              } else {
                jy++;
              }
            } else {
              i--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        i--;
      } else {
        lastv = 0;
        i = -1;
      }

      if (lastv > 0) {
        if (i + 1 != 0) {
          if (0 <= i) {
            std::memset(&work[0], 0, (i + 1) * sizeof(real_T));
          }

          iy = 0;
          jA = 12 * i + ii;
          for (iac = ii + 13; iac <= jA + 13; iac += 12) {
            knt = ii;
            xnorm = 0.0;
            d = iac + lastv;
            for (jy = iac; jy < d; jy++) {
              xnorm += b_A[jy - 1] * b_A[knt];
              knt++;
            }

            work[iy] += xnorm;
            iy++;
          }
        }

        if (!(-tau[b_i] == 0.0)) {
          jA = ii;
          jy = 0;
          for (iy = 0; iy <= i; iy++) {
            if (work[jy] != 0.0) {
              xnorm = work[jy] * -tau[b_i];
              knt = ii;
              iac = (lastv + jA) + 12;
              for (d = jA + 13; d <= iac; d++) {
                b_A[d - 1] += b_A[knt] * xnorm;
                knt++;
              }
            }

            jy++;
            jA += 12;
          }
        }
      }

      b_A[ii] = b_atmp;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::qr_bd0(const real_T A[72], real_T
  Q[72], real_T R[36])
{
  real_T b_A[72];
  real_T tau[6];
  real_T work[6];
  real_T c;
  int32_T coltop;
  int32_T d;
  int32_T exitg1;
  int32_T i;
  int32_T iac;
  int32_T iaii;
  int32_T itau;
  int32_T iy;
  int32_T jA;
  int32_T jy;
  int32_T lastc;
  int32_T lastv;
  boolean_T exitg2;
  xgeqrf_ii(A, b_A, tau);
  itau = 5;
  for (lastv = 0; lastv < 6; lastv++) {
    for (i = 0; i <= lastv; i++) {
      R[i + 6 * lastv] = b_A[12 * lastv + i];
    }

    for (i = lastv + 2; i < 7; i++) {
      R[(i + 6 * lastv) - 1] = 0.0;
    }

    work[lastv] = 0.0;
  }

  for (i = 5; i >= 0; i--) {
    iaii = (i * 12 + i) + 12;
    if (i + 1 < 6) {
      b_A[iaii - 12] = 1.0;
      if (tau[itau] != 0.0) {
        lastv = 12 - i;
        lastc = (iaii - i) - 1;
        while ((lastv > 0) && (b_A[lastc] == 0.0)) {
          lastv--;
          lastc--;
        }

        lastc = 5 - i;
        exitg2 = false;
        while ((!exitg2) && (lastc > 0)) {
          coltop = (lastc - 1) * 12 + iaii;
          jA = coltop + 1;
          do {
            exitg1 = 0;
            if (jA <= coltop + lastv) {
              if (b_A[jA - 1] != 0.0) {
                exitg1 = 1;
              } else {
                jA++;
              }
            } else {
              lastc--;
              exitg1 = 2;
            }
          } while (exitg1 == 0);

          if (exitg1 == 1) {
            exitg2 = true;
          }
        }

        lastc--;
      } else {
        lastv = 0;
        lastc = -1;
      }

      if (lastv > 0) {
        if (lastc + 1 != 0) {
          if (0 <= lastc) {
            std::memset(&work[0], 0, (lastc + 1) * sizeof(real_T));
          }

          iy = 0;
          jy = 12 * lastc + iaii;
          for (iac = iaii + 1; iac <= jy + 1; iac += 12) {
            coltop = iaii - 12;
            c = 0.0;
            d = iac + lastv;
            for (jA = iac; jA < d; jA++) {
              c += b_A[jA - 1] * b_A[coltop];
              coltop++;
            }

            work[iy] += c;
            iy++;
          }
        }

        if (!(-tau[itau] == 0.0)) {
          jA = iaii;
          jy = 0;
          for (iy = 0; iy <= lastc; iy++) {
            if (work[jy] != 0.0) {
              c = work[jy] * -tau[itau];
              coltop = iaii - 12;
              iac = lastv + jA;
              for (d = jA + 1; d <= iac; d++) {
                b_A[d - 1] += b_A[coltop] * c;
                coltop++;
              }
            }

            jy++;
            jA += 12;
          }
        }
      }
    }

    jy = iaii - i;
    for (lastv = iaii - 10; lastv <= jy; lastv++) {
      b_A[lastv - 1] *= -tau[itau];
    }

    b_A[iaii - 12] = 1.0 - tau[itau];
    jy = i - 1;
    for (lastv = 0; lastv <= jy; lastv++) {
      b_A[(iaii - lastv) - 13] = 0.0;
    }

    itau--;
  }

  for (itau = 0; itau < 6; itau++) {
    std::memcpy(&Q[itau * 12], &b_A[itau * 12], 12U * sizeof(real_T));
  }
}

void PathFollowingControllerRefMdlModelClass::
  EKFCorrector_correctStateAndSqrtCovariance(const real_T x[6], const real_T S
  [36], const real_T residue[6], const real_T Pxy_data[], const int32_T
  Pxy_size[2], const real_T Sy_data[], const real_T H_data[], const int32_T
  H_size[2], const real_T Rsqrt[36], real_T b_x[6], real_T b_S[36])
{
  real_T b_I_0[72];
  real_T unusedU0[72];
  real_T A[36];
  real_T K[36];
  real_T K_0[36];
  real_T b_I[36];
  real_T s;
  int32_T C_size[2];
  int32_T B_size_idx_0;
  int32_T K_tmp;
  int32_T aoffset;
  int32_T b_i;
  int32_T b_j;
  int32_T coffset;
  B_size_idx_0 = Pxy_size[1];
  coffset = Pxy_size[1];
  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < coffset; b_i++) {
      A[b_i + B_size_idx_0 * b_j] = Pxy_data[6 * b_i + b_j];
    }
  }

  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      K[b_i + 6 * b_j] = A[B_size_idx_0 * b_j + b_i];
    }
  }

  std::memcpy(&A[0], &K[0], 36U * sizeof(real_T));
  trisolve_n(Sy_data, A, K, C_size);
  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      B_size_idx_0 = 6 * b_j + b_i;
      A[b_i + 6 * b_j] = K[B_size_idx_0];
      b_I[B_size_idx_0] = Sy_data[6 * b_i + b_j];
    }
  }

  std::memcpy(&K[0], &A[0], 36U * sizeof(real_T));
  trisolve_np(b_I, K, A);
  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      K[b_i + 6 * b_j] = A[6 * b_i + b_j];
    }
  }

  for (b_j = 0; b_j < 6; b_j++) {
    s = 0.0;
    for (b_i = 0; b_i < 6; b_i++) {
      s += K[6 * b_i + b_j] * residue[b_i];
    }

    b_x[b_j] = x[b_j] + s;
  }

  std::memset(&b_I[0], 0, 36U * sizeof(real_T));
  for (B_size_idx_0 = 0; B_size_idx_0 < 6; B_size_idx_0++) {
    b_I[B_size_idx_0 + 6 * B_size_idx_0] = 1.0;
  }

  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      s = 0.0;
      for (B_size_idx_0 = 0; B_size_idx_0 < 6; B_size_idx_0++) {
        s += K[6 * B_size_idx_0 + b_j] * H_data[H_size[0] * b_i + B_size_idx_0];
      }

      B_size_idx_0 = 6 * b_i + b_j;
      A[B_size_idx_0] = b_I[B_size_idx_0] - s;
    }
  }

  for (b_j = 0; b_j < 6; b_j++) {
    coffset = b_j * 6 - 1;
    for (b_i = 0; b_i < 6; b_i++) {
      aoffset = b_i * 6 - 1;
      s = 0.0;
      K_tmp = b_j + 6 * b_i;
      K_0[K_tmp] = 0.0;
      for (B_size_idx_0 = 0; B_size_idx_0 < 6; B_size_idx_0++) {
        s += S[(aoffset + B_size_idx_0) + 1] * A[B_size_idx_0 * 6 + b_j];
        K_0[K_tmp] += K[6 * B_size_idx_0 + b_i] * Rsqrt[6 * b_j + B_size_idx_0];
      }

      b_I[(coffset + b_i) + 1] = s;
    }
  }

  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      B_size_idx_0 = 6 * b_j + b_i;
      coffset = b_i + 12 * b_j;
      b_I_0[coffset] = b_I[B_size_idx_0];
      b_I_0[coffset + 6] = K_0[B_size_idx_0];
    }
  }

  qr_bd0(b_I_0, unusedU0, K);
  for (b_j = 0; b_j < 6; b_j++) {
    for (b_i = 0; b_i < 6; b_i++) {
      b_S[b_i + 6 * b_j] = K[6 * b_i + b_j];
    }
  }
}

void PathFollowingControllerRefMdlModelClass::ExtendedKalmanFilter_correct
  (c_trackingEKF_LFRefMdl_T *obj, const real_T z[6], drivingCoordinateFrameType
   varargin_1_Frame, const real_T varargin_1_OriginPosition[3], const real_T
   varargin_1_Orientation[9], boolean_T varargin_1_HasVelocity, boolean_T
   varargin_1_HasElevation)
{
  static const int8_T tmp[36] = { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };

  real_T Pxy_data[36];
  real_T Rsqrt[36];
  real_T Sy_data[36];
  real_T dHdx_data[36];
  real_T obj_0[36];
  real_T zEstimated_data[6];
  real_T z_0[6];
  int32_T Pxy_size[2];
  int32_T Sy_size[2];
  int32_T dHdx_size[2];
  int32_T zEstimated_size;
  if ((!obj->pIsSetStateCovariance) || (obj->pSqrtStateCovarianceScalar != -1.0))
  {
    for (zEstimated_size = 0; zEstimated_size < 36; zEstimated_size++) {
      obj->pSqrtStateCovariance[zEstimated_size] =
        obj->pSqrtStateCovarianceScalar * static_cast<real_T>
        (tmp[zEstimated_size]);
    }

    obj->pIsSetStateCovariance = true;
    obj->pSqrtStateCovarianceScalar = -1.0;
  }

  if (obj->pIsFirstCallCorrect) {
    if (!obj->pIsValidMeasurementFcn) {
      obj->pIsValidMeasurementFcn = true;
    }

    obj->pIsFirstCallCorrect = false;
  }

  if ((!obj->pIsSetMeasurementNoise) || (obj->pSqrtMeasurementNoiseScalar !=
       -1.0)) {
    for (zEstimated_size = 0; zEstimated_size < 36; zEstimated_size++) {
      obj->pSqrtMeasurementNoise[zEstimated_size] =
        obj->pSqrtMeasurementNoiseScalar * static_cast<real_T>
        (tmp[zEstimated_size]);
    }

    obj->pIsSetMeasurementNoise = true;
    obj->pSqrtMeasurementNoiseScalar = -1.0;
  }

  EKFCorrectorAdditive_getMeasurementJacobianAndCovariance
    (obj->pSqrtMeasurementNoise, obj->pState, obj->pSqrtStateCovariance,
     varargin_1_Frame, varargin_1_OriginPosition, varargin_1_Orientation,
     varargin_1_HasVelocity, varargin_1_HasElevation, zEstimated_data,
     &zEstimated_size, Pxy_data, Pxy_size, Sy_data, Sy_size, dHdx_data,
     dHdx_size, Rsqrt);
  for (zEstimated_size = 0; zEstimated_size < 6; zEstimated_size++) {
    z_0[zEstimated_size] = z[zEstimated_size] - zEstimated_data[zEstimated_size];
  }

  for (zEstimated_size = 0; zEstimated_size < 6; zEstimated_size++) {
    zEstimated_data[zEstimated_size] = obj->pState[zEstimated_size];
  }

  std::memcpy(&obj_0[0], &obj->pSqrtStateCovariance[0], 36U * sizeof(real_T));
  EKFCorrector_correctStateAndSqrtCovariance(zEstimated_data, obj_0, z_0,
    Pxy_data, Pxy_size, Sy_data, dHdx_data, dHdx_size, Rsqrt, obj->pState,
    obj->pSqrtStateCovariance);
  obj->pIsSetStateCovariance = true;
  obj->pSqrtStateCovarianceScalar = -1.0;
  obj->pHasPrediction = false;
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_correct
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track, const
   BusDetectionConcatenation1Detections *detections)
{
  c_trackHistoryLogic_LFRefMdl_T *obj;
  real_T dt;
  int32_T i;
  uint32_T q0;
  uint32_T qY;
  boolean_T tmp[50];
  boolean_T x_idx_0;
  boolean_T x_idx_1;
  boolean_T x_idx_2;
  track->pObjectAttributes[static_cast<int32_T>(detections->SensorIndex) - 1] =
    detections->ObjectAttributes;
  track->pUsedObjectAttributes[static_cast<int32_T>(detections->SensorIndex) - 1]
    = true;
  if (detections->ObjectClassID != 0.0) {
    track->ObjectClassID = detections->ObjectClassID;
  }

  dt = detections->Time - track->Time;
  if (!(dt <= 0.0)) {
    predictTrackFilter(track->Filter, dt);
    track->Time = detections->Time;
  }

  ExtendedKalmanFilter_set_MeasurementNoise(track->Filter,
    detections->MeasurementNoise);
  trackingEKF_likelihood(track->Filter, detections->Measurement,
    &detections->MeasurementParameters);
  ExtendedKalmanFilter_correct(track->Filter, detections->Measurement,
    detections->MeasurementParameters.Frame,
    detections->MeasurementParameters.OriginPosition,
    detections->MeasurementParameters.Orientation,
    detections->MeasurementParameters.HasVelocity,
    detections->MeasurementParameters.HasElevation);
  track->UpdateTime = detections->Time;
  obj = track->TrackLogic;
  tmp[0] = true;
  for (i = 0; i < 49; i++) {
    tmp[i + 1] = obj->pRecentHistory[i];
  }

  for (i = 0; i < 50; i++) {
    obj->pRecentHistory[i] = tmp[i];
  }

  obj->pIsFirstUpdate = false;
  q0 = track->pAge;
  qY = q0 + /*MW:OvSatOk*/ 1U;
  if (q0 + 1U < q0) {
    qY = MAX_uint32_T;
  }

  track->pAge = qY;
  track->pIsCoasted = false;
  if (!track->IsConfirmed) {
    if (track->ObjectClassID != 0.0) {
      x_idx_0 = true;
    } else {
      obj = track->TrackLogic;
      if (obj->pIsFirstUpdate) {
        x_idx_0 = false;
      } else {
        x_idx_0 = obj->pRecentHistory[0];
        x_idx_1 = obj->pRecentHistory[1];
        x_idx_2 = obj->pRecentHistory[2];
        x_idx_0 = ((x_idx_0 + x_idx_1) + x_idx_2 >= 2);
      }
    }

    track->IsConfirmed = x_idx_0;
  }
}

void PathFollowingControllerRefMdlModelClass::nullAssignment_mkj(uint32_T
  x_data[], int32_T *x_size, const int32_T idx_data[], const int32_T idx_size[2])
{
  int32_T k0;
  int32_T loop_ub;
  int32_T nxin;
  int32_T nxout;
  boolean_T b_data[70];
  nxin = *x_size - 1;
  if (0 <= *x_size - 1) {
    std::memset(&b_data[0], 0, ((*x_size - 1) + 1) * sizeof(boolean_T));
  }

  nxout = idx_size[1] - 1;
  for (loop_ub = 0; loop_ub <= nxout; loop_ub++) {
    b_data[idx_data[loop_ub] - 1] = true;
  }

  k0 = 0;
  nxout = *x_size - 1;
  for (loop_ub = 0; loop_ub <= nxout; loop_ub++) {
    k0 += b_data[loop_ub];
  }

  nxout = *x_size - k0;
  k0 = -1;
  for (loop_ub = 0; loop_ub <= nxin; loop_ub++) {
    if ((loop_ub + 1 > *x_size) || (!b_data[loop_ub])) {
      k0++;
      x_data[k0] = x_data[loop_ub];
    }
  }

  if (1 > nxout) {
    nxout = 0;
  }

  *x_size = nxout;
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_initiateTracks
  (multiObjectTracker_LFRefMdl_T *obj, uint32_T OverallUnassigned_data[],
   int32_T *OverallUnassigned_size, uint32_T initTrIDs_data[], int32_T
   initTrIDs_size[2], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  BusDetectionConcatenation1Detections dets;
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track;
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_0;
  real_T costMatrix_data[70];
  real_T f_data[70];
  real_T origSen[70];
  real_T sensorsInUnassigned_data[70];
  real_T NewTrackNumber;
  real_T initSensor;
  real_T lastTrackInd;
  int32_T q_data[70];
  int32_T m_data[69];
  int32_T f_size[2];
  int32_T m_size[2];
  int32_T i;
  int32_T initSensor_tmp;
  int32_T loop_ub;
  int32_T partialTrueCount;
  uint32_T checkedUnassigned_data[70];
  uint32_T q0;
  uint32_T qY;
  boolean_T p_data[70];
  boolean_T exitg1;
  boolean_T tf;
  lastTrackInd = obj->pNumLiveTracks;
  obj_0 = obj->cDetectionManager;
  for (i = 0; i < 70; i++) {
    localB->dets_b[i] = obj_0->pDetections[i];
  }

  obj_0 = obj->cDetectionManager;
  for (i = 0; i < 70; i++) {
    origSen[i] = obj_0->pOriginatingSensor[i];
  }

  exitg1 = false;
  while ((!exitg1) && (*OverallUnassigned_size != 0)) {
    NewTrackNumber = obj->pNumLiveTracks + 1.0;
    q0 = obj->pLastTrackID;
    qY = q0 + /*MW:OvSatOk*/ 1U;
    if (q0 + 1U < q0) {
      qY = MAX_uint32_T;
    }

    tf = TrackManager_initiateTrack(obj, qY, localB->dets_b[static_cast<int32_T>
      (OverallUnassigned_data[0]) - 1].Time, localB->dets_b[static_cast<int32_T>
      (OverallUnassigned_data[0]) - 1].Measurement, localB->dets_b
      [static_cast<int32_T>(OverallUnassigned_data[0]) - 1].MeasurementNoise,
      localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0]) - 1].
      SensorIndex, localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0])
      - 1].ObjectClassID, localB->dets_b[static_cast<int32_T>
      (OverallUnassigned_data[0]) - 1].MeasurementParameters.Frame,
      localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0]) - 1].
      MeasurementParameters.OriginPosition, localB->dets_b[static_cast<int32_T>
      (OverallUnassigned_data[0]) - 1].MeasurementParameters.Orientation,
      localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0]) - 1].
      MeasurementParameters.HasVelocity, localB->dets_b[static_cast<int32_T>
      (OverallUnassigned_data[0]) - 1].MeasurementParameters.HasElevation,
      localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0]) - 1].
      ObjectAttributes);
    if (tf) {
      obj->pLastTrackID = qY;
      if (0 <= *OverallUnassigned_size - 1) {
        std::memset(&costMatrix_data[0], 0, ((*OverallUnassigned_size - 1) + 1) *
                    sizeof(real_T));
      }

      track = obj->pTracksList[static_cast<int32_T>(NewTrackNumber) - 1];
      initSensor_tmp = static_cast<int32_T>(OverallUnassigned_data[0]) - 1;
      initSensor = localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[0])
        - 1].SensorIndex;
      loop_ub = *OverallUnassigned_size;
      for (i = 0; i < loop_ub; i++) {
        sensorsInUnassigned_data[i] = origSen[static_cast<int32_T>
          (OverallUnassigned_data[i]) - 1];
      }

      loop_ub = *OverallUnassigned_size - 1;
      for (i = 0; i <= loop_ub; i++) {
        if (sensorsInUnassigned_data[i] == initSensor) {
          costMatrix_data[i] = (rtInf);
        }
      }

      loop_ub = *OverallUnassigned_size;
      for (i = 0; i < loop_ub; i++) {
        p_data[i] = !(localB->dets_b[initSensor_tmp].SensorIndex ==
                      sensorsInUnassigned_data[i]);
      }

      loop_ub = *OverallUnassigned_size - 1;
      initSensor_tmp = 0;
      for (i = 0; i <= loop_ub; i++) {
        if (p_data[i]) {
          initSensor_tmp++;
        }
      }

      partialTrueCount = 0;
      for (i = 0; i <= loop_ub; i++) {
        if (p_data[i]) {
          q_data[partialTrueCount] = i + 1;
          partialTrueCount++;
        }
      }

      for (i = 0; i < initSensor_tmp; i++) {
        checkedUnassigned_data[i] = OverallUnassigned_data[q_data[i] - 1];
      }

      ObjectTrack_distance_k(track, localB->dets_b, checkedUnassigned_data,
        &initSensor_tmp, f_data, f_size);
      initSensor_tmp = *OverallUnassigned_size - 1;
      loop_ub = 0;
      for (i = 0; i <= initSensor_tmp; i++) {
        if (!(sensorsInUnassigned_data[i] == initSensor)) {
          costMatrix_data[i] = f_data[loop_ub];
          loop_ub++;
        }
      }

      if (0 <= *OverallUnassigned_size - 1) {
        std::memset(&checkedUnassigned_data[0], 0, *OverallUnassigned_size *
                    sizeof(uint32_T));
      }

      initSensor = 0.0;
      loop_ub = *OverallUnassigned_size - 2;
      for (i = 0; i <= loop_ub; i++) {
        if (costMatrix_data[i + 1] <= obj->pCostOfNonAssignment) {
          dets = localB->dets_b[static_cast<int32_T>(OverallUnassigned_data[i +
            1]) - 1];
          ObjectTrack_correct(obj->pTracksList[static_cast<int32_T>
                              (NewTrackNumber) - 1], &dets);
        } else {
          initSensor++;
          checkedUnassigned_data[static_cast<int32_T>(initSensor) - 1] =
            OverallUnassigned_data[i + 1];
        }
      }

      obj->pConfirmedTracks[static_cast<int32_T>(NewTrackNumber) - 1] =
        obj->pTracksList[static_cast<int32_T>(NewTrackNumber) - 1]->IsConfirmed;
      if (initSensor > 0.0) {
        if (0 <= *OverallUnassigned_size - 1) {
          std::memcpy(&OverallUnassigned_data[0], &checkedUnassigned_data[0],
                      *OverallUnassigned_size * sizeof(uint32_T));
        }

        m_size[0] = 1;
        initSensor_tmp = *OverallUnassigned_size - static_cast<int32_T>
          (initSensor + 1.0);
        m_size[1] = initSensor_tmp + 1;
        for (i = 0; i <= initSensor_tmp; i++) {
          m_data[i] = static_cast<int32_T>(initSensor + 1.0) + i;
        }

        nullAssignment_mkj(OverallUnassigned_data, OverallUnassigned_size,
                           m_data, m_size);
      } else {
        exitg1 = true;
      }
    } else {
      exitg1 = true;
    }
  }

  NewTrackNumber = obj->pNumLiveTracks;
  if (lastTrackInd + 1.0 > NewTrackNumber) {
    initSensor_tmp = 0;
    i = -1;
  } else {
    initSensor_tmp = static_cast<int32_T>(lastTrackInd + 1.0) - 1;
    i = static_cast<int32_T>(NewTrackNumber) - 1;
  }

  initTrIDs_size[0] = 1;
  loop_ub = i - initSensor_tmp;
  initTrIDs_size[1] = loop_ub + 1;
  for (i = 0; i <= loop_ub; i++) {
    initTrIDs_data[i] = obj->pTrackIDs[initSensor_tmp + i];
  }
}

void PathFollowingControllerRefMdlModelClass::unique_vector_d(const uint32_T
  a_data[], const int32_T *a_size, uint32_T b_data[], int32_T *b_size)
{
  int32_T idx_data[200];
  int32_T iwork_data[200];
  int32_T i;
  int32_T i2;
  int32_T j;
  int32_T k;
  int32_T kEnd;
  int32_T na;
  int32_T p;
  int32_T pEnd;
  int32_T q;
  int32_T qEnd;
  uint32_T x;
  na = *a_size - 1;
  if (0 <= *a_size - 1) {
    std::memset(&idx_data[0], 0, *a_size * sizeof(int32_T));
  }

  if (*a_size != 0) {
    i = *a_size;
    for (k = 1; k <= i - 1; k += 2) {
      if (a_data[k - 1] <= a_data[k]) {
        idx_data[k - 1] = k;
        idx_data[k] = k + 1;
      } else {
        idx_data[k - 1] = k + 1;
        idx_data[k] = k;
      }
    }

    if ((*a_size & 1U) != 0U) {
      idx_data[*a_size - 1] = *a_size;
    }

    i = 2;
    while (i < (*a_size + 1) - 1) {
      i2 = i << 1;
      j = 1;
      for (pEnd = i + 1; pEnd < *a_size + 1; pEnd = qEnd + i) {
        p = j - 1;
        q = pEnd - 1;
        qEnd = j + i2;
        if (qEnd > *a_size + 1) {
          qEnd = *a_size + 1;
        }

        k = 0;
        kEnd = qEnd - j;
        while (k + 1 <= kEnd) {
          if (a_data[idx_data[p] - 1] <= a_data[idx_data[q] - 1]) {
            iwork_data[k] = idx_data[p];
            p++;
            if (p + 1 == pEnd) {
              while (q + 1 < qEnd) {
                k++;
                iwork_data[k] = idx_data[q];
                q++;
              }
            }
          } else {
            iwork_data[k] = idx_data[q];
            q++;
            if (q + 1 == qEnd) {
              while (p + 1 < pEnd) {
                k++;
                iwork_data[k] = idx_data[p];
                p++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < kEnd; k++) {
          idx_data[(j + k) - 1] = iwork_data[k];
        }

        j = qEnd;
      }

      i = i2;
    }
  }

  for (k = 0; k <= na; k++) {
    b_data[k] = a_data[idx_data[k] - 1];
  }

  na = -1;
  k = 0;
  while (k + 1 <= *a_size) {
    x = b_data[k];
    do {
      k++;
    } while (!((k + 1 > *a_size) || (b_data[k] != x)));

    na++;
    b_data[na] = x;
  }

  if (1 > na + 1) {
    na = 0;
  } else {
    na++;
  }

  *b_size = na;
}

void PathFollowingControllerRefMdlModelClass::repmat(const
  BusDetectionConcatenation1Detections *a, const real_T varargin_1[2],
  BusDetectionConcatenation1Detections b_data[], int32_T b_size[2])
{
  int32_T b_itilerow;
  int32_T ntilerows;
  b_size[0] = static_cast<int32_T>(varargin_1[0]);
  b_size[1] = 1;
  if (static_cast<int32_T>(varargin_1[0]) != 0) {
    ntilerows = static_cast<int32_T>(varargin_1[0]) - 1;
    for (b_itilerow = 0; b_itilerow <= ntilerows; b_itilerow++) {
      b_data[b_itilerow] = *a;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::
  GNNTracker_extractDetectionsForTrack(multiObjectTracker_LFRefMdl_T *obj, const
  uint32_T detectionsForCorrect_data[], const int32_T *detectionsForCorrect_size,
  BusDetectionConcatenation1Detections trackDetections_data[], int32_T
  trackDetections_size[2], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  BusDetectionConcatenation1Detections dets;
  e_matlabshared_tracking_internal_fusion_SimulinkDetectionMana_T *obj_0;
  real_T detectionsForCorrect[2];
  int32_T b;
  int32_T i;
  obj_0 = obj->cDetectionManager;
  for (i = 0; i < 70; i++) {
    localB->dets_p[i] = obj_0->pDetections[i];
  }

  dets = localB->dets_p[static_cast<int32_T>(detectionsForCorrect_data[0]) - 1];
  detectionsForCorrect[0] = *detectionsForCorrect_size;
  detectionsForCorrect[1] = 1.0;
  repmat(&dets, detectionsForCorrect, trackDetections_data, trackDetections_size);
  b = *detectionsForCorrect_size - 2;
  for (i = 0; i <= b; i++) {
    trackDetections_data[i + 1] = localB->dets_p[static_cast<int32_T>
      (detectionsForCorrect_data[i + 1]) - 1];
  }
}

void PathFollowingControllerRefMdlModelClass::ObjectTrack_correct_c
  (b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track, const
   BusDetectionConcatenation1Detections detections_data[], const int32_T
   detections_size[2])
{
  BusDetectionConcatenation1DetectionsMeasurementParameters e;
  c_trackHistoryLogic_LFRefMdl_T *obj;
  real_T detTime_data[400];
  real_T detTime_data_0[400];
  real_T detTime_data_1;
  real_T dt;
  real_T sensorIndex;
  int32_T detTime_size[2];
  int32_T detTime_size_0[2];
  int32_T b;
  int32_T b_i;
  int32_T c;
  uint32_T q0;
  uint32_T qY;
  boolean_T tmp[50];
  boolean_T x_idx_0;
  boolean_T x_idx_1;
  boolean_T x_idx_2;
  b = detections_size[0] - 1;
  detTime_size[0] = detections_size[0];
  for (b_i = 0; b_i <= b; b_i++) {
    detTime_data[b_i] = static_cast<real_T>(b_i) + 1.0;
    detTime_data[b_i + detTime_size[0]] = detections_data[b_i].Time;
  }

  detTime_size_0[0] = detections_size[0];
  detTime_size_0[1] = 2;
  b_i = detections_size[0] << 1;
  if (0 <= b_i - 1) {
    std::memcpy(&detTime_data_0[0], &detTime_data[0], b_i * sizeof(real_T));
  }

  sortrows(detTime_data_0, detTime_size_0, detTime_data, detTime_size);
  c = detections_size[0] - 1;
  for (b_i = 0; b_i <= c; b_i++) {
    detTime_data_1 = detTime_data[b_i];
    sensorIndex = detections_data[static_cast<int32_T>(detTime_data_1) - 1].
      SensorIndex;
    track->pObjectAttributes[static_cast<int32_T>(sensorIndex) - 1] =
      detections_data[static_cast<int32_T>(detTime_data_1) - 1].ObjectAttributes;
    track->pUsedObjectAttributes[static_cast<int32_T>(sensorIndex) - 1] = true;
    if (detections_data[static_cast<int32_T>(detTime_data_1) - 1].ObjectClassID
        != 0.0) {
      track->ObjectClassID = detections_data[static_cast<int32_T>(detTime_data_1)
        - 1].ObjectClassID;
    }

    sensorIndex = detections_data[static_cast<int32_T>(detTime_data_1) - 1].Time;
    dt = sensorIndex - track->Time;
    if (!(dt <= 0.0)) {
      predictTrackFilter(track->Filter, dt);
      track->Time = sensorIndex;
    }

    e = detections_data[static_cast<int32_T>(detTime_data_1) - 1].
      MeasurementParameters;
    ExtendedKalmanFilter_set_MeasurementNoise(track->Filter, detections_data[
      static_cast<int32_T>(detTime_data_1) - 1].MeasurementNoise);
    trackingEKF_likelihood(track->Filter, detections_data[static_cast<int32_T>
      (detTime_data_1) - 1].Measurement, &e);
    ExtendedKalmanFilter_correct(track->Filter, detections_data
      [static_cast<int32_T>(detTime_data_1) - 1].Measurement, e.Frame,
      e.OriginPosition, e.Orientation, e.HasVelocity, e.HasElevation);
    track->UpdateTime = detections_data[static_cast<int32_T>(detTime_data_1) - 1]
      .Time;
    obj = track->TrackLogic;
    tmp[0] = true;
    for (b = 0; b < 49; b++) {
      tmp[b + 1] = obj->pRecentHistory[b];
    }

    for (b = 0; b < 50; b++) {
      obj->pRecentHistory[b] = tmp[b];
    }

    obj->pIsFirstUpdate = false;
    q0 = track->pAge;
    qY = q0 + /*MW:OvSatOk*/ 1U;
    if (q0 + 1U < q0) {
      qY = MAX_uint32_T;
    }

    track->pAge = qY;
  }

  track->pIsCoasted = false;
  if (!track->IsConfirmed) {
    if (track->ObjectClassID != 0.0) {
      x_idx_0 = true;
    } else {
      obj = track->TrackLogic;
      if (obj->pIsFirstUpdate) {
        x_idx_0 = false;
      } else {
        x_idx_0 = obj->pRecentHistory[0];
        x_idx_1 = obj->pRecentHistory[1];
        x_idx_2 = obj->pRecentHistory[2];
        x_idx_0 = ((x_idx_0 + x_idx_1) + x_idx_2 >= 2);
      }
    }

    track->IsConfirmed = x_idx_0;
  }
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_updateAssignedTracks
  (multiObjectTracker_LFRefMdl_T *obj, const uint32_T OverallAssignments_data[],
   const int32_T OverallAssignments_size[2],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  int32_T h_data[200];
  int32_T trackDetections_size[2];
  int32_T OverallAssignments_size_0;
  int32_T UniqueTracks_size;
  int32_T c;
  int32_T loop_ub;
  int32_T partialTrueCount;
  int32_T trueCount;
  uint32_T OverallAssignments_data_0[200];
  uint32_T UniqueTracks_data[200];
  uint32_T UniqueTracks_data_0;
  boolean_T f_data[200];
  boolean_T f_data_0;
  if (OverallAssignments_size[0] != 0) {
    loop_ub = OverallAssignments_size[0];
    OverallAssignments_size_0 = OverallAssignments_size[0];
    if (0 <= loop_ub - 1) {
      std::memcpy(&OverallAssignments_data_0[0], &OverallAssignments_data[0],
                  loop_ub * sizeof(uint32_T));
    }

    unique_vector_d(OverallAssignments_data_0, &OverallAssignments_size_0,
                    UniqueTracks_data, &UniqueTracks_size);
    c = UniqueTracks_size - 1;
    for (OverallAssignments_size_0 = 0; OverallAssignments_size_0 <= c;
         OverallAssignments_size_0++) {
      UniqueTracks_data_0 = UniqueTracks_data[OverallAssignments_size_0];
      loop_ub = OverallAssignments_size[0];
      trueCount = 0;
      partialTrueCount = 0;
      for (UniqueTracks_size = 0; UniqueTracks_size < loop_ub; UniqueTracks_size
           ++) {
        f_data_0 = (OverallAssignments_data[UniqueTracks_size] ==
                    UniqueTracks_data_0);
        if (f_data_0) {
          trueCount++;
          h_data[partialTrueCount] = UniqueTracks_size + 1;
          partialTrueCount++;
        }

        f_data[UniqueTracks_size] = f_data_0;
      }

      for (UniqueTracks_size = 0; UniqueTracks_size < trueCount;
           UniqueTracks_size++) {
        OverallAssignments_data_0[UniqueTracks_size] = OverallAssignments_data
          [(h_data[UniqueTracks_size] + OverallAssignments_size[0]) - 1];
      }

      GNNTracker_extractDetectionsForTrack(obj, OverallAssignments_data_0,
        &trueCount, localB->trackDetections_data, trackDetections_size, localB);
      trueCount = OverallAssignments_size[0] - 1;
      loop_ub = 0;
      for (UniqueTracks_size = 0; UniqueTracks_size <= trueCount;
           UniqueTracks_size++) {
        if (f_data[UniqueTracks_size]) {
          loop_ub++;
        }
      }

      if (loop_ub != 0) {
        ObjectTrack_correct_c(obj->pTracksList[static_cast<int32_T>
                              (UniqueTracks_data_0) - 1],
                              localB->trackDetections_data, trackDetections_size);
      }

      obj->pConfirmedTracks[static_cast<int32_T>(UniqueTracks_data_0) - 1] =
        obj->pTracksList[static_cast<int32_T>(UniqueTracks_data_0) - 1]
        ->IsConfirmed;
    }
  }
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_deleteOldTracks
  (multiObjectTracker_LFRefMdl_T *obj, const uint32_T UnassignedTracks_data[],
   const int32_T *UnassignedTracks_size, uint32_T delTrIDs_data[], int32_T
   delTrIDs_size[2])
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *temp;
  c_trackHistoryLogic_LFRefMdl_T *obj_0;
  real_T e;
  real_T i;
  int32_T ii_data[100];
  int32_T indsToDelete_data[100];
  int32_T b;
  int32_T b_0;
  int32_T currentInd;
  int32_T f;
  int32_T idx;
  int32_T m_size_idx_1;
  int32_T nz;
  int32_T trueCount;
  uint32_T r_data[101];
  uint32_T q0;
  uint32_T qY;
  boolean_T m_data[101];
  boolean_T toDelete[100];
  boolean_T tmp[50];
  boolean_T x_data[5];
  boolean_T exitg1;
  boolean_T tentativeTrack;
  boolean_T x_idx_1;
  boolean_T x_idx_2;
  std::memset(&toDelete[0], 0, 100U * sizeof(boolean_T));
  b = static_cast<int32_T>(((-1.0 - static_cast<real_T>(*UnassignedTracks_size))
    + 1.0) / -1.0) - 1;
  for (idx = 0; idx <= b; idx++) {
    trueCount = static_cast<int32_T>(UnassignedTracks_data[static_cast<int32_T>(
      static_cast<real_T>(*UnassignedTracks_size) + -static_cast<real_T>(idx)) -
      1]) - 1;
    if (obj->pWasDetectable[trueCount]) {
      temp = obj->pTracksList[trueCount];
      temp->pIsCoasted = true;
      obj_0 = temp->TrackLogic;
      tmp[0] = false;
      for (nz = 0; nz < 49; nz++) {
        tmp[nz + 1] = obj_0->pRecentHistory[nz];
      }

      for (nz = 0; nz < 50; nz++) {
        obj_0->pRecentHistory[nz] = tmp[nz];
      }

      obj_0->pIsFirstUpdate = false;
      q0 = temp->pAge;
      qY = q0 + /*MW:OvSatOk*/ 1U;
      if (q0 + 1U < q0) {
        qY = MAX_uint32_T;
      }

      temp->pAge = qY;
      temp = obj->pTracksList[trueCount];
      obj_0 = temp->TrackLogic;
      tentativeTrack = !temp->IsConfirmed;
      q0 = temp->pAge;
      if (obj_0->pIsFirstUpdate) {
        toDelete[trueCount] = false;
      } else if (!tentativeTrack) {
        if (q0 > 5U) {
          for (nz = 0; nz < 5; nz++) {
            x_data[nz] = !obj_0->pRecentHistory[nz];
          }

          toDelete[trueCount] = ((((x_data[0] + x_data[1]) + x_data[2]) +
            x_data[3]) + x_data[4] >= 5);
        } else {
          if (1U > q0) {
            b_0 = -1;
          } else {
            b_0 = static_cast<int32_T>(q0) - 1;
          }

          currentInd = b_0 + 1;
          for (nz = 0; nz <= b_0; nz++) {
            x_data[nz] = !obj_0->pRecentHistory[nz];
          }

          if (b_0 + 1 == 0) {
            nz = 0;
          } else {
            nz = x_data[0];
            for (b_0 = 2; b_0 <= currentInd; b_0++) {
              nz += x_data[b_0 - 1];
            }
          }

          toDelete[trueCount] = (nz >= 5);
        }
      } else {
        tentativeTrack = obj_0->pRecentHistory[0];
        x_idx_1 = obj_0->pRecentHistory[1];
        x_idx_2 = obj_0->pRecentHistory[2];
        qY = 3U - /*MW:OvSatOk*/ q0;
        if (3U - q0 > 3U) {
          qY = 0U;
        }

        toDelete[trueCount] = (2 - ((tentativeTrack + x_idx_1) + x_idx_2) >
          static_cast<int32_T>(qY));
      }
    }
  }

  trueCount = 0;
  nz = 0;
  for (idx = 0; idx < 100; idx++) {
    if (toDelete[idx]) {
      trueCount++;
      ii_data[nz] = idx + 1;
      nz++;
    }
  }

  delTrIDs_size[0] = 1;
  delTrIDs_size[1] = trueCount;
  idx = trueCount - 1;
  for (trueCount = 0; trueCount <= idx; trueCount++) {
    delTrIDs_data[trueCount] = obj->pTrackIDs[ii_data[trueCount] - 1];
  }

  nz = toDelete[0];
  for (b_0 = 0; b_0 < 99; b_0++) {
    nz += toDelete[b_0 + 1];
  }

  idx = 0;
  trueCount = 1;
  exitg1 = false;
  while ((!exitg1) && (trueCount - 1 < 100)) {
    if (toDelete[trueCount - 1]) {
      idx++;
      ii_data[idx - 1] = trueCount;
      if (idx >= 100) {
        exitg1 = true;
      } else {
        trueCount++;
      }
    } else {
      trueCount++;
    }
  }

  if (1 > idx) {
    idx = 0;
  }

  idx--;
  if (0 <= idx) {
    std::memcpy(&indsToDelete_data[0], &ii_data[0], (idx + 1) * sizeof(int32_T));
  }

  for (idx = 0; idx < nz; idx++) {
    ObjectTrack_nullify(obj->pTracksList[indsToDelete_data[idx] - 1]);
  }

  b = static_cast<int32_T>(((-1.0 - static_cast<real_T>(nz)) + 1.0) / -1.0) - 1;
  for (idx = 0; idx <= b; idx++) {
    i = static_cast<real_T>(nz) + -static_cast<real_T>(idx);
    currentInd = indsToDelete_data[(nz - idx) - 1];
    b_0 = indsToDelete_data[static_cast<int32_T>(i) - 1];
    temp = obj->pTracksList[b_0 - 1];
    e = obj->pNumLiveTracks;
    f = static_cast<int32_T>((1.0 - (static_cast<real_T>(currentInd) + 1.0)) + e)
      - 1;
    for (trueCount = 0; trueCount <= f; trueCount++) {
      e = (static_cast<real_T>(currentInd) + 1.0) + static_cast<real_T>
        (trueCount);
      obj->pTracksList[static_cast<int32_T>(e - 1.0) - 1] = obj->pTracksList[
        static_cast<int32_T>(e) - 1];
    }

    obj->pTracksList[static_cast<int32_T>(obj->pNumLiveTracks) - 1] = temp;
    obj->pNumLiveTracks--;
    if (static_cast<real_T>(b_0) + 1.0 > 100.0) {
      b_0 = 0;
      trueCount = -1;
    } else {
      b_0 = currentInd;
      trueCount = 99;
    }

    f = trueCount - b_0;
    m_size_idx_1 = f + 2;
    for (trueCount = 0; trueCount <= f; trueCount++) {
      m_data[trueCount] = obj->pConfirmedTracks[b_0 + trueCount];
    }

    m_data[f + 1] = false;
    for (trueCount = 0; trueCount < m_size_idx_1; trueCount++) {
      obj->pConfirmedTracks[(currentInd + trueCount) - 1] = m_data[trueCount];
    }

    if (static_cast<real_T>(indsToDelete_data[static_cast<int32_T>(i) - 1]) +
        1.0 > 100.0) {
      b_0 = 0;
      trueCount = -1;
    } else {
      b_0 = currentInd;
      trueCount = 99;
    }

    f = trueCount - b_0;
    m_size_idx_1 = f + 2;
    for (trueCount = 0; trueCount <= f; trueCount++) {
      r_data[trueCount] = obj->pTrackIDs[b_0 + trueCount];
    }

    r_data[f + 1] = 0U;
    for (trueCount = 0; trueCount < m_size_idx_1; trueCount++) {
      obj->pTrackIDs[(currentInd + trueCount) - 1] = r_data[trueCount];
    }
  }
}

void PathFollowingControllerRefMdlModelClass::TrackManager_sendToBus(const
  BusMultiObjectTracker1Tracks st[100], const BusMultiObjectTracker1Tracks
  *varargin_2, real_T *tracksOutput_NumTracks, BusMultiObjectTracker1Tracks
  tracksOutput_Tracks[100])
{
  int32_T i;
  *tracksOutput_NumTracks = 100.0;
  for (i = 0; i < 100; i++) {
    tracksOutput_Tracks[i] = *varargin_2;
    tracksOutput_Tracks[i] = st[i];
  }
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_getTracks
  (multiObjectTracker_LFRefMdl_T *obj, const boolean_T list[100], real_T
   *tracksOutput_NumTracks, BusMultiObjectTracker1Tracks tracksOutput_Tracks[100],
   B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  BusMultiObjectTracker1Tracks track;
  sW4W9Ljj33pbPPYs8uukpCB_LFRefMdl_T expl_temp;
  int32_T ii_data[100];
  int32_T liveTracksToOutput_data[100];
  int32_T i;
  int32_T idx;
  int32_T ii_data_0;
  int32_T k;
  boolean_T exitg1;
  ObjectTrack_trackToStruct(obj->pTracksList[99], &expl_temp);
  track.TrackID = expl_temp.TrackID;
  track.BranchID = expl_temp.BranchID;
  track.SourceIndex = 0U;
  track.UpdateTime = expl_temp.UpdateTime;
  track.Age = expl_temp.Age;
  for (k = 0; k < 6; k++) {
    track.State[k] = 0.0;
  }

  std::memset(&track.StateCovariance[0], 0, 36U * sizeof(real_T));
  track.ObjectClassID = expl_temp.ObjectClassID;
  track.TrackLogic = History;
  for (k = 0; k < 5; k++) {
    track.TrackLogicState[k] = expl_temp.TrackLogicState[k];
  }

  track.IsConfirmed = expl_temp.IsConfirmed;
  track.IsCoasted = expl_temp.IsCoasted;
  track.IsSelfReported = true;
  track.ObjectAttributes[0] = expl_temp.ObjectAttributes[0];
  track.ObjectAttributes[1] = expl_temp.ObjectAttributes[1];
  for (i = 0; i < 100; i++) {
    localB->tracks[i] = track;
  }

  i = list[0];
  for (k = 0; k < 99; k++) {
    i += list[k + 1];
  }

  idx = 0;
  k = 1;
  exitg1 = false;
  while ((!exitg1) && (k - 1 < 100)) {
    if (list[k - 1]) {
      idx++;
      ii_data[idx - 1] = k;
      if (idx >= 100) {
        exitg1 = true;
      } else {
        k++;
      }
    } else {
      k++;
    }
  }

  if (1 > idx) {
    idx = 0;
  }

  for (k = 0; k < idx; k++) {
    ii_data_0 = ii_data[k];
    liveTracksToOutput_data[k] = ii_data_0;
    ii_data[k] = ii_data_0;
  }

  if (i > 0) {
    for (idx = 0; idx < i; idx++) {
      ObjectTrack_trackToStruct(obj->pTracksList[liveTracksToOutput_data[idx] -
        1], &expl_temp);
      localB->tracks[idx].TrackID = expl_temp.TrackID;
      localB->tracks[idx].BranchID = expl_temp.BranchID;
      localB->tracks[idx].SourceIndex = 0U;
      localB->tracks[idx].UpdateTime = expl_temp.UpdateTime;
      localB->tracks[idx].Age = expl_temp.Age;
      for (k = 0; k < 6; k++) {
        localB->tracks[idx].State[k] = expl_temp.State[k];
      }

      std::memcpy(&localB->tracks[idx].StateCovariance[0],
                  &expl_temp.StateCovariance[0], 36U * sizeof(real_T));
      localB->tracks[idx].ObjectClassID = expl_temp.ObjectClassID;
      localB->tracks[idx].TrackLogic = History;
      for (k = 0; k < 5; k++) {
        localB->tracks[idx].TrackLogicState[k] = expl_temp.TrackLogicState[k];
      }

      localB->tracks[idx].IsConfirmed = expl_temp.IsConfirmed;
      localB->tracks[idx].IsCoasted = expl_temp.IsCoasted;
      localB->tracks[idx].IsSelfReported = true;
      localB->tracks[idx].ObjectAttributes[0] = expl_temp.ObjectAttributes[0];
      localB->tracks[idx].ObjectAttributes[1] = expl_temp.ObjectAttributes[1];
    }
  }

  TrackManager_sendToBus(localB->tracks, &track, tracksOutput_NumTracks,
    tracksOutput_Tracks);
  *tracksOutput_NumTracks = i;
}

void PathFollowingControllerRefMdlModelClass::
  multiObjectTracker_formatSimulinkOutputs(multiObjectTracker_LFRefMdl_T *obj,
  real_T *varargout_1_NumTracks, BusMultiObjectTracker1Tracks
  varargout_1_Tracks[100], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  int32_T i;
  int32_T partialTrueCount;
  int32_T trueCount;
  int8_T b_data[100];
  boolean_T toOut[100];
  trueCount = 0;
  partialTrueCount = 0;
  for (i = 0; i < 100; i++) {
    toOut[i] = false;
    if (obj->pConfirmedTracks[i]) {
      trueCount++;
    }

    if (obj->pConfirmedTracks[i]) {
      b_data[partialTrueCount] = static_cast<int8_T>(i + 1);
      partialTrueCount++;
    }
  }

  for (i = 0; i < trueCount; i++) {
    toOut[b_data[i] - 1] = true;
  }

  GNNTracker_getTracks(obj, toOut, &localB->vargs.NumTracks,
                       localB->vargs.Tracks, localB);
  *varargout_1_NumTracks = localB->vargs.NumTracks;
  std::memcpy(&varargout_1_Tracks[0], &localB->vargs.Tracks[0], 100U * sizeof
              (BusMultiObjectTracker1Tracks));
}

void PathFollowingControllerRefMdlModelClass::GNNTracker_stepImpl
  (multiObjectTracker_LFRefMdl_T *obj, real_T detections_NumDetections, const
   BusDetectionConcatenation1Detections detections_Detections[70], real_T
   varargin_1, real_T *varargout_1_NumTracks, BusMultiObjectTracker1Tracks
   varargout_1_Tracks[100], B_TrackingandSensorFusion_LFRefMdl_T *localB)
{
  b_matlabshared_tracking_internal_fusion_ObjectTrack_LFRefMdl_T *track;
  emxArray_uint32_T_1x100_LFRefMdl_T obj_0;
  real_T dt;
  int32_T assigned_size[2];
  int32_T costMatrix_size[2];
  int32_T unassignedDets_size;
  int32_T unassignedTrs_size;
  uint32_T assigned_data[400];
  uint32_T unassignedTrs_data[100];
  uint32_T unassignedDets_data[70];
  obj->pNumDetections = SystemCore_step(obj->cDetectionManager,
    detections_NumDetections, detections_Detections);
  GNNTracker_associate(obj, assigned_data, assigned_size, unassignedTrs_data,
                       &unassignedTrs_size, unassignedDets_data,
                       &unassignedDets_size, localB->costMatrix_data,
                       costMatrix_size, localB);
  GNNTracker_trackDetectability(obj);
  GNNTracker_initiateTracks(obj, unassignedDets_data, &unassignedDets_size,
    obj_0.data, obj_0.size, localB);
  GNNTracker_updateAssignedTracks(obj, assigned_data, assigned_size, localB);
  GNNTracker_deleteOldTracks(obj, unassignedTrs_data, &unassignedTrs_size,
    obj_0.data, obj_0.size);
  if (varargin_1 > obj->pLastTimeStamp) {
    dt = obj->pNumLiveTracks;
    unassignedDets_size = static_cast<int32_T>(dt) - 1;
    for (unassignedTrs_size = 0; unassignedTrs_size <= unassignedDets_size;
         unassignedTrs_size++) {
      if (varargin_1 > obj->pTracksList[unassignedTrs_size]->Time) {
        track = obj->pTracksList[unassignedTrs_size];
        dt = varargin_1 - track->Time;
        if (!(dt <= 0.0)) {
          predictTrackFilter(track->Filter, dt);
          track->Time = varargin_1;
        }
      }
    }
  }

  obj->pLastTimeStamp = varargin_1;
  multiObjectTracker_formatSimulinkOutputs(obj, varargout_1_NumTracks,
    varargout_1_Tracks, localB);
}

void PathFollowingControllerRefMdlModelClass::getLogicalIndices_l2h5wg(boolean_T
  indOut[9])
{
  real_T y[9];
  real_T y_data[9];
  int32_T b_k;
  int32_T i;
  int32_T y_data_tmp;
  int8_T y_0[9];
  for (b_k = 0; b_k < 3; b_k++) {
    for (i = 0; i < 3; i++) {
      y_0[i + 3 * b_k] = 1;
    }
  }

  for (b_k = 0; b_k < 3; b_k++) {
    y[b_k] = y_0[3 * b_k];
    y[b_k + 3] = y_0[3 * b_k + 1];
    y[b_k + 6] = y_0[3 * b_k + 2];
  }

  for (b_k = 0; b_k < 3; b_k++) {
    for (i = 0; i < 3; i++) {
      y_data_tmp = 3 * b_k + i;
      y_data[y_data_tmp] = y[y_data_tmp] + 1.0;
      y[y_data_tmp] = y_data[y_data_tmp];
    }
  }

  for (b_k = 0; b_k < 9; b_k++) {
    indOut[b_k] = false;
    indOut[b_k] = (y[b_k] == 2.0);
  }
}

void PathFollowingControllerRefMdlModelClass::
  DetectionConcatenation_setScalarStructVals(const
  BusDetectionConcatenation1Detections *out, real_T in_Time, const real_T
  in_Measurement[6], const real_T in_MeasurementNoise[36], real_T in_SensorIndex,
  real_T in_ObjectClassID, real_T in_ObjectAttributes_TargetIndex, real_T
  in_ObjectAttributes_SNR, const BusRadarDetectionsMeasurementParameters
  *in_MeasurementParameters, BusDetectionConcatenation1Detections *b_out)
{
  BusDetectionConcatenation1DetectionsMeasurementParameters b_setVal;
  BusDetectionConcatenation1DetectionsMeasurementParameters f;
  BusRadarDetectionsObjectAttributes e;
  real_T b;
  real_T c;
  int32_T f_data[9];
  int32_T g_data[6];
  int32_T e_data[3];
  int32_T b_trueCount;
  int32_T c_i;
  int32_T partialTrueCount;
  drivingCoordinateFrameType b_0;
  int8_T h_data[36];
  boolean_T c_ind[36];
  boolean_T c_ind_0[9];
  boolean_T b_ind[6];
  boolean_T b_ind_0[3];
  boolean_T c_0;
  boolean_T ind_tmp_tmp;
  *b_out = *out;
  ind_tmp_tmp = getLogicalIndices();
  b = out->Time;
  if (ind_tmp_tmp) {
    b = in_Time;
  }

  b_out->Time = b;
  getLogicalIndices_l(b_ind);
  b_trueCount = 0;
  partialTrueCount = 0;
  for (c_i = 0; c_i < 6; c_i++) {
    if (b_ind[c_i]) {
      b_trueCount++;
      g_data[partialTrueCount] = c_i + 1;
      partialTrueCount++;
    }
  }

  for (partialTrueCount = 0; partialTrueCount < b_trueCount; partialTrueCount++)
  {
    b_out->Measurement[g_data[partialTrueCount] - 1] =
      in_Measurement[partialTrueCount];
  }

  getLogicalIndices_l2(c_ind);
  b_trueCount = 0;
  partialTrueCount = 0;
  for (c_i = 0; c_i < 36; c_i++) {
    if (c_ind[c_i]) {
      b_trueCount++;
      h_data[partialTrueCount] = static_cast<int8_T>(c_i + 1);
      partialTrueCount++;
    }
  }

  for (partialTrueCount = 0; partialTrueCount < b_trueCount; partialTrueCount++)
  {
    b_out->MeasurementNoise[h_data[partialTrueCount] - 1] =
      in_MeasurementNoise[partialTrueCount];
  }

  c = out->SensorIndex;
  if (ind_tmp_tmp) {
    c = in_SensorIndex;
  }

  b_out->SensorIndex = c;
  b = out->ObjectClassID;
  if (ind_tmp_tmp) {
    b = in_ObjectClassID;
  }

  b_out->ObjectClassID = b;
  b = out->ObjectAttributes.TargetIndex;
  c = out->ObjectAttributes.SNR;
  if (ind_tmp_tmp) {
    b = in_ObjectAttributes_TargetIndex;
    c = in_ObjectAttributes_SNR;
  }

  e = out->ObjectAttributes;
  if (getLogicalIndices()) {
    e.TargetIndex = b;
    e.SNR = c;
  }

  b_out->ObjectAttributes = e;
  b_setVal = out->MeasurementParameters;
  b_0 = out->MeasurementParameters.Frame;
  if (getLogicalIndices()) {
    b_0 = in_MeasurementParameters->Frame;
  }

  b_setVal.Frame = b_0;
  getLogicalIndices_l2h5w(b_ind_0);
  b_trueCount = 0;
  partialTrueCount = 0;
  for (c_i = 0; c_i < 3; c_i++) {
    if (b_ind_0[c_i]) {
      b_trueCount++;
      e_data[partialTrueCount] = c_i + 1;
      partialTrueCount++;
    }
  }

  for (partialTrueCount = 0; partialTrueCount < b_trueCount; partialTrueCount++)
  {
    b_setVal.OriginPosition[e_data[partialTrueCount] - 1] =
      in_MeasurementParameters->OriginPosition[partialTrueCount];
  }

  getLogicalIndices_l2h5wg(c_ind_0);
  b_trueCount = 0;
  partialTrueCount = 0;
  for (c_i = 0; c_i < 9; c_i++) {
    if (c_ind_0[c_i]) {
      b_trueCount++;
      f_data[partialTrueCount] = c_i + 1;
      partialTrueCount++;
    }
  }

  for (partialTrueCount = 0; partialTrueCount < b_trueCount; partialTrueCount++)
  {
    b_setVal.Orientation[f_data[partialTrueCount] - 1] =
      in_MeasurementParameters->Orientation[partialTrueCount];
  }

  c_0 = out->MeasurementParameters.HasElevation;
  if (ind_tmp_tmp) {
    c_0 = in_MeasurementParameters->HasElevation;
  }

  b_setVal.HasElevation = c_0;
  c_0 = out->MeasurementParameters.HasVelocity;
  if (ind_tmp_tmp) {
    c_0 = in_MeasurementParameters->HasVelocity;
  }

  b_setVal.HasVelocity = c_0;
  f = out->MeasurementParameters;
  if (getLogicalIndices()) {
    f = b_setVal;
  }

  b_out->MeasurementParameters = f;
}

// System initialize for atomic system: '<Root>/Tracking and Sensor Fusion'
void PathFollowingControllerRefMdlModelClass::TrackingandSensorFusion_Init(const
  BusVision *rtu_Vision, B_TrackingandSensorFusion_LFRefMdl_T *localB,
  DW_TrackingandSensorFusion_LFRefMdl_T *localDW)
{
  // Start for MATLABSystem: '<S3>/MATLAB System'
  localDW->obj_d.matlabCodegenIsDeleted = true;
  localDW->obj_d.isInitialized = 0;
  localDW->obj_d.matlabCodegenIsDeleted = false;
  localDW->objisempty_p = true;
  localDW->obj_d.isSetupComplete = false;
  localDW->obj_d.isInitialized = 1;
  localDW->obj_d.isSetupComplete = true;

  // Start for MATLABSystem: '<S3>/Detection Concatenation1' incorporates:
  //   MATLABSystem: '<S3>/MATLAB System'

  localDW->obj_m.matlabCodegenIsDeleted = true;
  localDW->obj_m.isInitialized = 0;
  localDW->obj_m.matlabCodegenIsDeleted = false;
  localDW->objisempty_i = true;
  localDW->obj_m.isSetupComplete = false;
  localDW->obj_m.isInitialized = 1;
  DetectionConcatenation_setupImpl(&localDW->obj_m, rtu_Vision->NumDetections,
    rtu_Vision->IsValidTime, rtu_Vision->Detections,
    localB->MATLABSystem.Detections, localB);
  localDW->obj_m.isSetupComplete = true;

  // Start for MATLABSystem: '<S3>/Multi-Object Tracker' incorporates:
  //   MATLABSystem: '<S3>/Detection Concatenation1'

  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.pLastTimeStamp = -2.2204460492503131E-16;
  localDW->obj.matlabCodegenIsDeleted = false;
  localDW->objisempty = true;
  SystemCore_setup(&localDW->obj, localB->DetectionConcatenation1.NumDetections,
                   localB->DetectionConcatenation1.Detections, localB);

  // InitializeConditions for MATLABSystem: '<S3>/Multi-Object Tracker'
  GNNTracker_resetImpl(&localDW->obj);
}

// Output and update for atomic system: '<Root>/Tracking and Sensor Fusion'
void PathFollowingControllerRefMdlModelClass::TrackingandSensorFusion(const
  BusRadar *rtu_Radar, const BusVision *rtu_Vision, real32_T rtu_Lane, real32_T
  rtu_Lane_p, real32_T rtu_Lane_px, real32_T rtu_Lane_m, real32_T rtu_Lane_k,
  real32_T rtu_Lane_c, real32_T rtu_Lane_mn, real32_T rtu_Lane_h, real32_T
  rtu_Lane_o, real32_T rtu_Lane_j, real_T rtu_SystemClock, real_T
  *rty_RelativeDistance, real_T *rty_RelativeVelocity, real_T *rty_MIOTrackIndex,
  B_TrackingandSensorFusion_LFRefMdl_T *localB,
  DW_TrackingandSensorFusion_LFRefMdl_T *localDW)
{
  BusDetectionConcatenation1Detections tmp;
  BusDetectionConcatenation1DetectionsMeasurementParameters b_setVal;
  BusDetectionConcatenation1DetectionsMeasurementParameters f;
  BusRadarDetectionsObjectAttributes e;
  BusRadarDetectionsObjectAttributes setVal;
  real_T mioState[6];
  real_T position[2];
  real_T b_0;
  real_T c;
  real_T d;
  real_T idx;
  int32_T e_data[9];
  int32_T g_data[6];
  int32_T d_data[3];
  int32_T b;
  int32_T b_n;
  int32_T b_trueCount;
  int32_T c_i;
  int32_T partialTrueCount;
  real32_T lb_idx_0;
  real32_T lb_idx_1;
  real32_T lb_idx_2;
  real32_T lb_idx_3;
  real32_T rb_idx_0;
  real32_T rb_idx_1;
  real32_T rb_idx_2;
  real32_T rb_idx_3;
  drivingCoordinateFrameType b_1;
  int8_T h_data[36];
  boolean_T c_ind[36];
  boolean_T c_ind_0[9];
  boolean_T b_ind[6];
  boolean_T b_ind_0[3];
  boolean_T c_0;
  boolean_T ind;

  // MATLABSystem: '<S3>/MATLAB System'
  helperClusterDetections_stepImpl(rtu_Radar->NumDetections,
    rtu_Radar->IsValidTime, rtu_Radar->Detections, &localB->r1.NumDetections,
    &localB->r1.IsValidTime, localB->r1.Detections, localB);

  // MATLABSystem: '<S3>/Detection Concatenation1' incorporates:
  //   MATLABSystem: '<S3>/MATLAB System'

  DetectionConcatenation_checkUniqueSensorIndex(rtu_Vision->NumDetections,
    rtu_Vision->Detections, localB->r1.NumDetections, localB->r1.Detections);
  localB->r = localDW->obj_m.pOutTemp;
  idx = 1.0;
  b = static_cast<int32_T>(rtu_Vision->NumDetections) - 1;
  if (0 <= static_cast<int32_T>(rtu_Vision->NumDetections) - 1) {
    ind = getLogicalIndices();
    getLogicalIndices_l(b_ind);
    getLogicalIndices_l2(c_ind);
    getLogicalIndices_l2h5w(b_ind_0);
    getLogicalIndices_l2h5wg(c_ind_0);
  }

  for (b_n = 0; b_n <= b; b_n++) {
    b_0 = localB->r.Detections[static_cast<int32_T>(idx) - 1].Time;
    if (ind) {
      b_0 = rtu_Vision->Detections[b_n].Time;
    }

    localB->r.Detections[static_cast<int32_T>(idx) - 1].Time = b_0;
    b_trueCount = 0;
    partialTrueCount = 0;
    for (c_i = 0; c_i < 6; c_i++) {
      if (b_ind[c_i]) {
        b_trueCount++;
        g_data[partialTrueCount] = c_i + 1;
        partialTrueCount++;
      }
    }

    for (c_i = 0; c_i < b_trueCount; c_i++) {
      localB->r.Detections[static_cast<int32_T>(idx) - 1].Measurement[g_data[c_i]
        - 1] = rtu_Vision->Detections[b_n].Measurement[c_i];
    }

    b_trueCount = 0;
    partialTrueCount = 0;
    for (c_i = 0; c_i < 36; c_i++) {
      if (c_ind[c_i]) {
        b_trueCount++;
        h_data[partialTrueCount] = static_cast<int8_T>(c_i + 1);
        partialTrueCount++;
      }
    }

    for (c_i = 0; c_i < b_trueCount; c_i++) {
      localB->r.Detections[static_cast<int32_T>(idx) - 1]
        .MeasurementNoise[h_data[c_i] - 1] = rtu_Vision->Detections[b_n].
        MeasurementNoise[c_i];
    }

    c = localB->r.Detections[static_cast<int32_T>(idx) - 1].SensorIndex;
    d = localB->r.Detections[static_cast<int32_T>(idx) - 1].ObjectClassID;
    setVal = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      ObjectAttributes;
    b_0 = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      ObjectAttributes.TargetIndex;
    if (ind) {
      c = rtu_Vision->Detections[b_n].SensorIndex;
      d = rtu_Vision->Detections[b_n].ObjectClassID;
      b_0 = rtu_Vision->Detections[b_n].ObjectAttributes.TargetIndex;
    }

    localB->r.Detections[static_cast<int32_T>(idx) - 1].SensorIndex = c;
    localB->r.Detections[static_cast<int32_T>(idx) - 1].ObjectClassID = d;
    setVal.TargetIndex = b_0;
    e = localB->r.Detections[static_cast<int32_T>(idx) - 1].ObjectAttributes;
    b_setVal = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      MeasurementParameters;
    b_1 = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      MeasurementParameters.Frame;
    if (ind) {
      e = setVal;
    }

    if (ind) {
      b_1 = rtu_Vision->Detections[b_n].MeasurementParameters.Frame;
    }

    localB->r.Detections[static_cast<int32_T>(idx) - 1].ObjectAttributes = e;
    b_setVal.Frame = b_1;
    b_trueCount = 0;
    partialTrueCount = 0;
    for (c_i = 0; c_i < 3; c_i++) {
      if (b_ind_0[c_i]) {
        b_trueCount++;
        d_data[partialTrueCount] = c_i + 1;
        partialTrueCount++;
      }
    }

    for (c_i = 0; c_i < b_trueCount; c_i++) {
      b_setVal.OriginPosition[d_data[c_i] - 1] = rtu_Vision->Detections[b_n].
        MeasurementParameters.OriginPosition[c_i];
    }

    b_trueCount = 0;
    partialTrueCount = 0;
    for (c_i = 0; c_i < 9; c_i++) {
      if (c_ind_0[c_i]) {
        b_trueCount++;
        e_data[partialTrueCount] = c_i + 1;
        partialTrueCount++;
      }
    }

    for (c_i = 0; c_i < b_trueCount; c_i++) {
      b_setVal.Orientation[e_data[c_i] - 1] = rtu_Vision->Detections[b_n].
        MeasurementParameters.Orientation[c_i];
    }

    c_0 = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      MeasurementParameters.HasVelocity;
    if (getLogicalIndices()) {
      c_0 = rtu_Vision->Detections[b_n].MeasurementParameters.HasVelocity;
    }

    b_setVal.HasVelocity = c_0;
    f = localB->r.Detections[static_cast<int32_T>(idx) - 1].
      MeasurementParameters;
    if (getLogicalIndices()) {
      f = b_setVal;
    }

    localB->r.Detections[static_cast<int32_T>(idx) - 1].MeasurementParameters =
      f;
    idx++;
  }

  c_i = static_cast<int32_T>(localB->r1.NumDetections) - 1;
  for (b_n = 0; b_n <= c_i; b_n++) {
    tmp = localB->r.Detections[static_cast<int32_T>(idx) - 1];
    DetectionConcatenation_setScalarStructVals(&tmp, localB->r1.Detections[b_n].
      Time, localB->r1.Detections[b_n].Measurement, localB->r1.Detections[b_n].
      MeasurementNoise, localB->r1.Detections[b_n].SensorIndex,
      localB->r1.Detections[b_n].ObjectClassID, localB->r1.Detections[b_n].
      ObjectAttributes.TargetIndex, localB->r1.Detections[b_n].
      ObjectAttributes.SNR, &localB->r1.Detections[b_n].MeasurementParameters,
      &localB->r.Detections[static_cast<int32_T>(idx) - 1]);
    idx++;
  }

  // MATLABSystem: '<S3>/Multi-Object Tracker' incorporates:
  //   MATLABSystem: '<S3>/Detection Concatenation1'
  //   MATLABSystem: '<S3>/MATLAB System'

  if (localDW->obj.TunablePropsChanged) {
    localDW->obj.TunablePropsChanged = false;
  }

  GNNTracker_stepImpl(&localDW->obj, rtu_Vision->NumDetections +
                      localB->r1.NumDetections, localB->r.Detections,
                      rtu_SystemClock, &localB->MultiObjectTracker.NumTracks,
                      localB->MultiObjectTracker.Tracks, localB);

  // End of MATLABSystem: '<S3>/Multi-Object Tracker'

  // MATLAB Function: '<S3>/Find Lead Car' incorporates:
  //   BusCreator generated from: '<S3>/Find Lead Car'
  //   Constant: '<S3>/Position Selector'
  //   MATLABSystem: '<S3>/Multi-Object Tracker'

  idx = 1000.0;
  b_0 = 0.0;
  if (rtu_Lane_k > 0.01) {
    lb_idx_0 = rtu_Lane_p / 6.0F;
    lb_idx_1 = rtu_Lane / 2.0F;
    lb_idx_2 = rtu_Lane_px;
    lb_idx_3 = rtu_Lane_m;
  } else {
    lb_idx_0 = 0.0F;
    lb_idx_1 = 0.0F;
    lb_idx_2 = 0.0F;
    lb_idx_3 = 1.8F;
  }

  if (rtu_Lane_j > 0.01) {
    rb_idx_0 = rtu_Lane_mn / 6.0F;
    rb_idx_1 = rtu_Lane_c / 2.0F;
    rb_idx_2 = rtu_Lane_h;
    rb_idx_3 = rtu_Lane_o;
  } else {
    rb_idx_0 = 0.0F;
    rb_idx_1 = 0.0F;
    rb_idx_2 = 0.0F;
    rb_idx_3 = -1.8F;
  }

  for (b_n = 0; b_n < static_cast<int32_T>(localB->MultiObjectTracker.NumTracks);
       b_n++) {
    for (c_i = 0; c_i < 2; c_i++) {
      position[c_i] = 0.0;
      for (b = 0; b < 6; b++) {
        position[c_i] += LFRefMdl_ConstP.PositionSelector_Value[(b << 1) + c_i] *
          localB->MultiObjectTracker.Tracks[b_n].State[b];
      }
    }

    if ((position[0] < idx) && (position[0] > 0.0) && (((static_cast<real32_T>
           (position[0]) * rb_idx_0 + rb_idx_1) * static_cast<real32_T>
          (position[0]) + rb_idx_2) * static_cast<real32_T>(position[0]) +
         rb_idx_3 <= position[1]) && (position[1] <= ((static_cast<real32_T>
           (position[0]) * lb_idx_0 + lb_idx_1) * static_cast<real32_T>
          (position[0]) + lb_idx_2) * static_cast<real32_T>(position[0]) +
         lb_idx_3)) {
      idx = position[0];
      b_0 = static_cast<real_T>(b_n) + 1.0;
    }
  }

  if (b_0 > 0.0) {
    for (c_i = 0; c_i < 6; c_i++) {
      mioState[c_i] = localB->MultiObjectTracker.Tracks[static_cast<int32_T>(b_0)
        - 1].State[c_i];
    }
  } else {
    for (b_n = 0; b_n < 6; b_n++) {
      mioState[b_n] = (rtInf);
    }
  }

  *rty_RelativeDistance = mioState[0];
  *rty_RelativeVelocity = mioState[1];
  *rty_MIOTrackIndex = b_0;

  // End of MATLAB Function: '<S3>/Find Lead Car'
}

// Termination for atomic system: '<Root>/Tracking and Sensor Fusion'
void PathFollowingControllerRefMdlModelClass::TrackingandSensorFusion_Term
  (DW_TrackingandSensorFusion_LFRefMdl_T *localDW)
{
  c_matlabshared_tracking_internal_fusion_AssignerGNN_LFRefMdl_T *obj;

  // Terminate for MATLABSystem: '<S3>/MATLAB System'
  if (!localDW->obj_d.matlabCodegenIsDeleted) {
    localDW->obj_d.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S3>/MATLAB System'

  // Terminate for MATLABSystem: '<S3>/Detection Concatenation1'
  if (!localDW->obj_m.matlabCodegenIsDeleted) {
    localDW->obj_m.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S3>/Detection Concatenation1'

  // Terminate for MATLABSystem: '<S3>/Multi-Object Tracker'
  if (!localDW->obj.matlabCodegenIsDeleted) {
    localDW->obj.matlabCodegenIsDeleted = true;
    if ((localDW->obj.isInitialized == 1) && localDW->obj.isSetupComplete) {
      localDW->obj.pNumLiveTracks = 0.0;
      std::memset(&localDW->obj.pTrackIDs[0], 0, 100U * sizeof(uint32_T));
      std::memset(&localDW->obj.pConfirmedTracks[0], 0, 100U * sizeof(boolean_T));
      localDW->obj.pLastTrackID = 0U;
      localDW->obj.pLastTimeStamp = -2.2204460492503131E-16;
      obj = localDW->obj.cAssigner;
      if (obj->isInitialized == 1) {
        obj->isInitialized = 2;
      }
    }
  }

  // End of Terminate for MATLABSystem: '<S3>/Multi-Object Tracker'
}

//
// File trailer for generated code.
//
// [EOF]
//
