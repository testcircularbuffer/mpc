//
// File: PathFollowingControlSystem.h
//
// Code generated for Simulink model 'LFRefMdl'.
//
// Model version                  : 1.753
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Sat Sep  4 19:34:05 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: Intel->x86-64 (Windows64)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_PathFollowingControlSystem_h_
#define RTW_HEADER_PathFollowingControlSystem_h_
#include <cmath>
#include <cstring>
#include <math.h>
#include "rtwtypes.h"
#include "LFRefMdl_types.h"
#include "rt_nonfinite.h"
#include "rt_assert.h"

// Block signals for system '<S2>/Path Following Control System'
typedef struct {
  real_T c_Hv[1320];
  real_T Su[800];
  real_T db[594];
  real_T Cm[572];
  real_T c_Sx[520];
} B_PathFollowingControlSystem_LFRefMdl_T;

// Block states (default storage) for system '<S2>/Path Following Control System' 
typedef struct {
  real_T last_mv_DSTATE[2];            // '<S20>/last_mv'
  real_T LastPcov_PreviousInput[169];  // '<S20>/LastPcov'
  real_T last_x_PreviousInput[13];     // '<S20>/last_x'
  boolean_T Memory_PreviousInput[18];  // '<S20>/Memory'
} DW_PathFollowingControlSystem_LFRefMdl_T;

// Invariant block signals for system '<S2>/Path Following Control System'
typedef const struct tag_ConstB_PathFollowingControlSystem_LFRefMdl_T {
  real_T Switch2;                      // '<S12>/Switch2'
  real_T MathFunction[4];              // '<S20>/Math Function'
  real_T MathFunction1[2];             // '<S20>/Math Function1'
  real_T MathFunction2[2];             // '<S20>/Math Function2'
  real_T utau;                         // '<S55>/Divide'
  real_T firstcolumn[4];               // '<S53>/Matrix Concatenate5'
  real_T Divide;                       // '<S54>/Divide'
  real_T b1;                           // '<S54>/Gain'
  real_T Divide1;                      // '<S54>/Divide1'
  real_T b2;                           // '<S54>/Gain1'
  real_T secondcolumn[4];              // '<S53>/Matrix Concatenate6'
  real_T MatrixConcatenate1[8];        // '<S53>/Matrix Concatenate1'
  real_T firstcolumn_c[6];             // '<S53>/Matrix Concatenate7'
  real_T secondcolumn_b[6];            // '<S53>/Matrix Concatenate8'
  real_T MatrixConcatenate2[12];       // '<S53>/Matrix Concatenate2'
  real_T a1;                           // '<S55>/Unary Minus'
  real_T An[4];                        // '<S55>/Matrix Concatenate'
  real_T firstcolumn_k[8];             // '<S53>/Matrix Concatenate3'
  real_T Divide6;                      // '<S54>/Divide6'
  real_T Divide7;                      // '<S54>/Divide7'
  real_T Sum;                          // '<S54>/Sum'
  real_T Gain2;                        // '<S54>/Gain2'
  real_T Product;                      // '<S54>/Product'
  real_T Product1;                     // '<S54>/Product1'
  real_T Sum1;                         // '<S54>/Sum1'
  real_T Gain3;                        // '<S54>/Gain3'
  real_T Sum3;                         // '<S54>/Sum3'
  real_T Gain4;                        // '<S54>/Gain4'
} ConstB_PathFollowingControlSystem_LFRefMdl_T;

#endif                              // RTW_HEADER_PathFollowingControlSystem_h_

//
// File trailer for generated code.
//
// [EOF]
//
