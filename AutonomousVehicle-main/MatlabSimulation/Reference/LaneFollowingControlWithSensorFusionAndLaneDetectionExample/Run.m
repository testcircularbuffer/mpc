close all 
bdclose all

%% Load parameter
helperLFSetUp();

%% Open Test Bench Model
% Open the Simulink test bench model.
open_system('LaneFollowingTestBenchExample')

%%
% Plot the road and the path that the ego vehicle.
plot(scenario)

%%
% Simulate the model to the end of the scenario.
sim('LaneFollowingTestBenchExample')

%%
% Plot the controller performance.
plotLFResults(logsout,time_gap,default_spacing)
