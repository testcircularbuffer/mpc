function md = getCurvature(Vx,time)
% Get previewed curvature from desired X and Y positions for LKA
%
% Inputs:
%   Vx: longitudinal velocity
%   time: time vector
%
% Outputs:
%   md: previewed curvature

%%
% % Desired X position
% Xref = Vx*time;
% % Desired Y position
% z1 = (2.4/50)*(Xref-27.19)-1.2;
% z2 = (2.4/43.9)*(Xref-56.46)-1.2;
% Yref = 8.1/2*(1+tanh(z1)) - 11.4/2*(1+tanh(z2));
% % Desired curvature
% DX = gradient(Xref,0.1);
% DY = gradient(Yref,0.1);
% D2Y = gradient(DY,0.1);
% curvature = DX.*D2Y./(DX.^2+DY.^2).^(3/2);

%%
Y_Target_1 = 10; % 5;
Y_Target_2 = -20; % -10;

T_Change_1 = 30;
T_Change_2 = 80;

T_Total_1 = 20;
T_Total_2 = 20;

% Desired X position
Xref = Vx*time;

% Desired Y position
z1 = 4/T_Total_1 * (Xref-T_Change_1);
z2 = 4/T_Total_2 * (Xref-T_Change_2);
Yref = Y_Target_1*(1+tanh(z1)) + Y_Target_2*(1+tanh(z2));

% Desired curvature
DX = gradient(Xref,0.1);
DY = gradient(Yref,0.1);
D2Y = gradient(DY,0.1);
curvature = DX.*D2Y./(DX.^2+DY.^2).^(3/2);

%%
% Stored curvature (as input for LKA)
md.time = time;
md.signals.values = curvature';
