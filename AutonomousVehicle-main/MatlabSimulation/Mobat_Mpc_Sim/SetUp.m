% Set up Script for the Simulation
%
% This script initializes the model. It loads
% necessary control constants and sets up the buses required for the
% referenced model.

%% General Simulation Parameters
Ts = 0.1; % Simulation sample time  (s)
simStopTime = 40; % Define a simulation stop time

%% Path following Controller Parameters
max_ac          = 2;     % Maximum acceleration   (m/s^2)
min_ac          = -15;   % Minimum acceleration   (m/s^2)
max_steer       = 0.26;  % Maximum steering       (rad)
min_steer       = -0.26; % Minimum steering       (rad) 
PredictionHorizon = 10;  % Prediction horizon     
ControlHorizon = 5;      % Control horizon

% *************Controller behavior*****************
% Tuning weight for longitudinal velocity tracking. 
% To produce smaller velocity-tracking errors, increase this weight.
WeightOnVelocityTracking = 0.4; 
% Tuning weight for lateral error. 
% To produce smaller lateral errors, increase this weight.
WeightOnLateralError = 0.2; 
% Tuning weight for changes in longitudinal acceleration. 
% To produce less-aggressive vehicle acceleration, increase this weight.
WeightOnChangeLongAcc = 0.01;
% Tuning weight for changes in steering angle. 
% To produce less-aggressive steering angle changes, increase this weight.
WeightOnChangeSteering = 0.1; % Weight on change of steering angle

%% Safe Following Distance control parameters
% Safe following distance constraint: Dr = Ds + Gt*Ve
% Ds is the Default spacing parameter.
% Gt is the Time gap input signal.
% Ve is the Longitudinal velocity input signal.
% Safe time gap in seconds between the lead vehicle and the ego vehicle. 
% This time gap is used to calculate the minimum safe following distance constraint.
time_gap     = 0.5;    % time gap            (s)
% Minimum spacing in meters between the lead vehicle and the ego vehicle.
% This value corresponds to the target relative distance between the ego and lead vehicles 
% when the ego vehicle velocity is zero.
safe_spacing = 10;     % default spacing     (m)

%% Ego Car Parameters
% Dynamics modeling parameters
m       = 900;      % Total mass of vehicle                          (kg)
Iz      = 2875;     % Yaw moment of inertia of vehicle               (m*N*s^2)
lf      = 1.2;      % Longitudinal distance from c.g. to front tires (m)
lr      = 1.6;      % Longitudinal distance from c.g. to rear tires  (m)
Cf      = 19000;    % Cornering stiffness of front tires             (N/rad)
Cr      = 33000;    % Cornering stiffness of rear tires              (N/rad)
tau     = 0.5;      % time constant for longitudinal dynamics 1/s/(tau*s+1)

%% Create driving scenario
v_set    = 5;       % set speed (m/s)
% Initial condition for the ego car in ISO 8855 coordinates
v0_ego   = 4;       % Initial speed of the ego car           (m/s)
x0_ego   = 0;       % Initial x position of ego car          (m)
y0_ego   = 0;       % Initial y position of ego car          (m)
yaw0_ego = 0;       % Initial yaw angle of ego car           (rad)
% Convert ISO 8855 to SAE J670E coordinates
y0_ego = -y0_ego;
yaw0_ego = -yaw0_ego;

%% Obtain the previewed curvature.
time = 0:Ts:simStopTime;
md = getCurvature(v_set,time);

%% Simulation vehicle 1 scenario
x1_init   = 30;          % Initial x position of v1                (m)
y1_init   = 0;           % Initial y position of v1                (m)
v1_init   = 0;           % Initial Longitudinal velocity of v1     (m/s)
v1_max    = 4;           % Maximum Longitudinal velocity of v1     (m/s)

%% Simulation vehicle 2 scenario, vehical 2 follows vehical 1
x2_init   = 15;          % Initial x position of v2                (m)
y2_init   = 0;           % Initial y position of v2                (m)
v2_init   = 0;           % Initial Longitudinal velocity of v2     (m/s)
v2_set    = 5;           % Normal Longitudinal velocity of v2      (m/s)
v2_max    = 6;           % Maximum Longitudinal velocity of v2     (m/s)
