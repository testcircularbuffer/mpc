% close all 
% bdclose all

%% Load parameter
SetUp();

%% Open Test Bench Model
% Open the Simulink test bench model.
open_system('LaneFollowingTestBenchExample')

%%
% Simulate the model to the end of the scenario.
sim('LaneFollowingTestBenchExample')
