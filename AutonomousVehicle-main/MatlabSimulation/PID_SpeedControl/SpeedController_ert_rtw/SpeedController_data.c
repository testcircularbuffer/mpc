/*
 * File: SpeedController_data.c
 *
 * Code generated for Simulink model 'SpeedController'.
 *
 * Model version                  : 10.5
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Wed Mar  2 00:45:47 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "SpeedController.h"
#include "SpeedController_private.h"

/* Block parameters (default storage) */
Parameters_SpeedController SpeedController_P = {
  /* Mask Parameter: SpeedController_InitialConditio
   * Referenced by: '<S28>/Filter'
   */
  0.0,

  /* Mask Parameter: SpeedController_InitialCondit_d
   * Referenced by: '<S33>/Integrator'
   */
  0.0,

  /* Mask Parameter: SpeedController_LowerSaturation
   * Referenced by: '<S40>/Saturation'
   */
  0.0,

  /* Mask Parameter: SpeedController_UpperSaturation
   * Referenced by: '<S40>/Saturation'
   */
  100.0,

  /* Computed Parameter: Integrator_gainval
   * Referenced by: '<S33>/Integrator'
   */
  0.02,

  /* Computed Parameter: Filter_gainval
   * Referenced by: '<S28>/Filter'
   */
  0.02
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
