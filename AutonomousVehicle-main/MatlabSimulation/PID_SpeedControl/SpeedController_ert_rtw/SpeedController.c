/*
 * File: SpeedController.c
 *
 * Code generated for Simulink model 'SpeedController'.
 *
 * Model version                  : 10.5
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Wed Mar  2 00:45:47 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "SpeedController.h"
#include "SpeedController_private.h"

/* Block states (default storage) */
D_Work_SpeedController SpeedController_DWork;

/* External inputs (root inport signals with default storage) */
ExternalInputs_SpeedController SpeedController_U;

/* External outputs (root outports fed by signals with default storage) */
ExternalOutputs_SpeedController SpeedController_Y;

/* Real-time model */
static RT_MODEL_SpeedController SpeedController_M_;
RT_MODEL_SpeedController *const SpeedController_M = &SpeedController_M_;

/* Model step function */
void SpeedController_step(void)
{
  real_T rtb_Add;
  real_T rtb_NProdOut;
  real_T u0;

  /* Sum: '<Root>/Add' incorporates:
   *  Inport: '<Root>/Feedback'
   *  Inport: '<Root>/Setpoint'
   */
  rtb_Add = SpeedController_U.Setpoint - SpeedController_U.Feedback;

  /* Product: '<S36>/NProd Out' incorporates:
   *  DiscreteIntegrator: '<S28>/Filter'
   *  Inport: '<Root>/Par_D'
   *  Inport: '<Root>/Par_N'
   *  Product: '<S27>/DProd Out'
   *  Sum: '<S28>/SumD'
   */
  rtb_NProdOut = (rtb_Add * SpeedController_U.Par_D -
                  SpeedController_DWork.Filter_DSTATE) * SpeedController_U.Par_N;

  /* Sum: '<S42>/Sum' incorporates:
   *  DiscreteIntegrator: '<S33>/Integrator'
   *  Inport: '<Root>/Par_P'
   *  Product: '<S38>/PProd Out'
   */
  u0 = (rtb_Add * SpeedController_U.Par_P +
        SpeedController_DWork.Integrator_DSTATE) + rtb_NProdOut;

  /* Saturate: '<S40>/Saturation' */
  if (u0 > SpeedController_P.SpeedController_UpperSaturation) {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal =
      SpeedController_P.SpeedController_UpperSaturation;
  } else if (u0 < SpeedController_P.SpeedController_LowerSaturation) {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal =
      SpeedController_P.SpeedController_LowerSaturation;
  } else {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal = u0;
  }

  /* End of Saturate: '<S40>/Saturation' */

  /* Update for DiscreteIntegrator: '<S33>/Integrator' incorporates:
   *  Inport: '<Root>/Par_I'
   *  Product: '<S30>/IProd Out'
   */
  SpeedController_DWork.Integrator_DSTATE += rtb_Add * SpeedController_U.Par_I *
    SpeedController_P.Integrator_gainval;

  /* Update for DiscreteIntegrator: '<S28>/Filter' */
  SpeedController_DWork.Filter_DSTATE += SpeedController_P.Filter_gainval *
    rtb_NProdOut;
}

/* Model initialize function */
void SpeedController_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(SpeedController_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&SpeedController_DWork, 0,
                sizeof(D_Work_SpeedController));

  /* external inputs */
  (void)memset(&SpeedController_U, 0, sizeof(ExternalInputs_SpeedController));

  /* external outputs */
  SpeedController_Y.CtrlSignal = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S33>/Integrator' */
  SpeedController_DWork.Integrator_DSTATE =
    SpeedController_P.SpeedController_InitialCondit_d;

  /* InitializeConditions for DiscreteIntegrator: '<S28>/Filter' */
  SpeedController_DWork.Filter_DSTATE =
    SpeedController_P.SpeedController_InitialConditio;
}

/* Model terminate function */
void SpeedController_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
