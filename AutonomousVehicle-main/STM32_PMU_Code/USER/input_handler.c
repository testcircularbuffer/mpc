/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "input_handler.h"

/******************************************************************************
* Define
******************************************************************************/

/******************************************************************************
* Local Function Declarations
******************************************************************************/

/******************************************************************************
* Global Variables
******************************************************************************/
bool input_pin_state[INPUT_NUM];
bool can_control_state[OUPUT_NUM];
bool can_control_mode = false;
bool can_control_mode_new_req = false;

static CanRxMsg RxMessage;
static uint16_t input_pin_on_cnt[INPUT_NUM];
static uint16_t input_pin_off_cnt[INPUT_NUM];

const uint16_t input_pin_name[INPUT_NUM] =
{
    GPIO_Pin_5,
    GPIO_Pin_6,
    GPIO_Pin_7
};
/******************************************************************************
* Global Function Declarations
******************************************************************************/
/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void init_input_data(void)
{
    uint8_t inx = 0;
    for (inx = 0; inx < INPUT_NUM; inx++)
    {
        input_pin_state[inx] = false;
        input_pin_on_cnt[inx] = 0;
        input_pin_off_cnt[inx] = 0;
    }

    for (inx = 0; inx < OUPUT_NUM; inx++)
    {
        can_control_state[inx] = false;
    }
    
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void input_pin_handler(void)
{
    uint8_t pin_num = 0;
    for (pin_num = 0; pin_num < INPUT_NUM; pin_num++)
    {
        if (Bit_SET == GPIO_ReadInputDataBit(INPUT_PORT, input_pin_name[pin_num]) )
        {
            input_pin_off_cnt[pin_num] = 0;

            if (input_pin_state[pin_num] != true)
            {
                input_pin_on_cnt[pin_num]++;

                if (input_pin_on_cnt[pin_num] >= PIN_ON_CNT_THRES)
                {
                    input_pin_on_cnt[pin_num] = 0;
                    input_pin_state[pin_num] = true;
                }
            }
        }
        else
        {
            input_pin_on_cnt[pin_num] = 0;

            if (input_pin_state[pin_num] != false)
            {
                input_pin_off_cnt[pin_num]++;

                if (input_pin_off_cnt[pin_num] >= PIN_OFF_CNT_THRES)
                {
                    input_pin_off_cnt[pin_num] = 0;
                    input_pin_state[pin_num] = false;
                }
            }  
        }
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
    if (RxMessage.StdId == ID_PDU_CONTROL)
    {
        if (RxMessage.Data[0] == 1)
        {   
            can_control_mode = true;
            Led_on();
        }
        else if ((RxMessage.Data[0] == 2) && (can_control_mode == true))
        {
            can_control_mode_new_req = true;
            if (RxMessage.Data[1] < OUPUT_NUM)
            {
                if (RxMessage.Data[2] != 0)
                {
                    can_control_state[RxMessage.Data[1]] = true;
                }
                else
                {
                    can_control_state[RxMessage.Data[1]] = false;
                }
            }
        }
        else
        {
            can_control_mode = false;
            Led_off();
        }
    }
}

