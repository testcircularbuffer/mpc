/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "output_handler.h"

/******************************************************************************
* Define
******************************************************************************/

/******************************************************************************
* Local Function Declarations
******************************************************************************/
static void power_control_btn(void);
static void power_control_can(void);

/******************************************************************************
* Global Variables
******************************************************************************/
static bool backup_input_pin_state[Input_End];
/******************************************************************************
* Global Function Declarations
******************************************************************************/
/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void init_output_data(void)
{
    uint8_t inx = 0;
    for (inx = 0; inx < Input_End; inx++)
    {
        backup_input_pin_state[inx] = false;
    }

	Disable_Fans();
    Disable_AutoMode();
    Disable_DriverBrake();
    Disable_DriverSteering();
    Disable_MTN();
    Disable_STM32F4();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void output_pin_handler(void)
{
    if (can_control_mode == true)
    {
        if (can_control_mode_new_req == true)
        {
            power_control_can();
            can_control_mode_new_req = false;
        }
    }
    else
    {
        power_control_btn();
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void power_control_btn(void)
{
    if (backup_input_pin_state[Input_Button_1] != input_pin_state[Input_Button_1])
    {
        if (input_pin_state[Input_Button_1] == true)
        {
            Enable_AutoMode();
            can_control_state[Output_AutoMode] = true;
        }
        else
        {
            Disable_AutoMode();
            can_control_state[Output_AutoMode] = false;
        }
				
				output_can_handler();

        backup_input_pin_state[Input_Button_1] = input_pin_state[Input_Button_1];
    }

    if (backup_input_pin_state[Input_Button_2] != input_pin_state[Input_Button_2])
    {
        if (input_pin_state[Input_Button_2] == true)
        {
            Enable_MTN();
            can_control_state[Output_MTN] = true;

            Enable_STM32F4();
            can_control_state[Output_STM32F4] = true;
        }
        else
        {
            Disable_MTN();
            can_control_state[Output_MTN] = false;

            Disable_STM32F4();
            can_control_state[Output_STM32F4] = false;
        }

        backup_input_pin_state[Input_Button_2] = input_pin_state[Input_Button_2];
    }

    if (backup_input_pin_state[Input_Button_3] != input_pin_state[Input_Button_3])
    {
        if (input_pin_state[Input_Button_3] == true)
        {
            Enable_DriverBrake();
			can_control_state[Output_DriverBrake] = true;
            Enable_DriverSteering();
            can_control_state[Output_DriverSteering] = true;
			Enable_Fans();
			can_control_state[Output_Fans] = true;
        }
        else
        {
            Disable_DriverBrake();
            can_control_state[Output_DriverBrake] = false;

            Disable_DriverSteering();
            can_control_state[Output_DriverSteering] = false;
					
            Disable_Fans();
            can_control_state[Output_Fans] = false;
        }

        backup_input_pin_state[Input_Button_3] = input_pin_state[Input_Button_3];
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void power_control_can(void)
{
    if (can_control_state[Output_DriverBrake] == true)
    {
        Enable_DriverBrake();
    }
    else
    {
        Disable_DriverSteering();
    }

    if (can_control_state[Output_DriverSteering] == true)
    {
        Enable_DriverSteering();
    }
    else
    {
        Disable_DriverSteering();
    }

    if (can_control_state[Output_STM32F4] == true)
    {
        Enable_STM32F4();
    }
    else
    {
        Disable_STM32F4();
    }

    if (can_control_state[Output_Fans] == true)
    {
        Enable_Fans();
    }
    else
    {
        Disable_Fans();
    }

    if (can_control_state[Output_AutoMode] == true)
    {
        Enable_AutoMode();
    }
    else
    {
        Disable_AutoMode();
    }


}


/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void output_can_handler(void)
{
    u8 can_send_data[8];
    can_send_data[0] = input_pin_state[Input_Button_1];
    can_send_data[1] = input_pin_state[Input_Button_2];
    can_send_data[2] = input_pin_state[Input_Button_3];
    can_send_data[3] = 0;
    can_send_data[4] = 0;
    can_send_data[5] = 0;
    can_send_data[6] = 0;
    can_send_data[7] = 0;
    user_CAN_Transmit(ID_PDU_STATUS, 8u, can_send_data);
}
