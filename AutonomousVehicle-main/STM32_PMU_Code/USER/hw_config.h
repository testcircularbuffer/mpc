/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef HW_PIN_CFG_H
#define HW_PIN_CFG_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"

/*******************************************************************************
* Definitions 
*******************************************************************************/
typedef enum {
    Input_Button_1 = 0, /* Auto/Manual */
    Input_Button_2, /* Embeded */
    Input_Button_3, /* Power stage */
    Input_End,
} Input_pin_t;

typedef enum {
    Output_DriverBrake = 0,
    Output_DriverSteering,
    Output_MTN,
    Output_STM32F4,
    Output_Fans,
    Output_AutoMode,
    Output_End,
} Output_pin_t;

/******************************************************************************
* Macro Definitions
******************************************************************************/
#define ID_PDU_CONTROL (Device_ID_PMU | Function_ID_PowerControl)
#define ID_PDU_STATUS (Device_ID_PMU | Function_ID_PowerButtonStatus)

#define Led_on() GPIO_ResetBits(GPIOC ,GPIO_Pin_13)
#define Led_off() GPIO_SetBits(GPIOC ,GPIO_Pin_13)

#define Enable_DriverBrake() GPIO_ResetBits(GPIOB ,GPIO_Pin_12)
#define Disable_DriverBrake() GPIO_SetBits(GPIOB ,GPIO_Pin_12)

#define Enable_DriverSteering() GPIO_ResetBits(GPIOB ,GPIO_Pin_13)
#define Disable_DriverSteering() GPIO_SetBits(GPIOB ,GPIO_Pin_13)

#define Enable_MTN() GPIO_ResetBits(GPIOB ,GPIO_Pin_14)
#define Disable_MTN() GPIO_SetBits(GPIOB ,GPIO_Pin_14)

#define Enable_STM32F4() GPIO_ResetBits(GPIOA ,GPIO_Pin_8)
#define Disable_STM32F4() GPIO_SetBits(GPIOA ,GPIO_Pin_8)

#define Enable_Fans() GPIO_ResetBits(GPIOA ,GPIO_Pin_12)
#define Disable_Fans() GPIO_SetBits(GPIOA ,GPIO_Pin_12)

#define Enable_AutoMode() GPIO_ResetBits(GPIOA ,GPIO_Pin_1)
#define Disable_AutoMode() GPIO_SetBits(GPIOA ,GPIO_Pin_1)

#define INPUT_PORT (GPIOB)

#define GetState_buton1() GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_5)
#define GetState_buton2() GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_6)
#define GetState_buton3() GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_7)


/** *********************************   DEFINE PIN   **************************************/
// #define 	GPIO_ChannelA_Pin			GPIO_Pin_6

/******************************************************************************
* Externs Definitions
******************************************************************************/
// #define TurnON_Led(Color)	GPIO_SetBits(GPIOD, Color)

// extern void delay_ms(u32 ms);
// extern void delay_us(u32 us);

void digital_pin_config(void);
void init_adc(void);
void init_can(void);

uint8_t user_CAN_Transmit(int _IDstd, int _length, uint8_t _data[]);

#endif /* HW_PIN_CFG_H */
