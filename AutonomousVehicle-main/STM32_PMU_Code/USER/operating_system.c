/* Includes ------------------------------------------------------------------*/
#include "operating_system.h"

uint32_t	SysTick_5ms_count;
boolean_t	SysTick_5ms_flag;

static void Cyclic1xBaseMain_1(void);
static void Cyclic2xBaseMain(void);
static void Cyclic4xBaseMain(void);
static void Cyclic8xBaseMain(void);
static void Cyclic1xBaseMain_2(void);

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void SysTick_Handler(void)
{
	static uint8_t tSysTimeSliceCnt_u8 = 0u;
	SysTick_5ms_flag = TRUE;
	SysTick_5ms_count++;
	tSysTimeSliceCnt_u8++;

	/* call 5ms function */
	Cyclic1xBaseMain_1();

	if (tSysTimeSliceCnt_u8 == 1u)
	{
		/* call 40ms slice function */
		Cyclic8xBaseMain();
	}

	if ((tSysTimeSliceCnt_u8 % 2u) == 0u)
	{
		/* call 10ms slice function */
		Cyclic2xBaseMain();
	}

	if ((tSysTimeSliceCnt_u8 == 3u) || (tSysTimeSliceCnt_u8 == 7u))
	{
		/* call 20ms slice function */
		Cyclic4xBaseMain();
	}

	if (tSysTimeSliceCnt_u8 == 8u )
	{
		tSysTimeSliceCnt_u8 = 0u;
	}
	
	/* call 5ms  function */
	Cyclic1xBaseMain_2();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void system_config(void)
{
	digital_pin_config();
	init_input_data();
	init_output_data();
	init_adc();
	init_can();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void Cyclic1xBaseMain_1(void)
{
	/* 5ms */
	input_pin_handler();
	output_pin_handler();
}

static void Cyclic2xBaseMain(void)
{
	/* 10ms */
}

static void Cyclic4xBaseMain(void)
{
	/* 20ms */
	//output_can_handler();
}

static void Cyclic8xBaseMain(void)
{
	/* 40ms */
}

static void Cyclic1xBaseMain_2(void)
{
	/* 5ms */
	
}
