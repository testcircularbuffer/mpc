/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"

/*******************************************************************************
* Definitions 
*******************************************************************************/
#define PIN_ON_CNT_THRES (8u)
#define PIN_OFF_CNT_THRES (8u)

#define INPUT_NUM (3u)
#define OUPUT_NUM (6u)

/******************************************************************************
* Macro Definitions
******************************************************************************/

/******************************************************************************
* Externs Definitions
******************************************************************************/
extern bool input_pin_state[INPUT_NUM];
extern bool can_control_state[OUPUT_NUM];
extern bool can_control_mode;
extern bool can_control_mode_new_req;

void init_input_data(void);
void input_pin_handler(void);

#endif /* INPUT_HANDLER_H */
