/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef VEHICLE_DATA_H
#define VEHICLE_DATA_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"

/*******************************************************************************
* Definitions 
*******************************************************************************/

/*******************************************************************************/
/* Device ID */
#define Device_ID_ECU 0x0200
#define Device_ID_PMU 0x0300

/* Function ID MCU */
#define Function_ID_VehControl 0x0001
#define Function_ID_VehStatus 0x0002

/* Function ID PMU */
#define Function_ID_PowerControl 0x0001
#define Function_ID_PowerButtonStatus 0x0002

/*******************************************************************************/
/* Moving direction of the vehicle */
typedef enum
{
	forward = 0,
	backward,
	vehstop,
}veh_dir;
typedef enum
{
	dir_forward = 0,
	dir_backward,
}moving_dir;

/* switch_state ON or OFF*/
typedef enum
{
	state_off = 0,
	state_on,
}switch_state;

/* turn_signal LEFT/RIGHT */
typedef enum
{
	turn_off = 0,
	turn_right,
	turn_left,
}turn_signal;

/*******************************************************************************/
/* Vehicle control data */
typedef struct
{
	s8 target_speed;
	u8 driving_mode;
	u8 turn_signal : 2;
	u8 horn : 1;
}veh_control_st;

/* Vehicle status data */
typedef struct
{
	s8 feedback_speed;
	u8 accelerator_level;
	u8 accelerator_switch : 1;
	u8 break_switch : 1;
	u8 moving_direction : 1;
	u8 turn_signal : 2;
	u8 horn : 1;
}veh_state_st;

/*******************************************************************************/
/* CAN Vehicle Control data extract message */
typedef union
{
	u8 can_data[8];
	veh_control_st signal;
}veh_control_un;

/* CAN Vehicle Status data extract message */
typedef union
{
	u8 can_data[8];
	veh_state_st signal;
}veh_state_un;

/*******************************************************************************/
typedef struct
{
	s8 target_speed;
	s8 actual_speed;
	u16 current_dac_val;
	u8 target_breaking_percentage;
	u8 actual_breaking_percentage;
}vehdata_speed;

typedef struct
{
	veh_dir dir;
}vehdata_dir;

/******************************************************************************
* Macro Definitions
******************************************************************************/

/******************************************************************************
* Externs Definitions
******************************************************************************/
extern vehdata_speed		vehdata_speed_data;
extern vehdata_dir			vehdata_dir_data;

extern veh_control_un		veh_control_data;
extern veh_state_un			veh_state_data;

extern void vehicle_data_init(void);

#endif /* VEHICLE_DATA_H */
