/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm_base.h"
#include "hw_config.h"
#include "input_handler.h"
#include "output_handler.h"
#include "vehicle_data.h"

#define USE_STM32F407
// #define USE_STM32F411

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __MAIN_H */
