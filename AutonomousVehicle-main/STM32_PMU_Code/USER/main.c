#include "main.h"                
#include "operating_system.h"
/*******************************************************************************
* Declaration 
*******************************************************************************/

/*******************************************************************************
* Definitions 
*******************************************************************************/

/*
 * The example "main" function illustrates what is required by your
 * application code to initialize, execute, and terminate the generated code.
 * Attaching rt_OneStep to a real-time clock is target specific.  This example
 * illustates how you do this relative to initializing the model.
 */
int main(void)
{
	system_config();
	/* Enable SysTick at 5ms interrupt */
	SysTick_Config(SystemCoreClock/200);
	
	while(1)
	{
		if(SysTick_5ms_flag == TRUE) 
		{ 
			/* 5ms task here */	
			SysTick_5ms_flag = FALSE;
		}
	}	
}
