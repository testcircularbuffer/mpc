/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "comm_output_handle.h"
/******************************************************************************
* Local Variables
******************************************************************************/

/******************************************************************************
* Global Variables
******************************************************************************/

/******************************************************************************
* Global Function Declarations
******************************************************************************/

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void comm_output_handle_init(void)
{

}

//veh_state_un veh_state_can_data_test; 
void comm_output_handler(void)
{
	static u8 time_counter = 0;
	
	if (time_counter > 5)
	{
		time_counter = 0;
		// Update data before send
		veh_state_can_data.signal.feedback_speed = encoder_handler_GetVehSpeed(); // unit km/h
		veh_state_can_data.signal.turn_signal = (u8)turn_off;
		veh_state_can_data.signal.horn = (u8)turn_off;
		
		//veh_state_can_data = veh_state_can_data_test;
		
		// Send out data via CAN
		user_CAN1_Transmit((Device_ID_ECU|Function_ID_VehStatus), 8u, veh_state_can_data.can_data);
	}
	else
	{
		time_counter ++;
	}
}
