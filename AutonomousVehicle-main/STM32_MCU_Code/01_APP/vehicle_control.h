/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef VEHICLE_CONTROL_H
#define VEHICLE_CONTROL_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"
/*******************************************************************************
* Definitions 
*******************************************************************************/

#define Set_moving_forward()    {GPIO_ResetBits(GPIOD, GPIO_MovingDirectionSwitch); \
                                    veh_state_can_data.signal.moving_direction = (u8)dir_forward;  }
#define Set_moving_backward()   {GPIO_SetBits(GPIOD, GPIO_MovingDirectionSwitch); \
                                    veh_state_can_data.signal.moving_direction = (u8)dir_backward;}

#define Set_moving_start()      {GPIO_ResetBits(GPIOD, GPIO_AccelerationSwitch); \
                                    veh_state_can_data.signal.accelerator_switch = 1;}
#define Set_moving_stop()       {GPIO_SetBits(GPIOD, GPIO_AccelerationSwitch); \
                                    veh_state_can_data.signal.accelerator_switch = 0;}

#define Set_brake_acive()       {GPIO_SetBits(GPIOD, GPIO_BrakeSwitch); \
                                    veh_state_can_data.signal.break_switch = (u8)state_on;}
#define Set_brake_inacive()     {GPIO_ResetBits(GPIOD, GPIO_BrakeSwitch); \
                                    veh_state_can_data.signal.break_switch = (u8)state_off;}
/******************************************************************************
* Macro Definitions
******************************************************************************/

/******************************************************************************
* Externs Definitions
******************************************************************************/
extern void vehicle_control_init(void);
extern void VehicleControl_handler(void);

#endif /* VEHICLE_CONTROL_H */
