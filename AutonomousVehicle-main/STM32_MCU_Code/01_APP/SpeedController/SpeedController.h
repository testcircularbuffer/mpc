/*
 * File: SpeedController.h
 *
 * Code generated for Simulink model 'SpeedController'.
 *
 * Model version                  : 10.5
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Wed Mar  2 00:45:47 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_SpeedController_h_
#define RTW_HEADER_SpeedController_h_
#include <string.h>
#include <stddef.h>
#ifndef SpeedController_COMMON_INCLUDES_
#define SpeedController_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* SpeedController_COMMON_INCLUDES_ */

#include "SpeedController_types.h"
#include "main.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Integrator_DSTATE;            /* '<S33>/Integrator' */
  real_T Filter_DSTATE;                /* '<S28>/Filter' */
} D_Work_SpeedController;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T Setpoint;                     /* '<Root>/Setpoint' */
  real_T Feedback;                     /* '<Root>/Feedback' */
  real_T Par_P;                        /* '<Root>/Par_P' */
  real_T Par_I;                        /* '<Root>/Par_I' */
  real_T Par_D;                        /* '<Root>/Par_D' */
  real_T Par_N;                        /* '<Root>/Par_N' */
} ExternalInputs_SpeedController;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T CtrlSignal;                   /* '<Root>/CtrlSignal' */
} ExternalOutputs_SpeedController;

/* Parameters (default storage) */
struct Parameters_SpeedController_ {
  real_T SpeedController_InitialConditio;
                              /* Mask Parameter: SpeedController_InitialConditio
                               * Referenced by: '<S28>/Filter'
                               */
  real_T SpeedController_InitialCondit_d;
                              /* Mask Parameter: SpeedController_InitialCondit_d
                               * Referenced by: '<S33>/Integrator'
                               */
  real_T SpeedController_LowerSaturation;
                              /* Mask Parameter: SpeedController_LowerSaturation
                               * Referenced by: '<S40>/Saturation'
                               */
  real_T SpeedController_UpperSaturation;
                              /* Mask Parameter: SpeedController_UpperSaturation
                               * Referenced by: '<S40>/Saturation'
                               */
  real_T Integrator_gainval;           /* Computed Parameter: Integrator_gainval
                                        * Referenced by: '<S33>/Integrator'
                                        */
  real_T Filter_gainval;               /* Computed Parameter: Filter_gainval
                                        * Referenced by: '<S28>/Filter'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_SpeedController {
  const char_T * volatile errorStatus;
};

/* Block parameters (default storage) */
extern Parameters_SpeedController SpeedController_P;

/* Block states (default storage) */
extern D_Work_SpeedController SpeedController_DWork;

/* External inputs (root inport signals with default storage) */
extern ExternalInputs_SpeedController SpeedController_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExternalOutputs_SpeedController SpeedController_Y;

/* Real-time Model object */
extern RT_MODEL_SpeedController *const SpeedController_M;

/* Model entry point functions */
extern void SpeedController_initialize(void);
extern void SpeedController_step(void);
extern void SpeedController_terminate(void);
extern void SpeedController_OneStep(void);
extern void SpeedController_Start(void);
extern void SpeedController_Stop(void);
extern void SpeedController_SetTargetDeceleration(u8 percentage_0_100);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'SpeedController'
 * '<S1>'   : 'SpeedController/SpeedController'
 * '<S2>'   : 'SpeedController/SpeedController/Anti-windup'
 * '<S3>'   : 'SpeedController/SpeedController/D Gain'
 * '<S4>'   : 'SpeedController/SpeedController/Filter'
 * '<S5>'   : 'SpeedController/SpeedController/Filter ICs'
 * '<S6>'   : 'SpeedController/SpeedController/I Gain'
 * '<S7>'   : 'SpeedController/SpeedController/Ideal P Gain'
 * '<S8>'   : 'SpeedController/SpeedController/Ideal P Gain Fdbk'
 * '<S9>'   : 'SpeedController/SpeedController/Integrator'
 * '<S10>'  : 'SpeedController/SpeedController/Integrator ICs'
 * '<S11>'  : 'SpeedController/SpeedController/N Copy'
 * '<S12>'  : 'SpeedController/SpeedController/N Gain'
 * '<S13>'  : 'SpeedController/SpeedController/P Copy'
 * '<S14>'  : 'SpeedController/SpeedController/Parallel P Gain'
 * '<S15>'  : 'SpeedController/SpeedController/Reset Signal'
 * '<S16>'  : 'SpeedController/SpeedController/Saturation'
 * '<S17>'  : 'SpeedController/SpeedController/Saturation Fdbk'
 * '<S18>'  : 'SpeedController/SpeedController/Sum'
 * '<S19>'  : 'SpeedController/SpeedController/Sum Fdbk'
 * '<S20>'  : 'SpeedController/SpeedController/Tracking Mode'
 * '<S21>'  : 'SpeedController/SpeedController/Tracking Mode Sum'
 * '<S22>'  : 'SpeedController/SpeedController/Tsamp - Integral'
 * '<S23>'  : 'SpeedController/SpeedController/Tsamp - Ngain'
 * '<S24>'  : 'SpeedController/SpeedController/postSat Signal'
 * '<S25>'  : 'SpeedController/SpeedController/preSat Signal'
 * '<S26>'  : 'SpeedController/SpeedController/Anti-windup/Passthrough'
 * '<S27>'  : 'SpeedController/SpeedController/D Gain/External Parameters'
 * '<S28>'  : 'SpeedController/SpeedController/Filter/Disc. Forward Euler Filter'
 * '<S29>'  : 'SpeedController/SpeedController/Filter ICs/Internal IC - Filter'
 * '<S30>'  : 'SpeedController/SpeedController/I Gain/External Parameters'
 * '<S31>'  : 'SpeedController/SpeedController/Ideal P Gain/Passthrough'
 * '<S32>'  : 'SpeedController/SpeedController/Ideal P Gain Fdbk/Disabled'
 * '<S33>'  : 'SpeedController/SpeedController/Integrator/Discrete'
 * '<S34>'  : 'SpeedController/SpeedController/Integrator ICs/Internal IC'
 * '<S35>'  : 'SpeedController/SpeedController/N Copy/Disabled'
 * '<S36>'  : 'SpeedController/SpeedController/N Gain/External Parameters'
 * '<S37>'  : 'SpeedController/SpeedController/P Copy/Disabled'
 * '<S38>'  : 'SpeedController/SpeedController/Parallel P Gain/External Parameters'
 * '<S39>'  : 'SpeedController/SpeedController/Reset Signal/Disabled'
 * '<S40>'  : 'SpeedController/SpeedController/Saturation/Enabled'
 * '<S41>'  : 'SpeedController/SpeedController/Saturation Fdbk/Disabled'
 * '<S42>'  : 'SpeedController/SpeedController/Sum/Sum_PID'
 * '<S43>'  : 'SpeedController/SpeedController/Sum Fdbk/Disabled'
 * '<S44>'  : 'SpeedController/SpeedController/Tracking Mode/Disabled'
 * '<S45>'  : 'SpeedController/SpeedController/Tracking Mode Sum/Passthrough'
 * '<S46>'  : 'SpeedController/SpeedController/Tsamp - Integral/Passthrough'
 * '<S47>'  : 'SpeedController/SpeedController/Tsamp - Ngain/Passthrough'
 * '<S48>'  : 'SpeedController/SpeedController/postSat Signal/Forward_Path'
 * '<S49>'  : 'SpeedController/SpeedController/preSat Signal/Forward_Path'
 */
#endif                                 /* RTW_HEADER_SpeedController_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
