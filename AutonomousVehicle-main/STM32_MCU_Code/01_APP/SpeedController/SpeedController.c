/*
 * File: SpeedController.c
 *
 * Code generated for Simulink model 'SpeedController'.
 *
 * Model version                  : 10.5
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Wed Mar  2 00:45:47 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "SpeedController.h"
#include "SpeedController_private.h"

/* Block states (default storage) */
D_Work_SpeedController SpeedController_DWork;

/* External inputs (root inport signals with default storage) */
ExternalInputs_SpeedController SpeedController_U;

/* External outputs (root outports fed by signals with default storage) */
ExternalOutputs_SpeedController SpeedController_Y;

/* Real-time model */
static RT_MODEL_SpeedController SpeedController_M_;
RT_MODEL_SpeedController *const SpeedController_M = &SpeedController_M_;

/*
 * Associating rt_OneStep with a real-time clock or interrupt service routine
 * is what makes the generated code "real-time".  The function rt_OneStep is
 * always associated with the base rate of the model.  Subrates are managed
 * by the base rate from inside the generated code.  Enabling/disabling
 * interrupts and floating point context switches are target specific.  This
 * example code indicates where these should take place relative to executing
 * the generated code step function.  Overrun behavior should be tailored to
 * your application needs.  This example simply sets an error status in the
 * real-time model and returns from rt_OneStep.
 */
void rt_OneStep(void);

/* Model step function */
void SpeedController_step(void)
{
  real_T rtb_Add;
  real_T rtb_NProdOut;
  real_T u0;

  /* Sum: '<Root>/Add' incorporates:
   *  Inport: '<Root>/Feedback'
   *  Inport: '<Root>/Setpoint'
   */
  rtb_Add = SpeedController_U.Setpoint - SpeedController_U.Feedback;

  /* Product: '<S36>/NProd Out' incorporates:
   *  DiscreteIntegrator: '<S28>/Filter'
   *  Inport: '<Root>/Par_D'
   *  Inport: '<Root>/Par_N'
   *  Product: '<S27>/DProd Out'
   *  Sum: '<S28>/SumD'
   */
  rtb_NProdOut = (rtb_Add * SpeedController_U.Par_D -
                  SpeedController_DWork.Filter_DSTATE) * SpeedController_U.Par_N;

  /* Sum: '<S42>/Sum' incorporates:
   *  DiscreteIntegrator: '<S33>/Integrator'
   *  Inport: '<Root>/Par_P'
   *  Product: '<S38>/PProd Out'
   */
  u0 = (rtb_Add * SpeedController_U.Par_P +
        SpeedController_DWork.Integrator_DSTATE) + rtb_NProdOut;

  /* Saturate: '<S40>/Saturation' */
  if (u0 > SpeedController_P.SpeedController_UpperSaturation) {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal =
      SpeedController_P.SpeedController_UpperSaturation;
  } else if (u0 < SpeedController_P.SpeedController_LowerSaturation) {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal =
      SpeedController_P.SpeedController_LowerSaturation;
  } else {
    /* Outport: '<Root>/CtrlSignal' */
    SpeedController_Y.CtrlSignal = u0;
  }

  /* End of Saturate: '<S40>/Saturation' */

  /* Update for DiscreteIntegrator: '<S33>/Integrator' incorporates:
   *  Inport: '<Root>/Par_I'
   *  Product: '<S30>/IProd Out'
   */
  SpeedController_DWork.Integrator_DSTATE += rtb_Add * SpeedController_U.Par_I *
    SpeedController_P.Integrator_gainval;

  /* Update for DiscreteIntegrator: '<S28>/Filter' */
  SpeedController_DWork.Filter_DSTATE += SpeedController_P.Filter_gainval *
    rtb_NProdOut;
}

// static volatile double Par_P = 20.0;
// static volatile double Par_I = 4.0;
// static volatile double Par_D = 0.2;

static volatile double Par_P = 20.0;
static volatile double Par_I = 4.0;
static volatile double Par_D = 0.2;
/* Model initialize function */
void SpeedController_initialize(void)
{
  /* Registration code */

  /* initialize error status */
  rtmSetErrorStatus(SpeedController_M, (NULL));

  /* states (dwork) */
  (void) memset((void *)&SpeedController_DWork, 0,
                sizeof(D_Work_SpeedController));

  /* external inputs */
  (void)memset(&SpeedController_U, 0, sizeof(ExternalInputs_SpeedController));

  SpeedController_U.Par_P = Par_P;
  SpeedController_U.Par_I = Par_I;
  SpeedController_U.Par_D = Par_D;
  SpeedController_U.Par_N = 0.0;

  /* external outputs */
  SpeedController_Y.CtrlSignal = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S33>/Integrator' */
  SpeedController_DWork.Integrator_DSTATE =
    SpeedController_P.SpeedController_InitialCondit_d;

  /* InitializeConditions for DiscreteIntegrator: '<S28>/Filter' */
  SpeedController_DWork.Filter_DSTATE =
    SpeedController_P.SpeedController_InitialConditio;
}

/* Model terminate function */
void SpeedController_terminate(void)
{
  /* (no terminate code required) */
}

void rt_OneStep(void)
{
  static boolean_T OverrunFlag = false;

  /* Disable interrupts here */

  /* Check for overrun */
  if (OverrunFlag) {
    rtmSetErrorStatus(SpeedController_M, "Overrun");
    return;
  }

  OverrunFlag = true;

  /* Save FPU context here (if necessary) */
  /* Re-enable timer or interrupt here */
  /* Set model inputs here */

  /* Step the model */
  SpeedController_step();

  /* Get model outputs here */

  /* Indicate task complete */
  OverrunFlag = false;

  /* Disable interrupts here */
  /* Restore FPU context here (if necessary) */
  /* Enable interrupts here */
}


static bool Controller_Ena = false;
void SpeedController_Start(void)
{
  Controller_Ena = true;
}

void SpeedController_Stop(void)
{
  Controller_Ena = false;
  vehdata_speed_data.current_dac_val = 0u;
	veh_state_can_data.signal.accelerator_level = 0u;
  DAC_SetChannel1Data(DAC_Align_12b_R, vehdata_speed_data.current_dac_val);
  SpeedController_initialize();
}

static u8 TargetDeceleration = 100u;
void SpeedController_SetTargetDeceleration(u8 percentage_0_100)
{
  if (percentage_0_100 <= 100)
  {
    TargetDeceleration = percentage_0_100;
  }
  
}

void SpeedController_OneStep(void)
{
  
  vehdata_speed_data.actual_speed = encoder_handler_GetVehSpeed();

  if (Controller_Ena)
  {
    if (vehdata_speed_data.target_speed >= 0)
    {
      SpeedController_U.Feedback = (real_T)encoder_handler_GetVehSpeed();
      SpeedController_U.Setpoint = (real_T)vehdata_speed_data.target_speed * (real_T)TargetDeceleration / 100.0;
    }
    else
    {
      SpeedController_U.Feedback = -(real_T)encoder_handler_GetVehSpeed();
      SpeedController_U.Setpoint = -(real_T)vehdata_speed_data.target_speed * (real_T)TargetDeceleration / 100.0;
    }
    

    rt_OneStep();
    
    if (vehdata_speed_data.target_speed == 0)
    {
      vehdata_speed_data.current_dac_val = 0;
    }
    else
    {
      vehdata_speed_data.current_dac_val = (u16)(SpeedController_Y.CtrlSignal * 4095.0 / 100.0);
    }
    
		veh_state_can_data.signal.accelerator_level = (u8)SpeedController_Y.CtrlSignal;
    DAC_SetChannel1Data(DAC_Align_12b_R, vehdata_speed_data.current_dac_val);
  }
  else
  {
    vehdata_speed_data.current_dac_val = 0u;
		veh_state_can_data.signal.accelerator_level = 0u;
    DAC_SetChannel1Data(DAC_Align_12b_R, vehdata_speed_data.current_dac_val);
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
