/*
 * File: SpeedController_types.h
 *
 * Code generated for Simulink model 'SpeedController'.
 *
 * Model version                  : 10.5
 * Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
 * C/C++ source code generated on : Wed Mar  2 00:45:47 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_SpeedController_types_h_
#define RTW_HEADER_SpeedController_types_h_
#include "rtwtypes.h"

/* Model Code Variants */

/* Parameters (default storage) */
typedef struct Parameters_SpeedController_ Parameters_SpeedController;

/* Forward declaration for rtModel */
typedef struct tag_RTM_SpeedController RT_MODEL_SpeedController;

#endif                                 /* RTW_HEADER_SpeedController_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
