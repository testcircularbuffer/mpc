#ifndef SYSTEM_DEFINE_H
#define SYSTEM_DEFINE_H

typedef enum
{
  NoError = 0,

  __ErrorLevel_1_Start,
  SpeedControlError,
  ObjectDetectionNoise,
  __ErrorLevel_1_End,

  __ErrorLevel_2_Start,
  Radar_CommError,
  ECU_CommError,
  LaneDetc_CommError,
  ObjDetc_CommError,
  GUI_CommError,
  __ErrorLevel_2_End,

  __ErrorLevel_3_Start,
  CurvatureOutRange,
  LaneFollowingError,
  __ErrorLevel_3_End,

} SystemMonitor_ErrorInfo;

typedef enum
{
  StopMode = 1,
  NormalDriving,
  LaneChange,
  Deceleration,
  EmergecyStop,
} DrivingModeSel_Mode;

#endif
