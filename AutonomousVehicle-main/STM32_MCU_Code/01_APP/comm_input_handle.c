/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "comm_input_handle.h"
/******************************************************************************
* Local Variables
******************************************************************************/

/******************************************************************************
* Global Variables
******************************************************************************/
veh_control_un veh_control_backup;

u8 can_output_ctrl_value[can_output_ctrl_End];
bool can_control_mode = false;
bool can_control_mode_new_req = false;
bool vehicle_mode_auto_flag = false; 
/* Read input pin */
bool input_pin_state[INPUT_NUM];
static uint16_t input_pin_on_cnt[INPUT_NUM];
static uint16_t input_pin_off_cnt[INPUT_NUM];

const uint16_t input_pin_name[INPUT_NUM] =
{
    GPIO_Pin_11,
};
/******************************************************************************
* Global Function Declarations
******************************************************************************/

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void comm_input_handle_init(void)
{
	u8 i = 0;
	for (i = 0; i < 8; i++)
	{
		veh_control_backup.can_data[i] = 0;
	}

	for (i = 0; i < can_output_ctrl_End; i++)
	{
		can_output_ctrl_value[i] = 0;
	}
}

void comm_input_handler(void)
{
	uint8_t pin_num = 0;
    for (pin_num = 0; pin_num < INPUT_NUM; pin_num++)
    {
        if (Bit_SET == GPIO_ReadInputDataBit(INPUT_PORT, input_pin_name[pin_num]) )
        {
            input_pin_off_cnt[pin_num] = 0;

            if (input_pin_state[pin_num] != true)
            {
                input_pin_on_cnt[pin_num]++;

                if (input_pin_on_cnt[pin_num] >= PIN_ON_CNT_THRES)
                {
                    input_pin_on_cnt[pin_num] = 0;
                    input_pin_state[pin_num] = true;
                }
            }
        }
        else
        {
            input_pin_on_cnt[pin_num] = 0;

            if (input_pin_state[pin_num] != false)
            {
                input_pin_off_cnt[pin_num]++;

                if (input_pin_off_cnt[pin_num] >= PIN_OFF_CNT_THRES)
                {
                    input_pin_off_cnt[pin_num] = 0;
                    input_pin_state[pin_num] = false;
                }
            }  
        }
    }

	// If target speed changed
	if (veh_control_can_data.signal.target_speed != veh_control_backup.signal.target_speed)
	{
		vehdata_speed_data.target_speed = veh_control_can_data.signal.target_speed;
	}
	
	// If target driving mode changed
	if (veh_control_can_data.signal.driving_mode != veh_control_backup.signal.driving_mode)
	{
		vehdata_status_data.veh_mode = veh_control_can_data.signal.driving_mode;
	}

	// If target turn_signal changed
	if (veh_control_can_data.signal.turn_signal != veh_control_backup.signal.turn_signal)
	{
		
	}

	// If target horn changed
	if (veh_control_can_data.signal.horn != veh_control_backup.signal.horn)
	{
		
	}
	
	/* Backup signal for next compare */
	veh_control_backup = veh_control_can_data;
}

void CAN1_RX0_IRQHandler(void)
{
	u8 i = 0;
	if (CAN_GetITStatus(CAN1,CAN_IT_FMP0) != RESET)
	{
		CAN_Receive(CAN1, CAN_FIFO0, &CAN1_RxMessage);

		if(CAN1_RxMessage.StdId == (Device_ID_ECU | Function_ID_VehControl))
		{
			for (i = 0; i < 8; i ++)
			{
				veh_control_can_data.can_data[i] = CAN1_RxMessage.Data[i];
			}
			
			Toggle_Led(Color_Green);
			
		}
		else if (CAN1_RxMessage.StdId == (Device_ID_ECU | Function_ID_VehTest))
		{
			if (CAN1_RxMessage.Data[0] == 1)
			{   
				can_control_mode = true;
			}
			else if ((CAN1_RxMessage.Data[0] == 2) && (can_control_mode == true))
			{
				can_control_mode_new_req = true;
				if (CAN1_RxMessage.Data[1] < can_output_ctrl_End)
				{
					can_output_ctrl_value[CAN1_RxMessage.Data[1]] = CAN1_RxMessage.Data[2];
				}
			}
			else
			{
				can_control_mode = false;
			}
		}
		else if (CAN1_RxMessage.StdId == ID_PMU_STATUS)
		{
			if (CAN1_RxMessage.Data[0] == 0)
			{
				vehicle_mode_auto_flag = false;
			}
			else
			{
				vehicle_mode_auto_flag = true;
			}

			
		}

		CAN_ClearITPendingBit(CAN1,CAN_IT_FMP0);
	}
}
