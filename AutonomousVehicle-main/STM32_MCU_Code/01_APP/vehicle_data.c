/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "vehicle_data.h"
/******************************************************************************
* Local Variables
******************************************************************************/

/******************************************************************************
* Global Variables
******************************************************************************/
vehdata_speed_st vehdata_speed_data;
vehdata_status_st vehdata_status_data;

veh_control_un veh_control_can_data;
veh_state_un veh_state_can_data;

/******************************************************************************
* Global Function Declarations
******************************************************************************/

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void vehicle_data_init(void)
{
	u8 i = 0;
	
	vehdata_speed_data.actual_speed = 0.0f;
	vehdata_speed_data.current_dac_val = 0;
	vehdata_speed_data.target_speed = 0;
	vehdata_status_data.dir = dir_forward;
	vehdata_status_data.veh_mode = StopMode;
	
	for (i = 0; i < 8; i++)
	{
		veh_control_can_data.can_data[i] = 0;
		veh_state_can_data.can_data[i] = 0;
	}
	
}
