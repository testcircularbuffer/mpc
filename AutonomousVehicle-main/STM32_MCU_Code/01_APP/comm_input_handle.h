/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef COMM_INPUT_HANDLE_H
#define COMM_INPUT_HANDLE_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"
/*******************************************************************************
* Definitions 
*******************************************************************************/

typedef enum {
    can_output_ctrl_speed = 0, /* control speed */
    can_output_ctrl_CTHT_StartStop, /* CTHT Start/Stop (PD13) */
    can_output_ctrl_CTHT_FWBW, /* CTHT FW/BW (PD15) */
    can_output_ctrl_PD12, /*  */
    can_output_ctrl_PD14,
    can_output_ctrl_End,
} can_output_ctrl_t;

typedef enum {
    BreakSwitch = 0, /* Auto/Manual */
    Input_End,
} Input_pin_t;

#define PIN_ON_CNT_THRES (8u)
#define PIN_OFF_CNT_THRES (8u)

#define INPUT_PORT (GPIOD)
#define INPUT_NUM (1u)
/******************************************************************************
* Macro Definitions
******************************************************************************/

/******************************************************************************
* Externs Definitions
******************************************************************************/
extern bool input_pin_state[INPUT_NUM];
extern u8 can_output_ctrl_value[can_output_ctrl_End];
extern bool can_control_mode;
extern bool can_control_mode_new_req;
extern bool vehicle_mode_auto_flag;

extern void comm_input_handle_init(void);
extern void comm_input_handler(void);

#endif /* COMM_INPUT_HANDLE_H */
