/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "vehicle_control.h"
/******************************************************************************
* Local Variables
******************************************************************************/
static bool backup_input_pin_state[Input_End];
static bool dir_change = false;

/******************************************************************************
* Global Variables
******************************************************************************/

/******************************************************************************
* Local Function Declarations
******************************************************************************/
static void VehicleControl_Test(void);
static void VehicleControl_Normal(void);
static bool VehicleControl_DirChangeCheck(void);
static void VehicleControl_BreakCheck(void);

static void VehicleControl_ModeCheck(void);
static void VehicleControl_StopMode(void);
static void VehicleControl_NormalDriving(void);
static void VehicleControl_LaneChange(void);
static void VehicleControl_Deceleration(void);
static void VehicleControl_EmergecyStop(void);

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void vehicle_control_init(void)
{
	Set_moving_stop();
	Set_moving_backward();
	SpeedController_Start();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void VehicleControl_handler(void)
{
	if (can_control_mode == true)
    {
        if (can_control_mode_new_req == true)
        {
            VehicleControl_Test();
            can_control_mode_new_req = false;
        }
    }
	else if (vehicle_mode_auto_flag == false)
	{
		Set_moving_stop();
		Set_moving_backward();
		SpeedController_Stop();
		VehicleControl_BreakCheck();
	}
    else
    {
        VehicleControl_Normal();
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void VehicleControl_Test(void)
{
	if (can_output_ctrl_value[can_output_ctrl_speed] <= 25)
    {
        DAC_SetChannel1Data(DAC_Align_12b_R, can_output_ctrl_value[can_output_ctrl_speed] * 163);
    }

	if (can_output_ctrl_value[can_output_ctrl_CTHT_StartStop] != 0)
	{
		Set_moving_start();
	}
	else
	{
		Set_moving_stop();
	}

	if (can_output_ctrl_value[can_output_ctrl_CTHT_FWBW] != 0)
	{
		Set_moving_forward();
	}
	else
	{
		Set_moving_backward();
	}

	if (can_output_ctrl_value[can_output_ctrl_PD12] != 0)
	{
		GPIO_SetBits(GPIOD, GPIO_Pin_12);
	}
	else
	{
		GPIO_ResetBits(GPIOD, GPIO_Pin_12);
	}

	if (can_output_ctrl_value[can_output_ctrl_PD14] != 0)
	{
		GPIO_SetBits(GPIOD, GPIO_Pin_14);
	}
	else
	{
		GPIO_ResetBits(GPIOD, GPIO_Pin_14);
	}

}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static bool VehicleControl_DirChangeCheck(void)
{
	if (dir_change == false)
	{
		if ((vehdata_speed_data.target_speed > 0)  && (vehdata_status_data.dir == dir_backward))
		{
			dir_change = true;
			vehdata_status_data.dir = dir_forward;
			SpeedController_Stop();
		}
		else if ((vehdata_speed_data.target_speed < 0)  && (vehdata_status_data.dir == dir_forward))
		{
			dir_change = true;
			vehdata_status_data.dir = dir_backward;
			SpeedController_Stop();
		}
		else
		{
			//vehdata_status_data.dir = dir_forward;
		}
	}
	else
	{
		if ((encoder_handler_GetVehSpeed() >= -0.1f) && ((encoder_handler_GetVehSpeed() <= 0.1f) ))
		{
			dir_change = false;
			SpeedController_Start();
			//Set_moving_start();
		}
	}

	return dir_change;
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void VehicleControl_Normal(void)
{
	if (true == VehicleControl_DirChangeCheck())
	{
		/* wait for dir change finished */
		Set_moving_stop();
	}
	else
	{
		Set_moving_start();
	}

	if ((vehdata_speed_data.target_speed > 0) && (vehdata_status_data.dir == dir_forward))
	{
		Set_moving_forward();
	}
	else if ((vehdata_speed_data.target_speed < 0)  && (vehdata_status_data.dir == dir_backward))
	{
		Set_moving_backward();
	}
	
	VehicleControl_ModeCheck();

}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void VehicleControl_BreakCheck(void)
{
	if (backup_input_pin_state[BreakSwitch] != input_pin_state[BreakSwitch])
    {
        if (input_pin_state[BreakSwitch] == true)
        {
			Set_brake_inacive();
        }
        else
        {
            /* NOTE: Break active at low level input pin */
			Set_brake_acive();
        }

        backup_input_pin_state[BreakSwitch] = input_pin_state[BreakSwitch];
    }
}
/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void VehicleControl_ModeCheck(void)
{
	
	VehicleControl_BreakCheck();

	if ((veh_state_can_data.signal.break_switch == (u8)state_on) || (vehdata_speed_data.target_speed == 0))
	{
		Set_moving_forward(); 
		vehdata_status_data.dir = dir_forward;
		Set_moving_stop();
		SpeedController_Stop();
		vehdata_status_data.veh_mode = StopMode;
	}
	else
	{
		vehdata_status_data.veh_mode = veh_control_can_data.signal.driving_mode;

		switch (vehdata_status_data.veh_mode)
		{
		case StopMode:
			VehicleControl_StopMode();
			break;
		case NormalDriving:
			VehicleControl_NormalDriving();
			break;
		case LaneChange:
			VehicleControl_LaneChange();
			break;
		case Deceleration:
			VehicleControl_Deceleration();
			break;
		case EmergecyStop:
			VehicleControl_EmergecyStop();
			break;
		
		default:
			break;
		}
	}

	//veh_mode_backup = vehdata_status_data.veh_mode;
}

static void VehicleControl_StopMode(void)
{
	SpeedController_Stop();
	Set_moving_forward();
	Set_moving_stop();
}

static void VehicleControl_NormalDriving(void)
{
	SpeedController_SetTargetDeceleration(100);
	SpeedController_Start();
	Set_moving_start();
}

static void VehicleControl_LaneChange(void)
{
	/* Do nothing for now */
}

static void VehicleControl_Deceleration(void)
{
	SpeedController_SetTargetDeceleration(50);
}

static void VehicleControl_EmergecyStop(void)
{
	SpeedController_Stop();
	Set_moving_stop();
}
