/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "motion_profile.h"

/******************************************************************************
* Local Variables
******************************************************************************/
typedef struct
{
    boolean_t speedramp_start;
    boolean_t speedramp_running;
    boolean_t speedramp_stop;
    float speedramp_time_s;
    float speedramp_time_s_actual;
}mp_speed_ramp_status;

typedef struct
{
    boolean_t braking_start;
    boolean_t braking_running;
    boolean_t braking_stop;
    float braking_time_s;
    float braking_time_s_actual;
}mp_breaking_status;

mp_speed_ramp_status mp_speed_ramp_status_data;
mp_breaking_status mp_breaking_status_data;
/******************************************************************************
* Global Variables
******************************************************************************/
motion_status motion_status_data;

/******************************************************************************
* Function Declarations
******************************************************************************/
// static void MP_RampUp_100msTask(void);
// static void MP_Braking_100msTask(u8 breking_time);

/******************************************************************************
* Function Definition 
******************************************************************************/

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void TIM4_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
    {
        // switch (motion_status_data)
        // {
        // case speed_ramp:
        //     //Toggle_Led(Color_Orange);
        //     MP_RampUp_100msTask();
        //     break;
				
		// case braking:
        //     MP_Braking_100msTask(232);
        //     break;
        
        // case motionstop:
        //     /* Nothing to do */
        //     //TurnOFF_Led(Color_Orange);
        //     break;

        // default:
        //     //TurnOFF_Led(Color_Orange);
        //     break;
        // }
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void mp_set_target_speed(float speedramp_time_s, s8 target_speed)
{
    motion_status_data = speed_ramp;

    mp_speed_ramp_status_data.speedramp_start = TRUE;
    mp_speed_ramp_status_data.speedramp_running = FALSE;
    mp_speed_ramp_status_data.speedramp_stop = FALSE;
    mp_speed_ramp_status_data.speedramp_time_s = speedramp_time_s;

    vehdata_speed_data.target_speed = target_speed;
}

// /******************************************************************************
// * Description: 
// * Parameter: 
// * Return Value: None
// * Author: Huy Le
// ******************************************************************************/
// static void MP_RampUp_100msTask(void)
// {
//     static float par_a1 = 0.0, par_b1 = 0.0;
//     static float ramp_target_dac_val = 0.0;

//     if (mp_speed_ramp_status_data.speedramp_start == TRUE)
//     {
//         mp_speed_ramp_status_data.speedramp_time_s_actual = 0;

//         if (vehdata_speed_data.target_speed > 0.0)
//         {
//             vehdata_status_data.dir = dir_forward;
//             /* Set pin direction of vehicle here */
//         }
//         else if (vehdata_speed_data.target_speed < 0.0)
//         {
//             vehdata_status_data.dir = dir_backward;
//             /* Set pin direction of vehicle here */
//         }
//         else
//         {
//             vehdata_status_data.dir = dir_stop;
//             /* Set pin direction of vehicle here */
//         }
        
//         ramp_target_dac_val = (float)vehdata_speed_data.target_speed * 4095 / Vehicle_Max_Speed;
//         if (ramp_target_dac_val < 0)
//         {
//             ramp_target_dac_val = -ramp_target_dac_val;
//         }
        
        
//         par_a1 = (ramp_target_dac_val - vehdata_speed_data.current_dac_val) / (mp_speed_ramp_status_data.speedramp_time_s);
//         par_b1 = ramp_target_dac_val - par_a1 * mp_speed_ramp_status_data.speedramp_time_s;

//         mp_speed_ramp_status_data.speedramp_start = FALSE;
//         mp_speed_ramp_status_data.speedramp_running = TRUE;
//     }


//     if ((mp_speed_ramp_status_data.speedramp_time_s_actual < (mp_speed_ramp_status_data.speedramp_time_s - (float)0.1)) && (mp_speed_ramp_status_data.speedramp_running == TRUE))
//     {
//         mp_speed_ramp_status_data.speedramp_time_s_actual += (float)0.1;

//         vehdata_speed_data.current_dac_val = (u16)(par_a1 * mp_speed_ramp_status_data.speedramp_time_s_actual + par_b1);

//         if (vehdata_speed_data.current_dac_val > 4095u)
//         {
//             vehdata_speed_data.current_dac_val = 4095u;
//         }
//         else if (vehdata_speed_data.current_dac_val == 0u)
//         {
//             Set_moving_stop();
//         }

//         DAC_SetChannel1Data(DAC_Align_12b_R, vehdata_speed_data.current_dac_val);
//     }
//     else
//     {
//         mp_speed_ramp_status_data.speedramp_running = FALSE;
//         mp_speed_ramp_status_data.speedramp_stop = TRUE;
//         motion_status_data = motionstop;
//     }
// }

// /******************************************************************************
// * Description: 
// * Parameter: 
// * Return Value: None
// * Author: Huy Le
// ******************************************************************************/
// void mp_set_braking(float braking_time_s, u8 breaking_percentage)
// {
//     motion_status_data = braking;

//     mp_breaking_status_data.braking_start = TRUE;
//     mp_breaking_status_data.braking_running = FALSE;
//     mp_breaking_status_data.braking_stop = FALSE;
//     mp_breaking_status_data.braking_time_s = braking_time_s;

//     vehdata_speed_data.target_breaking_percentage = breaking_percentage;
// }

// /******************************************************************************
// * Description: 
// * Parameter: 
// * Return Value: None
// * Author: Huy Le
// ******************************************************************************/
// static void MP_Braking_100msTask(u8 breking_time)
// {
//     static float par_a2 = 0.0, par_b2 = 0.0;
//     static float target_motor_step = 0.0;
//     static float actual_motor_step = 0.0;

//     if (mp_breaking_status_data.braking_start == TRUE)
//     {
//         mp_breaking_status_data.braking_time_s_actual = 0;
        
//         target_motor_step = (float)vehdata_speed_data.target_breaking_percentage * Vehicle_Max_Braking_Step_Motor / 100;
        
//         par_a2 = (target_motor_step - actual_motor_step) / (mp_breaking_status_data.braking_time_s);
//         par_b2 = target_motor_step - par_a2 * mp_breaking_status_data.braking_time_s;

//         mp_breaking_status_data.braking_start = FALSE;
//         mp_breaking_status_data.braking_running = TRUE;
//     }


//     if ((mp_breaking_status_data.braking_time_s_actual < (mp_breaking_status_data.braking_time_s - (float)0.1)) && (mp_breaking_status_data.braking_running == TRUE))
//     {
//         mp_breaking_status_data.braking_time_s_actual += (float)0.1;

//         actual_motor_step = (u16)(par_a2 * mp_breaking_status_data.braking_time_s_actual + par_b2);

//         if (actual_motor_step > Vehicle_Max_Braking_Step_Motor)
//         {
//             actual_motor_step = Vehicle_Max_Braking_Step_Motor;
//         }

//         vehdata_speed_data.actual_breaking_percentage = (u8)(actual_motor_step / target_motor_step * 100);

//         /* Motor Variable */
//         #if 0
//         static u32 Freq = 0u;
//         static double Duty = 0.0;
//         static u16 time_second = 0u;
//         static boolean_t start_motor = FALSE, motor_staus = FALSE;
//         u16 main_tick = 0u;
//         #endif

//         #if 0
//         if (start_motor == TRUE && (motor_staus == FALSE))
//         {
//             main_tick = 0u;
//             motor_staus = TRUE;
//             PwmHdl_Cfg_SetDuty(PwmHdl_Channel_1, Duty);
//         }

//         if ((main_tick >= (time_second * 200u)) && (motor_staus == TRUE))
//         {
//             //TurnOFF_Led(Color_Red);
//             main_tick = 0u;
//             start_motor = FALSE;
//             motor_staus = FALSE;
//             PwmHdl_Cfg_SetDuty(PwmHdl_Channel_1, 0.0);
//         }
//         else if (motor_staus == TRUE)
//         {
//             main_tick++;
//             //TurnON_Led(Color_Red);
//         }
        
//         PwmHdl_Cfg_SetFrequency(Freq);
//         #endif

//     }
//     else
//     {
//         mp_breaking_status_data.braking_running = FALSE;
//         mp_breaking_status_data.braking_stop = TRUE;
//         motion_status_data = motionstop;
//     }
// }
