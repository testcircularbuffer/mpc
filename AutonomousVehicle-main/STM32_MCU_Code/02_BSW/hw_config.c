/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "hw_config.h"

/******************************************************************************
* Define
******************************************************************************/

#if defined (USE_STM32F407)
#define APB1_Clock 42 // 42 MHz
#endif /* USE_STM32F407 */

#if defined (USE_STM32F411)
#define APB1_Clock 42 // 42 MHz
#endif /* USE_STM32F411 */

/******************************************************************************
* Local Variables
******************************************************************************/
PwmHdl_ChannelRuntime_t PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_Max] =
{ /* Enable? Pulse? */
	{TRUE,   0u},
	{FALSE,  0u},
	{FALSE,  0u},
	{FALSE,  0u},
};

u32 PwmHdl_Period = PWM_1_InitPeriod;

/******************************************************************************
* Local Function Declarations
******************************************************************************/
static void TIM5_DelayTimer_Config(u32 us);

/******************************************************************************
* Global Variables
******************************************************************************/

/******************************************************************************
* Global Function Declarations
******************************************************************************/
/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void TIM5_DelayTimer_Config(u32 us)
{
    TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
    TIM_TimeBaseStructure.TIM_Prescaler = APB1_Clock-1;
    TIM_TimeBaseStructure.TIM_Period = us -1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);
    TIM_ITConfig(TIM5,TIM_FLAG_Update, ENABLE);
    TIM_ITConfig(TIM5,TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM5, ENABLE);
}	

void delay_ms(u32 ms)
{
    u32 us;
    us = ms*1000u;
    TIM5_DelayTimer_Config(us);
    TIM_ClearFlag(TIM5,TIM_FLAG_Update);
    while ( TIM_GetFlagStatus(TIM5,TIM_FLAG_Update )!= SET){;}
    TIM_ClearFlag(TIM5,TIM_FLAG_Update);
    TIM_Cmd(TIM5, DISABLE);
}

void delay_us(u32 us)
{
    TIM5_DelayTimer_Config(us);
    TIM_ClearFlag(TIM5,TIM_FLAG_Update);
    while ( TIM_GetFlagStatus(TIM5,TIM_FLAG_Update )!= SET){;}
    TIM_ClearFlag(TIM5,TIM_FLAG_Update);
    TIM_Cmd(TIM5, DISABLE);
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void digital_pin_config(void)
{
    GPIO_InitTypeDef  GPIO_InitStructureOutput;
    GPIO_InitStructureOutput.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructureOutput.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructureOutput.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructureOutput.GPIO_PuPd = GPIO_PuPd_NOPULL;

    /******************* Port CLock enable/disable ***************/
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

    /* BLUE_LED_PIN Config */
    GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_15;		
    GPIO_Init(GPIOD, &GPIO_InitStructureOutput);

    /* RED_LED_PIN Config */
    GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_14;
    GPIO_Init(GPIOD, &GPIO_InitStructureOutput);

    /* ORANGE_LED_PIN Config */   
    GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_13;
    GPIO_Init(GPIOD, &GPIO_InitStructureOutput);
        
    /* GREEN_LED_PIN Config */   
    GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_12;
    GPIO_Init(GPIOD, &GPIO_InitStructureOutput);

    // RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    // /* Acceleration Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_0;
    // GPIO_Init(GPIOA, &GPIO_InitStructureOutput);

    // /* Moving Direction Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_1;
    // GPIO_Init(GPIOA, &GPIO_InitStructureOutput);

    // /* Brake Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_2;
    // GPIO_Init(GPIOA, &GPIO_InitStructureOutput);

    // RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    // /* Acceleration Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_0;
    // GPIO_Init(GPIOC, &GPIO_InitStructureOutput);

    // /* Moving Direction Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_1;
    // GPIO_Init(GPIOC, &GPIO_InitStructureOutput);

    // /* Brake Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_2;
    // GPIO_Init(GPIOC, &GPIO_InitStructureOutput);

    // /* Brake Switch */   
    // GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_3;
    // GPIO_Init(GPIOC, &GPIO_InitStructureOutput);

    GPIO_InitStructureOutput.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructureOutput.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructureOutput.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructureOutput.GPIO_PuPd = GPIO_PuPd_UP;
    /* GREEN_LED_PIN Config */   
    GPIO_InitStructureOutput.GPIO_Pin = GPIO_Pin_11;
    GPIO_Init(GPIOD, &GPIO_InitStructureOutput);
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void encoder_pin_config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_ChannelA_Pin | GPIO_ChannelB_Pin;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA,&GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_ChannelA_PinSource, GPIO_AF_TIM3);
    GPIO_PinAFConfig(GPIOA, GPIO_ChannelB_PinSource, GPIO_AF_TIM3);

    // Config encoder interface
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 ,ENABLE);
    TIM_EncoderInterfaceConfig(TIMx_Encoder ,TIM_EncoderMode_TI12,TIM_ICPolarity_Rising,TIM_ICPolarity_Rising);
    TIM_SetAutoreload(TIMx_Encoder ,0xFFFF); //Reset value when counting
    TIM_SetCounter(TIMx_Encoder, 0);
    TIM_Cmd(TIMx_Encoder, ENABLE);
	
    // //**********ENCODER*********
    // /* TIM1 clock enable */
    // GPIO_InitTypeDef GPIO_InitStructure;
    // RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);	
    // RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);

    // /* TIM1 channel1,2 configuration */
    // GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9 | GPIO_Pin_11;
    // GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    // GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    // GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    // GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    // GPIO_Init(GPIOE, &GPIO_InitStructure);

    // GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_TIM1);
    // GPIO_PinAFConfig(GPIOE, GPIO_PinSource11, GPIO_AF_TIM1);

    // /* Initialise encoder interface */
    // TIM_EncoderInterfaceConfig(TIM1, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);

    // /* TIM enable counter */
    // TIM1->CNT = 0;
    // TIM_Cmd(TIM1, ENABLE);
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
int32_t Encoder_GetCnt(void)
{
    static int32_t 	current_cnt = 0, last_cnt = 0, diff_cnt = 0;
    static int32_t 	current_pulse = 0;
	
	current_cnt = (int32_t)TIMx_Encoder->CNT;
	diff_cnt = current_cnt - last_cnt;
	if (diff_cnt > 32768)
	{
        diff_cnt -= 65536;
    }
	else if (diff_cnt < -32768)
    {
        diff_cnt += 65536;
    }
	current_pulse += diff_cnt;
    last_cnt = current_cnt;
	return current_pulse;
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void dac_config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    DAC_InitTypeDef  DAC_InitStructure;

    /* GPIOA clock enable (to be used with DAC) */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);                         
    /* DAC Periph clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

    /* Once the DAC channelx is enabled, the corresponding GPIO pin (PA4 or PA5) is
    automatically connected to the analog converter output (DAC_OUTx). In order to avoid
    parasitic consumption, the PA4 or PA5 pin should first be configured to analog (AIN). */
    /* DAC channel 1 & 2 (DAC_OUT1 = PA.4)(DAC_OUT2 = PA.5) configuration */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* DAC channel1 Configuration */
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
    DAC_Init(DAC_Channel_1, &DAC_InitStructure);

    /* Enable DAC Channel_1 */
    DAC_Cmd(DAC_Channel_1, ENABLE);

    DAC_SetChannel1Data(DAC_Align_12b_R, 0u);
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void pwm_config(void)
{

    /* PWM Timer1 - 4 channel config */
    GPIO_InitTypeDef           GPIO_InitStructure;
    TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
    TIM_OCInitTypeDef          TIM_OCInitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_1_InitPrescaler;
    TIM_TimeBaseStructure.TIM_Period = PwmHdl_Period;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0u;
    TIM_TimeBaseInit(PWM_1_TIMx, &TIM_TimeBaseStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High; 
    TIM_OCInitStructure.TIM_Pulse = 0u; 

    if (PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_1].Channel_Enabled == TRUE)
    {
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
        GPIO_Init(GPIOE, &GPIO_InitStructure);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource9, GPIO_AF_TIM1);

        TIM_OC1Init(PWM_1_TIMx, &TIM_OCInitStructure);
        TIM_OC1PreloadConfig(PWM_1_TIMx, TIM_OCPreload_Enable);
    }
    if (PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_2].Channel_Enabled == TRUE)
    {
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
        GPIO_Init(GPIOE, &GPIO_InitStructure);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource11, GPIO_AF_TIM1);

        TIM_OC2Init(PWM_1_TIMx, &TIM_OCInitStructure);
        TIM_OC2PreloadConfig(PWM_1_TIMx, TIM_OCPreload_Enable);
    }
    if (PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_3].Channel_Enabled == TRUE)
    {
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
        GPIO_Init(GPIOE, &GPIO_InitStructure);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource13, GPIO_AF_TIM1);

        TIM_OC3Init(PWM_1_TIMx, &TIM_OCInitStructure);
        TIM_OC3PreloadConfig(PWM_1_TIMx, TIM_OCPreload_Enable);
    }
    if (PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_4].Channel_Enabled == TRUE)
    {
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
        GPIO_Init(GPIOE, &GPIO_InitStructure);
        GPIO_PinAFConfig(GPIOE, GPIO_PinSource14, GPIO_AF_TIM1);

        TIM_OC4Init(PWM_1_TIMx, &TIM_OCInitStructure);
        TIM_OC4PreloadConfig(PWM_1_TIMx, TIM_OCPreload_Enable);
    }

    /* PWM_1_TIMx enable counter */
    TIM_CtrlPWMOutputs(PWM_1_TIMx, ENABLE);
    TIM_Cmd(PWM_1_TIMx, ENABLE);

}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void PwmHdl_Cfg_SetDuty(PwmHdl_Channel_en PwmHdl_Channel, double Duty)
{
    u32 Pulse;

    Pulse = (u32)((double)PwmHdl_Period * Duty / 100.0);

    switch (PwmHdl_Channel)
    {
        case PwmHdl_Channel_1:
        {
            TIM_SetCompare1(PWM_1_TIMx, Pulse);
            PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_1].Pulse = Pulse;
            break;
        }
        case PwmHdl_Channel_2:
        {
            TIM_SetCompare2(PWM_1_TIMx, Pulse);
            PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_2].Pulse = Pulse;
            break;
        }
        case PwmHdl_Channel_3:
        {
            TIM_SetCompare3(PWM_1_TIMx, Pulse);
            PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_3].Pulse = Pulse;
            break;
        }
        case PwmHdl_Channel_4:
        {
            TIM_SetCompare4(PWM_1_TIMx, Pulse);
            PwmHdl_Module1_ChannelRuntime[(u8)PwmHdl_Channel_4].Pulse = Pulse;
            break;
        }    
        default:
            break;
    }
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void PwmHdl_Cfg_SetFrequency(u32 Freq)
{
    if (Freq == 0u)
    {
        PwmHdl_Period = PWM_1_InitPeriod;
    }
    else if (Freq <= 20000u)
    {
        /* Keep Org value */
        PwmHdl_Period = (100000u / Freq) - 1u;
    }
    else
    {
        Freq = 20000u;
        PwmHdl_Period = (100000u / Freq) - 1u;
    }

    TIM_SetAutoreload(PWM_1_TIMx, PwmHdl_Period);
}

/* TIM4 config for motin timer control */
void motion_timer_config(void)
{
    NVIC_InitTypeDef    NVIC_InitStructure;
    TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = ((SystemCoreClock/2u)/1000u -1u); // frequency = 1000 Hz
    TIM_TimeBaseStructure.TIM_Period = 100 - 1;/* so xung trong 1 chu ky = 100 --> 100ms interrupt */
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE); /*thiet lap ngat khi tran bo nho co thong so TIM_IT_Update*/
    TIM_Cmd(TIM4, ENABLE);

    /*Cau hinh ngat timer*/
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

