/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm_base.h"
#include "stm32f4xx.h"
#include "SystemDefine.h"
#include "operating_system.h"
#include "hw_config.h"
#include "encoder_handle.h"
#include "comm_input_handle.h"
#include "comm_output_handle.h"
#include "motion_profile.h"
#include "vehicle_data.h"
#include "vehicle_control.h"
#include "user_CAN.h"
#include "SpeedController.h"

#define USE_STM32F407
// #define USE_STM32F411

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __MAIN_H */
