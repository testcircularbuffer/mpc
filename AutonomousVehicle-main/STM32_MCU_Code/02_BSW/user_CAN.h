/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USER_CAN_H
#define __USER_CAN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported constants/macros --------------------------------------------------------*/
	 
extern CanTxMsg CAN1_TxMessage;
extern CanRxMsg CAN1_RxMessage;
extern uint8_t ubKeyNumber;

union Can_float
{
	float Can_Float_Value;
	struct
	{
		unsigned byte1:8;
		unsigned byte2:8;
		unsigned byte3:8;
		unsigned byte4:8;
	} byte;
};

/* Exported types ------------------------------------------------------------*/
#define ID_ECU_CONTROL (Device_ID_ECU | Function_ID_VehControl)
#define ID_ECU_STATUS  (Device_ID_ECU | Function_ID_VehStatus)
#define ID_ECU_VEHICLE_TEST (Device_ID_ECU | Function_ID_VehTest)
#define ID_PMU_STATUS (Device_ID_PMU | Function_ID_PowerButtonStatus)
/** **********   DEFINE USART1   ***************************************************
		* @brief   CAN define 
		*/
		
	 
/* Exported function prototypes -----------------------------------------------------*/
void user_CAN1_Config(void);
void user_CAN1_Transmit(uint16_t _id, uint8_t _length, uint8_t _data[]);
void user_CAN_TestFnc(void);
#ifdef __cplusplus
}
#endif

#endif 
