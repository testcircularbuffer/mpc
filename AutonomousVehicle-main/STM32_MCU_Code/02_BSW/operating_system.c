/* Includes ------------------------------------------------------------------*/
#include "operating_system.h"

uint32_t	SysTick_5ms_count;
boolean_t	SysTick_5ms_flag;

static void Cyclic1xBaseMain_1(void);
static void Cyclic2xBaseMain(void);
static void Cyclic4xBaseMain(void);
static void Cyclic8xBaseMain(void);
static void Cyclic1xBaseMain_2(void);

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void SysTick_Handler(void)
{
	static uint8_t tSysTimeSliceCnt_u8 = 0u;
	SysTick_5ms_flag = TRUE;
	SysTick_5ms_count++;
	tSysTimeSliceCnt_u8++;

	/* call 5ms function */
	Cyclic1xBaseMain_1();

	if (tSysTimeSliceCnt_u8 == 1u)
	{
		/* call 40ms slice function */
		Cyclic8xBaseMain();
	}

	if ((tSysTimeSliceCnt_u8 % 2u) == 0u)
	{
		/* call 10ms slice function */
		Cyclic2xBaseMain();
	}

	if ((tSysTimeSliceCnt_u8 == 3u) || (tSysTimeSliceCnt_u8 == 7u))
	{
		/* call 20ms slice function */
		Cyclic4xBaseMain();
	}

	if (tSysTimeSliceCnt_u8 == 8u )
	{
		tSysTimeSliceCnt_u8 = 0u;
	}
	
	/* call 5ms  function */
	Cyclic1xBaseMain_2();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void system_config(void)
{
	digital_pin_config();
	pwm_config();
	motion_timer_config();
	dac_config();
	encoder_pin_config();
	encoder_handle_init();
	user_CAN1_Config();

	vehicle_data_init();
	vehicle_control_init();
	comm_input_handle_init();
	comm_output_handle_init();
	
	SpeedController_initialize();
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
static void Cyclic1xBaseMain_1(void)
{
	/* 5ms */
	comm_input_handler();
}

static void Cyclic2xBaseMain(void)
{
	/* 10ms */
	VehicleControl_handler();
	
}

static void Cyclic4xBaseMain(void)
{
	/* 20ms */
	// user_CAN_TestFnc();
	encoder_handler();
	SpeedController_OneStep();
	comm_output_handler();
}

static void Cyclic8xBaseMain(void)
{
	/* 40ms */
	
}

static void Cyclic1xBaseMain_2(void)
{
	/* 5ms */
	
}
