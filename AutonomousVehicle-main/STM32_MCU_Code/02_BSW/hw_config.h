/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

#ifndef HW_PIN_CFG_H
#define HW_PIN_CFG_H

/******************************************************************************
* Include Files
******************************************************************************/
#include "main.h"

/*******************************************************************************
* Definitions 
*******************************************************************************/
typedef enum
{
  PwmHdl_Channel_1 = 0,
	PwmHdl_Channel_2,
	PwmHdl_Channel_3,
	PwmHdl_Channel_4,
    PwmHdl_Channel_Max,
}PwmHdl_Channel_en;

/* pwm duty hw configuration typedef */
typedef struct
{
	boolean_t Channel_Enabled;
	u32 Pulse;
}PwmHdl_ChannelRuntime_t;

/******************************************************************************
* Macro Definitions
******************************************************************************/
/** **************************************   LED PIN   *******************************************/
#define Color_Blue		GPIO_Pin_15 /* Port D pin 15 */
#define Color_Red		GPIO_Pin_14 /* Port D pin 14 */
#define Color_Orange	GPIO_Pin_13 /* Port D pin 13 */
#define Color_Green		GPIO_Pin_12 /* Port D pin 12 */

// #define 	GPIO_AccelerationSwitch			GPIO_Pin_0 /* GPIOA */
// #define 	GPIO_MovingDirectionSwitch	GPIO_Pin_1 /* GPIOA */
// #define 	GPIO_BrakeSwitch						GPIO_Pin_2 /* GPIOA */
// #define 	GPIO_TurnLeft								GPIO_Pin_0 /* GPIOC */
// #define 	GPIO_TurnRight							GPIO_Pin_1 /* GPIOC */
// #define 	GPIO_Horn										GPIO_Pin_2 /* GPIOC */
// #define 	GPIO_FrontLight							GPIO_Pin_3 /* GPIOC */

#define GPIO_AccelerationSwitch				GPIO_Pin_13 /* GPIOD */
#define GPIO_BrakeSwitch							GPIO_Pin_14 /* GPIOD */
#define GPIO_MovingDirectionSwitch		GPIO_Pin_15 /* GPIOD */

/** *********************************   DEFINE TIMER MOTOR   *************************************/
#define PWM_1_TIMx               TIM1        /* TIM1 : 100MHz max for STM32F411 */
#define PWM_1_InitPrescaler      (1000u-1u)   /* PWM_1_TIMx clock = 100M/1000 = 100KHz */
#define PWM_1_InitPeriod         (100000u-1u) 	/* => 100K/100K = 1Hz PWM */

/** *********************************   DEFINE ENCODE PIN   **************************************/
#define 	GPIO_ChannelA_Pin			GPIO_Pin_6
#define 	GPIO_ChannelB_Pin			GPIO_Pin_7
#define 	GPIO_ChannelA_PinSource		GPIO_PinSource6
#define 	GPIO_ChannelB_PinSource		GPIO_PinSource7
#define		TIMx_Encoder				TIM3
#define		Encoder_Resolution			(4*360)

/******************************************************************************
* Externs Definitions
******************************************************************************/
#define TurnON_Led(Color)	GPIO_SetBits(GPIOD, Color)
#define TurnOFF_Led(Color)	GPIO_ResetBits(GPIOD, Color)
#define Toggle_Led(Color)	GPIO_ToggleBits(GPIOD, Color)

extern void delay_ms(u32 ms);
extern void delay_us(u32 us);

extern void digital_pin_config(void);

extern void encoder_pin_config(void);
extern int32_t Encoder_GetCnt(void);

extern void dac_config(void);

extern void pwm_config(void);
extern void PwmHdl_Cfg_SetFrequency(u32 Freq);
extern void PwmHdl_Cfg_SetDuty(PwmHdl_Channel_en PwmHdl_Channel, double Duty);

extern void motion_timer_config(void);
#endif /* HW_PIN_CFG_H */
