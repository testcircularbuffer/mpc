/******************************************************************************
* Company        : HCM Universyty of Technology. 
* Author         : Huy Le
* Description    : 
* Limitations    : 
******************************************************************************/

/******************************************************************************
* Include Files
******************************************************************************/
#include "encoder_handle.h"
/******************************************************************************
* Local Variables
******************************************************************************/

/******************************************************************************
* Global Variables
******************************************************************************/
static s32 Encoder_cnt;
static double Encoder_revolution;
static double Last_Encoder_revolution = 0;
static double Veh_Speed;

/******************************************************************************
* Global Function Declarations
******************************************************************************/

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void encoder_handle_init(void)
{

}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
void encoder_handler(void)
{
	Encoder_cnt = Encoder_GetCnt();
	Encoder_revolution = (double)Encoder_cnt / (double)Encoder_Resolution;
	
	Veh_Speed = (Encoder_revolution - Last_Encoder_revolution) * 25; // Unit ??
	
	Last_Encoder_revolution = Encoder_revolution;
	
	if(Veh_Speed == 0)
	{
		Veh_Speed = 0;
	}
}

/******************************************************************************
* Description: 
* Parameter: 
* Return Value: None
* Author: Huy Le
******************************************************************************/
float encoder_handler_GetVehSpeed(void)
{
    return (float)(-Veh_Speed);
}
