/* Includes ------------------------------------------------------------------*/
#include "user_CAN.h"
/* Private variables ---------------------------------------------------------*/

CanTxMsg CAN1_TxMessage;
CanRxMsg CAN1_RxMessage;
uint8_t ubKeyNumber;
/* Private function prototypes -----------------------------------------------*/

/* Exported function body ----------------------------------------------------*/

void user_CAN1_Config(void)
{
  GPIO_InitTypeDef				GPIO_InitStructure;
	CAN_InitTypeDef					CAN_InitStructure;
  CAN_FilterInitTypeDef		CAN_FilterInitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;

  /* CAN GPIOs configuration **************************************************/

  /* Enable GPIO clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

  /* Connect CAN pins to AF9 */
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); 
  
  /* Configure CAN RX and TX pins */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* CAN configuration ********************************************************/  
  /* Enable CAN clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  
  /* CAN register init */
  CAN_DeInit(CAN1);

  /* CAN cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;
  CAN_InitStructure.CAN_ABOM = DISABLE;
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = ENABLE;
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = ENABLE;
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
    
	CAN_InitStructure.CAN_Prescaler = 3;
  CAN_InitStructure.CAN_BS1 = CAN_BS1_6tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_7tq;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
  CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN filter init */
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = ID_PMU_STATUS <<5;
  CAN_FilterInitStructure.CAN_FilterIdLow = ID_ECU_CONTROL <<5;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = ID_ECU_STATUS <<5;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = ID_ECU_VEHICLE_TEST <<5; 
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);
  
  /* Transmit Structure preparation */
  CAN1_TxMessage.StdId = 0x0001;
  CAN1_TxMessage.ExtId = 0x10;
  CAN1_TxMessage.RTR = CAN_RTR_DATA;
  CAN1_TxMessage.IDE = CAN_ID_STD;
  CAN1_TxMessage.DLC = 8;
  
  /* Enable FIFO 0 message pending Interrupt */
  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

  NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void user_CAN1_Transmit(uint16_t _id, uint8_t _length, uint8_t _data[])
{
	uint32_t uwCounter = 0;
  uint8_t TransmitMailbox = 0;
	uint8_t i = 0;
	/* transmit */
  CAN1_TxMessage.StdId = _id;
  CAN1_TxMessage.RTR = CAN_RTR_DATA;
  CAN1_TxMessage.IDE = CAN_ID_STD;
	CAN1_TxMessage.DLC = _length;
	for(i=0; i< _length ; i++)
	{
		CAN1_TxMessage.Data[i] = _data[i];
	}

  TransmitMailbox = CAN_Transmit(CAN1, &CAN1_TxMessage);
  while((CAN_TransmitStatus(CAN1, TransmitMailbox)  !=  CANTXOK) && (uwCounter  !=  0xFFFF))
  {
    uwCounter++;
  }
  uwCounter = 0;
  while((CAN_MessagePending(CAN1, CAN_FIFO0) < 1) && (uwCounter  !=  0xFFFF))
  {
    uwCounter++;
  }
}

void user_CAN_TestFnc(void)
{
	static u8 time_counter = 0;
	static uint8_t dataTest[8] = {0};
	
	if (time_counter > 50)
	{
		time_counter = 0;
		dataTest[0]++;
		dataTest[1] = 0x55;
		dataTest[2] = 0x55;
		dataTest[3] = 0x55;
		dataTest[4] = 0x55;
		dataTest[5] = 0x55;
		dataTest[6] = 0x55;
		dataTest[7] = 0x55;
		user_CAN1_Transmit(0x001B, 8, dataTest);
	}
	else
	{
		time_counter ++;
	}
}

