﻿using System;
using System.Text;
using System.Net.Sockets;
using Newtonsoft.Json;
using static RosBridgeDotNet.RosBridgeDotNet;

namespace RosBridgeDotNet
{
    public class Turtle_Communication
    {
        public static PublishMessage turtle_up()
        {
            string op = "publish";
            string topic = "/turtle1/cmd_vel";

            Vector3 linear = new Vector3(2.0f, 0.0f, 0.0f);
            Vector3 angular = new Vector3(0.0f, 0.0f, 0.0f);
            Msg_turtle turtleGo1 = new Msg_turtle(linear, angular);

            PublishMessage m = new PublishMessage(op, topic, turtleGo1);

            return m;
        }

        public static PublishMessage turtle_down()
        {
            string op = "publish";
            string topic = "/turtle1/cmd_vel";

            Vector3 linear = new Vector3(-2.0f, 0.0f, 0.0f);
            Vector3 angular = new Vector3(0.0f, 0.0f, 0.0f);
            Msg_turtle turtleGo1 = new Msg_turtle(linear, angular);

            PublishMessage m = new PublishMessage(op, topic, turtleGo1);

            return m;
        }

        public static PublishMessage turtle_left()
        {
            string op = "publish";
            string topic = "/turtle1/cmd_vel";

            Vector3 linear = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 angular = new Vector3(0.0f, 0.0f, 1.57079632679f);
            Msg_turtle turtleGo1 = new Msg_turtle(linear, angular);

            PublishMessage m = new PublishMessage(op, topic, turtleGo1);

            return m;
        }

        public static PublishMessage turtle_right()
        {
            string op = "publish";
            string topic = "/turtle1/cmd_vel";

            Vector3 linear = new Vector3(0.0f, 0.0f, 0.0f);
            Vector3 angular = new Vector3(0.0f, 0.0f, -1.57079632679f);
            Msg_turtle turtleGo1 = new Msg_turtle(linear, angular);

            PublishMessage m = new PublishMessage(op, topic, turtleGo1);

            return m;
        }
    }
}
