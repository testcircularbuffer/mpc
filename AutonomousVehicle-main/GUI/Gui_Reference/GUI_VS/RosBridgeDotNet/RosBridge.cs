﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Net.Sockets;
using System.Numerics;
using RosSharp;

namespace RosBridgeDotNet
{
    public class RosBridgeDotNet
    {
        public class Advertise
        {
            public string op { get; set; }
            public string id { get; set; }
            public string topic { get; set; }
            public string type { get; set; }

            public Advertise(string _op, string _id, string _topic, string _type)
            {
                op = _op;
                id = _id;
                topic = _topic;
                type = _type;
            }

            public Advertise(string _op, string _topic, string _type)
            {
                op = _op;
                topic = _topic;
                type = _type;
            }
        }
        
        public class PublishMessage
        {
            public string op { get; set; }
            public string id { get; set; }
            public string topic { get; set; }
            public object msg { get; set; }

            public PublishMessage() { }

            public PublishMessage(string _op, string _topic, object _msg)
            {
                op = _op;
                topic = _topic;
                msg = _msg;
            }
                  
            public PublishMessage(string _msg)
            {
                msg = _msg;
            }
            public PublishMessage(string _op, string _id, string _topic, string _msg)
            {
                op = _op;
                id = _id;
                topic = _topic;
                msg = _msg;
            }            
        }

        public class SubscribeMessage
        {
            public string op { set; get; }
            public string id { set; get; }
            public string topic { set; get; }
            public string type { set; get; }
            //public int throttle_rate { set; get; }
            //public int queue_length { set; get; }
            //public int fragment_size { set; get; }
            public string compression { set; get; }


            public SubscribeMessage() { }

            //public SubscribeMessage(string _op, string _id, string _topic, string _type, int _throttle_rate, int _queue_length, int _fragment_size, string _compression)
            //{
            //    op = _op;
            //    id = _id;
            //    topic = _topic;
            //    type = _type;
            //    throttle_rate = _throttle_rate;
            //    queue_length = _queue_length;
            //    fragment_size = _fragment_size;
            //    compression = _compression;
            //}

            public SubscribeMessage(string _op, string _topic)
            {
                op = _op;
                topic = _topic;
            }

            public SubscribeMessage(string _op, string _id, string _topic)
            {
                op = _op;
                id = _id;
                topic = _topic;
            }

            public SubscribeMessage(string _op, string _id, string _topic, string _compression)
            {
                op = _op;
                id = _id;
                topic = _topic;
                compression = _compression;
            }
            public SubscribeMessage(string _op, string _id, string _topic,string _type, string _compression)
            {
                op = _op;
                id = _id;
                topic = _topic;
                type = _type;
                compression = _compression;
            }
        }

        public class UnSubscribeMessage
        {
            public string op { set; get; }
            public string id { set; get; }
            public string topic { set; get; }

            public UnSubscribeMessage(string _op, string _topic)
            {
                op = _op;
                topic = _topic;
            }

            public UnSubscribeMessage(string _op, string _id, string _topic)
            {
                op = _op;
                id = _id;
                topic = _topic;
            }
        }

        public class Vector3
        {
            public float x { get; set; }
            public float y { get; set; }
            public float z { get; set; }

            public Vector3(float xVal, float yVal, float zVal)
            {
                x = xVal;
                y = yVal;
                z = zVal;
            }
        }

        public class Msg_turtle
        {
            public Vector3 linear { set; get; }
            public Vector3 angular { set; get; }
            public Msg_turtle(Vector3 _linear, Vector3 _angular)
            {
                linear = _linear;
                angular = _angular;
            }
        }

        public class TurtleSim_Data
        {
            public string topic { get; set; }
            public Msg_turtle msg { get; set; }
            public string op { get; set; }
        }

        public class Thruster
        {
            public double bldc { set; get; }
            public double green_1 { set; get; }
            public double green_2 { set; get; }
            public double yellow_1 { set; get; }
            public double yellow_2 { set; get; }
            public double yellow_3 { set; get; }
        }

        public class ROS_Joystick
        {
            public Thruster thruster { set; get; }
            public ROS_Joystick(Thruster _thruster)
            {
                thruster = _thruster;
            }
        }
    }
}
