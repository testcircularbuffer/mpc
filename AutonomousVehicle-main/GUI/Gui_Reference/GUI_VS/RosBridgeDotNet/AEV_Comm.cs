﻿using System;
using System.Text;
using System.Net.Sockets;
using Newtonsoft.Json;
using static RosBridgeDotNet.RosBridgeDotNet;

namespace RosBridgeDotNet
{
    public class AEV_Comm
    {
        /*****************************************************************************************/
        public class LaneDetc_DataJson
        {
            public string topic { get; set; }
            public LaneDetc_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class LaneDetc_Msg
        {
            public UInt32 msg_counter { get; set; }
            public float centerOffset { get; set; }
            public float curvature { get; set; }

            public LaneDetc_Msg(UInt32 _msg_counter, float _centerOffset, float _curvature)
            {
                msg_counter = _msg_counter;
                centerOffset = _centerOffset;
                curvature = _curvature;
            }
        }
        public static PublishMessage LaneData_MsgMake(LaneDetc_Msg Msg)
        {
            string op = "publish";
            string topic = "/LaneDetc_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class Obstruction_IMU_DataJson
        {
            public string topic { get; set; }
            public Obstruction_IMU_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class Obstruction_IMU_Msg
        {
            public UInt32 msg_counter { get; set; }
            public bool isObject { get; set; }
            public float yaw_rate { get; set; }

            public Obstruction_IMU_Msg(UInt32 _msg_counter, bool _isObject, float _yaw_rate)
            {
                msg_counter = _msg_counter;
                isObject = _isObject;
                yaw_rate = _yaw_rate;
            }
        }
        public static PublishMessage Obstruction_IMU_MsgMake(Obstruction_IMU_Msg Msg)
        {
            string op = "publish";
            string topic = "/ObjDetc_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class GUI_DataJson
        {
            public string topic { get; set; }
            public GUI_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class GUI_Msg
        {
            public UInt32 msg_counter { get; set; }
            public bool userReqStart { get; set; }
            public bool userReqAutoRun { get; set; }
            public bool userReqStop { get; set; }
            public bool clearError { get; set; }
            public Int16 speedSetpoint { get; set; }
            public Byte turnSignal { get; set; }
            public bool horn { get; set; }
            public bool frontLight { get; set; }
            public Byte steeringLeftRight { get; set; }

            public GUI_Msg(UInt32 _msg_counter, 
                            bool _userReqStart,
                            bool _userReqAutoRun,
                            bool _userReqStop,
                            bool _clearError,
                            Int16 _speedSetpoint,
                            Byte _turnSignal,
                            bool _horn,
                            bool _frontLight,
                            Byte _steeringLeftRight)
            {
                msg_counter = _msg_counter;
                userReqStart = _userReqStart;
                userReqAutoRun = _userReqAutoRun;
                userReqStop = _userReqStop;
                clearError = _clearError;
                speedSetpoint = _speedSetpoint;
                turnSignal = _turnSignal;
                horn = _horn;
                frontLight = _frontLight;
                steeringLeftRight = _steeringLeftRight;
            }
        }
        public static PublishMessage GUI_MsgMake(GUI_Msg Msg)
        {
            string op = "publish";
            string topic = "/GUI_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class MPC_DataJson
        {
            public string topic { get; set; }
            public MPC_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class MPC_Msg
        {
            public UInt32 msg_counter { get; set; }
            public float SteeringAngle { get; set; }

            public MPC_Msg(UInt32 _msg_counter,
                            float _SteeringAngle)
            {
                msg_counter = _msg_counter;
                SteeringAngle = _SteeringAngle;
            }
        }
        public static PublishMessage MPC_MsgMake(MPC_Msg Msg)
        {
            string op = "publish";
            string topic = "/MPC_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class Radar_DataJson
        {
            public string topic { get; set; }
            public Radar_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class Radar_Msg
        {
            public float ttcSpeed { get; set; }
            public float ttcSteering { get; set; }
            public string ttcKey { get; set; }

            public UInt32 msg_counter { get; set; }
            public bool isObject { get; set; }
            public float distance { get; set; }

            public Radar_Msg(float _ttcSpeed,
                            float _ttcSteering,
                            string _ttcKey,
                            UInt32 _msg_counter,
                            bool _isObject,
                            float _distance)
            {
                ttcSpeed = _ttcSpeed;
                ttcSteering = _ttcSteering;
                ttcKey = _ttcKey;
                msg_counter = _msg_counter;
                isObject = _isObject;
                distance = _distance;
            }
        }
        public static PublishMessage Radar_MsgMake(MPC_Msg Msg)
        {
            string op = "publish";
            string topic = "/Radar_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class SystemMonitor_DataJson
        {
            public string topic { get; set; }
            public SystemMonitor_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class SystemMonitor_Msg
        {
            public bool errorFlag { get; set; }
            public bool stopRequestFlag { get; set; }
            public UInt32 errorInfo { get; set; }

            public SystemMonitor_Msg(bool _errorFlag,
                            bool _stopRequestFlag,
                            UInt32 _errorInfo)
            {
                errorFlag = _errorFlag;
                stopRequestFlag = _stopRequestFlag;
                errorInfo = _errorInfo;
            }
        }
        public static PublishMessage SystemMonitor_MsgMake(SystemMonitor_Msg Msg)
        {
            string op = "publish";
            string topic = "/SystemMonitor_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class EcuFeedback_DataJson
        {
            public string topic { get; set; }
            public EcuFeedback_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class EcuFeedback_Msg
        {
            public UInt32 msg_counter { get; set; }
            public byte feedbackSpeed_b1 { get; set; }
            public byte feedbackSpeed_b2 { get; set; }
            public byte feedbackSpeed_b3 { get; set; }
            public byte feedbackSpeed_b4 { get; set; }
            public byte acceleratorLevel { get; set; }
            public bool acceleratorSwitch { get; set; }
            public bool brakeSwitch { get; set; }
            public bool movingDirection { get; set; }
            public byte turnSignal { get; set; }
            public bool horn { get; set; }
            public bool frontLight { get; set; }

            public EcuFeedback_Msg(UInt32 _msg_counter,
                            byte _feedbackSpeed_b1,
                            byte _feedbackSpeed_b2,
                            byte _feedbackSpeed_b3,
                            byte _feedbackSpeed_b4,
                            byte _acceleratorLevel,
                            bool _acceleratorSwitch,
                            bool _brakeSwitch,
                            bool _movingDirection,
                            byte _turnSignal,
                            bool _horn,
                            bool _frontLight)
            {
                msg_counter = _msg_counter;
                feedbackSpeed_b1 = _feedbackSpeed_b1;
                feedbackSpeed_b2 = _feedbackSpeed_b2;
                feedbackSpeed_b3 = _feedbackSpeed_b3;
                feedbackSpeed_b4 = _feedbackSpeed_b4;
                acceleratorLevel = _acceleratorLevel;
                acceleratorSwitch = _acceleratorSwitch;
                brakeSwitch = _brakeSwitch;
                movingDirection = _movingDirection;
                turnSignal = _turnSignal;
                horn = _horn;
                frontLight = _frontLight;
            }
        }
        public static PublishMessage EcuFeedback_MsgMake(SystemMonitor_Msg Msg)
        {
            string op = "publish";
            string topic = "/ECU_Feedback_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

        /*****************************************************************************************/
        public class DrivingModeSel_DataJson
        {
            public string topic { get; set; }
            public DrivingModeSel_Msg msg { get; set; }
            public string op { get; set; }
        }
        public class DrivingModeSel_Msg
        {
            public UInt32 msg_counter { get; set; }
            public byte drivingMode { get; set; }

            public DrivingModeSel_Msg(UInt32 _msg_counter,
                            byte _drivingMode)
            {
                msg_counter = _msg_counter;
                drivingMode = _drivingMode;
            }
        }
        public static PublishMessage DrivingModeSel_MsgMake(DrivingModeSel_Msg Msg)
        {
            string op = "publish";
            string topic = "/DrivingMode_Data";
            PublishMessage m = new PublishMessage(op, topic, Msg);
            return m;
        }

    }
}
