﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RosBridgeDotNet
{
    public class MyVector3
    {
        //public static implicit operator Vector3(MyVector3 mv)
        //{
        //    return new Vector3(mv.x, mv.x, mv.z);
        //}

        //public static implicit operator MyVector3(Vector3 v)
        //{
        //    return new MyVector3(v.X, v.Y, v.Z);
        //}

        public float x { get; set; }

        public float y { get; set; }

        public float z { get; set; }

        public MyVector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public MyVector3(float xVal, float yVal, float zVal)
        {
            x = xVal;
            y = yVal;
            z = zVal;
        }

        public static MyVector3 operator +(MyVector3 mv1, MyVector3 mv2)
        {
            return new MyVector3(mv1.x + mv2.x, mv1.y + mv2.y, mv1.z + mv2.z);
        }

        public static MyVector3 operator -(MyVector3 mv1, MyVector3 mv2)
        {
            return new MyVector3(mv1.x - mv2.x, mv1.y - mv2.y, mv1.z - mv2.z);
        }

        public static MyVector3 operator -(MyVector3 mv1, float var)
        {
            return new MyVector3(mv1.x - var, mv1.y - var, mv1.z - var);
        }

        public static MyVector3 operator *(MyVector3 mv1, MyVector3 mv2)
        {
            return new MyVector3(mv1.x * mv2.x, mv1.y * mv2.y, mv1.z * mv2.z);
        }

        public static MyVector3 operator *(MyVector3 mv, float var)
        {
            return new MyVector3(mv.x * var, mv.y * var, mv.z * var);
        }

        public static MyVector3 operator %(MyVector3 mv1, MyVector3 mv2)
        {
            return new MyVector3(mv1.y * mv2.z - mv1.z * mv2.y,
                                 mv1.z * mv2.x - mv1.x * mv2.z,
                                 mv1.x * mv2.y - mv1.y * mv2.x);
        }

        public float this[int key]
        {
            get
            {
                return GetValueByIndex(key);
            }
            set { SetValueByIndex(key, value); }
        }

        private void SetValueByIndex(int key, float value)
        {
            if (key == 0) x = value;
            else if (key == 1) y = value;
            else if (key == 2) z = value;
        }

        private float GetValueByIndex(int key)
        {
            if (key == 0) return x;
            if (key == 1) return y;
            return z;
        }

        public float DotProduct(MyVector3 mv)
        {
            return x * mv.x + y * mv.y + z * mv.z;
        }

        public MyVector3 ScaleBy(float value)
        {
            return new MyVector3(x * value, y * value, z * value);
        }

        public MyVector3 ComponentProduct(MyVector3 mv)
        {
            return new MyVector3(x * mv.x, y * mv.y, z * mv.z);
        }

        public void ComponentProductUpdate(MyVector3 mv)
        {
            x *= mv.x;
            y *= mv.y;
            z *= mv.z;
        }

        public MyVector3 VectorProduct(MyVector3 mv)
        {
            return new MyVector3(y * mv.z - z * mv.y,
                                 z * mv.x - x * mv.z,
                                 x * mv.y - y * mv.x);
        }

        public float ScalarProduct(MyVector3 mv)
        {
            return x * mv.x + y * mv.y + z * mv.z;
        }

        public void AddScaledVector(MyVector3 mv, float scale)
        {
            x += mv.x * scale;
            y += mv.y * scale;
            z += mv.z * scale;
        }

        public float Magnitude()
        {
            return (float)Math.Sqrt(x * x + y * y + z * z);
        }

        public float SquareMagnitude()
        {
            return x * x + y * y + z * z;
        }

        public void Trim(float size)
        {
            if (SquareMagnitude() > size * size)
            {
                Normalize();
                x *= size;
                y *= size;
                z *= size;
            }
        }

        public void Normalize()
        {
            float m = Magnitude();
            if (m > 0)
            {
                x = x / m;
                y = y / m;
                z = z / m;
            }
            else
            {
                x = 0;
                y = 0;
                z = 0;
            }
        }

        public MyVector3 Inverted()
        {
            return new MyVector3(-x, -y, -z);
        }

        public MyVector3 Unit()
        {
            MyVector3 result = this;
            result.Normalize();
            return result;
        }

        public void Clear()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public static float Distance(MyVector3 mv1, MyVector3 mv2)
        {
            return (mv1 - mv2).Magnitude();
        }

        public static MyVector3 RandomMyVector()
        {
            Random rd = new Random();
            return new MyVector3(rd.Next(), rd.Next(), rd.Next());
        }

        public static MyVector3 zero()
        {
            return new MyVector3(0f, 0f, 0f);
        }

        //public static Vector3[] ReturnAsVector3(MyVector3[] mv3)
        //{
        //  Vector3[] v3 = new Vector3[mv3.Length];

        //  return v3;
        //}
    }
}
