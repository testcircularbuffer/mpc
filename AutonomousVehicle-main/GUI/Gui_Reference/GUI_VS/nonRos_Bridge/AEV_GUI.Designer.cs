﻿namespace nonRos_Bridge
{
    partial class AEV_GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.textBox_SysInfo = new System.Windows.Forms.TextBox();
            this.Notice_AcceleratorSwitch = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Notice_BreakSwitch = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Notice_Error = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.ovalShape1 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.TestCar = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.SystemMonitor_ErrorInfo = new System.Windows.Forms.Label();
            this.SystemMonitor_errorFlag = new System.Windows.Forms.Label();
            this.SystemMonitor_StopReqFlag = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Radar_msg_cnt = new System.Windows.Forms.Label();
            this.Radar_isObject = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.MPC_msg_cnt = new System.Windows.Forms.Label();
            this.MPC_Steering = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.objDetc_yawRate = new System.Windows.Forms.Label();
            this.objDetc_msg_cnt = new System.Windows.Forms.Label();
            this.objDetc_isObj = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ddhdhd = new System.Windows.Forms.Label();
            this.label_YawRateVal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.trackBar_YawRate = new System.Windows.Forms.TrackBar();
            this.btn_Adv_ObjDetcData = new System.Windows.Forms.Button();
            this.CarControl = new System.Windows.Forms.TabPage();
            this.groupBox_CarStatus = new System.Windows.Forms.GroupBox();
            this.Radar_Speed = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Radar_Steering = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Radar_Distance = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox_ActualSteeringAng = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label_MovingDirection = new System.Windows.Forms.Label();
            this.label_BrakeSwitch = new System.Windows.Forms.Label();
            this.label_AcceleratorSwitch = new System.Windows.Forms.Label();
            this.label_AcceleratorLevel = new System.Windows.Forms.Label();
            this.label_DrivingMode = new System.Windows.Forms.Label();
            this.textBox_actualSpeed = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.trackBar_actualSpeed = new System.Windows.Forms.TrackBar();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangle_isRadarObj = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangle_isCameraObj = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.groupBox_CarControl = new System.Windows.Forms.GroupBox();
            this.error_cnt_mpc_msg = new System.Windows.Forms.Label();
            this.label_SteeringAngleMsg = new System.Windows.Forms.Label();
            this.label_CurvatureVal = new System.Windows.Forms.Label();
            this.trackBar_Curvature = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label_LateralDeviationVal = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.trackBar_centerOffset = new System.Windows.Forms.TrackBar();
            this.btn_Adv_LaneDetcData = new System.Windows.Forms.Button();
            this.button_ResetSteering = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox_speedSetpoint = new System.Windows.Forms.TextBox();
            this.trackBar_SreeingAng = new System.Windows.Forms.TrackBar();
            this.label35 = new System.Windows.Forms.Label();
            this.button_clearError = new System.Windows.Forms.Button();
            this.textBox_SteeringAng = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button_userReqStop = new System.Windows.Forms.Button();
            this.button_TurnSignal_Left = new System.Windows.Forms.Button();
            this.button_userReqAutoRun = new System.Windows.Forms.Button();
            this.button_Horn = new System.Windows.Forms.Button();
            this.button_userReqStart = new System.Windows.Forms.Button();
            this.button_steeringRight = new System.Windows.Forms.Button();
            this.trackBar_speedSetpoint = new System.Windows.Forms.TrackBar();
            this.button_steeringLeft = new System.Windows.Forms.Button();
            this.button_TurnSignal_Right = new System.Windows.Forms.Button();
            this.button_frontLight = new System.Windows.Forms.Button();
            this.button_AutoSteering = new System.Windows.Forms.Button();
            this.button_Advertise_GuiMsg = new System.Windows.Forms.Button();
            this.SystemConnect = new System.Windows.Forms.TabPage();
            this.groupBox_DriverControl = new System.Windows.Forms.GroupBox();
            this.button_Servo_Off = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.tBoxDataReceive = new System.Windows.Forms.TextBox();
            this.button_Servo_On = new System.Windows.Forms.Button();
            this.button_SetDriverCurrent = new System.Windows.Forms.Button();
            this.button_SetDriverPos = new System.Windows.Forms.Button();
            this.Set_Current = new System.Windows.Forms.Label();
            this.Set_Position = new System.Windows.Forms.Label();
            this.textBox_DriverCurrent = new System.Windows.Forms.TextBox();
            this.textBox_DriverPosition = new System.Windows.Forms.TextBox();
            this.button_Position_Mode = new System.Windows.Forms.Button();
            this.groupBox_DriverComPort = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.cBoxPatityBits = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lbStaticComPort = new System.Windows.Forms.Label();
            this.cBoxStopBits = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.cBoxDataBits = new System.Windows.Forms.ComboBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.cBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.cBoxComPort = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox_ROS_System = new System.Windows.Forms.GroupBox();
            this.btn_Sub_MPC = new System.Windows.Forms.Button();
            this.btn_Sub_All = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.txt_Port = new System.Windows.Forms.TextBox();
            this.txt_IP = new System.Windows.Forms.TextBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.AEV_Tab = new System.Windows.Forms.TabControl();
            this.groupBox_ControlStatus = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Status_ros_advertise = new System.Windows.Forms.Button();
            this.Status_steering_auto = new System.Windows.Forms.Button();
            this.Status_steering_ON = new System.Windows.Forms.Button();
            this.Status_steering_driver = new System.Windows.Forms.Button();
            this.Status_ros_subscribe = new System.Windows.Forms.Button();
            this.Status_ros_connection = new System.Windows.Forms.Button();
            this.TestCar.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_YawRate)).BeginInit();
            this.CarControl.SuspendLayout();
            this.groupBox_CarStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_actualSpeed)).BeginInit();
            this.groupBox_CarControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Curvature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_centerOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_SreeingAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_speedSetpoint)).BeginInit();
            this.SystemConnect.SuspendLayout();
            this.groupBox_DriverControl.SuspendLayout();
            this.groupBox_DriverComPort.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox_ROS_System.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.AEV_Tab.SuspendLayout();
            this.groupBox_ControlStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Gray;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Location = new System.Drawing.Point(1003, 608);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(96, 35);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 200;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // textBox_SysInfo
            // 
            this.textBox_SysInfo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox_SysInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_SysInfo.Location = new System.Drawing.Point(12, 614);
            this.textBox_SysInfo.Multiline = true;
            this.textBox_SysInfo.Name = "textBox_SysInfo";
            this.textBox_SysInfo.Size = new System.Drawing.Size(985, 24);
            this.textBox_SysInfo.TabIndex = 31;
            // 
            // Notice_AcceleratorSwitch
            // 
            this.Notice_AcceleratorSwitch.BackColor = System.Drawing.Color.Gray;
            this.Notice_AcceleratorSwitch.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.Notice_AcceleratorSwitch.Location = new System.Drawing.Point(151, 239);
            this.Notice_AcceleratorSwitch.Name = "Notice_AcceleratorSwitch";
            this.Notice_AcceleratorSwitch.Size = new System.Drawing.Size(56, 12);
            // 
            // Notice_BreakSwitch
            // 
            this.Notice_BreakSwitch.BackColor = System.Drawing.Color.Gray;
            this.Notice_BreakSwitch.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.Notice_BreakSwitch.Location = new System.Drawing.Point(151, 266);
            this.Notice_BreakSwitch.Name = "Notice_BreakSwitch";
            this.Notice_BreakSwitch.Size = new System.Drawing.Size(56, 12);
            // 
            // Notice_Error
            // 
            this.Notice_Error.BackColor = System.Drawing.Color.Gray;
            this.Notice_Error.Location = new System.Drawing.Point(262, 203);
            this.Notice_Error.Name = "Notice_Error";
            this.Notice_Error.Size = new System.Drawing.Size(64, 59);
            // 
            // ovalShape1
            // 
            this.ovalShape1.BackColor = System.Drawing.Color.Red;
            this.ovalShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.ovalShape1.Location = new System.Drawing.Point(154, 113);
            this.ovalShape1.Name = "ovalShape1";
            this.ovalShape1.Size = new System.Drawing.Size(31, 30);
            // 
            // TestCar
            // 
            this.TestCar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TestCar.Controls.Add(this.groupBox9);
            this.TestCar.Controls.Add(this.groupBox6);
            this.TestCar.Controls.Add(this.groupBox5);
            this.TestCar.Controls.Add(this.groupBox4);
            this.TestCar.Controls.Add(this.groupBox3);
            this.TestCar.Controls.Add(this.label_YawRateVal);
            this.TestCar.Controls.Add(this.label8);
            this.TestCar.Controls.Add(this.trackBar_YawRate);
            this.TestCar.Controls.Add(this.btn_Adv_ObjDetcData);
            this.TestCar.Location = new System.Drawing.Point(4, 22);
            this.TestCar.Name = "TestCar";
            this.TestCar.Padding = new System.Windows.Forms.Padding(3);
            this.TestCar.Size = new System.Drawing.Size(871, 570);
            this.TestCar.TabIndex = 4;
            this.TestCar.Text = "TestCar";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.label39);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(618, 165);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(233, 95);
            this.groupBox9.TabIndex = 45;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "ECU_Data";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(133, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(39, 13);
            this.label37.TabIndex = 3;
            this.label37.Text = "Value";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(133, 44);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 13);
            this.label38.TabIndex = 4;
            this.label38.Text = "Value";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(24, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(55, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "msg cnt:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(24, 44);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(103, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "FeedbackSpeed:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.SystemMonitor_ErrorInfo);
            this.groupBox6.Controls.Add(this.SystemMonitor_errorFlag);
            this.groupBox6.Controls.Add(this.SystemMonitor_StopReqFlag);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(618, 48);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(233, 95);
            this.groupBox6.TabIndex = 44;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SystemMonitor_Data";
            // 
            // SystemMonitor_ErrorInfo
            // 
            this.SystemMonitor_ErrorInfo.AutoSize = true;
            this.SystemMonitor_ErrorInfo.Location = new System.Drawing.Point(92, 67);
            this.SystemMonitor_ErrorInfo.Name = "SystemMonitor_ErrorInfo";
            this.SystemMonitor_ErrorInfo.Size = new System.Drawing.Size(39, 13);
            this.SystemMonitor_ErrorInfo.TabIndex = 5;
            this.SystemMonitor_ErrorInfo.Text = "Value";
            // 
            // SystemMonitor_errorFlag
            // 
            this.SystemMonitor_errorFlag.AutoSize = true;
            this.SystemMonitor_errorFlag.Location = new System.Drawing.Point(93, 22);
            this.SystemMonitor_errorFlag.Name = "SystemMonitor_errorFlag";
            this.SystemMonitor_errorFlag.Size = new System.Drawing.Size(39, 13);
            this.SystemMonitor_errorFlag.TabIndex = 3;
            this.SystemMonitor_errorFlag.Text = "Value";
            // 
            // SystemMonitor_StopReqFlag
            // 
            this.SystemMonitor_StopReqFlag.AutoSize = true;
            this.SystemMonitor_StopReqFlag.Location = new System.Drawing.Point(92, 44);
            this.SystemMonitor_StopReqFlag.Name = "SystemMonitor_StopReqFlag";
            this.SystemMonitor_StopReqFlag.Size = new System.Drawing.Size(39, 13);
            this.SystemMonitor_StopReqFlag.TabIndex = 4;
            this.SystemMonitor_StopReqFlag.Text = "Value";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(20, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Error Flag:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(0, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "StopReqFlag:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(24, 67);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "ErrorInfo:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Radar_msg_cnt);
            this.groupBox5.Controls.Add(this.Radar_isObject);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(351, 282);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(233, 95);
            this.groupBox5.TabIndex = 45;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Radar_Data";
            // 
            // Radar_msg_cnt
            // 
            this.Radar_msg_cnt.AutoSize = true;
            this.Radar_msg_cnt.Location = new System.Drawing.Point(93, 22);
            this.Radar_msg_cnt.Name = "Radar_msg_cnt";
            this.Radar_msg_cnt.Size = new System.Drawing.Size(39, 13);
            this.Radar_msg_cnt.TabIndex = 3;
            this.Radar_msg_cnt.Text = "Value";
            // 
            // Radar_isObject
            // 
            this.Radar_isObject.AutoSize = true;
            this.Radar_isObject.Location = new System.Drawing.Point(92, 44);
            this.Radar_isObject.Name = "Radar_isObject";
            this.Radar_isObject.Size = new System.Drawing.Size(39, 13);
            this.Radar_isObject.TabIndex = 4;
            this.Radar_isObject.Text = "Value";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "msg cnt:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "isObject:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.MPC_msg_cnt);
            this.groupBox4.Controls.Add(this.MPC_Steering);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(351, 165);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(233, 95);
            this.groupBox4.TabIndex = 44;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "MPC_Data";
            // 
            // MPC_msg_cnt
            // 
            this.MPC_msg_cnt.AutoSize = true;
            this.MPC_msg_cnt.Location = new System.Drawing.Point(93, 22);
            this.MPC_msg_cnt.Name = "MPC_msg_cnt";
            this.MPC_msg_cnt.Size = new System.Drawing.Size(39, 13);
            this.MPC_msg_cnt.TabIndex = 3;
            this.MPC_msg_cnt.Text = "Value";
            // 
            // MPC_Steering
            // 
            this.MPC_Steering.AutoSize = true;
            this.MPC_Steering.Location = new System.Drawing.Point(92, 44);
            this.MPC_Steering.Name = "MPC_Steering";
            this.MPC_Steering.Size = new System.Drawing.Size(39, 13);
            this.MPC_Steering.TabIndex = 4;
            this.MPC_Steering.Text = "Value";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "msg cnt:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Steering:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.objDetc_yawRate);
            this.groupBox3.Controls.Add(this.objDetc_msg_cnt);
            this.groupBox3.Controls.Add(this.objDetc_isObj);
            this.groupBox3.Controls.Add(this.label88);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.ddhdhd);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(351, 48);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(233, 95);
            this.groupBox3.TabIndex = 43;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ObjDetc_Data";
            // 
            // objDetc_yawRate
            // 
            this.objDetc_yawRate.AutoSize = true;
            this.objDetc_yawRate.Location = new System.Drawing.Point(92, 67);
            this.objDetc_yawRate.Name = "objDetc_yawRate";
            this.objDetc_yawRate.Size = new System.Drawing.Size(39, 13);
            this.objDetc_yawRate.TabIndex = 5;
            this.objDetc_yawRate.Text = "Value";
            // 
            // objDetc_msg_cnt
            // 
            this.objDetc_msg_cnt.AutoSize = true;
            this.objDetc_msg_cnt.Location = new System.Drawing.Point(93, 22);
            this.objDetc_msg_cnt.Name = "objDetc_msg_cnt";
            this.objDetc_msg_cnt.Size = new System.Drawing.Size(39, 13);
            this.objDetc_msg_cnt.TabIndex = 3;
            this.objDetc_msg_cnt.Text = "Value";
            // 
            // objDetc_isObj
            // 
            this.objDetc_isObj.AutoSize = true;
            this.objDetc_isObj.Location = new System.Drawing.Point(92, 44);
            this.objDetc_isObj.Name = "objDetc_isObj";
            this.objDetc_isObj.Size = new System.Drawing.Size(39, 13);
            this.objDetc_isObj.TabIndex = 4;
            this.objDetc_isObj.Text = "Value";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(24, 22);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(55, 13);
            this.label88.TabIndex = 0;
            this.label88.Text = "msg cnt:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "obstruction:";
            // 
            // ddhdhd
            // 
            this.ddhdhd.AutoSize = true;
            this.ddhdhd.Location = new System.Drawing.Point(24, 67);
            this.ddhdhd.Name = "ddhdhd";
            this.ddhdhd.Size = new System.Drawing.Size(62, 13);
            this.ddhdhd.TabIndex = 2;
            this.ddhdhd.Text = "yaw_rate:";
            // 
            // label_YawRateVal
            // 
            this.label_YawRateVal.AutoSize = true;
            this.label_YawRateVal.Location = new System.Drawing.Point(163, 100);
            this.label_YawRateVal.Name = "label_YawRateVal";
            this.label_YawRateVal.Size = new System.Drawing.Size(34, 13);
            this.label_YawRateVal.TabIndex = 42;
            this.label_YawRateVal.Text = "Value";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "Yaw Rate";
            // 
            // trackBar_YawRate
            // 
            this.trackBar_YawRate.Location = new System.Drawing.Point(45, 115);
            this.trackBar_YawRate.Maximum = 100;
            this.trackBar_YawRate.Minimum = -100;
            this.trackBar_YawRate.Name = "trackBar_YawRate";
            this.trackBar_YawRate.Size = new System.Drawing.Size(251, 45);
            this.trackBar_YawRate.TabIndex = 40;
            this.trackBar_YawRate.Scroll += new System.EventHandler(this.trackBar_YawRate_Scroll);
            // 
            // btn_Adv_ObjDetcData
            // 
            this.btn_Adv_ObjDetcData.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Adv_ObjDetcData.Location = new System.Drawing.Point(45, 48);
            this.btn_Adv_ObjDetcData.Name = "btn_Adv_ObjDetcData";
            this.btn_Adv_ObjDetcData.Size = new System.Drawing.Size(175, 29);
            this.btn_Adv_ObjDetcData.TabIndex = 39;
            this.btn_Adv_ObjDetcData.Text = "Advertise_ObjDetc_Data";
            this.btn_Adv_ObjDetcData.UseVisualStyleBackColor = true;
            this.btn_Adv_ObjDetcData.Click += new System.EventHandler(this.btn_Adv_ObjDetcData_Click);
            // 
            // CarControl
            // 
            this.CarControl.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.CarControl.Controls.Add(this.groupBox_CarStatus);
            this.CarControl.Controls.Add(this.groupBox_CarControl);
            this.CarControl.Location = new System.Drawing.Point(4, 22);
            this.CarControl.Name = "CarControl";
            this.CarControl.Padding = new System.Windows.Forms.Padding(3);
            this.CarControl.Size = new System.Drawing.Size(871, 570);
            this.CarControl.TabIndex = 5;
            this.CarControl.Text = "CarControl";
            // 
            // groupBox_CarStatus
            // 
            this.groupBox_CarStatus.Controls.Add(this.Radar_Speed);
            this.groupBox_CarStatus.Controls.Add(this.label11);
            this.groupBox_CarStatus.Controls.Add(this.Radar_Steering);
            this.groupBox_CarStatus.Controls.Add(this.label2);
            this.groupBox_CarStatus.Controls.Add(this.Radar_Distance);
            this.groupBox_CarStatus.Controls.Add(this.label7);
            this.groupBox_CarStatus.Controls.Add(this.label44);
            this.groupBox_CarStatus.Controls.Add(this.label43);
            this.groupBox_CarStatus.Controls.Add(this.label42);
            this.groupBox_CarStatus.Controls.Add(this.label31);
            this.groupBox_CarStatus.Controls.Add(this.textBox_ActualSteeringAng);
            this.groupBox_CarStatus.Controls.Add(this.label32);
            this.groupBox_CarStatus.Controls.Add(this.label29);
            this.groupBox_CarStatus.Controls.Add(this.label34);
            this.groupBox_CarStatus.Controls.Add(this.label_MovingDirection);
            this.groupBox_CarStatus.Controls.Add(this.label_BrakeSwitch);
            this.groupBox_CarStatus.Controls.Add(this.label_AcceleratorSwitch);
            this.groupBox_CarStatus.Controls.Add(this.label_AcceleratorLevel);
            this.groupBox_CarStatus.Controls.Add(this.label_DrivingMode);
            this.groupBox_CarStatus.Controls.Add(this.textBox_actualSpeed);
            this.groupBox_CarStatus.Controls.Add(this.label28);
            this.groupBox_CarStatus.Controls.Add(this.trackBar_actualSpeed);
            this.groupBox_CarStatus.Controls.Add(this.shapeContainer2);
            this.groupBox_CarStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_CarStatus.Location = new System.Drawing.Point(492, 21);
            this.groupBox_CarStatus.Name = "groupBox_CarStatus";
            this.groupBox_CarStatus.Size = new System.Drawing.Size(357, 513);
            this.groupBox_CarStatus.TabIndex = 15;
            this.groupBox_CarStatus.TabStop = false;
            this.groupBox_CarStatus.Text = "Vehical Status";
            // 
            // Radar_Speed
            // 
            this.Radar_Speed.AutoSize = true;
            this.Radar_Speed.Location = new System.Drawing.Point(273, 447);
            this.Radar_Speed.Name = "Radar_Speed";
            this.Radar_Speed.Size = new System.Drawing.Size(48, 16);
            this.Radar_Speed.TabIndex = 41;
            this.Radar_Speed.Text = "Value";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(201, 445);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 16);
            this.label11.TabIndex = 40;
            this.label11.Text = "Speed:";
            // 
            // Radar_Steering
            // 
            this.Radar_Steering.AutoSize = true;
            this.Radar_Steering.Location = new System.Drawing.Point(273, 427);
            this.Radar_Steering.Name = "Radar_Steering";
            this.Radar_Steering.Size = new System.Drawing.Size(48, 16);
            this.Radar_Steering.TabIndex = 39;
            this.Radar_Steering.Text = "Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 426);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "Steeing:";
            // 
            // Radar_Distance
            // 
            this.Radar_Distance.AutoSize = true;
            this.Radar_Distance.Location = new System.Drawing.Point(273, 407);
            this.Radar_Distance.Name = "Radar_Distance";
            this.Radar_Distance.Size = new System.Drawing.Size(48, 16);
            this.Radar_Distance.TabIndex = 37;
            this.Radar_Distance.Text = "Value";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(201, 407);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 16);
            this.label7.TabIndex = 36;
            this.label7.Text = "Distance:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(204, 352);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(101, 15);
            this.label44.TabIndex = 35;
            this.label44.Text = "Object (Radar)";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(59, 352);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(112, 15);
            this.label43.TabIndex = 34;
            this.label43.Text = "Object (Camera)";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(174, 84);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(12, 16);
            this.label42.TabIndex = 31;
            this.label42.Text = "I";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(47, 113);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(104, 15);
            this.label31.TabIndex = 33;
            this.label31.Text = "ActualSteering:";
            // 
            // textBox_ActualSteeringAng
            // 
            this.textBox_ActualSteeringAng.Enabled = false;
            this.textBox_ActualSteeringAng.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_ActualSteeringAng.Location = new System.Drawing.Point(156, 108);
            this.textBox_ActualSteeringAng.Name = "textBox_ActualSteeringAng";
            this.textBox_ActualSteeringAng.Size = new System.Drawing.Size(95, 26);
            this.textBox_ActualSteeringAng.TabIndex = 31;
            this.textBox_ActualSteeringAng.TabStop = false;
            this.textBox_ActualSteeringAng.Text = "0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(257, 115);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 15);
            this.label32.TabIndex = 32;
            this.label32.Text = "Rad";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(271, 194);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 15);
            this.label29.TabIndex = 29;
            this.label29.Text = "ERROR:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(37, 27);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(114, 15);
            this.label34.TabIndex = 23;
            this.label34.Text = "FeedbackSpeed:";
            // 
            // label_MovingDirection
            // 
            this.label_MovingDirection.AutoSize = true;
            this.label_MovingDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MovingDirection.Location = new System.Drawing.Point(34, 222);
            this.label_MovingDirection.Name = "label_MovingDirection";
            this.label_MovingDirection.Size = new System.Drawing.Size(115, 15);
            this.label_MovingDirection.TabIndex = 22;
            this.label_MovingDirection.Text = "MovingDirection:";
            // 
            // label_BrakeSwitch
            // 
            this.label_BrakeSwitch.AutoSize = true;
            this.label_BrakeSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_BrakeSwitch.Location = new System.Drawing.Point(61, 280);
            this.label_BrakeSwitch.Name = "label_BrakeSwitch";
            this.label_BrakeSwitch.Size = new System.Drawing.Size(90, 15);
            this.label_BrakeSwitch.TabIndex = 21;
            this.label_BrakeSwitch.Text = "BrakeSwitch:";
            // 
            // label_AcceleratorSwitch
            // 
            this.label_AcceleratorSwitch.AutoSize = true;
            this.label_AcceleratorSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AcceleratorSwitch.Location = new System.Drawing.Point(26, 254);
            this.label_AcceleratorSwitch.Name = "label_AcceleratorSwitch";
            this.label_AcceleratorSwitch.Size = new System.Drawing.Size(125, 15);
            this.label_AcceleratorSwitch.TabIndex = 20;
            this.label_AcceleratorSwitch.Text = "AcceleratorSwitch:";
            // 
            // label_AcceleratorLevel
            // 
            this.label_AcceleratorLevel.AutoSize = true;
            this.label_AcceleratorLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AcceleratorLevel.Location = new System.Drawing.Point(34, 194);
            this.label_AcceleratorLevel.Name = "label_AcceleratorLevel";
            this.label_AcceleratorLevel.Size = new System.Drawing.Size(117, 15);
            this.label_AcceleratorLevel.TabIndex = 19;
            this.label_AcceleratorLevel.Text = "AcceleratorLevel:";
            // 
            // label_DrivingMode
            // 
            this.label_DrivingMode.AutoSize = true;
            this.label_DrivingMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DrivingMode.Location = new System.Drawing.Point(59, 169);
            this.label_DrivingMode.Name = "label_DrivingMode";
            this.label_DrivingMode.Size = new System.Drawing.Size(92, 15);
            this.label_DrivingMode.TabIndex = 18;
            this.label_DrivingMode.Text = "DrivingMode:";
            // 
            // textBox_actualSpeed
            // 
            this.textBox_actualSpeed.Enabled = false;
            this.textBox_actualSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_actualSpeed.Location = new System.Drawing.Point(157, 23);
            this.textBox_actualSpeed.Name = "textBox_actualSpeed";
            this.textBox_actualSpeed.Size = new System.Drawing.Size(44, 26);
            this.textBox_actualSpeed.TabIndex = 15;
            this.textBox_actualSpeed.TabStop = false;
            this.textBox_actualSpeed.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(207, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 15);
            this.label28.TabIndex = 16;
            this.label28.Text = "km/h";
            // 
            // trackBar_actualSpeed
            // 
            this.trackBar_actualSpeed.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.trackBar_actualSpeed.Enabled = false;
            this.trackBar_actualSpeed.Location = new System.Drawing.Point(57, 64);
            this.trackBar_actualSpeed.Maximum = 25;
            this.trackBar_actualSpeed.Minimum = -25;
            this.trackBar_actualSpeed.Name = "trackBar_actualSpeed";
            this.trackBar_actualSpeed.Size = new System.Drawing.Size(245, 45);
            this.trackBar_actualSpeed.TabIndex = 14;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 18);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangle_isRadarObj,
            this.rectangle_isCameraObj,
            this.Notice_Error,
            this.Notice_BreakSwitch,
            this.Notice_AcceleratorSwitch});
            this.shapeContainer2.Size = new System.Drawing.Size(351, 492);
            this.shapeContainer2.TabIndex = 28;
            this.shapeContainer2.TabStop = false;
            // 
            // rectangle_isRadarObj
            // 
            this.rectangle_isRadarObj.BackColor = System.Drawing.Color.Gray;
            this.rectangle_isRadarObj.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangle_isRadarObj.Location = new System.Drawing.Point(214, 363);
            this.rectangle_isRadarObj.Name = "rectangle_isRadarObj";
            this.rectangle_isRadarObj.Size = new System.Drawing.Size(56, 12);
            // 
            // rectangle_isCameraObj
            // 
            this.rectangle_isCameraObj.BackColor = System.Drawing.Color.Gray;
            this.rectangle_isCameraObj.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangle_isCameraObj.Location = new System.Drawing.Point(87, 362);
            this.rectangle_isCameraObj.Name = "rectangle_isCameraObj";
            this.rectangle_isCameraObj.Size = new System.Drawing.Size(56, 12);
            // 
            // groupBox_CarControl
            // 
            this.groupBox_CarControl.Controls.Add(this.label_CurvatureVal);
            this.groupBox_CarControl.Controls.Add(this.trackBar_Curvature);
            this.groupBox_CarControl.Controls.Add(this.label6);
            this.groupBox_CarControl.Controls.Add(this.label_LateralDeviationVal);
            this.groupBox_CarControl.Controls.Add(this.label5);
            this.groupBox_CarControl.Controls.Add(this.trackBar_centerOffset);
            this.groupBox_CarControl.Controls.Add(this.btn_Adv_LaneDetcData);
            this.groupBox_CarControl.Controls.Add(this.button_ResetSteering);
            this.groupBox_CarControl.Controls.Add(this.label41);
            this.groupBox_CarControl.Controls.Add(this.label33);
            this.groupBox_CarControl.Controls.Add(this.label30);
            this.groupBox_CarControl.Controls.Add(this.textBox_speedSetpoint);
            this.groupBox_CarControl.Controls.Add(this.trackBar_SreeingAng);
            this.groupBox_CarControl.Controls.Add(this.label35);
            this.groupBox_CarControl.Controls.Add(this.button_clearError);
            this.groupBox_CarControl.Controls.Add(this.textBox_SteeringAng);
            this.groupBox_CarControl.Controls.Add(this.label36);
            this.groupBox_CarControl.Controls.Add(this.label10);
            this.groupBox_CarControl.Controls.Add(this.button_userReqStop);
            this.groupBox_CarControl.Controls.Add(this.button_TurnSignal_Left);
            this.groupBox_CarControl.Controls.Add(this.button_userReqAutoRun);
            this.groupBox_CarControl.Controls.Add(this.button_Horn);
            this.groupBox_CarControl.Controls.Add(this.button_userReqStart);
            this.groupBox_CarControl.Controls.Add(this.button_steeringRight);
            this.groupBox_CarControl.Controls.Add(this.trackBar_speedSetpoint);
            this.groupBox_CarControl.Controls.Add(this.button_steeringLeft);
            this.groupBox_CarControl.Controls.Add(this.button_TurnSignal_Right);
            this.groupBox_CarControl.Controls.Add(this.button_frontLight);
            this.groupBox_CarControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_CarControl.Location = new System.Drawing.Point(19, 21);
            this.groupBox_CarControl.Name = "groupBox_CarControl";
            this.groupBox_CarControl.Size = new System.Drawing.Size(467, 513);
            this.groupBox_CarControl.TabIndex = 14;
            this.groupBox_CarControl.TabStop = false;
            this.groupBox_CarControl.Text = "Control";
            // 
            // error_cnt_mpc_msg
            // 
            this.error_cnt_mpc_msg.AutoSize = true;
            this.error_cnt_mpc_msg.Location = new System.Drawing.Point(30, 337);
            this.error_cnt_mpc_msg.Name = "error_cnt_mpc_msg";
            this.error_cnt_mpc_msg.Size = new System.Drawing.Size(143, 16);
            this.error_cnt_mpc_msg.TabIndex = 43;
            this.error_cnt_mpc_msg.Text = "error_cnt_mpc_msg";
            // 
            // label_SteeringAngleMsg
            // 
            this.label_SteeringAngleMsg.AutoSize = true;
            this.label_SteeringAngleMsg.Location = new System.Drawing.Point(30, 301);
            this.label_SteeringAngleMsg.Name = "label_SteeringAngleMsg";
            this.label_SteeringAngleMsg.Size = new System.Drawing.Size(16, 16);
            this.label_SteeringAngleMsg.TabIndex = 42;
            this.label_SteeringAngleMsg.Text = "0";
            // 
            // label_CurvatureVal
            // 
            this.label_CurvatureVal.AutoSize = true;
            this.label_CurvatureVal.Location = new System.Drawing.Point(295, 346);
            this.label_CurvatureVal.Name = "label_CurvatureVal";
            this.label_CurvatureVal.Size = new System.Drawing.Size(48, 16);
            this.label_CurvatureVal.TabIndex = 41;
            this.label_CurvatureVal.Text = "Value";
            // 
            // trackBar_Curvature
            // 
            this.trackBar_Curvature.Location = new System.Drawing.Point(191, 362);
            this.trackBar_Curvature.Maximum = 100;
            this.trackBar_Curvature.Minimum = -100;
            this.trackBar_Curvature.Name = "trackBar_Curvature";
            this.trackBar_Curvature.Size = new System.Drawing.Size(251, 45);
            this.trackBar_Curvature.TabIndex = 40;
            this.trackBar_Curvature.Scroll += new System.EventHandler(this.trackBar_Curvature_Scroll);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(196, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 16);
            this.label6.TabIndex = 39;
            this.label6.Text = "Curvature Invert(1/m)";
            // 
            // label_LateralDeviationVal
            // 
            this.label_LateralDeviationVal.AutoSize = true;
            this.label_LateralDeviationVal.Location = new System.Drawing.Point(295, 273);
            this.label_LateralDeviationVal.Name = "label_LateralDeviationVal";
            this.label_LateralDeviationVal.Size = new System.Drawing.Size(48, 16);
            this.label_LateralDeviationVal.TabIndex = 38;
            this.label_LateralDeviationVal.Text = "Value";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(196, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 16);
            this.label5.TabIndex = 37;
            this.label5.Text = "Lateral Deviation (m)";
            // 
            // trackBar_centerOffset
            // 
            this.trackBar_centerOffset.LargeChange = 1;
            this.trackBar_centerOffset.Location = new System.Drawing.Point(191, 288);
            this.trackBar_centerOffset.Minimum = -10;
            this.trackBar_centerOffset.Name = "trackBar_centerOffset";
            this.trackBar_centerOffset.Size = new System.Drawing.Size(251, 45);
            this.trackBar_centerOffset.TabIndex = 36;
            this.trackBar_centerOffset.Scroll += new System.EventHandler(this.trackBar_centerOffset_Scroll);
            // 
            // btn_Adv_LaneDetcData
            // 
            this.btn_Adv_LaneDetcData.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Adv_LaneDetcData.Location = new System.Drawing.Point(25, 273);
            this.btn_Adv_LaneDetcData.Name = "btn_Adv_LaneDetcData";
            this.btn_Adv_LaneDetcData.Size = new System.Drawing.Size(124, 52);
            this.btn_Adv_LaneDetcData.TabIndex = 34;
            this.btn_Adv_LaneDetcData.Text = "Advertise LaneDetc_Data";
            this.btn_Adv_LaneDetcData.UseVisualStyleBackColor = true;
            this.btn_Adv_LaneDetcData.Click += new System.EventHandler(this.btn_Adv_LaneDetcData_Click);
            // 
            // button_ResetSteering
            // 
            this.button_ResetSteering.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ResetSteering.Location = new System.Drawing.Point(274, 190);
            this.button_ResetSteering.Name = "button_ResetSteering";
            this.button_ResetSteering.Size = new System.Drawing.Size(79, 44);
            this.button_ResetSteering.TabIndex = 31;
            this.button_ResetSteering.Text = "Reset Steering";
            this.button_ResetSteering.UseVisualStyleBackColor = true;
            this.button_ResetSteering.Click += new System.EventHandler(this.button_ResetSteering_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(308, 82);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(12, 16);
            this.label41.TabIndex = 18;
            this.label41.Text = "I";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(308, 159);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(12, 16);
            this.label33.TabIndex = 17;
            this.label33.Text = "I";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(208, 27);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(77, 15);
            this.label30.TabIndex = 30;
            this.label30.Text = "Speed Set:";
            // 
            // textBox_speedSetpoint
            // 
            this.textBox_speedSetpoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_speedSetpoint.Location = new System.Drawing.Point(291, 21);
            this.textBox_speedSetpoint.Name = "textBox_speedSetpoint";
            this.textBox_speedSetpoint.Size = new System.Drawing.Size(44, 26);
            this.textBox_speedSetpoint.TabIndex = 12;
            this.textBox_speedSetpoint.TabStop = false;
            this.textBox_speedSetpoint.Text = "0";
            this.textBox_speedSetpoint.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // trackBar_SreeingAng
            // 
            this.trackBar_SreeingAng.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.trackBar_SreeingAng.Location = new System.Drawing.Point(191, 139);
            this.trackBar_SreeingAng.Maximum = 70;
            this.trackBar_SreeingAng.Minimum = -70;
            this.trackBar_SreeingAng.Name = "trackBar_SreeingAng";
            this.trackBar_SreeingAng.Size = new System.Drawing.Size(245, 45);
            this.trackBar_SreeingAng.SmallChange = 2;
            this.trackBar_SreeingAng.TabIndex = 24;
            this.trackBar_SreeingAng.Scroll += new System.EventHandler(this.trackBar_SreeingAng_Scroll);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(195, 111);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(90, 15);
            this.label35.TabIndex = 27;
            this.label35.Text = "Steering Set:";
            // 
            // button_clearError
            // 
            this.button_clearError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_clearError.ForeColor = System.Drawing.Color.Red;
            this.button_clearError.Location = new System.Drawing.Point(25, 191);
            this.button_clearError.Name = "button_clearError";
            this.button_clearError.Size = new System.Drawing.Size(124, 44);
            this.button_clearError.TabIndex = 3;
            this.button_clearError.Text = "clearError";
            this.button_clearError.UseVisualStyleBackColor = true;
            this.button_clearError.Click += new System.EventHandler(this.button_clearError_Click);
            // 
            // textBox_SteeringAng
            // 
            this.textBox_SteeringAng.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_SteeringAng.Location = new System.Drawing.Point(291, 107);
            this.textBox_SteeringAng.Name = "textBox_SteeringAng";
            this.textBox_SteeringAng.Size = new System.Drawing.Size(95, 26);
            this.textBox_SteeringAng.TabIndex = 25;
            this.textBox_SteeringAng.TabStop = false;
            this.textBox_SteeringAng.Text = "0";
            this.textBox_SteeringAng.TextChanged += new System.EventHandler(this.textBox_SteeringAng_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(392, 114);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 15);
            this.label36.TabIndex = 26;
            this.label36.Text = "Rad";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(341, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 15);
            this.label10.TabIndex = 13;
            this.label10.Text = "km/h";
            // 
            // button_userReqStop
            // 
            this.button_userReqStop.Font = new System.Drawing.Font("Yu Gothic UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_userReqStop.ForeColor = System.Drawing.Color.Red;
            this.button_userReqStop.Location = new System.Drawing.Point(25, 148);
            this.button_userReqStop.Name = "button_userReqStop";
            this.button_userReqStop.Size = new System.Drawing.Size(124, 37);
            this.button_userReqStop.TabIndex = 2;
            this.button_userReqStop.Text = "userReqStop";
            this.button_userReqStop.UseVisualStyleBackColor = true;
            this.button_userReqStop.Click += new System.EventHandler(this.button_userReqStop_Click);
            // 
            // button_TurnSignal_Left
            // 
            this.button_TurnSignal_Left.Font = new System.Drawing.Font("MS Reference Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_TurnSignal_Left.Location = new System.Drawing.Point(212, 446);
            this.button_TurnSignal_Left.Name = "button_TurnSignal_Left";
            this.button_TurnSignal_Left.Size = new System.Drawing.Size(96, 23);
            this.button_TurnSignal_Left.TabIndex = 4;
            this.button_TurnSignal_Left.Text = "TurnSignal_Left";
            this.button_TurnSignal_Left.UseVisualStyleBackColor = true;
            this.button_TurnSignal_Left.Click += new System.EventHandler(this.button_TurnSignal_Left_Click);
            // 
            // button_userReqAutoRun
            // 
            this.button_userReqAutoRun.Font = new System.Drawing.Font("Yu Gothic UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_userReqAutoRun.ForeColor = System.Drawing.Color.Blue;
            this.button_userReqAutoRun.Location = new System.Drawing.Point(25, 72);
            this.button_userReqAutoRun.Name = "button_userReqAutoRun";
            this.button_userReqAutoRun.Size = new System.Drawing.Size(124, 37);
            this.button_userReqAutoRun.TabIndex = 1;
            this.button_userReqAutoRun.Text = "userReqAutoRun";
            this.button_userReqAutoRun.UseVisualStyleBackColor = true;
            this.button_userReqAutoRun.Click += new System.EventHandler(this.button_userReqAutoRun_Click);
            // 
            // button_Horn
            // 
            this.button_Horn.Font = new System.Drawing.Font("MS Reference Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Horn.Location = new System.Drawing.Point(279, 417);
            this.button_Horn.Name = "button_Horn";
            this.button_Horn.Size = new System.Drawing.Size(73, 23);
            this.button_Horn.TabIndex = 5;
            this.button_Horn.Text = "Horn";
            this.button_Horn.UseVisualStyleBackColor = true;
            this.button_Horn.Click += new System.EventHandler(this.button_Horn_Click);
            // 
            // button_userReqStart
            // 
            this.button_userReqStart.Font = new System.Drawing.Font("Yu Gothic UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_userReqStart.ForeColor = System.Drawing.Color.Blue;
            this.button_userReqStart.Location = new System.Drawing.Point(25, 27);
            this.button_userReqStart.Name = "button_userReqStart";
            this.button_userReqStart.Size = new System.Drawing.Size(124, 37);
            this.button_userReqStart.TabIndex = 0;
            this.button_userReqStart.Text = "userReqStart";
            this.button_userReqStart.UseVisualStyleBackColor = true;
            this.button_userReqStart.Click += new System.EventHandler(this.button_userReqStart_Click);
            // 
            // button_steeringRight
            // 
            this.button_steeringRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_steeringRight.Location = new System.Drawing.Point(357, 190);
            this.button_steeringRight.Name = "button_steeringRight";
            this.button_steeringRight.Size = new System.Drawing.Size(79, 44);
            this.button_steeringRight.TabIndex = 10;
            this.button_steeringRight.Text = "Steering_Right";
            this.button_steeringRight.UseVisualStyleBackColor = true;
            this.button_steeringRight.Click += new System.EventHandler(this.button_steeringRight_Click);
            // 
            // trackBar_speedSetpoint
            // 
            this.trackBar_speedSetpoint.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.trackBar_speedSetpoint.Location = new System.Drawing.Point(191, 62);
            this.trackBar_speedSetpoint.Maximum = 25;
            this.trackBar_speedSetpoint.Minimum = -25;
            this.trackBar_speedSetpoint.Name = "trackBar_speedSetpoint";
            this.trackBar_speedSetpoint.Size = new System.Drawing.Size(245, 45);
            this.trackBar_speedSetpoint.TabIndex = 6;
            this.trackBar_speedSetpoint.Scroll += new System.EventHandler(this.trackBar_speedSetpoint_Scroll);
            // 
            // button_steeringLeft
            // 
            this.button_steeringLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_steeringLeft.Location = new System.Drawing.Point(191, 190);
            this.button_steeringLeft.Name = "button_steeringLeft";
            this.button_steeringLeft.Size = new System.Drawing.Size(79, 44);
            this.button_steeringLeft.TabIndex = 9;
            this.button_steeringLeft.Text = "Steering_Left";
            this.button_steeringLeft.UseVisualStyleBackColor = true;
            this.button_steeringLeft.Click += new System.EventHandler(this.button_steeringLeft_Click);
            // 
            // button_TurnSignal_Right
            // 
            this.button_TurnSignal_Right.Font = new System.Drawing.Font("MS Reference Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_TurnSignal_Right.Location = new System.Drawing.Point(314, 446);
            this.button_TurnSignal_Right.Name = "button_TurnSignal_Right";
            this.button_TurnSignal_Right.Size = new System.Drawing.Size(105, 23);
            this.button_TurnSignal_Right.TabIndex = 7;
            this.button_TurnSignal_Right.Text = "TurnSignal_Right";
            this.button_TurnSignal_Right.UseVisualStyleBackColor = true;
            this.button_TurnSignal_Right.Click += new System.EventHandler(this.button_TurnSignal_Right_Click);
            // 
            // button_frontLight
            // 
            this.button_frontLight.Font = new System.Drawing.Font("MS Reference Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_frontLight.Location = new System.Drawing.Point(279, 472);
            this.button_frontLight.Name = "button_frontLight";
            this.button_frontLight.Size = new System.Drawing.Size(73, 23);
            this.button_frontLight.TabIndex = 8;
            this.button_frontLight.Text = "frontLight";
            this.button_frontLight.UseVisualStyleBackColor = true;
            this.button_frontLight.Click += new System.EventHandler(this.button_frontLight_Click);
            // 
            // button_AutoSteering
            // 
            this.button_AutoSteering.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AutoSteering.Location = new System.Drawing.Point(204, 26);
            this.button_AutoSteering.Name = "button_AutoSteering";
            this.button_AutoSteering.Size = new System.Drawing.Size(134, 33);
            this.button_AutoSteering.TabIndex = 16;
            this.button_AutoSteering.Text = "Auto Sterring";
            this.button_AutoSteering.UseVisualStyleBackColor = true;
            this.button_AutoSteering.Click += new System.EventHandler(this.button_AutoSteering_Click);
            // 
            // button_Advertise_GuiMsg
            // 
            this.button_Advertise_GuiMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Advertise_GuiMsg.Location = new System.Drawing.Point(191, 45);
            this.button_Advertise_GuiMsg.Name = "button_Advertise_GuiMsg";
            this.button_Advertise_GuiMsg.Size = new System.Drawing.Size(135, 37);
            this.button_Advertise_GuiMsg.TabIndex = 11;
            this.button_Advertise_GuiMsg.Text = "Advertise_GuiMsg";
            this.button_Advertise_GuiMsg.UseVisualStyleBackColor = true;
            this.button_Advertise_GuiMsg.Click += new System.EventHandler(this.button_Advertise_GuiMsg_Click);
            // 
            // SystemConnect
            // 
            this.SystemConnect.BackColor = System.Drawing.SystemColors.Highlight;
            this.SystemConnect.Controls.Add(this.groupBox_DriverControl);
            this.SystemConnect.Controls.Add(this.groupBox_DriverComPort);
            this.SystemConnect.Controls.Add(this.groupBox_ROS_System);
            this.SystemConnect.Controls.Add(this.groupBox2);
            this.SystemConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SystemConnect.Location = new System.Drawing.Point(4, 22);
            this.SystemConnect.Name = "SystemConnect";
            this.SystemConnect.Padding = new System.Windows.Forms.Padding(3);
            this.SystemConnect.Size = new System.Drawing.Size(871, 570);
            this.SystemConnect.TabIndex = 0;
            this.SystemConnect.Text = "ROSBridge";
            // 
            // groupBox_DriverControl
            // 
            this.groupBox_DriverControl.Controls.Add(this.button_AutoSteering);
            this.groupBox_DriverControl.Controls.Add(this.button_Servo_Off);
            this.groupBox_DriverControl.Controls.Add(this.label27);
            this.groupBox_DriverControl.Controls.Add(this.tBoxDataReceive);
            this.groupBox_DriverControl.Controls.Add(this.button_Servo_On);
            this.groupBox_DriverControl.Controls.Add(this.button_SetDriverCurrent);
            this.groupBox_DriverControl.Controls.Add(this.button_SetDriverPos);
            this.groupBox_DriverControl.Controls.Add(this.Set_Current);
            this.groupBox_DriverControl.Controls.Add(this.Set_Position);
            this.groupBox_DriverControl.Controls.Add(this.textBox_DriverCurrent);
            this.groupBox_DriverControl.Controls.Add(this.textBox_DriverPosition);
            this.groupBox_DriverControl.Controls.Add(this.button_Position_Mode);
            this.groupBox_DriverControl.Location = new System.Drawing.Point(294, 224);
            this.groupBox_DriverControl.Name = "groupBox_DriverControl";
            this.groupBox_DriverControl.Size = new System.Drawing.Size(552, 316);
            this.groupBox_DriverControl.TabIndex = 25;
            this.groupBox_DriverControl.TabStop = false;
            this.groupBox_DriverControl.Text = "Driver Control";
            this.groupBox_DriverControl.Visible = false;
            // 
            // button_Servo_Off
            // 
            this.button_Servo_Off.BackColor = System.Drawing.Color.Red;
            this.button_Servo_Off.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Servo_Off.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Servo_Off.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Servo_Off.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_Servo_Off.Location = new System.Drawing.Point(372, 26);
            this.button_Servo_Off.Name = "button_Servo_Off";
            this.button_Servo_Off.Size = new System.Drawing.Size(145, 33);
            this.button_Servo_Off.TabIndex = 29;
            this.button_Servo_Off.Text = "Servo Off";
            this.button_Servo_Off.UseVisualStyleBackColor = false;
            this.button_Servo_Off.Click += new System.EventHandler(this.button_Servo_Off_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(19, 140);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(116, 16);
            this.label27.TabIndex = 28;
            this.label27.Text = "Data Received:";
            // 
            // tBoxDataReceive
            // 
            this.tBoxDataReceive.BackColor = System.Drawing.Color.White;
            this.tBoxDataReceive.Location = new System.Drawing.Point(22, 159);
            this.tBoxDataReceive.Multiline = true;
            this.tBoxDataReceive.Name = "tBoxDataReceive";
            this.tBoxDataReceive.ReadOnly = true;
            this.tBoxDataReceive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBoxDataReceive.Size = new System.Drawing.Size(495, 87);
            this.tBoxDataReceive.TabIndex = 25;
            // 
            // button_Servo_On
            // 
            this.button_Servo_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_Servo_On.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Servo_On.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Servo_On.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Servo_On.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_Servo_On.Location = new System.Drawing.Point(23, 26);
            this.button_Servo_On.Name = "button_Servo_On";
            this.button_Servo_On.Size = new System.Drawing.Size(145, 33);
            this.button_Servo_On.TabIndex = 19;
            this.button_Servo_On.Text = "Servo On";
            this.button_Servo_On.UseVisualStyleBackColor = false;
            this.button_Servo_On.Click += new System.EventHandler(this.button_Servo_On_Click);
            // 
            // button_SetDriverCurrent
            // 
            this.button_SetDriverCurrent.BackColor = System.Drawing.Color.Silver;
            this.button_SetDriverCurrent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_SetDriverCurrent.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_SetDriverCurrent.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SetDriverCurrent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_SetDriverCurrent.Location = new System.Drawing.Point(437, 270);
            this.button_SetDriverCurrent.Name = "button_SetDriverCurrent";
            this.button_SetDriverCurrent.Size = new System.Drawing.Size(80, 33);
            this.button_SetDriverCurrent.TabIndex = 27;
            this.button_SetDriverCurrent.Text = "Set";
            this.button_SetDriverCurrent.UseVisualStyleBackColor = false;
            this.button_SetDriverCurrent.Click += new System.EventHandler(this.button_SetDriverCurrent_Click);
            // 
            // button_SetDriverPos
            // 
            this.button_SetDriverPos.BackColor = System.Drawing.Color.Silver;
            this.button_SetDriverPos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_SetDriverPos.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_SetDriverPos.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SetDriverPos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_SetDriverPos.Location = new System.Drawing.Point(322, 88);
            this.button_SetDriverPos.Name = "button_SetDriverPos";
            this.button_SetDriverPos.Size = new System.Drawing.Size(80, 33);
            this.button_SetDriverPos.TabIndex = 24;
            this.button_SetDriverPos.Text = "Set";
            this.button_SetDriverPos.UseVisualStyleBackColor = false;
            this.button_SetDriverPos.Click += new System.EventHandler(this.button_SetDriverPos_Click);
            // 
            // Set_Current
            // 
            this.Set_Current.AutoSize = true;
            this.Set_Current.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Set_Current.Location = new System.Drawing.Point(229, 282);
            this.Set_Current.Name = "Set_Current";
            this.Set_Current.Size = new System.Drawing.Size(83, 15);
            this.Set_Current.TabIndex = 26;
            this.Set_Current.Text = "Set_Current";
            // 
            // Set_Position
            // 
            this.Set_Position.AutoSize = true;
            this.Set_Position.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Set_Position.Location = new System.Drawing.Point(119, 98);
            this.Set_Position.Name = "Set_Position";
            this.Set_Position.Size = new System.Drawing.Size(88, 15);
            this.Set_Position.TabIndex = 25;
            this.Set_Position.Text = "Set_Position";
            // 
            // textBox_DriverCurrent
            // 
            this.textBox_DriverCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DriverCurrent.Location = new System.Drawing.Point(322, 279);
            this.textBox_DriverCurrent.Name = "textBox_DriverCurrent";
            this.textBox_DriverCurrent.Size = new System.Drawing.Size(100, 21);
            this.textBox_DriverCurrent.TabIndex = 24;
            this.textBox_DriverCurrent.Text = "2.5";
            // 
            // textBox_DriverPosition
            // 
            this.textBox_DriverPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DriverPosition.Location = new System.Drawing.Point(216, 95);
            this.textBox_DriverPosition.Name = "textBox_DriverPosition";
            this.textBox_DriverPosition.Size = new System.Drawing.Size(100, 21);
            this.textBox_DriverPosition.TabIndex = 23;
            this.textBox_DriverPosition.Text = "0.0";
            // 
            // button_Position_Mode
            // 
            this.button_Position_Mode.BackColor = System.Drawing.Color.Silver;
            this.button_Position_Mode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Position_Mode.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Position_Mode.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Position_Mode.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_Position_Mode.Location = new System.Drawing.Point(23, 267);
            this.button_Position_Mode.Name = "button_Position_Mode";
            this.button_Position_Mode.Size = new System.Drawing.Size(145, 33);
            this.button_Position_Mode.TabIndex = 22;
            this.button_Position_Mode.Text = "Position Mode";
            this.button_Position_Mode.UseVisualStyleBackColor = false;
            this.button_Position_Mode.Click += new System.EventHandler(this.button_Position_Mode_Click);
            // 
            // groupBox_DriverComPort
            // 
            this.groupBox_DriverComPort.Controls.Add(this.progressBar1);
            this.groupBox_DriverComPort.Controls.Add(this.cBoxPatityBits);
            this.groupBox_DriverComPort.Controls.Add(this.groupBox10);
            this.groupBox_DriverComPort.Controls.Add(this.cBoxStopBits);
            this.groupBox_DriverComPort.Controls.Add(this.btnClose);
            this.groupBox_DriverComPort.Controls.Add(this.cBoxDataBits);
            this.groupBox_DriverComPort.Controls.Add(this.btnOpen);
            this.groupBox_DriverComPort.Controls.Add(this.cBoxBaudRate);
            this.groupBox_DriverComPort.Controls.Add(this.cBoxComPort);
            this.groupBox_DriverComPort.Controls.Add(this.label19);
            this.groupBox_DriverComPort.Controls.Add(this.label20);
            this.groupBox_DriverComPort.Controls.Add(this.label24);
            this.groupBox_DriverComPort.Controls.Add(this.label25);
            this.groupBox_DriverComPort.Controls.Add(this.label26);
            this.groupBox_DriverComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_DriverComPort.ForeColor = System.Drawing.Color.Black;
            this.groupBox_DriverComPort.Location = new System.Drawing.Point(23, 224);
            this.groupBox_DriverComPort.Name = "groupBox_DriverComPort";
            this.groupBox_DriverComPort.Size = new System.Drawing.Size(244, 316);
            this.groupBox_DriverComPort.TabIndex = 15;
            this.groupBox_DriverComPort.TabStop = false;
            this.groupBox_DriverComPort.Text = "Com Port Control";
            // 
            // progressBar1
            // 
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.progressBar1.Location = new System.Drawing.Point(42, 270);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(156, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // cBoxPatityBits
            // 
            this.cBoxPatityBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxPatityBits.FormattingEnabled = true;
            this.cBoxPatityBits.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.cBoxPatityBits.Location = new System.Drawing.Point(110, 146);
            this.cBoxPatityBits.Name = "cBoxPatityBits";
            this.cBoxPatityBits.Size = new System.Drawing.Size(109, 23);
            this.cBoxPatityBits.TabIndex = 17;
            this.cBoxPatityBits.Text = "None";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lbStaticComPort);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(110, 182);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(109, 69);
            this.groupBox10.TabIndex = 19;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "State Com Port";
            // 
            // lbStaticComPort
            // 
            this.lbStaticComPort.AutoSize = true;
            this.lbStaticComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStaticComPort.Location = new System.Drawing.Point(16, 25);
            this.lbStaticComPort.Name = "lbStaticComPort";
            this.lbStaticComPort.Size = new System.Drawing.Size(72, 31);
            this.lbStaticComPort.TabIndex = 22;
            this.lbStaticComPort.Text = "OFF";
            // 
            // cBoxStopBits
            // 
            this.cBoxStopBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxStopBits.FormattingEnabled = true;
            this.cBoxStopBits.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.cBoxStopBits.Location = new System.Drawing.Point(110, 116);
            this.cBoxStopBits.Name = "cBoxStopBits";
            this.cBoxStopBits.Size = new System.Drawing.Size(109, 23);
            this.cBoxStopBits.TabIndex = 16;
            this.cBoxStopBits.Text = "One";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(23, 224);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 27);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cBoxDataBits
            // 
            this.cBoxDataBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxDataBits.FormattingEnabled = true;
            this.cBoxDataBits.Items.AddRange(new object[] {
            "6",
            "7",
            "8"});
            this.cBoxDataBits.Location = new System.Drawing.Point(110, 87);
            this.cBoxDataBits.Name = "cBoxDataBits";
            this.cBoxDataBits.Size = new System.Drawing.Size(109, 23);
            this.cBoxDataBits.TabIndex = 15;
            this.cBoxDataBits.Text = "8";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.Lime;
            this.btnOpen.Location = new System.Drawing.Point(23, 189);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 29);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "OPEN";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // cBoxBaudRate
            // 
            this.cBoxBaudRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxBaudRate.FormattingEnabled = true;
            this.cBoxBaudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200"});
            this.cBoxBaudRate.Location = new System.Drawing.Point(110, 56);
            this.cBoxBaudRate.Name = "cBoxBaudRate";
            this.cBoxBaudRate.Size = new System.Drawing.Size(109, 23);
            this.cBoxBaudRate.TabIndex = 14;
            this.cBoxBaudRate.Text = "115200";
            // 
            // cBoxComPort
            // 
            this.cBoxComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxComPort.FormattingEnabled = true;
            this.cBoxComPort.Location = new System.Drawing.Point(110, 26);
            this.cBoxComPort.Name = "cBoxComPort";
            this.cBoxComPort.Size = new System.Drawing.Size(109, 23);
            this.cBoxComPort.TabIndex = 13;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 151);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 16);
            this.label19.TabIndex = 12;
            this.label19.Text = "Parity bit";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 117);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 16);
            this.label20.TabIndex = 11;
            this.label20.Text = "Stop bit";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 88);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 16);
            this.label24.TabIndex = 10;
            this.label24.Text = "Data bits";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(15, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 16);
            this.label25.TabIndex = 9;
            this.label25.Text = "Baudrate";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 30);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 16);
            this.label26.TabIndex = 8;
            this.label26.Text = "COM Port";
            // 
            // groupBox_ROS_System
            // 
            this.groupBox_ROS_System.Controls.Add(this.btn_Sub_MPC);
            this.groupBox_ROS_System.Controls.Add(this.btn_Sub_All);
            this.groupBox_ROS_System.Controls.Add(this.button_Advertise_GuiMsg);
            this.groupBox_ROS_System.Location = new System.Drawing.Point(294, 9);
            this.groupBox_ROS_System.Name = "groupBox_ROS_System";
            this.groupBox_ROS_System.Size = new System.Drawing.Size(552, 193);
            this.groupBox_ROS_System.TabIndex = 9;
            this.groupBox_ROS_System.TabStop = false;
            this.groupBox_ROS_System.Text = "ROS Command";
            this.groupBox_ROS_System.Visible = false;
            // 
            // btn_Sub_MPC
            // 
            this.btn_Sub_MPC.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Sub_MPC.Location = new System.Drawing.Point(33, 100);
            this.btn_Sub_MPC.Name = "btn_Sub_MPC";
            this.btn_Sub_MPC.Size = new System.Drawing.Size(135, 37);
            this.btn_Sub_MPC.TabIndex = 48;
            this.btn_Sub_MPC.Text = "Subscribe MPC";
            this.btn_Sub_MPC.UseVisualStyleBackColor = true;
            this.btn_Sub_MPC.Click += new System.EventHandler(this.btn_Sub_MPC_Click);
            // 
            // btn_Sub_All
            // 
            this.btn_Sub_All.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Sub_All.Location = new System.Drawing.Point(33, 44);
            this.btn_Sub_All.Name = "btn_Sub_All";
            this.btn_Sub_All.Size = new System.Drawing.Size(135, 37);
            this.btn_Sub_All.TabIndex = 47;
            this.btn_Sub_All.Text = "Subscribe All";
            this.btn_Sub_All.UseVisualStyleBackColor = true;
            this.btn_Sub_All.Click += new System.EventHandler(this.btn_Sub_All_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btn_Connect);
            this.groupBox2.Controls.Add(this.txt_Port);
            this.groupBox2.Controls.Add(this.txt_IP);
            this.groupBox2.Controls.Add(this.shapeContainer1);
            this.groupBox2.Location = new System.Drawing.Point(23, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 193);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ROS Connection";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(21, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Port";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(21, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "IP";
            // 
            // btn_Connect
            // 
            this.btn_Connect.BackColor = System.Drawing.Color.Lime;
            this.btn_Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Connect.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Connect.Location = new System.Drawing.Point(23, 131);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(75, 31);
            this.btn_Connect.TabIndex = 2;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.UseVisualStyleBackColor = false;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // txt_Port
            // 
            this.txt_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Port.Location = new System.Drawing.Point(53, 82);
            this.txt_Port.Name = "txt_Port";
            this.txt_Port.Size = new System.Drawing.Size(75, 21);
            this.txt_Port.TabIndex = 1;
            this.txt_Port.Text = "9091";
            // 
            // txt_IP
            // 
            this.txt_IP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IP.Location = new System.Drawing.Point(53, 44);
            this.txt_IP.Name = "txt_IP";
            this.txt_IP.Size = new System.Drawing.Size(156, 21);
            this.txt_IP.TabIndex = 0;
            this.txt_IP.Text = "192.168.0.2";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 18);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ovalShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(238, 172);
            this.shapeContainer1.TabIndex = 5;
            this.shapeContainer1.TabStop = false;
            // 
            // AEV_Tab
            // 
            this.AEV_Tab.Controls.Add(this.SystemConnect);
            this.AEV_Tab.Controls.Add(this.CarControl);
            this.AEV_Tab.Controls.Add(this.TestCar);
            this.AEV_Tab.Location = new System.Drawing.Point(12, 12);
            this.AEV_Tab.Name = "AEV_Tab";
            this.AEV_Tab.SelectedIndex = 0;
            this.AEV_Tab.Size = new System.Drawing.Size(879, 596);
            this.AEV_Tab.TabIndex = 6;
            // 
            // groupBox_ControlStatus
            // 
            this.groupBox_ControlStatus.Controls.Add(this.error_cnt_mpc_msg);
            this.groupBox_ControlStatus.Controls.Add(this.label1);
            this.groupBox_ControlStatus.Controls.Add(this.label_SteeringAngleMsg);
            this.groupBox_ControlStatus.Controls.Add(this.Status_ros_advertise);
            this.groupBox_ControlStatus.Controls.Add(this.Status_steering_auto);
            this.groupBox_ControlStatus.Controls.Add(this.Status_steering_ON);
            this.groupBox_ControlStatus.Controls.Add(this.Status_steering_driver);
            this.groupBox_ControlStatus.Controls.Add(this.Status_ros_subscribe);
            this.groupBox_ControlStatus.Controls.Add(this.Status_ros_connection);
            this.groupBox_ControlStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_ControlStatus.Location = new System.Drawing.Point(899, 27);
            this.groupBox_ControlStatus.Name = "groupBox_ControlStatus";
            this.groupBox_ControlStatus.Size = new System.Drawing.Size(200, 577);
            this.groupBox_ControlStatus.TabIndex = 32;
            this.groupBox_ControlStatus.TabStop = false;
            this.groupBox_ControlStatus.Text = "GUI Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(7, 434);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 130);
            this.label1.TabIndex = 6;
            this.label1.Text = "W: Increase speed\r\nS: Decrease speed\r\nA: Manual steering Left\r\nD: Manual steering" +
    " Right\r\nP: Switch AutoSteering\r\nL: Manual reset steering\r\nO: ON/OFF Motor steeri" +
    "ng\r\nSpace: Stop the car";
            // 
            // Status_ros_advertise
            // 
            this.Status_ros_advertise.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_ros_advertise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_ros_advertise.Location = new System.Drawing.Point(23, 98);
            this.Status_ros_advertise.Name = "Status_ros_advertise";
            this.Status_ros_advertise.Size = new System.Drawing.Size(155, 32);
            this.Status_ros_advertise.TabIndex = 5;
            this.Status_ros_advertise.Text = "Status_ros_advertise";
            this.Status_ros_advertise.UseVisualStyleBackColor = false;
            // 
            // Status_steering_auto
            // 
            this.Status_steering_auto.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_steering_auto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_steering_auto.Location = new System.Drawing.Point(23, 234);
            this.Status_steering_auto.Name = "Status_steering_auto";
            this.Status_steering_auto.Size = new System.Drawing.Size(155, 32);
            this.Status_steering_auto.TabIndex = 4;
            this.Status_steering_auto.Text = "Status_steering_auto";
            this.Status_steering_auto.UseVisualStyleBackColor = false;
            // 
            // Status_steering_ON
            // 
            this.Status_steering_ON.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_steering_ON.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_steering_ON.Location = new System.Drawing.Point(23, 197);
            this.Status_steering_ON.Name = "Status_steering_ON";
            this.Status_steering_ON.Size = new System.Drawing.Size(155, 32);
            this.Status_steering_ON.TabIndex = 3;
            this.Status_steering_ON.Text = "Status_steering_ON";
            this.Status_steering_ON.UseVisualStyleBackColor = false;
            // 
            // Status_steering_driver
            // 
            this.Status_steering_driver.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_steering_driver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_steering_driver.Location = new System.Drawing.Point(23, 160);
            this.Status_steering_driver.Name = "Status_steering_driver";
            this.Status_steering_driver.Size = new System.Drawing.Size(155, 32);
            this.Status_steering_driver.TabIndex = 2;
            this.Status_steering_driver.Text = "Status_steering_driver";
            this.Status_steering_driver.UseVisualStyleBackColor = false;
            // 
            // Status_ros_subscribe
            // 
            this.Status_ros_subscribe.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_ros_subscribe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_ros_subscribe.Location = new System.Drawing.Point(23, 63);
            this.Status_ros_subscribe.Name = "Status_ros_subscribe";
            this.Status_ros_subscribe.Size = new System.Drawing.Size(155, 32);
            this.Status_ros_subscribe.TabIndex = 1;
            this.Status_ros_subscribe.Text = "Status_ros_subscribe";
            this.Status_ros_subscribe.UseVisualStyleBackColor = false;
            // 
            // Status_ros_connection
            // 
            this.Status_ros_connection.BackColor = System.Drawing.Color.OrangeRed;
            this.Status_ros_connection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status_ros_connection.Location = new System.Drawing.Point(23, 28);
            this.Status_ros_connection.Name = "Status_ros_connection";
            this.Status_ros_connection.Size = new System.Drawing.Size(155, 32);
            this.Status_ros_connection.TabIndex = 0;
            this.Status_ros_connection.Text = "Status_ros_connection";
            this.Status_ros_connection.UseVisualStyleBackColor = false;
            // 
            // AEV_GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1111, 650);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox_ControlStatus);
            this.Controls.Add(this.textBox_SysInfo);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.AEV_Tab);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.KeyPreview = true;
            this.Name = "AEV_GUI";
            this.Text = "AEV_GUI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AEV_GUI_KeyDown);
            this.TestCar.ResumeLayout(false);
            this.TestCar.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_YawRate)).EndInit();
            this.CarControl.ResumeLayout(false);
            this.groupBox_CarStatus.ResumeLayout(false);
            this.groupBox_CarStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_actualSpeed)).EndInit();
            this.groupBox_CarControl.ResumeLayout(false);
            this.groupBox_CarControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_Curvature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_centerOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_SreeingAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_speedSetpoint)).EndInit();
            this.SystemConnect.ResumeLayout(false);
            this.groupBox_DriverControl.ResumeLayout(false);
            this.groupBox_DriverControl.PerformLayout();
            this.groupBox_DriverComPort.ResumeLayout(false);
            this.groupBox_DriverComPort.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox_ROS_System.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.AEV_Tab.ResumeLayout(false);
            this.groupBox_ControlStatus.ResumeLayout(false);
            this.groupBox_ControlStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.TextBox textBox_SysInfo;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape Notice_AcceleratorSwitch;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape Notice_BreakSwitch;
        private Microsoft.VisualBasic.PowerPacks.OvalShape Notice_Error;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovalShape1;
        private System.Windows.Forms.TabPage TestCar;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label SystemMonitor_ErrorInfo;
        private System.Windows.Forms.Label SystemMonitor_errorFlag;
        private System.Windows.Forms.Label SystemMonitor_StopReqFlag;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label Radar_msg_cnt;
        private System.Windows.Forms.Label Radar_isObject;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label MPC_msg_cnt;
        private System.Windows.Forms.Label MPC_Steering;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label objDetc_yawRate;
        private System.Windows.Forms.Label objDetc_msg_cnt;
        private System.Windows.Forms.Label objDetc_isObj;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label ddhdhd;
        private System.Windows.Forms.Label label_YawRateVal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TrackBar trackBar_YawRate;
        private System.Windows.Forms.Button btn_Adv_ObjDetcData;
        private System.Windows.Forms.TabPage CarControl;
        private System.Windows.Forms.Button button_AutoSteering;
        private System.Windows.Forms.GroupBox groupBox_CarStatus;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox_ActualSteeringAng;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label_MovingDirection;
        private System.Windows.Forms.Label label_BrakeSwitch;
        private System.Windows.Forms.Label label_AcceleratorSwitch;
        private System.Windows.Forms.Label label_AcceleratorLevel;
        private System.Windows.Forms.Label label_DrivingMode;
        private System.Windows.Forms.TextBox textBox_actualSpeed;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TrackBar trackBar_actualSpeed;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private System.Windows.Forms.Button button_Advertise_GuiMsg;
        private System.Windows.Forms.GroupBox groupBox_CarControl;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox_speedSetpoint;
        private System.Windows.Forms.TrackBar trackBar_SreeingAng;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button_clearError;
        private System.Windows.Forms.TextBox textBox_SteeringAng;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button_userReqStop;
        private System.Windows.Forms.Button button_TurnSignal_Left;
        private System.Windows.Forms.Button button_userReqAutoRun;
        private System.Windows.Forms.Button button_Horn;
        private System.Windows.Forms.Button button_userReqStart;
        private System.Windows.Forms.Button button_steeringRight;
        private System.Windows.Forms.TrackBar trackBar_speedSetpoint;
        private System.Windows.Forms.Button button_steeringLeft;
        private System.Windows.Forms.Button button_TurnSignal_Right;
        private System.Windows.Forms.Button button_frontLight;
        private System.Windows.Forms.TabPage SystemConnect;
        private System.Windows.Forms.GroupBox groupBox_ROS_System;
        private System.Windows.Forms.Button btn_Sub_All;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.TextBox txt_Port;
        private System.Windows.Forms.TextBox txt_IP;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.TabControl AEV_Tab;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangle_isRadarObj;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangle_isCameraObj;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button button_ResetSteering;
        private System.Windows.Forms.Label Radar_Distance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_Adv_LaneDetcData;
        private System.Windows.Forms.Label label_LateralDeviationVal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trackBar_centerOffset;
        private System.Windows.Forms.GroupBox groupBox_DriverComPort;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ComboBox cBoxPatityBits;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lbStaticComPort;
        private System.Windows.Forms.ComboBox cBoxStopBits;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cBoxDataBits;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ComboBox cBoxBaudRate;
        private System.Windows.Forms.ComboBox cBoxComPort;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox_DriverControl;
        private System.Windows.Forms.Button button_Servo_Off;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tBoxDataReceive;
        private System.Windows.Forms.Button button_Servo_On;
        private System.Windows.Forms.Button button_SetDriverCurrent;
        private System.Windows.Forms.Button button_SetDriverPos;
        private System.Windows.Forms.Label Set_Current;
        private System.Windows.Forms.Label Set_Position;
        private System.Windows.Forms.TextBox textBox_DriverCurrent;
        private System.Windows.Forms.TextBox textBox_DriverPosition;
        private System.Windows.Forms.Button button_Position_Mode;
        private System.Windows.Forms.Label label_CurvatureVal;
        private System.Windows.Forms.TrackBar trackBar_Curvature;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox_ControlStatus;
        private System.Windows.Forms.Button Status_ros_connection;
        private System.Windows.Forms.Button Status_ros_subscribe;
        private System.Windows.Forms.Button Status_steering_auto;
        private System.Windows.Forms.Button Status_steering_ON;
        private System.Windows.Forms.Button Status_steering_driver;
        private System.Windows.Forms.Button Status_ros_advertise;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Radar_Steering;
        private System.Windows.Forms.Label Radar_Speed;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_SteeringAngleMsg;
        private System.Windows.Forms.Label error_cnt_mpc_msg;
        private System.Windows.Forms.Button btn_Sub_MPC;
    }
}

