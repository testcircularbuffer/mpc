﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;
using System.IO.Ports;
using RosBridgeDotNet;
using static RosBridgeDotNet.RosBridgeDotNet;
using static RosBridgeDotNet.AEV_Comm;

namespace nonRos_Bridge
{
    public partial class AEV_GUI : Form
    {
        const float PI = 3.141592653f;
        const float STEERING_FACTOR = 10 * 2 * PI * 46 / 20 * 1.5f; // 

        bool AutoSteering_Flag = false;
        bool AdvertiseGuiMsg_Flag = false;
        public Socket m_clientSocket;

        public AEV_GUI()
        {
            InitializeComponent();
            {
                Load += new EventHandler(this.SocketClient_Load);
            }
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void SocketClient_Load(object sender, EventArgs e)
        {
            //txt_IP.Text = "192.168.100.2";
            txt_Port.Text = "9091";
        }
        int counter_error = 0;
        void ReceiveUTF8()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1000];
                    m_clientSocket.Receive(data);
                    string message = Encoding.UTF8.GetString(data);
                    bool is_Msg_Processed = false;

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            MPC_DataJson MPC_Out_Data = new MPC_DataJson();
                            MPC_Out_Data = JsonConvert.DeserializeObject<MPC_DataJson>(message);
                            if (MPC_Out_Data.topic == "/MPC_Data")
                            {
                                MPC_msg_cnt.Text = MPC_Out_Data.msg.msg_counter.ToString();
                                MPC_Steering.Text = MPC_Out_Data.msg.SteeringAngle.ToString();

                                label_SteeringAngleMsg.Text = (-MPC_Out_Data.msg.SteeringAngle).ToString();

                                if (AutoSteering_Flag == true)
                                {
                                    SetSteeringDriverPos(-MPC_Out_Data.msg.SteeringAngle);
                                    
                                }
                                else
                                {

                                }
                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                        counter_error++;
                        error_cnt_mpc_msg.Text = counter_error.ToString();
                    }

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            Obstruction_IMU_DataJson Obstruction_Data = new Obstruction_IMU_DataJson();
                            Obstruction_Data = JsonConvert.DeserializeObject<Obstruction_IMU_DataJson>(message);
                            if (Obstruction_Data.topic == "/ObjDetc_Data")
                            {
                                objDetc_msg_cnt.Text = Obstruction_Data.msg.msg_counter.ToString();
                                objDetc_yawRate.Text = Obstruction_Data.msg.yaw_rate.ToString();
                                if (Obstruction_Data.msg.isObject == true)
                                {
                                    objDetc_isObj.Text = "yes";
                                    rectangle_isCameraObj.BackColor = Color.Red;
                                }
                                else
                                {
                                    objDetc_isObj.Text = "no";
                                    rectangle_isCameraObj.BackColor = Color.Gray;
                                }
                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            Radar_DataJson Radar_Out_Data = new Radar_DataJson();
                            Radar_Out_Data = JsonConvert.DeserializeObject<Radar_DataJson>(message);
                            if (Radar_Out_Data.topic == "/Radar_Data")
                            {
                                Radar_msg_cnt.Text = Radar_Out_Data.msg.msg_counter.ToString();
                                Radar_isObject.Text = Radar_Out_Data.msg.isObject.ToString();
                                Radar_Distance.Text = Radar_Out_Data.msg.distance.ToString();
                                Radar_Steering.Text = Radar_Out_Data.msg.ttcSteering.ToString();
                                Radar_Speed.Text = Radar_Out_Data.msg.ttcSpeed.ToString();

                                if (Radar_Out_Data.msg.isObject == true)
                                {
                                    rectangle_isRadarObj.BackColor = Color.Red;
                                }
                                else
                                {
                                    rectangle_isRadarObj.BackColor = Color.Gray;
                                }

                                if (AutoSteering_Flag == true)
                                {
                                    SetSteeringDriverPos(-Radar_Out_Data.msg.ttcSteering);
                                }
                                else
                                {

                                }

                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            SystemMonitor_DataJson SystemMonitor_Out_Data = new SystemMonitor_DataJson();
                            SystemMonitor_Out_Data = JsonConvert.DeserializeObject<SystemMonitor_DataJson>(message);
                            if (SystemMonitor_Out_Data.topic == "/SystemMonitor_Data")
                            {
                                if (SystemMonitor_Out_Data.msg.errorFlag)
                                {
                                    Notice_Error.BackColor = Color.Red;
                                }
                                else
                                {
                                    Notice_Error.BackColor = Color.Gray;
                                }

                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            DrivingModeSel_DataJson DrivingMode_Out_Data = new DrivingModeSel_DataJson();
                            DrivingMode_Out_Data = JsonConvert.DeserializeObject<DrivingModeSel_DataJson>(message);
                            if (DrivingMode_Out_Data.topic == "/DrivingMode_Data")
                            {
                                if (DrivingMode_Out_Data.msg.drivingMode == 1)
                                {
                                    label_DrivingMode.Text = "DrivingMode: Stop Mode";
                                    label_DrivingMode.BackColor = Color.Orange;
                                }
                                else if (DrivingMode_Out_Data.msg.drivingMode == 2)
                                {
                                    label_DrivingMode.Text = "DrivingMode: Normal Mode";
                                    label_DrivingMode.BackColor = Color.Green;
                                }
                                else if (DrivingMode_Out_Data.msg.drivingMode == 3)
                                {
                                    label_DrivingMode.Text = "DrivingMode: LaneChange Mode";
                                    label_DrivingMode.BackColor = Color.Green;
                                }
                                else if (DrivingMode_Out_Data.msg.drivingMode == 4)
                                {
                                    label_DrivingMode.Text = "DrivingMode: Deceleration Mode";
                                    label_DrivingMode.BackColor = Color.Blue;
                                }
                                else if (DrivingMode_Out_Data.msg.drivingMode == 5)
                                {
                                    label_DrivingMode.Text = "DrivingMode: EmergecyStop Mode";
                                    label_DrivingMode.BackColor = Color.Red;
                                }

                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }

                    try
                    {
                        if (is_Msg_Processed == false)
                        {
                            EcuFeedback_DataJson EcuFeedback_Out_Data = new EcuFeedback_DataJson();
                            EcuFeedback_Out_Data = JsonConvert.DeserializeObject<EcuFeedback_DataJson>(message);
                            if (EcuFeedback_Out_Data.topic == "/ECU_Feedback_Data")
                            {
                                label_AcceleratorLevel.Text = "AcceleratorLevel: " + EcuFeedback_Out_Data.msg.acceleratorLevel.ToString() + " %";

                                if (EcuFeedback_Out_Data.msg.acceleratorSwitch)
                                {
                                    Notice_AcceleratorSwitch.BackColor = Color.Green;
                                }
                                else
                                {
                                    Notice_AcceleratorSwitch.BackColor = Color.Gray;
                                }

                                if (EcuFeedback_Out_Data.msg.brakeSwitch)
                                {
                                    Notice_BreakSwitch.BackColor = Color.Red;
                                }
                                else
                                {
                                    Notice_BreakSwitch.BackColor = Color.Gray;
                                }

                                if (EcuFeedback_Out_Data.msg.movingDirection)
                                {
                                    label_MovingDirection.Text = "MovingDirection: BackWard";
                                }
                                else
                                {
                                    label_MovingDirection.Text = "MovingDirection: ForWard";
                                }

                                float[] feedbackSpeed = new float[1];
                                var byteArray = new byte[4];
                                byteArray[0] = EcuFeedback_Out_Data.msg.feedbackSpeed_b1;
                                byteArray[1] = EcuFeedback_Out_Data.msg.feedbackSpeed_b2;
                                byteArray[2] = EcuFeedback_Out_Data.msg.feedbackSpeed_b3;
                                byteArray[3] = EcuFeedback_Out_Data.msg.feedbackSpeed_b4;
                                Buffer.BlockCopy(byteArray, 0, feedbackSpeed, 0, byteArray.Length);
                                textBox_actualSpeed.Text = ((Int16)feedbackSpeed[0]).ToString();

                                if ((feedbackSpeed[0] <= 25) && (feedbackSpeed[0] >= -25))
                                {
                                    trackBar_actualSpeed.Value = (Int16)feedbackSpeed[0];
                                }

                                is_Msg_Processed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                Close();
            }
        }

        private void sendToTcp(string needToSend)
        {
            try
            {
                if (m_clientSocket != null)
                {
                    if (m_clientSocket.Connected)
                    {
                        m_clientSocket.Send(Encoding.UTF8.GetBytes(needToSend));
                    }
                    else
                    {
                        ovalShape1.BackColor = Color.Red;
                        groupBox_ROS_System.Visible = false;
                        groupBox_DriverComPort.Visible = false;
                        groupBox_CarControl.Visible = false;
                        groupBox_CarStatus.Visible = false;
                    }
                        
                }
            }
            catch (SocketException se)
            {

                MessageBox.Show(se.Message);
            }
        }

        void Ros_Sub_MPC_Data()
        {
            string op = "subscribe";
            string id = null;
            string compression = "none";
            string type;
            string topic;
            SubscribeMessage m;
            string needToSend;

            type = "aev_pkg/mpc_msg";
            topic = "/MPC_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);

            btn_Sub_MPC.BackColor = Color.Lime;
        }
        private void btn_Sub_MPC_Click(object sender, EventArgs e)
        {
            Ros_Sub_MPC_Data();
        }


        void Ros_Sub_All()
        {
            btn_Sub_All.Text = "Unsubscribe";
            btn_Sub_All.BackColor = Color.Red;
            string op = "subscribe";
            string id = null;
            string compression = "none";

            string type;
            string topic;
            SubscribeMessage m;
            string needToSend;

            type = "aev_pkg/object_detection_msg";
            topic = "/ObjDetc_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(100);

            type = "aev_pkg/radar_msg";
            topic = "/Radar_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(100);

            //type = "aev_pkg/mpc_msg";
            //topic = "/MPC_Data";
            //m = new SubscribeMessage(op, id, topic, type, compression);
            //needToSend = JsonConvert.SerializeObject(m);
            //sendToTcp(needToSend);
            //Thread.Sleep(100);

            type = "aev_pkg/system_monitor_msg";
            topic = "/SystemMonitor_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(100);

            type = "aev_pkg/ecu_feedback_msg";
            topic = "/ECU_Feedback_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(100);

            type = "aev_pkg/driving_mode_msg";
            topic = "/DrivingMode_Data";
            m = new SubscribeMessage(op, id, topic, type, compression);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        void Ros_UnSub_All()
        {
            btn_Sub_All.Text = "Subscribe All";
            btn_Sub_All.BackColor = Color.Lime;
            string op = "unsubscribe";
            string topic;
            UnSubscribeMessage m;
            string needToSend;

            topic = "/ObjDetc_Data";
            m = new UnSubscribeMessage(op, topic);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(50);

            topic = "/Radar_Data";
            m = new UnSubscribeMessage(op, topic);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(50);

            //topic = "/MPC_Data";
            //m = new UnSubscribeMessage(op, topic);
            //needToSend = JsonConvert.SerializeObject(m);
            //sendToTcp(needToSend);
            //Thread.Sleep(50);

            topic = "/SystemMonitor_Data";
            m = new UnSubscribeMessage(op, topic);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(50);

            topic = "/ECU_Feedback_Data";
            m = new UnSubscribeMessage(op, topic);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
            Thread.Sleep(50);

            topic = "/DrivingMode_Data";
            m = new UnSubscribeMessage(op, topic);
            needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        private void btn_Sub_All_Click(object sender, EventArgs e)
        {
            if (btn_Sub_All.Text == "Subscribe All")
            {
                Ros_Sub_All();
            }
            else
            {
                Ros_UnSub_All();
            }
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            // See if we have text on the IP and Port text fields
            if (txt_IP.Text == "" || txt_Port.Text == "")
            {
                MessageBox.Show("IP Address and Port Number are required to connect to the Server\n");
                return;
            }
            try
            {
                m_clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Cet the remote IP address
                IPAddress ip = IPAddress.Parse(txt_IP.Text);
                int iPortNo = System.Convert.ToInt16(txt_Port.Text);
                // Create the end point 
                IPEndPoint ipEnd = new IPEndPoint(ip, iPortNo);
                // Connect to the remote host
                m_clientSocket.Connect(ipEnd); //UpdateControls;
                if (m_clientSocket.Connected)
                {
                }

                m_clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                byte[] handShake = Encoding.UTF8.GetBytes("raw\r\n\r\n");
                m_clientSocket.Send(handShake);
                ovalShape1.BackColor = Color.Lime;
                groupBox_ROS_System.Visible = Enabled;
                groupBox_DriverComPort.Visible = Enabled;

                Thread.Sleep(50);
                //Ros_Sub_All();
                //Thread.Sleep(50);
                //Advertise_GuiMsg();

            }
            catch (SocketException se)
            {
                string str;
                str = "\nConnection failed, is the server running?\n" + se.Message;
                MessageBox.Show(str);
                return;
            }

            Thread listen = new Thread(ReceiveUTF8);
            listen.IsBackground = true;
            listen.Start();
        }

        /**************************************************************************/

        private void btn_Adv_LaneDetcData_Click(object sender, EventArgs e)
        {
            try
            {
                string op = "advertise";
                string topic = "/LaneDetc_Data";
                string msg_type = "aev_pkg/lane_detection_msg";

                Advertise adv = new Advertise(op, topic, msg_type);
                string needToSend = JsonConvert.SerializeObject(adv);
                sendToTcp(needToSend);
            }
            catch (SocketException se)
            {
                MessageBox.Show("Can not Advertise! " + se.Message);
            }
        }

        float centerOffset_Div = 20.0f, Curvature_Div = 1000.0f;

        private void trackBar_centerOffset_Scroll(object sender, EventArgs e)
        {
            label_LateralDeviationVal.Text = (trackBar_centerOffset.Value / centerOffset_Div).ToString();
            AEV_Comm.LaneDetc_Msg laneDetc_Msg = new AEV_Comm.LaneDetc_Msg(1, (trackBar_centerOffset.Value / centerOffset_Div), 
                (trackBar_Curvature.Value / Curvature_Div));
            PublishMessage m = AEV_Comm.LaneData_MsgMake(laneDetc_Msg);
            string needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        private void trackBar_Curvature_Scroll(object sender, EventArgs e)
        {
            label_CurvatureVal.Text = (trackBar_Curvature.Value / Curvature_Div).ToString();
            AEV_Comm.LaneDetc_Msg laneDetc_Msg = new AEV_Comm.LaneDetc_Msg(1, (trackBar_centerOffset.Value / centerOffset_Div), 
                (trackBar_Curvature.Value / Curvature_Div));
            PublishMessage m = AEV_Comm.LaneData_MsgMake(laneDetc_Msg);
            string needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        /**************************************************************************/

        private void btn_Adv_ObjDetcData_Click(object sender, EventArgs e)
        {
            try
            {
                string op = "advertise";
                string topic = "/ObjDetc_Data";
                string msg_type = "aev_pkg/object_detection_msg";

                Advertise adv = new Advertise(op, topic, msg_type);
                string needToSend = JsonConvert.SerializeObject(adv);
                sendToTcp(needToSend);
            }
            catch (SocketException se)
            {
                MessageBox.Show("Can not Advertise! " + se.Message);
            }
        }

        private void trackBar_YawRate_Scroll(object sender, EventArgs e)
        {
            label_YawRateVal.Text = (trackBar_YawRate.Value / 100.0f).ToString();
            AEV_Comm.Obstruction_IMU_Msg Msg = new AEV_Comm.Obstruction_IMU_Msg(1, false, trackBar_YawRate.Value / 100.0f);
            PublishMessage m = AEV_Comm.Obstruction_IMU_MsgMake(Msg);
            string needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        UInt32 msg_counter = 0;
        bool userReqStart = false;
        bool userReqAutoRun = false;
        bool userReqStop = false;
        bool clearError = false;
        Int16 speedSetpoint = 0;
        Byte turnSignal = 0;
        bool horn = false;
        bool frontLight = false;
        Byte steeringLeftRight = 0;

        private void button_userReqAutoRun_Click(object sender, EventArgs e)
        {
            if (userReqAutoRun == false)
            {
                userReqAutoRun = true;
                button_userReqAutoRun.BackColor = Color.Lime;
            }
            else
            {
                userReqAutoRun = false;
                button_userReqAutoRun.BackColor = Color.Transparent;
            }
        }

        private void button_userReqStop_Click(object sender, EventArgs e)
        {
            if (userReqStop == false)
            {
                userReqStop = true;
                button_userReqStop.BackColor = Color.Lime;
            }
            else
            {
                userReqStop = false;
                button_userReqStop.BackColor = Color.Transparent;
            }
            textBox_speedSetpoint.Text = "0";
            trackBar_speedSetpoint.Value = 0;
            speedSetpoint = 0;
        }

        private void button_clearError_Click(object sender, EventArgs e)
        {
            if (clearError == false)
            {
                clearError = true;
                button_clearError.BackColor = Color.Lime;
            }
            else
            {
                clearError = false;
                button_clearError.BackColor = Color.Transparent;
            }
        }

        float currentSteeringPosition_Setpoint = 0.0f;
        private void button_steeringRight_Click(object sender, EventArgs e)
        {
            if (steeringLeftRight != 2)
            {
                steeringLeftRight = 2;
                button_steeringRight.BackColor = Color.Lime;
                button_steeringLeft.BackColor = Color.Transparent;
                button_ResetSteering.BackColor = Color.Transparent;
                timer3.Enabled = true;
            }
            else
            {
                steeringLeftRight = 0;
                button_steeringRight.BackColor = Color.Transparent;
                button_steeringLeft.BackColor = Color.Transparent;
                button_ResetSteering.BackColor = Color.Transparent;
                timer3.Enabled = false;
            }
            
        }

        private void button_steeringLeft_Click(object sender, EventArgs e)
        {
            if (steeringLeftRight != 1)
            {
                steeringLeftRight = 1;
                button_steeringLeft.BackColor = Color.Lime;
                button_steeringRight.BackColor = Color.Transparent;
                button_ResetSteering.BackColor = Color.Transparent;
                timer3.Enabled = true;
            }
            else
            {
                steeringLeftRight = 0;
                button_steeringRight.BackColor = Color.Transparent;
                button_steeringLeft.BackColor = Color.Transparent;
                button_ResetSteering.BackColor = Color.Transparent;
                timer3.Enabled = false;
            }
        }

        private void button_ResetSteering_Click(object sender, EventArgs e)
        {
            timer3.Enabled = false;

            SetSteeringDriverPos(0.0f);

            steeringLeftRight = 0;
            button_steeringRight.BackColor = Color.Transparent;
            button_steeringLeft.BackColor = Color.Transparent;
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (AutoSteering_Flag == false)
            {
                if (steeringLeftRight == 2)
                {
                    SetSteeringDriverPos((currentSteeringPosition_Setpoint - 0.05f));
                }
                else if (steeringLeftRight == 1)
                {
                    SetSteeringDriverPos((currentSteeringPosition_Setpoint + 0.05f));
                }
            }
        }


        private void button_Horn_Click(object sender, EventArgs e)
        {
            if (horn == false)
            {
                horn = true;
                button_Horn.BackColor = Color.Lime;
            }
            else
            {
                horn = false;
                button_Horn.BackColor = Color.Transparent;
            }
        }

        private void button_frontLight_Click(object sender, EventArgs e)
        {
            if (frontLight == false)
            {
                frontLight = true;
                button_frontLight.BackColor = Color.Lime;
            }
            else
            {
                frontLight = false;
                button_frontLight.BackColor = Color.Transparent;
            }
        }

        private void button_TurnSignal_Left_Click(object sender, EventArgs e)
        {
            if (turnSignal != 1)
            {
                turnSignal = 1;
                button_TurnSignal_Left.BackColor = Color.Lime;
                button_TurnSignal_Right.BackColor = Color.Transparent;
            }
            else
            {
                turnSignal = 0;
                button_TurnSignal_Left.BackColor = Color.Transparent;
                button_TurnSignal_Right.BackColor = Color.Transparent;
            }
        }

        private void button_TurnSignal_Right_Click(object sender, EventArgs e)
        {
            if (turnSignal != 2)
            {
                turnSignal = 2;
                button_TurnSignal_Left.BackColor = Color.Transparent;
                button_TurnSignal_Right.BackColor = Color.Lime;
            }
            else
            {
                turnSignal = 0;
                button_TurnSignal_Left.BackColor = Color.Transparent;
                button_TurnSignal_Right.BackColor = Color.Transparent;
            }
        }

        private void trackBar_speedSetpoint_Scroll(object sender, EventArgs e)
        {
            speedSetpoint = Convert.ToInt16(trackBar_speedSetpoint.Value);
            textBox_speedSetpoint.Text = speedSetpoint.ToString();
        }

        void Advertise_GuiMsg()
        {
            try
            {
                string op = "advertise";
                string topic = "GUI_Data";
                string msg_type = "aev_pkg/gui_msg";
                Advertise adv = new Advertise(op, topic, msg_type);
                string needToSend = JsonConvert.SerializeObject(adv);
                sendToTcp(needToSend);

                button_Advertise_GuiMsg.BackColor = Color.Lime;
                timer1.Enabled = true;
                groupBox_CarControl.Visible = true;
                groupBox_CarStatus.Visible = true;
                AdvertiseGuiMsg_Flag = true;

            }
            catch (SocketException se)
            {
                MessageBox.Show("Can not Advertise! " + se.Message);
                button_Advertise_GuiMsg.BackColor = Color.Transparent;
            }
        }

        private void button_Advertise_GuiMsg_Click(object sender, EventArgs e)
        {
            Advertise_GuiMsg();
        }

        private void button_userReqStart_Click(object sender, EventArgs e)
        {
            if (userReqStart == false)
            {
                userReqStart = true;
                button_userReqStart.BackColor = Color.Lime;
            }
            else
            {
                userReqStart = false;
                button_userReqStart.BackColor = Color.Transparent;
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Int16 temp_speedSetpoint;
            temp_speedSetpoint = Convert.ToInt16(textBox_speedSetpoint.Text);
            if ((temp_speedSetpoint <= 25) && (temp_speedSetpoint >= -25))
            {
                speedSetpoint = temp_speedSetpoint;
                trackBar_speedSetpoint.Value = speedSetpoint;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            msg_counter++;
            AEV_Comm.GUI_Msg gui_Msg = new AEV_Comm.GUI_Msg(msg_counter, userReqStart, 
                userReqAutoRun, userReqStop, clearError, speedSetpoint, 
                turnSignal, horn, frontLight, steeringLeftRight);
            PublishMessage m = AEV_Comm.GUI_MsgMake(gui_Msg);
            string needToSend = JsonConvert.SerializeObject(m);
            sendToTcp(needToSend);
        }

        /***********************************TAB DRIVER COM***************************************/
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
            this.Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cBoxComPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(cBoxBaudRate.Text);
                serialPort1.DataBits = Convert.ToInt32(cBoxDataBits.Text);
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cBoxStopBits.Text);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), cBoxPatityBits.Text);
                serialPort1.Open();
                progressBar1.Value = 100;
                lbStaticComPort.Text = "ON";
                groupBox4.Visible = true;
                groupBox_DriverControl.Visible = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                progressBar1.Value = 0;
                lbStaticComPort.Text = "OFF";
                groupBox_DriverControl.Visible = false;
            }
        }

        const byte MTR_PROTOCOL_CODE_EXECUTE_CMD = 0x03;
        const byte MTR_PROTOCOL_CODE_SET_REG = 0x01;
        const byte MTR_PROTOCOL_CMD_SERVO_ON = 0x01;
        const byte MTR_PROTOCOL_CMD_SERVO_OFF = 0x02;
        const byte MTR_PROTOCOL_REG_POSITION_REF = 93;
        const byte MTR_PROTOCOL_REG_POSITION_MODE = 98;
        const byte MTR_POS_STEP = (1);
        const byte MTR_REF_POS_ABS = 0;
        const byte MTR_PROTOCOL_REG_BASE_CURRENT = 62;

        bool SteeringMotor_Init_Finish = false;

        byte CalcCRC(byte uCode, byte uSize, byte[] buffer)
        {
            byte uCRC = 0;
            UInt16 uSum = 0;
            byte idx;
            {
                uSum += uCode;
                uSum += uSize;

                for (idx = 0; idx < uSize; idx++)
                {
                    uSum += buffer[idx + 2];
                }

                uCRC = (byte)(uSum & 0xFF); // Low Byte of nSum
                uCRC += (byte)(uSum >> 8); // High Byte of nSum
            }
            return uCRC;
        }

        private void button_Servo_On_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[4];
            arr[0] = MTR_PROTOCOL_CODE_EXECUTE_CMD;
            arr[1] = 1;
            arr[2] = MTR_PROTOCOL_CMD_SERVO_ON;
            arr[3] = CalcCRC(arr[0], arr[1], arr);
            if (serialPort1.IsOpen)
                serialPort1.Write(arr, 0, 4);

            SteeringMotor_Init_Finish = true;
        }

        private void button_Servo_Off_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[4];
            arr[0] = MTR_PROTOCOL_CODE_EXECUTE_CMD;
            arr[1] = 1;
            arr[2] = MTR_PROTOCOL_CMD_SERVO_OFF;
            arr[3] = CalcCRC(arr[0], arr[1], arr);
            if (serialPort1.IsOpen)
                serialPort1.Write(arr, 0, 4);

            SteeringMotor_Init_Finish = false;
        }


        private void button_Position_Mode_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[5];
            arr[0] = MTR_PROTOCOL_CODE_SET_REG;
            arr[1] = 2;
            arr[2] = MTR_PROTOCOL_REG_POSITION_MODE;
            arr[3] = MTR_POS_STEP;
            arr[4] = CalcCRC(arr[0], arr[1], arr);
            if (serialPort1.IsOpen)
                serialPort1.Write(arr, 0, 5);

            
        }

        private void button_AutoSteering_Click(object sender, EventArgs e)
        {
            if (AutoSteering_Flag == false)
            {
                AutoSteering_Flag = true;
            }
            else
            {
                AutoSteering_Flag = false;
            }
        }

        private void button_SetDriverPos_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[9];
            float DriverPosition = 0.0f;
            float[] position = new float[1];
            if (textBox_DriverPosition.Text == "")
            {
                // Do nothing
            }
            else
            {
                DriverPosition = -Convert.ToSingle(textBox_DriverPosition.Text);

                if ((DriverPosition <= 0.7) && (DriverPosition >= -0.7))
                {
                    position[0] = DriverPosition * STEERING_FACTOR;
                    var byteArray = new byte[4];
                    Buffer.BlockCopy(position, 0, byteArray, 0, byteArray.Length);

                    arr[0] = MTR_PROTOCOL_CODE_SET_REG;
                    arr[1] = 6;
                    arr[2] = MTR_PROTOCOL_REG_POSITION_REF;
                    arr[3] = byteArray[0];
                    arr[4] = byteArray[1];
                    arr[5] = byteArray[2];
                    arr[6] = byteArray[3];
                    arr[7] = MTR_REF_POS_ABS;
                    arr[8] = CalcCRC(arr[0], arr[1], arr);

                    if (serialPort1.IsOpen)
                    {
                        serialPort1.Write(arr, 0, 9);

                        currentSteeringPosition_Setpoint = DriverPosition;
                        textBox_SteeringAng.Text = currentSteeringPosition_Setpoint.ToString("00.00");
                        //trackBar_SreeingAng.Value = (Int16)(currentSteeringPosition_Setpoint * 100);

                    }
                }
                
            }
        }

        void SetSteeringDriverPos(float SteeringSet)
        {
            byte[] arr = new byte[9];
            float[] position = new float[1];

            if ((SteeringSet <= 0.7) && (SteeringSet >= -0.7) 
                && (Math.Abs(SteeringSet - currentSteeringPosition_Setpoint) > 0.003))
            {
                position[0] = SteeringSet * STEERING_FACTOR * 0.8f;
                var byteArray = new byte[4];
                Buffer.BlockCopy(position, 0, byteArray, 0, byteArray.Length);

                arr[0] = MTR_PROTOCOL_CODE_SET_REG;
                arr[1] = 6;
                arr[2] = MTR_PROTOCOL_REG_POSITION_REF;
                arr[3] = byteArray[0];
                arr[4] = byteArray[1];
                arr[5] = byteArray[2];
                arr[6] = byteArray[3];
                arr[7] = MTR_REF_POS_ABS;
                arr[8] = CalcCRC(arr[0], arr[1], arr);

                if (SteeringMotor_Init_Finish)
                {
                    if (serialPort1.IsOpen)
                    {
                        serialPort1.Write(arr, 0, 9);

                        currentSteeringPosition_Setpoint = SteeringSet;
                        textBox_SteeringAng.Text = currentSteeringPosition_Setpoint.ToString();
                        //trackBar_SreeingAng.Value = (Int16)(currentSteeringPosition_Setpoint * 100);
                    }
                }
            }
        }

        private void button_SetDriverCurrent_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[8];
            float[] driver_current = new float[1];
            if (textBox_DriverCurrent.Text == "")
            {
                // Do nothing
            }
            else
            {
                driver_current[0] = Convert.ToSingle(textBox_DriverCurrent.Text);
                var byteArray = new byte[4];
                Buffer.BlockCopy(driver_current, 0, byteArray, 0, byteArray.Length);

                arr[0] = MTR_PROTOCOL_CODE_SET_REG;
                arr[1] = 5;
                arr[2] = MTR_PROTOCOL_REG_BASE_CURRENT;
                arr[3] = byteArray[0];
                arr[4] = byteArray[1];
                arr[5] = byteArray[2];
                arr[6] = byteArray[3];
                arr[7] = CalcCRC(arr[0], arr[1], arr);

                if (serialPort1.IsOpen)
                    serialPort1.Write(arr, 0, 8);
            }
        }

        private void trackBar_SreeingAng_Scroll(object sender, EventArgs e)
        {
            if (AutoSteering_Flag == false)
            {
                float DriverPosition_set = 0.0f;

                DriverPosition_set = Convert.ToSingle(-trackBar_SreeingAng.Value) / 100.0f;

                if ((DriverPosition_set <= 0.7) && (DriverPosition_set >= -0.7))
                {
                    SetSteeringDriverPos(DriverPosition_set);
                }
            }
        }

        private void textBox_SteeringAng_TextChanged(object sender, EventArgs e)
        {
            if (AutoSteering_Flag == false)
            {
                float DriverPosition_set = 0.0f;

                DriverPosition_set = Convert.ToSingle(textBox_SteeringAng.Text);

                if ((DriverPosition_set <= 0.7) && (DriverPosition_set >= -0.7))
                {
                    SetSteeringDriverPos(DriverPosition_set);
                }
            }
        }

        int intlen = 0;
        private void timer2_Tick(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            if (intlen != ports.Length)
            {
                intlen = ports.Length;
                cBoxComPort.Items.Clear();
                for (int j = 0; j < intlen; j++)
                {
                    cBoxComPort.Items.Add(ports[j]); 
                }
            }

            if (userReqStart == true)
            {
                userReqStart = false;
                button_userReqStart.BackColor = Color.Transparent;
            }

            if ((m_clientSocket != null) && (m_clientSocket.Connected))
            {
                Status_ros_connection.BackColor = Color.Lime;

                if (btn_Sub_All.Text == "Subscribe All")
                {
                    Status_ros_subscribe.BackColor = Color.OrangeRed;
                    textBox_SysInfo.Text = "Didn't subscribe to ROS Message";
                    textBox_SysInfo.BackColor = Color.OrangeRed;
                }
                else
                {
                    Status_ros_subscribe.BackColor = Color.Lime;
                }

                if (AdvertiseGuiMsg_Flag == true)
                {
                    button_Advertise_GuiMsg.BackColor = Color.Lime;
                    Status_ros_advertise.BackColor = Color.Lime;
                    groupBox_CarControl.Visible = true;
                    groupBox_CarStatus.Visible = true;
                }
                else
                {
                    timer1.Enabled = false;
                    button_Advertise_GuiMsg.BackColor = Color.Gray;
                    Status_ros_advertise.BackColor = Color.OrangeRed;
                    groupBox_CarControl.Visible = false;
                    groupBox_CarStatus.Visible = false;
                }
            }
            else
            {
                Status_ros_connection.BackColor = Color.OrangeRed;

                textBox_SysInfo.Text = "ROS is not connected.";
                textBox_SysInfo.BackColor = Color.OrangeRed;

                btn_Sub_All.Text = "Subscribe All";
                btn_Sub_All.BackColor = Color.Lime;
                Status_ros_subscribe.BackColor = Color.OrangeRed;

                AdvertiseGuiMsg_Flag = false;
                button_Advertise_GuiMsg.BackColor = Color.Lime;
                Status_ros_advertise.BackColor = Color.OrangeRed;

            }

            if (serialPort1.IsOpen)
            {
                Status_steering_driver.BackColor = Color.Lime;
                if (SteeringMotor_Init_Finish)
                {
                    Status_steering_ON.BackColor = Color.Lime;

                    button_steeringLeft.Enabled = true;
                    button_ResetSteering.Enabled = true;
                    button_steeringRight.Enabled = true;
                    trackBar_SreeingAng.Enabled = true;

                    textBox_SysInfo.Text = "Ready";
                    textBox_SysInfo.BackColor = Color.WhiteSmoke;
                }
                else
                {
                    Status_steering_ON.BackColor = Color.OrangeRed;
                }
            }
            else
            {
                Status_steering_driver.BackColor = Color.OrangeRed;
                SteeringMotor_Init_Finish = false;

                button_steeringLeft.Enabled = false;
                button_ResetSteering.Enabled = false;
                button_steeringRight.Enabled = false;
                trackBar_SreeingAng.Enabled = false;
                groupBox4.Visible = false;

                textBox_SysInfo.Text = "Driver Steering not Ready";
                textBox_SysInfo.BackColor = Color.OrangeRed;
            }

            if (AutoSteering_Flag == true)
            {
                button_AutoSteering.BackColor = Color.Lime;
                Status_steering_auto.BackColor = Color.Lime;
                textBox_SteeringAng.Enabled = false;
                trackBar_SreeingAng.Enabled = false;
            }
            else
            {
                button_AutoSteering.BackColor = Color.Gray;
                Status_steering_auto.BackColor = Color.OrangeRed;
                textBox_SteeringAng.Enabled = true;
                trackBar_SreeingAng.Enabled = true;
            }

        }

        string dataIN;
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                dataIN = (string)serialPort1.ReadExisting();
                this.Invoke(new EventHandler(showdata));
            }
        }

        private void AEV_GUI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W) // increase speed
            {
                speedSetpoint = Convert.ToInt16(trackBar_speedSetpoint.Value);
                if (speedSetpoint < 25 && speedSetpoint >= -25)
                {
                    speedSetpoint += 1;
                }
                
                trackBar_speedSetpoint.Value = speedSetpoint;
                textBox_speedSetpoint.Text = speedSetpoint.ToString();
            }
            else if (e.KeyCode == Keys.S) // decrease speed
            {
                speedSetpoint = Convert.ToInt16(trackBar_speedSetpoint.Value);
                if (speedSetpoint <= 25 && speedSetpoint > -25)
                {
                    speedSetpoint -= 1;
                }
                trackBar_speedSetpoint.Value = speedSetpoint;
                textBox_speedSetpoint.Text = speedSetpoint.ToString();
            }
            else if (e.KeyCode == Keys.Space) // Stop
            {
                speedSetpoint = 0;
                trackBar_speedSetpoint.Value = speedSetpoint;
                textBox_speedSetpoint.Text = speedSetpoint.ToString();

                AutoSteering_Flag = false;

                //float DriverPosition_set = 0.0f;
                //trackBar_SreeingAng.Value = Convert.ToInt16(-DriverPosition_set * 100.0f);
                //SetSteeringDriverPos(DriverPosition_set);
            }
            else if (e.KeyCode == Keys.B) // Stop
            {
                speedSetpoint = 1;
                trackBar_speedSetpoint.Value = speedSetpoint;
                textBox_speedSetpoint.Text = speedSetpoint.ToString();

                AutoSteering_Flag = false;
                float DriverPosition_set = 0.0f;
                trackBar_SreeingAng.Value = Convert.ToInt16(-DriverPosition_set * 100.0f);
                SetSteeringDriverPos(DriverPosition_set);
            }
            else if (e.KeyCode == Keys.A) // Manual steering left
            {
                if (AutoSteering_Flag == false)
                {
                    float DriverPosition_set = 0.0f;

                    DriverPosition_set = Convert.ToSingle(-trackBar_SreeingAng.Value) / 100.0f;

                    if ((DriverPosition_set <= 0.6) && (DriverPosition_set >= -0.7))
                    {
                        DriverPosition_set += 0.1f;
                        trackBar_SreeingAng.Value = Convert.ToInt16(-DriverPosition_set * 100.0f);
                        SetSteeringDriverPos(DriverPosition_set);
                    }
                }
            }
            else if ((e.KeyCode == Keys.F) || (e.KeyCode == Keys.D)) // Manual steering right
            {
                if (AutoSteering_Flag == false)
                {
                    float DriverPosition_set = 0.0f;

                    DriverPosition_set = Convert.ToSingle(-trackBar_SreeingAng.Value) / 100.0f;

                    if ((DriverPosition_set <= 0.7) && (DriverPosition_set >= -0.6))
                    {
                        DriverPosition_set -= 0.1f;
                        trackBar_SreeingAng.Value = Convert.ToInt16(-DriverPosition_set * 100.0f);
                        SetSteeringDriverPos(DriverPosition_set);
                    }
                }
            }
            else if (e.KeyCode == Keys.L) // Manual reset steering
            {
                if (AutoSteering_Flag == false)
                {
                    float DriverPosition_set = 0.0f;
                    trackBar_SreeingAng.Value = Convert.ToInt16(-DriverPosition_set * 100.0f);
                    SetSteeringDriverPos(DriverPosition_set);
                }
            }
            else if (e.KeyCode == Keys.P) // Switch AutoSteering mode
            {
                if (AutoSteering_Flag == false)
                {
                    AutoSteering_Flag = true;
                }
                else
                {
                    AutoSteering_Flag = false;
                }
            }
            else if (e.KeyCode == Keys.O) // Switch Steering Motor ON/OFF
            {
                if (SteeringMotor_Init_Finish == false)
                {
                    byte[] arr = new byte[4];
                    arr[0] = MTR_PROTOCOL_CODE_EXECUTE_CMD;
                    arr[1] = 1;
                    arr[2] = MTR_PROTOCOL_CMD_SERVO_ON;
                    arr[3] = CalcCRC(arr[0], arr[1], arr);
                    if (serialPort1.IsOpen)
                    {
                        serialPort1.Write(arr, 0, 4);
                        SteeringMotor_Init_Finish = true;
                    }
                }
                else
                {
                    byte[] arr = new byte[4];
                    arr[0] = MTR_PROTOCOL_CODE_EXECUTE_CMD;
                    arr[1] = 1;
                    arr[2] = MTR_PROTOCOL_CMD_SERVO_OFF;
                    arr[3] = CalcCRC(arr[0], arr[1], arr);
                    if (serialPort1.IsOpen)
                    {
                        serialPort1.Write(arr, 0, 4);
                        SteeringMotor_Init_Finish = false;
                    }
                }
            }
        }

        private void showdata(object sender, EventArgs e)
        {
            byte[] data_byte = new byte[500];
            string data_text = "";
            string temp = "";
            for (UInt16 n = 0; n < dataIN.Length; n++)
            {
                data_byte[n] = Convert.ToByte(dataIN[n]);

                temp = string.Format("{0:00}", data_byte[n]);
                data_text += temp + " ";
            }

            data_text = data_text + "\r\n" + dataIN.Length.ToString() + " bytes";
            tBoxDataReceive.Text = (data_text + "\r\n");
            //tBoxDataReceive.SelectionStart = tBoxDataReceive.Text.Length;
            //tBoxDataReceive.ScrollToCaret();

            if (dataIN.Length == 69)
            {
                byte[] arr = new byte[4];
                float[] position = new float[1];
                float position_Steering;

                arr[0] = data_byte[6];
                arr[1] = data_byte[7];
                arr[2] = data_byte[8];
                arr[3] = data_byte[9];

                Buffer.BlockCopy(arr, 0, position, 0, 4);
                position_Steering = position[0] / STEERING_FACTOR;

                textBox_ActualSteeringAng.Text = position[0].ToString();
            }

        }


    }
}
