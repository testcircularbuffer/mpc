﻿namespace DriverCom
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tBoxOutData = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tBoxDataReceive = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbLengthReceiveData = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chBoxAddToOldData = new System.Windows.Forms.CheckBox();
            this.chBoxAlwayUpDate = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cBoxComPort = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cBoxPatityBits = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lbStaticComPort = new System.Windows.Forms.Label();
            this.cBoxStopBits = new System.Windows.Forms.ComboBox();
            this.cBoxDataBits = new System.Windows.Forms.ComboBox();
            this.cBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbLengthSendData = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSendData = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button_Servo_On = new System.Windows.Forms.Button();
            this.button_Position_Mode = new System.Windows.Forms.Button();
            this.groupBox_DriverControl = new System.Windows.Forms.GroupBox();
            this.button_SetDriverCurrent = new System.Windows.Forms.Button();
            this.button_SetDriverPos = new System.Windows.Forms.Button();
            this.Set_Current = new System.Windows.Forms.Label();
            this.Set_Position = new System.Windows.Forms.Label();
            this.textBox_DriverCurrent = new System.Windows.Forms.TextBox();
            this.textBox_DriverPosition = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox_DriverControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(23, 189);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "OPEN";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(23, 224);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.progressBar1.Location = new System.Drawing.Point(42, 359);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(156, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // tBoxOutData
            // 
            this.tBoxOutData.Location = new System.Drawing.Point(6, 19);
            this.tBoxOutData.Multiline = true;
            this.tBoxOutData.Name = "tBoxOutData";
            this.tBoxOutData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBoxOutData.Size = new System.Drawing.Size(261, 66);
            this.tBoxOutData.TabIndex = 15;
            this.tBoxOutData.TextChanged += new System.EventHandler(this.tBoxOutData_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tBoxOutData);
            this.groupBox3.Location = new System.Drawing.Point(13, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(275, 98);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Write Data Send Here";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox12);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Location = new System.Drawing.Point(319, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(315, 280);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Receive";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tBoxDataReceive);
            this.groupBox12.Location = new System.Drawing.Point(6, 19);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(295, 98);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            // 
            // tBoxDataReceive
            // 
            this.tBoxDataReceive.BackColor = System.Drawing.Color.White;
            this.tBoxDataReceive.Location = new System.Drawing.Point(6, 19);
            this.tBoxDataReceive.Multiline = true;
            this.tBoxDataReceive.Name = "tBoxDataReceive";
            this.tBoxDataReceive.ReadOnly = true;
            this.tBoxDataReceive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBoxDataReceive.Size = new System.Drawing.Size(278, 66);
            this.tBoxDataReceive.TabIndex = 15;
            this.tBoxDataReceive.TextChanged += new System.EventHandler(this.tBoxDataReceive_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Location = new System.Drawing.Point(6, 129);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(295, 139);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Control";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbLengthReceiveData);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(16, 91);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(268, 41);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            // 
            // lbLengthReceiveData
            // 
            this.lbLengthReceiveData.AutoSize = true;
            this.lbLengthReceiveData.Location = new System.Drawing.Point(160, 17);
            this.lbLengthReceiveData.Name = "lbLengthReceiveData";
            this.lbLengthReceiveData.Size = new System.Drawing.Size(23, 15);
            this.lbLengthReceiveData.TabIndex = 21;
            this.lbLengthReceiveData.Text = "00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "Data Receive Length: ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(16, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 57);
            this.button1.TabIndex = 18;
            this.button1.Text = "Clear Data Receive";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chBoxAddToOldData);
            this.groupBox5.Controls.Add(this.chBoxAlwayUpDate);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(137, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(147, 75);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            // 
            // chBoxAddToOldData
            // 
            this.chBoxAddToOldData.AutoSize = true;
            this.chBoxAddToOldData.Location = new System.Drawing.Point(11, 47);
            this.chBoxAddToOldData.Name = "chBoxAddToOldData";
            this.chBoxAddToOldData.Size = new System.Drawing.Size(113, 19);
            this.chBoxAddToOldData.TabIndex = 17;
            this.chBoxAddToOldData.Text = "Show all data";
            this.chBoxAddToOldData.UseVisualStyleBackColor = true;
            this.chBoxAddToOldData.CheckedChanged += new System.EventHandler(this.chBoxAddToOldData_CheckedChanged);
            // 
            // chBoxAlwayUpDate
            // 
            this.chBoxAlwayUpDate.AutoSize = true;
            this.chBoxAlwayUpDate.Location = new System.Drawing.Point(11, 22);
            this.chBoxAlwayUpDate.Name = "chBoxAlwayUpDate";
            this.chBoxAlwayUpDate.Size = new System.Drawing.Size(116, 19);
            this.chBoxAlwayUpDate.TabIndex = 16;
            this.chBoxAlwayUpDate.Text = "Only new data";
            this.chBoxAlwayUpDate.UseVisualStyleBackColor = true;
            this.chBoxAlwayUpDate.CheckedChanged += new System.EventHandler(this.chBoxAlwayUpDate_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "COM Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Baudrate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Data bits";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Stop bit";
            // 
            // cBoxComPort
            // 
            this.cBoxComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxComPort.FormattingEnabled = true;
            this.cBoxComPort.Location = new System.Drawing.Point(110, 26);
            this.cBoxComPort.Name = "cBoxComPort";
            this.cBoxComPort.Size = new System.Drawing.Size(109, 21);
            this.cBoxComPort.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.cBoxPatityBits);
            this.groupBox1.Controls.Add(this.groupBox10);
            this.groupBox1.Controls.Add(this.cBoxStopBits);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.cBoxDataBits);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Controls.Add(this.cBoxBaudRate);
            this.groupBox1.Controls.Add(this.cBoxComPort);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox1.Location = new System.Drawing.Point(640, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 427);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Com Port Control";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 392);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(90, 29);
            this.button3.TabIndex = 20;
            this.button3.Text = "ESP8266";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Red;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Location = new System.Drawing.Point(132, 388);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(96, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            this.btnExit.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnExit_MouseMove);
            // 
            // cBoxPatityBits
            // 
            this.cBoxPatityBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxPatityBits.FormattingEnabled = true;
            this.cBoxPatityBits.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.cBoxPatityBits.Location = new System.Drawing.Point(110, 146);
            this.cBoxPatityBits.Name = "cBoxPatityBits";
            this.cBoxPatityBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxPatityBits.TabIndex = 17;
            this.cBoxPatityBits.Text = "None";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lbStaticComPort);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(110, 182);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(109, 69);
            this.groupBox10.TabIndex = 19;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "State Com Port";
            // 
            // lbStaticComPort
            // 
            this.lbStaticComPort.AutoSize = true;
            this.lbStaticComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStaticComPort.Location = new System.Drawing.Point(16, 25);
            this.lbStaticComPort.Name = "lbStaticComPort";
            this.lbStaticComPort.Size = new System.Drawing.Size(72, 31);
            this.lbStaticComPort.TabIndex = 22;
            this.lbStaticComPort.Text = "OFF";
            // 
            // cBoxStopBits
            // 
            this.cBoxStopBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxStopBits.FormattingEnabled = true;
            this.cBoxStopBits.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.cBoxStopBits.Location = new System.Drawing.Point(110, 116);
            this.cBoxStopBits.Name = "cBoxStopBits";
            this.cBoxStopBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxStopBits.TabIndex = 16;
            this.cBoxStopBits.Text = "One";
            // 
            // cBoxDataBits
            // 
            this.cBoxDataBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxDataBits.FormattingEnabled = true;
            this.cBoxDataBits.Items.AddRange(new object[] {
            "6",
            "7",
            "8"});
            this.cBoxDataBits.Location = new System.Drawing.Point(110, 87);
            this.cBoxDataBits.Name = "cBoxDataBits";
            this.cBoxDataBits.Size = new System.Drawing.Size(109, 21);
            this.cBoxDataBits.TabIndex = 15;
            this.cBoxDataBits.Text = "8";
            // 
            // cBoxBaudRate
            // 
            this.cBoxBaudRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxBaudRate.FormattingEnabled = true;
            this.cBoxBaudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200"});
            this.cBoxBaudRate.Location = new System.Drawing.Point(110, 56);
            this.cBoxBaudRate.Name = "cBoxBaudRate";
            this.cBoxBaudRate.Size = new System.Drawing.Size(109, 21);
            this.cBoxBaudRate.TabIndex = 14;
            this.cBoxBaudRate.Text = "115200";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Parity bit";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Controls.Add(this.button2);
            this.groupBox8.Controls.Add(this.btnSendData);
            this.groupBox8.Location = new System.Drawing.Point(13, 129);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(275, 141);
            this.groupBox8.TabIndex = 20;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Control";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lbLengthSendData);
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(16, 89);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(248, 41);
            this.groupBox9.TabIndex = 18;
            this.groupBox9.TabStop = false;
            // 
            // lbLengthSendData
            // 
            this.lbLengthSendData.AutoSize = true;
            this.lbLengthSendData.Location = new System.Drawing.Point(127, 16);
            this.lbLengthSendData.Name = "lbLengthSendData";
            this.lbLengthSendData.Size = new System.Drawing.Size(23, 15);
            this.lbLengthSendData.TabIndex = 21;
            this.lbLengthSendData.Text = "00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "Data Send Length: ";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(156, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 56);
            this.button2.TabIndex = 18;
            this.button2.Text = "Clear Data Send";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSendData
            // 
            this.btnSendData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnSendData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSendData.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnSendData.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendData.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSendData.Location = new System.Drawing.Point(25, 32);
            this.btnSendData.Name = "btnSendData";
            this.btnSendData.Size = new System.Drawing.Size(92, 54);
            this.btnSendData.TabIndex = 2;
            this.btnSendData.Text = "Send CMD";
            this.btnSendData.UseVisualStyleBackColor = false;
            this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.groupBox3);
            this.groupBox11.Controls.Add(this.groupBox8);
            this.groupBox11.Location = new System.Drawing.Point(12, 12);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(301, 280);
            this.groupBox11.TabIndex = 21;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Data Send";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button_Servo_On
            // 
            this.button_Servo_On.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_Servo_On.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Servo_On.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Servo_On.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Servo_On.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_Servo_On.Location = new System.Drawing.Point(29, 36);
            this.button_Servo_On.Name = "button_Servo_On";
            this.button_Servo_On.Size = new System.Drawing.Size(145, 33);
            this.button_Servo_On.TabIndex = 19;
            this.button_Servo_On.Text = "Servo On";
            this.button_Servo_On.UseVisualStyleBackColor = false;
            this.button_Servo_On.Click += new System.EventHandler(this.button_Servo_On_Click);
            // 
            // button_Position_Mode
            // 
            this.button_Position_Mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_Position_Mode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_Position_Mode.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Position_Mode.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Position_Mode.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_Position_Mode.Location = new System.Drawing.Point(29, 75);
            this.button_Position_Mode.Name = "button_Position_Mode";
            this.button_Position_Mode.Size = new System.Drawing.Size(145, 33);
            this.button_Position_Mode.TabIndex = 22;
            this.button_Position_Mode.Text = "Position Mode";
            this.button_Position_Mode.UseVisualStyleBackColor = false;
            this.button_Position_Mode.Click += new System.EventHandler(this.button_Position_Mode_Click);
            // 
            // groupBox_DriverControl
            // 
            this.groupBox_DriverControl.Controls.Add(this.button_Servo_On);
            this.groupBox_DriverControl.Controls.Add(this.button_SetDriverCurrent);
            this.groupBox_DriverControl.Controls.Add(this.button_SetDriverPos);
            this.groupBox_DriverControl.Controls.Add(this.Set_Current);
            this.groupBox_DriverControl.Controls.Add(this.Set_Position);
            this.groupBox_DriverControl.Controls.Add(this.textBox_DriverCurrent);
            this.groupBox_DriverControl.Controls.Add(this.textBox_DriverPosition);
            this.groupBox_DriverControl.Controls.Add(this.button_Position_Mode);
            this.groupBox_DriverControl.Location = new System.Drawing.Point(12, 298);
            this.groupBox_DriverControl.Name = "groupBox_DriverControl";
            this.groupBox_DriverControl.Size = new System.Drawing.Size(622, 141);
            this.groupBox_DriverControl.TabIndex = 23;
            this.groupBox_DriverControl.TabStop = false;
            this.groupBox_DriverControl.Text = "Driver Control";
            this.groupBox_DriverControl.Visible = false;
            // 
            // button_SetDriverCurrent
            // 
            this.button_SetDriverCurrent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_SetDriverCurrent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_SetDriverCurrent.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_SetDriverCurrent.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SetDriverCurrent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_SetDriverCurrent.Location = new System.Drawing.Point(403, 75);
            this.button_SetDriverCurrent.Name = "button_SetDriverCurrent";
            this.button_SetDriverCurrent.Size = new System.Drawing.Size(80, 33);
            this.button_SetDriverCurrent.TabIndex = 27;
            this.button_SetDriverCurrent.Text = "Set";
            this.button_SetDriverCurrent.UseVisualStyleBackColor = false;
            this.button_SetDriverCurrent.Click += new System.EventHandler(this.button_SetDriverCurrent_Click);
            // 
            // button_SetDriverPos
            // 
            this.button_SetDriverPos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_SetDriverPos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_SetDriverPos.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_SetDriverPos.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_SetDriverPos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button_SetDriverPos.Location = new System.Drawing.Point(403, 40);
            this.button_SetDriverPos.Name = "button_SetDriverPos";
            this.button_SetDriverPos.Size = new System.Drawing.Size(80, 33);
            this.button_SetDriverPos.TabIndex = 24;
            this.button_SetDriverPos.Text = "Set";
            this.button_SetDriverPos.UseVisualStyleBackColor = false;
            this.button_SetDriverPos.Click += new System.EventHandler(this.button_SetDriverPos_Click);
            // 
            // Set_Current
            // 
            this.Set_Current.AutoSize = true;
            this.Set_Current.Location = new System.Drawing.Point(211, 87);
            this.Set_Current.Name = "Set_Current";
            this.Set_Current.Size = new System.Drawing.Size(63, 13);
            this.Set_Current.TabIndex = 26;
            this.Set_Current.Text = "Set_Current";
            // 
            // Set_Position
            // 
            this.Set_Position.AutoSize = true;
            this.Set_Position.Location = new System.Drawing.Point(208, 52);
            this.Set_Position.Name = "Set_Position";
            this.Set_Position.Size = new System.Drawing.Size(66, 13);
            this.Set_Position.TabIndex = 25;
            this.Set_Position.Text = "Set_Position";
            // 
            // textBox_DriverCurrent
            // 
            this.textBox_DriverCurrent.Location = new System.Drawing.Point(288, 84);
            this.textBox_DriverCurrent.Name = "textBox_DriverCurrent";
            this.textBox_DriverCurrent.Size = new System.Drawing.Size(100, 20);
            this.textBox_DriverCurrent.TabIndex = 24;
            this.textBox_DriverCurrent.Text = "2.5";
            // 
            // textBox_DriverPosition
            // 
            this.textBox_DriverPosition.Location = new System.Drawing.Point(288, 49);
            this.textBox_DriverPosition.Name = "textBox_DriverPosition";
            this.textBox_DriverPosition.Size = new System.Drawing.Size(100, 20);
            this.textBox_DriverPosition.TabIndex = 23;
            this.textBox_DriverPosition.Text = "0.0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(897, 451);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_DriverControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Form1";
            this.Text = "C# COM PORT SERIAL";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox_DriverControl.ResumeLayout(false);
            this.groupBox_DriverControl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox tBoxOutData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cBoxComPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cBoxPatityBits;
        private System.Windows.Forms.ComboBox cBoxStopBits;
        private System.Windows.Forms.ComboBox cBoxDataBits;
        private System.Windows.Forms.ComboBox cBoxBaudRate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chBoxAddToOldData;
        private System.Windows.Forms.CheckBox chBoxAlwayUpDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbLengthReceiveData;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lbLengthSendData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbStaticComPort;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox tBoxDataReceive;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSendData;
        private System.Windows.Forms.Button button_Servo_On;
        private System.Windows.Forms.Button button_Position_Mode;
        private System.Windows.Forms.GroupBox groupBox_DriverControl;
        private System.Windows.Forms.Button button_SetDriverCurrent;
        private System.Windows.Forms.Button button_SetDriverPos;
        private System.Windows.Forms.Label Set_Current;
        private System.Windows.Forms.Label Set_Position;
        private System.Windows.Forms.TextBox textBox_DriverCurrent;
        private System.Windows.Forms.TextBox textBox_DriverPosition;
    }
}

