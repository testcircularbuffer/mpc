#-------------------------------------------------
#
# Project created by QtCreator 2018-11-06T14:10:56
#
#-------------------------------------------------

QT      += core gui
QT      += network
QT      += svg
CONFIG += console
CONFIG += qwt
CONFIG += c++11
CONFIG +=debug_and_release

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI_ROV
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    mytcpclient.cpp \
    json.cpp

HEADERS  += mainwindow.h \
    mytcpclient.h \
    json.h \
    ROS_ROV.h \
    My_Define.h

FORMS    += mainwindow.ui

