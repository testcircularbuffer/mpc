#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextTable>
#include <QScrollBar>
#include <QSettings>
#include <QNetworkInterface>
#include "mytcpclient.h"
#include <QTimer>
#include <QKeyEvent>
#include <QMessageBox>
#include <QCloseEvent>

#define TCPSERVER 10
#define TCPCLIENT 20
#define UDPSERVER 30
#define APPVERSION "V1.2"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  private slots:

    /******************************************************************************
     *
     * TCP Client
     *
     ******************************************************************************/

    void onTcpClientButtonClicked();
    void onTcpClientNewConnection(const QString &from, quint16 port);
    void onTcpClientStopButtonClicked();
    void onTcpClientTimeOut();
    void onTcpClientDisconnectButtonClicked();
    void onTcpClientDisconnected();
    void onTcpClientSendMessage();
    void onTcpClientAppendMessage(const QString &from, const QString &message);
    void Add_Message_To_Box(const QString &from, const QString &message);

    void onRefreshButtonClicked();

    void on_pushButton_up_clicked();

    void on_pushButton_right_clicked();

    void on_pushButton_left_clicked();

    void on_pushButton_down_clicked();

    void Send_X_Control(void);
    void Send_Y_Control(void);
    void Send_R_Control(void);
    void Send_Z_Control(void);

    void on_pushButton_Advertise_clicked();

    void on_Advertise_Manual_Control_clicked();

    void on_Advertise_Setpoint_Data_clicked();

    void on_Advertise_PID_Mode_clicked();

    void on_Advertise_Config_Kx_clicked();

    void on_pb_Send_Manual_Control_clicked();

    void on_pb_Send_PID_Mode_clicked();

    void on_pb_Send_Setpoint_Data_clicked();

    void on_pb_Send_Kx_clicked();

    void on_pushButton_Move_Up_clicked();

    void on_pushButton_Move_Down_clicked();

    void on_pushButton_Move_Left_clicked();

    void on_pushButton_Move_Right_clicked();

    void on_pushButton_Stop_clicked();

    void on_Knob_x_sliderMoved(double value);

    void on_Knob_y_sliderMoved(double value);

    void on_Knob_z_sliderMoved(double value);

    void on_Knob_r_sliderMoved(double value);

    void on_pushButton_Reset_x_clicked();

    void on_pushButton_Reset_y_clicked();

    void on_pushButton_Reset_r_clicked();

    void on_pushButton_Auto_depth_clicked();

    void on_Slider_sliderMoved(double value);

    void on_pushButton_Config_All_clicked();

    void on_pushButton_Reset_z_clicked();

    void on_doubleSpinBox_valueChanged(double arg1);

    void on_pushButton_Start_Manual_clicked();

    void on_button_TcpClient_clicked();

    void on_ScrollBar_Select_Manual_sliderMoved(int position);

    void on_pushButton_Light_clicked();

    void on_pushButton_Adv_Light_clicked();

    void on_M1_percent_valueChanged(int arg1);

    void on_M2_percent_valueChanged(int arg1);

    void on_M3_percent_valueChanged(int arg1);

    void on_M4_percent_valueChanged(int arg1);

    void on_M5_percent_valueChanged(int arg1);

    void on_M6_percent_valueChanged(int arg1);

    void on_M7_percent_valueChanged(int arg1);

    void on_Knob_Set_Heading_sliderMoved(double value);

    void on_pushButton_SetYaw_clicked(); 

    void on_pushButton_Adv_ROV_Config_clicked();

    void on_pushButton_Config_Percent_M_clicked();

    void on_pushButton_Config_Percent_M_2_clicked();

    void on_pushButton_Config_Percent_M_3_clicked();

    void on_pushButton_Config_Percent_M_4_clicked();

    void on_pushButton_Config_Percent_M_5_clicked();

    void on_pushButton_Config_Percent_M_6_clicked();

    void on_pushButton_Config_Percent_M_7_clicked();

    void on_pushButton_Set_Roll_clicked();

    void on_pushButton_Set_Pitch_clicked();

    void on_lineEdit_Set_Heading_textChanged(const QString &arg1);

public slots:
    void user_Timer_Timeout();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

  protected:
    void closeEvent(QCloseEvent *event);

  private:
    Ui::MainWindow *ui;
    void initUI();
    void loadSettings();
    void saveSettings();
    void findLocalIPs();
    bool setupConnection(quint8 type);

    void restoreWindowState();

    void user_Key_Press_Event(QKeyEvent *Key_event);

    QTextTableFormat tableFormat;

    MyTCPClient *mytcpclient = nullptr;

    QHostAddress tcpClientTargetAddr;
    quint16 tcpClientTargetPort;

    QHostAddress localAddr;

    QString settingsFileDir;
    QList<QNetworkInterface> interfaceList;
    quint8 type;
    QString messageTCP = "[TCP] ";

    QTimer *Timer;
};

#endif // MAINWINDOW_H
