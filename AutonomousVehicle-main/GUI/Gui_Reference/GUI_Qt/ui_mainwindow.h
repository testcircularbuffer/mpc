/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qwt_knob.h"
#include "qwt_slider.h"
#include "qwt_thermo.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tab_Test_Connect_ROV;
    QWidget *tab_connect;
    QGridLayout *gridLayout_8;
    QGridLayout *gridLayout_5;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QComboBox *comboBox_Interface;
    QFrame *line;
    QLabel *label_2;
    QLabel *label_LocalIP;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *button_Refresh;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_targetIP;
    QLineEdit *lineEdit_TcpClientTargetIP;
    QFrame *line_4;
    QLabel *label_targetPort;
    QLineEdit *lineEdit_TcpClientTargetPort;
    QSpacerItem *horizontalSpacer;
    QPushButton *button_TcpClient;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_7;
    QTextBrowser *textBrowser_TcpClientMessage;
    QGridLayout *gridLayout;
    QComboBox *comboBox_topic;
    QPushButton *button_TcpClientSend;
    QLabel *label_topic;
    QSpacerItem *horizontalSpacer_2;
    QWidget *Manual_Control;
    QGroupBox *groupBox_Control_1;
    QPushButton *pushButton_Reset_x;
    QPushButton *pushButton_Reset_y;
    QPushButton *pushButton_Reset_r;
    QwtKnob *Knob_y;
    QwtKnob *Knob_x;
    QwtKnob *Knob_r;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_Move_Left;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButton_Move_Up;
    QPushButton *pushButton_Stop;
    QPushButton *pushButton_Move_Down;
    QPushButton *pushButton_Move_Right;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QPushButton *pushButton_Reset_z;
    QwtKnob *Knob_z;
    QLabel *label_18;
    QwtSlider *Slider;
    QPushButton *pushButton_Auto_depth;
    QwtThermo *Thermo;
    QLineEdit *lineEdit_SetDepth;
    QGroupBox *groupBox_State;
    QLineEdit *my_Roll;
    QLineEdit *my_Pitch;
    QLineEdit *my_Yaw;
    QLineEdit *my_Depth;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QPushButton *pushButton_SetYaw;
    QLabel *label_22;
    QScrollBar *ScrollBar_Select_Manual;
    QLabel *label_21;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_12;
    QwtKnob *Knob_Set_Heading;
    QwtKnob *Knob_Now_Heading;
    QLineEdit *lineEdit_Set_Heading;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_28;
    QLabel *label_Light_State;
    QPushButton *pushButton_Adv_Light;
    QPushButton *pushButton_Light;
    QLabel *label_29;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_5;
    QPushButton *pushButton_Start_Manual;
    QScrollBar *ScrollBar_Manual_State;
    QLabel *label_20;
    QLabel *label_19;
    QWidget *tab_Connect_ROV;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox_Setpoint_Data;
    QPushButton *Advertise_Setpoint_Data;
    QPushButton *pb_Send_Setpoint_Data;
    QWidget *layoutWidget_4;
    QGridLayout *gridLayout_6;
    QLabel *label_13;
    QLabel *label_14;
    QSpacerItem *horizontalSpacer_7;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_11;
    QComboBox *cbb_Select_PID;
    QDoubleSpinBox *dbs_Setpoint_Value;
    QGroupBox *groupBox_PID_Mode;
    QPushButton *Advertise_PID_Mode;
    QPushButton *pb_Send_PID_Mode;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_15;
    QComboBox *cbb_PID_Mode_State_Select;
    QGroupBox *groupBox_K_PID;
    QPushButton *Advertise_Config_Kx;
    QPushButton *pb_Send_Kx;
    QWidget *layoutWidget5;
    QGridLayout *gridLayout_9;
    QLabel *label_16;
    QLabel *label_4;
    QLabel *label_17;
    QWidget *layoutWidget6;
    QGridLayout *gridLayout_10;
    QComboBox *cbb_Select_PID_2;
    QComboBox *cbb_Select_Kx;
    QLineEdit *le_Kx_PID;
    QGroupBox *groupBox_Manual_Control;
    QPushButton *Advertise_Manual_Control;
    QPushButton *pb_Send_Manual_Control;
    QWidget *layoutWidget_5;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_3;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_4;
    QComboBox *cbb_Manual_Mode;
    QDoubleSpinBox *dbs_Duty_Value;
    QGroupBox *groupBox_NowState;
    QLineEdit *lineEdit_Kd_Roll;
    QLineEdit *lineEdit_Kp_Roll;
    QLabel *label_46;
    QLineEdit *lineEdit_Ki_Roll;
    QLineEdit *lineEdit_setpoint_roll;
    QCheckBox *checkBox_PID_Roll;
    QLineEdit *lineEdit_Kp_pitch;
    QLineEdit *lineEdit_Kd_pitch;
    QLabel *label_77;
    QLineEdit *lineEdit_Ki_Pitch;
    QCheckBox *checkBox_PID_Pitch;
    QLineEdit *lineEdit_setpoint_pitch;
    QLineEdit *lineEdit_Kp_Yaw;
    QLineEdit *lineEdit_kd_yaw;
    QLineEdit *lineEdit_Ki_Yaw;
    QCheckBox *checkBox_PID_Yaw;
    QLineEdit *lineEdit_setpoint_yaw;
    QLineEdit *lineEdit_Kp_Depth;
    QLineEdit *lineEdit_kd_depth;
    QLabel *label_83;
    QLineEdit *lineEdit_Ki_Depth;
    QLabel *label_86;
    QCheckBox *checkBox_PID_Depth;
    QLineEdit *lineEdit_setpoint_depth;
    QPushButton *pushButton_Config_All;
    QWidget *tab_control;
    QGroupBox *groupBox_Joy_State;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;
    QLineEdit *lineEdit_Joy_X;
    QLineEdit *lineEdit_Joy_Y;
    QLineEdit *lineEdit_Joy_R;
    QLineEdit *lineEdit_Joy_Z;
    QLineEdit *lineEdit_Joy_Slider;
    QLabel *label_27;
    QGroupBox *groupBox_Turtle;
    QPushButton *pushButton_Advertise;
    QWidget *layoutWidget7;
    QGridLayout *gridLayout_12;
    QPushButton *pushButton_right;
    QVBoxLayout *verticalLayout_8;
    QPushButton *pushButton_up;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_down;
    QPushButton *pushButton_left;
    QGroupBox *groupBox_Motor_Percent;
    QLabel *label_34;
    QLabel *label_35;
    QLabel *label_36;
    QLabel *label_37;
    QLabel *label_38;
    QLabel *label_39;
    QLabel *label_40;
    QSpinBox *M2_percent;
    QSpinBox *M3_percent;
    QSpinBox *M4_percent;
    QSpinBox *M5_percent;
    QSpinBox *M6_percent;
    QSpinBox *M7_percent;
    QSpinBox *M1_percent;
    QLabel *label_41;
    QSpinBox *SetRoll;
    QSpinBox *SetPitch;
    QLabel *label_42;
    QPushButton *pushButton_Config_Percent_M;
    QPushButton *pushButton_Config_Percent_M_2;
    QPushButton *pushButton_Config_Percent_M_3;
    QPushButton *pushButton_Config_Percent_M_4;
    QPushButton *pushButton_Config_Percent_M_5;
    QPushButton *pushButton_Config_Percent_M_6;
    QPushButton *pushButton_Config_Percent_M_7;
    QPushButton *pushButton_Set_Roll;
    QPushButton *pushButton_Set_Pitch;
    QPushButton *pushButton_Adv_ROV_Config;
    QLineEdit *lineEdit_TcpClientSend;
    QMenuBar *menuBar;
    QMenu *menuROS_Client;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(906, 702);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        MainWindow->setPalette(palette);
        MainWindow->setCursor(QCursor(Qt::ArrowCursor));
        MainWindow->setTabShape(QTabWidget::Rounded);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tab_Test_Connect_ROV = new QTabWidget(centralWidget);
        tab_Test_Connect_ROV->setObjectName(QStringLiteral("tab_Test_Connect_ROV"));
        tab_Test_Connect_ROV->setEnabled(true);
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(238, 238, 236, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(211, 215, 207, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush2);
        QBrush brush3(QColor(186, 189, 182, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush3);
        tab_Test_Connect_ROV->setPalette(palette1);
        tab_connect = new QWidget();
        tab_connect->setObjectName(QStringLiteral("tab_connect"));
        gridLayout_8 = new QGridLayout(tab_connect);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        groupBox_3 = new QGroupBox(tab_connect);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_3);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        comboBox_Interface = new QComboBox(groupBox_3);
        comboBox_Interface->setObjectName(QStringLiteral("comboBox_Interface"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBox_Interface->sizePolicy().hasHeightForWidth());
        comboBox_Interface->setSizePolicy(sizePolicy);
        comboBox_Interface->setMinimumSize(QSize(100, 0));
        comboBox_Interface->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_5->addWidget(comboBox_Interface);

        line = new QFrame(groupBox_3);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_5->addWidget(line);

        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_5->addWidget(label_2);

        label_LocalIP = new QLabel(groupBox_3);
        label_LocalIP->setObjectName(QStringLiteral("label_LocalIP"));

        horizontalLayout_5->addWidget(label_LocalIP);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        button_Refresh = new QPushButton(groupBox_3);
        button_Refresh->setObjectName(QStringLiteral("button_Refresh"));

        horizontalLayout_5->addWidget(button_Refresh);


        horizontalLayout_4->addLayout(horizontalLayout_5);


        gridLayout_5->addWidget(groupBox_3, 0, 0, 1, 1);

        groupBox_4 = new QGroupBox(tab_connect);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        verticalLayout_6 = new QVBoxLayout(groupBox_4);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_targetIP = new QLabel(groupBox_4);
        label_targetIP->setObjectName(QStringLiteral("label_targetIP"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_targetIP->sizePolicy().hasHeightForWidth());
        label_targetIP->setSizePolicy(sizePolicy1);
        label_targetIP->setMinimumSize(QSize(0, 0));
        label_targetIP->setBaseSize(QSize(0, 0));

        horizontalLayout_7->addWidget(label_targetIP);

        lineEdit_TcpClientTargetIP = new QLineEdit(groupBox_4);
        lineEdit_TcpClientTargetIP->setObjectName(QStringLiteral("lineEdit_TcpClientTargetIP"));
        lineEdit_TcpClientTargetIP->setMinimumSize(QSize(150, 0));
        lineEdit_TcpClientTargetIP->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_7->addWidget(lineEdit_TcpClientTargetIP);

        line_4 = new QFrame(groupBox_4);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);

        horizontalLayout_7->addWidget(line_4);

        label_targetPort = new QLabel(groupBox_4);
        label_targetPort->setObjectName(QStringLiteral("label_targetPort"));
        sizePolicy1.setHeightForWidth(label_targetPort->sizePolicy().hasHeightForWidth());
        label_targetPort->setSizePolicy(sizePolicy1);
        label_targetPort->setMinimumSize(QSize(0, 0));

        horizontalLayout_7->addWidget(label_targetPort);

        lineEdit_TcpClientTargetPort = new QLineEdit(groupBox_4);
        lineEdit_TcpClientTargetPort->setObjectName(QStringLiteral("lineEdit_TcpClientTargetPort"));
        lineEdit_TcpClientTargetPort->setMinimumSize(QSize(80, 0));
        lineEdit_TcpClientTargetPort->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_7->addWidget(lineEdit_TcpClientTargetPort);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);

        button_TcpClient = new QPushButton(groupBox_4);
        button_TcpClient->setObjectName(QStringLiteral("button_TcpClient"));

        horizontalLayout_7->addWidget(button_TcpClient);


        verticalLayout_6->addLayout(horizontalLayout_7);


        gridLayout_5->addWidget(groupBox_4, 1, 0, 1, 1);

        groupBox_2 = new QGroupBox(tab_connect);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_7 = new QGridLayout(groupBox_2);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        textBrowser_TcpClientMessage = new QTextBrowser(groupBox_2);
        textBrowser_TcpClientMessage->setObjectName(QStringLiteral("textBrowser_TcpClientMessage"));

        gridLayout_7->addWidget(textBrowser_TcpClientMessage, 0, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        comboBox_topic = new QComboBox(groupBox_2);
        comboBox_topic->setObjectName(QStringLiteral("comboBox_topic"));
        comboBox_topic->setEditable(false);

        gridLayout->addWidget(comboBox_topic, 0, 2, 1, 1);

        button_TcpClientSend = new QPushButton(groupBox_2);
        button_TcpClientSend->setObjectName(QStringLiteral("button_TcpClientSend"));

        gridLayout->addWidget(button_TcpClientSend, 0, 4, 1, 1);

        label_topic = new QLabel(groupBox_2);
        label_topic->setObjectName(QStringLiteral("label_topic"));

        gridLayout->addWidget(label_topic, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout, 1, 0, 1, 1);


        gridLayout_5->addWidget(groupBox_2, 2, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_5, 0, 0, 1, 1);

        tab_Test_Connect_ROV->addTab(tab_connect, QString());
        Manual_Control = new QWidget();
        Manual_Control->setObjectName(QStringLiteral("Manual_Control"));
        groupBox_Control_1 = new QGroupBox(Manual_Control);
        groupBox_Control_1->setObjectName(QStringLiteral("groupBox_Control_1"));
        groupBox_Control_1->setEnabled(false);
        groupBox_Control_1->setGeometry(QRect(10, 170, 861, 381));
        pushButton_Reset_x = new QPushButton(groupBox_Control_1);
        pushButton_Reset_x->setObjectName(QStringLiteral("pushButton_Reset_x"));
        pushButton_Reset_x->setGeometry(QRect(70, 210, 80, 25));
        pushButton_Reset_y = new QPushButton(groupBox_Control_1);
        pushButton_Reset_y->setObjectName(QStringLiteral("pushButton_Reset_y"));
        pushButton_Reset_y->setGeometry(QRect(250, 210, 80, 25));
        pushButton_Reset_r = new QPushButton(groupBox_Control_1);
        pushButton_Reset_r->setObjectName(QStringLiteral("pushButton_Reset_r"));
        pushButton_Reset_r->setGeometry(QRect(420, 210, 80, 25));
        Knob_y = new QwtKnob(groupBox_Control_1);
        Knob_y->setObjectName(QStringLiteral("Knob_y"));
        Knob_y->setGeometry(QRect(200, 30, 181, 181));
        Knob_y->setLowerBound(-100);
        Knob_y->setUpperBound(100);
        Knob_y->setScaleMaxMinor(5);
        Knob_y->setScaleStepSize(20);
        Knob_y->setValue(0);
        Knob_y->setMarkerStyle(QwtKnob::Triangle);
        Knob_y->setMarkerSize(10);
        Knob_y->setBorderWidth(3);
        Knob_x = new QwtKnob(groupBox_Control_1);
        Knob_x->setObjectName(QStringLiteral("Knob_x"));
        Knob_x->setGeometry(QRect(20, 30, 191, 181));
        QPalette palette2;
        QBrush brush4(QColor(255, 255, 255, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush4);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        Knob_x->setPalette(palette2);
        Knob_x->setLowerBound(-100);
        Knob_x->setUpperBound(100);
        Knob_x->setScaleMaxMinor(5);
        Knob_x->setScaleStepSize(20);
        Knob_x->setValue(0);
        Knob_x->setMarkerStyle(QwtKnob::Triangle);
        Knob_x->setMarkerSize(10);
        Knob_x->setBorderWidth(3);
        Knob_r = new QwtKnob(groupBox_Control_1);
        Knob_r->setObjectName(QStringLiteral("Knob_r"));
        Knob_r->setGeometry(QRect(370, 30, 191, 181));
        Knob_r->setLowerBound(-100);
        Knob_r->setUpperBound(100);
        Knob_r->setScaleMaxMinor(5);
        Knob_r->setScaleStepSize(20);
        Knob_r->setValue(0);
        Knob_r->setMarkerStyle(QwtKnob::Triangle);
        Knob_r->setMarkerSize(10);
        Knob_r->setBorderWidth(3);
        layoutWidget = new QWidget(groupBox_Control_1);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(240, 260, 256, 91));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_Move_Left = new QPushButton(layoutWidget);
        pushButton_Move_Left->setObjectName(QStringLiteral("pushButton_Move_Left"));
        pushButton_Move_Left->setEnabled(false);
        pushButton_Move_Left->setAutoRepeat(true);
        pushButton_Move_Left->setAutoRepeatDelay(200);

        horizontalLayout->addWidget(pushButton_Move_Left);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        pushButton_Move_Up = new QPushButton(layoutWidget);
        pushButton_Move_Up->setObjectName(QStringLiteral("pushButton_Move_Up"));
        pushButton_Move_Up->setEnabled(false);
        pushButton_Move_Up->setCheckable(false);
        pushButton_Move_Up->setAutoRepeat(true);
        pushButton_Move_Up->setAutoRepeatDelay(200);

        verticalLayout_3->addWidget(pushButton_Move_Up);

        pushButton_Stop = new QPushButton(layoutWidget);
        pushButton_Stop->setObjectName(QStringLiteral("pushButton_Stop"));

        verticalLayout_3->addWidget(pushButton_Stop);

        pushButton_Move_Down = new QPushButton(layoutWidget);
        pushButton_Move_Down->setObjectName(QStringLiteral("pushButton_Move_Down"));
        pushButton_Move_Down->setEnabled(false);
        pushButton_Move_Down->setAutoRepeat(true);
        pushButton_Move_Down->setAutoRepeatDelay(200);

        verticalLayout_3->addWidget(pushButton_Move_Down);


        horizontalLayout->addLayout(verticalLayout_3);

        pushButton_Move_Right = new QPushButton(layoutWidget);
        pushButton_Move_Right->setObjectName(QStringLiteral("pushButton_Move_Right"));
        pushButton_Move_Right->setEnabled(false);
        pushButton_Move_Right->setAutoRepeat(true);
        pushButton_Move_Right->setAutoRepeatDelay(200);

        horizontalLayout->addWidget(pushButton_Move_Right);

        label_5 = new QLabel(groupBox_Control_1);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(110, 110, 16, 16));
        label_6 = new QLabel(groupBox_Control_1);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(280, 110, 31, 17));
        label_7 = new QLabel(groupBox_Control_1);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(460, 110, 20, 20));
        pushButton_Reset_z = new QPushButton(groupBox_Control_1);
        pushButton_Reset_z->setObjectName(QStringLiteral("pushButton_Reset_z"));
        pushButton_Reset_z->setGeometry(QRect(600, 210, 80, 25));
        Knob_z = new QwtKnob(groupBox_Control_1);
        Knob_z->setObjectName(QStringLiteral("Knob_z"));
        Knob_z->setEnabled(false);
        Knob_z->setGeometry(QRect(540, 30, 191, 181));
        Knob_z->setLowerBound(-100);
        Knob_z->setUpperBound(100);
        Knob_z->setScaleMaxMinor(5);
        Knob_z->setScaleStepSize(20);
        Knob_z->setValue(0);
        Knob_z->setMarkerStyle(QwtKnob::Triangle);
        Knob_z->setMarkerSize(10);
        Knob_z->setBorderWidth(3);
        label_18 = new QLabel(groupBox_Control_1);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(630, 110, 67, 17));
        Slider = new QwtSlider(groupBox_Control_1);
        Slider->setObjectName(QStringLiteral("Slider"));
        Slider->setEnabled(false);
        Slider->setGeometry(QRect(720, 50, 61, 281));
        Slider->setLowerBound(0);
        Slider->setUpperBound(300);
        Slider->setScaleMaxMajor(10);
        Slider->setScaleMaxMinor(5);
        Slider->setScaleStepSize(0);
        Slider->setValue(0);
        Slider->setTotalSteps(300u);
        pushButton_Auto_depth = new QPushButton(groupBox_Control_1);
        pushButton_Auto_depth->setObjectName(QStringLiteral("pushButton_Auto_depth"));
        pushButton_Auto_depth->setEnabled(false);
        pushButton_Auto_depth->setGeometry(QRect(720, 350, 131, 25));
        Thermo = new QwtThermo(groupBox_Control_1);
        Thermo->setObjectName(QStringLiteral("Thermo"));
        Thermo->setGeometry(QRect(790, 60, 61, 261));
        Thermo->setLowerBound(0);
        Thermo->setUpperBound(300);
        Thermo->setScaleMaxMajor(10);
        Thermo->setScaleMaxMinor(5);
        Thermo->setScaleStepSize(0);
        Thermo->setAlarmLevel(100);
        Thermo->setOrigin(1);
        Thermo->setSpacing(3);
        Thermo->setBorderWidth(2);
        Thermo->setPipeWidth(10);
        Thermo->setValue(0);
        lineEdit_SetDepth = new QLineEdit(groupBox_Control_1);
        lineEdit_SetDepth->setObjectName(QStringLiteral("lineEdit_SetDepth"));
        lineEdit_SetDepth->setGeometry(QRect(670, 350, 51, 25));
        groupBox_State = new QGroupBox(Manual_Control);
        groupBox_State->setObjectName(QStringLiteral("groupBox_State"));
        groupBox_State->setEnabled(false);
        groupBox_State->setGeometry(QRect(160, 0, 711, 181));
        my_Roll = new QLineEdit(groupBox_State);
        my_Roll->setObjectName(QStringLiteral("my_Roll"));
        my_Roll->setGeometry(QRect(70, 40, 81, 25));
        my_Pitch = new QLineEdit(groupBox_State);
        my_Pitch->setObjectName(QStringLiteral("my_Pitch"));
        my_Pitch->setGeometry(QRect(70, 70, 81, 25));
        my_Yaw = new QLineEdit(groupBox_State);
        my_Yaw->setObjectName(QStringLiteral("my_Yaw"));
        my_Yaw->setGeometry(QRect(70, 100, 81, 25));
        my_Depth = new QLineEdit(groupBox_State);
        my_Depth->setObjectName(QStringLiteral("my_Depth"));
        my_Depth->setGeometry(QRect(70, 130, 81, 25));
        layoutWidget1 = new QWidget(groupBox_State);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 40, 51, 111));
        verticalLayout_5 = new QVBoxLayout(layoutWidget1);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget1);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_5->addWidget(label_8);

        label_9 = new QLabel(layoutWidget1);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_5->addWidget(label_9);

        label_10 = new QLabel(layoutWidget1);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout_5->addWidget(label_10);

        label_11 = new QLabel(layoutWidget1);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout_5->addWidget(label_11);

        pushButton_SetYaw = new QPushButton(groupBox_State);
        pushButton_SetYaw->setObjectName(QStringLiteral("pushButton_SetYaw"));
        pushButton_SetYaw->setGeometry(QRect(340, 150, 131, 25));
        label_22 = new QLabel(groupBox_State);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setGeometry(QRect(540, 140, 61, 20));
        ScrollBar_Select_Manual = new QScrollBar(groupBox_State);
        ScrollBar_Select_Manual->setObjectName(QStringLiteral("ScrollBar_Select_Manual"));
        ScrollBar_Select_Manual->setEnabled(false);
        ScrollBar_Select_Manual->setGeometry(QRect(480, 100, 121, 31));
        ScrollBar_Select_Manual->setAutoFillBackground(false);
        ScrollBar_Select_Manual->setMinimum(1);
        ScrollBar_Select_Manual->setMaximum(2);
        ScrollBar_Select_Manual->setPageStep(2);
        ScrollBar_Select_Manual->setValue(1);
        ScrollBar_Select_Manual->setOrientation(Qt::Horizontal);
        label_21 = new QLabel(groupBox_State);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setGeometry(QRect(490, 140, 31, 17));
        doubleSpinBox = new QDoubleSpinBox(groupBox_State);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(500, 70, 69, 26));
        doubleSpinBox->setMaximum(80);
        doubleSpinBox->setSingleStep(10);
        doubleSpinBox->setValue(40);
        label_12 = new QLabel(groupBox_State);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(490, 30, 91, 41));
        Knob_Set_Heading = new QwtKnob(groupBox_State);
        Knob_Set_Heading->setObjectName(QStringLiteral("Knob_Set_Heading"));
        Knob_Set_Heading->setEnabled(false);
        Knob_Set_Heading->setGeometry(QRect(290, 10, 161, 141));
        Knob_Set_Heading->setLowerBound(-180);
        Knob_Set_Heading->setUpperBound(180);
        Knob_Set_Heading->setScaleMaxMajor(45);
        Knob_Set_Heading->setScaleMaxMinor(5);
        Knob_Set_Heading->setScaleStepSize(45);
        Knob_Set_Heading->setValue(0);
        Knob_Set_Heading->setTotalAngle(360);
        Knob_Set_Heading->setMarkerStyle(QwtKnob::Triangle);
        Knob_Set_Heading->setMarkerSize(10);
        Knob_Set_Heading->setBorderWidth(3);
        Knob_Now_Heading = new QwtKnob(groupBox_State);
        Knob_Now_Heading->setObjectName(QStringLiteral("Knob_Now_Heading"));
        Knob_Now_Heading->setGeometry(QRect(150, 10, 161, 141));
        Knob_Now_Heading->setLowerBound(-180);
        Knob_Now_Heading->setUpperBound(180);
        Knob_Now_Heading->setScaleMaxMajor(18);
        Knob_Now_Heading->setScaleMaxMinor(1);
        Knob_Now_Heading->setScaleStepSize(45);
        Knob_Now_Heading->setValue(0);
        Knob_Now_Heading->setTotalAngle(360);
        Knob_Now_Heading->setMarkerStyle(QwtKnob::Triangle);
        Knob_Now_Heading->setMarkerSize(10);
        Knob_Now_Heading->setBorderWidth(3);
        lineEdit_Set_Heading = new QLineEdit(groupBox_State);
        lineEdit_Set_Heading->setObjectName(QStringLiteral("lineEdit_Set_Heading"));
        lineEdit_Set_Heading->setEnabled(false);
        lineEdit_Set_Heading->setGeometry(QRect(290, 150, 51, 25));
        layoutWidget2 = new QWidget(groupBox_State);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(620, 40, 82, 104));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_28 = new QLabel(layoutWidget2);
        label_28->setObjectName(QStringLiteral("label_28"));

        verticalLayout_2->addWidget(label_28);

        label_Light_State = new QLabel(layoutWidget2);
        label_Light_State->setObjectName(QStringLiteral("label_Light_State"));

        verticalLayout_2->addWidget(label_Light_State);

        pushButton_Adv_Light = new QPushButton(layoutWidget2);
        pushButton_Adv_Light->setObjectName(QStringLiteral("pushButton_Adv_Light"));

        verticalLayout_2->addWidget(pushButton_Adv_Light);

        pushButton_Light = new QPushButton(layoutWidget2);
        pushButton_Light->setObjectName(QStringLiteral("pushButton_Light"));

        verticalLayout_2->addWidget(pushButton_Light);

        label_29 = new QLabel(groupBox_State);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setGeometry(QRect(200, 150, 61, 17));
        line_2 = new QFrame(groupBox_State);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(150, 20, 16, 161));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(groupBox_State);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(470, 20, 16, 161));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_5 = new QFrame(groupBox_State);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(600, 20, 16, 161));
        line_5->setFrameShape(QFrame::VLine);
        line_5->setFrameShadow(QFrame::Sunken);
        pushButton_Start_Manual = new QPushButton(Manual_Control);
        pushButton_Start_Manual->setObjectName(QStringLiteral("pushButton_Start_Manual"));
        pushButton_Start_Manual->setEnabled(false);
        pushButton_Start_Manual->setGeometry(QRect(20, 20, 101, 61));
        ScrollBar_Manual_State = new QScrollBar(Manual_Control);
        ScrollBar_Manual_State->setObjectName(QStringLiteral("ScrollBar_Manual_State"));
        ScrollBar_Manual_State->setEnabled(false);
        ScrollBar_Manual_State->setGeometry(QRect(10, 90, 121, 31));
        ScrollBar_Manual_State->setAutoFillBackground(false);
        ScrollBar_Manual_State->setMinimum(1);
        ScrollBar_Manual_State->setMaximum(2);
        ScrollBar_Manual_State->setPageStep(2);
        ScrollBar_Manual_State->setValue(1);
        ScrollBar_Manual_State->setOrientation(Qt::Horizontal);
        label_20 = new QLabel(Manual_Control);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setGeometry(QRect(10, 130, 31, 17));
        label_19 = new QLabel(Manual_Control);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(100, 130, 21, 17));
        tab_Test_Connect_ROV->addTab(Manual_Control, QString());
        tab_Connect_ROV = new QWidget();
        tab_Connect_ROV->setObjectName(QStringLiteral("tab_Connect_ROV"));
        gridLayout_2 = new QGridLayout(tab_Connect_ROV);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        groupBox_Setpoint_Data = new QGroupBox(tab_Connect_ROV);
        groupBox_Setpoint_Data->setObjectName(QStringLiteral("groupBox_Setpoint_Data"));
        groupBox_Setpoint_Data->setEnabled(false);
        Advertise_Setpoint_Data = new QPushButton(groupBox_Setpoint_Data);
        Advertise_Setpoint_Data->setObjectName(QStringLiteral("Advertise_Setpoint_Data"));
        Advertise_Setpoint_Data->setGeometry(QRect(10, 30, 101, 31));
        pb_Send_Setpoint_Data = new QPushButton(groupBox_Setpoint_Data);
        pb_Send_Setpoint_Data->setObjectName(QStringLiteral("pb_Send_Setpoint_Data"));
        pb_Send_Setpoint_Data->setEnabled(false);
        pb_Send_Setpoint_Data->setGeometry(QRect(10, 70, 101, 51));
        layoutWidget_4 = new QWidget(groupBox_Setpoint_Data);
        layoutWidget_4->setObjectName(QStringLiteral("layoutWidget_4"));
        layoutWidget_4->setGeometry(QRect(120, 50, 271, 31));
        gridLayout_6 = new QGridLayout(layoutWidget_4);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        label_13 = new QLabel(layoutWidget_4);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_6->addWidget(label_13, 0, 0, 1, 1);

        label_14 = new QLabel(layoutWidget_4);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_6->addWidget(label_14, 0, 2, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(128, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_7, 0, 1, 1, 1);

        layoutWidget3 = new QWidget(groupBox_Setpoint_Data);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(123, 80, 275, 28));
        gridLayout_11 = new QGridLayout(layoutWidget3);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        gridLayout_11->setContentsMargins(0, 0, 0, 0);
        cbb_Select_PID = new QComboBox(layoutWidget3);
        cbb_Select_PID->setObjectName(QStringLiteral("cbb_Select_PID"));

        gridLayout_11->addWidget(cbb_Select_PID, 0, 0, 1, 1);

        dbs_Setpoint_Value = new QDoubleSpinBox(layoutWidget3);
        dbs_Setpoint_Value->setObjectName(QStringLiteral("dbs_Setpoint_Value"));
        dbs_Setpoint_Value->setDecimals(6);
        dbs_Setpoint_Value->setMinimum(-180);
        dbs_Setpoint_Value->setMaximum(180);
        dbs_Setpoint_Value->setSingleStep(0.1);

        gridLayout_11->addWidget(dbs_Setpoint_Value, 0, 1, 1, 1);


        verticalLayout_7->addWidget(groupBox_Setpoint_Data);

        groupBox_PID_Mode = new QGroupBox(tab_Connect_ROV);
        groupBox_PID_Mode->setObjectName(QStringLiteral("groupBox_PID_Mode"));
        groupBox_PID_Mode->setEnabled(false);
        Advertise_PID_Mode = new QPushButton(groupBox_PID_Mode);
        Advertise_PID_Mode->setObjectName(QStringLiteral("Advertise_PID_Mode"));
        Advertise_PID_Mode->setGeometry(QRect(10, 30, 101, 31));
        pb_Send_PID_Mode = new QPushButton(groupBox_PID_Mode);
        pb_Send_PID_Mode->setObjectName(QStringLiteral("pb_Send_PID_Mode"));
        pb_Send_PID_Mode->setEnabled(false);
        pb_Send_PID_Mode->setGeometry(QRect(10, 70, 101, 51));
        layoutWidget4 = new QWidget(groupBox_PID_Mode);
        layoutWidget4->setObjectName(QStringLiteral("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(120, 40, 188, 50));
        verticalLayout_4 = new QVBoxLayout(layoutWidget4);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_15 = new QLabel(layoutWidget4);
        label_15->setObjectName(QStringLiteral("label_15"));

        verticalLayout_4->addWidget(label_15);

        cbb_PID_Mode_State_Select = new QComboBox(layoutWidget4);
        cbb_PID_Mode_State_Select->setObjectName(QStringLiteral("cbb_PID_Mode_State_Select"));

        verticalLayout_4->addWidget(cbb_PID_Mode_State_Select);


        verticalLayout_7->addWidget(groupBox_PID_Mode);

        groupBox_K_PID = new QGroupBox(tab_Connect_ROV);
        groupBox_K_PID->setObjectName(QStringLiteral("groupBox_K_PID"));
        groupBox_K_PID->setEnabled(false);
        Advertise_Config_Kx = new QPushButton(groupBox_K_PID);
        Advertise_Config_Kx->setObjectName(QStringLiteral("Advertise_Config_Kx"));
        Advertise_Config_Kx->setGeometry(QRect(10, 30, 101, 31));
        pb_Send_Kx = new QPushButton(groupBox_K_PID);
        pb_Send_Kx->setObjectName(QStringLiteral("pb_Send_Kx"));
        pb_Send_Kx->setEnabled(false);
        pb_Send_Kx->setGeometry(QRect(10, 70, 101, 51));
        layoutWidget5 = new QWidget(groupBox_K_PID);
        layoutWidget5->setObjectName(QStringLiteral("layoutWidget5"));
        layoutWidget5->setGeometry(QRect(121, 50, 261, 19));
        gridLayout_9 = new QGridLayout(layoutWidget5);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        gridLayout_9->setContentsMargins(0, 0, 0, 0);
        label_16 = new QLabel(layoutWidget5);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_9->addWidget(label_16, 0, 0, 1, 1);

        label_4 = new QLabel(layoutWidget5);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_9->addWidget(label_4, 0, 1, 1, 1);

        label_17 = new QLabel(layoutWidget5);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_9->addWidget(label_17, 0, 2, 1, 1);

        layoutWidget6 = new QWidget(groupBox_K_PID);
        layoutWidget6->setObjectName(QStringLiteral("layoutWidget6"));
        layoutWidget6->setGeometry(QRect(120, 70, 267, 27));
        gridLayout_10 = new QGridLayout(layoutWidget6);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        gridLayout_10->setContentsMargins(0, 0, 0, 0);
        cbb_Select_PID_2 = new QComboBox(layoutWidget6);
        cbb_Select_PID_2->setObjectName(QStringLiteral("cbb_Select_PID_2"));

        gridLayout_10->addWidget(cbb_Select_PID_2, 0, 0, 1, 1);

        cbb_Select_Kx = new QComboBox(layoutWidget6);
        cbb_Select_Kx->setObjectName(QStringLiteral("cbb_Select_Kx"));

        gridLayout_10->addWidget(cbb_Select_Kx, 0, 1, 1, 1);

        le_Kx_PID = new QLineEdit(layoutWidget6);
        le_Kx_PID->setObjectName(QStringLiteral("le_Kx_PID"));

        gridLayout_10->addWidget(le_Kx_PID, 0, 2, 1, 1);


        verticalLayout_7->addWidget(groupBox_K_PID);

        groupBox_Manual_Control = new QGroupBox(tab_Connect_ROV);
        groupBox_Manual_Control->setObjectName(QStringLiteral("groupBox_Manual_Control"));
        groupBox_Manual_Control->setEnabled(false);
        Advertise_Manual_Control = new QPushButton(groupBox_Manual_Control);
        Advertise_Manual_Control->setObjectName(QStringLiteral("Advertise_Manual_Control"));
        Advertise_Manual_Control->setGeometry(QRect(11, 31, 101, 31));
        pb_Send_Manual_Control = new QPushButton(groupBox_Manual_Control);
        pb_Send_Manual_Control->setObjectName(QStringLiteral("pb_Send_Manual_Control"));
        pb_Send_Manual_Control->setEnabled(false);
        pb_Send_Manual_Control->setGeometry(QRect(10, 70, 101, 51));
        layoutWidget_5 = new QWidget(groupBox_Manual_Control);
        layoutWidget_5->setObjectName(QStringLiteral("layoutWidget_5"));
        layoutWidget_5->setGeometry(QRect(120, 30, 291, 31));
        gridLayout_3 = new QGridLayout(layoutWidget_5);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget_5);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(128, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 0, 1, 1, 1);

        label_3 = new QLabel(layoutWidget_5);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 0, 2, 1, 1);

        layoutWidget_2 = new QWidget(groupBox_Manual_Control);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(121, 61, 291, 31));
        gridLayout_4 = new QGridLayout(layoutWidget_2);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        cbb_Manual_Mode = new QComboBox(layoutWidget_2);
        cbb_Manual_Mode->setObjectName(QStringLiteral("cbb_Manual_Mode"));

        gridLayout_4->addWidget(cbb_Manual_Mode, 0, 0, 1, 1);

        dbs_Duty_Value = new QDoubleSpinBox(layoutWidget_2);
        dbs_Duty_Value->setObjectName(QStringLiteral("dbs_Duty_Value"));
        dbs_Duty_Value->setDecimals(6);
        dbs_Duty_Value->setMinimum(-100);
        dbs_Duty_Value->setMaximum(100);
        dbs_Duty_Value->setSingleStep(0.1);

        gridLayout_4->addWidget(dbs_Duty_Value, 0, 1, 1, 1);


        verticalLayout_7->addWidget(groupBox_Manual_Control);


        horizontalLayout_2->addLayout(verticalLayout_7);

        groupBox_NowState = new QGroupBox(tab_Connect_ROV);
        groupBox_NowState->setObjectName(QStringLiteral("groupBox_NowState"));
        groupBox_NowState->setEnabled(false);
        lineEdit_Kd_Roll = new QLineEdit(groupBox_NowState);
        lineEdit_Kd_Roll->setObjectName(QStringLiteral("lineEdit_Kd_Roll"));
        lineEdit_Kd_Roll->setGeometry(QRect(40, 160, 51, 25));
        lineEdit_Kp_Roll = new QLineEdit(groupBox_NowState);
        lineEdit_Kp_Roll->setObjectName(QStringLiteral("lineEdit_Kp_Roll"));
        lineEdit_Kp_Roll->setGeometry(QRect(40, 80, 51, 25));
        label_46 = new QLabel(groupBox_NowState);
        label_46->setObjectName(QStringLiteral("label_46"));
        label_46->setGeometry(QRect(10, 80, 21, 17));
        lineEdit_Ki_Roll = new QLineEdit(groupBox_NowState);
        lineEdit_Ki_Roll->setObjectName(QStringLiteral("lineEdit_Ki_Roll"));
        lineEdit_Ki_Roll->setGeometry(QRect(40, 120, 51, 25));
        lineEdit_setpoint_roll = new QLineEdit(groupBox_NowState);
        lineEdit_setpoint_roll->setObjectName(QStringLiteral("lineEdit_setpoint_roll"));
        lineEdit_setpoint_roll->setGeometry(QRect(40, 200, 61, 25));
        checkBox_PID_Roll = new QCheckBox(groupBox_NowState);
        checkBox_PID_Roll->setObjectName(QStringLiteral("checkBox_PID_Roll"));
        checkBox_PID_Roll->setEnabled(false);
        checkBox_PID_Roll->setGeometry(QRect(40, 30, 81, 23));
        checkBox_PID_Roll->setAutoFillBackground(true);
        checkBox_PID_Roll->setCheckable(true);
        checkBox_PID_Roll->setChecked(false);
        checkBox_PID_Roll->setTristate(false);
        lineEdit_Kp_pitch = new QLineEdit(groupBox_NowState);
        lineEdit_Kp_pitch->setObjectName(QStringLiteral("lineEdit_Kp_pitch"));
        lineEdit_Kp_pitch->setGeometry(QRect(130, 80, 51, 25));
        lineEdit_Kd_pitch = new QLineEdit(groupBox_NowState);
        lineEdit_Kd_pitch->setObjectName(QStringLiteral("lineEdit_Kd_pitch"));
        lineEdit_Kd_pitch->setGeometry(QRect(130, 160, 51, 25));
        label_77 = new QLabel(groupBox_NowState);
        label_77->setObjectName(QStringLiteral("label_77"));
        label_77->setGeometry(QRect(10, 120, 21, 17));
        lineEdit_Ki_Pitch = new QLineEdit(groupBox_NowState);
        lineEdit_Ki_Pitch->setObjectName(QStringLiteral("lineEdit_Ki_Pitch"));
        lineEdit_Ki_Pitch->setGeometry(QRect(130, 120, 51, 25));
        checkBox_PID_Pitch = new QCheckBox(groupBox_NowState);
        checkBox_PID_Pitch->setObjectName(QStringLiteral("checkBox_PID_Pitch"));
        checkBox_PID_Pitch->setEnabled(false);
        checkBox_PID_Pitch->setGeometry(QRect(130, 30, 81, 23));
        checkBox_PID_Pitch->setAutoFillBackground(true);
        checkBox_PID_Pitch->setCheckable(true);
        checkBox_PID_Pitch->setTristate(false);
        lineEdit_setpoint_pitch = new QLineEdit(groupBox_NowState);
        lineEdit_setpoint_pitch->setObjectName(QStringLiteral("lineEdit_setpoint_pitch"));
        lineEdit_setpoint_pitch->setGeometry(QRect(130, 200, 61, 25));
        lineEdit_Kp_Yaw = new QLineEdit(groupBox_NowState);
        lineEdit_Kp_Yaw->setObjectName(QStringLiteral("lineEdit_Kp_Yaw"));
        lineEdit_Kp_Yaw->setGeometry(QRect(220, 80, 51, 25));
        lineEdit_kd_yaw = new QLineEdit(groupBox_NowState);
        lineEdit_kd_yaw->setObjectName(QStringLiteral("lineEdit_kd_yaw"));
        lineEdit_kd_yaw->setGeometry(QRect(220, 160, 51, 25));
        lineEdit_Ki_Yaw = new QLineEdit(groupBox_NowState);
        lineEdit_Ki_Yaw->setObjectName(QStringLiteral("lineEdit_Ki_Yaw"));
        lineEdit_Ki_Yaw->setGeometry(QRect(220, 120, 51, 25));
        checkBox_PID_Yaw = new QCheckBox(groupBox_NowState);
        checkBox_PID_Yaw->setObjectName(QStringLiteral("checkBox_PID_Yaw"));
        checkBox_PID_Yaw->setEnabled(false);
        checkBox_PID_Yaw->setGeometry(QRect(220, 30, 81, 23));
        checkBox_PID_Yaw->setAutoFillBackground(true);
        checkBox_PID_Yaw->setCheckable(true);
        checkBox_PID_Yaw->setTristate(false);
        lineEdit_setpoint_yaw = new QLineEdit(groupBox_NowState);
        lineEdit_setpoint_yaw->setObjectName(QStringLiteral("lineEdit_setpoint_yaw"));
        lineEdit_setpoint_yaw->setGeometry(QRect(220, 200, 61, 25));
        lineEdit_Kp_Depth = new QLineEdit(groupBox_NowState);
        lineEdit_Kp_Depth->setObjectName(QStringLiteral("lineEdit_Kp_Depth"));
        lineEdit_Kp_Depth->setGeometry(QRect(310, 80, 51, 25));
        lineEdit_kd_depth = new QLineEdit(groupBox_NowState);
        lineEdit_kd_depth->setObjectName(QStringLiteral("lineEdit_kd_depth"));
        lineEdit_kd_depth->setGeometry(QRect(310, 160, 51, 25));
        label_83 = new QLabel(groupBox_NowState);
        label_83->setObjectName(QStringLiteral("label_83"));
        label_83->setGeometry(QRect(10, 200, 67, 17));
        lineEdit_Ki_Depth = new QLineEdit(groupBox_NowState);
        lineEdit_Ki_Depth->setObjectName(QStringLiteral("lineEdit_Ki_Depth"));
        lineEdit_Ki_Depth->setGeometry(QRect(310, 120, 51, 25));
        label_86 = new QLabel(groupBox_NowState);
        label_86->setObjectName(QStringLiteral("label_86"));
        label_86->setGeometry(QRect(10, 160, 21, 17));
        checkBox_PID_Depth = new QCheckBox(groupBox_NowState);
        checkBox_PID_Depth->setObjectName(QStringLiteral("checkBox_PID_Depth"));
        checkBox_PID_Depth->setEnabled(false);
        checkBox_PID_Depth->setGeometry(QRect(310, 30, 92, 23));
        checkBox_PID_Depth->setAutoFillBackground(true);
        checkBox_PID_Depth->setCheckable(true);
        checkBox_PID_Depth->setChecked(false);
        checkBox_PID_Depth->setTristate(false);
        lineEdit_setpoint_depth = new QLineEdit(groupBox_NowState);
        lineEdit_setpoint_depth->setObjectName(QStringLiteral("lineEdit_setpoint_depth"));
        lineEdit_setpoint_depth->setGeometry(QRect(310, 200, 61, 25));
        pushButton_Config_All = new QPushButton(groupBox_NowState);
        pushButton_Config_All->setObjectName(QStringLiteral("pushButton_Config_All"));
        pushButton_Config_All->setEnabled(false);
        pushButton_Config_All->setGeometry(QRect(130, 470, 141, 51));

        horizontalLayout_2->addWidget(groupBox_NowState);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        tab_Test_Connect_ROV->addTab(tab_Connect_ROV, QString());
        tab_control = new QWidget();
        tab_control->setObjectName(QStringLiteral("tab_control"));
        groupBox_Joy_State = new QGroupBox(tab_control);
        groupBox_Joy_State->setObjectName(QStringLiteral("groupBox_Joy_State"));
        groupBox_Joy_State->setEnabled(false);
        groupBox_Joy_State->setGeometry(QRect(10, 10, 281, 261));
        label_23 = new QLabel(groupBox_Joy_State);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setGeometry(QRect(30, 40, 67, 17));
        label_24 = new QLabel(groupBox_Joy_State);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setGeometry(QRect(30, 80, 67, 17));
        label_25 = new QLabel(groupBox_Joy_State);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setGeometry(QRect(30, 120, 67, 17));
        label_26 = new QLabel(groupBox_Joy_State);
        label_26->setObjectName(QStringLiteral("label_26"));
        label_26->setGeometry(QRect(30, 160, 67, 17));
        lineEdit_Joy_X = new QLineEdit(groupBox_Joy_State);
        lineEdit_Joy_X->setObjectName(QStringLiteral("lineEdit_Joy_X"));
        lineEdit_Joy_X->setGeometry(QRect(130, 40, 113, 25));
        lineEdit_Joy_Y = new QLineEdit(groupBox_Joy_State);
        lineEdit_Joy_Y->setObjectName(QStringLiteral("lineEdit_Joy_Y"));
        lineEdit_Joy_Y->setGeometry(QRect(130, 80, 113, 25));
        lineEdit_Joy_R = new QLineEdit(groupBox_Joy_State);
        lineEdit_Joy_R->setObjectName(QStringLiteral("lineEdit_Joy_R"));
        lineEdit_Joy_R->setGeometry(QRect(130, 120, 113, 25));
        lineEdit_Joy_Z = new QLineEdit(groupBox_Joy_State);
        lineEdit_Joy_Z->setObjectName(QStringLiteral("lineEdit_Joy_Z"));
        lineEdit_Joy_Z->setGeometry(QRect(130, 160, 113, 25));
        lineEdit_Joy_Slider = new QLineEdit(groupBox_Joy_State);
        lineEdit_Joy_Slider->setObjectName(QStringLiteral("lineEdit_Joy_Slider"));
        lineEdit_Joy_Slider->setGeometry(QRect(130, 200, 113, 25));
        label_27 = new QLabel(groupBox_Joy_State);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setGeometry(QRect(30, 200, 67, 17));
        groupBox_Turtle = new QGroupBox(tab_control);
        groupBox_Turtle->setObjectName(QStringLiteral("groupBox_Turtle"));
        groupBox_Turtle->setEnabled(false);
        groupBox_Turtle->setGeometry(QRect(580, 10, 281, 261));
        pushButton_Advertise = new QPushButton(groupBox_Turtle);
        pushButton_Advertise->setObjectName(QStringLiteral("pushButton_Advertise"));
        pushButton_Advertise->setGeometry(QRect(80, 50, 121, 71));
        layoutWidget7 = new QWidget(groupBox_Turtle);
        layoutWidget7->setObjectName(QStringLiteral("layoutWidget7"));
        layoutWidget7->setGeometry(QRect(10, 170, 256, 81));
        gridLayout_12 = new QGridLayout(layoutWidget7);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        gridLayout_12->setContentsMargins(0, 0, 0, 0);
        pushButton_right = new QPushButton(layoutWidget7);
        pushButton_right->setObjectName(QStringLiteral("pushButton_right"));

        gridLayout_12->addWidget(pushButton_right, 0, 2, 1, 1);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        pushButton_up = new QPushButton(layoutWidget7);
        pushButton_up->setObjectName(QStringLiteral("pushButton_up"));

        verticalLayout_8->addWidget(pushButton_up);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer);

        pushButton_down = new QPushButton(layoutWidget7);
        pushButton_down->setObjectName(QStringLiteral("pushButton_down"));

        verticalLayout_8->addWidget(pushButton_down);


        gridLayout_12->addLayout(verticalLayout_8, 0, 1, 1, 1);

        pushButton_left = new QPushButton(layoutWidget7);
        pushButton_left->setObjectName(QStringLiteral("pushButton_left"));

        gridLayout_12->addWidget(pushButton_left, 0, 0, 1, 1);

        groupBox_Motor_Percent = new QGroupBox(tab_control);
        groupBox_Motor_Percent->setObjectName(QStringLiteral("groupBox_Motor_Percent"));
        groupBox_Motor_Percent->setEnabled(true);
        groupBox_Motor_Percent->setGeometry(QRect(320, 10, 231, 351));
        label_34 = new QLabel(groupBox_Motor_Percent);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setGeometry(QRect(30, 80, 31, 17));
        label_35 = new QLabel(groupBox_Motor_Percent);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setGeometry(QRect(30, 110, 31, 17));
        label_36 = new QLabel(groupBox_Motor_Percent);
        label_36->setObjectName(QStringLiteral("label_36"));
        label_36->setGeometry(QRect(30, 140, 31, 17));
        label_37 = new QLabel(groupBox_Motor_Percent);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setGeometry(QRect(30, 170, 31, 17));
        label_38 = new QLabel(groupBox_Motor_Percent);
        label_38->setObjectName(QStringLiteral("label_38"));
        label_38->setGeometry(QRect(30, 200, 31, 17));
        label_39 = new QLabel(groupBox_Motor_Percent);
        label_39->setObjectName(QStringLiteral("label_39"));
        label_39->setGeometry(QRect(30, 230, 31, 17));
        label_40 = new QLabel(groupBox_Motor_Percent);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setGeometry(QRect(30, 260, 31, 17));
        M2_percent = new QSpinBox(groupBox_Motor_Percent);
        M2_percent->setObjectName(QStringLiteral("M2_percent"));
        M2_percent->setGeometry(QRect(70, 110, 71, 26));
        M2_percent->setMaximum(150);
        M2_percent->setValue(100);
        M3_percent = new QSpinBox(groupBox_Motor_Percent);
        M3_percent->setObjectName(QStringLiteral("M3_percent"));
        M3_percent->setGeometry(QRect(70, 140, 71, 26));
        M3_percent->setMaximum(150);
        M3_percent->setValue(100);
        M4_percent = new QSpinBox(groupBox_Motor_Percent);
        M4_percent->setObjectName(QStringLiteral("M4_percent"));
        M4_percent->setGeometry(QRect(70, 170, 71, 26));
        M4_percent->setMaximum(150);
        M4_percent->setValue(100);
        M5_percent = new QSpinBox(groupBox_Motor_Percent);
        M5_percent->setObjectName(QStringLiteral("M5_percent"));
        M5_percent->setGeometry(QRect(70, 200, 71, 26));
        M5_percent->setMaximum(150);
        M5_percent->setValue(100);
        M6_percent = new QSpinBox(groupBox_Motor_Percent);
        M6_percent->setObjectName(QStringLiteral("M6_percent"));
        M6_percent->setGeometry(QRect(70, 230, 71, 26));
        M6_percent->setMaximum(150);
        M6_percent->setValue(100);
        M7_percent = new QSpinBox(groupBox_Motor_Percent);
        M7_percent->setObjectName(QStringLiteral("M7_percent"));
        M7_percent->setGeometry(QRect(70, 260, 71, 26));
        M7_percent->setMaximum(150);
        M7_percent->setValue(100);
        M1_percent = new QSpinBox(groupBox_Motor_Percent);
        M1_percent->setObjectName(QStringLiteral("M1_percent"));
        M1_percent->setGeometry(QRect(70, 80, 71, 26));
        M1_percent->setMaximum(150);
        M1_percent->setValue(100);
        label_41 = new QLabel(groupBox_Motor_Percent);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setGeometry(QRect(10, 290, 61, 17));
        SetRoll = new QSpinBox(groupBox_Motor_Percent);
        SetRoll->setObjectName(QStringLiteral("SetRoll"));
        SetRoll->setGeometry(QRect(70, 290, 71, 26));
        SetRoll->setMaximum(150);
        SetRoll->setValue(4);
        SetPitch = new QSpinBox(groupBox_Motor_Percent);
        SetPitch->setObjectName(QStringLiteral("SetPitch"));
        SetPitch->setGeometry(QRect(70, 320, 71, 26));
        SetPitch->setMaximum(150);
        SetPitch->setValue(4);
        label_42 = new QLabel(groupBox_Motor_Percent);
        label_42->setObjectName(QStringLiteral("label_42"));
        label_42->setGeometry(QRect(10, 320, 61, 17));
        pushButton_Config_Percent_M = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M->setObjectName(QStringLiteral("pushButton_Config_Percent_M"));
        pushButton_Config_Percent_M->setEnabled(false);
        pushButton_Config_Percent_M->setGeometry(QRect(160, 80, 61, 25));
        pushButton_Config_Percent_M_2 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_2->setObjectName(QStringLiteral("pushButton_Config_Percent_M_2"));
        pushButton_Config_Percent_M_2->setEnabled(false);
        pushButton_Config_Percent_M_2->setGeometry(QRect(160, 110, 61, 25));
        pushButton_Config_Percent_M_3 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_3->setObjectName(QStringLiteral("pushButton_Config_Percent_M_3"));
        pushButton_Config_Percent_M_3->setEnabled(false);
        pushButton_Config_Percent_M_3->setGeometry(QRect(160, 140, 61, 25));
        pushButton_Config_Percent_M_4 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_4->setObjectName(QStringLiteral("pushButton_Config_Percent_M_4"));
        pushButton_Config_Percent_M_4->setEnabled(false);
        pushButton_Config_Percent_M_4->setGeometry(QRect(160, 170, 61, 25));
        pushButton_Config_Percent_M_5 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_5->setObjectName(QStringLiteral("pushButton_Config_Percent_M_5"));
        pushButton_Config_Percent_M_5->setEnabled(false);
        pushButton_Config_Percent_M_5->setGeometry(QRect(160, 200, 61, 25));
        pushButton_Config_Percent_M_6 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_6->setObjectName(QStringLiteral("pushButton_Config_Percent_M_6"));
        pushButton_Config_Percent_M_6->setEnabled(false);
        pushButton_Config_Percent_M_6->setGeometry(QRect(160, 230, 61, 25));
        pushButton_Config_Percent_M_7 = new QPushButton(groupBox_Motor_Percent);
        pushButton_Config_Percent_M_7->setObjectName(QStringLiteral("pushButton_Config_Percent_M_7"));
        pushButton_Config_Percent_M_7->setEnabled(false);
        pushButton_Config_Percent_M_7->setGeometry(QRect(160, 260, 61, 25));
        pushButton_Set_Roll = new QPushButton(groupBox_Motor_Percent);
        pushButton_Set_Roll->setObjectName(QStringLiteral("pushButton_Set_Roll"));
        pushButton_Set_Roll->setEnabled(false);
        pushButton_Set_Roll->setGeometry(QRect(160, 290, 61, 25));
        pushButton_Set_Pitch = new QPushButton(groupBox_Motor_Percent);
        pushButton_Set_Pitch->setObjectName(QStringLiteral("pushButton_Set_Pitch"));
        pushButton_Set_Pitch->setEnabled(false);
        pushButton_Set_Pitch->setGeometry(QRect(160, 320, 61, 25));
        pushButton_Adv_ROV_Config = new QPushButton(groupBox_Motor_Percent);
        pushButton_Adv_ROV_Config->setObjectName(QStringLiteral("pushButton_Adv_ROV_Config"));
        pushButton_Adv_ROV_Config->setGeometry(QRect(70, 40, 89, 25));
        tab_Test_Connect_ROV->addTab(tab_control, QString());

        verticalLayout->addWidget(tab_Test_Connect_ROV);

        lineEdit_TcpClientSend = new QLineEdit(centralWidget);
        lineEdit_TcpClientSend->setObjectName(QStringLiteral("lineEdit_TcpClientSend"));
        lineEdit_TcpClientSend->setMinimumSize(QSize(241, 25));

        verticalLayout->addWidget(lineEdit_TcpClientSend);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 906, 22));
        menuROS_Client = new QMenu(menuBar);
        menuROS_Client->setObjectName(QStringLiteral("menuROS_Client"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuROS_Client->menuAction());

        retranslateUi(MainWindow);

        tab_Test_Connect_ROV->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ROV_CONTROL_GUI", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Network Interface:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "IP: ", Q_NULLPTR));
        label_LocalIP->setText(QApplication::translate("MainWindow", "255.255.255.255", Q_NULLPTR));
        button_Refresh->setText(QApplication::translate("MainWindow", "Refresh", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Client connect to:", Q_NULLPTR));
        label_targetIP->setText(QApplication::translate("MainWindow", "IP:", Q_NULLPTR));
        label_targetPort->setText(QApplication::translate("MainWindow", "Port:", Q_NULLPTR));
        button_TcpClient->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Message:", Q_NULLPTR));
        comboBox_topic->clear();
        comboBox_topic->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "/GUI_Sonar", Q_NULLPTR)
         << QApplication::translate("MainWindow", "/IMU_Depth", Q_NULLPTR)
         << QApplication::translate("MainWindow", "/turtle1/cmd_vel", Q_NULLPTR)
        );
        button_TcpClientSend->setText(QApplication::translate("MainWindow", "Subcribe topic", Q_NULLPTR));
        label_topic->setText(QApplication::translate("MainWindow", "Topic:", Q_NULLPTR));
        tab_Test_Connect_ROV->setTabText(tab_Test_Connect_ROV->indexOf(tab_connect), QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        groupBox_Control_1->setTitle(QApplication::translate("MainWindow", "Control", Q_NULLPTR));
        pushButton_Reset_x->setText(QApplication::translate("MainWindow", "Reset X", Q_NULLPTR));
        pushButton_Reset_y->setText(QApplication::translate("MainWindow", "Reset Y", Q_NULLPTR));
        pushButton_Reset_r->setText(QApplication::translate("MainWindow", "Reset R", Q_NULLPTR));
        pushButton_Move_Left->setText(QApplication::translate("MainWindow", "<", Q_NULLPTR));
        pushButton_Move_Up->setText(QApplication::translate("MainWindow", "^", Q_NULLPTR));
        pushButton_Stop->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
        pushButton_Move_Down->setText(QApplication::translate("MainWindow", "v", Q_NULLPTR));
        pushButton_Move_Right->setText(QApplication::translate("MainWindow", ">", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "X", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Y", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "R", Q_NULLPTR));
        pushButton_Reset_z->setText(QApplication::translate("MainWindow", "Reset Z", Q_NULLPTR));
        label_18->setText(QApplication::translate("MainWindow", "Z", Q_NULLPTR));
        pushButton_Auto_depth->setText(QApplication::translate("MainWindow", "Auto Depth", Q_NULLPTR));
        groupBox_State->setTitle(QApplication::translate("MainWindow", "State:", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Roll", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "Pitch", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "Yaw", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "Deprh", Q_NULLPTR));
        pushButton_SetYaw->setText(QApplication::translate("MainWindow", "Auto Heading", Q_NULLPTR));
        label_22->setText(QApplication::translate("MainWindow", "Joystick", Q_NULLPTR));
        label_21->setText(QApplication::translate("MainWindow", "GUI", Q_NULLPTR));
        label_12->setText(QApplication::translate("MainWindow", "Amplification\n"
" coefficient", Q_NULLPTR));
        lineEdit_Set_Heading->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        label_28->setText(QApplication::translate("MainWindow", "Light:", Q_NULLPTR));
        label_Light_State->setText(QApplication::translate("MainWindow", "OFF", Q_NULLPTR));
        pushButton_Adv_Light->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pushButton_Light->setText(QApplication::translate("MainWindow", "ON", Q_NULLPTR));
        label_29->setText(QApplication::translate("MainWindow", "Heading", Q_NULLPTR));
        pushButton_Start_Manual->setText(QApplication::translate("MainWindow", "Start Control\n"
" Manual", Q_NULLPTR));
        label_20->setText(QApplication::translate("MainWindow", "OFF", Q_NULLPTR));
        label_19->setText(QApplication::translate("MainWindow", "ON", Q_NULLPTR));
        tab_Test_Connect_ROV->setTabText(tab_Test_Connect_ROV->indexOf(Manual_Control), QApplication::translate("MainWindow", "Manual_Control", Q_NULLPTR));
        groupBox_Setpoint_Data->setTitle(QApplication::translate("MainWindow", "Setpoint Data:", Q_NULLPTR));
        Advertise_Setpoint_Data->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pb_Send_Setpoint_Data->setText(QApplication::translate("MainWindow", "Send_Setpoint\n"
"Data", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Select PID:", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "Setpoint Value:", Q_NULLPTR));
        cbb_Select_PID->clear();
        cbb_Select_PID->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Select_PID_Roll", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Pitch", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Yaw", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Depth", Q_NULLPTR)
        );
        groupBox_PID_Mode->setTitle(QApplication::translate("MainWindow", "PID Mode:", Q_NULLPTR));
        Advertise_PID_Mode->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pb_Send_PID_Mode->setText(QApplication::translate("MainWindow", "Send\n"
"PID_Mode", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "PID Mode:", Q_NULLPTR));
        cbb_PID_Mode_State_Select->clear();
        cbb_PID_Mode_State_Select->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "PID_Mode_Roll_ENA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Roll_DIS", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Pitch_ENA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Pitch_DIS", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Yaw_ENA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Yaw_DIS", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Depth_ENA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "PID_Mode_Depth_DIS", Q_NULLPTR)
        );
        groupBox_K_PID->setTitle(QApplication::translate("MainWindow", "Config K PID:", Q_NULLPTR));
        Advertise_Config_Kx->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pb_Send_Kx->setText(QApplication::translate("MainWindow", "Send\n"
"Config", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "Select PID:", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Select Kx", Q_NULLPTR));
        label_17->setText(QApplication::translate("MainWindow", "Value:", Q_NULLPTR));
        cbb_Select_PID_2->clear();
        cbb_Select_PID_2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Select_PID_Roll", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Pitch", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Yaw", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Select_PID_Depth", Q_NULLPTR)
        );
        cbb_Select_Kx->clear();
        cbb_Select_Kx->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Kp", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Ki", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Kd", Q_NULLPTR)
        );
        groupBox_Manual_Control->setTitle(QApplication::translate("MainWindow", "Manual Control:", Q_NULLPTR));
        Advertise_Manual_Control->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pb_Send_Manual_Control->setText(QApplication::translate("MainWindow", "Send_Manual\n"
"Control", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Manual_Mode:", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Duty_Value:", Q_NULLPTR));
        cbb_Manual_Mode->clear();
        cbb_Manual_Mode->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Manual_Mode_ForWard", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_BackWard", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_TurnLeft", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_TurnRight", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_HorizontialLeft", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_HorizontialRight", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_Dive", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Manual_Mode_Rise", Q_NULLPTR)
        );
        groupBox_NowState->setTitle(QApplication::translate("MainWindow", "Now State:", Q_NULLPTR));
        label_46->setText(QApplication::translate("MainWindow", "Kp:", Q_NULLPTR));
        checkBox_PID_Roll->setText(QApplication::translate("MainWindow", "PID Roll", Q_NULLPTR));
        label_77->setText(QApplication::translate("MainWindow", "Ki:", Q_NULLPTR));
        checkBox_PID_Pitch->setText(QApplication::translate("MainWindow", "PID Pitch", Q_NULLPTR));
        checkBox_PID_Yaw->setText(QApplication::translate("MainWindow", "PID Yaw", Q_NULLPTR));
        label_83->setText(QApplication::translate("MainWindow", "SP:", Q_NULLPTR));
        label_86->setText(QApplication::translate("MainWindow", "Kd:", Q_NULLPTR));
        checkBox_PID_Depth->setText(QApplication::translate("MainWindow", "PID Depth", Q_NULLPTR));
        pushButton_Config_All->setText(QApplication::translate("MainWindow", "Config\n"
"All", Q_NULLPTR));
        tab_Test_Connect_ROV->setTabText(tab_Test_Connect_ROV->indexOf(tab_Connect_ROV), QApplication::translate("MainWindow", "PID Mode", Q_NULLPTR));
        groupBox_Joy_State->setTitle(QApplication::translate("MainWindow", "Joystick", Q_NULLPTR));
        label_23->setText(QApplication::translate("MainWindow", "X Value", Q_NULLPTR));
        label_24->setText(QApplication::translate("MainWindow", "Y Value", Q_NULLPTR));
        label_25->setText(QApplication::translate("MainWindow", "R Value", Q_NULLPTR));
        label_26->setText(QApplication::translate("MainWindow", "Z value", Q_NULLPTR));
        label_27->setText(QApplication::translate("MainWindow", "Slider", Q_NULLPTR));
        groupBox_Turtle->setTitle(QApplication::translate("MainWindow", "Turtle test", Q_NULLPTR));
        pushButton_Advertise->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        pushButton_right->setText(QApplication::translate("MainWindow", "Right", Q_NULLPTR));
        pushButton_up->setText(QApplication::translate("MainWindow", "Up", Q_NULLPTR));
        pushButton_down->setText(QApplication::translate("MainWindow", "Down", Q_NULLPTR));
        pushButton_left->setText(QApplication::translate("MainWindow", "Left", Q_NULLPTR));
        groupBox_Motor_Percent->setTitle(QApplication::translate("MainWindow", "Motor config", Q_NULLPTR));
        label_34->setText(QApplication::translate("MainWindow", "M1", Q_NULLPTR));
        label_35->setText(QApplication::translate("MainWindow", "M2", Q_NULLPTR));
        label_36->setText(QApplication::translate("MainWindow", "M3", Q_NULLPTR));
        label_37->setText(QApplication::translate("MainWindow", "M4", Q_NULLPTR));
        label_38->setText(QApplication::translate("MainWindow", "M5", Q_NULLPTR));
        label_39->setText(QApplication::translate("MainWindow", "M6", Q_NULLPTR));
        label_40->setText(QApplication::translate("MainWindow", "M7", Q_NULLPTR));
        label_41->setText(QApplication::translate("MainWindow", "Set Roll", Q_NULLPTR));
        label_42->setText(QApplication::translate("MainWindow", "Set Pitch", Q_NULLPTR));
        pushButton_Config_Percent_M->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_2->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_3->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_4->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_5->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_6->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Config_Percent_M_7->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Set_Roll->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Set_Pitch->setText(QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        pushButton_Adv_ROV_Config->setText(QApplication::translate("MainWindow", "Advertise", Q_NULLPTR));
        tab_Test_Connect_ROV->setTabText(tab_Test_Connect_ROV->indexOf(tab_control), QApplication::translate("MainWindow", "Config", Q_NULLPTR));
        menuROS_Client->setTitle(QApplication::translate("MainWindow", "ROS Control", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
