/*********************************************************************
* FIle name: My_Define.h
* Author: Quang Huy Le
* Date modified: 
*********************************************************************/
#ifndef MY_DEFINE_H_
#define MY_DEFINE_H_
/* Includes ------------------------------------------------------------------*/

#include <QVariant>
#include <QString>
#include <QQueue>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "json.h"

/* Exported constants/macros --------------------------------------------------------*/
#define Manual_Mode_ForWard				"Manual_Mode_ForWard"
#define Manual_Mode_BackWard			"Manual_Mode_BackWard"
#define Manual_Mode_TurnLeft			"Manual_Mode_TurnLeft"
#define Manual_Mode_TurnRight			"Manual_Mode_TurnRight"
#define Manual_Mode_HorizontialLeft		"Manual_Mode_HorizontialLeft"
#define Manual_Mode_HorizontialRight	"Manual_Mode_HorizontialRight"
#define Manual_Mode_Dive				"Manual_Mode_Dive"
#define Manual_Mode_Rise				"Manual_Mode_Rise"

#define PID_Mode_Roll_ENA		"PID_Mode_Roll_ENA"
#define PID_Mode_Roll_DIS		"PID_Mode_Roll_DIS"
#define PID_Mode_Pitch_ENA		"PID_Mode_Pitch_ENA"
#define PID_Mode_Pitch_DIS		"PID_Mode_Pitch_DIS"
#define PID_Mode_Yaw_ENA		"PID_Mode_Yaw_ENA"
#define PID_Mode_Yaw_DIS		"PID_Mode_Yaw_DIS"
#define PID_Mode_Depth_ENA		"PID_Mode_Depth_ENA"
#define PID_Mode_Depth_DIS		"PID_Mode_Depth_DIS"

#define Select_PID_Roll			"Select_PID_Roll"
#define Select_PID_Pitch		"Select_PID_Pitch"
#define Select_PID_Yaw			"Select_PID_Yaw"
#define Select_PID_Depth		"Select_PID_Depth"

#define Light_State_ON			"Light_State_ON"
#define Light_State_OFF			"Light_State_OFF"
	
#define Valve_State_OPEN		"Valve_State_OPEN"
#define Valve_State_CLOSE		"Valve_State_CLOSE"

#define Config_Kp           	"Config_Kp"
#define Config_Ki           	"Config_Ki"
#define Config_Kd            	"Config_Kd"

#define Select_Config_M1_Percent            "1"
#define Select_Config_M2_Percent            "2"
#define Select_Config_M3_Percent            "3"
#define Select_Config_M4_Percent            "4"
#define Select_Config_M5_Percent            "5"
#define Select_Config_M6_Percent            "6"
#define Select_Config_M7_Percent            "7"
#define Select_Config_Set_Roll              "8"
#define Select_Config_Set_Pitch             "9"
#define Select_Config_PID_Percent           "10"

/* Exported types ------------------------------------------------------------*/

union Can_float
{
  float Can_Float_Value;
  struct
  {
    unsigned byte1:8;
    unsigned byte2:8;
    unsigned byte3:8;
    unsigned byte4:8;
  } byte;
};

#endif  // MY_DEFINE_H_
