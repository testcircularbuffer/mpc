/**
 * \file ROS_ROV.h
 */

#ifndef ROS_ROV_H
#define ROS_ROV_H

#include <QVariant>
#include <QString>
#include <QQueue>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "json.h"
#include "My_Define.h"

using QtJson::JsonObject;

typedef struct
{
    double Roll;
    double Pitch;
    double Yaw;
    double Depth;
} IMU_Depth_Data;

typedef struct
{
    QString Select_PID;
    QString Config_Kx;
    int Value;
} K_PID_Data;

typedef struct
{
    QString Manual_Mode;
    int Duty_Value;
} Manual_Control_Data;

typedef struct
{
    QString Select_PID;
    int Setpoint_Value;
} Setpoint_Data;


JsonObject Json_Make_Setpoint_Data(QString Select_PID, double Setpoint_Value)
{
    JsonObject Setpoint_Data;
    JsonObject msg;
    msg["Select_PID"] = Select_PID;
    msg["Setpoint_Value"] = Setpoint_Value;
    Setpoint_Data["op"] = "publish";
    Setpoint_Data["topic"] = "GUI_Config_Setpoint";
    Setpoint_Data["msg"] = msg;
    return Setpoint_Data;
}

JsonObject Json_Make_ROV_Config_Data(QString Manual_Mode, double Duty_Value)
{
    JsonObject ROV_Config_Data;
    JsonObject msg;
    msg["Manual_Mode"] = Manual_Mode;
    msg["Duty_Value"] = Duty_Value;
    ROV_Config_Data["op"] = "publish";
    ROV_Config_Data["topic"] = "GUI_ROV_Conf";
    ROV_Config_Data["msg"] = msg;
    return ROV_Config_Data;
}

JsonObject Json_Make_K_PID_Data(QString Select_PID, QString Config_Kx, double Value)
{
    JsonObject K_PID_Data;
    JsonObject msg;
    msg["Select_PID"] = Select_PID;
    msg["Config_Kx"] = Config_Kx;
    msg["Value"] = Value;
    K_PID_Data["op"] = "publish";
    K_PID_Data["topic"] = "GUI_Config_K_PID";
    K_PID_Data["msg"] = msg;
    return K_PID_Data;
}

JsonObject Json_Make_Manual_Control_Data(QString Manual_Mode, double Duty_Value)
{
    JsonObject Manual_Control_Data;
    JsonObject msg;
    msg["Manual_Mode"] = Manual_Mode;
    msg["Duty_Value"] = Duty_Value;
    Manual_Control_Data["op"] = "publish";
    Manual_Control_Data["topic"] = "GUI_Manual_Control";
    Manual_Control_Data["msg"] = msg;
    return Manual_Control_Data;
}

JsonObject Json_Make_PID_Mode_Data(QString PID_Mode)
{
    JsonObject PID_Mode_Data;
    JsonObject msg;
    msg["data"] = PID_Mode;
    PID_Mode_Data["op"] = "publish";
    PID_Mode_Data["topic"] = "GUI_PID_Mode";
    PID_Mode_Data["msg"] = msg;
    return PID_Mode_Data;
}

JsonObject Json_Make_Light_Control_Data(QString Light_State)
{
    JsonObject Light_Control_Data;
    JsonObject msg;
    msg["data"] = Light_State;
    Light_Control_Data["op"] = "publish";
    Light_Control_Data["topic"] = "GUI_Light_Control";
    Light_Control_Data["msg"] = msg;
    return Light_Control_Data;
}
JsonObject Json_Make_Advertise_Msg(QString topic, QString type)
{
    JsonObject Advertise_Msg;
    Advertise_Msg["op"] = "advertise";
    Advertise_Msg["topic"] = topic;
    Advertise_Msg["type"] = type;
    return Advertise_Msg;
}

JsonObject Json_Make_Unadvertise_Msg(QString topic, QString type)
{
    JsonObject Unadvertise_Msg;
    Unadvertise_Msg["op"] = "unadvertise";
    Unadvertise_Msg["topic"] = topic;
    Unadvertise_Msg["type"] = type;
    return Unadvertise_Msg;
}

JsonObject Json_Make_Subscribe_Msg(QString topic)
{
    JsonObject Subscribe_Msg;
    Subscribe_Msg["op"] = "subscribe";
    Subscribe_Msg["topic"] = topic;
    return Subscribe_Msg;
}

JsonObject Json_Make_Unsubscribe_Msg(QString topic)
{
    JsonObject Unsubscribe_Msg;
    Unsubscribe_Msg["op"] = "unsubscribe";
    Unsubscribe_Msg["topic"] = topic;
    return Unsubscribe_Msg;
}

IMU_Depth_Data Json_Unpack_IMU_Depth_Data(JsonObject Json_Msg)
{
    IMU_Depth_Data _IMU_Depth_Data;
    _IMU_Depth_Data.Roll = Json_Msg["Roll"].toDouble();
    _IMU_Depth_Data.Pitch = Json_Msg["Pitch"].toDouble();
    _IMU_Depth_Data.Yaw = Json_Msg["Yaw"].toDouble();
    _IMU_Depth_Data.Depth = Json_Msg["Depth"].toDouble();
    return _IMU_Depth_Data;
}


#endif //ROS_ROV_H
