/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[52];
    char stringdata0[1299];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 24), // "onTcpClientButtonClicked"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 24), // "onTcpClientNewConnection"
QT_MOC_LITERAL(4, 62, 4), // "from"
QT_MOC_LITERAL(5, 67, 4), // "port"
QT_MOC_LITERAL(6, 72, 28), // "onTcpClientStopButtonClicked"
QT_MOC_LITERAL(7, 101, 18), // "onTcpClientTimeOut"
QT_MOC_LITERAL(8, 120, 34), // "onTcpClientDisconnectButtonCl..."
QT_MOC_LITERAL(9, 155, 23), // "onTcpClientDisconnected"
QT_MOC_LITERAL(10, 179, 22), // "onTcpClientSendMessage"
QT_MOC_LITERAL(11, 202, 24), // "onTcpClientAppendMessage"
QT_MOC_LITERAL(12, 227, 7), // "message"
QT_MOC_LITERAL(13, 235, 22), // "onRefreshButtonClicked"
QT_MOC_LITERAL(14, 258, 24), // "on_pushButton_up_clicked"
QT_MOC_LITERAL(15, 283, 27), // "on_pushButton_right_clicked"
QT_MOC_LITERAL(16, 311, 26), // "on_pushButton_left_clicked"
QT_MOC_LITERAL(17, 338, 26), // "on_pushButton_down_clicked"
QT_MOC_LITERAL(18, 365, 31), // "on_pushButton_Advertise_clicked"
QT_MOC_LITERAL(19, 397, 35), // "on_Advertise_Manual_Control_c..."
QT_MOC_LITERAL(20, 433, 34), // "on_Advertise_Setpoint_Data_cl..."
QT_MOC_LITERAL(21, 468, 29), // "on_Advertise_PID_Mode_clicked"
QT_MOC_LITERAL(22, 498, 30), // "on_Advertise_Config_Kx_clicked"
QT_MOC_LITERAL(23, 529, 27), // "on_button_TcpClient_clicked"
QT_MOC_LITERAL(24, 557, 33), // "on_pb_Send_Manual_Control_cli..."
QT_MOC_LITERAL(25, 591, 27), // "on_pb_Send_PID_Mode_clicked"
QT_MOC_LITERAL(26, 619, 32), // "on_pb_Send_Setpoint_Data_clicked"
QT_MOC_LITERAL(27, 652, 21), // "on_pb_Send_Kx_clicked"
QT_MOC_LITERAL(28, 674, 31), // "on_button_TcpClientSend_clicked"
QT_MOC_LITERAL(29, 706, 29), // "on_pushButton_Move_Up_clicked"
QT_MOC_LITERAL(30, 736, 31), // "on_pushButton_Move_Down_clicked"
QT_MOC_LITERAL(31, 768, 29), // "on_pushButton_Move_Up_pressed"
QT_MOC_LITERAL(32, 798, 31), // "on_pushButton_Move_Left_clicked"
QT_MOC_LITERAL(33, 830, 32), // "on_pushButton_Move_Right_clicked"
QT_MOC_LITERAL(34, 863, 26), // "on_pushButton_Stop_clicked"
QT_MOC_LITERAL(35, 890, 21), // "on_Knob_x_sliderMoved"
QT_MOC_LITERAL(36, 912, 5), // "value"
QT_MOC_LITERAL(37, 918, 21), // "on_Knob_y_sliderMoved"
QT_MOC_LITERAL(38, 940, 21), // "on_Knob_z_sliderMoved"
QT_MOC_LITERAL(39, 962, 21), // "on_Knob_r_sliderMoved"
QT_MOC_LITERAL(40, 984, 29), // "on_pushButton_Reset_x_clicked"
QT_MOC_LITERAL(41, 1014, 29), // "on_pushButton_Reset_y_clicked"
QT_MOC_LITERAL(42, 1044, 29), // "on_pushButton_Reset_r_clicked"
QT_MOC_LITERAL(43, 1074, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(44, 1096, 32), // "on_pushButton_Auto_depth_clicked"
QT_MOC_LITERAL(45, 1129, 21), // "on_Slider_sliderMoved"
QT_MOC_LITERAL(46, 1151, 32), // "on_pushButton_Config_All_clicked"
QT_MOC_LITERAL(47, 1184, 29), // "on_pushButton_Reset_z_clicked"
QT_MOC_LITERAL(48, 1214, 29), // "on_doubleSpinBox_valueChanged"
QT_MOC_LITERAL(49, 1244, 4), // "arg1"
QT_MOC_LITERAL(50, 1249, 30), // "on_pushButton_Move_Up_released"
QT_MOC_LITERAL(51, 1280, 18) // "user_Timer_Timeout"

    },
    "MainWindow\0onTcpClientButtonClicked\0"
    "\0onTcpClientNewConnection\0from\0port\0"
    "onTcpClientStopButtonClicked\0"
    "onTcpClientTimeOut\0"
    "onTcpClientDisconnectButtonClicked\0"
    "onTcpClientDisconnected\0onTcpClientSendMessage\0"
    "onTcpClientAppendMessage\0message\0"
    "onRefreshButtonClicked\0on_pushButton_up_clicked\0"
    "on_pushButton_right_clicked\0"
    "on_pushButton_left_clicked\0"
    "on_pushButton_down_clicked\0"
    "on_pushButton_Advertise_clicked\0"
    "on_Advertise_Manual_Control_clicked\0"
    "on_Advertise_Setpoint_Data_clicked\0"
    "on_Advertise_PID_Mode_clicked\0"
    "on_Advertise_Config_Kx_clicked\0"
    "on_button_TcpClient_clicked\0"
    "on_pb_Send_Manual_Control_clicked\0"
    "on_pb_Send_PID_Mode_clicked\0"
    "on_pb_Send_Setpoint_Data_clicked\0"
    "on_pb_Send_Kx_clicked\0"
    "on_button_TcpClientSend_clicked\0"
    "on_pushButton_Move_Up_clicked\0"
    "on_pushButton_Move_Down_clicked\0"
    "on_pushButton_Move_Up_pressed\0"
    "on_pushButton_Move_Left_clicked\0"
    "on_pushButton_Move_Right_clicked\0"
    "on_pushButton_Stop_clicked\0"
    "on_Knob_x_sliderMoved\0value\0"
    "on_Knob_y_sliderMoved\0on_Knob_z_sliderMoved\0"
    "on_Knob_r_sliderMoved\0"
    "on_pushButton_Reset_x_clicked\0"
    "on_pushButton_Reset_y_clicked\0"
    "on_pushButton_Reset_r_clicked\0"
    "on_pushButton_clicked\0"
    "on_pushButton_Auto_depth_clicked\0"
    "on_Slider_sliderMoved\0"
    "on_pushButton_Config_All_clicked\0"
    "on_pushButton_Reset_z_clicked\0"
    "on_doubleSpinBox_valueChanged\0arg1\0"
    "on_pushButton_Move_Up_released\0"
    "user_Timer_Timeout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      45,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  239,    2, 0x08 /* Private */,
       3,    2,  240,    2, 0x08 /* Private */,
       6,    0,  245,    2, 0x08 /* Private */,
       7,    0,  246,    2, 0x08 /* Private */,
       8,    0,  247,    2, 0x08 /* Private */,
       9,    0,  248,    2, 0x08 /* Private */,
      10,    0,  249,    2, 0x08 /* Private */,
      11,    2,  250,    2, 0x08 /* Private */,
      13,    0,  255,    2, 0x08 /* Private */,
      14,    0,  256,    2, 0x08 /* Private */,
      15,    0,  257,    2, 0x08 /* Private */,
      16,    0,  258,    2, 0x08 /* Private */,
      17,    0,  259,    2, 0x08 /* Private */,
      18,    0,  260,    2, 0x08 /* Private */,
      19,    0,  261,    2, 0x08 /* Private */,
      20,    0,  262,    2, 0x08 /* Private */,
      21,    0,  263,    2, 0x08 /* Private */,
      22,    0,  264,    2, 0x08 /* Private */,
      23,    0,  265,    2, 0x08 /* Private */,
      24,    0,  266,    2, 0x08 /* Private */,
      25,    0,  267,    2, 0x08 /* Private */,
      26,    0,  268,    2, 0x08 /* Private */,
      27,    0,  269,    2, 0x08 /* Private */,
      28,    0,  270,    2, 0x08 /* Private */,
      29,    0,  271,    2, 0x08 /* Private */,
      30,    0,  272,    2, 0x08 /* Private */,
      31,    0,  273,    2, 0x08 /* Private */,
      32,    0,  274,    2, 0x08 /* Private */,
      33,    0,  275,    2, 0x08 /* Private */,
      34,    0,  276,    2, 0x08 /* Private */,
      35,    1,  277,    2, 0x08 /* Private */,
      37,    1,  280,    2, 0x08 /* Private */,
      38,    1,  283,    2, 0x08 /* Private */,
      39,    1,  286,    2, 0x08 /* Private */,
      40,    0,  289,    2, 0x08 /* Private */,
      41,    0,  290,    2, 0x08 /* Private */,
      42,    0,  291,    2, 0x08 /* Private */,
      43,    0,  292,    2, 0x08 /* Private */,
      44,    0,  293,    2, 0x08 /* Private */,
      45,    1,  294,    2, 0x08 /* Private */,
      46,    0,  297,    2, 0x08 /* Private */,
      47,    0,  298,    2, 0x08 /* Private */,
      48,    1,  299,    2, 0x08 /* Private */,
      50,    0,  302,    2, 0x08 /* Private */,
      51,    0,  303,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    4,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   36,
    QMetaType::Void, QMetaType::Double,   36,
    QMetaType::Void, QMetaType::Double,   36,
    QMetaType::Void, QMetaType::Double,   36,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   36,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   49,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onTcpClientButtonClicked(); break;
        case 1: _t->onTcpClientNewConnection((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 2: _t->onTcpClientStopButtonClicked(); break;
        case 3: _t->onTcpClientTimeOut(); break;
        case 4: _t->onTcpClientDisconnectButtonClicked(); break;
        case 5: _t->onTcpClientDisconnected(); break;
        case 6: _t->onTcpClientSendMessage(); break;
        case 7: _t->onTcpClientAppendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 8: _t->onRefreshButtonClicked(); break;
        case 9: _t->on_pushButton_up_clicked(); break;
        case 10: _t->on_pushButton_right_clicked(); break;
        case 11: _t->on_pushButton_left_clicked(); break;
        case 12: _t->on_pushButton_down_clicked(); break;
        case 13: _t->on_pushButton_Advertise_clicked(); break;
        case 14: _t->on_Advertise_Manual_Control_clicked(); break;
        case 15: _t->on_Advertise_Setpoint_Data_clicked(); break;
        case 16: _t->on_Advertise_PID_Mode_clicked(); break;
        case 17: _t->on_Advertise_Config_Kx_clicked(); break;
//        case 18: _t->on_button_TcpClient_clicked(); break;
        case 19: _t->on_pb_Send_Manual_Control_clicked(); break;
        case 20: _t->on_pb_Send_PID_Mode_clicked(); break;
        case 21: _t->on_pb_Send_Setpoint_Data_clicked(); break;
        case 22: _t->on_pb_Send_Kx_clicked(); break;
//        case 23: _t->on_button_TcpClientSend_clicked(); break;
        case 24: _t->on_pushButton_Move_Up_clicked(); break;
        case 25: _t->on_pushButton_Move_Down_clicked(); break;
//        case 26: _t->on_pushButton_Move_Up_pressed(); break;
        case 27: _t->on_pushButton_Move_Left_clicked(); break;
        case 28: _t->on_pushButton_Move_Right_clicked(); break;
        case 29: _t->on_pushButton_Stop_clicked(); break;
        case 30: _t->on_Knob_x_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 31: _t->on_Knob_y_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 32: _t->on_Knob_z_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 33: _t->on_Knob_r_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 34: _t->on_pushButton_Reset_x_clicked(); break;
        case 35: _t->on_pushButton_Reset_y_clicked(); break;
        case 36: _t->on_pushButton_Reset_r_clicked(); break;
//        case 37: _t->on_pushButton_clicked(); break;
        case 38: _t->on_pushButton_Auto_depth_clicked(); break;
        case 39: _t->on_Slider_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 40: _t->on_pushButton_Config_All_clicked(); break;
        case 41: _t->on_pushButton_Reset_z_clicked(); break;
        case 42: _t->on_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
//        case 43: _t->on_pushButton_Move_Up_released(); break;
        case 44: _t->user_Timer_Timeout(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 45)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 45;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 45)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 45;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
