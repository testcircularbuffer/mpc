#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "json.h"
#include "ROS_ROV.h"
#include <QKeyEvent>
#include <QDebug>
#include <QMessageBox>
#include <QCloseEvent>
#include <math.h>

using QtJson::JsonObject;

double Kp_Roll, Kp_Pitch, Kp_Yaw, Kp_Depth;
double Ki_Roll, Ki_Pitch, Ki_Yaw, Ki_Depth;
double Kd_Roll, Kd_Pitch, Kd_Yaw, Kd_Depth;
double Setpoint_Roll = 180, Setpoint_Pitch, Setpoint_Yaw, Setpoint_Depth;

double U_M1 = 0.0, U_M2 = 0.0, U_M3 = 0.0, U_M4 = 0.0, U_M5 = 0.0, U_M6 = 0.0, U_M7 = 0.0;

double amplification_coefficient = 40.0;

double AnalogX,AnalogY,AnalogZ,RotZ,Slider;
QString Button1,Button2,Button3,Button4,Button5,Button6,Button7,Button8,Button9,Button10,Button11,
Button12,Button13,Button14;

double val_x = 0.0, val_y = 0.0, val_r = 0.0, val_z = 0.0;

double X_Control = 0.0, Y_Control = 0.0, R_Control = 0.0, Z_Control = 0.0;

double m1_percent = 100, m2_percent = 100, m3_percent = 100, m4_percent = 100, m5_percent = 100,
    m6_percent = 100, m7_percent = 100;

QString Select_Manual = " ";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    restoreWindowState();
    ui->setupUi(this);
    initUI();
    findLocalIPs();
    loadSettings();

    // buttons
    connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientButtonClicked()));
    connect(ui->button_Refresh, SIGNAL(clicked()), this, SLOT(onRefreshButtonClicked()));
}

QtJson::JsonObject ConvertToJson(const QString &message)
{
    bool ok;
    // json is a QString containing the JSON data
    QtJson::JsonObject result = QtJson::parse(message, ok).toMap();
    return(result);
}

/******************************************************************************
 ******************************************************************************
 **
 ** TCP Client
 **
 ******************************************************************************
 ******************************************************************************/

/***********************************
 *
 * TCP client start button clicked
 *
 ***********************************/
void MainWindow::onTcpClientButtonClicked()
{
    disconnect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientButtonClicked()));

    if (setupConnection(TCPCLIENT))
    {
        ui->statusBar->showMessage(messageTCP + "Connecting to " + tcpClientTargetAddr.toString() + ": " + QString::number(tcpClientTargetPort), 0);
        ui->lineEdit_TcpClientTargetIP->setDisabled(true);
        ui->lineEdit_TcpClientTargetPort->setDisabled(true);
        ui->button_TcpClient->setText("Stop");

        connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientStopButtonClicked()));
        connect(mytcpclient, SIGNAL(myClientConnected(QString, quint16)), this, SLOT(onTcpClientNewConnection(QString, quint16)));
        connect(mytcpclient, SIGNAL(connectionFailed()), this, SLOT(onTcpClientTimeOut()));
    }

    saveSettings();
}

/***********************************
 *
 * TCP client has a new connection
 *
 ***********************************/
void MainWindow::onTcpClientNewConnection(const QString &from, quint16 port)
{
    disconnect(mytcpclient, SIGNAL(myClientConnected(QString, quint16)), this, SLOT(onTcpClientNewConnection(QString, quint16)));
    disconnect(mytcpclient, SIGNAL(connectionFailed()), this, SLOT(onTcpClientTimeOut()));
    disconnect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientStopButtonClicked()));
    connect(mytcpclient, SIGNAL(myClientDisconnected()), this, SLOT(onTcpClientDisconnected()));

    ui->button_TcpClient->setDisabled(false);
    ui->button_TcpClient->setText("Disconnect");

    ui->button_TcpClientSend->setDisabled(false);
    ui->lineEdit_TcpClientSend->setDisabled(false);
    ui->textBrowser_TcpClientMessage->setDisabled(false);

    ui->statusBar->showMessage(messageTCP + "Connected to " + from + ": " + QString::number(port), 0);
    connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientDisconnectButtonClicked()));

    connect(mytcpclient, SIGNAL(newMessage(QString, QString)), this, SLOT(onTcpClientAppendMessage(QString, QString)));
    connect(ui->button_TcpClientSend, SIGNAL(clicked()), this, SLOT(onTcpClientSendMessage()));
    connect(ui->lineEdit_TcpClientSend, SIGNAL(returnPressed()), this, SLOT(onTcpClientSendMessage()));

    ui->groupBox_K_PID->setEnabled(true);
    ui->groupBox_Manual_Control->setEnabled(true);
    ui->groupBox_NowState->setEnabled(true);
    ui->groupBox_PID_Mode->setEnabled(true);
    ui->groupBox_Setpoint_Data->setEnabled(true);
    ui->groupBox_Joy_State->setEnabled(true);
    ui->groupBox_Motor_Percent->setEnabled(true);
    ui->groupBox_Turtle->setEnabled(true);
    ui->Knob_z->setEnabled(true);
    ui->pushButton_Start_Manual->setEnabled(true);
    ui->pushButton_Auto_depth->setEnabled(true);
    ui->Knob_Set_Heading->setDisabled(true);
    ui->checkBox_PID_Roll->setChecked(true);
    ui->checkBox_PID_Pitch->setChecked(true);
    ui->lineEdit_setpoint_roll->setText(QString::number(Setpoint_Roll));
    ui->lineEdit_setpoint_pitch->setText(QString::number(Setpoint_Pitch));

    JsonObject sub;
    sub["op"] = "subscribe";
    sub["topic"] = "/IMU_Depth"; //
    sub["type"] = "/beginner_tutorials/IMU_Depth_Data"; //
    mytcpclient->sendMessage(QtJson::serializeStr(sub));

    sub["op"] = "subscribe";
    sub["topic"] = "/Joystick_State"; //
    sub["type"] = "/beginner_tutorials/Joystick_State_Data"; //
    mytcpclient->sendMessage(QtJson::serializeStr(sub));

    JsonObject adv = Json_Make_Advertise_Msg("GUI_Config_Setpoint", "beginner_tutorials/Setpoint_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->lineEdit_TcpClientSend->clear();
    ui->pb_Send_Setpoint_Data->setEnabled(true);

    JsonObject adv1 = Json_Make_Advertise_Msg("GUI_ROV_Conf", "beginner_tutorials/Manual_Control_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv1));
    Add_Message_To_Box("Me", QtJson::serialize(adv1));

    Timer = new QTimer(this);
    connect(Timer, SIGNAL(timeout()), this, SLOT(user_Timer_Timeout()));

}

/***********************************
 *
 * TCP client stop button clicked
 *
 ***********************************/
void MainWindow::onTcpClientStopButtonClicked()
{
    disconnect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientStopButtonClicked()));

    ui->statusBar->showMessage(messageTCP + "Stopped", 2000);
    disconnect(mytcpclient, SIGNAL(myClientConnected(QString, quint16)), this, SLOT(onTcpClientNewConnection(QString, quint16)));
    disconnect(mytcpclient, SIGNAL(connectionFailed()), this, SLOT(onTcpClientTimeOut()));
    ui->button_TcpClient->setText("Connect");
    mytcpclient->abortConnection();
    ui->lineEdit_TcpClientTargetIP->setDisabled(false);
    ui->lineEdit_TcpClientTargetPort->setDisabled(false);

    ui->button_TcpClientSend->setDisabled(true);
    ui->lineEdit_TcpClientSend->setDisabled(true);
    ui->textBrowser_TcpClientMessage->setDisabled(true);

    connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientButtonClicked()));
}

/***********************************
 *
 * TCP client connection time out
 *
 ***********************************/
void MainWindow::onTcpClientTimeOut()
{
    ui->statusBar->showMessage(messageTCP + "Connection time out", 2000);
    disconnect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientStopButtonClicked()));
    disconnect(mytcpclient, SIGNAL(myClientConnected(QString, quint16)), this, SLOT(onTcpClientNewConnection(QString, quint16)));
    disconnect(mytcpclient, SIGNAL(connectionFailed()), this, SLOT(onTcpClientTimeOut()));

    ui->button_TcpClient->setText("Connect");
    ui->lineEdit_TcpClientTargetIP->setDisabled(false);
    ui->lineEdit_TcpClientTargetPort->setDisabled(false);

    mytcpclient->closeClient();
    connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientButtonClicked()));
}

/***********************************
 *
 * TCP client diconnect button clicked
 *
 ***********************************/
void MainWindow::onTcpClientDisconnectButtonClicked()
{
    mytcpclient->disconnectCurrentConnection();
}

/***********************************
 *
 * TCP client disconnected
 *
 ***********************************/
void MainWindow::onTcpClientDisconnected()
{
    ui->statusBar->showMessage(messageTCP + "Disconnected from server", 2000);
    disconnect(mytcpclient, SIGNAL(myClientDisconnected()), this, SLOT(onTcpClientDisconnected()));
    disconnect(mytcpclient, SIGNAL(newMessage(QString, QString)), this, SLOT(onTcpClientAppendMessage(QString, QString)));
    disconnect(ui->button_TcpClientSend, SIGNAL(clicked()), this, SLOT(onTcpClientSendMessage()));
    disconnect(ui->lineEdit_TcpClientSend, SIGNAL(returnPressed()), this, SLOT(onTcpClientSendMessage()));
    disconnect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientDisconnectButtonClicked()));
    ui->button_TcpClient->setText("Connect");

    Timer->stop();

    ui->button_TcpClientSend->setDisabled(true);
    ui->lineEdit_TcpClientSend->setDisabled(true);
    ui->textBrowser_TcpClientMessage->setDisabled(true);

    ui->button_TcpClient->setDisabled(false);
    ui->lineEdit_TcpClientTargetIP->setDisabled(false);
    ui->lineEdit_TcpClientTargetPort->setDisabled(false);

    mytcpclient->closeClient();
    mytcpclient->close();

    ui->groupBox_K_PID->setDisabled(true);
    ui->groupBox_Manual_Control->setDisabled(true);
    ui->groupBox_NowState->setDisabled(true);
    ui->groupBox_PID_Mode->setDisabled(true);
    ui->groupBox_Setpoint_Data->setDisabled(true);
    ui->groupBox_Control_1->setDisabled(true);
    ui->groupBox_Joy_State->setDisabled(true);
    ui->groupBox_Motor_Percent->setDisabled(true);
    ui->groupBox_Turtle->setDisabled(true);
    ui->pb_Send_Kx->setDisabled(true);
    ui->pb_Send_Manual_Control->setDisabled(true);
    ui->pb_Send_PID_Mode->setDisabled(true);
    ui->pb_Send_Setpoint_Data->setDisabled(true);
    ui->Slider->setDisabled(true);
    ui->checkBox_PID_Yaw->setChecked(false);
    ui->pushButton_Config_All->setDisabled(true);
    ui->groupBox_State->setDisabled(true);
    ui->Knob_z->setDisabled(true);
    ui->pushButton_Start_Manual->setDisabled(true);
    ui->pushButton_Auto_depth->setDisabled(true);

    connect(ui->button_TcpClient, SIGNAL(clicked()), this, SLOT(onTcpClientButtonClicked()));
}

/***********************************
 *
 * Add message to box
 *
 ***********************************/
void MainWindow::Add_Message_To_Box(const QString &from, const QString &message)
{
    QTextCursor cursor(ui->textBrowser_TcpClientMessage->textCursor());
    cursor.movePosition(QTextCursor::End);
    if (from == "System")
    {
        QColor color = ui->textBrowser_TcpClientMessage->textColor();
        ui->textBrowser_TcpClientMessage->setTextColor(Qt::red);
        ui->textBrowser_TcpClientMessage->append(message);
        ui->textBrowser_TcpClientMessage->setTextColor(color);
    }
    else
    {
        QTextTable *table = cursor.insertTable(1, 2, tableFormat);
        table->cellAt(0, 0).firstCursorPosition().insertText('<' + from + "> ");
        table->cellAt(0, 1).firstCursorPosition().insertText(message);
    }
    QScrollBar *bar = ui->textBrowser_TcpClientMessage->verticalScrollBar();
    bar->setValue(bar->maximum());
}

/***********************************
 *
 * TCP client append a message
 *
 ***********************************/
void MainWindow::onTcpClientAppendMessage(const QString &from, const QString &message)
{
    if (from.isEmpty() || message.isEmpty())
    {
        return;
    }

//    QTextCursor cursor(ui->textBrowser_TcpClientMessage->textCursor());
//    cursor.movePosition(QTextCursor::End);

//    if (from == "System")
//    {
//        QColor color = ui->textBrowser_TcpClientMessage->textColor();
//        ui->textBrowser_TcpClientMessage->setTextColor(Qt::gray);
//        ui->textBrowser_TcpClientMessage->append(message);
//        ui->textBrowser_TcpClientMessage->setTextColor(color);
//    }
//    else
//    {
//        QTextTable *table = cursor.insertTable(1, 2, tableFormat);
//        table->cellAt(0, 0).firstCursorPosition().insertText('<' + from + "> ");
//        table->cellAt(0, 1).firstCursorPosition().insertText(message);
//    }
    //    QScrollBar *bar = ui->textBrowser_TcpClientMessage->verticalScrollBar();
    //    bar->setValue(bar->maximum());

    bool ok;
    QtJson::JsonObject result = QtJson::parse(message, ok).toMap();
    QString msg_string = QtJson::serialize(result["msg"]);
    QString topic_string = QtJson::serialize(result["topic"]);
    JsonObject msg = ConvertToJson(msg_string);

    if(topic_string == "\"/IMU_Depth\"")
    {
//        double Roll_Value = (QtJson::serialize(msg["Roll"])).toDouble();
//        double Pitch_Value = (QtJson::serialize(msg["Pitch"])).toDouble();
        double Yaw_Value = (QtJson::serialize(msg["Yaw"])).toDouble();
        double Depth_Value = (QtJson::serialize(msg["Depth"])).toDouble();

        ui->Thermo->setValue(Depth_Value);
        ui->my_Roll->setText(QtJson::serialize(msg["Roll"]));
        ui->my_Pitch->setText(QtJson::serialize(msg["Pitch"]));
        ui->my_Yaw->setText(QtJson::serialize(msg["Yaw"]));
        ui->Knob_Now_Heading->setValue(Yaw_Value);
        ui->my_Depth->setText(QtJson::serialize(msg["Depth"]));
    }

    if(topic_string == "\"/Joystick_State\"")
    {
        AnalogY = (QtJson::serialize(msg["AnalogX"])).toDouble();
        AnalogX = (QtJson::serialize(msg["AnalogY"])).toDouble();
        AnalogX = -AnalogX;
        AnalogZ = (QtJson::serialize(msg["AnalogZ"])).toDouble();
        RotZ = (QtJson::serialize(msg["RotZ"])).toDouble();
        Slider = (QtJson::serialize(msg["Slider"])).toDouble();
//        Button1 = (QtJson::serialize(msg["Button1"]));
//        Button2 = (QtJson::serialize(msg["Button2"]));
//        Button3 = (QtJson::serialize(msg["Button3"]));
//        Button4 = (QtJson::serialize(msg["Button4"]));
//        Button5 = (QtJson::serialize(msg["Button5"]));
//        Button6 = (QtJson::serialize(msg["Button6"]));
//        Button7 = (QtJson::serialize(msg["Button7"]));
//        Button8 = (QtJson::serialize(msg["Button8"]));
//        Button9 = (QtJson::serialize(msg["Button9"]));
//        Button10 = (QtJson::serialize(msg["Button10"]));
//        Button11 = (QtJson::serialize(msg["Button11"]));
//        Button12 = (QtJson::serialize(msg["Button12"]));
//        Button13 = (QtJson::serialize(msg["Button13"]));
//        Button14 = (QtJson::serialize(msg["Button14"]));

//        if (Button1 == "true")
//        {}

        ui->lineEdit_Joy_Y->setText(QtJson::serialize(msg["AnalogX"]));
        ui->lineEdit_Joy_X->setText(QtJson::serialize(msg["AnalogY"]));
        ui->lineEdit_Joy_R->setText(QtJson::serialize(msg["RotZ"]));
        ui->lineEdit_Joy_Z->setText(QtJson::serialize(msg["AnalogZ"]));
        ui->lineEdit_Joy_Slider->setText(QtJson::serialize(msg["Slider"]));
    }
}

/***********************************
 *
 * Send message through TCP client
 *
 ***********************************/
void MainWindow::onTcpClientSendMessage()
{
    QString topic = ui->comboBox_topic->currentText();
    //    if (topic.isEmpty())
    //    {
    //        return;
    //    }
    QString type;
    if(topic == "/turtle1/cmd_vel") type = "/geometry_msgs/Twist";
    else if (topic == "/image_raw") type = "/sensor_msgs/Image";
    else if (topic == "/IMU_Depth") type = "/beginner_tutorials/IMU_Depth_Data";

    JsonObject huy;
    huy["op"] = "subscribe";
    huy["topic"] = topic; //         /turtle1/cmd_vel       /image_raw
    huy["type"] = type; //           /geometry_msgs/Twist   /sensor_msgs/Image

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

/***********************************
 *
 * UI initialization
 *
 ***********************************/
void MainWindow::initUI()
{
    QString rule = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    ui->lineEdit_TcpClientTargetIP->setValidator(new QRegExpValidator(QRegExp("^" + rule + "\\." + rule + "\\." + rule + "\\." + rule + "$"), this));
    ui->lineEdit_TcpClientTargetPort->setValidator(new QIntValidator(0, 65535, this));

    ui->textBrowser_TcpClientMessage->setFocusPolicy(Qt::NoFocus);

    ui->lineEdit_TcpClientSend->setFocusPolicy(Qt::StrongFocus);
    ui->lineEdit_TcpClientSend->setFocus();

    ui->button_TcpClientSend->setDisabled(true);
    ui->lineEdit_TcpClientSend->setDisabled(true);
    ui->textBrowser_TcpClientMessage->setDisabled(true);
    ui->pushButton_Light->setDisabled(true);

    tableFormat.setBorder(0);

//    ui->label_HomePage->setText("<a href=\"https://zpeng.me/\">My home page</a>");
//    ui->label_HomePage->setTextFormat(Qt::RichText);
//    ui->label_HomePage->setTextInteractionFlags(Qt::TextBrowserInteraction);
//    ui->label_HomePage->setOpenExternalLinks(true);
}

/***********************************
 *
 * Setup connections
 *
 ***********************************/
bool MainWindow::setupConnection(quint8 type)
{
    bool isSuccess = false;
    localAddr.setAddress(ui->label_LocalIP->text());

    switch (type)
    {
    case TCPCLIENT:
        isSuccess = true;
        tcpClientTargetAddr.setAddress(ui->lineEdit_TcpClientTargetIP->text());
        tcpClientTargetPort = ui->lineEdit_TcpClientTargetPort->text().toInt();
        if (mytcpclient == nullptr)
        {
            mytcpclient = new MyTCPClient;
        }
        mytcpclient->connectTo(tcpClientTargetAddr, tcpClientTargetPort);
        break;
    }
    return isSuccess;
}

/***********************************
 *
 * Find IP of local WiFi connections
 *
 ***********************************/
void MainWindow::findLocalIPs()
{
    ui->comboBox_Interface->clear();
    interfaceList.clear();
    QList<QNetworkInterface> listInterface = QNetworkInterface::allInterfaces();
    for (int i = 0; i < listInterface.size(); ++i)
    {
        //qDebug()<<listInterface.at(i).name();
        if (listInterface.at(i).humanReadableName().contains("Wi-Fi") || listInterface.at(i).humanReadableName().contains("wlp"))
        {
            interfaceList.append(listInterface.at(i));
        }
    }

    if (interfaceList.isEmpty())
    {
        // TODO wifilist is empty
    }
    else
    {
        for (int j = 0; j < interfaceList.size(); ++j)
        {
            ui->comboBox_Interface->addItem(interfaceList.at(j).humanReadableName());
        }
    }
}

/***********************************
 *
 * Load settings from local configuration file
 *
 ***********************************/
void MainWindow::loadSettings()
{
    settingsFileDir = QApplication::applicationDirPath() + "/config.ini";
    QSettings settings(settingsFileDir, QSettings::IniFormat);
    ui->lineEdit_TcpClientTargetIP->setText(settings.value("TCP_CLIENT_TARGET_IP", "127.0.0.1").toString());
    ui->lineEdit_TcpClientTargetPort->setText(settings.value("TCP_CLIENT_TARGET_PORT", 1234).toString());

    int index = settings.value("interfaceIndex", 0).toInt();
    if (ui->comboBox_Interface->count() >= index)
    {
        ui->comboBox_Interface->setCurrentIndex(index);
        for (int i = 0; i < interfaceList.at(index).addressEntries().size(); ++i)
        {
            if (interfaceList.at(index).addressEntries().at(i).ip().protocol() == QAbstractSocket::IPv4Protocol)
            {
                ui->label_LocalIP->setText(interfaceList.at(index).addressEntries().at(i).ip().toString());
            }
        }
    }
    else if (ui->comboBox_Interface->count() > 0 && ui->comboBox_Interface->count() < index)
    {
        ui->comboBox_Interface->setCurrentIndex(0);
        for (int i = 0; i < interfaceList.at(0).addressEntries().size(); ++i)
        {
            if (interfaceList.at(0).addressEntries().at(i).ip().protocol() == QAbstractSocket::IPv4Protocol)
            {
                ui->label_LocalIP->setText(interfaceList.at(0).addressEntries().at(i).ip().toString());
            }
        }
    }

    Kp_Roll = settings.value("Kp_Roll", "4.0").toDouble();
    Kp_Pitch = settings.value("Kp_Pitch", "4.0").toDouble();
    Kp_Yaw = settings.value("Kp_Yaw", "4.0").toDouble();
    Kp_Depth = settings.value("Kp_Depth", "4.0").toDouble();
    Ki_Roll = settings.value("Ki_Roll", "0.05").toDouble();
    Ki_Pitch = settings.value("Ki_Pitch", "0.05").toDouble();
    Ki_Yaw = settings.value("Ki_Yaw", "0.05").toDouble();
    Ki_Depth = settings.value("Ki_Depth", "0.05").toDouble();
    Kd_Roll = settings.value("Kd_Roll", "0.0").toDouble();
    Kd_Pitch = settings.value("Kd_Pitch", "0.0").toDouble();
    Kd_Yaw = settings.value("Kd_Yaw", "0.0").toDouble();
    Kd_Depth = settings.value("Kd_Depth", "0.0").toDouble();

    m1_percent = settings.value("m1_percent", "100").toInt();
    m2_percent = settings.value("m2_percent", "100").toInt();
    m3_percent = settings.value("m3_percent", "100").toInt();
    m4_percent = settings.value("m4_percent", "100").toInt();
    m5_percent = settings.value("m5_percent", "100").toInt();
    m6_percent = settings.value("m6_percent", "100").toInt();
    m7_percent = settings.value("m7_percent", "100").toInt();

    ui->lineEdit_Kp_Roll->setText(settings.value("Kp_Roll", "4.0").toString());
    ui->lineEdit_Ki_Roll->setText(settings.value("Ki_Roll", "0.05").toString());
    ui->lineEdit_Kd_Roll->setText(settings.value("Kd_Roll", "0.0").toString());

    ui->lineEdit_Kp_pitch->setText(settings.value("Kp_Pitch", "4.0").toString());
    ui->lineEdit_Ki_Pitch->setText(settings.value("Ki_Pitch", "0.05").toString());
    ui->lineEdit_Kd_pitch->setText(settings.value("Kd_Pitch", "0.0").toString());

    ui->lineEdit_Kp_Yaw->setText(settings.value("Kp_Yaw", "4.0").toString());
    ui->lineEdit_Ki_Yaw->setText(settings.value("Ki_Yaw", "0.05").toString());
    ui->lineEdit_kd_yaw->setText(settings.value("Kd_Yaw", "0.0").toString());

    ui->lineEdit_Kp_Depth->setText(settings.value("Kp_Depth", "4.0").toString());
    ui->lineEdit_Ki_Depth->setText(settings.value("Ki_Depth", "0.05").toString());
    ui->lineEdit_kd_depth->setText(settings.value("Kd_Depth", "0.0").toString());

    ui->M1_percent->setValue(m1_percent);
    ui->M2_percent->setValue(m2_percent);
    ui->M3_percent->setValue(m3_percent);
    ui->M4_percent->setValue(m4_percent);
    ui->M5_percent->setValue(m5_percent);
    ui->M6_percent->setValue(m6_percent);
    ui->M7_percent->setValue(m7_percent);
}

/***********************************
 *
 * Save settings to local configuration file
 *
 ***********************************/
void MainWindow::saveSettings()
{
    QSettings settings(settingsFileDir, QSettings::IniFormat);
    settings.setValue("TCP_CLIENT_TARGET_IP", ui->lineEdit_TcpClientTargetIP->text());
    settings.setValue("TCP_CLIENT_TARGET_PORT", ui->lineEdit_TcpClientTargetPort->text());
    settings.setValue("INTERFACE_INDEX", ui->comboBox_Interface->currentIndex());

    Kp_Roll = ui->lineEdit_Kp_Roll->text().toDouble();
    Kp_Pitch = ui->lineEdit_Kp_pitch->text().toDouble();
    Kp_Yaw = ui->lineEdit_Kp_Yaw->text().toDouble();
    Kp_Depth = ui->lineEdit_Kp_Depth->text().toDouble();
    Ki_Roll = ui->lineEdit_Ki_Roll->text().toDouble();
    Ki_Pitch = ui->lineEdit_Ki_Pitch->text().toDouble();
    Ki_Yaw = ui->lineEdit_Ki_Yaw->text().toDouble();
    Ki_Depth = ui->lineEdit_Ki_Depth->text().toDouble();
    Kd_Roll = ui->lineEdit_Kd_Roll->text().toDouble();
    Kd_Pitch = ui->lineEdit_Kd_pitch->text().toDouble();
    Kd_Yaw = ui->lineEdit_kd_yaw->text().toDouble();
    Kd_Depth = ui->lineEdit_kd_depth->text().toDouble();

    settings.setValue("Kp_Roll", Kp_Roll);
    settings.setValue("Kp_Pitch", Kp_Pitch);
    settings.setValue("Kp_Yaw", Kp_Yaw);
    settings.setValue("Kp_Depth", Kp_Depth);
    settings.setValue("Ki_Roll", Ki_Roll);
    settings.setValue("Ki_Pitch", Ki_Pitch);
    settings.setValue("Ki_Yaw", Ki_Yaw);
    settings.setValue("Ki_Depth", Ki_Depth);
    settings.setValue("Kd_Roll", Kd_Roll);
    settings.setValue("Kd_Pitch", Kd_Pitch);
    settings.setValue("Kd_Yaw", Kd_Yaw);
    settings.setValue("Kd_Depth", Kd_Depth);

    settings.setValue("m1_percent", m1_percent);
    settings.setValue("m2_percent", m2_percent);
    settings.setValue("m3_percent", m3_percent);
    settings.setValue("m4_percent", m4_percent);
    settings.setValue("m5_percent", m5_percent);
    settings.setValue("m6_percent", m6_percent);
    settings.setValue("m7_percent", m7_percent);

    settings.sync();
}

void MainWindow::onRefreshButtonClicked()
{
    saveSettings();
    findLocalIPs();
    loadSettings();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
//    QSettings settings("ZPeng", "SocketTest");
//    settings.setValue("geometry", saveGeometry());
//    settings.setValue("windowState", saveState());
    if(QMessageBox::question(this,"Question?","Save setting before quit ???") == QMessageBox::Yes)
        {
            saveSettings();
            this->close();
            QMainWindow::closeEvent(event);
        }
}

void MainWindow::restoreWindowState()
{
//    QSettings settings("ZPeng", "SocketTest");
//    restoreGeometry(settings.value("geometry").toByteArray());
    //    restoreState(settings.value("windowState").toByteArray());
}

void MainWindow::user_Key_Press_Event(QKeyEvent *Key_event)
{
//    if(Key_event->key() == Qt::Key_8)
//    {
//        ui->pushButton_Move_Up->setEnabled(true);
//        ui->pushButton_Move_Down->setDisabled(true);
//        ui->pushButton_Move_Left->setDisabled(true);
//        ui->pushButton_Move_Right->setDisabled(true);
//        qDebug() << "Key W: " ;
//        Key_event->accept();
//    }
//    else if(Key_event->key() == Qt::Key_2)
//    {
//        ui->pushButton_Move_Down->setEnabled(true);
//        ui->pushButton_Move_Up->setDisabled(true);
//        ui->pushButton_Move_Left->setDisabled(true);
//        ui->pushButton_Move_Right->setDisabled(true);
//        Key_event->accept();
//    }
//    else if(Key_event->key() == Qt::Key_4)
//    {
//        ui->pushButton_Move_Left->setEnabled(true);
//        ui->pushButton_Move_Up->setDisabled(true);
//        ui->pushButton_Move_Down->setDisabled(true);
//        ui->pushButton_Move_Right->setDisabled(true);
//        Key_event->accept();
//    }
//    else if(Key_event->key() == Qt::Key_6)
//    {
//        ui->pushButton_Move_Right->setEnabled(true);
//        ui->pushButton_Move_Up->setDisabled(true);
//        ui->pushButton_Move_Left->setDisabled(true);
//        ui->pushButton_Move_Down->setDisabled(true);
//        Key_event->accept();
//    }
    qDebug() << "Key W: " ;
    Key_event->accept();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/***********************************
 *
 * Control turtle
 *
 ***********************************/
void MainWindow::on_pushButton_up_clicked()
{
    JsonObject huy;
    JsonObject msg;
    JsonObject linear;
    JsonObject angular;
    linear["x"] = 2.0;
    linear["y"] = 0.0;
    linear["z"] = 0.0;
    angular["x"] = 0.0;
    angular["y"] = 0.0;
    angular["z"] = 0.0;
    msg["linear"] = linear;
    msg["angular"] = angular;
    huy["op"] = "publish";
    huy["topic"] = "/turtle1/cmd_vel";
    huy["msg"] = msg;

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_pushButton_right_clicked()
{
    JsonObject huy;
    JsonObject msg;
    JsonObject linear;
    JsonObject angular;
    linear["x"] = 0.0;
    linear["y"] = 0.0;
    linear["z"] = 0.0;
    angular["x"] = 0.0;
    angular["y"] = 0.0;
    angular["z"] = -2.0;
    msg["linear"] = linear;
    msg["angular"] = angular;
    huy["op"] = "publish";
    huy["topic"] = "/turtle1/cmd_vel";
    huy["msg"] = msg;

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_pushButton_left_clicked()
{
    JsonObject huy;
    JsonObject msg;
    JsonObject linear;
    JsonObject angular;
    linear["x"] = 0.0;
    linear["y"] = 0.0;
    linear["z"] = 0.0;
    angular["x"] = 0.0;
    angular["y"] = 0.0;
    angular["z"] = 2.0;
    msg["linear"] = linear;
    msg["angular"] = angular;
    huy["op"] = "publish";
    huy["topic"] = "/turtle1/cmd_vel";
    huy["msg"] = msg;

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_pushButton_down_clicked()
{
    JsonObject huy;
    JsonObject msg;
    JsonObject linear;
    JsonObject angular;
    linear["x"] = -2.0;
    linear["y"] = 0.0;
    linear["z"] = 0.0;
    angular["x"] = 0.0;
    angular["y"] = 0.0;
    angular["z"] = 0.0;
    msg["linear"] = linear;
    msg["angular"] = angular;
    huy["op"] = "publish";
    huy["topic"] = "/turtle1/cmd_vel";
    huy["msg"] = msg;

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_pushButton_Advertise_clicked()
{
    JsonObject huy;
    huy["op"] = "advertise";
    huy["topic"] = "/turtle1/cmd_vel";
    huy["type"] = "geometry_msgs/Twist";

    mytcpclient->sendMessage(QtJson::serializeStr(huy)); // hoặc serialize đều được

    Add_Message_To_Box("Me", QtJson::serialize(huy));
    ui->lineEdit_TcpClientSend->clear();
}

/***********************************
 *
 * TEST CONTROL ROV
 *
 ***********************************/

void MainWindow::Send_X_Control(void)
{
    if(val_x > 100) val_x = 100.0;
    if(val_x < -100) val_x = -100.0;

    if(Select_Manual == "GUI")
    {
        X_Control = val_x * amplification_coefficient / 100;
    }
    else if(Select_Manual == "Joystick")
    {
        X_Control = AnalogX * amplification_coefficient / 100;
    }
    else if(Select_Manual == " ")
    {
         X_Control = 0;
         return;
    }

    JsonObject Manual_Control_Data;
    if(X_Control >= 0)
    {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_ForWard, X_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
    }
    else if (X_Control <0)
    {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_BackWard, -X_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
    }
}

void MainWindow::Send_Y_Control(void)
{
    if(val_y > 100) val_y = 100.0;
    if(val_y < -100) val_y = -100.0;

    if(Select_Manual == "GUI")
    {
        Y_Control = val_y * amplification_coefficient / 100;
    }
    else if(Select_Manual == "Joystick")
    {
        Y_Control = AnalogY * amplification_coefficient / 100;
    }
    else if(Select_Manual == " ")
         Y_Control = 0;

    JsonObject Manual_Control_Data;
   if(Y_Control >= 0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_HorizontialRight, Y_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
   else if (Y_Control <0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_HorizontialLeft, -Y_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
}
void MainWindow::Send_R_Control(void)
{
    if(val_r > 100) val_r = 100.0;
    if(val_r < -100) val_r = -100.0;

    if(Select_Manual == "GUI")
    {
        R_Control = val_r * amplification_coefficient / 100;
    }
    else if(Select_Manual == "Joystick")
    {
        R_Control = RotZ * amplification_coefficient / 100;
    }
    else if(Select_Manual == " ")
         R_Control = 0;

    JsonObject Manual_Control_Data;
   if(R_Control >= 0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_TurnRight, R_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
   else if (R_Control <0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_TurnLeft, -R_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
}

void MainWindow::Send_Z_Control(void)
{
    if(val_z > 100) val_z = 100.0;
    if(val_z < -100) val_z = -100.0;

    if(Select_Manual == "GUI")
    {
        Z_Control = val_z * amplification_coefficient / 100;
    }
    else if(Select_Manual == "Joystick")
    {
        Z_Control = AnalogZ * amplification_coefficient / 100;
    }
    else if(Select_Manual == " ")
         Z_Control = 0;

    JsonObject Manual_Control_Data;
   if(Z_Control >= 0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_Dive, Z_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
   else if (Z_Control <0)
   {
       Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_Rise, -Z_Control);
       mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
       Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
       ui->lineEdit_TcpClientSend->clear();
   }
}

void MainWindow::on_Advertise_Manual_Control_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_Manual_Control", "beginner_tutorials/Manual_Control_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->lineEdit_TcpClientSend->clear();
    ui->pb_Send_Manual_Control->setEnabled(true);
}

void MainWindow::on_Advertise_Setpoint_Data_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_Config_Setpoint", "beginner_tutorials/Setpoint_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->lineEdit_TcpClientSend->clear();
    ui->pb_Send_Setpoint_Data->setEnabled(true);
}

void MainWindow::on_Advertise_PID_Mode_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_PID_Mode", "std_msgs/String");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->lineEdit_TcpClientSend->clear();
    ui->pb_Send_PID_Mode->setEnabled(true);
}

void MainWindow::on_Advertise_Config_Kx_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_Config_K_PID", "beginner_tutorials/K_PID_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->lineEdit_TcpClientSend->clear();
    ui->pb_Send_Kx->setEnabled(true);
    ui->pushButton_Config_All->setEnabled(true);
}

void MainWindow::on_pb_Send_Manual_Control_clicked()
{
    //JsonObject Manual_Control_Data = Json_Make_Manual_Control_Data(Manual_Mode_ForWard, 50.0);
    JsonObject Manual_Control_Data = Json_Make_Manual_Control_Data(ui->cbb_Manual_Mode->currentText(), float(ui->dbs_Duty_Value->value()));
    mytcpclient->sendMessage(QtJson::serializeStr(Manual_Control_Data));
    Add_Message_To_Box("Me", QtJson::serialize(Manual_Control_Data));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_pb_Send_PID_Mode_clicked()
{
    JsonObject PID_Mode = Json_Make_PID_Mode_Data(ui->cbb_PID_Mode_State_Select->currentText());
    mytcpclient->sendMessage(QtJson::serializeStr(PID_Mode));
    Add_Message_To_Box("Me", QtJson::serialize(PID_Mode));
    ui->lineEdit_TcpClientSend->clear();

    if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Roll_ENA")
    {
        ui->checkBox_PID_Roll->setChecked(true);
        ui->lineEdit_Kp_Roll->setText(QString::number(Kp_Roll));
        ui->lineEdit_Ki_Roll->setText(QString::number(Ki_Roll));
        ui->lineEdit_Kd_Roll->setText(QString::number(Kd_Roll));
        ui->lineEdit_setpoint_roll->setText(QString::number(Setpoint_Roll));
    }
    else if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Roll_DIS")
        ui->checkBox_PID_Roll->setChecked(false);

    if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Pitch_ENA")
    {
        ui->checkBox_PID_Pitch->setChecked(true);
        ui->lineEdit_Kp_pitch->setText(QString::number(Kp_Pitch));
        ui->lineEdit_Ki_Pitch->setText(QString::number(Ki_Pitch));
        ui->lineEdit_Kd_pitch->setText(QString::number(Kd_Pitch));
        ui->lineEdit_setpoint_pitch->setText(QString::number(Setpoint_Pitch));
    }
    else if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Pitch_DIS")
        ui->checkBox_PID_Pitch->setChecked(false);

    if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Yaw_ENA")
    {
        ui->checkBox_PID_Yaw->setChecked(true);
        ui->lineEdit_Kp_Yaw->setText(QString::number(Kp_Yaw));
        ui->lineEdit_Ki_Yaw->setText(QString::number(Ki_Yaw));
        ui->lineEdit_kd_yaw->setText(QString::number(Kd_Yaw));
        ui->lineEdit_setpoint_yaw->setText(QString::number(Setpoint_Yaw));
    }
    else if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Yaw_DIS")
        ui->checkBox_PID_Yaw->setChecked(false);

    if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Depth_ENA")
    {
        ui->checkBox_PID_Depth->setChecked(true);
        ui->lineEdit_Kp_Depth->setText(QString::number(Kp_Depth));
        ui->lineEdit_Ki_Depth->setText(QString::number(Ki_Depth));
        ui->lineEdit_kd_depth->setText(QString::number(Kd_Depth));
        ui->lineEdit_setpoint_depth->setText(QString::number(Setpoint_Depth));
        ui->Slider->setEnabled(true);
    }
    else if(ui->cbb_PID_Mode_State_Select->currentText() == "PID_Mode_Depth_DIS")
    {
        ui->checkBox_PID_Depth->setChecked(false);
        ui->Slider->setDisabled(true);
    }

}

void MainWindow::on_pb_Send_Setpoint_Data_clicked()
{
    JsonObject Setpoint_Data = Json_Make_Setpoint_Data(ui->cbb_Select_PID->currentText(),ui->dbs_Setpoint_Value->value());
    mytcpclient->sendMessage(QtJson::serializeStr(Setpoint_Data));
    Add_Message_To_Box("Me", QtJson::serialize(Setpoint_Data));
    ui->lineEdit_TcpClientSend->clear();

    if(ui->cbb_Select_PID->currentText() == "Select_PID_Roll")
    {
        ui->lineEdit_setpoint_roll->setText(QString::number(ui->dbs_Setpoint_Value->value()));
        Setpoint_Roll = ui->dbs_Setpoint_Value->value();
    }
    else if(ui->cbb_Select_PID->currentText() == "Select_PID_Pitch")
    {
        ui->lineEdit_setpoint_pitch->setText(QString::number(ui->dbs_Setpoint_Value->value()));
        Setpoint_Pitch = ui->dbs_Setpoint_Value->value();
    }
    else if(ui->cbb_Select_PID->currentText() == "Select_PID_Yaw")
    {
        ui->lineEdit_setpoint_yaw->setText(QString::number(ui->dbs_Setpoint_Value->value()));
        Setpoint_Yaw = ui->dbs_Setpoint_Value->value();
    }
    else if(ui->cbb_Select_PID->currentText() == "Select_PID_Depth")
    {
        ui->lineEdit_setpoint_depth->setText(QString::number(ui->dbs_Setpoint_Value->value()));
        Setpoint_Depth = ui->dbs_Setpoint_Value->value();
        ui->Slider->setValue(ui->dbs_Setpoint_Value->value());
    }
}

void MainWindow::on_pb_Send_Kx_clicked()
{
    if(ui->le_Kx_PID->text() == "")
    {
        QMessageBox::information(this,"Information!!!","Enter your configuration.");
        return;
    }
    QString Select_Kx;
    if (ui->cbb_Select_Kx->currentText() == "Kp")
    {
        Select_Kx = "Config_Kp";
        if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Roll")
        {
            ui->lineEdit_Kp_Roll->setText(ui->le_Kx_PID->text());
            Kp_Roll = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Pitch")
        {
            ui->lineEdit_Kp_pitch->setText(ui->le_Kx_PID->text());
            Kp_Pitch = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Yaw")
        {
            ui->lineEdit_Kp_Yaw->setText(ui->le_Kx_PID->text());
            Kp_Yaw = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Depth")
        {
            ui->lineEdit_Kp_Depth->setText(ui->le_Kx_PID->text());
            Kp_Depth = ui->le_Kx_PID->text().toDouble();
        }
    }
    else if (ui->cbb_Select_Kx->currentText() == "Ki")
    {
        Select_Kx = "Config_Ki";
        if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Roll")
        {
            ui->lineEdit_Ki_Roll->setText(ui->le_Kx_PID->text());
            Ki_Roll = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Pitch")
        {
            ui->lineEdit_Ki_Pitch->setText(ui->le_Kx_PID->text());
            Ki_Pitch = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Yaw")
        {
            ui->lineEdit_Ki_Yaw->setText(ui->le_Kx_PID->text());
            Ki_Yaw = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Depth")
        {
            ui->lineEdit_Ki_Depth->setText(ui->le_Kx_PID->text());
            Ki_Depth = ui->le_Kx_PID->text().toDouble();
        }
    }
    else if (ui->cbb_Select_Kx->currentText() == "Kd")
    {
        Select_Kx = "Config_Kd";
        if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Roll")
        {
            ui->lineEdit_Kd_Roll->setText(ui->le_Kx_PID->text());
            Kd_Roll = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Pitch")
        {
            ui->lineEdit_Kd_pitch->setText(ui->le_Kx_PID->text());
            Kd_Pitch = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Yaw")
        {
            ui->lineEdit_kd_yaw->setText(ui->le_Kx_PID->text());
            Kd_Yaw = ui->le_Kx_PID->text().toDouble();
        }
        else if(ui->cbb_Select_PID_2->currentText() == "Select_PID_Depth")
        {
            ui->lineEdit_kd_depth->setText(ui->le_Kx_PID->text());
            Kd_Depth = ui->le_Kx_PID->text().toDouble();
        }
    }
    JsonObject Config_Kx = Json_Make_K_PID_Data(ui->cbb_Select_PID_2->currentText(),
                                                Select_Kx, ui->le_Kx_PID->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Add_Message_To_Box("Me", QtJson::serialize(Config_Kx));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::user_Timer_Timeout()
{
    double analog,_analogX,_analogY,_analogR;
    _analogX = abs(AnalogX);
    _analogY = abs(AnalogY);
    _analogR = abs(RotZ);

    if(_analogX >= _analogY)
        analog = _analogX;
    else
        analog = _analogY;

    if(_analogR >= analog)
        analog = _analogR;

    if(analog == _analogX)
        Send_X_Control();
    else if(analog == _analogY)
        Send_Y_Control();
    else if(analog == _analogR)
        Send_R_Control();

    Send_Z_Control();
}

void MainWindow::on_pushButton_Move_Up_clicked()
{
    val_x = 100.0;
    ui->Knob_x->setValue(val_x);
    ui->Knob_y->setValue(0.0);
    ui->Knob_r->setValue(0.0);
    Send_X_Control();
}

void MainWindow::on_pushButton_Move_Down_clicked()
{
    val_x = -100.0;
    ui->Knob_x->setValue(val_x);
    ui->Knob_y->setValue(0.0);
    ui->Knob_r->setValue(0.0);
    Send_X_Control();
}


void MainWindow::on_pushButton_Move_Left_clicked()
{
    val_y = -100.0;;
    ui->Knob_y->setValue(val_y);
    ui->Knob_x->setValue(0.0);
    ui->Knob_r->setValue(0.0);
    Send_Y_Control();
}

void MainWindow::on_pushButton_Move_Right_clicked()
{
    val_y = +100.0;
    ui->Knob_x->setValue(0.0);
    ui->Knob_r->setValue(0.0);
    ui->Knob_y->setValue(val_y);
    Send_Y_Control();
}

void MainWindow::on_pushButton_Stop_clicked()
{
    val_x = val_y = val_r = val_z = 0.0;
    ui->Knob_x->setValue(0.0);
    ui->Knob_y->setValue(0.0);
    ui->Knob_r->setValue(0.0);
    ui->Knob_z->setValue(0.0);
    Send_X_Control();
    Send_Z_Control();
}

void MainWindow::on_Knob_x_sliderMoved(double value)
{
    ui->Knob_x->setValue(value);
    val_x = value;
    Send_X_Control();
    val_y = val_r = 0.0;
    ui->Knob_y->setValue(0.0);
    ui->Knob_r->setValue(0.0);
}

void MainWindow::on_Knob_y_sliderMoved(double value)
{
    ui->Knob_y->setValue(value);
    val_y = value;
    Send_Y_Control();
    val_x = val_r = 0.0;
    ui->Knob_x->setValue(0.0);
    ui->Knob_r->setValue(0.0);
}

void MainWindow::on_Knob_r_sliderMoved(double value)
{
    ui->Knob_r->setValue(value);
    val_r = value;
    Send_R_Control();
    val_y = val_x = 0.0;
    ui->Knob_y->setValue(0.0);
    ui->Knob_x->setValue(0.0);
}

void MainWindow::on_Knob_z_sliderMoved(double value)
{
    ui->Knob_z->setValue(value);
    val_z = value;
    Send_Z_Control();
}

void MainWindow::on_pushButton_Reset_x_clicked()
{
    val_x = 0.0;
    ui->Knob_x->setValue(0.0);
    Send_X_Control();
}

void MainWindow::on_pushButton_Reset_y_clicked()
{
    val_y = 0.0;
    ui->Knob_y->setValue(0.0);
    Send_Y_Control();
}

void MainWindow::on_pushButton_Reset_r_clicked()
{
    val_r = 0.0;
    ui->Knob_r->setValue(0.0);
    Send_R_Control();
}
void MainWindow::on_pushButton_Reset_z_clicked()
{
    val_z = 0.0;
    ui->Knob_z->setValue(0.0);
    Send_Z_Control();
}

void MainWindow::on_pushButton_Auto_depth_clicked()
{
    if(ui->pushButton_Auto_depth->text() == "Auto Depth")
    {
        ui->pushButton_Auto_depth->setText("Stop Auto Depth");
        JsonObject adv = Json_Make_Advertise_Msg("GUI_Config_Setpoint", "beginner_tutorials/Setpoint_Data");
        mytcpclient->sendMessage(QtJson::serializeStr(adv));

        JsonObject adv1 = Json_Make_Advertise_Msg("GUI_PID_Mode", "std_msgs/String");
        mytcpclient->sendMessage(QtJson::serializeStr(adv1));

        JsonObject PID_Mode = Json_Make_PID_Mode_Data("PID_Mode_Depth_ENA");
        mytcpclient->sendMessage(QtJson::serializeStr(PID_Mode));

        Add_Message_To_Box("Me", QtJson::serialize(adv));
        Add_Message_To_Box("Me", QtJson::serialize(adv1));
        Add_Message_To_Box("Me", QtJson::serialize(PID_Mode));

        ui->lineEdit_TcpClientSend->clear();
        ui->pb_Send_Setpoint_Data->setEnabled(true);
        ui->Slider->setEnabled(true);
        ui->checkBox_PID_Depth->setChecked(true);

        ui->lineEdit_Kp_Depth->setText(QString::number(Kp_Depth));
        ui->lineEdit_Ki_Depth->setText(QString::number(Ki_Depth));
        ui->lineEdit_kd_depth->setText(QString::number(Kd_Depth));
        ui->lineEdit_setpoint_depth->setText(QString::number(Setpoint_Yaw));
    }
    else if(ui->pushButton_Auto_depth->text() == "Stop Auto Depth")
    {
        ui->pushButton_Auto_depth->setText("Auto Depth");
        JsonObject PID_Mode = Json_Make_PID_Mode_Data("PID_Mode_Depth_DIS");
        mytcpclient->sendMessage(QtJson::serializeStr(PID_Mode));

        Add_Message_To_Box("Me", QtJson::serialize(PID_Mode));

        ui->lineEdit_TcpClientSend->clear();;
        ui->Slider->setDisabled(true);
        ui->checkBox_PID_Depth->setChecked(false);

        val_z = 0.0;
        ui->Knob_z->setValue(0.0);
        Send_Z_Control();
    }
}

void MainWindow::on_Slider_sliderMoved(double value)
{
    Setpoint_Depth = value;
    JsonObject Setpoint_Data = Json_Make_Setpoint_Data("Select_PID_Depth",value);
    mytcpclient->sendMessage(QtJson::serializeStr(Setpoint_Data));
    Add_Message_To_Box("Me", QtJson::serialize(Setpoint_Data));
    ui->lineEdit_TcpClientSend->clear();
    ui->lineEdit_SetDepth->setText(QString::number(value));

    ui->lineEdit_setpoint_depth->setText(QString::number(Setpoint_Depth));
}

void MainWindow::on_pushButton_Config_All_clicked()
{
    JsonObject Config_Kx;

    Config_Kx = Json_Make_K_PID_Data("Select_PID_Roll","Select_Kp", ui->lineEdit_Kp_Roll->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Roll","Select_Ki", ui->lineEdit_Ki_Roll->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Roll","Select_Kd", ui->lineEdit_Kd_Roll->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));

    Config_Kx = Json_Make_K_PID_Data("Select_PID_Pitch","Select_Kp", ui->lineEdit_Kp_pitch->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Pitch","Select_Ki", ui->lineEdit_Ki_Pitch->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Pitch","Select_Kd", ui->lineEdit_Kd_pitch->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));

    Config_Kx = Json_Make_K_PID_Data("Select_PID_Yaw","Select_Kp", ui->lineEdit_Kp_Yaw->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Yaw","Select_Ki", ui->lineEdit_Ki_Yaw->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Yaw","Select_Kd", ui->lineEdit_kd_yaw->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));

    Config_Kx = Json_Make_K_PID_Data("Select_PID_Depth","Select_Kp", ui->lineEdit_Kp_Depth->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Depth","Select_Ki", ui->lineEdit_Ki_Depth->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));
    Config_Kx = Json_Make_K_PID_Data("Select_PID_Depth","Select_Kd", ui->lineEdit_kd_depth->text().toDouble());
    mytcpclient->sendMessage(QtJson::serializeStr(Config_Kx));

    Add_Message_To_Box("Me", QtJson::serialize(Config_Kx));
    ui->lineEdit_TcpClientSend->clear();
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    amplification_coefficient = arg1;
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_PID_Percent, 80.0-amplification_coefficient);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
    Send_Z_Control();
}

bool button_start_state = false;
void MainWindow::on_pushButton_Start_Manual_clicked()
{
    if(button_start_state == false)
    {
        button_start_state = true;
        ui->ScrollBar_Manual_State->setValue(2);
        ui->pushButton_Move_Up->setEnabled(true);
        ui->pushButton_Move_Down->setEnabled(true);
        ui->pushButton_Move_Left->setEnabled(true);
        ui->pushButton_Move_Right->setEnabled(true);
        ui->ScrollBar_Select_Manual->setEnabled(true);
        ui->groupBox_Control_1->setEnabled(true);
        ui->groupBox_State->setEnabled(true);
        JsonObject adv = Json_Make_Advertise_Msg("GUI_Manual_Control", "beginner_tutorials/Manual_Control_Data");
        mytcpclient->sendMessage(QtJson::serializeStr(adv));

        ui->pushButton_Start_Manual->setText("Stop Manual\nControl");

        if (ui->ScrollBar_Select_Manual->value() == 1)
        {
            Select_Manual = "GUI";
            Timer->stop();
        }
        else if (ui->ScrollBar_Select_Manual->value() == 2)
        {
            Select_Manual = "Joystick";
            connect(Timer, SIGNAL(timeout()), this, SLOT(user_Timer_Timeout()));
            Timer->start(200);
        }
    }
    else if(button_start_state == true)
    {
        button_start_state = false;
        ui->ScrollBar_Manual_State->setValue(1);
        ui->pushButton_Move_Up->setDisabled(true);
        ui->pushButton_Move_Down->setDisabled(true);
        ui->pushButton_Move_Left->setDisabled(true);
        ui->pushButton_Move_Right->setDisabled(true);
        ui->ScrollBar_Select_Manual->setDisabled(true);
        ui->groupBox_Control_1->setDisabled(true);
        ui->groupBox_State->setDisabled(true);

        Select_Manual = " ";
        Timer->stop();
        ui->pushButton_Start_Manual->setText("Start Manual\nControl");
    }

}

void MainWindow::on_button_TcpClient_clicked()
{

}

void MainWindow::on_ScrollBar_Select_Manual_sliderMoved(int position)
{
    if (position == 1)
    {
        Select_Manual = "GUI";
        Timer->stop();
    }
    else if (position == 2)
    {
        Select_Manual = "Joystick";
        connect(Timer, SIGNAL(timeout()), this, SLOT(user_Timer_Timeout()));
        Timer->start(200);
    }

}

void MainWindow::on_pushButton_Light_clicked()
{
    if(ui->pushButton_Light->text() == "ON")
    {
        ui->label_Light_State->setText("ON");
        ui->pushButton_Light->setText("OFF");
        JsonObject Light_Control = Json_Make_Light_Control_Data("Light_State_ON");
        mytcpclient->sendMessage(QtJson::serializeStr(Light_Control));
        Add_Message_To_Box("Me", QtJson::serialize(Light_Control));
    }
    else if(ui->pushButton_Light->text() == "OFF")
    {
        ui->label_Light_State->setText("OFF");
        ui->pushButton_Light->setText("ON");
        JsonObject Light_Control = Json_Make_Light_Control_Data("Light_State_OFF");
        mytcpclient->sendMessage(QtJson::serializeStr(Light_Control));
        Add_Message_To_Box("Me", QtJson::serialize(Light_Control));
    }
}

void MainWindow::on_pushButton_Adv_Light_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_Light_Control", "std_msgs/String");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->pushButton_Light->setEnabled(true);
}

void MainWindow::on_M1_percent_valueChanged(int arg1)
{
    m1_percent = arg1;
}

void MainWindow::on_M2_percent_valueChanged(int arg1)
{
    m2_percent = arg1;
}

void MainWindow::on_M3_percent_valueChanged(int arg1)
{
    m3_percent = arg1;
}

void MainWindow::on_M4_percent_valueChanged(int arg1)
{
    m4_percent = arg1;
}

void MainWindow::on_M5_percent_valueChanged(int arg1)
{
    m5_percent = arg1;
}

void MainWindow::on_M6_percent_valueChanged(int arg1)
{
    m6_percent = arg1;
}

void MainWindow::on_M7_percent_valueChanged(int arg1)
{
    m7_percent = arg1;
}

void MainWindow::on_Knob_Set_Heading_sliderMoved(double value)
{
    ui->lineEdit_Set_Heading->setText(QString::number(value));

    Setpoint_Yaw = value;
    JsonObject Setpoint_Data = Json_Make_Setpoint_Data("Select_PID_Yaw",value);
    mytcpclient->sendMessage(QtJson::serializeStr(Setpoint_Data));
    Add_Message_To_Box("Me", QtJson::serialize(Setpoint_Data));
    ui->lineEdit_TcpClientSend->clear();

    ui->lineEdit_setpoint_yaw->setText(QString::number(Setpoint_Yaw));
}

void MainWindow::on_pushButton_SetYaw_clicked()
{
    if(ui->pushButton_SetYaw->text() == "Auto Heading")
    {
        ui->pushButton_SetYaw->setText("Stop Auto Heading");
        JsonObject adv = Json_Make_Advertise_Msg("GUI_Config_Setpoint", "beginner_tutorials/Setpoint_Data");
        mytcpclient->sendMessage(QtJson::serializeStr(adv));

        JsonObject adv1 = Json_Make_Advertise_Msg("GUI_PID_Mode", "std_msgs/String");
        mytcpclient->sendMessage(QtJson::serializeStr(adv1));

        JsonObject PID_Mode = Json_Make_PID_Mode_Data("PID_Mode_Yaw_ENA");
        mytcpclient->sendMessage(QtJson::serializeStr(PID_Mode));

        Add_Message_To_Box("Me", QtJson::serialize(adv));
        Add_Message_To_Box("Me", QtJson::serialize(adv1));
        Add_Message_To_Box("Me", QtJson::serialize(PID_Mode));

        ui->lineEdit_TcpClientSend->clear();
        ui->pb_Send_Setpoint_Data->setEnabled(true);
        ui->Knob_Set_Heading->setEnabled(true);
        ui->lineEdit_Set_Heading->setEnabled(true);
        ui->checkBox_PID_Yaw->setChecked(true);

        ui->lineEdit_Kp_Yaw->setText(QString::number(Kp_Depth));
        ui->lineEdit_Ki_Yaw->setText(QString::number(Ki_Depth));
        ui->lineEdit_kd_yaw->setText(QString::number(Kd_Depth));
        ui->lineEdit_setpoint_yaw->setText(QString::number(Setpoint_Yaw));
    }
    else if(ui->pushButton_SetYaw->text() == "Stop Auto Heading")
    {
        ui->pushButton_SetYaw->setText("Auto Heading");
        JsonObject PID_Mode = Json_Make_PID_Mode_Data("PID_Mode_Yaw_DIS");
        mytcpclient->sendMessage(QtJson::serializeStr(PID_Mode));

        Add_Message_To_Box("Me", QtJson::serialize(PID_Mode));

        ui->lineEdit_TcpClientSend->clear();;
        ui->Knob_Set_Heading->setDisabled(true);
        ui->lineEdit_Set_Heading->setDisabled(true);
        ui->checkBox_PID_Yaw->setChecked(false);

        val_x = 0.0;
        ui->Knob_x->setValue(0.0);
        Send_X_Control();
    }
}

void MainWindow::on_pushButton_Adv_ROV_Config_clicked()
{
    JsonObject adv = Json_Make_Advertise_Msg("GUI_ROV_Conf", "beginner_tutorials/Manual_Control_Data");
    mytcpclient->sendMessage(QtJson::serializeStr(adv));
    Add_Message_To_Box("Me", QtJson::serialize(adv));
    ui->pushButton_Config_Percent_M->setEnabled(true);
    ui->pushButton_Config_Percent_M_2->setEnabled(true);
    ui->pushButton_Config_Percent_M_3->setEnabled(true);
    ui->pushButton_Config_Percent_M_4->setEnabled(true);
    ui->pushButton_Config_Percent_M_5->setEnabled(true);
    ui->pushButton_Config_Percent_M_6->setEnabled(true);
    ui->pushButton_Config_Percent_M_7->setEnabled(true);
    ui->pushButton_Set_Roll->setEnabled(true);
    ui->pushButton_Set_Pitch->setEnabled(true);
}

void MainWindow::on_pushButton_Config_Percent_M_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M1_Percent, m1_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_2_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M2_Percent, m2_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_3_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M3_Percent, m3_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_4_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M4_Percent, m4_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_5_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M5_Percent, m5_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_6_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M6_Percent, m6_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Config_Percent_M_7_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_M7_Percent, m7_percent);
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Set_Roll_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_Set_Roll, ui->SetRoll->value());
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_pushButton_Set_Pitch_clicked()
{
    JsonObject Config = Json_Make_ROV_Config_Data(Select_Config_Set_Pitch, ui->SetPitch->value());
    mytcpclient->sendMessage(QtJson::serializeStr(Config));
    Add_Message_To_Box("Me", QtJson::serialize(Config));
}

void MainWindow::on_lineEdit_Set_Heading_textChanged(const QString &arg1)
{
    double value = arg1.toDouble();
    ui->Knob_Set_Heading->setValue(value);
    JsonObject Setpoint_Data = Json_Make_Setpoint_Data("Select_PID_Yaw",value);
    mytcpclient->sendMessage(QtJson::serializeStr(Setpoint_Data));
    Add_Message_To_Box("Me", QtJson::serialize(Setpoint_Data));
}
