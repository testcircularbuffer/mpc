/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[78];
    char stringdata0[2077];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 24), // "onTcpClientButtonClicked"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 24), // "onTcpClientNewConnection"
QT_MOC_LITERAL(4, 62, 4), // "from"
QT_MOC_LITERAL(5, 67, 4), // "port"
QT_MOC_LITERAL(6, 72, 28), // "onTcpClientStopButtonClicked"
QT_MOC_LITERAL(7, 101, 18), // "onTcpClientTimeOut"
QT_MOC_LITERAL(8, 120, 34), // "onTcpClientDisconnectButtonCl..."
QT_MOC_LITERAL(9, 155, 23), // "onTcpClientDisconnected"
QT_MOC_LITERAL(10, 179, 22), // "onTcpClientSendMessage"
QT_MOC_LITERAL(11, 202, 24), // "onTcpClientAppendMessage"
QT_MOC_LITERAL(12, 227, 7), // "message"
QT_MOC_LITERAL(13, 235, 18), // "Add_Message_To_Box"
QT_MOC_LITERAL(14, 254, 22), // "onRefreshButtonClicked"
QT_MOC_LITERAL(15, 277, 24), // "on_pushButton_up_clicked"
QT_MOC_LITERAL(16, 302, 27), // "on_pushButton_right_clicked"
QT_MOC_LITERAL(17, 330, 26), // "on_pushButton_left_clicked"
QT_MOC_LITERAL(18, 357, 26), // "on_pushButton_down_clicked"
QT_MOC_LITERAL(19, 384, 14), // "Send_X_Control"
QT_MOC_LITERAL(20, 399, 14), // "Send_Y_Control"
QT_MOC_LITERAL(21, 414, 14), // "Send_R_Control"
QT_MOC_LITERAL(22, 429, 14), // "Send_Z_Control"
QT_MOC_LITERAL(23, 444, 31), // "on_pushButton_Advertise_clicked"
QT_MOC_LITERAL(24, 476, 35), // "on_Advertise_Manual_Control_c..."
QT_MOC_LITERAL(25, 512, 34), // "on_Advertise_Setpoint_Data_cl..."
QT_MOC_LITERAL(26, 547, 29), // "on_Advertise_PID_Mode_clicked"
QT_MOC_LITERAL(27, 577, 30), // "on_Advertise_Config_Kx_clicked"
QT_MOC_LITERAL(28, 608, 33), // "on_pb_Send_Manual_Control_cli..."
QT_MOC_LITERAL(29, 642, 27), // "on_pb_Send_PID_Mode_clicked"
QT_MOC_LITERAL(30, 670, 32), // "on_pb_Send_Setpoint_Data_clicked"
QT_MOC_LITERAL(31, 703, 21), // "on_pb_Send_Kx_clicked"
QT_MOC_LITERAL(32, 725, 29), // "on_pushButton_Move_Up_clicked"
QT_MOC_LITERAL(33, 755, 31), // "on_pushButton_Move_Down_clicked"
QT_MOC_LITERAL(34, 787, 31), // "on_pushButton_Move_Left_clicked"
QT_MOC_LITERAL(35, 819, 32), // "on_pushButton_Move_Right_clicked"
QT_MOC_LITERAL(36, 852, 26), // "on_pushButton_Stop_clicked"
QT_MOC_LITERAL(37, 879, 21), // "on_Knob_x_sliderMoved"
QT_MOC_LITERAL(38, 901, 5), // "value"
QT_MOC_LITERAL(39, 907, 21), // "on_Knob_y_sliderMoved"
QT_MOC_LITERAL(40, 929, 21), // "on_Knob_z_sliderMoved"
QT_MOC_LITERAL(41, 951, 21), // "on_Knob_r_sliderMoved"
QT_MOC_LITERAL(42, 973, 29), // "on_pushButton_Reset_x_clicked"
QT_MOC_LITERAL(43, 1003, 29), // "on_pushButton_Reset_y_clicked"
QT_MOC_LITERAL(44, 1033, 29), // "on_pushButton_Reset_r_clicked"
QT_MOC_LITERAL(45, 1063, 32), // "on_pushButton_Auto_depth_clicked"
QT_MOC_LITERAL(46, 1096, 21), // "on_Slider_sliderMoved"
QT_MOC_LITERAL(47, 1118, 32), // "on_pushButton_Config_All_clicked"
QT_MOC_LITERAL(48, 1151, 29), // "on_pushButton_Reset_z_clicked"
QT_MOC_LITERAL(49, 1181, 29), // "on_doubleSpinBox_valueChanged"
QT_MOC_LITERAL(50, 1211, 4), // "arg1"
QT_MOC_LITERAL(51, 1216, 34), // "on_pushButton_Start_Manual_cl..."
QT_MOC_LITERAL(52, 1251, 27), // "on_button_TcpClient_clicked"
QT_MOC_LITERAL(53, 1279, 38), // "on_ScrollBar_Select_Manual_sl..."
QT_MOC_LITERAL(54, 1318, 8), // "position"
QT_MOC_LITERAL(55, 1327, 27), // "on_pushButton_Light_clicked"
QT_MOC_LITERAL(56, 1355, 31), // "on_pushButton_Adv_Light_clicked"
QT_MOC_LITERAL(57, 1387, 26), // "on_M1_percent_valueChanged"
QT_MOC_LITERAL(58, 1414, 26), // "on_M2_percent_valueChanged"
QT_MOC_LITERAL(59, 1441, 26), // "on_M3_percent_valueChanged"
QT_MOC_LITERAL(60, 1468, 26), // "on_M4_percent_valueChanged"
QT_MOC_LITERAL(61, 1495, 26), // "on_M5_percent_valueChanged"
QT_MOC_LITERAL(62, 1522, 26), // "on_M6_percent_valueChanged"
QT_MOC_LITERAL(63, 1549, 26), // "on_M7_percent_valueChanged"
QT_MOC_LITERAL(64, 1576, 31), // "on_Knob_Set_Heading_sliderMoved"
QT_MOC_LITERAL(65, 1608, 28), // "on_pushButton_SetYaw_clicked"
QT_MOC_LITERAL(66, 1637, 36), // "on_pushButton_Adv_ROV_Config_..."
QT_MOC_LITERAL(67, 1674, 38), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(68, 1713, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(69, 1754, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(70, 1795, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(71, 1836, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(72, 1877, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(73, 1918, 40), // "on_pushButton_Config_Percent_..."
QT_MOC_LITERAL(74, 1959, 30), // "on_pushButton_Set_Roll_clicked"
QT_MOC_LITERAL(75, 1990, 31), // "on_pushButton_Set_Pitch_clicked"
QT_MOC_LITERAL(76, 2022, 35), // "on_lineEdit_Set_Heading_textC..."
QT_MOC_LITERAL(77, 2058, 18) // "user_Timer_Timeout"

    },
    "MainWindow\0onTcpClientButtonClicked\0"
    "\0onTcpClientNewConnection\0from\0port\0"
    "onTcpClientStopButtonClicked\0"
    "onTcpClientTimeOut\0"
    "onTcpClientDisconnectButtonClicked\0"
    "onTcpClientDisconnected\0onTcpClientSendMessage\0"
    "onTcpClientAppendMessage\0message\0"
    "Add_Message_To_Box\0onRefreshButtonClicked\0"
    "on_pushButton_up_clicked\0"
    "on_pushButton_right_clicked\0"
    "on_pushButton_left_clicked\0"
    "on_pushButton_down_clicked\0Send_X_Control\0"
    "Send_Y_Control\0Send_R_Control\0"
    "Send_Z_Control\0on_pushButton_Advertise_clicked\0"
    "on_Advertise_Manual_Control_clicked\0"
    "on_Advertise_Setpoint_Data_clicked\0"
    "on_Advertise_PID_Mode_clicked\0"
    "on_Advertise_Config_Kx_clicked\0"
    "on_pb_Send_Manual_Control_clicked\0"
    "on_pb_Send_PID_Mode_clicked\0"
    "on_pb_Send_Setpoint_Data_clicked\0"
    "on_pb_Send_Kx_clicked\0"
    "on_pushButton_Move_Up_clicked\0"
    "on_pushButton_Move_Down_clicked\0"
    "on_pushButton_Move_Left_clicked\0"
    "on_pushButton_Move_Right_clicked\0"
    "on_pushButton_Stop_clicked\0"
    "on_Knob_x_sliderMoved\0value\0"
    "on_Knob_y_sliderMoved\0on_Knob_z_sliderMoved\0"
    "on_Knob_r_sliderMoved\0"
    "on_pushButton_Reset_x_clicked\0"
    "on_pushButton_Reset_y_clicked\0"
    "on_pushButton_Reset_r_clicked\0"
    "on_pushButton_Auto_depth_clicked\0"
    "on_Slider_sliderMoved\0"
    "on_pushButton_Config_All_clicked\0"
    "on_pushButton_Reset_z_clicked\0"
    "on_doubleSpinBox_valueChanged\0arg1\0"
    "on_pushButton_Start_Manual_clicked\0"
    "on_button_TcpClient_clicked\0"
    "on_ScrollBar_Select_Manual_sliderMoved\0"
    "position\0on_pushButton_Light_clicked\0"
    "on_pushButton_Adv_Light_clicked\0"
    "on_M1_percent_valueChanged\0"
    "on_M2_percent_valueChanged\0"
    "on_M3_percent_valueChanged\0"
    "on_M4_percent_valueChanged\0"
    "on_M5_percent_valueChanged\0"
    "on_M6_percent_valueChanged\0"
    "on_M7_percent_valueChanged\0"
    "on_Knob_Set_Heading_sliderMoved\0"
    "on_pushButton_SetYaw_clicked\0"
    "on_pushButton_Adv_ROV_Config_clicked\0"
    "on_pushButton_Config_Percent_M_clicked\0"
    "on_pushButton_Config_Percent_M_2_clicked\0"
    "on_pushButton_Config_Percent_M_3_clicked\0"
    "on_pushButton_Config_Percent_M_4_clicked\0"
    "on_pushButton_Config_Percent_M_5_clicked\0"
    "on_pushButton_Config_Percent_M_6_clicked\0"
    "on_pushButton_Config_Percent_M_7_clicked\0"
    "on_pushButton_Set_Roll_clicked\0"
    "on_pushButton_Set_Pitch_clicked\0"
    "on_lineEdit_Set_Heading_textChanged\0"
    "user_Timer_Timeout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      70,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  364,    2, 0x08 /* Private */,
       3,    2,  365,    2, 0x08 /* Private */,
       6,    0,  370,    2, 0x08 /* Private */,
       7,    0,  371,    2, 0x08 /* Private */,
       8,    0,  372,    2, 0x08 /* Private */,
       9,    0,  373,    2, 0x08 /* Private */,
      10,    0,  374,    2, 0x08 /* Private */,
      11,    2,  375,    2, 0x08 /* Private */,
      13,    2,  380,    2, 0x08 /* Private */,
      14,    0,  385,    2, 0x08 /* Private */,
      15,    0,  386,    2, 0x08 /* Private */,
      16,    0,  387,    2, 0x08 /* Private */,
      17,    0,  388,    2, 0x08 /* Private */,
      18,    0,  389,    2, 0x08 /* Private */,
      19,    0,  390,    2, 0x08 /* Private */,
      20,    0,  391,    2, 0x08 /* Private */,
      21,    0,  392,    2, 0x08 /* Private */,
      22,    0,  393,    2, 0x08 /* Private */,
      23,    0,  394,    2, 0x08 /* Private */,
      24,    0,  395,    2, 0x08 /* Private */,
      25,    0,  396,    2, 0x08 /* Private */,
      26,    0,  397,    2, 0x08 /* Private */,
      27,    0,  398,    2, 0x08 /* Private */,
      28,    0,  399,    2, 0x08 /* Private */,
      29,    0,  400,    2, 0x08 /* Private */,
      30,    0,  401,    2, 0x08 /* Private */,
      31,    0,  402,    2, 0x08 /* Private */,
      32,    0,  403,    2, 0x08 /* Private */,
      33,    0,  404,    2, 0x08 /* Private */,
      34,    0,  405,    2, 0x08 /* Private */,
      35,    0,  406,    2, 0x08 /* Private */,
      36,    0,  407,    2, 0x08 /* Private */,
      37,    1,  408,    2, 0x08 /* Private */,
      39,    1,  411,    2, 0x08 /* Private */,
      40,    1,  414,    2, 0x08 /* Private */,
      41,    1,  417,    2, 0x08 /* Private */,
      42,    0,  420,    2, 0x08 /* Private */,
      43,    0,  421,    2, 0x08 /* Private */,
      44,    0,  422,    2, 0x08 /* Private */,
      45,    0,  423,    2, 0x08 /* Private */,
      46,    1,  424,    2, 0x08 /* Private */,
      47,    0,  427,    2, 0x08 /* Private */,
      48,    0,  428,    2, 0x08 /* Private */,
      49,    1,  429,    2, 0x08 /* Private */,
      51,    0,  432,    2, 0x08 /* Private */,
      52,    0,  433,    2, 0x08 /* Private */,
      53,    1,  434,    2, 0x08 /* Private */,
      55,    0,  437,    2, 0x08 /* Private */,
      56,    0,  438,    2, 0x08 /* Private */,
      57,    1,  439,    2, 0x08 /* Private */,
      58,    1,  442,    2, 0x08 /* Private */,
      59,    1,  445,    2, 0x08 /* Private */,
      60,    1,  448,    2, 0x08 /* Private */,
      61,    1,  451,    2, 0x08 /* Private */,
      62,    1,  454,    2, 0x08 /* Private */,
      63,    1,  457,    2, 0x08 /* Private */,
      64,    1,  460,    2, 0x08 /* Private */,
      65,    0,  463,    2, 0x08 /* Private */,
      66,    0,  464,    2, 0x08 /* Private */,
      67,    0,  465,    2, 0x08 /* Private */,
      68,    0,  466,    2, 0x08 /* Private */,
      69,    0,  467,    2, 0x08 /* Private */,
      70,    0,  468,    2, 0x08 /* Private */,
      71,    0,  469,    2, 0x08 /* Private */,
      72,    0,  470,    2, 0x08 /* Private */,
      73,    0,  471,    2, 0x08 /* Private */,
      74,    0,  472,    2, 0x08 /* Private */,
      75,    0,  473,    2, 0x08 /* Private */,
      76,    1,  474,    2, 0x08 /* Private */,
      77,    0,  477,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UShort,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    4,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    4,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   50,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   54,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Int,   50,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   50,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onTcpClientButtonClicked(); break;
        case 1: _t->onTcpClientNewConnection((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2]))); break;
        case 2: _t->onTcpClientStopButtonClicked(); break;
        case 3: _t->onTcpClientTimeOut(); break;
        case 4: _t->onTcpClientDisconnectButtonClicked(); break;
        case 5: _t->onTcpClientDisconnected(); break;
        case 6: _t->onTcpClientSendMessage(); break;
        case 7: _t->onTcpClientAppendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 8: _t->Add_Message_To_Box((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 9: _t->onRefreshButtonClicked(); break;
        case 10: _t->on_pushButton_up_clicked(); break;
        case 11: _t->on_pushButton_right_clicked(); break;
        case 12: _t->on_pushButton_left_clicked(); break;
        case 13: _t->on_pushButton_down_clicked(); break;
        case 14: _t->Send_X_Control(); break;
        case 15: _t->Send_Y_Control(); break;
        case 16: _t->Send_R_Control(); break;
        case 17: _t->Send_Z_Control(); break;
        case 18: _t->on_pushButton_Advertise_clicked(); break;
        case 19: _t->on_Advertise_Manual_Control_clicked(); break;
        case 20: _t->on_Advertise_Setpoint_Data_clicked(); break;
        case 21: _t->on_Advertise_PID_Mode_clicked(); break;
        case 22: _t->on_Advertise_Config_Kx_clicked(); break;
        case 23: _t->on_pb_Send_Manual_Control_clicked(); break;
        case 24: _t->on_pb_Send_PID_Mode_clicked(); break;
        case 25: _t->on_pb_Send_Setpoint_Data_clicked(); break;
        case 26: _t->on_pb_Send_Kx_clicked(); break;
        case 27: _t->on_pushButton_Move_Up_clicked(); break;
        case 28: _t->on_pushButton_Move_Down_clicked(); break;
        case 29: _t->on_pushButton_Move_Left_clicked(); break;
        case 30: _t->on_pushButton_Move_Right_clicked(); break;
        case 31: _t->on_pushButton_Stop_clicked(); break;
        case 32: _t->on_Knob_x_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 33: _t->on_Knob_y_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 34: _t->on_Knob_z_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 35: _t->on_Knob_r_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 36: _t->on_pushButton_Reset_x_clicked(); break;
        case 37: _t->on_pushButton_Reset_y_clicked(); break;
        case 38: _t->on_pushButton_Reset_r_clicked(); break;
        case 39: _t->on_pushButton_Auto_depth_clicked(); break;
        case 40: _t->on_Slider_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 41: _t->on_pushButton_Config_All_clicked(); break;
        case 42: _t->on_pushButton_Reset_z_clicked(); break;
        case 43: _t->on_doubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 44: _t->on_pushButton_Start_Manual_clicked(); break;
        case 45: _t->on_button_TcpClient_clicked(); break;
        case 46: _t->on_ScrollBar_Select_Manual_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 47: _t->on_pushButton_Light_clicked(); break;
        case 48: _t->on_pushButton_Adv_Light_clicked(); break;
        case 49: _t->on_M1_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 50: _t->on_M2_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 51: _t->on_M3_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 52: _t->on_M4_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 53: _t->on_M5_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 54: _t->on_M6_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 55: _t->on_M7_percent_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 56: _t->on_Knob_Set_Heading_sliderMoved((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 57: _t->on_pushButton_SetYaw_clicked(); break;
        case 58: _t->on_pushButton_Adv_ROV_Config_clicked(); break;
        case 59: _t->on_pushButton_Config_Percent_M_clicked(); break;
        case 60: _t->on_pushButton_Config_Percent_M_2_clicked(); break;
        case 61: _t->on_pushButton_Config_Percent_M_3_clicked(); break;
        case 62: _t->on_pushButton_Config_Percent_M_4_clicked(); break;
        case 63: _t->on_pushButton_Config_Percent_M_5_clicked(); break;
        case 64: _t->on_pushButton_Config_Percent_M_6_clicked(); break;
        case 65: _t->on_pushButton_Config_Percent_M_7_clicked(); break;
        case 66: _t->on_pushButton_Set_Roll_clicked(); break;
        case 67: _t->on_pushButton_Set_Pitch_clicked(); break;
        case 68: _t->on_lineEdit_Set_Heading_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 69: _t->user_Timer_Timeout(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 70)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 70;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 70)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 70;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
