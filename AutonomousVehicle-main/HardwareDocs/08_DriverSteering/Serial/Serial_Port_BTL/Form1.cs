﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

enum ERROR_CODE_e
{
    ERROR_NONE = 0,             /**<  0x00 - No error */
    ERROR_BAD_FRAME_ID,         /**<  0x01 - BAD Frame ID. The Frame ID has not been recognized by the firmware. */
    ERROR_CODE_SET_READ_ONLY,   /**<  0x02 - Write on read-only. The master wants to write on a read-only register. */
    ERROR_CODE_GET_WRITE_ONLY,  /**<  0x03 - Read not allowed. The value cannot be read. */
    ERROR_CODE_NO_TARGET_DRIVE, /**<  0x04 - Bad target drive. The target motor is not supported by the firmware. */
    ERROR_CODE_WRONG_SET,       /**<  0x05 - Value used in the frame is out of range expected by the FW. */
    ERROR_CODE_CMD_ID,          /**<  0x06 - NOT USED */
    ERROR_CODE_WRONG_CMD,       /**<  0x07 - Bad command ID. The command ID has not been recognized. */
    ERROR_CODE_OVERRUN,         /**<  0x08 - Overrun error. Transmission speed too fast, frame not received correctly */
    ERROR_CODE_TIMEOUT,         /**<  0x09 - Timeout error. Received frame corrupted or unrecognized by the FW. */
    ERROR_CODE_BAD_CRC,         /**<  0x0A - The computed CRC is not equal to the received CRC byte. */
    ERROR_BAD_MOTOR_andle_sSELECTED,   /**<  0x0B - Bad target drive. The target motor is not supported by the firmware. */
    ERROR_MP_NOT_ENABLED        /**<  0x0C - Motor Profiler not enabled. */
};

enum MTR_PROTOCOL_e
{
    MTR_PROTOCOL_REG_TARGET_MOTOR,          /* 0   */
    MTR_PROTOCOL_REG_FLAGS,                 /* 1   */
    MTR_PROTOCOL_REG_STATUS,                /* 2   */
    MTR_PROTOCOL_REG_CONTROL_MODE,          /* 3   */
    MTR_PROTOCOL_REG_POS_REF,             /* 4   */
    MTR_PROTOCOL_REG_POS_KP,             /* 4   */

    MTR_PROTOCOL_REG_SPEED_REF,             /* 4   */
    MTR_PROTOCOL_REG_SPEED_KP,              /* 5   */
    MTR_PROTOCOL_REG_SPEED_KI,              /* 6   */
    MTR_PROTOCOL_REG_SPEED_FF,             /* 4   */
    MTR_PROTOCOL_REG_TORQUE_REF,            /* 8   */
    MTR_PROTOCOL_REG_TORQUE_KP,             /* 9   */
    MTR_PROTOCOL_REG_TORQUE_KI,             /* 10  */
    MTR_PROTOCOL_REG_FLUX_REF,              /* 12  */
    MTR_PROTOCOL_REG_FLUX_KP,               /* 13  */
    MTR_PROTOCOL_REG_FLUX_KI,               /* 14  */
    MTR_PROTOCOL_REG_FLUXWK_KP,             /* 22  */
    MTR_PROTOCOL_REG_FLUXWK_KI,             /* 23  */
    MTR_PROTOCOL_REG_FLUXWK_BUS,            /* 24  */
    MTR_PROTOCOL_REG_BUS_VOLTAGE,           /* 25  */
    MTR_PROTOCOL_REG_HEATS_TEMP,            /* 26  */
    MTR_PROTOCOL_REG_MOTOR_PP,           /* 27  */
    MTR_PROTOCOL_REG_SPEED_MEAS,            /* 30  */
    MTR_PROTOCOL_REG_TORQUE_MEAS,           /* 31  */
    MTR_PROTOCOL_REG_FLUX_MEAS,             /* 32  */
    MTR_PROTOCOL_REG_FLUXWK_BUS_MEAS,       /* 33  */
    MTR_PROTOCOL_REG_I_ALPHA,               /* 37  */
    MTR_PROTOCOL_REG_I_BETA,                /* 38  */
    MTR_PROTOCOL_REG_I_Q,                   /* 39  */
    MTR_PROTOCOL_REG_I_D,                   /* 40  */
    MTR_PROTOCOL_REG_I_Q_REF,               /* 41  */
    MTR_PROTOCOL_REG_I_D_REF,               /* 42  */
    MTR_PROTOCOL_REG_V_Q,                   /* 43  */
    MTR_PROTOCOL_REG_V_D,                   /* 44  */
    MTR_PROTOCOL_REG_V_ALPHA,               /* 45  */
    MTR_PROTOCOL_REG_V_BETA,                /* 46  */
    MTR_PROTOCOL_REG_MEAS_EL_ANGLE,         /* 47  */
    MTR_PROTOCOL_REG_MEAS_ROT_SPEED,        /* 48  */
    MTR_PROTOCOL_REG_OBS_BEMF_ALPHA,        /* 53  */
    MTR_PROTOCOL_REG_OBS_BEMF_BETA,         /* 54  */
    MTR_PROTOCOL_REG_OBS_CR_EL_ANGLE,       /* 55  */
    MTR_PROTOCOL_REG_OBS_CR_ROT_SPEED,      /* 56  */
    MTR_PROTOCOL_REG_OBS_CR_I_ALPHA,        /* 57  */
    MTR_PROTOCOL_REG_OBS_CR_I_BETA,         /* 58  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_ALPHA,     /* 59  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_BETA,      /* 60  */
    MTR_PROTOCOL_REG_MAX_APP_SPEED,         /* 63  */
    MTR_PROTOCOL_REG_MIN_APP_SPEED,         /* 64  */
    MTR_PROTOCOL_REG_IQ_SPEEDMODE,          /* 65  */
    MTR_PROTOCOL_REG_EST_BEMF_LEVEL,        /* 66  */
    MTR_PROTOCOL_REG_OBS_BEMF_LEVEL,        /* 67  */
    MTR_PROTOCOL_REG_EST_CR_BEMF_LEVEL,     /* 68  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_LEVEL,     /* 69  */
    MTR_PROTOCOL_REG_FF_1Q,                 /* 70  */
    MTR_PROTOCOL_REG_FF_1D,                 /* 71  */
    MTR_PROTOCOL_REG_FF_2,                  /* 72  */
    MTR_PROTOCOL_REG_FF_VQ,                 /* 73  */
    MTR_PROTOCOL_REG_FF_VD,                 /* 74  */
    MTR_PROTOCOL_REG_SPEED_KP_DIV,          /* 110 */
    MTR_PROTOCOL_REG_SPEED_KI_DIV,          /* 111 */
    MTR_PROTOCOL_REG_SC_STARTUP_SPEED,      /* 125 */
    MTR_PROTOCOL_REG_SC_STARTUP_ACC,        /* 126 */
    MTR_PROTOCOL_REG_BASE_CURRENT,
    MTR_PROTOCOL_REG_STOP_CURRENT,
    MTR_PROTOCOL_REG_ENCODER_REVOLUTION,
    MTR_PROTOCOL_REG_MICROSTEP,
    MTR_PROTOCOL_REG_SINE_KP,
    MTR_PROTOCOL_REG_SINE_KI,
    MTR_PROTOCOL_REG_SINE_GAIN_1,
    MTR_PROTOCOL_REG_SINE_GAIN_2,
    MTR_PROTOCOL_REG_SINE_GAIN_3,
    MTR_PROTOCOL_REG_SINE_GAIN_4,
    MTR_PROTOCOL_REG_SINE_GAIN_5,
    MTR_PROTOCOL_REG_SPEED_LIMIT,
    MTR_PROTOCOL_REG_CURRENT_LIMIT,
    MTR_PROTOCOL_REG_RESONACE_FRE,
    MTR_PROTOCOL_REG_RESONACE_GAIN,
    MTR_PROTOCOL_REG_SOFWARE_LIMIT_POS,
    MTR_PROTOCOL_REG_SOFWARE_LIMIT_NEG,
    MTR_PROTOCOL_REG_PRESET_POS,

    MTR_PROTOCOL_REG_CHANNEL_1,
    MTR_PROTOCOL_REG_CHANNEL_2,
    MTR_PROTOCOL_REG_CHANNEL_3,
    MTR_PROTOCOL_REG_CHANNEL_4,
    MTR_PROTOCOL_REG_CHANNEL_DIGITAL,
    MTR_PROTOCOL_REG_TRIGGER_CHANNEL,
    MTR_PROTOCOL_REG_TRIGGER_LEVEL,
    MTR_PROTOCOL_REG_TRIGGER_EDGE,
    MTR_PROTOCOL_REG_TRIGGER_POSITION,
    MTR_PROTOCOL_REG_MONITOR_MODE,
    MTR_PROTOCOL_REG_MONITOR_COUNT,
    MTR_PROTOCOL_REG_MONITOR_ENANLE,
    MTR_PROTOCOL_REG_MONITOR_AUTO,

    MTR_PROTOCOL_REG_POSITION_REF,
    MTR_PROTOCOL_REG_MOVE_SPEED,
    MTR_PROTOCOL_REG_ACCEL_TIME,
    MTR_PROTOCOL_REG_DECEL_TIME,
    MTR_PROTOCOL_REG_INTERVAL_TIME,

    MTR_PROTOCOL_REG_POSITION_MODE,
    MTR_PROTOCOL_REG_SPEED_MODE,
    MTR_PROTOCOL_REG_CURRENT_MODE,

};


namespace Serial_Port_BTL
{
    public partial class Form1 : Form
    {

        string dataOut, dataIN; 
        public Form1()
        {
            InitializeComponent();
        }
        // thiets lập serial Port
       
        //hàm tự động cập nhập tất cả các Port 
        int intlen = 0; // intlen bằng tổng số Port trong Com
        private void timer1_Tick(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            if(intlen != ports.Length)
            {
                intlen = ports.Length;
                cBoxComPort.Items.Clear();
                for (int j = 0; j < intlen; j++)
                {
                    cBoxComPort.Items.Add(ports[j]);                  // hàm chọn cổng COM
                }
            }
        }

        // hàm bật kết nối serial Port
        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cBoxComPort.Text;
                serialPort1.BaudRate = Convert.ToInt32(cBoxBaudRate.Text);
                serialPort1.DataBits = Convert.ToInt32(cBoxDataBits.Text);
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cBoxStopBits.Text);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), cBoxPatityBits.Text);
                serialPort1.Open();
                progressBar1.Value = 100;
                lbStaticComPort.Text = "ON";
                groupBox11.Visible = true;
                groupBox4.Visible = true;
                groupBox_DriverControl.Visible = true;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        //hàm ngắt kết nối serial Port
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                progressBar1.Value = 0;
                lbStaticComPort.Text = "OFF";
                groupBox11.Visible = false;
                groupBox4.Visible = false;
                groupBox_DriverControl.Visible = false;
            }
        }

      
        // hàm đóng cửa sổ app
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
            this.Close();
        }

        // thiết lập nhận data
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {  
         dataIN =(string)serialPort1.ReadExisting();
            this.Invoke(new EventHandler (showdata));

        }

        // hàm con của biến nhận data
        private void showdata(object sender, EventArgs e)
        {
            byte[] data_byte = new byte[200];
            string data_text = "";
            string temp = "";
            for (UInt16 n = 0; n < dataIN.Length; n++)
            {
                data_byte[n] = Convert.ToByte(dataIN[n]);

                temp = string.Format("{0:00}", data_byte[n]);
                data_text += temp + " ";
            }
            tBoxDataReceive.Text = (data_text + "\r\n");
            tBoxDataReceive.SelectionStart = tBoxDataReceive.Text.Length;

            //if (chBoxAlwayUpDate.Checked)
            //{
            //    //t = dataIN;
            //    tBoxDataReceive.Text = dataIN;
            //}
            //else if (chBoxAddToOldData.Checked)
            //{
            //    tBoxDataReceive.Text += dataIN;
            //}

        }

        //lựa chọn cách nhận về là liên tục hoặc cộng dồn data cũ

        private void chBoxAlwayUpDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chBoxAlwayUpDate.Checked)
            {
                chBoxAlwayUpDate.Checked = true;
                chBoxAddToOldData.Checked = false;
            }
            else { chBoxAddToOldData.Checked = true; }
        }

        private void chBoxAddToOldData_CheckedChanged(object sender, EventArgs e)
        {
            if (chBoxAddToOldData.Checked)
            {
                chBoxAddToOldData.Checked = true;
                chBoxAlwayUpDate.Checked = false;
            }
            else { chBoxAlwayUpDate.Checked = true; }
        }

    // xóa ô dữ liệu nhận về
        private void button1_Click(object sender, EventArgs e)
        {
            if (tBoxDataReceive.Text != "")
            {
                tBoxDataReceive.Text = "";
            }
        }


    // xóa ô data gửi đi
        private void button2_Click(object sender, EventArgs e)
        {
            if (tBoxOutData.Text != "")
            {
                tBoxOutData.Text = "";
            }
        }

        private void btnSendData_Click(object sender, EventArgs e)
        {
            dataOut = tBoxOutData.Text+"\r";
            if (serialPort1.IsOpen)
                serialPort1.WriteLine(dataOut);

        }

        
        // độ dài chuỗi data gửi đi
        private void tBoxOutData_TextChanged(object sender, EventArgs e)
        {
            int dataOUTLength = tBoxOutData.TextLength;
            lbLengthSendData.Text = string.Format("{0:00}", dataOUTLength);
        }

        // độ dài chuỗi data nhận về
        private void tBoxDataReceive_TextChanged(object sender, EventArgs e)
        {
            int RxLength = tBoxDataReceive.TextLength;
            lbLengthReceiveData.Text = string.Format("{0:00}", RxLength);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox11.Visible = false;
            groupBox4.Visible = false;
            groupBox_DriverControl.Visible = false;
            chBoxAlwayUpDate.Checked = true;
        }

        private void btnExit_MouseMove(object sender, MouseEventArgs e)
        {
            btnExit.ForeColor = Color.White;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.ForeColor = Color.Black;
        }

        // hiển thị form hướng dẫn
        private void button3_Click(object sender, EventArgs e)
        {
            Form2 fmb = new Form2();
            fmb.FormClosed += new FormClosedEventHandler(fmb_FormClosed1);
            fmb.Show();
        }

        const byte MTR_PROTOCOL_CODE_EXECUTE_CMD = 0x03;
        const byte MTR_PROTOCOL_CODE_SET_REG = 0x01;
        const byte MTR_PROTOCOL_CMD_SERVO_ON = 0x01;
        const byte MTR_PROTOCOL_REG_POSITION_REF = 93;
        const byte MTR_PROTOCOL_REG_POSITION_MODE = 98;
        const byte MTR_POS_STEP = (1);
        const byte MTR_REF_POS_ABS = 0;
        const byte MTR_PROTOCOL_REG_BASE_CURRENT = 62;
        byte CalcCRC(byte uCode, byte uSize, byte[] buffer)
        {
            byte uCRC = 0;
            UInt16 uSum = 0;
            byte idx;
            {
                uSum += uCode;
                uSum += uSize;

                for (idx = 0; idx < uSize; idx++)
                {
                    uSum += buffer[idx + 2];
                }

                uCRC = (byte)(uSum & 0xFF); // Low Byte of nSum
                uCRC += (byte)(uSum >> 8); // High Byte of nSum
            }
            return uCRC;
        }

        private void button_Servo_On_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[4];
            arr[0] = MTR_PROTOCOL_CODE_EXECUTE_CMD;
            arr[1] = 1;
            arr[2] = MTR_PROTOCOL_CMD_SERVO_ON;
            arr[3] = CalcCRC(arr[0], arr[1], arr);
            if (serialPort1.IsOpen)
                serialPort1.Write(arr,0,4);
        }

        private void button_Position_Mode_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[5];
            arr[0] = MTR_PROTOCOL_CODE_SET_REG;
            arr[1] = 2;
            arr[2] = MTR_PROTOCOL_REG_POSITION_MODE;
            arr[3] = MTR_POS_STEP;
            arr[4] = CalcCRC(arr[0], arr[1], arr);
            if (serialPort1.IsOpen)
                serialPort1.Write(arr, 0, 5);
        }

        private void button_SetDriverPos_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[9];
            float[] position = new float[1];
            if (textBox_DriverPosition.Text == "")
            {
                // Do nothing
            }
            else
            {
                position[0] = Convert.ToSingle(textBox_DriverPosition.Text);
                var byteArray = new byte[4];
                Buffer.BlockCopy(position, 0, byteArray, 0, byteArray.Length);

                arr[0] = MTR_PROTOCOL_CODE_SET_REG;
                arr[1] = 6;
                arr[2] = MTR_PROTOCOL_REG_POSITION_REF;
                arr[3] = byteArray[0];
                arr[4] = byteArray[1];
                arr[5] = byteArray[2];
                arr[6] = byteArray[3];
                arr[7] = MTR_REF_POS_ABS;
                arr[8] = CalcCRC(arr[0], arr[1], arr);

                if (serialPort1.IsOpen)
                    serialPort1.Write(arr, 0, 9);
            }
        }

        private void button_SetDriverCurrent_Click(object sender, EventArgs e)
        {
            byte[] arr = new byte[8];
            float[] driver_current = new float[1];
            if (textBox_DriverCurrent.Text == "")
            {
                // Do nothing
            }
            else
            {
                driver_current[0] = Convert.ToSingle(textBox_DriverCurrent.Text);
                var byteArray = new byte[4];
                Buffer.BlockCopy(driver_current, 0, byteArray, 0, byteArray.Length);

                arr[0] = MTR_PROTOCOL_CODE_SET_REG;
                arr[1] = 5;
                arr[2] = MTR_PROTOCOL_REG_BASE_CURRENT;
                arr[3] = byteArray[0];
                arr[4] = byteArray[1];
                arr[5] = byteArray[2];
                arr[6] = byteArray[3];
                arr[7] = CalcCRC(arr[0], arr[1], arr);

                if (serialPort1.IsOpen)
                    serialPort1.Write(arr, 0, 8);
            }
        }

        private void fmb_FormClosed1(object sender, FormClosedEventArgs e)
        {
        }
     }
 }

