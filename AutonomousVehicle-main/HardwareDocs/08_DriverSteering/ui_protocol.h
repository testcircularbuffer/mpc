
#ifndef UI_PROTOCOL_H
#define UI_PROTOCOL_H
#include <QSerialPort>
#include <QtCore>
#include <QThread>
#include "protocolprocess.h"
#include <QWidget>
#include <QTimer>
#include <QMutex>
#define FCP_HEADER_SIZE (2)
#define FCP_CRC_SIZE (1)
#define FCP_REG_SIZE (1)
#define MTR_PROTOCOL_CODE_SET_REG        0x01
#define MTR_PROTOCOL_CODE_GET_REG        0x02
#define MTR_PROTOCOL_CODE_EXECUTE_CMD    0x03
#define MTR_PROTOCOL_CODE_REGULAR_UPDATE_DATA 0x0D
#define MTR_PROTOCOL_CODE_MONITOR       0x0E
#define MTR_PROTOCOL_CODE_EMERGENCY		0x0F

#define MTR_PROTOCOL_CODE_STORE_TOADDR   0x04
#define MTR_PROTOCOL_CODE_LOAD_FROMADDR  0x05
#define MTR_PROTOCOL_CODE_GET_BOARD_INFO 0x06
#define MTR_PROTOCOL_CODE_GET_MP_INFO    0x0B
#define MTR_PROTOCOL_CODE_GET_FW_VERSION 0x0C


#define MTR_PROTOCOL_CMD_SERVO_ON	   0x01
#define MTR_PROTOCOL_CMD_SERVO_OFF      0x02
#define MTR_PROTOCOL_CMD_STOP_PPF      0x03
#define MTR_PROTOCOL_CMD_ESTOP         0x04
#define MTR_PROTOCOL_CMD_RESET         0x05
#define MTR_PROTOCOL_CMD_PING          0x06
#define MTR_PROTOCOL_CMD_FAULT_ACK     0x07


#define MTR_PROTOCOL_MONITOR_TRIGGER_EXECUTE 0x00
#define MTR_PROTOCOL_MONITOR_TRIGGER_SEND 0x01
#define MTR_PROTOCOL_MONITOR_TRIGGER_STOP 0x02

#define GUI_ERROR_CODE 0xFFFFFFFF
typedef enum
{
    MTR_PROTOCOL_REG_TARGET_MOTOR,          /* 0   */
    MTR_PROTOCOL_REG_FLAGS,                 /* 1   */
    MTR_PROTOCOL_REG_STATUS,                /* 2   */
    MTR_PROTOCOL_REG_CONTROL_MODE,          /* 3   */
    MTR_PROTOCOL_REG_POS_REF,             /* 4   */
    MTR_PROTOCOL_REG_POS_KP,             /* 4   */

    MTR_PROTOCOL_REG_SPEED_REF,             /* 4   */
    MTR_PROTOCOL_REG_SPEED_KP,              /* 5   */
    MTR_PROTOCOL_REG_SPEED_KI,              /* 6   */
    MTR_PROTOCOL_REG_SPEED_FF,             /* 4   */
    MTR_PROTOCOL_REG_TORQUE_REF,            /* 8   */
    MTR_PROTOCOL_REG_TORQUE_KP,             /* 9   */
    MTR_PROTOCOL_REG_TORQUE_KI,             /* 10  */
    MTR_PROTOCOL_REG_FLUX_REF,              /* 12  */
    MTR_PROTOCOL_REG_FLUX_KP,               /* 13  */
    MTR_PROTOCOL_REG_FLUX_KI,               /* 14  */
    MTR_PROTOCOL_REG_FLUXWK_KP,             /* 22  */
    MTR_PROTOCOL_REG_FLUXWK_KI,             /* 23  */
    MTR_PROTOCOL_REG_FLUXWK_BUS,            /* 24  */
    MTR_PROTOCOL_REG_BUS_VOLTAGE,           /* 25  */
    MTR_PROTOCOL_REG_HEATS_TEMP,            /* 26  */
    MTR_PROTOCOL_REG_MOTOR_PP,           /* 27  */
    MTR_PROTOCOL_REG_SPEED_MEAS,            /* 30  */
    MTR_PROTOCOL_REG_TORQUE_MEAS,           /* 31  */
    MTR_PROTOCOL_REG_FLUX_MEAS,             /* 32  */
    MTR_PROTOCOL_REG_FLUXWK_BUS_MEAS,       /* 33  */
    MTR_PROTOCOL_REG_I_ALPHA,               /* 37  */
    MTR_PROTOCOL_REG_I_BETA,                /* 38  */
    MTR_PROTOCOL_REG_I_Q,                   /* 39  */
    MTR_PROTOCOL_REG_I_D,                   /* 40  */
    MTR_PROTOCOL_REG_I_Q_REF,               /* 41  */
    MTR_PROTOCOL_REG_I_D_REF,               /* 42  */
    MTR_PROTOCOL_REG_V_Q,                   /* 43  */
    MTR_PROTOCOL_REG_V_D,                   /* 44  */
    MTR_PROTOCOL_REG_V_ALPHA,               /* 45  */
    MTR_PROTOCOL_REG_V_BETA,                /* 46  */
    MTR_PROTOCOL_REG_MEAS_EL_ANGLE,         /* 47  */
    MTR_PROTOCOL_REG_MEAS_ROT_SPEED,        /* 48  */
    MTR_PROTOCOL_REG_OBS_BEMF_ALPHA,        /* 53  */
    MTR_PROTOCOL_REG_OBS_BEMF_BETA,         /* 54  */
    MTR_PROTOCOL_REG_OBS_CR_EL_ANGLE,       /* 55  */
    MTR_PROTOCOL_REG_OBS_CR_ROT_SPEED,      /* 56  */
    MTR_PROTOCOL_REG_OBS_CR_I_ALPHA,        /* 57  */
    MTR_PROTOCOL_REG_OBS_CR_I_BETA,         /* 58  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_ALPHA,     /* 59  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_BETA,      /* 60  */
    MTR_PROTOCOL_REG_MAX_APP_SPEED,         /* 63  */
    MTR_PROTOCOL_REG_MIN_APP_SPEED,         /* 64  */
    MTR_PROTOCOL_REG_IQ_SPEEDMODE,          /* 65  */
    MTR_PROTOCOL_REG_EST_BEMF_LEVEL,        /* 66  */
    MTR_PROTOCOL_REG_OBS_BEMF_LEVEL,        /* 67  */
    MTR_PROTOCOL_REG_EST_CR_BEMF_LEVEL,     /* 68  */
    MTR_PROTOCOL_REG_OBS_CR_BEMF_LEVEL,     /* 69  */
    MTR_PROTOCOL_REG_FF_1Q,                 /* 70  */
    MTR_PROTOCOL_REG_FF_1D,                 /* 71  */
    MTR_PROTOCOL_REG_FF_2,                  /* 72  */
    MTR_PROTOCOL_REG_FF_VQ,                 /* 73  */
    MTR_PROTOCOL_REG_FF_VD,                 /* 74  */
    MTR_PROTOCOL_REG_SPEED_KP_DIV,          /* 110 */
    MTR_PROTOCOL_REG_SPEED_KI_DIV,          /* 111 */
    MTR_PROTOCOL_REG_SC_STARTUP_SPEED,      /* 125 */
    MTR_PROTOCOL_REG_SC_STARTUP_ACC,        /* 126 */
    MTR_PROTOCOL_REG_BASE_CURRENT,
    MTR_PROTOCOL_REG_STOP_CURRENT,
    MTR_PROTOCOL_REG_ENCODER_REVOLUTION,
    MTR_PROTOCOL_REG_MICROSTEP,
    MTR_PROTOCOL_REG_SINE_KP,
    MTR_PROTOCOL_REG_SINE_KI,
    MTR_PROTOCOL_REG_SINE_GAIN_1,
    MTR_PROTOCOL_REG_SINE_GAIN_2,
    MTR_PROTOCOL_REG_SINE_GAIN_3,
    MTR_PROTOCOL_REG_SINE_GAIN_4,
    MTR_PROTOCOL_REG_SINE_GAIN_5,
    MTR_PROTOCOL_REG_SPEED_LIMIT,
    MTR_PROTOCOL_REG_CURRENT_LIMIT,
    MTR_PROTOCOL_REG_RESONACE_FRE,
    MTR_PROTOCOL_REG_RESONACE_GAIN,
    MTR_PROTOCOL_REG_SOFWARE_LIMIT_POS,
    MTR_PROTOCOL_REG_SOFWARE_LIMIT_NEG,
    MTR_PROTOCOL_REG_PRESET_POS,

    MTR_PROTOCOL_REG_CHANNEL_1,
    MTR_PROTOCOL_REG_CHANNEL_2,
    MTR_PROTOCOL_REG_CHANNEL_3,
    MTR_PROTOCOL_REG_CHANNEL_4,
    MTR_PROTOCOL_REG_CHANNEL_DIGITAL,
    MTR_PROTOCOL_REG_TRIGGER_CHANNEL,
    MTR_PROTOCOL_REG_TRIGGER_LEVEL,
    MTR_PROTOCOL_REG_TRIGGER_EDGE,
    MTR_PROTOCOL_REG_TRIGGER_POSITION,
    MTR_PROTOCOL_REG_MONITOR_MODE,
    MTR_PROTOCOL_REG_MONITOR_COUNT,
    MTR_PROTOCOL_REG_MONITOR_ENANLE,
    MTR_PROTOCOL_REG_MONITOR_AUTO,

    MTR_PROTOCOL_REG_POSITION_REF,
    MTR_PROTOCOL_REG_MOVE_SPEED,
    MTR_PROTOCOL_REG_ACCEL_TIME,
    MTR_PROTOCOL_REG_DECEL_TIME,
    MTR_PROTOCOL_REG_INTERVAL_TIME,

    MTR_PROTOCOL_REG_POSITION_MODE,
    MTR_PROTOCOL_REG_SPEED_MODE,
    MTR_PROTOCOL_REG_CURRENT_MODE,

} MTR_Protocol_REG_t;
#define ACK_NOERROR 0xF0
#define ACK_ERROR   0xFF
#define ATR_FRAME_START 0xE0


#define ACK_STATE_WAIT 0x00
#define ACK_STATE_ERROR 0x8F
#define ACK_STATE_DONE  0x80

#define MONITOR_STATE_MISS 0x05
#define MONITOR_STATE_IDLE 0x14
#define MONITOR_STATE_TIMEOUT 0xF6

#define MTR_PROTOCOL_CODE_NONE        0x00
/* Marco Trigger edge */
#define MTR_PROTOCOL_TRIGGER_EDGE_RISING    1
#define MTR_PROTOCOL_TRIGGER_EDGE_FALLING   2
#define MTR_PROTOCOL_TRIGGER_EDGE_BOTH      3
/*Macro Trigger mode */
#define MTR_PROTOCOL_TRIGGER_MODE_SINGLE    1
#define MTR_PROTOCOL_TRIGGER_MODE_AUTO      2
/*Marco monitor channel */
#define MTR_PROTOCOL_CH_IA          0x01
#define MTR_PROTOCOL_CH_IAREF       0x02
#define MTR_PROTOCOL_CH_IB          0x03
#define MTR_PROTOCOL_CH_IBREF       0x04
#define MTR_PROTOCOL_CH_IQ          0x05
#define MTR_PROTOCOL_CH_IQREF       0x06
#define MTR_PROTOCOL_CH_ID          0x07
#define MTR_PROTOCOL_CH_IDREF       0x08
#define MTR_PROTOCOL_CH_VA          0x09
#define MTR_PROTOCOL_CH_VAREF       0x0A
#define MTR_PROTOCOL_CH_VB          0x0B
#define MTR_PROTOCOL_CH_VBREF       0x0C
#define MTR_PROTOCOL_CH_VQ          0x0D
#define MTR_PROTOCOL_CH_VQREF       0x0E
#define MTR_PROTOCOL_CH_VD          0x0F
#define MTR_PROTOCOL_CH_VDREF       0x10
#define MTR_PROTOCOL_CH_SPEED       0x11
#define MTR_PROTOCOL_CH_SPEEDREF    0x12
#define MTR_PROTOCOL_CH_SPEEDPPF    0x13
#define MTR_PROTOCOL_CH_SPEEDERROR	0x14
#define MTR_PROTOCOL_CH_POS         0x15
#define MTR_PROTOCOL_CH_POSREF      0x16
#define MTR_PROTOCOL_CH_POSERROR	0x17
#define MTR_PROTOCOL_CH_LEADANGLE   0x18
#define MTR_PROTOCOL_CH_VBUS		0x19
#define MTR_PROTOCOL_CH_TEMPERATURE 0x20


#define MTR_PROTOCOL_CH_1           0x01
#define MTR_PROTOCOL_CH_2           0x02
#define MTR_PROTOCOL_CH_3           0x03
#define MTR_PROTOCOL_CH_4           0x04

#define MTR_PROTOCOL_MONITOR_MOVE_TRIGGER 0x0F
#define MTR_PROTOCOL_MONITOR_MOVE_PERIOD 0xFF
/* Id reference status */
#define     MTR_ID_ZERO_CONST            (0)
#define     MTR_ID_FLUXWKN               (1)
#define     MTR_ID_MANUAL                (2)
/* Iq reference status */
#define     MTR_IQ_ZERO_CONST            (0)
#define     MTR_IQ_SPEED_PI_OUTPUT       (1)
#define     MTR_IQ_MANUAL                (2)
/* Speed reference status */
#define     MTR_SPEED_ZERO_CONST         (0)
#define     MTR_POS_CONTROL_OUTPUT       (1)
#define     MTR_SPEED_MANUAL             (2)
/* Position reference mode */
#define     MTR_POS_ZERO_CONST           (0)
#define     MTR_POS_STEP                 (1)
#define     MTR_POS_TRAPEZOID            (2)
/* Position reference value */
#define     MTR_REF_POS_ABS             (0x00)
#define     MTR_REF_POS_RELATE_POS      (0x01)
#define     MTR_REF_POS_RELATE_NEG      (0x02)
/* Control loop mode */
#define     MTR_LOOP_CURRENT_ID          (0)
#define     MTR_LOOP_CURRENT_IQ          (1)
#define     MTR_LOOP_SPEED               (2)
#define     MTR_LOOP_POSITION            (3)
/* Control method mode */
/*************************/
#define MTR_PROTOCOL_MARK_ERROR				0x0001
#define MTR_PROTOCOL_MARK_HWLIMIT_POS		0x0002
#define MTR_PROTOCOL_MARK_HWLIMIT_NEG		0x0004
#define MTR_PROTOCOL_MARK_SWLIMIT_POS		0x0008
#define MTR_PROTOCOL_MARK_SWLIMIT_NEG		0x0010
#define MTR_PROTOCOL_MARK_SERVO_ON			0x0020
#define MTR_PROTOCOL_MARK_ORIGIN			0x0040
#define MTR_PROTOCOL_MARK_INPOSITION		0x0080

#define MTR_PROTOCOL_MARK_ORIGIN_RETURN		0x0100
#define MTR_PROTOCOL_MARK_ORIGIN_DONE		0x0200
#define MTR_PROTOCOL_MARK_MOTION_DIR		0x0400
#define MTR_PROTOCOL_MARK_MOTIONING			0x0800
#define MTR_PROTOCOL_MARK_MOTION_ACCEL		0x1000
#define MTR_PROTOCOL_MARK_MOTION_DECEL		0x2000
#define MTR_PROTOCOL_MARK_MOTION_CONST		0x4000

#define MTR_PROTOCOL_MARK_INPUT_1			0x0001
#define MTR_PROTOCOL_MARK_INPUT_2			0x0002
#define MTR_PROTOCOL_MARK_INPUT_3			0x0004
#define MTR_PROTOCOL_MARK_INPUT_4			0x0008
#define MTR_PROTOCOL_MARK_OUPUT_1			0x0010
#define MTR_PROTOCOL_MARK_OUPUT_2			0x0020
#define MTR_PROTOCOL_MARK_OUPUT_3			0x0040
#define MTR_PROTOCOL_MARK_OUPUT_4			0x0080


#define     MTR_ERROR_NONE                (0x0000)
#define     MTR_ERROR_OVER_CURRENT_HW     (0x0001)
#define     MTR_ERROR_OVER_VOLTAGE        (0x0002)
#define     MTR_ERROR_OVER_SPEED          (0x0004)
#define     MTR_ERROR_UNDER_VOLTAGE       (0x0080)
#define     MTR_ERROR_OVER_CURRENT_SW     (0x0100)
#define     MTR_ERROR_OVER_TEMPERATURE    (0x0200)
#define     MTR_ERROR_ENCODER_DISCONNECT (0x0400)
#define     MTR_ERROR_UNKNOWN             (0xffff)
#define  	MTR_ERROR_SW 		  			(0x0008)
/* List of error codes */
typedef enum ERROR_CODE_e
{
    ERROR_NONE = 0,             /**<  0x00 - No error */
    ERROR_BAD_FRAME_ID,         /**<  0x01 - BAD Frame ID. The Frame ID has not been recognized by the firmware. */
    ERROR_CODE_SET_READ_ONLY,   /**<  0x02 - Write on read-only. The master wants to write on a read-only register. */
    ERROR_CODE_GET_WRITE_ONLY,  /**<  0x03 - Read not allowed. The value cannot be read. */
    ERROR_CODE_NO_TARGET_DRIVE, /**<  0x04 - Bad target drive. The target motor is not supported by the firmware. */
    ERROR_CODE_WRONG_SET,       /**<  0x05 - Value used in the frame is out of range expected by the FW. */
    ERROR_CODE_CMD_ID,          /**<  0x06 - NOT USED */
    ERROR_CODE_WRONG_CMD,       /**<  0x07 - Bad command ID. The command ID has not been recognized. */
    ERROR_CODE_OVERRUN,         /**<  0x08 - Overrun error. Transmission speed too fast, frame not received correctly */
    ERROR_CODE_TIMEOUT,         /**<  0x09 - Timeout error. Received frame corrupted or unrecognized by the FW. */
    ERROR_CODE_BAD_CRC,         /**<  0x0A - The computed CRC is not equal to the received CRC byte. */
    ERROR_BAD_MOTOR_andle_sSELECTED,   /**<  0x0B - Bad target drive. The target motor is not supported by the firmware. */
    ERROR_MP_NOT_ENABLED        /**<  0x0C - Motor Profiler not enabled. */
} ERROR_CODE;

class UiControlDriver : public QWidget
{
    Q_OBJECT
private:
    QSerialPort* m_pPort;
    QTimer* m_pTimerAck;
    QTimer* m_pTimerMonitor;
    QByteArray m_aRcBuffer;
    unsigned char m_uAckSate;
    unsigned char m_uMonitorSate;
    unsigned char uSeqCmd;
    void SendMonitorMsg(unsigned char uMonitorCmd, unsigned char uSeq);
    unsigned char CalcCRC(unsigned char uCode, unsigned char uSize, unsigned char * buffer);
    unsigned char RxCallBack(unsigned char uCode, unsigned char uSize, unsigned char * buffer);
    void RxProcess(QByteArray aData);

public:

    explicit UiControlDriver(QWidget *parent = nullptr);

    ProtocolProcess* m_pSendPolling;

    bool ConnectPort(QString PortName);
    void DisconnectPort();
    void ConfigPort(QSerialPort::BaudRate BaudRate,
                           QSerialPort::DataBits DataBis,
                           QSerialPort::FlowControl FlowControl,
                           QSerialPort::Parity Parity,
                           QSerialPort::StopBits StopBit);
    void EnableMainUi(bool bMain);
    void SendCmd(unsigned char uCommand);
    void SendAckMonitor(unsigned uSeq);
    void SetReg(MTR_Protocol_REG_t uReg, unsigned char *uSetData ,unsigned char size  );
    void GetReg(MTR_Protocol_REG_t uReg, unsigned char *uGetData,unsigned char size );
signals:
    void SendFrameSignal(unsigned char * txBuffer, unsigned char hLen);
    void SendFrameNoAckSignal(unsigned char * txBuffer, unsigned char hLen);
    void RegularUpdate(float rCmdPos, float rActPos, float rActVel,
                       unsigned short hSwStatusCode, unsigned short hHwStatusCode,
                       unsigned short hErrorCode);
    void UpdateMonitorSignal(unsigned char * puBuffer, unsigned char useq);
    void UpdateAlarm(unsigned char * puBuffer, unsigned char uIndex);
public slots:
    void TimerAckCallBack();
    void TimerMonitorCallBack();
    void RecieveFrame(void);
    void SendFrame(unsigned char * puBuffer, unsigned char hLen);
    void SendFrameNoAck(unsigned char * puBuffer, unsigned char hLen);
};


#endif // UI_PROTOCOL_H
