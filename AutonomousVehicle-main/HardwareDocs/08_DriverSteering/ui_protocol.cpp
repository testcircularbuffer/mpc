#include "ui_protocol.h"
UiControlDriver::UiControlDriver(QWidget *parent)
{

    m_pTimerAck = new QTimer(this);
    m_pTimerMonitor = new QTimer(this);
    m_pSendPolling = new ProtocolProcess(this);
    m_pSendPolling->m_uACKSTATE = ACK_STATE_DONE;
    connect(m_pTimerAck,SIGNAL(timeout()), this, SLOT(TimerAckCallBack()));
    connect(m_pTimerMonitor,SIGNAL(timeout()), this, SLOT(TimerMonitorCallBack()));
}

bool UiControlDriver::ConnectPort(QString PortName)
{
    m_pPort->close();
    m_pPort->setPortName(PortName);
    bool ret = m_pPort->open(QIODevice::ReadWrite);
    m_pSendPolling->m_uACKSTATE = ACK_STATE_DONE;
    m_pSendPolling->m_uMsgCount = 0;
    m_uMonitorSate = MONITOR_STATE_IDLE;
    return ret;

}

void UiControlDriver::DisconnectPort()
{
    m_pPort->close();
}
void UiControlDriver::ConfigPort(QSerialPort::BaudRate BaudRate,
                                 QSerialPort::DataBits DataBits,
                                 QSerialPort::FlowControl FlowControl,
                                 QSerialPort::Parity Parity,
                                 QSerialPort::StopBits StopBit)
{
    m_pPort = new QSerialPort;
    m_pPort->setBaudRate(BaudRate);
    m_pPort->setDataBits(DataBits);
    m_pPort->setParity(Parity);
    m_pPort->setStopBits(StopBit);
    m_pPort->setFlowControl(FlowControl);
}

void UiControlDriver::EnableMainUi(bool bMain)
{
    if (bMain)
    {

        connect(m_pPort,SIGNAL(readyRead()),this,SLOT(RecieveFrame()));
        connect(this,SIGNAL(SendFrameSignal(unsigned char * , unsigned char )),m_pSendPolling,SLOT(AddMsg(unsigned char * , unsigned char )));
        connect(m_pSendPolling,SIGNAL(ReadySend(unsigned char* , unsigned char )),this, SLOT(SendFrame(unsigned char * , unsigned char )));
        m_pSendPolling->start();
    }
    else
    {
        /* Do nothing */
    }
}
void UiControlDriver::SendCmd(unsigned char uCommand)
{
    unsigned char tx_data;
    unsigned char uLen;
    unsigned char TxFrameLevel = 0;
    unsigned char uBuff[256];

    uLen =  FCP_HEADER_SIZE + FCP_CRC_SIZE + FCP_REG_SIZE;
    while (TxFrameLevel <=  FCP_HEADER_SIZE + FCP_REG_SIZE)
    {
        switch ( TxFrameLevel )
        {
        case 0:
            tx_data =  MTR_PROTOCOL_CODE_EXECUTE_CMD;
            break;
        case 1:
            tx_data = 1;
            break;
        case 2:
            tx_data =  uCommand;
            break;
        case 3:
            tx_data = CalcCRC(uBuff[0],uBuff[1],&uBuff[2]);
            break;
        default:
            break;
        }
        uBuff[TxFrameLevel] = tx_data;
        TxFrameLevel++;
    }
    emit SendFrameSignal(uBuff, uLen);
}

void UiControlDriver::SendAckMonitor(unsigned uSeq)
{
    unsigned char tx_data;
    unsigned char uLen;
    unsigned char TxFrameLevel = 0;
    unsigned char uBuff[256];

    uLen =  FCP_HEADER_SIZE + FCP_CRC_SIZE + FCP_REG_SIZE;
    while (TxFrameLevel <=  FCP_HEADER_SIZE + FCP_REG_SIZE)
    {
        switch ( TxFrameLevel )
        {
        case 0:
            tx_data =  MTR_PROTOCOL_CODE_MONITOR;
            break;
        case 1:
            tx_data = 1;
            break;
        case 2:
            tx_data =  uSeq;
            break;
        case 3:
            tx_data = CalcCRC(uBuff[0],uBuff[1],&uBuff[2]);
            break;
        default:
            break;
        }
        uBuff[TxFrameLevel] = tx_data;
        TxFrameLevel++;
    }
    emit SendFrameNoAckSignal(uBuff, uLen);
}

void UiControlDriver::SetReg(MTR_Protocol_REG_t uReg, unsigned char *uSetData,unsigned char uSizeData)
{
    unsigned char tx_data;
    unsigned char uLen;
    unsigned char TxFrameLevel = 0;
    unsigned char uBuff[256];

    uLen = uSizeData + FCP_HEADER_SIZE +FCP_CRC_SIZE+FCP_REG_SIZE;
    while (TxFrameLevel <= uSizeData + FCP_REG_SIZE + FCP_HEADER_SIZE)
    {
        switch ( TxFrameLevel )
        {
        case 0:
            tx_data =  MTR_PROTOCOL_CODE_SET_REG;
            break;
        case 1:
            tx_data =  uSizeData + FCP_REG_SIZE;
            break;
        case 2:
            tx_data = uReg;
            break;
        default:
            if ( TxFrameLevel < uSizeData + FCP_HEADER_SIZE + FCP_REG_SIZE)
            {
                tx_data =  uSetData[TxFrameLevel - FCP_HEADER_SIZE - FCP_REG_SIZE];
            }
            else
            {
                tx_data = CalcCRC(uBuff[0],uBuff[1],&uBuff[2]);
            }
            break;
        }
        uBuff[TxFrameLevel] = tx_data;
        TxFrameLevel++;
    }
    emit SendFrameSignal(uBuff, uLen);
    /* Send the data byte */

}

void UiControlDriver::GetReg(MTR_Protocol_REG_t uReg, unsigned char *uGetData,unsigned char size)
{

}

void UiControlDriver::RecieveFrame()
{
    if(m_pPort->isReadable())
    {
        m_aRcBuffer.append(m_pPort->readAll());
        while (m_aRcBuffer.size() >= 3)
        {
            unsigned char uFrameLen = m_aRcBuffer[1] + 3;
            if (m_aRcBuffer.size() >= uFrameLen)
            {
                QByteArray aPacket = m_aRcBuffer.left(uFrameLen);
                m_aRcBuffer.remove(0,uFrameLen);
                RxProcess(aPacket);
            }
        }
        qDebug()<<QString::number(m_aRcBuffer.length());
    }
}

void UiControlDriver::SendFrame(unsigned char *puBuffer, unsigned char hLen)
{

    m_pPort->write((const char *)puBuffer,hLen);
    m_pSendPolling->m_uACKSTATE = ACK_STATE_WAIT;
    m_pTimerAck->start(1000);
}

void UiControlDriver::SendFrameNoAck(unsigned char *puBuffer, unsigned char hLen)
{
    QMutex Mutex;
    Mutex.lock();
    m_pPort->write((const char *)puBuffer,hLen);
    Mutex.unlock();
}

void UiControlDriver::TimerAckCallBack()
{
    m_pSendPolling->m_uACKSTATE = ACK_STATE_ERROR;
    m_pTimerAck->stop();
    qDebug()<< "Ack Time out";
}

void UiControlDriver::TimerMonitorCallBack()
{
    m_pTimerMonitor->stop();
    m_uMonitorSate = MONITOR_STATE_TIMEOUT;
    qDebug()<<"Monitor time out";
}

void UiControlDriver::SendMonitorMsg(unsigned char uMonitorCmd, unsigned char uSeq)
{
    unsigned char tx_data;
    unsigned char uLen;
    unsigned char TxFrameLevel = 0;
    unsigned char uBuff[256];
    unsigned char uSize = 2;
    uLen = uSize + FCP_HEADER_SIZE +FCP_CRC_SIZE;
    while (TxFrameLevel <= uSize  + FCP_HEADER_SIZE)
    {
        switch ( TxFrameLevel )
        {
        case 0:
            tx_data =  MTR_PROTOCOL_CODE_MONITOR;
            break;
        case 1:
            tx_data =  uSize ;
            break;
        case 2:
            tx_data = uMonitorCmd;
            break;
        case 3:
            tx_data = uSeq;
        default:
            tx_data = CalcCRC(uBuff[0],uBuff[1],&uBuff[2]);
            break;
        }
        uBuff[TxFrameLevel] = tx_data;
        TxFrameLevel++;

    }
    emit SendFrameSignal(uBuff,uLen);
    /* Send the data byte */
}

unsigned char UiControlDriver::CalcCRC(unsigned char uCode, unsigned char uSize, unsigned char * buffer)
{
    uint8_t uCRC = 0;
    uint16_t uSum = 0;
    uint8_t idx;
    {
        uSum += uCode;
        uSum += uSize;

        for ( idx = 0; idx < uSize; idx++ )
        {
            uSum += buffer[idx];
        }

        uCRC = (uint8_t)(uSum & 0xFF) ; // Low Byte of nSum
        uCRC += (uint8_t) (uSum >> 8) ; // High Byte of nSum
    }
    return uCRC ;
}

unsigned char UiControlDriver::RxCallBack(unsigned char uCode, unsigned char uSize, unsigned char *buffer)
{

    switch(uCode)
   {
    case ACK_NOERROR:
        m_pSendPolling->PrepareMsg();
        m_pSendPolling->m_uACKSTATE = ACK_STATE_DONE;
        qDebug()<<"Ack done";
        m_pTimerAck->stop();
        break;
    case ACK_ERROR:
        m_pSendPolling->m_uACKSTATE = ACK_STATE_ERROR;
        qDebug()<< "Ack error" +QString::number(buffer[0]);
        m_pTimerAck->stop();
        break;
    case MTR_PROTOCOL_CODE_SET_REG:
        break;
    case MTR_PROTOCOL_CODE_GET_REG:
        break;
    case MTR_PROTOCOL_CODE_EXECUTE_CMD:
        break;
    case MTR_PROTOCOL_CODE_GET_BOARD_INFO:
        break;
    case MTR_PROTOCOL_CODE_GET_MP_INFO:
        break;
    case MTR_PROTOCOL_CODE_GET_FW_VERSION:
        break;
    case MTR_PROTOCOL_CODE_REGULAR_UPDATE_DATA:
    {
        float rCmdPos = *((float *)&buffer[0]);
        float rActPos = *((float *)&buffer[4]);
        float rActSpd = *((float *)&buffer[8]);
        unsigned short hSwStatusCode = *((unsigned short*)&buffer[12]);
        unsigned short hHwStatusCode = *((unsigned short*)&buffer[14]);
        unsigned short hErrorCode = *((unsigned short*)&buffer[16]);
        emit RegularUpdate(rCmdPos, rActPos, rActSpd,hSwStatusCode,hHwStatusCode,hErrorCode);
        break;
    }
    case MTR_PROTOCOL_CODE_MONITOR:
    {

        unsigned char uSeq = buffer[0];
        qDebug()<<QString::number((uSeq))+"><" + QString::number((uSeqCmd));
        if (uSeq == uSeqCmd)
        {
            emit UpdateMonitorSignal(&buffer[1], uSeq);

            if (uSeqCmd == 0)
            {
                m_pTimerMonitor->start(250);
            }
            if (uSeqCmd < 99)
            {
                uSeqCmd++;
            }
            else
            {
                uSeqCmd = 0;
                m_pTimerMonitor->stop();
            }

        }
        else
        {
            m_uMonitorSate = MONITOR_STATE_MISS;
        }
        if ((m_uMonitorSate == MONITOR_STATE_TIMEOUT)|(m_uMonitorSate == MONITOR_STATE_MISS))
        {
            uSeqCmd = 0;
            m_uMonitorSate = MONITOR_STATE_IDLE;
        }

    }
        break;
    case MTR_PROTOCOL_CODE_EMERGENCY:
    {
        unsigned char uIndex = buffer[0];
        emit UpdateAlarm(&buffer[1],uIndex);
    }
        break;

    default:
        break;
    }

    return 0;
}

void UiControlDriver::RxProcess(QByteArray aData)
{
    unsigned char uCode = aData[0];
    unsigned char uSize = aData[1];
    unsigned char uCRC = 0;
    unsigned char puBuff[256];
    unsigned char hLen  = uSize +FCP_CRC_SIZE +FCP_HEADER_SIZE;
    unsigned char hRxLevel = 0;
    for (int i = 0; i < hLen; i++  )
    {
        switch ( hRxLevel )
        {
        case 0: // First Byte received --> The Code
            uCode = aData[i];
            hRxLevel++;
            break;

        case 1: // Second Byte received --> Size of the payload
            uSize = aData[i];
            hRxLevel++;
            break;

        default: // In the payload or the "CRC"

            if ( hRxLevel < (uSize + FCP_HEADER_SIZE) )
            {
                // Read byte is for the payload
                puBuff[hRxLevel - FCP_HEADER_SIZE] = aData[i];
                hRxLevel++;
            }
            else
            {
                // Read byte is for the "CRC"
                uCRC = aData[i];
                /* Check the Control Sum */
                if ( (CalcCRC(uCode,uSize,puBuff)) == uCRC )
                {
                    RxCallBack(uCode, uSize, puBuff);
                }
                else
                {

                }
            }
            break;
        }
    }
}


